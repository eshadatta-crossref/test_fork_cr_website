{%- if obs.internal_link() -%}   
<div style="color:#ef3340">   
{%- endif -%}
{% if error_count %}
**Number of times the error occurs:** {{ error_count }}   
{% endif -%}   
**Link text:** [{{ obs.anchor_text }}]({{ obs.href }})   
**Link url:** `{{ obs.href }}`     
{%- if templated %}   
**Example page:** [`{{ obs.page }}`]({{ obs.page }})   
{%- endif %}   
**Result:** `{{ obs.result }}`   
**Error ID:** `{{ obs.unique_link_id }}`   
{% if obs.internal_link() -%}   
</div>   
{%- endif %}   

---




