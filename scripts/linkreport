#!/usr/bin/env python

from __future__ import print_function
import sys
import requests
import hashlib
import csv
import json
import re

# from HTMLParser import HTMLParser
from collections import Counter
from collections import OrderedDict
from collections import defaultdict
import numpy as np
from jinja2 import Environment, FileSystemLoader

# from urllib.parse import urlparse
import tldextract
import datetime

# urlname;parentname;baseref;result;warningstring;infostring;valid;url;line;column;name;dltime;size;checktime;cached;level;modified


# linkchecker --output=csv --no-warnings --verbose --ignore-url=\.php$  --check-extern http://testweb.crossref.org | tee testweb.csv


class Observation(object):
    """An HTTP link"""

    def __init__(self, page, href, anchor_text, result, valid):

        # page URL
        self.page = page

        # href
        self.href = href

        # anchor text
        self.anchor_text = anchor_text

        # is this a link-  not some other element that has a URL
        self.is_visible_link = href != "" and anchor_text != ""

        # HTTP status message
        self.result = result

        # Is the an anchor or some other URL that was followed?
        self.valid = valid == "True"

        # So that we can identify links that are essentially the same regardless
        # of what page they are one
        self.unique_link_id = hashlib.sha224(
            (href + anchor_text).encode("utf-8")
        ).hexdigest()

        # So that we can identify specific errors that we might want to ignore
        # in reports (e.g. an example of a deliberately bad link)
        self.unique_error_id = hashlib.sha224(
            (page + href + anchor_text).encode("utf-8")
        ).hexdigest()

    def broken_link(self):
        return self.result == "404 Not Found"

    def internal_link(self):
        return ROOT_DOMAIN in self.href


################################################################


def get_whitelist():
    try:
        response = requests.get(WHITELIST_RESOURCE)
        return json.loads(response.content)
    except:
        return ["Failed to retrieve whitelist from: " + WHITELIST_RESOURCE]


def whitelisted(t):
    # Make a regex that matches if any of our regexes match.
    combined = "(" + ")|(".join(error_whitelist) + ")"
    return re.match(combined, t)


def outliers(a, m=2):
    data = np.array(a)
    return list(set(data[abs(data - np.mean(data)) > m * np.std(data)]))


def inliers(a, m=2):
    data = np.array(a)
    return list(set(data[abs(data - np.mean(data)) < m * np.std(data)]))


def keys_where_value_in_a(d, a):
    return [k for k, v in d.items() if v in a]


def items_where_key_in_a(d, a):
    return {k: v for k, v in d.items() if k in a}


DEFAULT_ROOT_DOMAIN = "crossref.org"
DEFAULT_SITE_URL = "staging.crossref.org"
WHITELIST_RESOURCE = "https://staging.crossref.org/admin/error_whitelist.json"

# HREF_COL=7
HREF_COL = 0
PAGE_COL = 1
RESULT_COL = 3
VALID_COL = 6
ANCHOR_TEXT_COL = 10

try:
    fn = sys.argv[1]
    ROOT_DOMAIN = a[2] if len(sys.argv) > 3 else DEFAULT_ROOT_DOMAIN
    SITE_URL = a[3] if len(sys.argv) > 4 else DEFAULT_SITE_URL
except Exception as e:
    print("You need to provide a linkchecker csv file")
    sys.exit(1)

## Keeping track of thress things....
## Every URL checked
## link errors. 404
## other errors

# All pages
all_pages = set([])
# How many  errors occur on each page
page_total_error_counter = Counter()
# How many link errors occur on each page
page_link_error_counter = Counter()
# How many other errors per page
page_other_error_counter = Counter()

# URLs
unique_urls_counter = Counter()
obs_indexed_by_href = defaultdict(list)

# All errors
errors = []
unique_errors_counter = Counter()
# errors_indexed_by_url = defaultdict(list)
# 404s
broken_links = []
unique_broken_links_counter = Counter()
broken_links_indexed_by_link_id = defaultdict(list)
broken_links_indexed_by_page = defaultdict(list)
# Other errors
other_errors = []
unique_other_errors_counter = Counter()
other_errors_indexed_by_link_id = defaultdict(list)
other_errors_indexed_by_page = defaultdict(list)

# A whitelist of errors to ignore
error_whitelist = get_whitelist()
# Keep track of errors that we encounter and ignore in our reporting
ignored_errors = Counter()


with open(fn, "r") as csvfile:
    lcreader = csv.reader(csvfile, delimiter=";")
    tcount = 1
    rcount = 0
    try:

        for row in lcreader:

            # The person who wrote link checker inserts birthday messages
            # (and possibly other stuff) in the output as comments. Have
            # no idea if/when these will show up. Seriously- he does this.

            rcount += 1
            # print(rcount)
            if len(row) == 1:
                continue
            try:
                page_url = row[PAGE_COL]
                href = row[HREF_COL]
                anchor_text = row[ANCHOR_TEXT_COL]
                result = row[RESULT_COL]
                valid = row[VALID_COL]

                obs = Observation(page_url, href, anchor_text, result, valid)

                # All the pages we've looked at
                all_pages.add(page_url)

                # Record urls we've looked at
                unique_urls_counter[href] += 1

                # Keep track of all places where an href mentioned so that
                # we can check consistency of naming.
                obs_indexed_by_href[obs.href].append(obs)

                # short circuit if the check was ok.
                if obs.valid:
                    continue

                # We have an error, either a 404 or something else
                errors.append(obs)
                unique_errors_counter[obs.unique_link_id] += 1

                page_total_error_counter[page_url] += 1

                if obs.broken_link():
                    # a 404 error was reported

                    # Record page-level
                    page_link_error_counter[page_url] += 1
                    broken_links_indexed_by_page[page_url].append(obs)

                    broken_links.append(obs)
                    unique_broken_links_counter[obs.unique_link_id] += 1
                    broken_links_indexed_by_link_id[obs.unique_link_id].append(obs)
                else:
                    # some other sort of error

                    # Record page-level
                    page_other_error_counter[page_url] += 1
                    other_errors_indexed_by_page[page_url].append(obs)

                    other_errors.append(obs)
                    unique_other_errors_counter[obs.unique_link_id] += 1
                    other_errors_indexed_by_link_id[obs.unique_link_id].append(obs)

            except Exception as e:
                print(row, file=sys.stderr)
                raise e

    except Exception as e:
        print("borked", file=sys.stderr)


## Let's start some reporting

env = Environment(loader=FileSystemLoader("templates"))

## Site summary

site_summary_section = ""
site_summary_template = env.get_template("site_summary.md")
site_summary_section = site_summary_template.render(
    total_pages=len(all_pages),
    total_pages_with_errors=len(page_total_error_counter),
    errors=len(errors),
    unique_errors=len(unique_errors_counter),
    broken_links=len(broken_links),
    unique_broken_links=len(unique_broken_links_counter),
    other_errors=len(other_errors),
    unique_other_errors=len(unique_other_errors_counter),
)


## Collect information about the most frequently broken links.

outlier_counts = outliers(list(unique_broken_links_counter.values()))
outlier_ids = keys_where_value_in_a(unique_broken_links_counter, outlier_counts)
outlier_broken_links = OrderedDict(
    sorted(
        items_where_key_in_a(broken_links_indexed_by_link_id, outlier_ids).items(),
        key=lambda t: len(t[1]),
        reverse=True,
    )
)


# TK do report here
template_link_report_section = ""
template_link_report_template = env.get_template("template_link_report.md")
obs_detail_template = env.get_template("obs_detail.md")

rendered_observations = ""
for outlier_id in outlier_broken_links:

    if whitelisted(outlier_id):
        # if outlier_id in error_whitelist:
        ignored_errors[outlier_id] += 1
        continue

    obs_detail = obs_detail_template.render(
        templated=True,
        error_count=len(outlier_broken_links[outlier_id]),
        obs=outlier_broken_links[outlier_id][0],
    )
    # rendered_observations += obs_detail
    template_link_report_section += obs_detail



# Collect infomration about the most frequently broken pages
# Which pages are most broken?
sorted_broken_pages = OrderedDict(
    sorted(broken_links_indexed_by_page.items(), key=lambda t: len(t[1]), reverse=True)
)

page_report_section = ""
page_report_template = env.get_template("page_report.md")
obs_detail_template = env.get_template("obs_detail.md")


for url, observations in sorted_broken_pages.items():

    if whitelisted(url):
        # if url in error_whitelist:
        ignored_errors[url] += 1
        continue

    seen = []  # don't report the same broken link several times for the page.
    rendered_observations = ""
    for obs in observations:

        if whitelisted(obs.unique_error_id):
            ##if obs.unique_error_id in error_whitelist:
            ignored_errors[obs.unique_error_id] += 1
            continue
        if obs.unique_error_id in seen:
            continue
        seen.append(obs.unique_error_id)

        obs_detail = obs_detail_template.render(templated=False, obs=obs)
        rendered_observations += obs_detail

    page_report = page_report_template.render(
        page_error_count=len(observations),
        page_url=url,
        observations=rendered_observations,
    )
    page_report_section += page_report

## Link naming  consistency
link_consistency_section = ""
link_template = env.get_template("link_detail.md")

for href, observations in obs_indexed_by_href.items():
    if len(observations) > 1:
        # names = defaultdict(list)
        names = defaultdict(set)

        for obs in observations:
            names[repr(obs.anchor_text)].add(obs.page)
        if len(names) > 1:
            link_detail = link_template.render(href=observations[0].href, names=names)
            link_consistency_section += link_detail


template = env.get_template("report.md")

report = template.render(
    sitename=SITE_URL,
    report_date=datetime.datetime.utcnow().isoformat(),
    whitelist_url=WHITELIST_RESOURCE,
    whitelist=error_whitelist,
    ignored_errors=ignored_errors,
    site_summary_section=site_summary_section,
    template_link_report_section=template_link_report_section,
    page_report_section=page_report_section,
    link_consistency_section=link_consistency_section,
)


print(report)

print("Done")

