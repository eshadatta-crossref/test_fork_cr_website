+++
title = "Product Manager"
date = 2021-01-19
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Bryan Vickery"
weight = 3
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position are closed. {{% /divwrap %}}

## Product Manager

**Location:** Remote

Crossref makes research objects easy to find, cite, link, assess, and reuse. As an open infrastructure organization, we ingest and distribute metadata from our 13000+ member organizations worldwide, ensuring community collaboration in everything that we do. Our work helps achieve open research and open metadata goals, for the benefit of society. This is a time of considerable change for the Crossref community, and you can help shape our future.  

We are a [small team](/people/) with a big impact, and we’re looking for a creative, technically-oriented Product Manager to join us in improving scholarly communications. Reporting to the [Director of Product](/people/org-chart/), Product Managers at Crossref are expected to work across multiple products and services. This role, at least initially, is focused around ‘scholarly stewardship’ and includes [Similarity Check](/services/similarity-check/), [Funder Registry](/services/funder-registry/) and [registering research grants](/education/content-registration/content-types-intro/grants/), as well as others.  

### Key responsibilities  

- Manage all aspects of the product life-cycle for one or more key services or products within the Crossref ecosystem
- Seek, absorb and articulate community input through advisory groups, convening meetings, and reviewing data
- Influence product strategy focusing on business priorities and user experience
- Integrate usability studies, user research, system investigations, and ongoing community feedback into product requirements
- Define goals, methods and metrics for product adoption to track success
- Coordinate and direct working groups made up of community members and users
- Promote product adoption directly with the community as well as develop relationships with key community influencers and strategic partners
- Evangelize products and values to rally people and resources behind ideas and ambitions critical to success
- Advance the concepts of research integrity within the scholarly community

### About you  

- You think in terms of the big picture, but deliver on the details
- You have a nose for great products and advocate for new features using qualitative and quantitative reasoning
- You can turn incomplete, conflicting, or ambiguous inputs into solid action plans
- You will, ideally, have an understanding and experience of complex workflow systems
- You care about open infrastructure and want to make scholarly communications better
- You do whatever it takes to make your product and community successful, whether that means writing a QA plan or track down the root cause of a user’s frustration
- You are passionate about understanding community needs, working transparently, eliciting advice and feedback openly and advising on community calls
- You communicate with empathy and exceptional precision
- You can convey and encapsulate strategic (and technical) concepts in presentations verbally, visually, and textually.
- You are comfortable working with developers. You are technical enough to discuss with engineers critical questions about architecture and product choices
- You obsess about continuous product improvement
- You are self-motivated with a collaborative and can-do attitude and enjoy working with a small team across multiple time zones
- You maintain order in a dynamic environment, managing multiple priorities
- You are committed to agile best practices
- You have 5+ years of product management experience with internet technologies and/or equivalent experience in the research publishing arena

This position is full time and, as for all Crossref employees, location is flexible. The Crossref team is geographically distributed in Europe and North America and we fully support working from home. We have two small offices (Lynnfield, MA, USA and Oxford, UK) that are temporarily closed due to the pandemic. It would be good to have a minimum 3-hour overlap with the US Eastern time zone.   

To apply, please send your cover letter, resume, and at least one sample of an effective product specification or epic/story development to Lindsay Russell at jobs@crossref.org. Even if you don’t think you have all of the right experience, we’re really excited to hear from you.  

Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff is critical to our success as a global organization, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.  

Crossref is committed to a policy of non-discrimination and equal opportunity for all  employees and qualified applicants for employment without regard to  race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.