+++
title = "Technical Support Specialist"
author = "Isaac Farley"
date = 2021-06-15
draft = false
image = "/images/banner-images/jobs-robots.jpg"
weight = 3
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position are closed. {{% /divwrap %}}  

## Technical Support Specialist

**Location:** Remote (Africa, West Asia) <br>
**Closing date:** Thursday, 2021 July 15 <br>

## About Crossref
Crossref makes research objects easy to find, cite, link, assess, and reuse. As an open infrastructure organisation, we ingest and distribute metadata from our 14000+ member organisations worldwide, ensuring community collaboration in everything that we do. Our work helps achieve open research and open metadata goals, for the benefit of society. This is a time of considerable change for the Crossref community, and you can help shape our future.

## About the role
Reporting to our Technical Support Manager, the full-time Technical Support Specialist is an important role in our Member Experience team, part of Crossref’s Outreach group, a fourteen-strong distributed [team](https://www.crossref.org/people/org-chart/) with colleagues across the US and Europe. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re aiming for a more [open approach](https://www.crossref.org/strategy/#live-up-to-posi) to having conversations with people all around the world - including within our growing [community forum](https://community.crossref.org/), which the right candidate will help us expand, in multiple languages.

We’re looking for a Technical Support Specialist to provide first-line help to our international community of publishers, librarians, funders, researchers, and developers on a range of services that help them deposit, find, link, cite, and assess scholarly content. You’ll be working closely with five other technical and membership support colleagues to provide support and guidance for people with a wide range of technical experience. The strongest candidates will not necessarily be from a technical background, but they’ll have interest and initiative to grow their technical skills while communicating the complexity of our products and services in straightforward and easy-to-understand terms. You’ll help our community both create and retrieve metadata records with tools ranging from simple user interfaces to APIs and integrations.

Crossref is a distributed team serving members and users around the world. We are seeking candidates to [bolster our team](https://www.crossref.org/strategy/#bolster-the-team), in accordance with our 2025 strategic agenda, to work remotely from Africa or West Asia (where Crossref is administratively able to support employees). We work a flexible schedule; for training and synchronous problem-solving, we also ask that candidates have availability between 13.00 and 15:00 UTC.

## Key responsibilities
- Replying to and solving community queries using the Zendesk support system
- Using our various tools and APIs to find the answers to these queries, or pointing users to support materials that will help them
- Working with colleagues on particularly tricky tickets, escalating as necessary
- Working efficiently but also kindly and with empathy with our very diverse, global community

## About you
We are looking for a proactive candidate with a unique blend of customer service skills, analytical trouble-shooting skills, and a passion to help others. You’ll have an interest in data and technology and will be a quick learner of new technologies. You’ll be able to build relationships with our community members and serve their very diverse needs - from assisting those with basic queries to really digging into some knotty technical queries. Because of this, you’ll also be able to distill those complex and technically challenging queries into easy-to-follow guidance.

**Essential**

- The ability to clearly communicate complex technical information to technical and non-technical users, using open questions to get to the bottom of things when queries don’t seem to make sense
- Quick learner of new technologies; can rapidly pick up new processes and systems; and, have interest and initiative to grow your own technical skills
- Extremely organized and can bring order to chaos, independently manage multiple priorities
- Able to balance a very diverse role, wearing a lot of different hats and providing a wide range of support
- Proactive in asking questions and making suggestions for improvements
- Process-driven but able to cope with occasional ambiguity and lack of clarity - open to feedback and adaptable when things change quickly
- A truly global perspective - we have over 15,000 member organizations from 140 countries across numerous time zones

**Nice to have**

- Experience helping customers and solving problems in creative and unique ways
- Experience with or interest in XML, metadata, and Crossref as well as scholarly research and information science
- Experience with Zendesk and Gitlab or similar support and issue management software

---

## To apply

To apply, please send your cover letter and resume to Lindsay Russell at [jobs@crossref.org](mailto:jobs@crossref.org). Even if you don’t think you have all of the right experience, we’re really excited to hear from you.


Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff is critical to our success as a global organization, and we seek to recruit, develop and retain the most talented people from a diverse candidate pool.

Crossref is committed to a policy of non-discrimination and equal opportunity for all  employees and qualified applicants for employment without regard to  race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.