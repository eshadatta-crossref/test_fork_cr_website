+++
title = "R&D Programmer"
date = 2018-01-05
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Geoffrey Bilder"
weight = 3
parent = "Jobs"
type = "jobs-labs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position are closed as of 2018-02-28 {{% /divwrap %}}

## Come work at Crossref as an **R&D Programmer**. It'll be fun!
<br>
- **Location:** Based in Oxford, UK. Remote work possible<br>
- **Reports to:** Director of Strategic Initiatives<br>
- **Benefits:** Competitive<br>

## About the position

We are hiring an R&D Programmer to help prototype and develop new web-based tools and services.

### We are looking for someone who:

- Is expert in one or more programming languages (Java, Clojure, Python, Ruby).
- Wants to learn new skills and work with a variety of technologies.
- Relishes working with metadata.
- Has experience delivering web-based applications using agile methodologies.
- Enjoys working with a small, geographically dispersed team.
- Groks mixed-content model XML.
- Groks RDF.
- Groks REST.
- Can see a solo project through or collaborate in a larger team.
- Has deployed and maintained Linux based systems.
- Understands relational databases (MySQL, Oracle).
- Tests first.


### Bonus points for:

- Experience building tools for online scholarly communication.
- Experience with a variety of programming language paradigms (OO, Functional, Declarative).
- Experience with ElasticSearch, Solr or Lucene.
- Having contributed to open source projects.
- Experience with front-end development (HTML, CSS, React, Angular or similar).
- Having worked on standards bodies.
- Experience with public speaking.


### Responsibilities

The R&D Programmer will report to the Director of Strategic Initiatives. They will be responsible for prototyping and developing new Crossref initiatives and applying new Internet technologies to further Crossref’s mission to make research outputs easy to find, cite, link, assess, and reuse.

The post will work with the Director of Strategic Initiatives and the Director of Technology to research, develop and implement new services – taking ideas from concept to prototype and, where appropriate, create and deploy production services.

The R&D Programmer may represent Crossref at conferences and in industry activities and projects. They will play also an active role in developing industry and community technical standards and help develop technical guidelines for Crossref members.

Working with the Director of Strategic Initiatives and Director of Technology, the R&D Programmer will ensure that new services are designed with a robust and sustainable architecture.  The R&D Programmer will actively engage with technical representatives from Crossref’s membership, the library community, scholarly researchers and broader Internet initiatives.


### Location & travel requirements

Crossref has offices in the US (Lynnfield, Massachusetts) and the UK (Oxford). We also support remote work. This position is in the R&D team, which is currently split between the UK and France.

Remote workers should expect they will need to visit an office approximately 5 days a quarter along the travel (possibly international) which that entails.

If you work from an office you will be expected to travel internationally for ~ 5 days once a year.

The position In either case, travel *can* increase should you have an interest in representing Crossref at industry events.

### Salary

Competitive depending on skills and expertise. Excellent benefits.

### To apply

Send cover letter and a CV via email to:

Geoffrey Bilder

gbilder@crossref.org

### About Crossref

Crossref is a not-for-profit membership organization representing members of all stripes (commercial publishers, scientific societies, university presses, open access, etc.). Crossref currently has just over 30 staff in the US and UK and France, yet it combines the small, intimate atmosphere of a technical startup, with the financial stability and strong international presence of a major commercial organization. We do important stuff, but we have a lot of fun doing it.

---
Please contact [Geoffrey Bilder](mailto:gbilder@crossref.org) with any questions.