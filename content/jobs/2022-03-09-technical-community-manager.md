+++
title = "Technical Community Manager"
author = "Rosa Clark"
date = 2022-03-09
draft = false
image = "/images/banner-images/forum-starlings.jpg"
weight = 1
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position closed March 2022. {{% /divwrap %}}

## Join us as our brand new **Technical Community Manager** to expand the adoption and integration of ROR (Research Organization Registry) throughout the global scholarly communications ecosystem.

We are looking for a full-time Technical Community Manager to expand the adoption and integration of ROR throughout the global scholarly communications ecosystem.

[ROR](https://ror.org/) is a community-led initiative to develop an open, sustainable, usable, and unique identifier for every research organization in the world. It is jointly managed by DataCite, Crossref, and California Digital Library. Each of these operating organizations provides input on decisions and strategies that support the growth and sustainability of ROR. Our goal is to address the problem of tracking affiliations in research communications. The Technical Community Manager will be a key driver of that change.

The Technical Community Manager will work closely with the small and committed core ROR team, staff from the three operating organizations, and the broader ROR community, to promote and support the adoption of ROR in systems used throughout research and scholarly communications workflows. This includes engaging with new and existing ROR adopters and other community stakeholders to understand their workflows and systems, and to guide their implementations and integrations.

This position will be employed by Crossref as a full-time staff member and included in all Crossref staff activities. It is fully remote, so location and hours are flexible but overlap with the US Pacific timezone will be necessary. As pandemic circumstances allow, we expect to resume a small amount of international travel for meetings and events.

## Responsibilities

1.  Build strategies to drive adoption and technical implementation  
- Identify opportunities and challenges for adoption and integration of ROR in key research and scholarly communications systems and workflows  
- Develop and implement strategies to encourage and support the adoption and integration of ROR into scholarly communication systems and workflows, including showcasing exemplars and promoting best practices
- Develop measures/benchmarks to assess and communicate adoption progress both internally and to the ROR community  

2. Lead technical community engagement efforts  
- Organize regular meetings, webinars, and other events for new and current integrators
- Engage with the ROR community to develop and encourage integration best practices
- Build and maintain tools and documentation that meet the needs of key communities
- Engage with the ROR community to identify and build consensus around evolving needs Provide first-line support and troubleshooting help to adopters

3. Cultivate and manage relationships with adopters
- Introduce ROR to potential adopters you have identified, such as specific publishers, funders, repositories, research institutions, and the service providers and developers that offer platforms to those organizations
- Maintain ongoing communications with adopters through regular check-ins to ensure their integration work is well-supported
- Consult with adopters to recommend ROR integration approaches for their particular system/use case, collaborating with other ROR team members as needed
- Communicate feedback about adopter needs to the ROR team

4. Contribute to the development and implementation of overall ROR strategies
- Based on community needs, identify specifications for improvements and new features
- Collaborate with ROR team on product development strategy
- Work with ROR and Crossref teams to develop and implement strategies that support wider adoption

## Skills and experience

- Community management experience, particularly in an international environment.
- An understanding that community management needs a mix of interpersonal, technical, program management, program development, and communication skills. The CSCCE skills wheel is a good resource to explore.
- While we don’t ask for a specific number of years' experience, entry-level candidates are unlikely to be successful in the role.
- Sufficient technical skills to advise adopters on integrations, including experience with making and troubleshooting requests to RESTful APIs and familiarity with XML and JSON data structures (or technical aptitude and a desire to learn!)
- Deep knowledge of research and scholarly communications systems and workflows, and familiarity with the academic research environment  Familiarity with research infrastructure and the open science landscape
- Familiarity with a not-for-profit environment and the transparency that entails
- Ability to work remotely with small distributed teams across global time zones
- Strong, compelling, and clear written, oral, and visual communication
- Self-motivated to succeed, take initiative, and seek continuous improvement

## Working at ROR & Crossref

As a young start-up initiative, ROR is a dynamic place to work as we are growing quickly and laying the groundwork for long-term sustainability. We are a fun community (we do actually roar sometimes 🦁) but we also take our work seriously! ROR’s three operating organizations work closely together and everyone contributing to ROR balances the needs of ROR with those of their home organization. We also work closely with ROR adopters and community stakeholders through working groups and advisory boards and aim for all of these activities to be open and transparent, in line with the Principles of Open Scholarly Infrastructure (POSI).  

ROR has a Project Lead based at CDL and a Metadata Curation Lead contracted with Crossref. Our previous Adoption Lead has moved over to become our new full-time Technical Lead based at DataCite. We’re now reshaping the previous adoption role on the ROR team as this Technical Community Manager. This is a full-time role and you will be employed at Crossref. This means you will need to balance being part of two teams: ROR; and Crossref.  

Crossref is committed to supporting ongoing professional development opportunities and promoting self-learning for its 40+ people. Crossref—and ROR—are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture.”  

## Thinking of applying?

We especially encourage applications from people with backgrounds historically underrepresented in research and scholarly communications.  

The role will be accountable to the ROR operations team and within Crossref will report to Ginny Hendricks who will review applications along with Project Lead Maria Gould and Technical Lead Liz Krznarich. Candidates who meet the qualifications will be invited to a 30-minute screening call. Those subsequently shortlisted will be invited to a 90-minute online interview which will include an exercise you’ll have a chance to prepare for.  

To apply, please send a CV and covering letter explaining how your skills match ROR’s goals to  [jobs@ror.org](mailto:obs@ror.org), by 16th March 2022. Interviews will take place in late March/early April.

## Equal opportunities commitment

Crossref and ROR are committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref and ROR will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

**Thanks for your interest in joining Crossref. We are excited to hear from you!**