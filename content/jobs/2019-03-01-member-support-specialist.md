+++
title = "Member Support Specialist"
date = 2019-03-01
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Amanda Bartell"
weight = 3
rank = 4
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position are closed as of 2019-04-15 {{% /divwrap %}}

## Come and work with us as our **Member Support Specialist**. It’ll be fun!

Are you a customer support specialist looking to expand your technical skills? Maybe you’re an editorial assistant keen to improve scholarly communications?

- **Location:** Flexible. Either office-based (Lynnfield, MA or Oxford, UK) or remote/home-based in North America or Europe
- **Reports to:** Amanda Bartell, Head of Member Experience
- **Salary and benefits:** Competitive

## About the role

This role is focused on Similarity Check, one of our key services which helps our members promote editorial integrity. It’s a very diverse role and is a great opportunity to get wide-ranging experience within Crossref and scholarly communications. You’ll be working closely with three other technical support staff, our membership coordinator, our billing team, and the product manager for this service - plus publishers across the globe. You’ll be helping members understand their technical obligations for participating in Similarity Check, navigate the application process, and learn how to use the service. You’ll make sure that our support materials meet the needs of the members, and be the “voice of the user” in conversations with the developers and partners who run the service.

This is an important new role in the Member Experience team, part of Crossref’s Outreach team, a fourteen-strong team split between offices in Boston and Oxford, plus dispersed team members across the US and Europe. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re embarking on a new onboarding program for the thousands of publishers that join as members every year and currently rolling out an educational program for existing members and users. And we’re aiming for a more open approach to having conversations with people all around the world, in multiple languages.



## Key responsibilities

- Support Similarity Check users and applicants via email, Twitter and other channels.
- Own and drive the administrative process for members taking on the service. 
- Support members in meeting the technical obligations for participating in the service. 
- Manage the onboarding process, working with our Education Manager and the Product Manager to ensure that documentation and other support materials meet member needs.
- Providing support for members using the Similarity Check service through iThenticate.
- Provide support for queries about Similarity Check billing. 
- Analyze, communicate and triage support issues to the product manager and technology providers to inform future development. 
- Work closely with the technology providers’ support team to proactively communicate with members about technical issues, outages, and planned maintenance etc.
- Provide wider technical and membership support for other services as required.


## About you

We're looking for a smart, savvy person who can wear many hats, has a truly global outlook, a collaborative style, and an inner drive to make things better. You’ll be a quick learner of new technologies, and ideally have an understanding of editorial processes. You’ll have a unique blend of analytical troubleshooting skills, customer service skills, and a passion to help others. You’ll be able to build relationships with our members and serve their very diverse needs - from hand holding those with basic queries to really digging into some knotty technical queries.
 
You are:

- Ideally familiar with manuscript submission and review process.
- Able to balance a very diverse role, wearing a lot of different hats and providing a wide range of support. 
- Experience helping customers and solving problems in creative and unique ways. 
- Able to communicate technical issues to a non-technical audience and use open questions to get to the bottom of things when members don’t seem to make sense.
- Strong written and verbal communication skills with the ability to communicate clearly.
- A truly global perspective - we have 10,000 member organizations from 118 countries across numerous time zones.
- Quick learner of new technologies and can rapidly pick up new programs and systems.
- Extremely organized and attentive to detail 
- Bachelor's degree. 
- Experience with Zendesk and Jira or similar support and issue management software will be helpful. 
- Knowledge of XML, metadata, scholarly research or information science a bonus.
- Other languages a bonus, particularly Russian.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have over 11,000 members across 118 countries, and thousands of tools and services relying on our metadata.

---

## To apply

If you’d like to join the Crossref team, and contribute to [our mission](/about), please send a cover letter and your CV to [jobs@crossref.org](mailto:jobs@crossref.org). We can't wait to read all about you!