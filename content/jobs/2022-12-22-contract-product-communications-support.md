+++
title = "Product Communications Support Contractor"
date = 2022-10-28
draft = false
image = "/images/banner-images/hr-job-wildflower-meadow.jpg"
author = "Michelle Cancel"
caption = "“Flower Garden”, Euskirchen, Germany, by Mina-Marie Michell via Pexels"
weight = 1
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position is closed. {{% /divwrap %}}


## Request for services: Contract for Product Communications Support

Come and work with us as an independent **Product Communications Support Contractor**. It’ll be fun!

**Location:** Remote  
**Duration of contract:** 12 months

## About the contractor role
We require services for product communications to support the Community Engagement and Communications team in liaising effectively with both Product and Outreach teams, to keep our community abreast of progress, and to encourage broad adoption of existing and new tools. You will be collaborating with Community Engagement and Communications colleagues to devise effective ways of engaging our members and other stakeholders with the ever-changing functionality of the tools we provide.

If you’d like to use engaging product communications to help organisations make scholarly outputs more discoverable, and support the integrity of the scholarly record, this freelance opportunity may be for you.

## Scope of work
- Identify opportunities and create campaigns to highlight less well-known and changing functionality of existing products and services, and, working with product managers, create and manage associated communications and launch plans.
- Support the Events & Communications Manager in managing the content calendar, ensuring a balanced proportion of product communications in the context of overall outreach and engagement activities.
- Support Crossref in soliciting community feedback and—leveraging others’ insights across the organisation—tailor effective ways of engaging our diverse and global audiences.
- Translate complex technical information into written actionable and mobilising language, with deliverables such as keeping our slide library up to date with relevant stories, facts, and figures.
- Collaborate with subject-matter experts to update each 'Service' page on our website, integrating them with project tracking tools to make these sections dynamic and current.
- Work with the full range of digital communications to engage our community. Note we use Act-On (email), Hugo (website/blog), Discourse (forum), Trello (org-wide roadmap), and Jira (product development and R&D).

## Deliverables
- Development and deployment of audience-centred communications resources to engage the Crossref community with relevant services and maximise usage as appropriate, in support of the Managed Member Journey (including but not limited to communications about metadata completeness, registering references, use of crossmark, relationships metadata).
- Campaigns plans and execution in support of launch of new and updated services, such as changes to Crossref APIs and grant registration form.
- User and audience research for service updates under development, such as Relationships API and new Participation Reports.
- Delivery of online showcase and support events for new and updated Crossref services.

## About the team
The contractor will work closely with Kora Korzec, Head of Community Engagement and Communications, and will collaborate with the broader Outreach group on a variety of projects and programmes to augment our product communications, using a full range of digital communications and leverage event opportunities, to maximise adoption of tools and best practice. We adopt an approachable, community-appropriate tone and style in our communications. We’re looking to re-engage with our community through face-to-face opportunities as well as online, so some travel may be involved in this role (according to our [our latest thinking on travel and sustainability](/blog/rethinking-staff-travel-meetings-and-events/)).

Our primary aim is to engage colleagues from the member organizations and other stakeholders to be actively involved in capturing documentation of the scholarly progress and making it transparent. This contributes to co-creating a robust [research nexus](/blog/seeing-your-place-in-the-research-nexus/). As part of the wider Outreach group at Crossref, we seek to encourage adoption and development of best practices in scholarly publishing and communication with regards to metadata and permanence of scholarly record. [Colleagues across the organization](/people/) are helpful, easy-going and supportive, and you’d be expected to work collaboratively with different teams. You can also watch the [recording of our recent Annual Meeting](/crossref-live-annual/) to learn more about the current conversations in our community.

## About Crossref
Crossref is a non-profit membership organization that exists to make scholarly communications better. We make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs.

Crossref is at its core a community organization with 17,500 members across 148 countries (and counting)! We’re committed to lowering barriers for global participation in the research enterprise, we’re funded by members and subscribers, and we engage regularly with them in multiple ways from webinars to working groups.

Crossref operates and continuously develops an impressive [portfolio of services](/services/), products and features to support scholarly communication and infrastructure organisations to contribute to, maintain and preserve robust documentation of the scholarly process. From registration forms and APIs, to complex systems of linking scholarly works with references or citations, and metadata retrieval, our busy Product Team continuously develops and refines these metadata tools.

## About You
- 3+ years of experience in technical customer service/marketing/product communications
- Collaborative attitude, pragmatic and proactive approach
- Excellent communications skills in English (we’re always happy to hear if you’re able to communicate in other languages too)
- Ability to translate complex technical information into accessible narratives in English
- Experience of content creation and leveraging diverse digital engagement and communications channels to get a message across
- Experience of organising and executing online events
- Demonstrable effective project management skills
- Good understanding of audience segmentation; experience of doing it would be even better
- Good working understanding of product development life cycle
- Experience working in academia or scholarly communications is nice to have but not required
- Experience conducting customer or user research (qualitative or quantitative) would be useful but not essential

---

## How to respond  

If you’ve got a passion for equity and diversity and would like to use your engagement and communications skills to help our community shed light on the inner workings behind the progress of science, we encourage you to read on and respond by February 5, 2023.

Please send a CV and a cover letter (each no longer than 2 pages) to share how you meet the requirements of the contract role, accompanied by a brief portfolio of relevant work, and a rate sheet or fee schedule to [jobs@crossref.org](mailto:jobs@crossref.org). One of the best ways of offering evidence of your suitability in the letter is with an example of a relevant project, highlighting your contributions to that project that showcase relevant skills. Nota bene, if you’re including hyperlinks in any of your documents to things available online, please ensure these can be accessed by third parties, and give some context as to what was your role in creating it if not clearly stated on the material itself. As it’s essential for this work that you have access to reliable high speed internet connection, please indicate clearly in your letter whether that is the case.

We intend to start reviewing responses on February 6, 2023 and contact the selected profiles shortly after to start conversations.