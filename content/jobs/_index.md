+++
title = "Jobs"
date = "2023-03-13"
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Lucy Ofiesh"
Rank = 2
aliases = [ "/01company/jobs.html",]
x-version = "0.0.0"

[menu.main]
parent = "About us"
weight = 30

+++

## Come work at Crossref. It'll be fun!

Help us achieve [our mission](/strategy) to make research outputs easier to find, cite, link, assess, and reuse. We're a small but mighty group working with over 17,000 members from 146 countries, and we have thousands of tools and services relying on our metadata. We take our work seriously but usually not ourselves. We have offices in Oxford, UK, and Lynnfield, MA, USA, but actively seek remote/home-based people too.  

{{% divwrap blue-highlight %}}

[Product Manager](/jobs/2023-03-11-product-manager-2023-03-11)  

[Contract Technical Support](/jobs/2023-03-01-contract-technical-support)  
{{% /divwrap %}}



Take a look at our [current team](/people), many of whom have worked here for years, and check out the [org chart](/people/org-chart) to see where our vacancies fit in.  

<!-- {{% divwrap yellow-highlight %}} We don't have any jobs open currently; please check back another time! {{% /divwrap %}}   -->

<!-- ## Open Positions

{{% divwrap blue-highlight %}} <a href="">Title</a> (closes YYYY-MM-DD) {{% /divwrap %}} -->





---
Please contact our HR Manager, [Michelle Cancel](mailto:jobs@crossref.org), for any questions.