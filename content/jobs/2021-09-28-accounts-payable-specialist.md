+++
title = "Accounts Payable Specialist"
author = "Lucy Ofiesh"
date = 2021-09-28
draft = false
image = "/images/banner-images/jobs-robots.jpg"
weight = 3
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position closed 2021-11-29. {{% /divwrap %}}  

## Come and work with us as our **Accounts Payable Specialist**. It’ll be fun!

​​Crossref makes research objects easy to find, cite, link, assess, and reuse. As an open infrastructure organization, we ingest and distribute metadata from our 14000+ member organizations worldwide, ensuring community collaboration in everything that we do. Our work helps achieve open research and open metadata goals for the benefit of society. This is a time of considerable change for the Crossref community, and you can help shape our future.

We are a [small team](/people/) with a big impact, and we’re looking for a detail-oriented self-starter to join the team as Part-time Accounts Payable Specialist.

## About the Role

Reporting to the Supervising Accountant, the Accounts Payable Specialist is a key role within the Finance team. The Accounts Payable Specialist is responsible for full-cycle Accounts Payable accounting for both USA and UK-based offices, assuring proper recording within the accounting system. This position is the lead contact for vendor relations and the internal expense reporting application. The position requires a skill set and personality type capable of performing a broad range of duties and responsibilities with minimal supervision and a high degree of accuracy and thoroughness.

## About You

The successful candidate will possess the following:
* The ability to organize work, set priorities, follow-up and work proactively and accurately
* Excellent oral, written, data entry, and communication skills
* The ability to work collaboratively and independently
* A self-starter and problem solver with exceptional attention to detail
* Able to adapt and succeed in a fluid and flexible environment
* Be motivated, self-directed, and detail-oriented
* Have experience in a multi-currency environment (GBP)

## Key Responsibilities
* Responsible for the full cycle AP function for both the UK and USA offices, including entering invoices into Intacct, obtaining payment approvals, and facilitating payment processing (checks/wires/direct debits/ACH’s)
* Responsible for managing corporate credit cards, including reviewing and reconciling to statements monthly
* Responsible for the Expensify expense reporting platform, including maintaining knowledge of updates and enhancements and troubleshooting
* Responsible for the yearly 1099/1096 filing and vendor reporting
* Act as backup for other Finance Team staff
* Responding to Zendesk inquires and assisting in collections as needed
* Assist with monthly and quarterly financial reporting
* Assist with audit
* Other ad hoc financial and operational projects

## Qualifications
* 2-5 years of accounting experience
* Solid experience using cloud-based/accounting applications (Intacct)
* Solid experience using Microsoft Excel and other tools (Gmail/google docs, etc.)
* Bachelors Degree in Accounting/Business or equivalent business experience preferred

This position is full-time (30 hours). The Crossref team is geographically distributed in Europe, North America and Africa, and we fully support working from home. We have two small offices (Lynnfield, MA, USA and Oxford, UK) that are temporarily closed due to the pandemic. It would be good to have a minimum 3-hour overlap with the US Eastern time zone.

##  To Apply

To apply, please send your cover letter and resume to Lindsay Russell at [jobs@crossref.org](mailto:jobs@crossref.org). Even if you don’t think you have all of the right experience, we’re really excited to hear from you.

Crossref is an equal opportunity employer. We believe that diversity and inclusion among our staff are critical to our success as a global organization. Therefore, we seek to recruit, develop, and retain the most talented people from a diverse candidate pool.

Crossref is committed to a policy of non-discrimination and equal opportunity for all  employees and qualified applicants for employment without regard to  race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.