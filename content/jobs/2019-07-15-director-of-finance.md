+++
title = "Director of Finance & Operations"
date = 2019-07-15
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Lindsay Russell"
weight = 3
rank = 4
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position closed August 2019. {{% /divwrap %}}

**Location:** Flexible - Crossref has members globally and offices in Oxford, UK and Lynnfield, MA, USA but being office-based is not necessary.      
**Reports to:** Ed Pentz, Executive Director     
**Salary and benefits:** Competitive  

{{% divwrap blue-highlight %}} Applications for this position will close on 9 August 2019. {{% /divwrap %}}  

## In a nutshell

Crossref faces an exciting future as we grow further internationally beyond our existing 120 countries and our staff becomes increasingly distributed. We're seeking a globally-minded and resourceful person with the skills, vision, and drive to help us achieve our mission. That means putting our community first and making it easier to work with us across the board - from implementing modern systems that work for all our members' many languages and currencies, to providing personable support. This is not just your average finance role; everyone at Crossref wears many hats, can articulate our value and purpose, and has a naturally collaborative and communicative nature. Come and show your financial prowess and operational flair at Crossref. It'll be a challenge. But it'll be fun!

## About the role

Reporting to the Executive Director, the Finance and Operations Director is a key member of Crossref's leadership team and has strategic and managerial responsibility for all aspects of Crossref's finances, HR, legal, and governance. You will work closely with the Executive Director, the senior leadership team, the Finance and Operations Team and the board to instil a culture of transparency, collaboration, and dynamic leadership across the organization - and to ensure that all activities support Crossref's mission and strategic priorities and foster continuous improvement and innovation while mitigating operational risks.

As part of the leadership team at Crossref you will contribute to our strategic planning and help define and develop our organization's culture. This role is responsible for planning, preparing, monitoring, reporting, and analyzing all aspects of the organization's finance functions, providing guidance and support with financial reporting, budgets, forecasts, investment portfolios, payroll, and benefits. The role also manages all commercial insurance policies (general liability, Directors and Officers, cybersecurity, etc.). You will provide key metrics, insights, and recommendations on all financial, HR, legal, and governance matters to the senior team, Executive Director, Treasurer, and board.


## Key responsibilities

### Financial planning and Leadership

* Provide leadership in the development and continuous evaluation of short and long-term strategic financial objectives;
* Provide timely and accurate analysis of budgets, financial reports and financial trends in order to assist the Executive Director, the board and other staff directors in performing their responsibilities;
* Evaluate and advise on the financial impact of long range planning, new partnerships/alliances, and the introduction of new programs/strategies;
* Establish and maintain strong relationships with staff directors to identify their needs;
* Provide strategic financial input and leadership on decision making issues affecting the organization; i.e., evaluation of potential alliances, new services, and investments;
* Work with external audit firm overseeing financial statements and internal control audits;
* Maintain positive and effective banking relationships, and oversee all treasury related functions;
* Manage the relationship with external counsel and oversee work on contracts, legal compliance, and governance matters.

### Organizational management and leadership

* Provide clear, accurate reporting and forecasting of Crossref’s performance in the context of its strategic direction and long term mission.
* Attend and present at board meetings and support the Executive Director in developing effective relationships with board members; provide support to the Treasurer;
* Provide financial analyses and reports for Executive Director, Treasurer, and board and adhere to all regulatory reporting requirements and deadlines;
* Manage a team of [six staff](/people/org-chart/) including the  Controller (Head of Finance), Head of Accounting, and HR Manager;
* Lead the finance/accounting team to ensure timely and accurate reporting, forecasting, budgeting, risk management, tax returns, government forms, and financial audits;
* Work with insurance company to ensure we are compliant;
* Oversee and provide data for the efficient operation of financial systems and internal financial controls. You will ensure compliance with all relevant regulations such as GAAP, 501c (6) organizations, IRS, and local and state reporting requirements;
* Work with the Directors to translate strategy and communicate financial information and develop long term growth plans and financial projections;
Serve as staff representative on the board’s audit and any ad hoc finance committees, contributing and providing reports as needed;
* Provide leadership of the HR functions, managing benefits and compensation and all regulatory compliance, oversee the payroll process, administration of the 401K plan and overseas plans;
* Serve as Secretary of the Corporation (as defined in [the Bylaws](/board-and-governance/bylaws/));
* Oversee the annual board of directors election.

## About you

* 15+ years in progressively responsible financial or operational leadership roles with proven leadership in nonprofit finance, administration and operations;
* A strategic thinker with a rich understanding of how finances affect the needs and goals of a mission driven nonprofit organization and how to manage costs while driving revenue growth;
* A strong leader with the ability to coach, mentor and develop staff into a well- functioning team, assessing strengths and weaknesses that will help you lead and guide team;
* Experience working with an international organization and dealing with financial issues across multiple countries
* A strong team player with a commitment to creating a positive and engaging work environment;
* Demonstrated ability to balance financial goals against organizational mission;
* Knowledge of nonprofit financial environment, policies and procedures;
* Knowledge of US GAAP and accounting theories and practices;
* Experience working with an international organization and dealing with financial
issues across multiple countries
* Knowledge of databases and accounting systems with strong general ledger,
AP, AR, payroll, income tax and working knowledge of banking and investment;
* A self-directed leader that is a good manager of your own time with the ability
to focus despite competing demands on your time;
* Strong written and verbal communication skills, able to communicate clearly,
simply, and effectively;
* Outstanding interpersonal relations and relationship management, and
comfortable working with other teams;
* Travel required to board meetings and Director face-to-face meetings.

## Process & timeline

Prospective candidates interested in applying should contact our search partners Perrett Laver. For an informal discussion about the role, contact [Daniel Flynn](mailto:Daniel.Flynn@perrettlaver.com) to hear more or address any questions you may have by August 9th, 2019.

Completed applications comprised of CV and cover letter can be uploaded at [http://www.perrettlaver.com/candidates](http://www.perrettlaver.com/candidates) quoting reference number 4216 or sent to Daniel Flynn directly at [Daniel.Flynn@perrettlaver.com](mailto:Daniel.Flynn@perrettlaver.com).
The deadline for applications will be Friday, August 9.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.  

Since January 2000 we have grown from strength to strength and now have over 12,000 members across 120 countries, and thousands of tools and services relying on our metadata.  

We can offer the successful candidate a challenging and fun environment to work in. We’re fewer than 40 professionals but together we are dedicated to our global mission. We are constantly adapting to ensure we get there, and we don’t tend to take ourselves too seriously along the way.   


---