+++
title = "Head of Community Engagement and Communications"
author = "Ginny Hendricks"
date = 2021-12-08
draft = false
image = "/images/banner-images/forum-starlings.jpg"
weight = 1
parent = "Jobs"
x-version = "0.0.0"

+++

{{% divwrap red-highlight %}} Applications for this position closed 2022-01-20. {{% /divwrap %}}  

## Join us as our brand new **Head of Community Engagement and Communications** and help advance open research

Crossref is a not-for-profit membership organization that exists to make scholarly communications better by providing metadata and services that make research objects easy to find, cite, link, assess, and reuse. We’re passionate about providing open foundational infrastructure for the scholarly communications ecosystem - and we’re continuously evolving our tools and services in response to emerging needs.

Crossref is at its core a community organization. We’re committed to lowering barriers for global participation in the research enterprise, we’re funded by members and subscribers, and we engage regularly with them in multiple ways from webinars to working groups. The Head of Community Engagement and Communications (CEC) is an exciting and newly-created leadership position, which reflects an evolution of our former Marketing and Communications (‘marcomms’) approach to be more in line with our community-focused mission. We are looking for someone who understands that community-led not-for-profit organizations need a different approach — co-creating, listening, and facilitating conversations — rather than simply broadcasting.

With 16,000 members across 146 countries (and counting), this is a role where you’ll be driving communications strategy and building relationships from day one. You’ll combine big picture thinking about our engagement strategy with attention to detail in coordinating community programming, content creation, and communications. And you’ll ensure that we continue to work responsively in a manner that supports diverse, global participation.

As scientific community engagement is an emerging profession, practical experience in this area is more important to us than traditional qualifications. The successful person may in fact have a technical background in scholarly communications but can show strong community management and communications skills. We prefer candidates who can show some familiarity or affinity with the work of the [CSCCE](https://www.cscce.org/) and knowledge of the dynamics of scholarly communications.

## Key responsibilities

This is a senior role and it's crucial to Crossref's mission and future ability to deliver on our [strategy](/strategy). It provides the exciting opportunity to:

* Lead a **team** of two community engagement managers and one communications and events manager, and guide their work with publishers, funders, infrastructure organizations, and research institutions.
* Work with internal and external data sources and partners to build out our country-level **community engagement strategy** and extend activities that lower barriers to participation in Crossref, measuring successful outcomes such as new members joining (new constituencies, new countries).
* Oversee our **Sponsors** program, which is the primary route to membership for emerging countries, and our **Ambassadors** program. You’ll be building relationships with key Sponsors and Ambassadors around the world, and ensuring we are working optimally together for our members’ benefit.
* Create and oversee **product communications** plans including messaging, roll-out schedules, and adoption campaigns, keeping in regular touch with product/outreach colleagues to plan for key developments and articulate how they will be of use and to which sectors of our community.
* Be or become an **advocate for community engagement** as a core set of skills in the work of open scholarly infrastructure organizations. Facilitate and participate in committees and advisory groups led by other initiatives, and bring insights back to share with colleagues to inform and adapt our own priorities and organizational strategy.
* Refresh our **content** strategy using your knowledge of the changing dynamics in research communications, and an understanding of our recent repositioning to work toward the Principles of Open Scholarly Infrastructure. You’ll lead the coordination of content for all our activities - including identifying opportunities to create together with the community and to curate and archive essential resources so that they remain discoverable and reusable long after their initial publication.
* Establish a program management approach to all interactions such as advisory groups, webinars, newsletters, social, blog, website, documentation, discussion forums, and conference participation, including developing our multilingual and multi-time-zone **programming**. Oversee the calendar of activities and plan ahead for key events such as our annual board election, annual report, conferences, and other community meetings.
* Data-driven, you’ll interrogate **analytics** (website, email, CRM, etc.) to understand and optimize the reach and effectiveness of engagement programs.
* Develop a library of **creative resources** and refine and add materials such as animations, diagrams, and slide libraries. Package them for different purposes such as onboarding, or different constituencies such as research funders, or different use cases such as API querying. Involve the community in **co-creating** such resources where possible, being mindful of serving an international audience.
* Some **international travel** will likely be appropriate when it’s safe to do so, for example to in-person meetings with colleagues, members, and sponsors.

> We’re looking for the right candidate—who can be based anywhere—while being aware that many of our engagement activities include the Asia Pacific timezones.

## Your skills and experience

### Interpersonal

* Ability to build trusted relationships with colleagues and community members---even remotely---including brokering outcomes where everyone gets what they need
* A truly global and inclusive perspective; we have 16,000 member organizations from 146 countries across all time zones and hundreds of languages
* A desire to bring people together, listening for emergent needs, and responsively providing training and other solutions  
* A coaching style of leadership that is empathetic and unconcerned with hierarchy

### Technical

* Technical resourcefulness to get hands-on with our websites (built on Hugo, using markdown and Git, with Matomo for analytics) and our systems, managing our email and content platforms and their integrations
* A good understanding of APIs and metadata, unfazed by technical jargon or digging into metadata for reporting
* A strong creative edge and an appreciation for minimalist design with some experience of media production

### Program management

* Strategic thinking while loving the detail; ability to get from an ambiguous idea to achievable chunks of work
* Equally comfortable facilitating and chairing meetings and working groups as well as coaching other staff in leading their own
* Hands-on with event management and hosting from logistics to reporting back and follow-up

### Program development

* Demonstrable experience managing communities, including developing and guiding programming activities
* Formalizing collaborations as long-term partnerships including drafting MOUs, and overseeing relationships with and management of our Sponsors
* Knowledge of current priority topics within scholarly communications and publishing
* Understanding of mission-driven or community-led organizations and their sustainability and governance challenges

### Communication

* Concise writing and editing skills  
* Experience with content planning, creation, and curation including archiving
* Experience with value message creation and product roll-out/adoption plans
* Always on the look-out for opportunities to present at external events, ability to create consistent stories that align with our strategy, and being comfortable speaking and presenting yourself
* Demonstrated experience with digital communications strategy including social media and community forums
* Languages other than English would be a plus

## What it’s like working at Crossref

We’re about [40 staff](/people) and now ‘remote-first’ although we have optional offices in Oxford, UK, and Boston, USA. We are dedicated to an open and fair research ecosystem and that’s reflected in our ethos and staff culture. We like to work hard but we have fun too! We take a creative, iterative approach to our projects, and believe that all team members can enrich the culture and performance of our whole organization. This means that while this is a senior role, like all roles at Crossref, it is also a hands-on one. Check out the [organization chart](/people/org-chart).

We are active supporters of ongoing professional development opportunities and promote self-learning at every opportunity. Crossref has a healthy financial situation and we only continue to grow. While we won’t have a clear hierarchical path for staff to follow, there are always evolving opportunities to progress and be challenged.

##  Thinking of applying?

We especially encourage applications from people with backgrounds historically under-represented in research and scholarly communications.

To apply, please send a CV along with a covering letter to Ginny Hendricks at [jobs@crossref.org](mailto:jobs@crossref.org).

Please strive to get applications in by the first week of January 2022.
_[EDIT 13th Jan: We are now keeping the role open until Thursday 20th January]_

The role will report to Ginny, Director of Member & Community Outreach at Crossref, and she will review all applications along with Lindsay Russell, our HR Manager, and Lou Woodley, Executive Director of CSCCE. Candidates who have been shortlisted will be invited to a first interview which will include some exercises you’ll have a chance to prepare for. And then there will be follow-up meetings for the final candidates to meet the team - Rosa, Susan, and Vanessa. We aim to make an offer before the end of January.

##  Equal opportunities commitment

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, colour, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law.  Crossref will make reasonable accommodations for qualified individuals with known disabilities in accordance with applicable law.

**Thanks for your interest in joining Crossref. We are excited to hear from you!**