+++
title = "FAQs"
date = "2017-12-20"
draft = false
type = "Help"
author = "Patricia Feeney"
rank = 4
parent = "Get help"
weight = 2
x-version = "0.0.0"

+++

Here are answers to some general questions. If you don't find answers here, please review our [detailed documentation](/documentation/), or [contact us](/contact/) for help.

{{% row %}}
{{% column %}}
## General help
* [Am I eligible for membership?](#q1general)
* [How do I get a DOI for my paper?](#q2general)
* [When (and how) do I pay for my DOIs?](#q3general)
* [How do I find the DOI for a particular article?](#q4general)
* [How do I handle title transfers?](#q5general)
* [I've found a problem with your metadata - how do I get you to fix it?](#q6general)
* [How do I update my contact information?](#q7general)

## Content Registration
* [How do I register my content?](/documentation/member-setup/choose-content-registration-method/)
* [What is my DOI suffix](/documentation/member-setup/constructing-your-dois/) / [is my suffix OK?](/documentation/member-setup/constructing-your-dois#00007)
* [What types of content can I register?](/documentation/member-setup/choose-content-registration-method#00000)
* [I messed up - how do I correct my metadata record?](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/)
* [What does this error message mean?](/documentation/content-registration/verify-your-registration/troubleshooting-submissions/)
* [I registered a DOI but it is not working - what do I do?](/documentation/content-registration/verify-your-registration/)

## Updating and maintaining metadata records
* [My content has moved, how do I update my resource resolution URLs?](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00171)
* [How do I tell you about my title change?](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/#00167)
* [Do I need to create a new DOI if I’ve missed something or provided incorrect metadata?](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/)
* [Can I delete records and/or DOIs?](/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois/)

## Reports
* [How do I access reports](/documentation/reports/)?
* [I hate reports, can you stop emailing them to me](/documentation/reports#00612)?
* [What does this email mean?](/documentation/reports#00612)
* I have questions about my [DOI error report](/documentation/reports/doi-error-report/), [resolution report](/documentation/reports/resolution-report/), [conflict report](/documentation/reports/conflict-report/),
[Schematron report](/documentation/reports/schematron-report/), or [depositor report](/documentation/reports/depositor-report/)
{{% /column %}}
{{% column %}}

## Crossref services
* [I have questions about Crossmark](/documentation/crossmark/)
* [I have questions about Similarity Check](/documentation/similarity-check/)
* [I have questions about Cited-by](/documentation/cited-by/)

## Multiple resolution
* [What is multiple resolution](/documentation/content-registration/creating-and-managing-dois/multiple-resolution/)?
* [How do I update my multiple resolution URLs?](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00128)
* [I want my links to go to one place - how do I turn off multiple resolution?](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00129)
* [How does multiple resolution affect my resolution statistics?](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00117)
* [What if I want to do multiple resolution but sometimes want to direct people to a single URL?](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00135)
* [What if I want to use different URLs based on where the user is coming from - do you support country codes?](/documentation/content-registration/creating-and-managing-dois/multiple-resolution#00130)

## Co-access
* [What is Co-access?](co-access#q1)
* [What problem does Co-access solve?](co-access#q2)
* [Who is Co-access for and how does it work?](co-access#q3)
* [How much does Co-access cost?](co-access#q4)
* [How do I participate in Co-access?](co-access#q5)
* [What is the difference between Multiple Resolution and Co-access?](co-access#q6)
* [Can Co-access be used for journal content DOIs too?](co-access#q7)
* [Doesn’t Co-access violate the “uniqueness” rule?](co-access#q8)
* [What about citation splitting?](co-access#q9)
* [How does Co-access affect resolution reports?](co-access#10)
* [How are Co-access relationships represented in Crossref metadata?](co-access#q11)
* [How are Co-Access groups defined](co-access#12)
* [What if a deposit is made by a party not included in a Co-access agreement?](co-access#q13)
* [I’m a book publisher, how do I know which aggregators have registered me for Co-access?](co-access#q14)
* [How are Co-Access groups defined?](co-access#q15)
* [Can I opt-out a single title in a Co-access group?](co-access#q16)
* [Is Co-access a long term solution?](co-access#q17)

{{% /column %}}
{{% /row %}}

---

## General help
<a name="q1general"></a>
### Am I eligible for membership?
If you publish scholarly content online or represent organizations who publish, you are eligible to [become a member](/membership/). You also must be able to commit to our [member terms](/membership/terms/).
<a name="q2general"></a>

### How do I get a DOI for my paper?
We don't supply DOIs ad-hoc.  If the publisher of your paper is a member, they'll register your article on your behalf.
<a name="q3general"></a>

### When (and how) do I pay for my DOIs?
Your Content Registration fees will be invoiced quarterly. Invoices can be paid through our [payment portal](https://portal.tpro3.com/CrossRef/Customer_Portal). In addition to credit card payments we also accept wires and checks. Questions about logging in or billing in general can be emailed to our [finance team](mailto:billing@crossref.org).
<a name="q4general"></a>

### How do I find a DOI for a particular article?
To look up a single DOI use our [Metadata Search](https://search.crossref.org) interface. If you want to look up metadata records or DOIs in volume, read more about [metadata retrieval](/services/metadata-retrieval/).
<a name="q5general"></a>

### How do I handle title transfers?
If you've acquired a title from another member, you need to let us know about the transfer and provide confirmation from the disposing publisher.  We'll accept transfers posted to the [Enhanced Transfer Alerting Service (ETAS)](https://journaltransfer.issn.org/). If you don't participate in Transfer, your confirmation may be a forwarded email from the disposing publisher to the acquiring publisher acknowledging the transfer.  See our [title and record ownership transfer documentation](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois) for more details.

<a name="q6general"></a>
### I've found a problem within your metadata - how do I get you to fix it?
While we aren't able to correct the metadata provided by our members, report any metadata issues to our [support staff](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) and we'll contact the responsible member and ask them to make corrections.

<a name="q7general"></a>
### How do I update my contact information?
Please contact our [membership specialist](mailto:member@crossref.org) with any changes to your contact information.