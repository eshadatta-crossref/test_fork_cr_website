+++
title = "Help with Co-access"
date = "2017-12-20"
draft = false
author = "Sara Bowman"
rank = 3
parent = "FAQs"
weight = 100
x-version = "0.0.0"

+++

* [What is Co-access?](#q1)
* [What problem does Co-access solve?](#q2)
* [Who is Co-access for and how does it work?](#q3)
* [How much does Co-access cost?](#q4)
* [How do I participate in Co-access?](#q5)
* [What is the difference between Multiple Resolution and Co-access?](#q6)
* [Can Co-access be used for journal content DOIs too?](#q7)
* [Doesn’t Co-access violate the “uniqueness” rule?](#q8)
* [What about citation splitting?](#q9)
* [How does Co-access affect resolution reports?](#q10)
* [How are Co-access relationships represented in Crossref metadata?](#q11)
* [How are Co-Access groups defined](#q12)
* [What if a deposit is made by a party not included in a Co-access agreement?](#q13)
* [I’m a book publisher, how do I know which aggregators have registered me for Co-access?](#q14)
* [How are Co-Access groups defined?](#q15)
* [Can I opt-out a single title in a Co-access group?](#q16)
* [Is Co-access a long term solution?](#q16)

<a name="q1"></a>
## What is Co-access?
Co-access allows multiple Crossref members to register content and create DOIs for the same book content; both whole titles or individual chapters. This means that there can be multiple DOIs registered for the same book content. Prefixes must be pre-registered for Co-access in order to participate. Co-access does not require a primary depositor. For Co-access registered members, we’ll match records with near identical metadata (for example, matching ISBNs and titles) and enter them into a Co-access relationship where matches are found between participating members. This means that Co-access enables better linking between different Crossref DOIs and gives book publishers and aggregators of book content greater flexibility to host content within a timeframe and in a location which suits them best.

<a name="q2"></a>
## What problem does Co-access solve?
Many organizations can be involved in the hosting and distribution of books. Up until now, Crossref’s Multiple Resolution (MR) functionality has worked well with journals, where content may exist on the publisher’s site and also on a third-party hosting platform. In this case the content on the publisher site gets registered first and then the URLs for the content on the third-party sites can be added after this. Both the publisher and the third-party sites can use the same DOI.  Unlike with  journal content, book publishers often outsource their content hosting to multiple aggregators/platforms with none of them considered the primary site. Additionally in some cases, the book publisher does not deposit any metadata and DOIs with Crossref at all. Multiple Resolution requires coordination between the primary publisher and the secondary content hosts which is too burdensome to be feasible between book aggregators - especially as there is often no primary depositor (i.e the book publisher). Instead, publishers leave depositing to secondary content hosts. This means that MR functionality is unsuitable for this use case, as book aggregators have expressed the need for a process that allows independent transactions on the part of any secondary content hosts.

<a name="q3"></a>
## Who is Co-access for and how does it work?
Co-access is for any Crossref publisher or sponsoring publisher member who faces the challenge of assigning DOI identifiers to book content that is distributed across a number of different platforms. Crossref members who aggregate book content on behalf of other publishers can contact Crossref to request that their prefix be added to the Co-access group for each of the book publisher members they work with.

For items from approved prefixes, Crossref's system will automatically look to establish a Co-access match with any other DOI where near-identical bibliographic metadata is found. When matches are found, an interim page hosted by Crossref will be displayed to end users when any matched DOI URL is followed. Users can then interact with the Co-Access interim page to choose their preferred DOI to follow in order to access the content.

<a name="q4"></a>
## How much does Co-access cost?
Each depositor is billed for their own item registered. There are no additional fees to participate in Co-access.

<a name="q5"></a>
## How do I participate in Co-access?
You must first be registered for Co-access in order to participate. Crossref members who host/aggregate book content on behalf of other Crossref book publisher members, can register their prefix, and those of the book publishers they work with, by sending the details of your Co-access relationship to  [support@crossref.org](mailto:support@crossref.org).

Before contacting our support team, aggregators should please ensure they have spoken with each book publisher they work with about Co-access deposits and have collected the following details:

* Aggregator's own Crossref membership information (i.e depositor prefix/s)

* The name, prefix/s and contact details of each Crossref book publisher member you work with

* Aggregator's company logo file

* The logo file for each publisher

<a name="q6"></a>
## What is the difference between Multiple Resolution and Co-access?
[Multiple Resolution](/get-started/multiple-resolution/) is when a single DOI has multiple resolution URLs; for example, if the same content is available in different locations. When the DOI is resolved it goes to an interim landing page that shows the different resolution URLs. Co-access is for book content and is when multiple DOIs are assigned to the same book title or chapter; each DOI is deposited by a different member and each has only one resolution URL. The DOIs are grouped together and when any of the DOIs is resolved they go to the an interim landing page showing all the DOIs and the resolution URLs.

<a name="q7"></a>
## Can Co-access be used for journal content DOIs too?
No, Co-access is only for book content. Journal publishers do not encounter the same distributed content issues that book publishers and their aggregators face. In the rare cases where journal content is distributed, members can use our existing Multiple Resolution functionality in order to add multiple hosting locations within a single Crossref deposit.

<a name="q8"></a>
## Doesn’t Co-access violate the “uniqueness” rule?
Yes it does. While it is Crossref policy that only one DOI be created for a given work because Crossref DOIs are citation identifiers, we are enabling multiple DOIs to be assigned for the same book content in order to solve a very specific use case identified by our book publisher members and their aggregators. Crossref offers Co-access exclusively to book publishers to address the specific structural needs inherent to their production and distribution environment. See the response above in “What problem does Co-access solve?” and below in “What about citation splitting?”.

<a name="q9"></a>
## What about citation splitting?
The Crossref DOI is a citation identifier. This means that we identify content to enable accurate citation for scholarly content. This is different from other identifiers, like the ISBN, which are used to identify all the different formats - hardback, paperback, ePub. Therefore, a basic Crossref principle is that content, even if it’s available in different formats, should only have one Crossref DOI. For content that is part of Co-access, there will be multiple DOIs for the same content and this could mean that where systems and services use the DOI to track citations that all the citations will not be captured since they are spread across multiple DOIs. In addition, a service like Crossref Event Data (which collects post publication events), having multiple DOIs for the same content makes it harder to track activity. Crossref DOIs are increasingly being used to track activity. So Co-access involves a tradeoff between increased flexibility against accurate citation and event counting.

<a name="q10"></a>
## How does Co-access affect resolution reports?
The DOI that was clicked on initially, before the landing page is enabled, is the DOI resolution that is captured in our reports. If a user selects an alternative DOI from the options presented on the Co-access landing page, we do not report this as a resolution.

<a name="q11"></a>
## How are Co-access relationships represented in Crossref metadata?
To inspect the output metadata to see what DOIs are related through Co-access, see our <a href="https://support.crossref.org/hc/en-us/articles/115003688983" target="_blank">technical documentation</a> for details.

<a name="q12"></a>
## How are Co-Access groups defined?
Co-Access is enabled between prefixes. Prefixes registered for Co-access will be grouped to allow each member of the group access to a given book content item. This allows each member to deposit (and update) their own DOI for that title and for the content items (chapters) within that title. Once a Co-access group is defined any member could create a DOI for any title owned by any member of the Co-Access group.

<a name="q13"></a>
## What if a deposit is made by a party not included in a Co-access agreement?
If the depositor’s prefix is not in a registered Co-access group, then the conflicting deposit will be rejected. Multiple registered items for the same book title is only possible if prefixes have been pre-registered for Co-access.

<a name="q14"></a>
## I’m a book publisher, how do I know which aggregators have registered me for Co-access?
Members can email [support@crossref.org](mailto:support@crossref.org) to obtain a list of all prefixes they are in Co-access with.

<a name="q15"></a>
## How are Co-Access groups defined?
Co-Access is enabled between prefixes. Prefixes registered for Co-access will be grouped to allow each member of the group access to a given book content item. This allows each member to deposit (and update) their own DOI for that title and for the content items (chapters) within that title.

<a name="q16"></a>
## Can I opt-out a single title in a Co-access group?
Ideally, single title opt-out should occur before any Co-access deposits are made for that item. To do this, the title owner should contact us at [support@crossref.org](mailto:support@crossref.org) to request we set a Co-access exclusion flag on their title. If a title owner want to undo Co-access deposits that have already been made and matched, then they should our support team to ask that the title be excluded from Co-access and that any unwanted DOIs be aliased.

<a name="q17"></a>
## Is Co-access a long term solution?
No, Co-access is an interim solution to solve the current issue faced by our book publisher members and their content aggregators. In the long run, we will be exploring more effective solutions to improve how Crossref accommodates the unique challenges of the book publishing environment.


---
**More questions?** Review [other Crossref FAQs](/faqs) or visit our [support site](https://support.crossref.org/hc/en-us/articles/115003688983-Co-access-) to open a support ticket or review detailed technical documentation.