---
archives:
- 2018
author: Kirsty Meddings
authors:
- Kirsty Meddings
- Anna Tolwinska
categories:
- Member Briefing
- Participation
- Metadata
- Content Registration
- Research Nexus
date: 2018-04-26
draft: false
title: How good is your metadata?
x-version: 0.0.0
---

Exciting news! We are getting very close to the beta release of a new tool to publicly show metadata coverage. As members register their content with us they also add additional information which gives context for other members and for services that help e.g. discovery or analytics.

Richer metadata makes content useful. Participation reports will give---for the first time---a clear picture for anyone to see the metadata Crossref has. This is data that's long been available via our Public REST API, now visualized.

<!--more-->

### Who are participation reports for? Everyone!

It's an opportunity to evaluate and educate. See for yourself where the gaps are, and what our members could improve upon. Understand best practice through seeing what others are doing, and learn how to level-up.

Monitor what metadata is being registered, even if this work is done by a third party or another department. And see what other organizations in scholarly communications see when they use Crossref metadata in their research, tools, and services.

The beta release—expected after acceptance testing some time late May—will let anyone look up any of our 15,000+ members and see whether they are registering ten key elements that add context and richness to the basic required bibliographic metadata.

### What do we mean by ‘richer metadata’?

The ten checks for Beta, will be:

{{% imagewrap right %}} <img src="/images/blog/checklist.png" alt=“checklist" height="250px" width="200px" class="img-responsive" /> {{% /imagewrap %}}

* References
* ~Open references~ _[EDIT 6th June 2022 - all references are now open by default]._
* ORCID iDs
* Funder IDs
* Funding award numbers
* Crossmark metadata
* License information
* Full text links
* Similarity Check URLs
* Abstracts

Each of these additional metadata elements helps increase discovery and wider and more varied use---and usefulness---of research outputs.

### Why are we doing this and what do we mean by ‘participation’?

Over the years when we’ve talked with our members about their metadata, we learned that many just can’t be certain exactly how they’re performing. It could be that they’ve outsourced [Content Registration](/services/content-registration) to another service provider or larger publisher, or it could be they just weren’t previously aware they could collect and share authors’ ORCID iDs, Funder IDs, and so on. So our primary aim is to give our members the information they need in order to make a case for improving their metadata records. Each check will come with information about why it is important and guidance on how to improve. Additionally, with the growing use of Crossref as a central source of metadata for the research community, it’s in everyone’s interest to be as transparent as possible about what metadata we have - and encourage greater understanding of what’s possible.

Member ‘participation’ is an important concept. Crossref [distinguishes itself from other DOI registration agencies](/membership/benefits) by providing this richer infrastructure which allows for things like funding information, license information, links between data and preprints, and so on—all  contributing to the [research nexus](/blog/the-research-nexus-better-research-through-better-metadata/) for everyone’s benefit.

Membership of Crossref is not just about getting a persistent identifier for your content, it’s about placing your content in context by providing as much metadata as possible and looking after it long-term.

Here’s a sneak preview of what the report will look like:

<img src="/images/blog/springer-nature-prep.jpg" alt="Crossref participation report - Springer Nature" width="100%" />

So whether you’re a member who wants to run a “health check” on your own metadata, or a consumer of metadata interested in what’s available and from whom, watch this space for Participation Reports!


### Would you like a heads-up on your report, pre-beta?

Beta will be released some time in May or June this year, following acceptance testing with members and others. Then we’re looking for about 20 members to have a half-hour phone call with a walk-through ‘health check’. Please [contact Anna if you’d like to schedule one](mailto:annat@crossref.org).