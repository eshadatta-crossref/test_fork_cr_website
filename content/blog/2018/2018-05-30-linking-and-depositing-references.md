---
archives:
- 2018
author: Anna Tolwinska
authors:
- Anna Tolwinska
categories:
- References
- Reference Linking
- Cited-by
- Citation
- Linking
date: 2018-05-30
draft: false
title: Linking references is different from depositing references
x-version: 0.0.0
---

From time to time we get questions from members asking what the difference is between reference linking and depositing references as part the Content Registration process.

Here's the distinction:

> Linking out to other articles from your reference lists is a key part of being a Crossref members - it's an obligation in the membership agreement and it levels the playing field when all members link their references to one another. Depositing references when you register your content is completely different. It's enriching the metadata record that describes your content, and it allows Crossref and others---including non-members---to use them.

### Reference Linking
A research article usually includes a reference list of citations to other works that helped inform it. The original function of Crossref was to provide a central service for publishers that enabled them to link to each others' content from these reference lists---using a DOI as a persistent link. This meant that members of all sizes and in all disciplines could easily link to one another without having to sign hundreds of bilateral agreements.

We made Reference Linking [obligatory](/membership/terms) for Crossref members because it's fundamental to making content discoverable, and because  when everyone links their references, research travels further and benefits everyone.  

### Depositing references

Every single day hundreds of members register and update their metadata with us---and every single day hundreds of organizations search for, extract and use it. To make sure your content is discovered in this process, it's important to make the metadata you register with us as rich as possible. Rich metadata includes information such as journal title, article author, publication date, page numbers, ISSN, abstracts, ORCID iDs, funding information, clinical trials numbers, license information, and of course---references.

Additionally, registering references is a prerequisite for participating in our Cited-by service---which provides citation counts and lists, and ultimately makes your content more discoverable.

We know it's not easy for smaller publishers to deposit references. Our upcoming Metadata Manager tool will allow you to register your references at the same time as the rest of your content. This service is currently in development but [let us know if you want to try it out](mailto:support@crossref.org).


{{% row %}}
{{% column %}}

### Reference Linking
Reference Linking means adding Crossref DOI links to the reference list for journal articles on your article pages as per this example: https://doi.org/10.1088/1367-2630/1/1/006.

#### How it works
First retrieve DOIs for all available references either through our [human](https://search.crossref.org) or [machine](https://api.crossref.org) interfaces. Then make sure you use the DOI link in your references and on your article landing page using the [Crossref DOI display guidelines](https://doi.org/10.13003/5jchdy).

#### Why it’s useful

Reference Linking:

- Enables you to link to more than 10,000 publishers without having to sign multiple agreements
- Helps with discoverability, because DOIs don’t break if implemented correctly
- Displays your DOIs as URLs so that anyone can copy and share them
- Makes your content more useful to readers
- Drives traffic to your website from other publishers.

#### Is it obligatory?
Yes, within a short time after becoming a member and only for the journal content type. It’s encouraged for other content types.

{{% /column %}}

{{% column %}}


### Registering References

Registering references means submitting them as part of your Crossref metadata deposit as per this example:
https://www.crossref.org/xml-samples/article_with_references.xml.

#### How it works

Whenever you register content with us, make sure you include your references in the submission. You can also add references to your existing content via a [metadata redeposit](https://support.crossref.org/hc/en-us/articles/213022486-Updating-your-metadata), or our [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/), or our [Simple Text Query form](https://support.crossref.org/hc/en-us/articles/214236226).


#### Why it’s useful

References registered as part of your metadata:

- Make your content more discoverable
- Make your content richer and more useful
- Are required to participate in our Cited-by service (this service shows what articles cite your article)
- Enables discovery of research
- Enables evaluation of research
- Highlights your contents’ provenance
- Helps with citation counts.

#### Is it obligatory?

No, it’s optional, but strongly encouraged. It is required however if you are participating in our Cited-by service.

{{% /column %}}
{{% /row %}}


---
If you have any questions about reference linking or registering your references please [get in touch in ](mailto:support@crossref.org).