---
archives:
- 2018
author: Ginny Hendricks
authors:
- Ginny Hendricks
categories:
- Community
- Crossref LIVE
- Annual Meeting
date: 2018-11-02
draft: false
title: Ten more days 'til Toronto
x-version: 0.0.0
---

Our LIVE Annual Meeting is back in North America for the first time since 2015, and with just 10 days to go, there’s a lot going on in preparation. As you’d expect with a `How good is your metadata?` theme---the two-days will be entirely devoted to the subject of metadata---because it touches everything we do, and everything that publishers, hosting platforms, funders, researchers, and librarians do. Oh, and it's actually super awesome too---and occasionally fun.

<!--more-->

Metadata is what is used to describe the story of research: its origin, its contributors, its attention, and its relationships with other objects. The more machines start to do what humans cannot---parse millions of files through multiple views---the more we see what connections are missing, and the more we start to understand the opportunities that better metadata could offer.

We love metadata so much that we're producing an 8-foot-high depiction of the 'perfect' record, in both XML and JSON, for people to gape at and annotate in person. Sneak preview:

{{< figure src="/images/blog/perfect-record.png" caption="The perfect metadata record is eight feet tall." width="500" attr="SchemaSchemer" attrlink="/people/patricia-feeney" >}}

Both days feature plenary-style talks, insights from ourselves and guests who will regale us with tales of metadata woes and wonders.

[Lisa](/people/lisa-hart-martin) will be there at the end of Day 1 to update everyone on some recent and potential governance changes, and---the reason we started these gatherings---to reveal the results of our [2018 board election](/board-and-governance/elections/2018-slate), the second contested election we've held, and already with twice the voters from 2017.

Our amazing guest speakers are too brilliant and too experienced to highlight in just one blog. But check out the [LIVE18 schedule](/crossref-live-annual) to see what they'll be talking about:

{{% row %}}
{{% column %}}

- **Patricia Cruse**, DataCite
- **Ravit David**, University of Toronto
- **Clare Dean**, Metadata 2020
- **Paul Dlug**, American Physical Society
- **Kristen Fisher Ratan**, CoKo Foundation

{{% /column %}}
{{% column %}}

- **Stefanie Haustein**, University of Ottawa
- **Bianca Kramer**, Utrecht University
- **Graham Nott**, Freelance developer (eLife/JATS)
- **Jodi Schneider**, University of Urbana-Champaign
- **Shelley Stall**, American Geophysical Union

{{% /column %}}
{{% /row %}}

We’ll be taking over the entire second floor of the Toronto Reference Library, whose three rooms will house a bunch of conversational sessions as well as some more formal talks:

- `Rally` is the main room where we’ll have the plenary-style talks, a corner for `Unscheduled Maintenance` offering live support for your questions about billing or tech for [Ryan](/people/ryan-mcfall), [Shayn](/people/shayn-smulyan), [Isaac](/people/isaac-farley), [Jason](/people/jason-hanna), [Chuck](/people/chuck-koscher), & [Mike](/people/mike-yalter). Running down the whole left side of this room is also the `You-are-Crossref` wall where the community will showcase their work with metadata through posters - feel free to bring one along and find [Patricia](/people/patricia-feeney) to get the sticky tack.

- The `LIVE Lounge` is where you can eat, drink, rest, and chat and where you'll likely find [Rosa](/people/rosa-clark) as she laises between the caterers, the venue, AV, and all of us. The Lounge is also where we'll gather for much-needed post-election refreshments at the end of Tuesday.

- `The Bigger Ambitions Room` is where a lot of the `Unplugged` sessions will take place. This room will feature three separate stations:

  - Crossref Labs & Product where you can chat with [Geoffrey](/people/geoffrey-bilder), [Esha](/people/esha-datta), [Jennifer L](/people/jennifer-lin), [Patrick](/people/patrick-polischuk/), and [Christine](/people/christine-buske) about your big ideas for us, and what we're working on already.
  - Metadata discussions and annotations of the perfect record (previewed above) with [Patricia](/people/patricia-feeney), together with space to ideate around metadata principles.
  - Uses and users of metadata where [Jennifer K](/people/jennifer-kemp) will help us understand just how far Crossref metadata can reach, and who and what people are doing with it.

We cannot wait to show you what else we have planned :-)

For those of you not able to attend, recordings of the presentations will be made available on the [event page](/crossref-live-annual) directly soon after.

Otherwise - see you there!