---
archives:
- 2018
author: Christine Buske
authors:
- Christine Buske
categories:
- Citation
- Collaboration
- Data
- Event Data
- Identifiers
date: 2018-09-12
draft: false
title: Event Data is production ready
x-version: 0.0.0
---

We’ve been working on [Event Data](/services/event-data) for some time now, and in the spirit of openness, much of that story has already been [shared](/categories/event-data) with the community. In fact, when I recently joined as Crossref’s [Product Manager for Event Data](/blog/hello-meet-event-data-version-1-and-new-product-manager), I jumped onto an already fast moving train—headed for a bright horizon.

<!--more-->

What’s on the horizon? Well, the reality is you never really reach the horizon. Good product development—in my opinion—is like that train. You keep aiming for the horizon and passing all the stations (milestones) along the way, but the horizon keeps moving as you add features, improve the service, and maybe even review where you are headed. However, for Event Data we are pleased to say we have now arrived at a rather important station.

### Technical readiness

Thank you to all the beta testers who have journeyed with us this far—we’ve listened and learned, refined and rebuilt with the help of your feedback. We are now thrilled to say that we are service production ready. We’ve reached the station called ‘technical readiness’, and are eager to see more users board our train!

During this time of building and refining, Event Data has grown to include at least 66,7 million events from sources like (in order of magnitude): Wikipedia, Cambia Lens, Twitter, Datacite, F1000, Newfeeds, Reddit links, Wordpress.com, Crossref, Reddit, Hypothesis, and Stackexchange. Wikipedia alone accounts for 50 million events (and counting).

### What does this mean?

Event Data is production ready.

Being production ready means we are not going to make any breaking changes to the code, and we are excited to see more people [jump on board](https://www.eventdata.crossref.org/guide/) to explore where you can go with Event Data, and what product or service you might want to build with it.

### Getting started

Having a look at Event Data, and using it, is easy. While the [user guide](https://www.eventdata.crossref.org/guide/) outlines everything you need to know to get fully engrossed, you can get your feet wet with a few sample queries:

Above I mentioned Event Data has about 50 million Wikipedia events, you can check if that has grown by looking at a query that lists all distinct events by source (your browser will need a [JSON viewer](https://chrome.google.com/webstore/search/json?hl=en&_category=extensions) extension):

[`https://api.eventdata.crossref.org/v1/events/distinct?facet=source/:*&rows=0`](https://api.eventdata.crossref.org/v1/events/distinct?facet=source%3A*&rows=0)

You can also see a [live stream of events](http://live.eventdata.crossref.org/live.html) going through Event Data.

For all events registered for a specific content item, you simply query  `http://api.eventdata.crossref.org/v1/events?obj-id=https://doi.org/XXX`, where XXX is replaced with the DOI.


### What next?

We are now focusing on the final stretch towards the official roll-out. Beyond this, we will continue to add sources and features and have a healthy roadmap to keep us on track. We value any feedback you have for us about your own journey with Event Data. Your feedback may help shape the direction we take in the future. Most of all, we are all excited to see what people build with it!

We look forward to continuing on our Event Data journey and we welcome you all aboard the train! Please [contact me](mailto:eventdata@crossref.org) with your ideas.

___