---
archives:
- 2018
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
categories:
- Community
- DOI
- Content types
- Sponsorship
- Membership
- Translations
date: 2018-10-05
draft: false
title: A wrap up of the Crossref blog series for SciELO
x-version: 0.0.0
---

Crossref member SciELO (Scientific Electronic Library Online), based in Brazil, celebrated two decades of operation last week with a three-day event [The SciELO 20 Years Conference](https://www.scielo20.org/en/).

<!--more-->

The celebration constituted an important landmark in SciELO’s evolution, and an exceptional moment for them to promote the advancement of an inclusive, global approach to scholarly communication and to the open access movement.  

As part of the anniversary activities SciELO asked us to write a series of five blogs that would help the organizations of Brazil to better understand the following:

 - Why all articles should have a DOI
 - The critical role of the DOI
 - The basics of content types: translations, preprints, Crossmark, and more
 - The basics of Crossref sponsorship, and  
 - How to make the most of your Crossref membership

Below you’ll find an abstract of each of these blog posts as well as a link to the published posts in Brazilian Portuguese, Spanish and English.

**Why all articles should have a DOI**<br>
In today’s world, an author’s work needs a Digital Object Identifier (DOI) for it to become discoverable, citable, and linkable. This unique alphanumeric string identifies the content of a research work, and remains associated with it irrespective of changes to its web location. Discover the origins of the DOI, how Crossref was founded, and why they continue to exist and persist.

Read the full blog in [Brazilian Portuguese](https://blog.scielo.org/blog/2018/07/17/as-razoes-porque-o-crossref-existe-e-persiste/#.W7XScBNKhQI), [Spanish](https://blog.scielo.org/es/2018/07/17/por-que-crossref-existe-y-persiste/#.W7XSYRNKhQI), or [English](https://blog.scielo.org/en/2018/07/17/why-crossref-exists-and-persists/#.W3QO7ZNKg0o)

**The critical role of the DOI**<br>
Find out why URL links to research articles are fragile, and how DOIs are essential in building stable, persistent links between research objects. This is achieved through the metadata that members deposit with Crossref, as part of their obligations. Learn how we can all contribute to creating a global, robust research record.

Read the full blog in [Spanish](https://blog.scielo.org/es/2018/08/02/el-papel-critico-del-doi/#.W7db8hNKhQI) or [English](https://blog.scielo.org/en/2018/08/02/the-critical-role-of-the-doi/#.W7dcARNKhQI)

**The basics of content types: Preprints, Crossmark, translations, and more**<br>
What’s the difference between preprints and ahead of print? When should you use each; and, what are the DOI requirements? This article answers those questions and provides a basic overview of how to connect the metadata records of related content types, like translations.

Read the full blog in [Brazilian Portuguese](https://blog.scielo.org/blog/2018/08/22/os-fundamentos-sobre-os-tipos-de-conteudo-preprints-crossmark-traducoes-e-muito-mais/#.W7dcDhNKhQI), [Spanish](https://blog.scielo.org/es/2018/08/22/conceptos-basicos-de-los-tipos-de-contenido-preprints-crossmark-traducciones-y-mas/), or [English](https://blog.scielo.org/en/2018/08/22/the-basics-of-content-types-preprints-crossmark-translations-and-more/#.W7dcLBNKhQI)


**The basics of Crossref sponsorship**<br>
There are many organizations that want to register content and benefit from the services Crossref provides, but may not be able to do so alone. These organizations use sponsors.  Sponsors are organizations who publish on behalf of groups of smaller organizations. Nearly 650 of our 800 Brazilian members are represented by such a sponsor.   

Read the full blog in [Brazilian Portuguese](https://blog.scielo.org/blog/2018/08/31/os-fundamentos-do-patrocinio-no-crossref/#.W7dcQRNKhQI), [Spanish](https://blog.scielo.org/es/2018/08/31/los-fundamentos-del-patrocinio-en-crossref/), or [English](https://blog.scielo.org/en/2018/08/31/the-basics-of-sponsorship-at-crossref/#.W7dcWhNKhQI)

**How to make the most of your Crossref membership**<br>
Since Crossref was founded in 2000, its member organizations have registered metadata and persistent identifiers (DOIs) for over 100 million content items. This information is used extensively by the research community—individuals and organizations—who need to find, cite, link and assess research outputs. As a SciELO member, the metadata you provide to Crossref when you register content is key to the discoverability of your journal content.

Read the full blog in [Brazilian Portuguese](https://blog.scielo.org/blog/2018/10/03/como-os-periodicos-podem-aproveitar-ao-maximo-sua-associacao-ao-crossref/#.W7dcaBNKhQK), [Spanish](https://blog.scielo.org/es/2018/10/03/como-las-revistas-pueden-aprovechar-al-maximo-la-membresia-de-crossref/#.W7XRsRNKhQI), or [English](https://blog.scielo.org/en/2018/10/03/how-journals-can-make-the-most-of-crossref-membership/#.W7UYkGhKiUk)

___