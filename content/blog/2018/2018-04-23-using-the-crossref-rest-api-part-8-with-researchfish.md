---
archives:
- 2018
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
- Gavin Reddick
categories:
- APIs
- Identifiers
- Interoperability
- API Case Study
date: 2018-04-23
draft: false
title: Using the Crossref REST API. Part 8 (with Researchfish)
x-version: 0.0.0
---

Continuing our [blog series](/categories/api-case-study/) highlighting the uses of Crossref metadata, we talked to Gavin Reddick, Chief Analyst at [Researchfish](https://www.researchfish.net/) about the work they’re doing, and how they’re using our REST API as part of their workflow.

<!--more-->

### Introducing Researchfish

[Researchfish](https://www.researchfish.net/) is the world’s leading platform for the reporting of the outputs, outcomes and impacts of funded research. It is used by over 100 funding organisations in Europe, North America and Australasia and currently tracks around €50 billion of funding, across 125,000 grants. Researchers have reported around 2.5 million attributed outcomes in Researchfish and roughly half of these are publications with the other half being collaborations, further funding, data sets, policy influences, engagement activities etc.

Funders use Researchfish to ask grantees to report on the outcomes of their grant and Researchfish makes it easy for researchers to do this in a structured way. Researchfish seeks to improve the quality and robustness of the evidence base available for evaluation. It works with funders, research organisations and researchers to present, explain and evaluate the impact of research across all disciplines and a wide range of output types.

### How is the Crossref REST API used in Researchfish?

1. 	Search<br>
As publications are a major output of research it is important to make the reporting of those publications be as easy as possible and quality of the information on those publications as high as possible. Researchfish integrates with a number of publication APIs, including Crossref, which enables users to enter a number of DOIs or search by author, title, etc. to find their publication.

2. 	Direct Harvest<br>
Researchfish uses funding acknowledgements in the Crossref metadata to add publications to researchers’ portfolios and report the publications as arising from the grant. If the acknowledgement exists it’s important to use it instead of asking researchers to report the same thing twice.

3. 	Interoperability<br>
Research organisations can upload publications to Researchfish on behalf of researchers, re-using information from their local systems. We use the Crossref REST API to validate the data provided by universities before uploading.

4. 	Metadata Enrichment – Open Access<br>
We use the license and embargo period information in the Crossref metadata to help understand the open access status of publications and whether they meet any policy requirements, without researchers having to take any steps to report in this complex area.

5. 	Metadata Enrichment – Normalisation/deduplication<br>
As Researchfish allows users to add information from lots of different sources it is very important to normalise the data and prevent the same publication being reported multiple times in different ways. We use the Crossref REST API as part of this process.

### What are the future plans for Researchfish?

We are looking to expand the range of integrations to support non-publication outputs and allow some of the same functionality that we have built for publications. We already have integrations to support the reporting of patents, collaborations, further funding and next destinations but are looking to enhance these, along with expanding links to data sets, clinical trials, software and spin out companies.

### What else would Researchfish like to see in Crossref?

Crossref is an excellent resource and most of our wish list would be to see more uptake of existing fields e.g. retractions and the ability to use them more flexibly in the REST API. We would also like to see a little more consistency in some of the metadata – publication type is the area that seems to cause the most confusion, particularly around conference proceedings and clinical trials.

---

Thank you Researchfish! If you would like to contribute a case study on the uses of Crossref Metadata APIs please contact the [Community team](mailto:feedback@crossref.org).