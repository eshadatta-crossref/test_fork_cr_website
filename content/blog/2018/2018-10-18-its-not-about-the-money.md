---
archives:
- 2018
author: Amy Bosworth
authors:
- Amy Bosworth
categories:
- Membership
- Content Registration
- Community
date: 2018-10-18
draft: false
title: It’s not about the money, money, money.
x-version: 0.0.0
---

But actually, sometimes it is about the money. As a not-for-profit membership organization that is obsessed with persistence, we have a duty to remain sustainable and manage our finances in a responsible way. Our annual audit is incredibly thorough, and our outside auditors and Board-based Audit committee consistently report that we’re in good shape.

Our Membership & Fees committee regularly reviews both membership fees and Content Registration fees for a growing range of research outputs. Together with our staff, the Board regularly reviews financial projections that inform our budgeting process and approve our budget each year.

### Financial sustainability means the persistence of our infrastructure and services

We run a tight ship here at Crossref. We have to. So it’s not ideal when we have to chase members and users for late payments, but it’s an important part of keeping the organization afloat, and keeping our dedicated service to scholarly communications running. And that’s my job at Crossref.  

Working here for over six years now, I’ve seen a lot of development in our finance department. We strive as a team to always improve our communication with members and users to deliver the best ‘customer’ experience. To do this, we are always tweaking our processes to improve efficiency and accuracy, and [welcome all feedback](mailto:billing@crossref.org).  

### How the invoice schedule works

Our annual membership invoices are sent out each January, and our Content Registration invoices are generated four times a year, each quarter. All invoices are emailed to the billing contact for your organization (please be sure to update us with any contact changes!) and have a due date of net 45 days. Our invoices now have a “pay now” link in the body of the email. This offers a faster and more convenient way for you to pay, simply by clicking on the link to our payment portal. You can also view invoices as PDFs in the payment portal. An important part of our accounting process is the automated invoice reminder schedule. There are three billing reminders we send by email:

1. The day immediately after the invoice due date;
2. 21 days past the invoice due date; and
3. 45 days past the invoice due date.  

### We don’t want to see you go!

We understand there are many factors that can make prompt payment a challenge for some people: international transfer delays or fees; funding for your publishing operations may end; change of contacts; problems receiving our emails.

When an account is 90 days past due, a further email notifies you that your service is at risk of suspension. If an account is then suspended for non-payment it becomes at risk of being ‘terminated’. Once an account has been terminated, you will need to contact our membership specialist to rejoin Crossref. Please note that we send numerous notifications/reminders before suspension or termination takes place (we don’t want to see you go!). We can always be reached at [billing@crossref.org](mailto:billing@crossref.or) for any invoice inquiries you may have.

### Tips that work for other users

There are some things you can do to speed-up or simplify payments:

- Pay with a credit card, using our online payment portal. This is fast, convenient, and lower in fees
- Always reference an invoice number on the payment to ensure that it’s  applied to your account efficiently
- Be sure to make [`billing@crossref.org`](mailto:billing@crossref.org) a ‘safe’ email address, so that you receive our invoices and reminders
- Always keep us up-to-date with any contact changes at your organization, to ensure that we have accurate information for invoicing and other communication
- We recommend giving us a generic email address for your accounts payable team, such as `accounts@publisher.com` so that if somebody leaves that job, invoices can still get through.

Thanks for working with us! Please let me know in the comments below if you have any feedback or additional tips for your fellow Crossref community members.

---