---
archives:
- 2018
author: Jennifer Kemp
authors:
- Jennifer Kemp
categories:
- Books
- Content Registration
- Metadata
- Content Types
- Best practices
date: 2018-10-16
draft: false
title: Good, better, best. Never let it rest.
x-version: 0.0.0
---

Best practices seem to be having a moment. In the ten years since the [Books Advisory Group](/working-groups/books) first created a [best practice guide for books](/education/content-registration/content-types-intro/books-and-chapters/), the community beyond Crossref has developed or updated at least 17 best practice resources, as [collected here](http://www.metadata2020.org/resources/metadata-best-practices/) by the [Metadata 2020](http://www.metadata2020.org/) initiative. (Full disclosure: I co-chair its Best Practices group.)

<!--more-->

Books have been one of the fastest growing content types at Crossref for some time, and best practices are just one of the Book Advisory Group's efforts. Over the past ten years, the members of the books group have updated and added to the guide, and it’s now time for it to get some visibility, [so we have added it to our website](/education/content-registration/content-types-intro/books-and-chapters/) for easy reference.

<p align="center"><img src="/images/blog/bookcontent.png" alt="bookscontent" width="75%" />
</p>

These best practices are not documented for the sake of it. They have real value and can help guide internal conversations to evaluate current practices, for example. They can also play a role in making or changing policies, training staff and providing instructions to authors on citation formatting.

Here are a few recent changes I’d like to highlight:

- A new section has been added that addresses books hosted on multiple platforms
- The section on versions, (including books in multiple formats) has been expanded and clarified
- A section on the use of DOIs in citations has been added

It is neither final nor comprehensive, and never will be. Best practices by their very nature must evolve over time—and those with such a broad scope as books will inevitably lack some detail—but that’s all the more reason for the community to stay engaged. Looking ahead to future work from the group, chapter-level metadata is likely to get more attention.

Over the past few years the Books Advisory Group, chaired with aplomb by Emily Ayubi of the American Psychological Association (APA), has spent a lot of time on Crossref initiatives, like [Multiple Resolution](/get-started/multiple-resolution/) and [DOI display changes](https://doi.org/10.13003/5jchdy) but also on broader industry topics like ORCID iDs for book authors, and the Books Citation Index.

As Emily’s term as chair comes to an end this year, we welcome Charles Watkinson of the University of Michigan as chair starting in 2019. The group meets next on 12 December when we will hear from [Coko](https://coko.foundation/) about Editoria and have a discussion about developing our new [Metadata Manager](/blog/metadata-manager-members-represent/) Content Registration tool for books, and more.

If you want to share your thoughts on best practices or if you have other topics you’d like us to consider, [please get in touch](mailto:feedback@crossref.org).

___