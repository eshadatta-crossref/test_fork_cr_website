---
archives:
- 2016
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- APIs
- Data
- DOIs
- Outreach
- API Case Study
date: 2016-08-18
slug: /crossref-metadata-api-part-1-authorea
title: Using the Crossref Metadata API. Part 1 (with Authorea)
x-version: 0.0.0
---

<span ><span >Did you know that we have a shiny, not so new, </span><a href="https://github.com/CrossRef/rest-api-doc"><span >API</span></a><span > kicking around? If you missed </span><a href="/blog/researchers-just-wanna-have-funds/"><span >Geoffrey’s post in 2014 </span></a><span >(or don’t want a Cyndi Lauper song stuck in your head all day), the short explanation is that the Crossref Metadata API exposes the information that publishers provide Crossref when they register their content with us. And it’s not just the bibliographic metadata either-funding and licensing information, full-text links (useful for text-mining), ORCID iDs and update information (via Crossmark)-are all available, if included in the publishers’ metadata. </span></span>

<span >Interested? This is the kickoff a series of case studies on the innovative and interesting things people are doing with the Metadata API. Welcome to Part 1.</span>

<span ><b>What can you do with the Metadata API?</b></span>

<li >
  <span ><span >Build search interfaces. We’ve built some ourselves. Check out </span><a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//"><span >Crossref Metadata Search</span></a><span > to search the metadata of over 80 million journal articles, books, standards, datasets & more. Or </span><a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//funding"><span >Crossref Funder Search</span></a><span > t</span><span >o search nearly 15,000 funders and the 982,162 records we have that contain funding data. </span></span>
</li>
<li >
  <span ><span >Provide cross-publisher support for <a href="http://tdmsupport.crossref.org">text and data mining</a> applications.</span></span>
</li>
<li >
  <span >Get really interesting top-level reports on the metadata Crossref holds - or look at subsets of the information you’re interested in. </span>
</li>
<li >
  <span >Third parties are free to build their own products and tools that build off of the Metadata API (below are some of the many examples that we will highlight in this series).</span>
</li>

<span >Importantly, there’s no sign-up required to use the Metadata API - the data are facts from members, therefore not subject to copyright and free to use for whatever purpose anyone chooses. </span>

<span ><span >To help, Scott Chamberlain of rOpenSci has built a set of </span><a href="https://github.com/ropensci/rcrossref"><span >robust libraries for accessing the Metadata API</span></a><span >. These libraries are now available in the R, Python and Ruby languages. </span><a href="/blog/python-and-ruby-libraries-for-accessing-the-crossref-api/"><span >Scott’s blog post</span></a><span > has some great information on those. For those using the libraries, there have been a few updates since Scott’s post - </span><a href="https://github.com/sckott/serrano/blob/master/CHANGELOG.md#022-2016-06-07"><span >to serrano</span></a><span >, and support for field queries has been </span><a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md#field-queries"><span >added to habanero </span></a><span >(coming to serrano and rCrossref soon). Any feedback/bug reports can be submitted via the GitHub repos <a href="https://github.com/sckott/serrano">serrano</a> or <a href="https://github.com/sckott/habanero">habanero</a></span><span >. </span><span >There’s also a<a href="https://github.com/scienceai/crossref"> javascript library</a>, </span><span >authored by </span><span >Robin Berjon</span><span >. </span></span>

<span ><b>Who’s using the Crossref Metadata API?</b></span>

<span >We get around 30 million requests a month. We’d like to share a few case studies to showcase what they’re doing and how they’re using it. Look out for a series of posts over the next few months where we’ll open the floor to those using the API and let them explain how and why. </span>

<span >We’ll let Authorea kick things off&#8230;       </span>

<span ><b><span >Alberto Pepe, co-founder of Authorea explains:</span><a href="/wp/blog/uploads/2016/08/Authorea.png"><img class="size-full wp-image-1941 alignright" src="/wp/blog/uploads/2016/08/Authorea.png" alt="Authorea" width="297" height="124" /></a></b></span>

<span ><span ><a href="https://www.authorea.com/">Authorea</a> is a word processor for researchers and scholars. It is a collaboration platform to write, share and openly research </span><span >in real-time: write manuscripts and include rich media, such as data sets, software, source code and videos. The media-rich, data-driven capabilities of Authorea make it the perfect platform to create and disseminate a new generation of research articles, which are natively web-based, open, and reproducible. Authorea is free to use.</span></span>

<span ><b>How is the Crossref Metadata API used within Authorea?</b></span>

<span >Authorea is specifically made for scholarly documents such as research articles, conference papers, grey literature, class notes, student papers, and problem sets. What makes scholarly documents so peculiar are their citations and references, mathematical notation, tables, and data. For citations and references, we built a citation tool which allows authors to search and cite scholarly papers with ease, without having to leave the editor. While in the middle of writing a sentence, authors can click the &#8220;cite&#8221; button and a citation tool opens up:</span>

<span ><a href="/wp/blog/uploads/2016/05/Authorea-screenshot.jpg"><img class="size-medium wp-image-1715 alignleft" src="/wp/blog/uploads/2016/05/Authorea-screenshot-241x300.jpg" alt="Authorea screenshot" width="241" height="300" srcset="/wp/blog/uploads/2016/05/Authorea-screenshot-241x300.jpg 241w, /wp/blog/uploads/2016/05/Authorea-screenshot.jpg 576w" sizes="(max-width: 241px) 85vw, 241px" /></a></span>

<span >We currently use two engines for searching scholarly literature via their APIs: Crossref and Pubmed. Our authors love being able to search (by author name, paper title, topic, etc) and add references to their papers on the fly, in one click.</span>

<span ><b>What are the future plans for Authorea?</b></span>

<span >Among the many plans we have for the future, there is one which is also tied to Crossref: we are going to let authors assign DOIs to Authorea articles such as blog posts, preprints, &#8220;data papers&#8221;, &#8220;software papers&#8221; and other kinds of grey literature which does not fit in the traditional scholarly journals.</span>

<span ><b>What else would you like to see in our metadata?</b></span>

<span ><span >Well, since you ask: we would love to see unique BibTex IDs being served by the Metadata API (right now, you create the ID automatically using author name and year). Also, in some cases, some important metadata fields are missing (even author or title). I think it is actually more important to fix existing metadata rather than add new fields! </span></span>

<span ><b>Keen to share what you’re doing with the Crossref Metadata API? Contact </b><a href="mailto:feedback@crossref.org"><b>feedback@crossref.org</b></a><b> and share your story.</b></span>