---
archives:
- 2016
author: Kirsty Meddings
authors:
- Kirsty Meddings
categories:
- Crossmark
- Identifiers
- Linking
- Metadata
date: 2016-01-18
title: Linking clinical trials = enriched metadata and increased transparency
x-version: 0.0.0
---

We will shortly be adding a new feature to Crossmark. In a section called “Clinical Trials” we will be using new metadata fields to link together all of the publications we know about that reference a particular clinical trial.

Most medical journals make clinical trial registration a prerequisite for publication. Trials should be registered with one of the fifteen [WHO-approved public trial registries](https://web.archive.org/web/20160220120635/http://www.who.int/ictrp/network/primary/en) , or with [clinicaltrials.gov](http://www.clinicaltrials.gov/")  which is run by the US National Library of Medicine. Once registered, a trial is assigned a **clinical trial number (CTN)** which is subsequently used to identify that trial in any publications that report on it.  

Publications that result from any one trial are likely to be released in multiple journals from different publishers and at different times, for example secondary 
analyses coming some time after the publication of the initial results. Cross-publisher collaboration is paramount to linking all of these publications together so that researchers, funders, and regulatory agencies can understand the whole set of results from clinical trials. With this in mind, a group of medical publishers, led by BioMedCentral, approached Crossref to establish a working group, and here, [they designed an approach to address this problem:](http://blogs.biomedcentral.com/on-medicine/2014/01/31/threaded-publications-one-step-closer) “thread” all the various documents together surrounding a clinical trial.  

## Updated upstream

To implement threaded publications, publishers extract clinical trial numbers from papers, or ask authors to submit those numbers to them. Publishers add the CTNs to the Crossref DOI metadata via three new fields: clinical trial number, clinical trial registry where trial is registered, and trial stage (pre-results, results or post-results of the trial). Crossref has assigned unique IDs to each trial registry (much the same as we have done for funders in our [Funder Registry](/services/funder-registry) and for the same reason - trial registry names and URIs can change over time and we need a persistent identifier). U</span><span >sing a combination of trial registry ID and clinical trial number, we can easily identify other content in the Crossref database that cites the same trial. Finally, Crossref displays the clinical trial metadata on the respective papers for all participating Crossmark publishers. Crossmark is a convenient place for readers to access the clinical trial information and is readily accessible directly from the journal article (online and PDF versions). And of course all of the data also goes into our [open API](https://api.crossref.org) so that anyone can make use of it.   

The reporting of clinical trial results is notoriously inconsistent, something that the [AllTrials initiative](http://www.alltrials.net/") is also seeking to address. Publishers can help by collecting this information upstream and disseminating it using the existing Crossref infrastructure.   


We ask all publishers to deposit the clinical trial data which is so critical to transparency in this area of research, and have already had the [first data](https://api.crossref.org/v1/works/10.3310/hta191010) in from Crossref member the  [National Institute of Health Research](http://www.nihr.ac.uk/). Once we launch the initial set of linked clinical trials, we will expand coverage of the threaded publications to include all content that reports on or references a clinical trial, from protocol to results to supporting data and systematic reviews.

 Stay tuned and watch this space as threaded publications rolls out to journal articles across publishers!