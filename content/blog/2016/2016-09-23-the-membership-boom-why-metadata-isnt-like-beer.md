---
archives:
- 2016
author: Susan Collins
authors:
- Susan Collins
categories:
- Member Briefing
date: 2016-09-23
slug: the-membership-boom-why-metadata-isnt-like-beer
title: The membership boom & why metadata isn’t like beer
x-version: 0.0.0
---

<span >You might recognize my name if you’ve ever applied for Crossref membership on behalf of your organization. It recently occurred to me that, since I’ve been working in our membership department for eight years, I’ve been a part of shepherding new members for half of our history. And my, how we’ve grown. </span><!--more-->

### <span ><b>Membership growth by country</b></span>

<span >Though it may be easy to see our membership growth by looking at the numbers, I think it’s interesting to consider where we’ve grown.  The top ten member countries have dramatically changed since Crossref began sixteen years ago.  At the end of our first year of operations, our membership included 54 publishers and affiliated organizations.  The majority were from the US and the UK, with a small number from Germany, the Netherlands, and Japan. </span>

<span >In 2012, participation in our sponsors program began to increase. Sponsors are affiliated organizations that act on behalf of smaller publishers and societies who wish to register their content with Crossref.  Several organizations from Turkey and South Korea were among the first sponsors to join and were very successful in representing a large number of publishers and societies from their regions. Soon to follow were sponsors from India, Ukraine, Russia and Brazil. In 2014, the Public Knowledge Project (PKP) became a sponsoring affiliate, focusing on smaller publishers with the aim of increasing the quality and global reach of scholarly publishing.  With the introduction of our sponsor program, the past few years have seen a steady increase in the geographical diversity of our members.  </span>

<span ><span >There are </span><a href="http://www.worldatlas.com/nations.htm"><span >194 countries</span></a><span > in the world.  It’s pretty amazing that organizations in 112 of the world’s countries are now represented in our membership. Do I think we’ll see members joining from the other 82 nations? I don’t know but I hope so.</span></span>

<span >A look at our trending nations chart shows the diversity of our membership as we’ve grown, depicting the countries that produced the most new members over the last two years.  There has been tremendous growth from South Korea! What I find just as interesting is that we have new members from so many different nations that they form their own special bloc, shown here as &#8220;Other.&#8221; </span>

![Membership growth](/images/blog/blog_membergrowth.jpg)

<span >Our growth has taken place at a remarkable rate.  When I joined Crossref in 2008, we had over 1800 publishers and affiliates and we were adding about 300 new members per year.  In 2015, nearly 1500 members joined and we are seeing even larger numbers so far in 2016.  Counting all publishers, affiliates, libraries, sponsors and represented members, our new member total through the end of August is nearly 1200 and will most certainly overtake the 2015 figure.  </span>

![Members by year](/images/blog/blog_membersbyyear.png)

### **<span >Member perceptions</span>**

<span >With such a range of new members each month it’s even more important that we help people understand the benefits of joining Crossref.  That it’s not just registering metadata and DOIs but maintaining and improving records over time, and participating in reference linking.  We are adding and improving some educational tools that will help everyone understand how our services can enhance the discoverability of content, and why sharing richer metadata supports their full participation in the scholarly community.  We are in the process of developing a new, cleaner website with videos that better explain our services-to be released in the next few weeks,-a new onboarding experience, and new and improved query and deposit tools. </span>

### <span ><b>Connected metadata isn’t like beer </b></span>

<span >Sometimes inviting more people to a party means there is less beer to go around.  Fortunately for everyone, metadata isn’t like beer. In fact, the more metadata you draw from the tap, the more useful it becomes.  So inviting new members to join Crossref makes our community better and more valuable for everyone.  Every member uses that metadata to link their content to every other member’s content.  This makes all members’ content easier to find, link, and cite, not just at the moment it is published, but over time.   </span>

<span ><span >Members from around the globe join Crossref everyday and help guide our growing community.  If you are interested in joining please contact me at </span><a href="mailto:member@crossref.org"><span >member@crossref.org</span></a><span >.</span></span>