---
archives:
- 2016
author: Madeleine Watson
authors:
- Madeleine Watson
categories:
- Collaboration
- Data
- Event Data
- Identifiers
- Metadata
date: 2016-02-25
title: 'Event Data: open for your interpretation'
x-version: 0.0.0
---

### <span ><strong>What happens to a research work outside of the formal literature? That’s what Event Data will aim to answer when the service launches later this year.</strong></span>

<span ><a href="/wp/blog/uploads/2016/02/CROSSREF_EventData_logo.png" rel="attachment wp-att-1356"><img class="alignnone wp-image-1356 size-medium" src="/wp/blog/uploads/2016/02/CROSSREF_EventData_logo-300x124.png" alt="Crossref Event Data Logo" width="300" height="124" srcset="/wp/blog/uploads/2016/02/CROSSREF_EventData_logo-300x124.png 300w, /wp/blog/uploads/2016/02/CROSSREF_EventData_logo-768x319.png 768w, /wp/blog/uploads/2016/02/CROSSREF_EventData_logo-1024x425.png 1024w, /wp/blog/uploads/2016/02/CROSSREF_EventData_logo-1200x498.png 1200w, /wp/blog/uploads/2016/02/CROSSREF_EventData_logo.png 1374w" sizes="(max-width: 300px) 85vw, 300px" /></a></span>

<span >Following the successful <a href="/blog/crossrefs-doi-event-tracker-pilot/" target="_blank">DOI Event Tracker pilot</a> in Spring 2014, development has been underway to build our new service, newly re-named Crossref Event Data. It’s an open data service that registers online activity (specifically, events) associated with Crossref metadata. Event Data will collect and store a record of any activity surrounding a research work from a defined set of web sources. The data will be made available as part of our metadata search service or via our Metadata API and normalised across a diverse set of sources. Data will be open, audit-able and replicable.</span>

<span >We expect to include the following sources at the launch of the clearinghouse in Q3 (pending final confirmation):</span>

<span >[table id=1 /]</span>

# <span >What could you achieve?</span>

<span >Anyone interested in metrics and analytics will have direct and open access to a single collection of DOI activity data of events occurring outside of the formal literature. As Event Data records are time-stamped, you can be assured that the data you receive is both auditable and replicable. Collected and stored by Crossref in the one location, we invite researchers, publishers, funders and altmetrics providers to consider the possibilities Event Data offers to enrich and expand your work. </span>

### <span ><strong>With such a corpus of open, transferable and auditable raw data at your fingertips, what could you achieve? </strong></span>

## <span >General and altmetrics service providers</span>

<span >Crossref Event Data is a centrally-managed resource, therefore as a third party vendor you will have the ability to collect real-time data from a central location to enrich, analyze, interpret and report via your own tools. Using our API, you will gain regular access to our collection of raw, auditable data to feed into your own tools and services ready for aggregation and analysis. Additionally, the optional benefit of an SLA with Crossref will ensure that your clients have access to a reliable and flexible source of event data.</span>

## <span >Journal editors</span>

<span ><span >Using the data collected in our service, as an editor you can attract authors by offering data on the audience’s research interest, track the full-scope of article dissemination and gain a better understanding of how the publications you manage compare to each other. By analysing the Event Data records, you can q</span><span >uickly find reviewers based on publication network analysis, identify new areas to grow author submissions and track the reach of submissions selected for publication. </span></span>

## <span >Funders</span>

<span >As a Funder, you can use Event Data to isolate and track the dissemination and usage of the research you funded outside of the scholarly literature. As the data is portable, you can be assured that should a journal move, your ability to track its dissemination moves with it. Using the Event Data records collection, you can:</span>

  * <span >Efficiently track progress of the research impact of grant awardees in an automated fashion, with the signals most relevant to your organization</span>
<li >
  <span >Develop measurements of research engagement at the article level which reflect your mission and current funding priorities</span>
</li>
<li >
  <span >Gain visibility into the potent success stories highlighting the impact of your work for your development campaigns</span>
</li>
<li >
  <span >Analyze trends of past and future funding programs</span>
</li>
<li >
  <span >More effectively pursue your funding strategy and manage your portfolio based on data-driven decision making. </span>
</li>

## <span >Publishers and publishing platforms </span>

<span >By analyzing and interpreting the Event Data collection, as a publisher or content distributor you can use the records to undertake the following metric-lead analysis to help drive your business needs: </span>

<li >
  <span >Conduct more robust publication growth analysis across titles, subject areas, or all published literature</span>
</li>
<li >
  <span >Gain a balanced understanding of the engagement on your publications across subject areas, titles, or managing editors</span>
</li>
<li >
  <span >Enhance author services (personalization, content discovery, profile management, etc.)</span>
</li>
<li >
  <span >Focused and data-driven product development of tools and services to drive audience engagement</span>
</li>
<li >
  <span >Provide content distributors data on downstream reach of publications.</span>
</li>

## <span >Bibliometricians</span>

<span >Event Data heavily supports Bibliometric research by facilitating the tracking of DOI-related research activity across different platforms and channels. As a Bibliometrician, use trusted raw data as the underlying data for your research, which you can easily obtain from Crossref in a single, normalized format across a variety of sources. Additionally, as Event Data data is replicable, portable and auditable, you will be assured of high quality results in your research projects.</span>

## <span >Research institutions </span>

<span >All of the stakeholders in your institution, from the research, development and marketing offices to the researchers themselves, will benefit from access to data about where and how your research is being discussed in mainstream and social media. As a research institution, Event Data can help you:</span>

<li >
  <span >Track dissemination of publications (types of channels, rate of growth, etc.) by members of the institution</span>
</li>
<li >
  <span >Access up-to-date information on the research progress of faculty members, useful for tenure and promotion decisions</span>
</li>
<li >
  <span >View data on downstream impact of publications</span>
</li>
<li >
  <span >Roll up data for custom reporting of department’s research activities</span>
</li>

# <span >Stay tuned, testing begins soon!</span>

<span ><span >With development work on the MVP (Minimum Viable Product) scheduled to complete shortly, we will soon be releasing a small subset of data sources that are collecting event data as well as a testing environment for interested parties to explore a very preliminary version of the software as we continue to work towards implementation of the full Event Data clearinghouse release in Q3. Look out for our MVP announcement, with full technical specifications and confirmation of the selected initial pull and push sources, over the coming weeks.</span> </span>