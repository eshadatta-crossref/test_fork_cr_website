---
archives:
- 2016
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- DOIs
- Identifiers
- Linked Data
date: 2016-06-29
title: DOI-like strings and fake DOIs
x-version: 0.0.0
---

## <span >TL;DR</span>

<span >Crossref discourages our members from using DOI-like strings or fake DOIs.</span>

[<img class="alignnone wp-image-1850 size-thumbnail" src="/wp/blog/uploads/2016/06/prohibited-150x150.png" alt="discouraged" width="150" height="150" srcset="/wp/blog/uploads/2016/06/prohibited-150x150.png 150w, /wp/blog/uploads/2016/06/prohibited-300x300.png 300w, /wp/blog/uploads/2016/06/prohibited.png 729w" sizes="(max-width: 150px) 85vw, 150px" />][1]

## <span >Details</span>

<span >Recently we have seen quite <a href="https://go-to-hellman.blogspot.co.uk/2016/06/wileys-fake-journal-of-constructive.html">a bit of debate</a> around the use of so-called “fake-DOIs.” We have also been quoted as saying that we discourage the use of “fake DOIs” or “DOI-like strings”. This post outlines some of the cases in which we’ve seen fake DOIs used and why we recommend against doing so.</span>

## <span >Using DOI-like strings as internal identifiers</span>

<span >Some of our members use DOI-like strings as internal identifiers for their manuscript tracking systems. These only get registered as real DOIs with Crossref once an article is published. This seems relatively harmless, except that, frequently, the unregistered DOI-like strings for unpublished (e.g. under review or rejected manuscripts) content &#8216;escape’ into the public as well. People attempting to use these DOI-like strings get understandably confused and angry when they don’t resolve or otherwise work as DOIs. After years of experiencing the frustration that these DOI-like things cause, we have taken to recommending that our members not use DOI-like strings as their internal identifiers.</span>

### <span >Using DOI-like strings in access control compliance applications</span>

<span >We’ve also had members use DOI-like strings as the basis for systems that they use to detect and block tools designed to bypass the member’s access control system and bulk-download content. The methods employed by our members have fallen into two broad categories:</span>

  * <span >Spider (or robot) traps.</span>
  * <span >Proxy bait.</span>

### Spider traps

[<img class="alignnone wp-image-1849 size-thumbnail" src="/wp/blog/uploads/2016/06/web-150x150.png" alt="spider trap" width="150" height="150" srcset="/wp/blog/uploads/2016/06/web-150x150.png 150w, /wp/blog/uploads/2016/06/web-300x300.png 300w, /wp/blog/uploads/2016/06/web.png 729w" sizes="(max-width: 150px) 85vw, 150px" />][2]

<span >A &#8220;<a href="https://en.wikipedia.org/wiki/Spider_trap">spider trap</a>&#8221; is essentially a tripwire that allows a site owner to detect when a spider/robot is crawling their site to download content. The technique involves embedding a special trigger URL in a public page on a web site. The URL is embedded such that a normal user should not be able see it or follow it, but an automated bot (aka &#8220;spider&#8221;) will detect it and follow it. The theory is that when one of these trap URLs is followed, the website owner can then conclude that the ip address from which it was followed harbours a bot and take action. Usually the action is to inform the organisation from which the bot is connecting and to ask them to block it. But sometimes triggering a spider trap has resulted in the IP address associated with it being instantly cut off. This, in turn, can affect an entire university’s access to said member’s content.</span>

<span >When a spider/bot trap includes a DOI-like string, then we have seen some particularly pernicious problems as they can trip-up legitimate tools and activities as well. For example, a bibliographic management browser plugin might automatically extract DOIs and retrieve metadata on pages visited by a researcher. If the plugin were to pick up one of these spider traps DOI-like strings, it might inadvertently trigger the researcher being blocked- or worse- the researcher’s entire university being blocked. In the past, this has even been a problem for Crossref itself. We periodically run tools to test DOI resolution and to ensure that our members are properly displaying DOIs, Crossmarks, and metadata as per their member obligations. We’ve occasionally been blocked when we ran across the spider traps as well.</span>

### Proxy bait

[<img class="alignnone wp-image-1848 size-thumbnail" src="/wp/blog/uploads/2016/06/bait-150x150.png" alt="proxy bait" width="150" height="150" srcset="/wp/blog/uploads/2016/06/bait-150x150.png 150w, /wp/blog/uploads/2016/06/bait-300x300.png 300w, /wp/blog/uploads/2016/06/bait.png 729w" sizes="(max-width: 150px) 85vw, 150px" />][3]

<span >Using proxy bait is similar to using a spider trap, but it has an important difference. It does not involve embedding specially crafted DOI like strings on the member’s website itself. The DOI-like strings are instead fed directly to tools designed to subvert the member’s access control systems. These tools, in turn, use proxies on a subscriber’s network to retrieve the &#8220;bait&#8221; DOI-like string. When the member sees one of these special DOI-like strings being requested from a particular institution, they then know that said institution’s network harbours a proxy. In theory this technique never exposes the DOI-like strings to the public and automated tools should not be able to stumble upon them. However, recently one of our members had some of these DOI-like strings &#8220;escape&#8221; into the public and at least one of them was indexed by Google. The problem was compounded because people clicking on these DOI-like strings sometimes ended having their university’s IP address banned from the member’s web site. As you can imagine, there has been a lot of gnashing of teeth. We are convinced, in this case, that the member was doing their best to make sure the DOI-like strings never entered the public. But they did nonetheless. We think this just underscores how hard it is to ensure DOI-like strings remain private and why we recommend our members not use them.</span>

## <span >Pedantry and terminology</span>

<span >Notice that we have not used the phrase &#8220;fake DOI&#8221; yet. This is because, internally, at least, we have distinguished between &#8220;DOI-like strings&#8221; and &#8220;fake DOIs.&#8221; The terminology might be daft, but it is what we’ve used in the past and some of our members at least will be familiar with it. We don’t expect anybody outside of Crossref to know this.</span>

<span >To us, the following is not a DOI:</span>

<span >10.5454/JPSv1i220161014</span>

<span >It is simply a string of alphanumeric characters that copy the DOI syntax. We call them &#8220;DOI-like strings.&#8221; It is not registered with any DOI registration agency and one cannot lookup metadata for it. If you try to “resolve” it, you will simply get an error. Here, you can try it. Don’t worry- clicking on it will not disable access for your university.</span>

<http://doi.org/10.5454/JPSv1i220161014>

<span >The following is what we have sometimes called a “fake DOI”</span>

<span >10.5555/12345678</span>

<span >It is registered with Crossref, resolves to a fake article in a fake journal called The Journal of Psychoceramics (the study of Cracked Pots) run by a fictitious author (Josiah Carberry) who has a fake ORCID (<a href="http://orcid.org/0000-0002-1825-0097">http://orcid.org/0000-0002-1825-0097</a>) but who is affiliated with a real university (<a href="http://www.brown.edu">Brown University</a>).</span>

<span >Again, you can try it.</span>

<span ><a href="http://doi.org/10.5555/12345678">http://doi.org/10.5555/12345678</a></span>

<span >And you can even look up metadata for it.</span>

<span ><a href="https://api.crossref.org/v1/works/10.5555/12345678">https://api.crossref.org/v1/works/10.5555/12345678</a></span>

<span >Our dirty little secret is that this &#8220;fake DOI&#8221; was registered and is controlled by Crossref.</span>

<span >Why does this exist? Aren’t we subverting the scholarly record? Isn’t this awful? Aren’t we at the very least hypocrites? And how does a real university feel about having this fake author and journal associated with them?</span>

<span >Well- the DOI is using a prefix that we use for testing. It follows a long tradition of test identifiers starting with &#8220;5&#8221;. <a href="https://en.wikipedia.org/wiki/555_(telephone_number)">Fake phone numbers in the US start with &#8220;555&#8221;</a>. Many credit card companies <a href="https://web.archive.org/web/20160707151357/https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/credit_card_numbers.htm">reserve fake numbers starting with &#8220;5&#8221;</a>. For example, Mastercard’s are &#8220;5555555555554444&#8221; and &#8220;5105105105105100.&#8221;</span>

<span >We have created this fake DOI, the fake journal and the fake ORCID so that we can test our systems and demonstrate interoperable features and tools. The fake author, Josiah Carberry, is <a href="http://library.brown.edu/hay/carberry.php">a long-running joke at Brown University</a>. He even has a <a href="https://en.wikipedia.org/wiki/Josiah_S._Carberry">Wikipedia entry</a>. There are also a lot of other DOIs under the test prefix “5555.”</span>

<span >We acknowledge that the term “fake DOI” might not be the best in this case- but it is a term we’ve used internally at least and it is worth distinguishing it from the case of DOI-like strings mentioned above.</span>

<span >But back to the important stuff….</span>

<span >As far as we know, none of our members has ever registered a &#8220;fake DOI&#8221; (as defined above) in order to detect and prevent the circumvention of their access control systems. If they had, we would consider it much more serious than the mere creation of DOI-like strings. The information associated with registered DOIs becomes part of the persistent scholarly citation record. Many, many third party systems and tools make use of our API and metadata including bibliographic management tools, <a href="https://en.wikipedia.org/wiki/Text_mining">TDM</a> tools, <a href="https://en.wikipedia.org/wiki/Current_research_information_system">CRIS</a> systems, <a href="https://en.wikipedia.org/wiki/Altmetrics">altmetrics</a> services, etc. It would be a very bad thing if people started to worry that the legitimate use of registered DOIs could inadvertently block them from accessing content. Crossref DOIs are designed to encourage discovery and access- not block it.</span>

<span >And again, we have absolutely no evidence that any of our members has registered fake DOIs.</span>

<span >But just in case, we will continue to discourage our members from using DOI-like strings and/or registering fake DOIs.</span>

<span >This has been a public service announcement from the identifier dweebs at Crossref.</span>

## <span >Image Credits</span>

<span >Unless otherwise noted, included images purchased from <a href="https://thenounproject.com/">The Noun Project</a></span>

 [1]: #
 [2]: #
 [3]: #