---
archives:
- 2016
author: Jennifer Kemp
authors:
- Jennifer Kemp
categories:
- Collaboration
- Crossref LIVE
- Metadata
- Outreach
- Community
date: 2016-06-28
title: Outreach Day DC. Next Up? You Tell Us
x-version: 0.0.0
---

<span >Rallying the community is a key Crossref role. Sometimes this means collaborating on new initiatives but it is also an ongoing process, a cornerstone of our outreach efforts. Part of rallying the community is bringing people together, literally, in a series of outreach days around the globe. It means we encourage dialog with us and among members and non-publisher affiliates. We want to hear from the community and we hope to facilitate conversations in it. Not just about Crossref, but larger issues of scholarly communications and your particular part in it. The Crossref outreach team is doing a number of events around the world to bring together the community for updates, feedback and discussion.</span>

<span >On 16 June, Crossref hosted an all day session in Washington, DC where we were joined by about 35 attendees from the region, mostly publishers. The size of the group made for lots of discussion, and we are grateful for the feedback. Here is what we took away from the event:</span>

### <span ><strong>We all need a better understanding of who is using Crossref metadata and how</strong></span>

<span >Sure, we all know that, for example, submission systems, libraries and hosting platforms use Crossref metadata (‘metadata out’), but pinpointing where in workflows (often multiple instances) and the interplay between publishers and these systems? Not so much. <strong>Help us change that:</strong> <a href="http://goo.gl/forms/E1l4rYHHLEHb8bsj1" target="_blank">take this short survey</a> to tell us how publisher metadata quality affects your systems and workflows and we will, in turn, make use cases (anonymized if you wish) available as part of an ongoing effort to promote the value of more, better and enriched metadata.</span>

<span >Here I must say a big thank you to our guest speaker for the day, Carly Robinson, who provided an excellent presentation on the work of <a href="http://www.osti.gov/home/about.html" target="_blank">OSTI</a>, of the U.S. Department of Energy. Carly shared examples of how OSTI uses the Crossref metadata in their systems to aid compliance and compliment the DOE public access model. A live use case is a welcome way to partner with our community!</span>

### <span ><strong>The more things change, the more they emphasize core best practices</strong></span>

<span >A good part of the day was spent on new initiatives such as: <a href="/blog/members-will-soon-be-able-to-assign-crossref-dois-to-preprints/" target="_blank">DOIs for preprints</a>, <a href="/blog/auto-update-has-arrived-orcid-records-move-to-the-next-level/" target="_blank">auto-update of ORCID records</a>, <a href="/blog/community-responses-to-our-proposal-for-early-content-registration/" target="_blank">'early content registration' </a>, <a href="/blog/linked-clinical-trials-are-here/" target="_blank">linked clinical trials</a> and <a href="/blog/" target="_blank">more</a>. All good stuff-the industry evolves and workflows must keep pace-but none of which generated a great deal of questions or expressed concern.</span>

<span >One session that did spur a lot of discussion was a simple overview of where Crossref services sit in the publishing process (including pre- and post-). Perhaps this is because it was early in the day but the much-appreciated discussion underscored the need to make the case for enriched metadata in a well-understood workflow that reflects the roles of publishers and affiliate users of metadata.</span>

### <span ><b>Outreach is an experiment in which we are all subjects</b></span>

<span >Finally, it must be noted here that we actively seek feedback on our Community Outreach days! We are not a large team and we can’t do as many outreach days as we’d like, but we are very open to hearing from you: So, tell us in <a href="http://goo.gl/forms/MDDRy8WUgyiwzo4m2" target="_blank">this quick survey:</a> what should we discuss? And where should we head next?</span>

<img class="alignleft" src="/wp/blog/uploads/2016/06/Crossref-DC-Outreach-Day-2016.jpg" alt="D.C. Crossref Outreach Day" width="436" height="327" />