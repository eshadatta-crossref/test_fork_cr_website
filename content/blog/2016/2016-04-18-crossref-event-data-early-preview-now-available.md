---
archives:
- 2016
author: Madeleine Watson
authors:
- Madeleine Watson
categories:
- Citation
- DataCite
- Collaboration
- Data
- Event Data
- Identifiers
- News Release
- Wikipedia
date: 2016-04-18
title: 'Crossref Event Data: early preview now available'
x-version: 0.0.0
---

<img src="http://assets.crossref.org/logo/crossref-event-data-logo-200.svg" alt="Crossref Event Data logo" width="200" height="83" />

<span >Test out the early preview of Event Data while we continue to develop it. Share your thoughts. And be warned: we may break a few eggs from time to time!</span><figure id="attachment_1530"  class="wp-caption alignright">

<a href="/wp/blog/uploads/2016/04/Screen-Shot-2016-04-18-at-14.43.59.png" rel="attachment wp-att-1530"><img class="wp-image-1530 size-full" src="/wp/blog/uploads/2016/04/Screen-Shot-2016-04-18-at-14.43.59.png" alt="Egg" width="197" height="243" /></a><figcaption class="wp-caption-text"></span> <span >Chicken by anbileru adaleru from the The Noun Project</span></figcaption></figure>

<span >Want to discover which research works are being shared, liked and commented on? What about the number of times a scholarly item is referenced? Starting today, you can whet your appetite with an early preview of the forthcoming Crossref Event Data service. We invite you to start exploring the activity of DOIs as they permeate and interact with the world after publication.</span>

<!--more-->

## <span >But first, a bit of background</span>

<span ><span >Discussion around scholarly research increasingly occurs online after publication, for example on blogs, sharing services, social media, and wikis. These ‘events’ occur across the web on numerous platforms and are a critical part of the scholarly enterprise. We are developing an infrastructure service (</span><a href="http://eventdata.crossref.org"><span >Crossref Event Data</span></a><span >) that collects, stores, and delivers raw data of the events occurring with Crossref DOIs. We will store the data in an open, auditable and portable form for the community to access. Publishers, platforms, funders, bibliometricians and service providers may benefit from access to this raw data, and it can be used to feed into research records or proprietary tools and services that offer aggregation and analysis. </span></span>

<span ><span >For more information, see our </span><a href="/blog/det-poised-for-launch/"><span >pilot blog post</span></a><span > and description of </span><a href="/blog/event-data-open-for-your-interpretation/"><span >potential use cases</span></a><span >.</span></span>

## <span >Collaborative, transparent development </span><figure id="attachment_1524"  class="wp-caption alignright">

<a href="/wp/blog/uploads/2016/04/JoeMartin.png" rel="attachment wp-att-1524"><img class="size-medium wp-image-1524" src="/wp/blog/uploads/2016/04/JoeMartin-300x236.png" alt="Photo of collaborators Martin Fenner and Joe Wass enjoying a meal together. " width="300" height="236" srcset="/wp/blog/uploads/2016/04/JoeMartin-300x236.png 300w, /wp/blog/uploads/2016/04/JoeMartin.png 438w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text"></span> <span >Developers Martin Fenner (DataCite) and Joe Wass (Crossref) enjoy a tofu break</span></figcaption></figure>

<span ><span >Lagotto, the software originally developed at PLOS, has been extended and improved in a joint effort between DataCite and Crossref. The two DOI Registration Agencies have partnered to envision, build and release the service. On the 13th of April, after a year of</span> <span >collaboration, we jointly released Lagotto 5.0. You can read about the collaboration on the </span></span><span ><a href="https://doi.org/10.5438/pe54-zj5t"><span >DataCite blog post</span></a><span >.</span></span>

<span >Crossref and DataCite will continue to work closely together to develop Lagotto and the Event Data service. Although Crossref Event Data has mostly Crossref DOIs at launch, you will be able to find DataCite DOIs if they are cited in Crossref or Wikipedia.</span>

 <span ><span >All of the software that runs Event Data, including Lagotto, is developed in the open and is open source. Please refer to the </span><a href="http://eventdata.crossref.org/guide/"><span >Crossref Event Data Technical User Guide</span></a><span > for full details.</span></span>

## <span >Preview the data</span>

<span ><span >This service is currently under development with a full launch expected the second half of 2016. Before it is launched however, we invite you to take a look around and preview a subset of the data sources we plan to include. Y</span><span >ou may experience occasional hiccups while we continue building the service.</span></span>

<span >At this stage, we are working with data from three sources although we will greatly expand the variety of platforms from which we collect data as development progresses. At this stage, you can view Mendeley bookmarks, Wikipedia references, and Crossref to DataCite links.</span>

### <span >Mendeley</span>

<span >Mendeley is a reference manager and academic social network for scholars. View the number of social bookmarks from scholars or groups on Mendeley.</span>

<span ><span >For example,  </span><a href="http://doi.org/10.1016/J.JIP.2016.03.007"><span >doi.org/10.1016/J.JIP.2016.03.007</span></a><span > currently has </span><a href="https://www.mendeley.com/research/hygienic-food-reduce-pathogen-risk-bumblebees/"><span >8 readers on Mendeley</span></a><span > to date.</span></span>

<span ><a href="/wp/blog/uploads/2016/04/Medeley-example.png" rel="attachment wp-att-1525"><img class="alignnone wp-image-1525 size-large" src="/wp/blog/uploads/2016/04/Medeley-example-1024x446.png" alt="Example of event data in Mendeley." width="840" height="366" srcset="/wp/blog/uploads/2016/04/Medeley-example-1024x446.png 1024w, /wp/blog/uploads/2016/04/Medeley-example-300x131.png 300w, /wp/blog/uploads/2016/04/Medeley-example-768x334.png 768w, /wp/blog/uploads/2016/04/Medeley-example-1200x522.png 1200w, /wp/blog/uploads/2016/04/Medeley-example.png 1300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" /></a></span>

### <span >Wikipedia </span>

<span >Wikipedia is an online encyclopaedia, the Internet’s largest and most popular general reference work. View references in Wikipedia of Crossref publications in Wikipedia articles in all languages.</span>

<span ><span >For example, </span><a href="http://doi.org/10.3897/ZOOKEYS.565.7185"><span >doi.org/10.3897/ZOOKEYS.565.7185</span></a><span > was referenced in the </span><a href="https://ru.wikipedia.org/wiki/Oxyscelio"><span >Russian Wikipedia page on Oxyscelio</span></a></span>

<span ><a href="/wp/blog/uploads/2016/04/Wikipedia-example.png" rel="attachment wp-att-1526"><img class="alignnone wp-image-1526 size-large" src="/wp/blog/uploads/2016/04/Wikipedia-example-1024x472.png" alt="Example of event data for a DOI referenced in a Wikipedia page" width="840" height="387" srcset="/wp/blog/uploads/2016/04/Wikipedia-example-1024x472.png 1024w, /wp/blog/uploads/2016/04/Wikipedia-example-300x138.png 300w, /wp/blog/uploads/2016/04/Wikipedia-example-768x354.png 768w, /wp/blog/uploads/2016/04/Wikipedia-example-1200x553.png 1200w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" /></a></span>

### <span >Crossref to DataCite links</span>

<span >DataCite is a global consortium that assigns DOIs to research data. This enables people to find, share, use, and cite data. You can view all the data citations to DataCite research outputs found in Crossref publications (work is underway to make the links found in DataCite metadata available in Event Data). </span>

<span ><span >For example, Global, Regional, and National Fossil-Fuel CO2 Emissions (<a href="http://doi.org/10.3334/CDIAC/00001" target="_blank">doi.org/10.3334/CDIAC/00001</a>) dataset </span><span >has been referenced by </span><a href="http://api.eventdata.crossref.org/works/doi.org/10.3334/CDIAC/00001"><span >six Crossref publications</span></a><span > to date. Software links are also included. Another</span><span > example is</span><span > </span><span >PGOPHER (<a href="http://doi.org/10.5523/bris.huflggvpcuc1zvliqed497r2">doi.org/10.5523/bris.huflggvpcuc1zvliqed497r2</a>)</span><span >, a general purpose software for simulating and fitting rotational, vibrational and electronic spectra, which has been referenced by </span><a href="http://api.eventdata.crossref.org/works/doi.org/10.5523/BRIS.HUFLGGVPCUC1ZVLIQED497R2"><span >seven Crossref publications</span></a><span > to date.</span></span>

## <span >Ready to take a spin?</span>

<span ><span >You can explore the Crossref Event Data early preview by visiting </span><a href="http://eventdata.crossref.org"><span >http://eventdata.crossref.org</span></a><span > and following the links to featured examples within our interim application for inspecting the data, technical documentation, and our </span><a href="http://eventdata.crossref.org/guide/#quick-start"><span >Quick Start guide</span></a><span >.</span></span>

## <span >Share your thoughts</span>

<span ><span >This service is currently under development and as such we welcome your thoughts and feedback on the data we are collecting curren</span><span >tly from our three active sources. As a reminder, we expect to include the following sources as part of our full service launch later this year </span><span >(pending confirmation):</span></span>

<span >[table id=1 /]</span>

&nbsp;

<span ><span >We’re also on the lookout for new data sources to investigate for future inclusion in the Event Data service so please do </span><a href="mailto:eventdata@crossref.org"><span >get in touch</span></a><span > with requests and recommendations. As we continue to build the service throughout 2016, we will be committing to a model of continuous development so that we can make new sources available as they are completed.</span></span>

<span >Watch this blog for regular updates on our progress, or subscribe to receive new blog posts by email (just add your details to the upper right side of this page).</span>

&nbsp;

&nbsp;

&nbsp;

&nbsp;