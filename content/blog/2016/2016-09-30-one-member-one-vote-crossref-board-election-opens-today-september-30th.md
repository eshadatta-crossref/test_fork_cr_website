---
archives:
- 2016
author: Lisa Hart Martin
authors:
- Lisa Hart Martin
categories:
- Board
- Member Briefing
- News Release
- Crossref LIVE
- Election
date: 2016-09-30
title: 'One member, one vote: Crossref Board Election opens today, September 30th'
x-version: 0.0.0
---

## <span ><b>Watch for two important emails on September 30</b><b>th</b><b> – one with a voting link and material, and one with your username and password.</b></span>

<span ><b>Running</b><span > Crossref well is a key part of our mission. It’s important that we be as neutral and fair as possible, and we are always striving for that balance. One of our stated principles is “One member, one vote”. And each year we encourage each of our members-standing at over 6000 today-to participate in the election of new board members.</span></span>

<span ><span >It is hard to believe that November 2</span><span >nd</span><span > will be Crossref’s 17</span><span >th</span><span > annual meeting and our 16</span><span >th</span><span > annual Board of Directors election. How time flies, and <a href="/blog/the-membership-boom-why-metadata-isnt-like-beer/">oh, how we have grown</a>!</span></span><figure id="attachment_2215"  class="wp-caption alignnone">

[<img class="size-large wp-image-2215" src="/wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths-1024x663.png" alt="Crossref's Truths, taken from our forthcoming new website. " width="840" height="544" srcset="/wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths-1024x663.png 1024w, /wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths-300x194.png 300w, /wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths-768x497.png 768w, /wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths-1200x777.png 1200w, /wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths.png 1287w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" />][1]<figcaption class="wp-caption-text">Crossref’s Truths, taken from our forthcoming new website.</figcaption></figure>

<span ><span >I am hoping that we can </span><b>rally</b><span > the membership to participate in this important process!</span></span>

<span >Candidates will be elected at <a href="https://www.eventbrite.com/e/crossref-live16-registration-25928526922">Crossref LIVE16</a> for three-year terms to fill five of the 16 Board seats whose terms expire this year.  The slate of candidates was recommended by the Nominating Committee, which consisted of three Board members not up for re-election, and two Crossref members that are not on the Board. </span>

<span >This year, Jasper Simons, APA; Paul Peters, Hindawi; Jason Wilde, AIP; Chris Fell, Cambridge University Press; and Rebecca Lawrence, f1000 served on the Nominating Committee.  The Committee met to discuss the process, criteria, and potential candidates, and put forward a slate which was required to be at least equal to the number of Board seats up for election. The slate may or may not consist of Board members up for re-election.</span>

<span ><span >Crossref members are welcome to run as independent candidates, as long as they have ten member endorsements sent to </span><a href="mailto:lhart@crossref.org"><span >lhart@crossref.org</span></a><span > with the intent to run. We sent a notification of the process in advance (this year on August 26</span><span >th</span><span >), so any nominations could be included in the voting materials that will be sent via email on September 30</span><span >th</span><span >.</span></span>

### **<span >You can access online voting from today at:</span>**

### **<span ><a href="https://eballot4.votenet.com/PILA/admin">https://eballot4.votenet.com/PILA/admin</a>. Watch your inbox today for emails with your username and password!</span>**

 [1]: /wp/blog/uploads/2016/09/screencapture-crossref-org-about-truths.png