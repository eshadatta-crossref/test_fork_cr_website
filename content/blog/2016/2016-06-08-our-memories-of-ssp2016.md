---
Slug: our-memories-of-ssp2016
archives:
- 2016
author: April Ondis
authors:
- April Ondis
categories:
- Meetings
- Outreach
- Community
date: 2016-06-08
title: 'Our memories of #SSP2016'
x-version: 0.0.0
---

<p class="p1">
  <b></b><span class="s1" >Last week a bunch of Crossref’s staff traveled to the 2016 Society for Scholarly Publishing Annual Meeting in Vancouver, BC.  After we returned en masse, all nine of us put our heads together to share some of our personal memories of the event.   </span>
</p><figure id="attachment_1732"  class="wp-caption alignright">

[<img class="wp-image-1732 " src="/wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_-240x300.jpg" alt="Enjoying-the-High-Wire-Run-Walk-at-SSP2016_" width="314" height="393" srcset="/wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_-240x300.jpg 240w, /wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_-768x959.jpg 768w, /wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_-820x1024.jpg 820w, /wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_-1200x1499.jpg 1200w, /wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_.jpg 1500w" sizes="(max-width: 314px) 85vw, 314px" />][1]<figcaption class="wp-caption-text"><span >Crossref’s Rosa and Susan at the Fun Walk/Run sponsored by High Wire. 5K before breakfast!</span></figcaption></figure>

<p class="p1">
  <span class="s1" ><b>On <em>Cybersecurity and the Scholarly World</em> —</b>“The session described the many and complicated security threats that IT systems face and how threat detection and defense is a constantly ongoing activity. Certainly system administrators are challenged with the technology issues that build firewalls, block intrusions and divert disruptive activity. But perhaps even more important are the social issues that must be managed to develop an informed user community that is immune to the less technical but probably more effective hacks like phishing for user passwords.”</span>
</p>

<p class="p1">
  <span class="s1" ><b>On <em>Persistent Identifiers in Scholarly Communications: What, Why, How, Where, and Who? </em></b>“Everyone from Crossref loved this panel, which should come as no surprise (wink).  Persistent identifiers such as DOIs and ORCID iDs enable machine and human readers to discover, cite, link, and correctly attribute works across different platforms.  David Crotty of the Oxford University Press said it best with &#8216;If you’re not actively building these persistent identifiers into your systems, get busy!’ Alice Meadows of ORCID represented the scholarly communications infrastructure with an image of shiny copper plumbing - don’t tell me we don’t have glamorous jobs!  Laura Rueda of DataCite had particularly helpful diagrams to explain how persistent identifiers ease and speed the workflow of a research object as it travels from researcher to publisher to the greater community.” </span>
</p>

<p class="p4">
  <span ><span class="s3"><b>On</b> <em><b>Crossing Boundaries: Encouraging Diversity in Scientific Communication</b></em> with </span><span class="s1">Dr. Margaret-Ann Armour<i> — </i>“I decided to attend this keynote when I saw that men as well as women were in the audience.  Dr. Armour had great anecdotes that supported formal data on women’s roles in STEM.  It made me reflect on how the path to a career in scholarly publishing is often not direct, and relies on personal networking.  She was very witty and deserved her standing ovation.”</span></span>
</p>

<p class="p4">
  <span ><span class="s1"><b>On <i>Standards and Recommended Practices to Support Adoption of Altmetrics</i> —  </b>“Todd Carpenter summed up the intent behind many altmetrics initiatives when he said that understanding how many people are using and reading scholarly content is important because &#8216;we all want to know how we’re doing’ but &#8216;this project should never become the number’ because the intent is about ‘trying to add flavor and nuance to the conversation in a meaningful way’.  Stuart Maxwell of Scholarly IQ also made a really astute observation that “all assessment is in some way subjective - impact is relative to how you compare yourself to other researchers in your field.” What especially appealed to me about this session was learning that NISO extends its remit to include the data quality performance of altmetrics aggregators themselves.  Asking each aggregator to self-report a publicly available, annual accounting of how they comply with the <a href="https://www.niso.org/publications/rp-25-2016-altmetrics">Altmetrics Data Quality Code of Conduct</a> will likely increase consistency, transparency and trust.&#8221; </span><span class="s1"> </span></span>
</p>

### <span class="s1" ><b>SSP receptions & evening events, where mashed potato sundaes were a thing</b></span> {.p1}

<p class="p1">
  <span class="s1" >Yes, the sessions are great, but some of the really interesting sights, sounds and discussions occur at the evening events. It’s impossible for one person to cover all of them (or is it?), but our idea of a few memorable highlights from this year’s SSP are, in no particular order:</span>
</p>

<p class="p1">
  <span class="s1" >&#8220;Tuesday’s reception—bar conveniently located just steps from the Crossref booth meant lots of good traffic! The convivial atmosphere made it easy to ignore that we were all tantalizingly close to the glorious view just outside the hotel doors. Wednesday’s reception was a chance to meet all the folks who didn’t make it in Tuesday. Though it seems most of us were delayed arriving in Vancouver, it was well worth the trip and arriving to find a few hundred colleagues all enjoying happy hour is a fine way to start a meeting.&#8221;</span>
</p>

<p class="p1">
  <span class="s1" >&#8220;HighWire’s reception at the Vancouver Rowing Club provided a lovely walk on the way there, a great band at the party and a shrimp tower almost (but not quite) too good looking to eat. The pouring rain on the walk back made for a memorable bonding experience.&#8221;</span>
</p>

<p class="p1">
  <span class="s1" >&#8220;Wildebeest was the atmospheric site of the Silverchair reception and great chance to see a bit of downtown before enjoying some good cheese and fine company. At least two of us attending made plans to save the world through better metadata. Over sparkling rose wine no less.&#8221;</span>
</p>

<p class="p7">
  <span ><span class="s5"><br /> <a href="/wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1.jpeg"><img class=" wp-image-1736 alignleft" src="/wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1-300x225.jpeg" alt="Sheridan-at-Vancouver-Aquarium" width="360" height="270" srcset="/wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1-300x225.jpeg 300w, /wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1-768x576.jpeg 768w, /wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1-1024x768.jpeg 1024w, /wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1-1200x900.jpeg 1200w, /wp/blog/uploads/2016/06/Sheridan-at-Vancouver-Aquarium-1.jpeg 1227w" sizes="(max-width: 360px) 85vw, 360px" /></a>&#8220;Dolphins and sea otters made merry in a pool outside the Sheridan Group reception at the Vancouver Aquarium, while we noshed and drank with the fishes inside.  But the food rivalled the undersea sights. </span><span class="s1">A very nice gentleman with an ice cream scoop filled a parfait glass with a perfectly round dollop of mashed potatoes and told me to help myself to toppings. Shut the front door! I got the works.  Delicious creamy mashed (whipped) potatoes of a perfect consistency, a ladle full of warm brown gravy topped with a generous sprinkle of finely sliced green onions (scallions), and a healthy spray of large, crispy bacon pieces!!  It looked like a sundae … that you eat with a fork!!&#8221;<br /> </span></span>
</p>

<p class="p1">
  <span class="s1" >&#8220;The President’s reception was in the world’s largest hotel suite (approximately), with some very photogenic desserts and a lot of happy people who know that it’s well worth sacrificing some sleep for the event.&#8221;</span>
</p>

<p class="p1">
  <span class="s1" >Of course, the hotel bar in the evenings had some memorable discussions too but what happens in the bar stays in the bar, right? And we should probably all be grateful for the early last call …</span>
</p>

<p class="p1">
  <span ><strong><span class="s1">’Til next year!</span></strong></span>
</p>

 [1]: /wp/blog/uploads/2016/06/Enjoying-the-High-Wire-Run-Walk-at-SSP2016_.jpg