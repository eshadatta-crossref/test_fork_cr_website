---
archives:
- 2016
author: Jennifer Lin
authors:
- Jennifer Lin
categories:
- Citation
- Metadata
- Persistence
- Research Nexus
date: 2016-08-25
title: 'The article nexus: linking publications to associated research outputs'
x-version: 0.0.0
---

<span >Crossref began its service by linking publications to other publications</span> <span >via references.</span> <span >Today, this extends to relationships with associated entities. People (authors, reviewers, editors, other collaborators), funders, and research affiliations are important players in this story. Other metadata also figure prominently in it as well: references, licenses and access indicators, publication history (updates, revisions, corrections, retractions, publication dates), clinical trial and study information, etc. The list goes on.</span>

What is lesser known (and utilized) is that Crossref is increasingly linking publications to associated scholarly artifacts. At the bottom of it all, these links can help researchers better understand, reproduce, and build off of the results in the paper. But associated research objects can enormously bolster the research enterprise in many ways (e.g., discovery, reporting, evaluation, etc.).

> With all the relationships declared across all 80+ million Crossref metadata records, Crossref creates a global metadata graph across subject areas and disciplines that can be used by all.

### Research article nexus

<span >As research increasingly goes digital, more research artifacts associated with the formal publication are stored or shared online. We see a plethora of materials closely connected to publications, including: versions, peer reviews, datasets generated or analysed in the research, software packages used in the analysis, protocols and related materials, preprints, conference posters, language translations, comments, etc. Occasionally, these resources are linked from the publication. But very rarely are these relationships made available beyond the publisher platform. </span>

<span >Crossref will make these relationships available to the broader research ecosystem. When publishers register content for a publication, they can identify the associated scholarly artifacts directly in the article metadata. Doing so not only groups digital objects together, but formally associates with the publication. Each link is a relationship and the sum of all these relationships constitutes a “</span>**research article nexus.**<span >”</span>

[<img class="alignnone wp-image-1990 size-large" src="/wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px-1024x956.png" width="840" height="784" srcset="/wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px-1024x956.png 1024w, /wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px-300x280.png 300w, /wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px-768x717.png 768w, /wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px-1200x1120.png 1200w, /wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px.png 1250w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" />][1]

An assortment of connections already abound in the wild today. Examples include:

<li >
  <span >F1000Research article <a href="http://doi.org/10.12688/f1000research.2-198.v3">http://doi.org/10.12688/f1000research.2-198.v3</a></span><span > connected to initial version <a href="http://doi.org/10.12688/f1000research.2-198.v1">http://doi.org/10.12688/f1000research.2-198.v1</a> </span>
</li>
<li >
  <span >OECD publication </span><a href="http://doi.org/10.1787/empl_outlook-2014-en"><span >http://dx.doi.org/10.1787/empl_outlook-2014-en</span></a><span > and its German translation </span><a href="http://doi.org/10.1787/empl_outlook-2014-de"><span >http://dx.doi.org/10.1787/empl_outlook-2014-de</span></a>
</li>
<li >
  <span >PeerJ article </span><a href="https://doi.org/10.7717/peerj.1135"><span >http://doi.org/10.7717/peerj.1135</span></a><span > and its peer review <a href="http://doi.org/10.7287/peerj.1135v0.1/reviews/3">http://doi.org/10.7287/peerj.1135v0.1/reviews/3</a> </span>
</li>
<li >
  <span >eLife article </span><a href="http://doi.org/10.7554/eLife.09771"><span >http://doi.org/10.7554/eLife.09771</span></a><span > and its BioArXiv preprint </span><a href="http://doi.org/10.1101/018317"><span >http://doi.org/10.1101/018317</span></a>
</li>
<li >
  <span >PLOS ONE article </span><a href="http://dx.doi.org/10.1371/journal.pone.0161541"><span >http://doi.org/10.1371/journal.pone.0161541</span></a><span > with underlying data in Dryad </span><a href="http://dx.doi.org/10.5061/dryad.d2vf8"><span >http://doi.org/10.5061/dryad.d2vf8</span></a>
</li>
<li >
  <span >Frontiers article </span><a href="http://dx.doi.org/10.3389/fevo.2015.00015"><span >http://doi.org/10.3389/fevo.2015.00015</span></a><span > with a figshare </span><a href="https://dx.doi.org/10.6084/m9.figshare.1305089.v1"><span >http://doi.org/10.6084/m9.figshare.1305089.v1</span></a><span > video </span>
</li>
<li >
  <span >Journal of Chemical Theory and Computation article </span><a href="http://doi.org/10.1021/ct400399x"><span >http://doi.org/10.1021/ct400399x</span></a><span > with software archived in Zenodo </span><a href="http://doi.org/10.5281/zenodo.60678"><span >http://doi.org/10.5281/zenodo.60678</span></a>
</li>
<li >
  <span >Nature Biotech article </span><a href="http://doi.org/10.1038/nbt.3481"><span >http://doi.org/10.1038/nbt.3481</span></a><span > with a Protocols.io protocol </span><a href="http://doi.org/10.17504/protocols.io.dm649d"><span >http://doi.org/10.17504/protocols.io.dm649d</span></a>
</li>

To date, almost all these relationships are not directly recorded in the article metadata (great job, PeerJ!). And as a result, they are more than likely “invisible” to the broader scholarly research ecosystem. Publishers can remedy these gaps by depositing associations when registering content with Crossref or updating the records after registration. That is how the article nexus is formed.

<span >(Associated datasets can also be identified in the reference list as per </span>[<span >Joint Declaration of Data Citation Principles</span>][2] <span >as with the </span>[<span >FORCE11 Software Citation Principles</span>][3]<span >. </span>_<span >Stay tuned next week for a follow up blog post on Crossref’s support for publisher data and software citations through its metadata.</span>_<span >)</span>

### Forming the nexus

<span >The mechanism of declaring these relationships is straightforward and a longstanding part of the standard deposit process. For each associated research object, simply provide the identifier and identifier type for the object, an optional description of it, as well as name the relationship into the metadata record. For the latter, Crossref and DataCite share a closed list of relationship types, which ensures interoperability between mappings. See Crossref </span>[<span >technical documentation</span>][4] <span >for more details. </span>

<span >We maintain a </span>[<span >list of the recommended relation types</span>][5] <span >for a host of associated research objects to promote standardization across publishers. If you have relationships not specified, please contact us at </span>[<span >feedback@crossref.org</span>][6] <span >to identify a suitable one considered best practice. Common adoption of relation types will make relationship metadata useful to tool builders and systems. For example, programmatic queries on supporting materials require proper tagging of their respective relationship types.</span>

This approach is highly extensible and accommodates the introduction of new research object forms as they emerge. It also supports associated research objects regardless of identifier type. When an associated entity has a DOI, however, we can validate the relationship during metadata processing as well as provide a more reliable representation of the article nexus.

### Article nexus: a far richer scholarly map

<span >Bibliographic metadata is like a ship’s manifest that catalogs each item of cargo in a ship’s hold - crate, drum, sack, and barrel. It identifies the components that have an internal relation to the publication (contributor, funder, article update, license, etc.), each of which are well-understood points on the scholarly map. But when we integrate the article nexus into the graph, new territories become visible - not isolated islands, but places with highways connecting them to addresses already known.</span>

<span >When a publication has its relationships clearly identified, the connections both go out as well as lead back to it. The more connections, the more visibility on the </span>[<span >scholarly map, as the Art of Cartography</span>][7] <span >goes. Numerous systems tap into this map: publishing, funders, research institutions, research councils, indexers & repositories, indexers, research information systems, lab & diagnostics systems, reference management and literature discovery, other PID suppliers. So publishers, you can provide the fullest value to your own publishing operation, your authors, their research communities, and the overall research enterprise by ensuring that all publications are fully linked both inside and out.</span>

 [1]: /wp/blog/uploads/2016/08/DOI-network-diagram_v3_600x560px.png
 [2]: https://www.force11.org/group/joint-declaration-data-citation-principles-final
 [3]: https://www.force11.org/software-citation-principles
 [4]: http://support.crossref.org/hc/en-us/articles/214357426-Relationships-between-DOIs-and-other-objects
 [5]: http://support.crossref.org/hc/en-us/articles/214357426#aro
 [6]: mailto:Feedback@crossref.org
 [7]: /blog/crossref-the-art-of-cartography-an-open-map-for-scholarly-communications/