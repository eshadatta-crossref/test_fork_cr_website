---
archives:
- 2016
author: Ed Pentz
authors:
- Ed Pentz
categories:
- DataCite
- ORCID
- CDL
- Organization Identifier
- Request for Community Comment
- Community
- Collaboration
- Persistence
- Identifiers
date: 2016-10-31
title: 'The Organization Identifier Project: a way forward'
x-version: 0.0.0
---

The scholarly communications sector has built and adopted a series of open identifier and metadata infrastructure systems to great success.  Content identifiers (through Crossref and DataCite) and contributor identifiers (through ORCID) have become foundational infrastructure to the industry.  <!--more-->


{{% imagewrap right %}} <img src="/wp/blog/uploads/2016/10/Screenshot-2016-10-31-15.42.15-300x201.png" alt="Organization Identifier Project" width="300px" /> {{% /imagewrap %}}

But there still seems to be one piece of the infrastructure that is missing.  There is as yet no open, stakeholder-governed infrastructure for organization identifiers and associated metadata.

In order to understand this gap, Crossref, DataCite and ORCID have been collaborating to:

  * Explore the current landscape of organizational identifiers;
  * Collect the use-cases that would benefit our respective stakeholders in scholarly communications industry;
  * Identify those use-cases that can be more feasibly addressed in the near term; and
  * Explore how the three organizations can collaborate (with each other and with others) to practically address this key missing piece of scholarly infrastructure.

The result of this work is in three related papers being released by Crossref, DataCite and ORCID for community review and feedback. The three papers are:

  * Organization Identifier Project: A Way Forward ([PDF][1]; [GDoc][2])
  * Organization Identifier Provider Landscape ([PDF][3]; [GDoc][4])
  * Technical Considerations for an Organization Identifier Registry ([PDF][5]; [GDoc][6])

We invite the community to comment on these papers both via email (<a href="mailto:oi-project@orcid.org">oi-project@orcid.org</a>) and at</span> <a href="http://pidapalooza.org">PIDapalooza</a> on November 9th and 10th and at <a href="https://crossreflive16.sched.org">Crossref LIVE16</a> on November 1st and 2nd. To move The OI Project forward, we will be forming a Community Working Group with the goal of holding an initial meeting before the end of 2016. The Working Group’s main charge is to develop a plan to launch and sustain an open, independent, non-profit organization identifier registry to facilitate the disambiguation of researcher affiliations.

### Crossref Use Cases</span>

Crossref has also been discussing the needs of its members over the last year and there is value in focusing on the affiliation name ambiguity problem with research outputs and contributors. In terms of the metadata that Crossref collects, something that is missing has been affiliations for the authors of publications. Over the last couple of years, Crossref has been expanding what it collects - for example, funding and licensing data and ORCID iDs - and this enables a fuller picture of what we are calling the [article nexus](/blog/the-article-nexus-linking-publications-to-associated-research-outputs). In order to continue to fill out the metadata we collect - and for our members to use in their own systems and publications - we need an organization identifier.

Another use case for Crossref is identifying funders as part of collecting funder data to enable connecting funding sources with the published scholarly literature. In order to enable the reliable identification of funders in the Crossref system we created the Open Funder Registry that now has over 15,000 funders available as Open Data under a <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 waiver</a>. While this has been very successful, it is a very narrowly focused registry and is not suitable for a broad, community-run organization identifier registry that addresses the affiliation use case. In future, our goal will be to merge the Open Funder Registry into the identifier registry that the Organization Identifier Working Group will work on.

By working collaboratively we can define a pragmatic and cost-effective service that will meet a fundamental need of all scholarly communication stakeholders.

Geoffrey Bilder will be focusing <a href="https://crossreflive16.sched.com/event/8hqy/geoffrey-bilder-the-case-of-the-missing-leg">his talk at Crossref LIVE16</a> this week on this initiative, dubbed The OI Project. The talk is scheduled for 2pm UK time and will be live streamed along with the rest of that day’s program.

 [1]: https://doi.org/10.5438/2906
 [2]: https://docs.google.com/document/d/1PpWRBnlrU_X6TwYzQlB89w4FNXMLqieJv-RW0irNTsg/edit?usp=sharing
 [3]: https://doi.org/10.5438/4716
 [4]: https://docs.google.com/document/d/1lcKXWm9PxDvVWBxdlH7BVU7w8esnW0F_dppNiCJ9BW8/edit#
 [5]: https://doi.org/10.5438/7885
 [6]: https://docs.google.com/a/datacite.org/document/d/1Zj5sRRdnjKLjY81AbaeUdal3n6VuQgi1H66vRMaayiA/edit?usp=sharing