---
archives:
- 2016
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- DOIs
- Identifiers
- Linking
- Wikipedia
date: 2016-04-04
title: 'The Wikipedia Library: A Partnership of Wikipedia and Publishers to Enhance
  Research and Discovery'
x-version: 0.0.0
---

<span ><span >Back in 2014, Geoffrey Bilder blogged about the kick-off of </span><a href="/blog/citation-needed/"><span >an initiative between Crossref and Wikimedia</span></a><span > to better integrate scholarly literature into the world’s largest knowledge space, Wikipedia. Since then, Crossref has been working to coordinate activities with Wikimedia: Joe Wass has worked with them to create </span><a href="https://live.eventdata.crossref.org/live.html"><span >a live stream of content being cited in Wikipedia</span></a><span >; and we’re including Wikipedia in </span><a href="/blog/event-data-open-for-your-interpretation/"><span >Event Data</span></a><span >, a new service to launch later this year. In that time, we’ve also seen Wikipedia importance grow in terms of the volume of DOI referrals.</span></span><figure id="attachment_1412"  class="wp-caption alignright">

<a href="/wp/blog/uploads/2016/03/Stinson_Alex_June_2015_2.jpg" rel="attachment wp-att-1412"><img class="wp-image-1412 size-medium" src="/wp/blog/uploads/2016/03/Stinson_Alex_June_2015_2-300x200.jpg" alt="Alex Stinson, Project Manager for the Wikipedia Library, and our guest blogger! This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license (Source: Myleen Hollero Photography) " width="300" height="200" srcset="/wp/blog/uploads/2016/03/Stinson_Alex_June_2015_2-300x200.jpg 300w, /wp/blog/uploads/2016/03/Stinson_Alex_June_2015_2-768x512.jpg 768w, /wp/blog/uploads/2016/03/Stinson_Alex_June_2015_2.jpg 800w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">Alex Stinson, Project Manager for the Wikipedia Library, and our guest blogger! This file is licensed under the [Creative Commons Attribution-Share Alike 3.0 Unported license][1] (Source: Myleen Hollero Photography)</figcaption></figure>

_<span ><span >Alex Stinson, Project Manager for the Wikipedia Library, and guest blogger! This file is licensed under the <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.en">Creative Commons Attribution-Share Alike 3.0 Unported license</a> (Source: Myleen Hollero Photography)</span></span>_

<span ><span >How can we keep this momentum going and continue to improve the way we link Wikipedia articles with the formal literature? We invited Alex Stinson, a project manager at </span><a href="https://en.wikipedia.org/wiki/Wikipedia:The_Wikipedia_Library"><span >The Wikipedia Library </span></a><span >(and one of our first guest bloggers) to explain more:</span></span>

<span ><span >Wikipedia provides the most public gateway to academic and scholarly research. With millions of citations to academic as well as non-academic but reliable sources, like those produced by newspapers, its ecosystem of 5 million English Wikipedia articles and 35 million articles in </span><a href="https://www.wikipedia.org/"><span >hundreds of languages</span></a><span > provides the first stop for researchers in both scholarly and informal research situations. The practice of “checking Wikipedia” has become ubiquitous in a number of fields; for example, Wikipedia is the most visited </span><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4376174/"><span >source of medical information online</span></a><span >, even providing the first stop for many </span><a href="http://www.ncbi.nlm.nih.gov/pubmed/23137251"><span >medical students and medical practitioners when looking for medical literature</span></a><span >.</span></span>

<span ><span ><span >The Wikipedia Library prog</span>ram helps Wikipedia’s volunteer editors access and use the best sources in their research and citations.  Through </span><a href="https://en.wikipedia.org/wiki/Wikipedia:TWL/Publishers"><span >partnerships</span></a><span > with over fifty leading publishers and aggregators, like JSTOR, Project Muse, Elsevier, Newspapers.com, Highbeam, Oxford University Press and others, we have been able to give over 3000 of our most prolific volunteers access to over 5500 accounts. These are clear, win-win relationships where Wikipedia editors get to use these databases to improve Wikipedia, while in turn linking to authoritative resources and enhancing their discovery. </span></span>

<span >JSTOR has been working with us since 2012, providing over 500 accounts to our editors. Kristen Garlock at JSTOR writes: </span>

> <span ><span >&#8220;We’re very happy to collaborate with the Wikipedia Library to provide JSTOR access to Wikipedia editors. Supporting the initiative to increase editor access to scholarly resources and improve the quality of information and sources on Wikipedia has the potential to help all Wikipedia readers. In addition to providing more discoverability for our institutional subscribers, introducing new audiences to the scholarship on JSTOR them discover access opportunities like our </span><a href="http://about.jstor.org/rr"><span >Register & Read program</span></a><span >.&#8221;</span></span>

<span >There are strong signals that Wikipedia’s role in the citation ecosystem helps ensure the best materials reach the public through its over 400 million monthly readers: </span>

<li >
  <span ><span >The latest estimates by Crossref show that Wikipedia has </span><a href="https://youtu.be/8qO3BYDN67k?t=11m15s"><span >risen from the 8th most prolific referrer to DOIs to the 5th</span></a><span >. </span></span>
</li>
<li >
  <span >Two of our access partners have found that around half of the referrals arriving from Wikipedia were able to authenticate into their subscription resources, suggesting that a large portion of our readers can take advantage of subscriptions provided by scholarly institutions. </span>
</li>
<li >
  <span ><span >Wikipedia is highly influential in the open access ecosystem as well, with a recent study showing </span><a href="http://arxiv.org/abs/1506.07608"><span >higher citation rates for OA materials </span></a><span >than those behind a paywall.</span></span>
</li>
<li >
  <span ><a href="https://en.wikipedia.org/wiki/Altmetrics"><span >Altmetrics</span></a><span > tools (such as Altmetric.com, ImpactStory or Plum Analytics) are recognizing Wikipedia’s importance by including Wikipedia citations </span><a href="http://www.altmetric.com/blog/new-source-alert-wikipedia/"><span >in their impact metrics</span></a><span >. </span></span>
</li>

<span ><span >Despite these advances, we think this is only the beginning of Wikipedia’s impact on the landscape of scholarly research and discovery. Wikipedia can become a highly integrated research platform within the broader research ecosystem, where the best scholarship is summarized and discoverable-where Wikipedia effectively becomes the </span><a href="https://meta.wikimedia.org/wiki/Wikipedia_as_the_front_matter_to_all_research"><span >front matter to all research</span></a><span >.</span></span>

<span ><span >However, there are some clear barriers to fulfilling this vision. Currently, most citations on Wikipedia are stored in free-text and not readily available in machine-readable formats; our community is working to fix this. Wikipedia also has major systematic gaps in topics where either we lack volunteer interest or Wikipedia reflects </span><a href="https://en.wikipedia.org/wiki/Wikipedia:Systemic_bias"><span >larger systemic biases within society or scholarship</span></a><span >.We need the help of volunteers, experts, industry partners, and information technologists to grow Wikipedia’s collection of citations, especially around key missing areas, and to transform existing citations into structured formats. </span></span>

<span ><a href="https://www.wikidata.org/"><i><span >WikiData</span></i></a><span >, Wikipedia’s sister project which crowdsources structured metadata, offers an excellent opportunity for improving the impact of Wikipedia in research.  Having Wikipedia citations </span><a href="https://www.wikidata.org/wiki/Wikidata:WikiProject_Source_MetaData"><span >stored in this structured ecosystem</span></a><span >, connecting metadata with semantic meaning, would allow the citations in Wikipedia to become the backbone for discovery tools which emphasize the hand-curated interrelationships between authoritative sources and the knowledge collected by Wikipedia and Wikidata editors.</span></span>

<span >We need more collaborators to realize the full vision of Wikipedia supporting research in the most effective ways:</span>

<li >
  <span ><span >We need help from publishers with subscription databases, to help us give our editors access to the databases through </span><a href="https://en.wikipedia.org/wiki/Wikipedia:The_Wikipedia_Library/Publishers"><span >The Wikipedia Library’s access partnership program</span></a><span >. These high-quality source materials allow our editors to expose that research in a number of languages and for millions of readers. </span></span>
</li>
<li >
  <span ><span >We need help from the open access community, to figure out how to better support increased citation and strategic use of open access materials within Wikipedia and other Wikimedia projects. </span><a href="http://blog.wikimedia.org/2015/09/16/open-access-in-a-closed-world/"><span >Our community has some ideas, but we need your input and collaboration</span></a><span >.</span></span>
</li>
<li >
  <span ><span >We need your expertise to build our structured metadata ecosystem, by helping Wikidata </span><a href="https://www.wikidata.org/wiki/Wikidata:WikiProject_Source_MetaData"><span >map and collect citation data</span></a><span >.</span></span>
</li>
<li >
  <span ><span >We need the larger research community to promote Wikipedia as a scholarly communications tool and make contributing to Wikipedia an important part of </span><a href="https://en.wikipedia.org/wiki/Wikipedia:Research_help/Scholars_and_experts"><span >the social responsibility of experts</span></a><span >. Wider citation of sources in Wikipedia ensures widespread discovery and dissemination of that research.</span></span>
</li>

<span ><span >If you think you can help, we invite you to contact us at </span><a href="mailto:wikipedialibrary@wikimedia.org"><span >wikipedialibrary@wikimedia.org</span></a><span > or via </span><a href="https://twitter.com/wikilibrary"><span >Twitter @WikiLibrary</span></a><span >. </span></span>

&nbsp;

 [1]: https://creativecommons.org/licenses/by-sa/3.0/deed.en