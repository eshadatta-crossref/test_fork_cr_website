---
archives:
- 2016
author: Kirsty Meddings
authors:
- Kirsty Meddings
categories:
- Clinical Trials
- Crossmark
- Identifiers
- Linking
- Metadata
date: 2016-06-21
title: Linked Clinical Trials initiative gathers momentum
x-version: 0.0.0
---

<span >We now have <a href="/blog/linked-clinical-trials-are-here/">linked clinical trials</a> deposits coming in from five publishers: BioMedCentral, BMJ, Elsevier, National Institute for Health Research and PLOS. It’s still a relatively small pool of metadata - <a href="https://api.crossref.org/v1/works?filter=has-clinical-trial-number:true">around 4000 DOIs</a> with associated clinical trial numbers - but we’re delighted to see that &#8220;threads&#8221; of publications are already starting to form.</span>

{{% imagewrap right %}} <img src="/images/blog/clinical-trials-blog.png" alt="An exemplary image" width="300px" /> {{% /imagewrap %}}

<span >If you look at <a href="https://doi.org/10.1016/s0140-6736(14)61836-5">this article in <em>The Lancet</em></a> and click on the Crossmark button you will see that in the Clinical Trials section there are links to three other articles reporting on the same trial: two from the <em>American Heart Journal</em> and one from BMJ’s <em>Heart</em>. Readers can navigate between these four articles in three separate journals using the Crossmark functionality- a new set of links and routes for discovery have appeared.</span>

<span >In another example, three articles from <em><a href="https://doi.org/10.1371/journal.pone.0017554">PLOS ONE</a> </em>are threaded together around a trial for the treatment of Type 1 diabetes. And here another PLOS journal, <a href="https://doi.org/10.1371/journal.pone.0017554"><em>Neglected Tropical Diseases</em></a> links through to a <em>PLOS ONE</em> article about the same trial.</span>

<span >If you publish in the health sciences please do consider joining this exciting initiative so that we can expand these threads and build up the metadata. Read the <a href="http://crossmarksupport.crossref.org/linked-clinical-trials/">tech specs here</a> or drop me an email if you have questions.</span>