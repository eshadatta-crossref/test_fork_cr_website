---
archives:
- 2016
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- APIs
- ORCID
- Event Data
- Interoperability
- API Case Study
date: 2016-09-08
title: Using the Crossref Metadata API. Part 2 (with PaperHive)
x-version: 0.0.0
---

<span >We first met the team from </span>[<span >PaperHive</span>][1] <span >at SSP in June, pointed them in the direction of the </span>[<span >Crossref Metadata API</span>][2] <span >and let things progress from there. That’s the nice thing about having an API - because it’s a common and easy way for developers to access and use metadata, it makes it possible to use with lots of diverse systems and services.</span>

<span >So how are things going? Alexander Naydenov, PaperHive’s Co-founder gives us an update on how they’re working with the Crossref metadata: </span>

<!--more-->

<span ><b>PaperHive</b></span>

<span >PaperHive is a web-platform for collaborative reading and a cross­-publisher layer of   interaction on top of research documents. It lets researchers communicate in published documents in a productive and time-saving way. PaperHive thus puts academic literature, which is integrated with the platform, in the limelight and increases content usage and reader engagement.</span>

<span ><a href="https://paperhive.org/"><img class="size-medium wp-image-2051 alignright" src="/wp/blog/uploads/2016/09/Logo-PaperHive-300x59.png" alt="Logo PaperHive" width="300" height="59" srcset="/wp/blog/uploads/2016/09/Logo-PaperHive-300x59.png 300w, /wp/blog/uploads/2016/09/Logo-PaperHive-768x151.png 768w, /wp/blog/uploads/2016/09/Logo-PaperHive.png 800w" sizes="(max-width: 300px) 85vw, 300px" /></a>Transforming reading into a process of collaboration gives researchers a reason to return to the content and discover new enrichments they can benefit from. Functionality like hiving, deep linking, and the PaperHive browser extension embeds communication in the researcher’s workflow. PaperHive is free to use!</span>

<span ><b>How is the Crossref API used within PaperHive?</b></span>

<span >PaperHive extends the concept of a living document and offers an innovative way of displaying content without hosting it. Instead, academic documents are dynamically pulled from the publisher’s servers thus ensuring compliance with content licensing. It enables readers to stay in touch with the articles of interest beyond just saving them in an offline folder.</span>

<span >Crossref is the common ground on which third party companies and initiatives can build valuable services for publishers and researchers. It facilitates the integration of content into PaperHive by providing the metadata of articles and books from numerous publishers independent of the technology behind their content platforms. Moreover, if the publishers provide ORCID identifiers of authors in the Crossref metadata, researchers can immediately interact with the readers of their works.</span>

<span ><b>What are the future plans for PaperHive?</b></span>

<span >In addition to integrating further publishers’ content and extending PaperHive’s feature set for readers, we also plan to extend our partnerships with other technology providers.</span>

<span >As far as our cooperation with Crossref is concerned, we are looking forward to the implementation of the</span> [<span >Crossref Event Data API</span>][3]<span >.</span>

<span ><b>What else would you like to see in Crossref metadata?</b></span>

<span >-</span> <span >      </span><span >The quality of the existing metadata should be improved significantly. We noticed that important fields such as author or title are missing in the metadata of many documents. PaperHive ignores articles and books with incomplete metadata because it impairs the user experience. Publishers, authors and readers can only benefit from the wider and more active usage of content, so we hope that more publishers will improve the data their provide Crossref with.</span>

<span >-</span> <span >      </span><span >Since researchers are working with full texts on PaperHive, it would be great if  links to the full text are provided in the metadata of all articles and books. The metadata should also contain information about the format of the full text (e.g., PDF, EPUB, HTML).</span>

<span ><b>Thanks Alex!</b></span>

<span ><b>Just getting started with the API or what to know more? Get in touch via <a href="mailto:feedback@crossref.org">feedback@crossref.org</a> and pass on your questions and comments.</b></span>

&nbsp;

 [1]: https://paperhive.org/
 [2]: https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md
 [3]: http://eventdata.crossref.org/