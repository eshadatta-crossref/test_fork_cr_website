---
archives:
- 2016
author: Anna Tolwinska
authors:
- Anna Tolwinska
categories:
- DOIs
- Identifiers
- Linking
- Metadata
- Persistence
date: 2016-04-25
title: Getting Started with Crossref DOIs, courtesy of Scholastica
x-version: 0.0.0
---

I had a great chat with [Danielle Padula][1] of [Scholastica][2], a journals _platform with an integrated peer-review process that was founded in 2011.  We talked about how journals_ get started with Crossref, and she turned our conversation into a blog post that describes the steps to begin registering content and depositing metadata with us.  Since the result is a really useful description of our new member on-boarding process, I want to share it with you here as well.  As always, comments and questions are welcome here, at <member@Crossref.org>, and [@CrossrefOrg][3].  - Anna_

The internet is in a constant state of change, with new content being added to the web by the minute and old content sometimes getting moved around. While the benefit of publishing scholarly outputs online is that it’s possible to update them at any moment, moving or modifying content can also &#8230;

Read more at: <https://blog.scholasticahq.com/post/getting-started-with-dois-at-your-journal-interview-with-anna-tolwinska-crossref/>

 [1]: https://twitter.com/djpadula5
 [2]: https://scholasticahq.com/
 [3]: http://twitter.com/crossreforg