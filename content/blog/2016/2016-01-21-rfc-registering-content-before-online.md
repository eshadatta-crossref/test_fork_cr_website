---
archives:
- 2016
author: Ginny Hendricks
authors:
- Ginny Hendricks
categories:
- Accepted Manuscripts
- Content Registration
- DOIs
- Publishing
- Request for Community Comment
date: 2016-01-21
slug: /rfc-registering-content-before-online
title: 'Request for Community Comment: registering content before online availability'
x-version: 0.0.0
---

<span >Crossref is proposing a process to support the registration of content&#8212;including DOIs and other metadata&#8212;prior to that content being made available, or published, online. We’ve drafted a paper providing background on the reasons we want to support this and highlighting the use cases. One of the main needs is in journal publishing to support registration of Accepted Manuscripts immediately on or shortly after acceptance, and dealing with press embargoes.</span><figure id="attachment_1303"  class="wp-caption alignright">

<a href="http://outreach.crossref.org/acton/attachment/16781/f-000b/1/-/-/-/-/RFC4Feb-RegisterContentBeforeOnline.pdf" target="_blank" rel="attachment wp-att-1292"><img class="wp-image-1303 size-medium" src="/wp/blog/uploads/2016/01/Screenshot-2016-01-20-00.00.24-225x300.png" alt="Proposal doc for community comment" width="225" height="300" srcset="/wp/blog/uploads/2016/01/Screenshot-2016-01-20-00.00.24-225x300.png 225w, /wp/blog/uploads/2016/01/Screenshot-2016-01-20-00.00.24.png 754w" sizes="(max-width: 225px) 85vw, 225px" /></a><figcaption class="wp-caption-text"><span >Proposal doc for community comment</span></figcaption></figure>

_**<span >We request community comment on the </span>**__**<span >proposed approach as outlined <a href="http://outreach.crossref.org/acton/attachment/16781/f-000b/1/-/-/-/-/RFC4Feb-RegisterContentBeforeOnline.pdf">in this report</a>.</span>**_  

<span ><strong>Some examples of what we’d like to know:</strong></span>

  * <span >Are you aware of the issues outlined in this proposal?</span>
  * <span >Are you aware of the funder and institutional requirements for authors to take action on acceptance of manuscripts for publication in journals?</span>
  * <span >Do you think the proposed solution and workflows are reasonable?</span>
  * <span >Are you likely to update your workflow to register content early?</span>
  * <span >If you are likely to update your workflow, how long do you estimate it will take?</span>
  * <span >Any other general comments, questions or feedback on anything raised in this document. </span>

<span ><b><i>Please send comments, feedback and questions to me, Ginny, at <a href="mailto:feedback@crossref.org">feedback@crossref.org</a>. The deadline for comments is February 4th. Thanks!</i></b></span>