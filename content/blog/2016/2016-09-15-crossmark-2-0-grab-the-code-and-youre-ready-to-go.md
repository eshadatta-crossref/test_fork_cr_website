---
archives:
- 2016
author: Kirsty Meddings
authors:
- Kirsty Meddings
categories:
- Crossmark
date: 2016-09-15
title: Crossmark 2.0 - grab the code and you’re ready to go!
x-version: 0.0.0
---

<span ><span >On September 1st we completed the final stage of the Crossmark v2.0 release and sent an email to all participating publishers containing instructions for upgrading. The first phase of v2.0 happened when we changed the design and layout of the Crossmark box back in May of this year. That allowed us to better display the growing set of additional metadata that our members are depositing, and saw the introduction of the </span><a href="/blog/linked-clinical-trials-are-here/"><span >Linked Clinical Trials</span></a><span > feature.</span></span>

<span ><span ><img class="size-full wp-image-2120 alignright" src="/wp/blog/uploads/2016/09/crossmark_stack.png" alt="crossmark_stack" width="277" height="187" /></span></span>

<span ><span >Now all publishers have the opportunity to </span><a href="http://crossmarksupport.crossref.org/"><span >complete the upgrade</span></a><span > by simply replacing the Crossmark button and the piece of code that calls the box. The new button designs are, we think, a much better fit for most websites, and are designed to look more like a button than a flat logo. The new buttons are also </span><a href="https://assets.crossref.org/"><span >available<br /> as .eps</span></a><span > files for placement in PDFs.</span></span>  

&nbsp;<figure id="attachment_2125"  class="wp-caption alignleft">

<img class="size-medium wp-image-2125" src="/wp/blog/uploads/2016/09/Screenshot_20160915-154051-300x182.jpg" alt="Crossmark box on a mobile phone" width="300" height="182" srcset="/wp/blog/uploads/2016/09/Screenshot_20160915-154051-300x182.jpg 300w, /wp/blog/uploads/2016/09/Screenshot_20160915-154051-768x467.jpg 768w, /wp/blog/uploads/2016/09/Screenshot_20160915-154051-1024x623.jpg 1024w, /wp/blog/uploads/2016/09/Screenshot_20160915-154051.jpg 1184w" sizes="(max-width: 300px) 85vw, 300px" /><figcaption class="wp-caption-text">Crossmark box on a mobile phone</figcaption></figure>

> <span >Most importantly, switching to 2.0 makes the Crossmark box responsive for better display on mobile devices.</span>

&nbsp;

<span ><span >Just two weeks after the code release a number of publishers have already upgraded and are running Crossmark 2.0 on their content. Congrats to the </span><a href="http://www.dx.doi.org/10.11604/pamj.2016.24.338.8455"><span >Pan African Medical Journal</span></a><span > who were the first member to upgrade just a couple of days after the release.  Of course we realise that many members will need time to schedule the upgrade, and while we are keen to see as many early adopters as possible, we will support version 1.5 of Crossmark through to the end of March 2017.</span></span>

<span ><span >If your content is running Crossmark 2.0 we would love to see it. </span><a href="mailto:crossmark_info@crossref.org"><span >Drop us a line</span></a><span > or put a link in the comments below.</span></span>