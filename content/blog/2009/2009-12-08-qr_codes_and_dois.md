---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Linking
date: 2009-12-08
title: QR Codes and DOIs
x-version: 0.0.0
---

Inspired by [Google’s recent promotion of QR Codes][1], I thought it might be fun to experiment with encoding a Crossref DOI and a bit of metadata into one of the critters. I’ve put a [short write-up of the experiment][2] on the [Crossref Labs][3] site, which includes a demonstration of how you can generate a [QR Code][4] for any given Crossref DOI. Put them on postcards and send them to your friends for the holidays. Tattoo them on your pets. The possibilities are endless.

 [1]: http://www.techcrunch.com/2009/12/06/google-local-maps-qr-code/
 [2]: http://labs.crossref.org/qr-code-generator/
 [3]: http://labs.crossref.org
 [4]: http://en.wikipedia.org/wiki/QR_Code