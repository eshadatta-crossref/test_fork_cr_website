---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Linking
date: 2009-02-12
title: DOIs in an iPhone application
x-version: 0.0.0
---

Very cool to see Alexander Griekspoor releasing an iPhone version of his award-winning Papers application. A while ago Alex intigrated DOI metadata lookup into the Mac version of papers and now I can get a silly thrill from seeing Crossref DOIs integrated in an iPhone app. Alex has just posted [a preview video of the iPhone application][1] and it includes a cameo appearance by a DOI. Yay.

 [1]: https://web.archive.org/web/20100317112846/http://mekentosj.com/papers/iphone/