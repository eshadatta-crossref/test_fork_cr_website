---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- iPub
date: 2009-09-07
slug: prc-report-and-ipub-revisited
title: PRC Report and &#8220;iPub&#8221; revisited
x-version: 0.0.0
---

OK, so this has nothing to do with any Crossref projects- but there is an interesting [new PRC report][1] out by [Mark Ware][2] in which he explores how SMEs (small/medium-sized enterprises) make use of scholarly articles and whether the scholarly publishing industry is doing anything to make their lives easier. This is a topic that is close to my heart. For the past few years I’ve been saying ([most recently at SSP09][3]) that I think scholarly publishers are much too quick to dismiss the possibility of creating an iTunes-like service for scholarly publications (aka &#8220;iPub&#8221;). The report certainly seems to indicate that there is an important audience that would benefit from such a service (SMEs) and even goes so far as to cite my occasional rants on the subject. The summary of my iPub argument has been that:

  * A very large percentage of the web visits that hit publishers web sites come from sources that are unrecognised. That is, they don’t come from a subscribing institution and they don’t seem to come from a registered user or anybody who has visited the site previously. For many publishers the level of such unrecognised visitors can amount to over 90% of all the traffic that hits their sites. Most industries would look at this percentage and work hard to figure out how to monetize some of it. Our industry seems to treat it like “noise”, reasoning that only people in recognised academic and professional institutions are going to desire or understand the content on scholarly journal sites.
  * [Evidence from the NSF][4] shows that significantly more than 50% of US students who graduate with an S&#038;E degree end up employed outside of directly S&#038;E related fields. This represents a large percentage of potential consumers of scholarly and professional publications who are not part of a recognised academic or professional institution. SMEs anybody?
  * These potential consumers are faced with a bewildering variety of sources for their content. They have to deal with multiple publisher sites with different interfaces and different PPV checkout procedures. And they have to navigate all this without the aid of library finding tools or the professional researcher’s understanding the scholarly journal environment. It is no wonder that they give up hope once they land on our abstract pages and face the gauntlet of another PPV checkout system.

It seems to me that the industry could provide a single interface and PPV shopping cart interface targeted at allowing people who work outside of traditional subscribing institutions to easily purchase individual article downloads from scholarly publishers. The system would be modelled at least in part by Apple’s iTunes, a system that has been lauded (and denounced) for revolutionising the way in which consumers buy music online. The chief virtues of the iTunes system are often cited as being:

  * It contains a critical mass of content
  * It provides a simple and consistent user interface
  * It has a simple and inexpensive pricing model
  * It disaggregated content (per song purchases)
  * It interfaced transparently with the iPod.

A scholarly publishing &#8220;iPub&#8221; system could seek to emulate many of these strengths but not all. Clearly such a system could not impose uniform pricing or dictate pricing, as that would be anti-competitive. The PRC report makes this same point.

Some, including the PRC report, also claim that the publishing industry has no equivalent of the &#8220;iPod&#8221; and that this would be a weakness of the system. I don’t agree with this- I think that the &#8220;iPod&#8221; in this case is currently called &#8220;paper.&#8221; In the future we will almost certainly migrate to some iPod/Kindle-like device, but as far as fulfilling most of the iPod’s functionality (portable rendering of the content) right now, I suspect paper fits the bill.

Finally, there is a another oft-expressed concern that such a system might confuse channels for existing audiences and that librarians in particular would be very hostile to such a system. The truth is, I don’t know how librarians would react to such a system. The few I’ve mentioned it to certainly seemed amenable to the idea. Maybe this is where the PRC should do some follow-up research?

In any case, it seems to me that there is potentially much to be gained by simply providing an easy PPV experience where users don’t have to register with multiple sites and cope with multiple shopping cart applications. Publishers can’t seriously think that they gain competitive advantage through their shopping carts? If not, then why not standardise on a uniform interface that is easily purchased from? Perhaps it doesn’t have to look like iTunes but can instead look like PayPal (PayPub?). Providing a simple mechanism like this might enable the industry to meet the needs of important and often overlooked audiences. I keep wondering if [CCC][5] could help publishers do something here?

 [1]: http://www.publishingresearch.net/SMEaccess.htm
 [2]: http://mrkwr.wordpress.com/mark-ware-consulting/
 [3]: http://www.slideshare.net/Crossref/itunes-for-scholarly-publishing
 [4]: http://www.nsf.gov/statistics/srvyrecentgrads/
 [5]: http://www.copyright.com/