---
archives:
- 2009
author: thammond
authors:
- thammond
categories:
- RSS
date: 2009-02-19
title: Real PRISM in the RSS Wilds
x-version: 0.0.0
---

Alf Eaton just [posted][1] a real nice analysis of [ticTOCs][2] RSS feeds. Good to see that almost half of the feeds (46%) are now in RDF and that fully a third (34%) are using PRISM metadata to disclose bibliographic fields.
  
The one downside from a Crossref point of view is that these feeds are still using the old PRISM version (1.2) and not the new version (2.0) which was released a year ago and blogged [here][3]. That version supports the elements **prism:doi** for the bare DOI, as well as **prism:url** for the DOI proxy server URL.
  
There are still some improvements to be made in serving up these feeds (as Alf’s analysis shows for content type), but overall things are looking pretty good. 🙂

 [1]: http://hublog.hubmed.org/archives/001818.html
 [2]: http://www.tictocs.ac.uk/
 [3]: /blog/prismdoi/