---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- ORCID
date: 2009-03-11
title: Researcher Identification Primer
x-version: 0.0.0
---

[Discussions around &#8220;contributor Ids&#8221;][1] (aka &#8220;Author ID, Researcher ID, etc.) seem to be becoming quite popular. In [the interview][2] that I pointed to in my [last post][3], I mentioned that Crossref has been talking with a group of researchers who were very interested in creating some sort of authenticated contributor ID as a mechanism for controlling who gets trusted access to sensitive genome-wide aggregate genotype data.

Well, I’m delighted to say that said group of researchers(at the [GEN2PHEN][4] project) have created a &#8220;[Researcher Identification Primer][5]&#8221; website in which they outline the many use-cases and issues around creating a mechanism for unambiguously identifying and/or authenticating researchers. This looks like a great resource and I expect it will serve as a useful focus for further discussion around the issue.

 [1]: https://cameronneylon.net/blog/a-specialist-openid-service-to-provide-unique-researcher-ids/
 [2]: https://web.archive.org/web/20091225201433/http://network.nature.com/people/mfenner/blog/2009/02/17/interview-with-geoffrey-bilder
 [3]: /blog/an-interview-about-author-ids/
 [4]: http://www.gen2phen.org/
 [5]: https://web.archive.org/web/20090418151033/http://www.gen2phen.org/researcher-identification/researcher-identification-primer