---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- ORCID
date: 2009-04-27
title: What do people want from an author identifier?
x-version: 0.0.0
---

<span ><a href="https://web.archive.org/web/20090219202623/http://network.nature.com/people/mfenner/profile">Martin Fenner</a> continues his interest in the subject of author identifiers. He recently <a href="https://web.archive.org/web/20090417062326/http://network.nature.com/people/mfenner/blog/2009/04/13/a-few-questions-about-author-identifiers">posted an online poll</a> asking people some specific questions about how they would like to see an author identifier implemented.<sup>*</sup></span>

 <span ><a href="https://web.archive.org/web/20090429164110/http://network.nature.com/people/mfenner/blog/2009/04/26/a-few-questions-about-author-identifiers-the-answers">The results of the poll</a> are in and, though the sample was very small, the results are interesting. The responses are both gratifying -there seems to be a general belief that Crossref has a roll to play here- and perplexing -most think the identifier needs to identify other &#8220;contributors&#8221; to the scholarly communications process- yet there seems to be a preference for the moniker &#8220;digital author identifier&#8221;. This latter preference is certainly a surprise to us as we had been focusing our efforts on identifying analog authors. The only &#8220;digital authors&#8221; I know of are <a href="http://pdos.csail.mit.edu/scigen/">this one at at MIT</a> and possibly <a href="http://news.bbc.co.uk/2/hi/science/nature/7979113.stm">this one at Aberystwyth University.</a> 😉</span>

 <span >Anyway, There are some <a href="https://www.facebook.com/e/bb174794-519c-02a7-8a00-9283013298d8/A-few-questions-about-author-identifiers-the">additional reactions</a> to Martin’s poll on FriendFeed.</span>

 <span >Finally, I should have blogged about this earlier, but the March issue of <em>Science</em> included a summary of the initiatives and discussions surrounding the creation of an industry &#8220;author identifier&#8221; in an article titled “Are You Ready to Become a Number” (<a href="http://dx.doi.org/10.1126/science.323.5922.1662">http://dx.doi.org/10.1126/science.323.5922.1662</a>).</span>

 <span >In pointing people at this, I feel like I must make a clarification to the article. In short, I don’t think any of our members would &#8220;force&#8221; anybody to use an author identifier whether it came from Crossref or from anybody else. Though it is likely that in the interview I used the terms &#8220;carrot&#8221; and &#8220;stick&#8221;, in truth publisher’s would, instead of &#8220;a stick&#8221;, at most wield a <a href="http://en.wikipedia.org/wiki/Nerf">Nerf</a> bat. Having said that, the essential point remains- even if most major publishers *strongly* encouraged all of their authors to use the system, it would take several years before the system had a critical mass of data.</span>

 <span ><sup>*</sup>Note that I deliberately didn’t point CrossTech readers at this poll as it was being conducted because I thought doing so might introduce a Crossref bias.</span>