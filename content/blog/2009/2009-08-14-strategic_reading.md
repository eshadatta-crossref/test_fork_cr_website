---
archives:
- 2009
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Publishing
date: 2009-08-14
title: Strategic Reading
x-version: 0.0.0
---

[Allen Renear][1] and [Carole Palmer][2] have just published an article titled &#8220;Strategic Reading, Ontologies, and the Future of Scientific Publishing&#8221; in the current issue of _Science_ (<http://dx.doi.org/10.1126/science.1157784>). I’m particularly happy to see this paper published because I actually got to witness the genesis of these ideas in my living room back in 2006. Since then, Allen and Carole’s ideas have profoundly influenced my thinking on the application of technology to scholarly communication.

Those who have seen me speak at conferences recently will have heard me do an awful lot of ranting about the how publishers and librarians need to help researchers practice the time-honored art of &#8220;reading avoidance&#8221; (or as Renear and Palmer politely put it- &#8220;strategic reading&#8221;). I even managed to squeeze this rant into a [recent interview][3] I did with Wiley-Blackwell.

The essence of my argument has been that our industries need not be bamboozled by the technical jargon and messianic hand-waving that typically accompany discussions of new technology trends like &#8220;web 2.0&#8221;, &#8220;text-mining&#8221;, &#8220;the semantic web&#8221;, &#8220;micro-blogging&#8221;, etc. This is because there is a fairly simple way for us to understand the relative import (or lack thereof) of new technologies to scholarly communication and that is to ask the following question:

&#8220;Can the application of this technology in the realm of scholarly communication help researchers to read less?&#8221;

If the answer is &#8220;yes&#8221;, then you’d better pay very close attention to it.

In fact, I’d go so far as to say the history of scholarly publishing can be characterized by the successful adoption of conventions and tools that help researchers read strategically.

Now I have something to cite when I rant.

Anyway, congratulations to Allen &#038; Carole.

 [1]: https://web.archive.org/web/20171002013342/http://people.ischool.illinois.edu/~renear/renearcv.html
 [2]: https://web.archive.org/web/20090901021407/http://people.lis.illinois.edu/~clpalmer
 [3]: http://www.wiley.com/bw/journalnews/newsitem.asp?release=2262