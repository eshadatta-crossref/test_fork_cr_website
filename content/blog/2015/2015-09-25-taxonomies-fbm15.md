---
archives:
- 2015
author: Ginny Hendricks
authors:
- Ginny Hendricks
categories:
- Collaboration
- Meetings
- Standards
- Taxonomies
date: 2015-09-25
slug: /taxonomies-meet-up-at-fbf15
title: 'Taxonomies Meet-up at #FBM15'
x-version: 0.0.0
---

The [Taxonomies Interest Group][1] would like to invite Crossref members to an informal drop-in at the Frankfurt Book Fair:

**4-5pm on Wednesday 14th October at the TEMIS booth H76**

<!--more-->

The group would like to discuss how different publishers use their taxonomies for content enrichment and to explore the role that the Crossref interest group can play in promoting industry collaboration and emerging standards. TEMIS have kindly offered to host the event at their booth and provide refreshments: Please come by from 4pm at Booth H76.

Graham McCann from IOP Publishing and Christian Kohl from De Gruyter will be coordinating the event. For background information on the work the group is doing, take a look at this [webinar recording from March 2015][2].

 [1]: https://www.crossref.org/labs/
 [2]: https://www.crossref.org/webinars