---
archives:
- 2015
author: Jennifer Lin
authors:
- Jennifer Lin
categories:
- ORCID
- Collaboration
- Data
- Event Data
- Research Funders
- Identifiers
- Interoperability
- Linked Data
- Member Briefing
- News Release
- Wikipedia
date: 2015-09-15
slug: /det-poised-for-launch/
title: 'DOI Event Tracker (DET): Pilot progresses and is poised for launch'
x-version: 0.0.0
---

<span ><a href="/wp/blog/uploads/2015/09/doi_tracker_graphic.001.jpg"><img class="alignright size-medium wp-image-700" src="/wp/blog/uploads/2015/09/doi_tracker_graphic.001-300x225.jpg" alt="" width="300" height="225" srcset="/wp/blog/uploads/2015/09/doi_tracker_graphic.001-300x225.jpg 300w, /wp/blog/uploads/2015/09/doi_tracker_graphic.001.jpg 1024w, /wp/blog/uploads/2015/09/doi_tracker_graphic.001-624x468.jpg 624w" sizes="(max-width: 300px) 85vw, 300px" /></a></span>

<span >Publishers, researchers, funders, institutions and technology providers are all interested in better understanding how scholarly research is used. Scholarly content has always been discussed by scholars outside the formal literature and by others beyond the academic community. We need a way to monitor and distribute this valuable information.<!--more--></span>

## <span >The Crossref DOI Event Tracker (DET)</span>

<span >To meet this need, Crossref will be introducing a new service that tracks activity surrounding a research work from potentially any web source where an event is associated with a DOI. Following a successful <a href="/blog/crossrefs-doi-event-tracker-pilot/">pilot run</a> started Spring 2014, the service has been approved to move toward production and is expected to launch in 2016. Any party wishing to join this phase is welcome to contact Jennifer Lin. The DOI Event Tracker (DET) registers a wide variety of events such as bookmarks, comments, social shares, citations, and links to other research entities, from a growing list of online sources. DET aggregates them, and stores and delivers the data in many ways.</span>

<span ><strong>Open, portable, and licensed for maximum reuse</strong></span>

 <span >Crossref has long served as the citation linking and metadata infrastructure provider for scholarly communication; the new DOI Event Tracker is a natural next step, providing a practical solution as a resource for the whole community. The tracker offers the following features:</span>

  * <span >Data on event activity across a common pool of online channels.</span>
  * <span >Near real-time alerting for select sources with push notifications to the system.</span>
  * <span >Cross-publisher monitoring to enable benchmarking and provide context to the data.</span>
  * <span >Common format for normalizing data results across the diverse set of sources via modern REST API.</span>
  * <span >Secure and regularly refreshed backups of critical data for long term data preservation.</span>
  * <span >Transparency of data collection so as to ensure auditable, replicable, and trustworthy results.</span>
  * <span >Query-initiated retrieval or real-time alerts when an event of interest occurs.</span>
  * <span >CC-0 license for open and flexible propagation of data.</span>

<span >A number of platforms are already confirmed and more parties are welcomed at any stage. So far we have confirmation to track DOI events on the following platforms:</span>

<span >[table id=1 /]</span>

<span >This set of sources reflects our initial focus on parties willing to allow their data to be redistributed in the common pool. Efforts are underway to expand the source list to include <a href="http://twitter.com">Twitter</a> and <a href="https://www.mysciencework.com/">MyScienceWork</a>, among others. Publishers can also act as sources by publishing and distributing DOI event data via the DET when an event occurs on its platform (for example, when a PDF is downloaded, or when a comment mentions a DOI in a locally hosted discussion forum, etc.). This would make local DOI activity globally available to funders, researchers, institutions, etc.</span>

<span >DET provides benefits of scale and ease of access as a central point for collecting and propagating data to the community. As a single point of access, it overcomes the business and technical hurdles that are a part of managing multiple online sources where scholarly activity occurs, in a rapidly changing landscape of online channels. This resource covers content across publishers and serves as a strong foundation to support the development of tools and services by any party. DET users will always be able to combine the DET data with those individually collected via negotiated or paid access. DET remains a utility separate from any value-added amenities, such as analytics, presentation, and reporting.</span>

## <span >DET Service-Level Agreement</span>

<span >For those who seek the highest level of service and a more flexible range of access options, Crossref will provide a Service-Level Agreement (SLA) service for the DOI Event Tracker. The DET SLA includes the following additional features on top of the common data offering:</span>

  * <span >Access to the complete suite of sources, which includes restricted and/or paid sources in addition to common data, providing the fullest picture of DOI usage activity possible.</span>
  * <span >Guaranteed uptime and response time to the latest raw data on the aggregate activity surrounding a DOI.</span>
  * <span >Guaranteed support response time to questions and issues surrounding data and data delivery.</span>
  * <span >Flexible data access options: on-demand real time data access and scheduled bulk downloads for processing batch analytics.</span>
  * <span >Optimum retrieval rates and accelerated delivery speeds with the dedicated SLA API.</span>
  * <span >Access to a webhook API for events of interest as an alternative to polling DET.</span>
  * <span >Standardized and enhanced linkback service for the difficult-to-track, grey literature.</span>

<span >The DET SLA service has a simple, value-based pricing model based on subscriber size. <a href="https://docs.google.com/a/crossref.org/forms/d/1_pOnL6500eFebismbHMlAJINxVFqvDFMMkupZualmNo/viewform?usp=send_form">Register your interest</a> in Crossref’s DOI Event Tracker and the DET SLA service if you would like stay informed of the upcoming launch. Please contact <a href="mailto:jlin@crossref.org">Jennifer Lin</a> for more information.</span>

<span ><em>Image modified from &#8220;<a href="https://thenounproject.com/term/radar/50290/">Radar</a>&#8221; icon by Karsten Barnett from the Noun Project.</em></span>