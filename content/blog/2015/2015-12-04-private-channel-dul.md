---
archives:
- 2015
author: Jennifer Lin
authors:
- Jennifer Lin
categories:
- Data
- Identifiers
- Usage
date: 2015-12-04
title: 'Distributed Usage Logging: A private channel for private data'
x-version: 0.0.0
---

<img src="/wp/blog/uploads/2015/11/PSM_V70_D236_Forty_wire_telephone_switchboard.png" alt="image 1907 forty wire telephone switchboard" width="263px" height="300px" class="img-responsive" />

_Forty wire telephone switchboard, 1907, Author unknown, Popular Science Monthly Vol 70, Wikimedia Commons._

A few months ago Crossref announced that we will be launching a new service for the community in 2016 that tracks activities around DOIs recording user content interactions. These “events” cover a broad spectrum of online activities including publication usage, links to datasets, social bookmarks, blog mentions, social shares, comments, recommendations, etc. The [Event Data](/blog/det-poised-for-launch) service collects the data and make it available to all in an open clearinghouse so that data are open, comparable, audit-able, and portable. These data are all publicly available from external platform partners, and they meet the terms of distribution from each partner. <!--more-->

But Crossref and its members are also concerned about privacy. We recognise that not all data can be made open and public. Particularly if it is sensitive, personally identifiable data about usage. With this in mind, we are also launching an affiliated service, Distributed Usage Logging (DUL), for external parties to transmit sensitive data on user content interactions directly to authorized end points. As researchers are increasingly using “alternative” (non-publisher) platforms to store, access and share literature, publishers are correspondingly  interested in incorporating the activity on their publications into their COUNTER reports.

Interested third-party sites might include the following:

- Institutional and subject repositories
- Aggregator platforms (EBSCOhost, IngentaConnect)
- Researcher-oriented social-networking sites (e.g. Academia.edu, ResearchGate, Mendeley)
- Reading environments and tools (e.g. ReadCube, Utopia Documents)

For publishers to process such events via their COUNTER-compliant usage reporting streams, they need private usage information and a secure channel by which to receive the data from the external platforms. Crossref will provide a switchboard that will enable these non-publisher platforms can safely transmit private data directly to the publisher.</span>

<span ><span >The work ahead entails close collaboration between Crossref, [COUNTER](http://www.projectcounter.org/), and the partners who will be sending and receiving the private data. The cross-organizational team will be working towards the following before launch: technical infrastructure development for production service, semantic definition of the usage logging message, assignment and validation of credentials to participants in the scheme, participant integration of the DUL API, and incorporation of this data type into the COUNTER Code of Practice. We will also continue to consult with data privacy and security authorities to ensure that the scheme respects all governmental obligations and community best practice regarding the processing of personal data.

We will share more about the launch of the service as we make progress along the way. Please contact [Jennifer Lin](mailto:feedback@crossref.org) for more information.