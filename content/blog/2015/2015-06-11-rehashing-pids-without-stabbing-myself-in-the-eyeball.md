---
archives:
- 2015
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Citation
- Identifiers
- Linking
- Persistence
date: 2015-06-11
title: Rehashing PIDs without stabbing myself in the eyeball
x-version: 0.0.0
---

Anybody who knows me or reads this blog is probably aware that I don’t exactly [hold back][1] when discussing [problems][2] with the DOI system. But just occasionally I find myself actually defending the thing&#8230;<!--more-->About once a year somebody suggests that we could replace existing persistent citation identifiers (e.g. DOIs) with some new technology that would fix some of the weaknesses of the current systems. Usually said person is unhappy that current systems like

[DOI][3], [Handle][4], [Ark][5], [perma.cc][6], etc. depend largely on a social element to update the pointers between the identifier and the current location of the resource being identified. It just seems manifestly old-fashioned and ridiculous that we should still depend on [bags of meat][7] to keep our digital linking infrastructure from falling apart.

In the past, [I’ve threatened to stab myself in the eyeball][8] if I was forced to have the discussion again. But the dirty little secret is that I play this game myself sometimes. After all, [the best thing a mission-driven membership organisation could do for its members would be to fulfil its mission and put itself out of business][9]. If we could come up with a technical fix that didn’t require the social component, it would save our members a lot of money and effort.

When one of these ideas is posed, there is a brief flurry of activity as another generation goes through the same thought processes and (so far) comes to the same conclusions.

The proposals I’ve seen generally fall into one of the following groups:

  * Replace persistent identifiers (PIDs) with [hashes][10], [checksums][11], etc.
  * Just use search (often, but not always coupled with 1 above)
  * Automagically create PIDs out of metadata.
  * Automagically redirect broken citations to archived versions of the content identified
  * And more recently… use the [blockchain][12]

I thought it might help advance the discussion and avoid a bunch of dead ends if I summarised (rehashed?) some of the issues that should be considered when exploring these options.

Warning: Refers to [FRBR][13] terminology. Those of a sensitive disposition might want to turn away now.

  * DOIs, PMIDs, etc. and other persistent identifiers are primarily used by our community as &#8220;citation identifiers&#8221;. We generally cite at the &#8220;expression&#8221; level.
  * Consider the difference between how a &#8220;citation identifier&#8221; a &#8220;work identifier&#8221; and a &#8220;content verification identifier&#8221; might function.
  * How do you deal with &#8220;equivalent manifestations&#8221; of the same expression. For example the ePub, PDF and HTML representations of the same article are intellectually equivalent and interchangeable when citing. The same applies to csv & tsv representations of the same dataset. So, for example, how do hashes work here as a citation identifier?
  * Content can be changed in ways that typically doesn’t effect the interpretation or crediting of the work. For example, by reformatting, correcting spelling, etc. In these cases the copies should share the same citation identifier, but the hashes will be different.
  * Content that is virtually identical (and shares the same hash) might be republished in different venues (e.g. a normal issue and a thematic issue). Context in citation is important. How do you point somebody at the copy in the correct context?
  * Some copies of an article or dataset are stewarded by publishers. That is, if there is an update, errata, corrigenda, retraction/withdrawal, they can reflect that on the stewarded copy, not on copies they don’t host or control. Location is, in fact, important here.
  * Some copies of content will be nearly identical, but will differ in ways that would affect the interpretation and/or crediting of the work. A corrected number in a table for example. How would you create a citation form a search that would differentiate the correct version from the incorrect version?
  * Some content might be restricted, private or under embargo. For example private patient data, sensitive data about archaeological finds or the migratory patterns of endangered animals.
  * Some content is behind paywalls (cue jeremiads)
  * Content is increasingly composed of static and dynamic elements. How do you identify the parts that can be hashed?
  * How do you create an identifier out of metadata and not have them look like [this][14]?

This list is a starting point that should allow people to avoid a lot of blind alleys.

In the mean time, good luck to those seeking alternatives to the current crop of persistent citation identifier systems. I’m not convinced it is possible to replace them, but if it is- I hope I beat you to it. 🙂 And I hope I can avoid stabbing myself in the eye.

 [1]: /blog/dois-unambiguously-and-persistently-identify-published-trustworthy-citable-online-scholarly-literature-right/
 [2]: /blog/january-2015-doi-outage-followup-report
 [3]: http://www.doi.org
 [4]: http://www.handle.net
 [5]: http://en.wikipedia.org/wiki/Archival_Resource_Key
 [6]: http://perma.cc
 [7]: http://tvtropes.org/pmwiki/pmwiki.php/Main/CallAHumanAMeatbag
 [8]: https://web.archive.org/web/20170811141334/http://blogs.plos.org/mfenner/2009/02/17/interview_with_geoffrey_bilder/
 [9]: http://cameronneylon.net/blog/principles-for-open-scholarly-infrastructures/
 [10]: http://en.wikipedia.org/wiki/Hash_function
 [11]: http://en.wikipedia.org/wiki/Checksum
 [12]: http://en.wikipedia.org/wiki/Blockchain
 [13]: http://en.wikipedia.org/wiki/Functional_Requirements_for_Bibliographic_Records
 [14]: http://en.wikipedia.org/wiki/Serial_Item_and_Contribution_Identifier