---
archives:
- 2015
author: Joe Wass
authors:
- Joe Wass
categories:
- Citation
- Citation Formats
- Crossref Labs
- Event Data
- Linking
- Wikipedia
date: 2015-03-03
tags:
- chronograph
title: Real-time Stream of DOIs being cited in Wikipedia
x-version: 0.0.0
---

## <span >TL;DR</span>

<span >Watch a real-time stream of DOIs being cited (and “un-cited!” ) in Wikipedia articles across the world: [https://live.eventdata.crossref.org/live.html](https://live.eventdata.crossref.org/live.html)   

## <span >Background</span>

<span >For years we’ve known that the Wikipedia was a major referrer of Crossref DOIs and about a year ago <a href="/blog/many-metrics-such-data-wow/">we confirmed</a> that, in fact, the Wikipedia is the 8th largest refer of Crossref DOIs. We know <a href="http://chronograph.labs.crossref.org/domain.html?domain=wikipedia.org">that people follow the DOIs</a>, too. This despite a fraction of Wikipedia citations to the scholarly literature even using DOIs. So back in August we decided to create a <a href="/blog/citation-needed/">Wikimedia Ambassador programme</a>. The goal of the programme was to promote the use of persistent identifiers in citation and attribution in Wikipedia articles.</span> We would do this through outreach and through the development of better citation-related tools.<!--more-->

Remember when we [originally wrote about our experiments with the PLOS ALM code][1] and how that has transitioned into the [DOI Event Tracking Pilot][2]? In those posts we mentioned that one of the hurdles in gathering information about DOI events is the actual process of polling third party APIs for activity related to millions of DOIs. Most parties simply wouldn’t be willing handle the load of a 100K API calls an hour. Besides, polling is a tremendously inefficient process, only a fraction of DOIs are ever going to generate events, but we’d have to poll for each of them, repeatedly, forever, to get an accurate picture of DOI activity. We needed a better way. We needed to see if we could reverse this process and convince some parties to instead &#8220;push&#8221; us information whenever they saw DOI related events (e.g. citations, downloads, shares, etc). If only we could convince somebody to try this&#8230;

## Wikipedia DOI Events

In December 2014 we took the opportunity of the [2014 PLOS/Crossref ALM Workshop][3] in San Francisco too meet with [Max Klein][4] and [Anthony Di Franco][5] where we kicked off a very exciting project.

There’s always someone editing a [Wikipedia][6] somewhere in the world. In fact, you can see a dizzying [live stream of edits][7]. We thought that given that there are so many DOIs in Wikipedia, that live stream may contain some diamonds (DOIs are made of diamond, that’s how they can be persistent). Max and Anthony went away and came back with a demo that contains a surprising amount of DOI activity.

That demo is evolving into a concrete service, called [Cocytus][8]. It is running at Wikimedia Labs monitoring live edits as you read this.

For now we’re feeding that data into the [DOI Events Collection app][9] (which is an off-shoot of the [Chronograph project][10]). We are in the process of modifying the [Lagotto code][11] so that we can instead push those events into the [DOI Event Tracking Instance][12].

The first DOI event we noticed was delightfully prosaic: The DOI for [&#8220;The polymath project&#8221;][13] is cited by the Wikipedia page for [&#8220;Polymath Project&#8221;][14]. Prosaic perhaps, but the authors of that paper probably want to know. Maybe they can help edit the page.

Or how about this. Someone wrote a a paper about [why people edit Wikipedia][15] and then it was cited by Wikipedia. And then [the citation was removed][16]. The plot thickens&#8230;

We’re interested in seeing how DOIs are used outside of the formal scholarly literature. What does that mean? We don’t fully know, that’s the point. We have retractions in scholarly literature (and our [Crossmark metadata and service][17] allow publishers to record that), but it’s a bit different on Wikipedia. Edit wars are fought over &#8230; well you can [see for yourself][18].

Citations can slip in and out of articles. We saw the DOI [10.1001/archpediatrics.2011.832][19] deleted from [&#8220;Bipolar disorder in children&#8221;][20]. If we’d not been monitoring the live feed (we had considered analysing snapshots of the Wikipedia in bulk) we might never have seen that. This is part of what non-traditional citations means, and it wasn’t obvious until we’d seen it.

You can see this activity on the [Chronograph’s stream][21]. Or [check your favourite DOI][9]. Please be aware that we’re only collecting newly added citations as of today. We do intend to go back and back-fill, but that may take some time- as it \* cough \* requires polling again.

## Some Technical Things

A few interesting things that happened as a result of all this:

### <span >Secure URLs</span>

<span >SSL and HTTPS were invented so you could do things like banking on the web without fear of interception or tampering. As the web becomes a more important part of life, many sites are upgrading from HTTP to HTTPS, the secure version. This is not only because your confidential details may be tampered with, but because certain governments might not like you reading certain materials.</span>

<span >Because of this, some time ago, Wikipedia decided to embark on an upgrade to <a href="https://blog.wikimedia.org/2013/08/01/future-https-wikimedia-projects/">HTTPS</a> last year, and they are a certain way along the path. The <a href="http://www.doi.org/">IDF</a>, who are responsible for running the DOI system, upgraded to HTTPS this Summer, although most DOIs are referred to by HTTP still.</span>

<span >We met with <a href="http://nitens.org/taraborelli/home">Dario Taraborelli</a> at the ALM workshop and discussed the DOI referral data that is fed into the <a href="http://chronograph.labs.crossref.org">Chronograph</a>. We put two and two together and realised that Wikipedia was linking to DOIs (which are mostly HTTP) from pages which might be served over HTTPS. New policies in HTML5 specify that referrer URL headers shouldn’t be sent from HTTPS to HTTP (in case there was something secret in them). The upshot of this is, if someone’s browsing Wikipedia via HTTPS and click on a normal DOI, we won’t know that the user came from Wikipedia. Not a huge problem today, but as Wikipedia switches over to entirely secure, we’re going to miss out on very useful information.</span>

<span >Fortunately, the HTML5 specification includes a way to fix this (without leaking sensitive information). We discussed this with Dario, and he did some research, and <a href="https://meta.wikimedia.org/wiki/Research:Wikimedia_referrer_policy">came up with a suggestion</a>, which got <a href="https://meta.wikimedia.org/wiki/Research_talk:Wikimedia_referrer_policy">discussed</a>. It’s fascinating to watch a democratic process like this take place and take part in it.</span>

<span >We’re waiting to see how the discussion turns out, and hope that it all works out so we can continue to report on how amazing Wikipedia is at sending people to scholarly literature.</span>

### <span >How shall I cite thee?</span>

<span >Another discussion grew out of that process, and we started talking to a Wikipedian called Nemo (note to Latin scholars: we weren’t just talking to ourselves). Nemo (real name Federico Leva) had a few suggestions of his own. Another way to solve the referrer problem is by using HTTPS URLs (HTML5 allows browsers to send the referrer domain when going from HTTPS to HTTPS).</span>

<span >This means going back to all the articles that use DOIs and change them from HTTP to HTTPS. Not as simple as it sounds, and it doesn’t sound simple. We started looking into how DOIs were cited on Wikipedia.</span>

<span >After some research we found that there are more ways that we expected to cite DOIs.</span>

<span >First, there’s the URL. You can see it in action in <a href="https://en.wikipedia.org/w/index.php?title=GridLAB-D&action=edit">this article</a>. URLs can take various forms.</span>

  * <span >http://dx.doi.org/10.5555/12345678</span>
  * <span >http://doi.org/10.5555/12345678</span>
  * <span >https://dx.doi.org/10.5555/12345678</span>
  * <span >https://doi.org/10.5555/12345678</span>
  * <span >http://doi.org/hvx</span>
  * <span >https://doi.org/hvx</span>

<span >Second there’s the <a href="https://en.wikipedia.org/wiki/Template:Cite_journal">official template tag</a>, seen in action <a href="https://en.wikipedia.org/w/index.php?title=Bird&action=edit">here</a>:</span>

<pre>&lt;ref name="SCI-20140731"&gt;{{cite journal |title=Sustained miniaturization and anatomical innovation in the dinosaurian ancestors of birds |url=http://www.sciencemag.org/content/345/6196/562 |date=1 August 2014 |journal=[[Science (journal)|Science]] |volume=345 |issue=6196 |pages=562–566 |doi=10.1126/science.1252243 |accessdate=2 August 2014 |last1=Lee |first1=Michael S. Y. |first2=Andrea|last2=Cau |first3=Darren|last3=Naish|first4=Gareth J.|last4=Dyke}}&lt;/ref&gt;
</pre>

<span >There’s a DOI in there somewhere. This is the best way to cite DOIs, firstly as it’s actually a proper traditional citation and there’s nothing magic about DOIs, secondly because it’s a template tag and can be re-rendered to look slightly different if needed.</span>

<span >Third there’s the old official <a href="https://en.wikipedia.org/wiki/Template:Cite_doi">DOI template tag</a> that’s now discouraged:</span>

<pre>&lt;ref name="Example2006"&gt;{{Cite doi|10.1146/annurev.earth.33.092203.122621}}&lt;/ref&gt;</pre>

<span >And then there’s another <a href="https://en.wikipedia.org/wiki/Wikipedia:Template_messages/Links#Miscellanea">one</a>.</span>

<pre>{{doi|10.5555/123456789}}
</pre>

<span >Knowing all this helps us find DOIs. But if we want to convert DOIs links in Wikipedia to use HTTPS, it means that there are more template tags to modify and more pages to re-render.</span>

<span >Nemo also put DOIs on the <a href="https://meta.wikimedia.org/wiki/Interwiki_map">Interwiki Map</a> which should make automatically changing some of the URLs a lot easier.</span>

<span >We’re very grateful to Nemo for his suggestions and work on this. We’ll report back!</span>

### <span >The elephant in the room</span>

<span >Those of you who know how DOIs work will have spotted an unsecured elephant in the room. When you visit a DOI, you visit the URL, which hits the <a href="http://www.doi.org/doi_handbook/3_Resolution.html#3.7.3">DOI resolver proxy server</a>, which returns a message to your browser to redirect to the landing page on the publisher’s site.</span>

<span >Securely talking to the DOI resolver by using HTTPS instead of HTTP means that no-one can eavesdrop and see which DOI you are visiting, or tamper with the result and send you off to a different page. But the page you are sent to will be, in nearly all cases, still HTTP. Upgrading infrastructure isn’t trivial, and, with over 4000 members (mostly publishers), most Crossref DOIs will still redirect to standard HTTP pages for the foreseeable future.</span>

<span >You can keep as secure as possible by using <a href="https://www.eff.org/https-everywhere">HTTPS Everywhere</a>.</span>

## <span >Fin</span>

<span >There’s lots going on, watch this space to see developments. Thanks for reading this, and all the links. We’d love to know what you think.</span>

## <span >Bootnote</span>

<span >Not long after this blog post was published we saw something very interesting.</span>

<img src="/wp/blog/uploads/2015/03/Screen-Shot-2015-03-04-at-17.18.42.png" alt="Interesting DOI" class="img-responsive" />

<span >That’s no DOI. We like interesting things, but they can panic us. This turned out to be a great example of why this kind of thing can be useful. A minute’s digging and we <a href="https://ja.wikipedia.org/w/index.php?title=%E6%9C%80%E5%A4%A7%E3%83%95%E3%83%AD%E3%83%BC%E5%95%8F%E9%A1%8C&diff=54616146&oldid=54612246">found the article edit</a>:</span>

<img src="/wp/blog/uploads/2015/03/Screen-Shot-2015-03-04-at-17.20.06.png" alt="Wikipedia typo" class="img-responsive" />

<span >It turns out that this was a typo: someone put a title when they should have put in a DOI. And, as <a href="http://events.labs.crossref.org/dois/a%20data%20structure%20for%20dynamic%20trees">the event</a> shows, this was removed from the Wikipedia article.</span>

 [1]: /blog/many-metrics-such-data-wow
 [2]: /blog/crossrefs-doi-event-tracker-pilot/
 [3]: http://figshare.com/articles/ALM_Workshop_2014_Report/1287503
 [4]: https://en.wikipedia.org/wiki/User:Notconfusing
 [5]: https://twitter.com/dfko_0
 [6]: https://en.wikipedia.org/wiki/List_of_Wikipedias
 [7]: http://wikistream.wmflabs.org/
 [8]: https://github.com/notconfusing/cocytus
 [9]: https://web.archive.org/web/20150308012303/http://events.labs.crossref.org/
 [10]: /blog/introducing-chronograph/
 [11]: https://github.com/articlemetrics/lagotto
 [12]: http://det.labs.crossref.org/
 [13]: http://dx.doi.org/10.1145/1978942.1979213
 [14]: https://en.wikipedia.org/wiki/Polymath_Project
 [15]: http://dx.doi.org/10.1080/0144929x.2014.929744
 [16]: https://web.archive.org/web/20150321130048/http://events.labs.crossref.org/dois/10.1080/0144929x.2014.929744
 [17]: https://www.crossref.org/services/crossmark
 [18]: https://en.wikipedia.org/wiki/Wikipedia:Lamest_edit_wars
 [19]: http://dx.doi.org/10.1001/archpediatrics.2011.832
 [20]: https://en.wikipedia.org/wiki/Bipolar_disorder_in_children
 [21]: https://web.archive.org/web/20150422055509/http://events.labs.crossref.org/events/types/WikipediaCitation