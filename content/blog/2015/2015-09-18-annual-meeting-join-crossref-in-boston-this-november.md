---
archives:
- 2015
author: April Ondis
authors:
- April Ondis
categories:
- Crossref LIVE
- Annual Meeting
- Community
- Member Briefing
date: 2015-09-18
title: 'Annual Meeting: Join Crossref in Boston this November!'
x-version: 0.0.0
---

We’d like to invite the scholarly publishing community to get together in Boston this November with the Crossref Annual Meeting as a rally point. This is the event we hold just once a year to get the whole team under one roof, host a lively discussion with the leading voices in scholarly communications, present technical workshops, and offer you the chance to get hands’ on with our latest metadata services. **Our [free two-day event][1] takes place from November 17-18, 2015 in Boston, MA.**<!--more-->

**Agenda:**

  * Tuesday, November 17 - Tech Workshops:

The morning is an opportunity to get into small groups and talk directly with our development and support teams. We will present best practices around using Crossref’s metadata. After lunch, we will feature member case studies with tips on implementation and lessons learned. If you’re on the technical production side of scholarly publishing, you’ll want to be there — and not just for the beer & pretzels afterwards.

  * Wednesday, November 18 - Member Meeting:

A day to hear from thought leaders from the larger scholarly publishing community as well as from inside Crossref. Our keynote speaker will be **Dr. Ben Goldacre** (Bad Science), and our distinguished speakers include **Dr. Scott Chamberlain** (rOpenSci), **Dr. Juan Pablo Alperin** (Public Knowledge Project), and **Dr. Martin Eve**, (Open Library of Humanities). We will share details about the road map for Crossref Labs’ current and future initiatives, hear about the latest organizational developments from new members of our team, and see the debut of our new brand logo and communications strategy. Following the formal discussion, we’ll continue the conversation over cocktails as part of our celebration of Crossref’s milestone 15th Anniversary!

**✱ Tickets:**

Reserve your free tickets here: <https://www.eventbrite.com/e/crossref15-tech-workshops-member-meeting-tickets-17921679225>

**Who Should Attend?**

Scholarly publishers, technology providers, librarians, researchers, academic institutions, funders, journalists, and others who are keen to discuss tools and services to advance scholarly publishing are encouraged to attend.

**✱ Venue:**

  * [Hotel Taj Boston][2]
  * 15 Arlington Street
  * Boston, MA 02116 USA

**About Crossref** Crossref is a not-for profit membership organization that wants to improve research communication. We organize publisher metadata, run the infrastructure that makes DOI links work, and we rally multiple community stakeholders in order to develop tools and services to enable advancements in scholarly publishing.

 [1]: https://www.eventbrite.com/e/crossref15-tech-workshops-member-meeting-tickets-17921679225
 [2]: https://www.tajhotels.com/en-in/taj/taj-boston/