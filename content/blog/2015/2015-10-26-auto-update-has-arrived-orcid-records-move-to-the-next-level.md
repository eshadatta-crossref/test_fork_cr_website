---
archives:
- 2015
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- ORCID
- Identifiers
- DataCite
date: 2015-10-26
title: Auto-Update Has Arrived! ORCID Records Move to the Next Level
x-version: 0.0.0
---

<p class="p1">
  <span class="s1">Crossref goes live in tandem with DataCite to push both publication and dataset information to ORCID profiles automatically. All organisations that deposit ORCID iDs with Crossref and/or DataCite will see this information going further, automatically updating author records. </span>
</p>

<p class="p1">
  <!--more-->
</p>

We’re cross-posting [ORCID’s blog](https://info.orcid.org/auto-update-has-arrived-orcid-records-move-to-the-next-level/) below with all the details:

<span >Since ORCID’s inception, our key goal has been to unambiguously identify researchers and provide tools to automate the connection between researchers and their creative works.  We are taking a big step towards achieving this goal today, with the launch of [Auto-Update functionality](https://info.orcid.org/new-functionality-friday-auto-update-your-orcid-record/) in collaboration with [Crossref](http://www.crossref.org/) and [DataCite](https://www.datacite.org/.  

<span >There’s already been a lot of excitement about Auto-Update: Crossref’s recent announcement about the imminent launch generated a flurry of discussion and celebration on social media. Our own [tweet](https://twitter.com/ORCID_Org/status/647020600192581633) on the topic was viewed over 10,500 times and retweeted by 60 other accounts. </span>

<span >So why all the fuss? We think Auto-Update will transform the way researchers manage their scholarly record.  Until now, researchers have had to manually maintain their record, connecting new activities as they are made public.  In ORCID, that meant using [Search & Link tools](https://support.orcid.org/hc/en-us/articles/360006973653-Add-works-by-direct-import-from-other-systems) developed by our member organizations to claim works manually.  Researchers frequently ask,  “Why, if I include my ORCID iD when I submit a manuscript or dataset, isn’t my ORCID record “automagically” updated when the work is published?”</span>

<span >With the launch of Auto-Update, that is just what will happen. </span>

**It might seem like magic but there are a few steps to make it work:**

**Researchers.** <span >You need to do two things:  (1) use your ORCID iD when submitting a paper or dataset, and (2) [authorize Crossref and DataCite to update your ORCID record](https://support.orcid.org/hc/en-us/articles/360006973653-Add-works-by-direct-import-from-other-systems).   In keeping with [our commitment to ensuring that researchers maintain full control of their ORCID record](https://orcid.org/privacy-policy), you may revoke this permission at any time, and may also choose privacy settings for the information posted on your record.</span>

**Publishers and data centers.** These organizations also have two things to do: (1) collect ORCID identifiers during the submission workflow, using a process that involves authentication (not a type-in field!), and (2) embed the iD in the published paper and include the iD when submitting information to Crossref or DataCite.  

**Crossref and DataCite.** Upon receipt of data from a publisher or data center with a valid identifier, Crossref or DataCite can automatically push that information to the researcher’s ORCID record.  

<span >More information about how to opt out of this service can be found here: [the ORCID Inbox](https://support.orcid.org/hc/en-us/articles/360006972953-ORCID-inbox-notifications-and-frequency-settings).  

{{% imagewrap right %}}
{{< figure src="/images/blog/ORCID-graphic-223x300.png" width="350" align="right">}}
{{% /imagewrap %}}

<span >Why is this so revolutionary? </span>

A bit of background, first. Crossref and DataCite, both non-profit organizations, are leaders in minting DOIs (Digital Object Identifiers) for research publications and datasets.  A [DOI](http://www.crossref.org/01company/16fastfacts.html#sthash.o7NGwOnP.dpuf) is a unique alphanumeric string assigned to a digital object – in this case, an electronic journal article, book chapter, or a dataset. Each DOI is associated with a set of basic metadata and a URL pointer to the full text, so that it uniquely identifies the content item and provides a persistent link to its location on the internet.

<span >Crossref, working with over a thousand scholarly publishers, has generated well over 75 million DOIs for journal articles and book chapters.  DataCite works with nearly 600 data centers worldwide and has generated over 6.5 million DOIs to date. Between them, Crossref and DataCite have already received almost a half a million works from publishers and data centers that include an ORCID iD validated by the author/contributor.  With Auto-Update functionality in place, information about these articles can transit (with the author’s permission) to the author’s ORCID record. </span>

<span >Auto-Update doesn’t stop at a researcher’s ORCID record.  Systems that have integrated ORCID APIs and have a researcher’s ORCID record connected to that system &#8212; their faculty profile system, library repository, webpage, funder reporting system &#8212; can receive alerts from ORCID.  Information can move easily and unambiguously across systems. </span>

<span >This is the beginning of the end for the endless rekeying of information that plagues researchers &#8212; and anyone involved in research reporting.  Surely something to celebrate!</span>

<span >Questions you may have:</span>

**Q. What do I need to do to sign up for auto-update?**

<span >You need to grant permission to Crossref and DataCite to post information to your ORCID record.  You can do this today by using the Search and Link wizard for DataCite available through the ORCID Registry or the DataCite [Metadata Search page](https://web.archive.org/web/20151123212630/http://search.labs.datacite.org/).  We also have added a new ORCID Inbox, so that you can receive a message from Crossref or DataCite if they receive a datafile with your iD, and you can grant permission directly. See [More on the ORCID Inbox](https://support.orcid.org/hc/en-us/articles/360006972953-ORCID-inbox-notifications-and-frequency-settings).  

**Q. Will Crossref and DataCite be able to update my ORCID record with already published works for which I did not use my ORCID iD?**

<span >No.  The auto-update process only applies to those works that these organizations receive that include your ORCID iD. For previous works that did not include your ORCID iD, you will need to use the DataCite and Crossref Search and Link wizards to connect information with your iD.</span>

**Q. What information will be posted to my record?**

<span >With your permission, basic information about the article (such as title, list of contributors, journal or publisher) or dataset (such as data center name and date of publication) will be posted, along with a DOI that allows users to navigate to the source paper or dataset landing page.</span>

**Q. What if my journal or data center doesn’t collect ORCID iDs?**

Ask them to!  This simple step can be accomplished using either the Public or Member ORCID APIs. Information about integrating ORCID iDs in [publishing](https://info.orcid.org/documentation/workflows/) and [repository](http://members.orcid.org/repository-systems) workflows is publicly available.  

 [1]: https://info.orcid.org/auto-update-has-arrived-orcid-records-move-to-the-next-level/
 [2]: https://info.orcid.org/new-functionality-friday-auto-update-your-orcid-record/
 [3]: http://www.crossref.org/
 [4]: https://www.datacite.org/
 [5]: https://twitter.com/ORCID_Org/status/647020600192581633
 [6]: https://support.orcid.org/hc/en-us/articles/360006973653-Add-works-by-direct-import-from-other-systems
 [7]: https://orcid.org/privacy-policy
 [8]: https://support.orcid.org/hc/en-us/articles/360006972953-ORCID-inbox-notifications-and-frequency-settings
 [9]: /wp/blog/uploads/2015/10/ORCID-graphic.png
 [10]: http://www.crossref.org/01company/16fastfacts.html#sthash.o7NGwOnP.dpuf
 [11]: https://web.archive.org/web/20151123212630/http://search.labs.datacite.org/
 [12]: https://info.orcid.org/documentation/workflows/
 [13]: http://members.orcid.org/repository-systems