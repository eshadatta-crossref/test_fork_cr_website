---
archives:
- 2015
author: April Ondis
authors:
- April Ondis
categories:
- Annual Meeting
- Member Briefing
- Community
- Crossref LIVE
date: 2015-11-24
title: Watch Speaker Videos from the 2015 Annual Meeting
x-version: 0.0.0
---

**You might have missed it, but you haven’t missed out.**  If you want to watch – or savor re-watching – the presentations from last week’s 2015 Crossref Annual Meeting, we’ve embedded each video below in chronological order. Sit back, relax, and take it all in (again) just as though you were in an air-conditioned ballroom at the Taj. Note: if your organization blocks Wistia videos, please whitelist these domains: *.wistia.com and fast.wistia.net.<!--more-->

**Ed Pentz**, Crossref Executive Director, focuses on the best practice of writing DOIs as actionable hyperlinks in his presentation, _Crossref Best Practice:_ <http://www.slideshare.net/Crossref/ed-pentz-crossref15-55435481> (slides only)

**Martin Paul Eve** senior lecturer at Birkbeck University, London, delivers a trenchant criticism of the process small publishers must go through when getting and depositing their first Crossref DOI in his presentation, _Crossref Deposit: A Scholar-Publisher Experience_:



**Anne Coghill**, Manager of Peer Review Operations for the American Chemical Society, detailed their process for deciding where in the manuscript workflow to insert CrossCheck plagiarism screening in her presentation, _American Chemical Society Publications and CrossCheck_: <http://www.slideshare.net/Crossref/ann-coghill-crossref15> (slides only)

**Ben Hogan**, Regional Manager in Wiley’s Peer Review Management team, shares Wiley’s pain points as well as its positive experiences in using CrossCheck to detect plagiarism in his presentation, _CrossCheck Usage and Case Studies: _



**Jure Triglav**, Lead Developer for the PubSweet Publishing Framework at the Collaborative Knowledge Foundation,  demonstrates how to mine data from the corpus of open science using Crossref’s metadata via its API and open source tools from the Collaborative Knowledge Foundation in his presentation, _Making Science Writing Smarter:_



**Scott Chamberlain,** open science researcher, shows the several advantages of using programmatic tools such as R, Python, and Ruby to mine text and data, including Crossref metadata, in his presentation, _Text and Data Mining: _



**Helen Duriez**, ePublishing Manager at the Royal Society, describes the Royal Society’s experience with providing Crossmark data as a means of communicating document version information in her presentation, _Crossmark – a journey through time (and space?) 2015:_



**John Chodacki**, chair of Crossref’s DET committee, describes the future state of the DOI Event Tracker as an open hub for collecting and sharing data around web events that involve DOIs in his presentation, _DOI Event Tracker 2015_:



**Marc Abrahams**, editor and co-founder of the Annals of Improbable Research, makes you LAUGH, then THINK with his keynote speech, _Improbable Research, the Ig Nobel Prizes, and You:_



**Juan Pablo Alperin** describes the ways that Crossref and the Public Knowledge Project can work together to support common goals, in his presentation, _PKP and Crossref:_ _Two P’s in a Cross: _



**Ed Pentz**, Crossref Executive Director, summarizes the organization’s expansion over the past year with his presentation, _Crossref Growth and Change_:



**Ginny Hendricks**, Director of Member & Community Outreach, details the findings of Crossref’s recent stakeholder research and the organization’s future plans to enhance member experience with her presentation, _Member & Community Outreach_:



**Jennifer Lin**, Director of Product Management, visualizes Crossref’s role as a map maker for the scholarly web in her presentation, _Crossref: Building an Open Map for the Scholarly Enterprise:_



**Chuck Koscher**, Director of Technology, gives us performance stats for the Crossref system, including aggregate uptimes and how long it takes to deposit metadata, in his presentation, _Crossref System Performance:_



**Geoffrey Bilder**, Director of Strategic Initiatives, sheds light on the status of current and future research projects that are part of Crossref’s new product development process in his presentation, _Strategic Initiatives Update:_



**Scott Chamberlain**, open science researcher, proposes the use of programmatic tools, such as the R programming language working with the Crossref search API, to undertake scientific research in his presentation, _Thinking Programmatically:_



**Martin Paul Eve**, senior lecturer at Birkbeck University, London, bears us back to the origins of the scholarly mission, considers the implications of the notion that researchers work within a symbolic economy, and looks at the practical challenges brought about by open access modes of publication for works in the Humanities in his wide-ranging presentation, _Open Access & the Humanities: Digital Approaches:_



**Slideshare, Too! **

Finally, each speaker has generously made their slides available here: <http://www.slideshare.net/Crossref/tag/crossref15>

&nbsp;

_ _