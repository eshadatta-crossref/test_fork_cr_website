---
archives:
- 2015
author: Ginny Hendricks
authors:
- Ginny Hendricks
categories:
- Member Briefing
- Metadata
- News Release
- News Release
- ORCID
date: 2015-09-24
title: Crossref to Auto-Update ORCID Records
x-version: 0.0.0
---

<span >In the next few weeks, authors with an ORCID iD will be able to have Crossref automatically push information about their published work to their ORCID record. It’s something that <a href="https://info.orcid.org/new-functionality-friday-auto-update-your-orcid-record/">ORCID users have been asking for</a> and we’re pleased to be the first to develop the integration. 230 publishers already include ORCID iDs in their metadata deposits with us, and currently there are 248,000 DOIs that include ORCID iDs.</span>

<span ><!--more--></span>

<span ><strong>What this means for researchers</strong></span>

<span ><img class="alignright" src="http://info.orcid.org/wp-content/uploads/2021/01/orcid_128x128.png" alt="ORCID iD icon" /> More visibility for your work! Crossref represents over 5000 scholarly publishers and many of them ask authors for their ORCID iD and include it in the publication information they send us. Also it will mean less manual searching and adding; you’ve always been able to <a href="https://web.archive.org/web/20131229210637/http://search.crossref.org/">search crossref metadata</a> for your name and/or publications and manually add them to your ORCID record, this auto-update simply means that when your publishers include the info we can update and add work(s) to your ORCID record automatically for you. You can still choose to hide/show whatever works you choose, and, of course, you’ll have the opportunity to authorize or switch off the integration completely (though future publications may trigger a new request). Overall, you’ll benefit from a more complete and up-to-date ORCID record to showcase your work.</span>

<span ><strong>What this means for publishers</strong></span>

<span >If you’re one of the 230 Crossref publishers who already supply ORCID iDs along with the usual metadata submissions, then you’re all good. If you don’t offer this yet, you might want to think about starting - it’s beneficial for funders, publishers, other researchers, libraries, and universities to be able to integrate with complete researcher records. You can ask for ORCIDs upon manuscript submission or acceptance and tag it in your metadata deposits with Crossref. We’ll ensure the rest.</span>

<span ><strong>Various caveats and important details to be aware of</strong></span>

  1. <span >Apparently not all publishers are members of Crossref (we know, crazy), and in addition only a subset of Crossref publishers (230 in total) are asking authors for ORCID iDs and/or including them in their metadata deposits.</span>
  2. <span >Some publishers may choose to opt out of passing through the details to ORCID using the Crossref auto-update (perhaps they plan to send this directly at some point) but if you’ve included your ORCID with your submission and it isn’t automatically updated, then check with your publisher.</span>
  3. <span >We have a “backlog” of almost 250,000 DOIs that include ORCID iDs so that may mean we do some bulk updates at a later date where authors will receive an email with a long list of works to add. Even if the works have been listed before, it’s worth accepting as it will add the most up-to-date metadata to ensure the most accurate record.</span>

<span >Any questions can be directed to <a href="http://mailto:support@crossref.org">our support team</a>.</span>