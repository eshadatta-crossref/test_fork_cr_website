---
archives:
- 2011
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Data
- DataCite
- Identifiers
- Linked Data
- Standards
date: 2011-10-10
title: DataCite supporting content negotiation
x-version: 0.0.0
---

In April [In April][1] for its DOIs. At the time I cheekily called-out [DataCite][2] to start supporting content negotiation as well.

Edward Zukowski (DataCite’s resident propellor-head) took up the challenge with gusto and, as of September 22nd [DataCite has also been supporting content negotiation for its DOIs][3]. This means that one million more DOIs are now <a class="zem_slink" href="http://en.wikipedia.org/wiki/Linked_Data" title="Linked Data" rel="wikipedia">linked-data</a> friendly. Congratulations to Ed and the rest of the team at DataCite.

We hope this is a trend. Back in June [Knowledge Exchange][4] organized a seminar on Persistent Object Identifiers. One of the outcomes of the meeting was &#8220;[Den Haag Manifesto][5]&#8221; a document outlining five relatively simple steps that different persistent identifier systems could take in order to increase interoperability. Most of these steps involved adopting linked data principles including support for content negotiation. We look forward to hearing about other persistent identifiers adopting these principles over the next year.

Having said that, this time I will refrain from calling-out anybody specifically&#8230;

<div  class="zemanta-pixie">
  <a class="zemanta-pixie-a" href="http://www.zemanta.com/" title="Enhanced by Zemanta"><img  class="zemanta-pixie-img" src="http://img.zemanta.com/zemified_e.png?x-id=f7639c9b-8fd7-4af4-9c08-4f283778f4c2" alt="Enhanced by Zemanta" /></a>
</div>

 [1]: /blog/content-negotiation-for-crossref-dois/
 [2]: http://datacite.org/
 [3]: http://data.datacite.org/
 [4]: http://www.knowledge-exchange.info/
 [5]: https://web.archive.org/web/20130808010317/http://www.knowledge-exchange.info/Default.aspx?ID=62&M=News&NewsID=124