---
archives:
- 2011
author: Karl Ward
authors:
- Karl Ward
categories:
- Reverse Look-up
- Domains
- Member Experience
date: 2011-11-22
title: Determining the Crossref membership status of a domain
x-version: 0.0.0
---

We’ve been asked a few times if it is possible to determine whether or not a particular domain name belongs to a Crossref member. To address this we’re launching another small service that performs something like a &#8220;reverse look-up&#8221; of URLs and domain names to DOIs and Crossref member status.

The service provides an API that will attempt to reverse look-up a URL to a DOI and return the membership status (member or non-member) of the root domain of the URL. In practice resolving URLs to DOIs has substantial limitations - many publishers redirect the resolution URL of DOIs to other online content and URLs become clogged up with session IDs and other cruft appearing in their query parameters. All of this means it is unlikely that the URLs that appear to be the end result of DOI resolution are actually the URLs pointed to.

However, it’s also possible to provide only a host name, in which case, as with a URL, the Crossref membership status for the root domain will be returned.

There’s also a downloadable list of hashed domains that belong to Crossref members which will be useful to those who want to determine the membership status of a domain locally. Also, a bookmarklet allows anyone to easily check a web page they are looking at to see if the domain it is hosted on belongs to a Crossref member.