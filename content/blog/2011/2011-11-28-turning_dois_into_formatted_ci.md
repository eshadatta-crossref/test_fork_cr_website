---
archives:
- 2011
author: Karl Ward
authors:
- Karl Ward
categories:
- Citation Formats
- Crossref Labs
- DOIs
- Linked Data
- Metadata
date: 2011-11-28
title: Turning DOIs into formatted citations
x-version: 0.0.0
---

<span >Today two new content types were added to dx.doi.org resolution for Crossref DOIs. These allow anyone to retrieve DOI bibliographic metadata as formatted bibliographic entries. To perform the formatting we’re using the <a href="http://citationstyles.org/">citation style language</a> processor, <a href="https://web.archive.org/web/20120113111420/https://bitbucket.org/fbennett/citeproc-js/wiki/Home">citeproc-js</a> which supports a shed load of citation styles and locales. </span>

<span >In fact, all the styles and locales found in the CSL repositories, including many common styles such as bibtex, apa, ieee, harvard, vancouver and chicago are supported. First off, if you’d like to try citation formatting without using content negotiation, there’s <a href="https://web.archive.org/web/20120201085933/http://citation.crrd.dyndns.org/"><strong>a simple web UI</strong></a> that allows input of a DOI, style and locale selection. If you’re more into accessing the web via your favorite programming language, have a look at these content negotiation curl examples. To make a request for the new &#8220;text/bibliography&#8221; content type:</span> <tt>$ curl -LH "Accept: text/bibliography; style=bibtex" http://dx.doi.org/10.1038/nrd842 @article{Atkins_Gershell_2002, title={From the analyst's couch: Selective anticancer drugs}, volume={1}, DOI={10.1038/nrd842}, number={7}, journal={Nature Reviews Drug Discovery}, author={Atkins, Joshua H. and Gershell, Leland J.}, year={2002}, month={Jul}, pages={491-492}}</tt> A locale can be specified with the &#8220;locale&#8221; content type parameter, like this: <tt>$ curl -LH "Accept: text/bibliography; style=mla; locale=fr-FR" http://dx.doi.org/10.1038/nrd842 Atkins, Joshua H., et Leland J. Gershell. « From the analyst's couch: Selective anticancer drugs ». Nature Reviews Drug Discovery 1.7 (2002): 491-492.</tt> <span >You may want to process metadata through CSL yourself. For this use case, there’s another new content type, &#8220;application/citeproc+json&#8221; that returns metadata in a citeproc-friendly JSON form:</span> <tt>$ curl -LH "Accept: application/citeproc+json" http://dx.doi.org/10.1038/nrd842 {"volume":"1","issue":"7","DOI":"10.1038/nrd842","title":"From the analyst's couch: Selective anticancer drugs","container-title":"Nature Reviews Drug Discovery","issued":{"date-parts":[[2002,7]]},"author":[{"family":"Atkins","given":"Joshua H."},{"family":"Gershell","given":"Leland J."}],"page":"491-492","type":"article-journal"}</tt> <span >Finally, to retrieve lists of supported styles and locales, see:</span>

 <span >* <a href="https://crosscite.org">https://crosscite.org</a></span>

<span ><a href="https://github.com/citation-style-language/styles">style</a> and <a href="https://github.com/citation-style-language/locales">locale</a> repositories. There’s one big caveat to all this. The CSL processor will do its best with Crossref metadata which can unfortunately be quite patchy at times. There may be pieces of metadata missing, inaccurate metadata or even metadata items stored under the wrong field, all resulting in odd-looking formatted citations. Most of the time, though, it works.</span>