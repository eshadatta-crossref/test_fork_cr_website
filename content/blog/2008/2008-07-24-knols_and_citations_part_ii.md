---
archives:
- 2008
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Discussion
date: 2008-07-24
title: Knols and Citations Part II
x-version: 0.0.0
---

[Tony’s post][1] highlights Knol’s &#8220;service&#8221; URIs. Another issue is that many Knol entries have nice long lists of unlinked references. The HTML code behind the references is very sparse.

Might the DOI be of use in linking out from these references? I think so. Then, of course, there’s the issue of DOIs for Knols&#8230;

 [1]: /blog/knols-and-citations/