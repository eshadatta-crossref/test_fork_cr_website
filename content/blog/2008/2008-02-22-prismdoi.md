---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Metadata
date: 2008-02-22
title: prism:doi
x-version: 0.0.0
---

The new [PRISM][1] spec (v. 2.0) was published this week, see the [press release][2]. (Downloads are available [here][3].)

This is a significant development as there is support for XMP profiles, to complement the existing XML and RDF/XML profiles. And, as PRISM is one of the major vocabularies being used by publishers, I would urge you all to go take a look at it and to consider upgrading your applications to using it.

**One caveat.** There’s a new element `<tt>prism:doi</tt>` (PRISM Namespace, 4.2.13) which sits alongside another new element `<tt>prism:url</tt>` (PRISM Namespace, 4.2.55). Unfortunately the `<tt>prism:doi</tt>` element is shown to take DOI proxy URL as its value - and not the DOI string itself, e.g.

  * Model #1  
    `<prism:doi rdf:resource=”http://dx.doi.org/10.1030/03054”/>`
  * Model #2  
    `<prism:doi>http://dx.doi.org/10.1030/03054</prism:doi>`

This seems to me to just plain wrong. The DOI in itself is not a URL (or URI) - although can, and should, be represented in URI form when used in Web contexts (i.e. pretty much most of the time). As a literal it should be used in its native form as specified in [ ANSI/NISO Z39.84 - 2005 Syntax for the Digital Object Identifier][4]. This would only satisfy Model #2 above.

To satisfy Model #1 above a URI form for DOI would be required. And this is **not** the service URI denoted by the proxy. It would either have to be:

* Model #1 - Registered URI Form  
            `<prism:doi rdf:resource=”info:doi/10.1030/03054”/>`
              * Model #1 - Unregistered URI Form  
            `<prism:doi rdf:resource=”doi:10.1030/03054”/>`

Any comments? Some guidelines from Crossref would be useful - although maybe further discussion is required. It is, of course, a constant bugbear that &#8220;doi:&#8221; remains an unregistered URI scheme.

 [1]: https://www.idealliance.org/prism-metadata/
 [2]: https://web.archive.org/web/20160326011620/http://www.prismstandard.org//news/2008/PRISM_%20PR021908.pdf
 [3]: https://web.archive.org/web/20080828062500/http://www.prismstandard.org/specifications/
 [4]: https://web.archive.org/web/20150923223621/http://www.niso.org/apps/group_public/download.php/14689/z39-84-2005_r2010.pdf