---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Web
date: 2008-07-28
title: Does Size Matter?
x-version: 0.0.0
---

Interesting [post][1] from Google, in which they say:

> _&#8220;Recently, even our search engineers stopped in awe about just how big the web is these days &#8212; when our systems that process links on the web to find new content hit a milestone: 1 trillion (as in 1,000,000,000,000) unique URLs on the web at once!&#8221;_

Puts Crossref’s 32,639,020 unique DOIs into some kind of perspective: 0.0033%. But nonetheless that trace percentage still seems to me to be reasonably large, especially in view of it forming a persistent and curated set.
  
**_Update:_** Talking of Google numbers, [pingdom][2] has a post &#8220;[Map of all Google data center locations][3]&#8221; with maps of US, Europe and World locations.

 [1]: http://googleblog.blogspot.com/2008/07/we-knew-web-was-big.html
 [2]: http://royal.pingdom.com/
 [3]: http://royal.pingdom.com/?p=276