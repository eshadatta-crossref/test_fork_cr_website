---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Blog
date: 2008-07-21
title: CrossTech By Numbers
x-version: 0.0.0
---

CrossTech is two years old (less one month) and we have now seen some 145 posts. Breaking the posts down by poster we arrive at the following chart:

<img alt="crosstech.png" src="/wp/blog/images/crosstech.png" width="477" height="171" />

Note this is not any real attempt at vainglory, more a simple excuse to play with the wonderful [Google Chart API][1]. Also, above I’ve taken the liberty of putting up an image (.png), although the chart could have been generated on the fly from [this link][2] (or [tinyurl here][3]).

What is of interest in the chart is that approximately 3/4 of the posts are by Crossref members (TH, EN, RK) and 1/4 by Crossref staff (EP, GB, AT, CK). Certainly Crossref staffers are doing their bit for this blog. There’s also way too many posts from me. It would be really interesting to see some others’ views or observations per the CrossTech logo legend (_&#8220;&#8230;, collaboration, &#8230;&#8221;_).

I guess the real impediment is that one needs to request an account before posting. (Certainly there’s no reason for any member to be shy about requesting an account and posting.) Note that I haven’t considered the number of commentators to the blog which is larger than the number of posters. Also a number of Crossref members are very active with their own blogs. Those blogs with a tech focus could (should?) be scooped up by a [Planet][4] style aggregator if there would be sufficient interest in maintaining a publishing technology hub.

One can only hope that the numbers will continue to grow (by direct posts or by aggregations) and that there will be a wider info share over the next couple of years.

 [1]: http://code.google.com/apis/chart/
 [2]: https://web.archive.org/web/20090218215119/http://code.google.com/apis/chart
 [3]: http://tinyurl.com/6k38ra
 [4]: https://web.archive.org/web/20080907094552/http://www.planetplanet.org/