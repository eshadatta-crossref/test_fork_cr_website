---
archives:
- 2008
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- News Release
date: 2008-09-18
title: Crossref is hiring an R&D software engineer
x-version: 0.0.0
---

Crossref is hiring an R&D software engineer to work in our Oxford office. This is a fantastic opportunity to work on wide range of projects that promise to revolutionize scholarly publishing.