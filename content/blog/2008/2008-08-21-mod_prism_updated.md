---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- RSS
date: 2008-08-21
title: mod_prism (Updated)
x-version: 0.0.0
---

I’ve just put up for comment a revised mod_prism (0.3) of the existing mod_prism RSS 1.0 module. This is now updated to the current PRISM version (v2.0) which was released in February ’08 and reissued with Errata in July ’08. The current mod_prism draft is registered [here][1].

The new draft charts all (five) versions of the PRISM specification (v1.0-v2.0) and maps PRISM terms to RSS 1.0 elements. Though not required as such for use of terms within an RSS 1.0 feed, an RSS 1.0 module does allow for easy housekeeping as well as providing usage guidelines and examples for how to use PRISM terms within an RSS 1.0 feed.

The main interest for Crossref members will be the opportunity to update their current RSS 1.0 feeds to include the new PRISM terms **prism:doi** and **prism:url**. I blogged earlier [here][2] about **prism:doi** as it first appeared. The suggestions I put forward there were subsequently incorporated into the Errata for 2.0 which were published in July and are avaliable as a zip file [here][3].

I would be very interested in receiving any feedback. I guess I should add to the v1.2 example of an RSS item in the draft an example also of a v2.0 RSS item which makes use of both **prism:doi** and **prism:url**.

 [1]: http://purl.org/rss/1.0/modules/
 [2]: /blog/prismdoi/
 [3]: https://web.archive.org/web/20081019002715/http://www.prismstandard.org//specifications/2.0/PRISM2.0Errata.zip