---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Search
date: 2008-07-29
title: Search Web Services - New Committee Drafts
x-version: 0.0.0
---

As posted [here][1] on the [SRU Implementors][2] list, the [OASIS Search Web Services Technical Committee][3] has announced the release of five Committee Drafts, informally known as:

  1. [Abstract Protocol Definition  (APD)][4] 
      * [Binding for SRU 1.2][5] 
          * [Auxiliary Binding for HTTP GET][6] 
              * [CQL 1.2][7] 
                  * [Binding for OpenSearch][8]</ol> 
                    Links to specific document formats are given at the bottom of the mail. A list of the TC public documents is also available [here][9].
  
                    The next phase of work for the TC will be the development of SRU/CQL 2.0, and the Description Language.

 [1]: http://listserv.loc.gov/cgi-bin/wa?A2=ind0807&#038;L=zng&#038;T=0&#038;P=52
 [2]: https://web.archive.org/web/20130303230855/http://sun8.loc.gov/listarch/zng.html
 [3]: http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=search-ws
 [4]: http://docs.oasis-open.org/search-ws/v1.0/apd-V1.0.html
 [5]: http://docs.oasis-open.org/search-ws/v1.0/sru-1-2-V1.0.html
 [6]: http://docs.oasis-open.org/search-ws/v1.0/binding-for-get-V1.0.html
 [7]: http://docs.oasis-open.org/search-ws/v1.0/cql-1-2-v1.0.html
 [8]: http://docs.oasis-open.org/search-ws/v1.0/opensearch-v1.0.html
 [9]: https://www.oasis-open.org/committees/documents.php?wg_abbrev=search-ws