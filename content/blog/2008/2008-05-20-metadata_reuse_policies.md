---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Metadata
date: 2008-05-20
title: Metadata Reuse Policies
x-version: 0.0.0
---

Following on from yesterday’s [post][1] about making metadata available on our Web pages, I wanted to ask here about &#8220;metadata reuse policies&#8221;. Does anybody have a clue as to what might constitute a best practice in this area? I’m specifically interested in license terms, rather than how those terms would be encoded or carried. Increasingly we are finding more channels to distribute metadata (RSS, HTML, OAI-PMH, etc.) but don’t yet have any clear statement for our customers as to how they might reuse that data.
  
Time to put the caveats aside and focus on the actuals.

 [1]: /blog/natures-metadata-for-web-pages