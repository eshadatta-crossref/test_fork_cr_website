---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- XMP
date: 2008-10-20
title: XMP Marches On
x-version: 0.0.0
---

For those who may be interested in the progress of XMP, Adobe’s Gunar Penikis has just announced [^1] two new releases of XMP SDKs: XMP Toolkit 4.4 (with support for new file formats), and FileInfo SDK (for customizing CS4 UIs). More importantly, though, may be the new edition of the XMP spec - see [here][1], which is bumped from a modest 112 page document to a 3-parter at 199 pages.

[^1]: Update Aug 2022: the announcement blog post mentioned above was previously at blogs.adobe.com/gunar/2008/10/new_xmp_sdks_released.html but is no longer live.

Looks to be quite a thorough spec bar one telling particular: there is no version number and no date! The previous version was likewise unnumbered though at least dated as &#8220;September 2005&#8221;. Btw, I’m not sure of there is any archive of XMP specs being maintained by Adobe. At least, I’m not aware of any page with that information. Perhaps we can refer to our [earlier call][2] to have XMP turned over to a standards organization to formalize a public spec.

 [1]: http://www.adobe.com/devnet/xmp/
 [2]: /blog/now-what-about-xmp/