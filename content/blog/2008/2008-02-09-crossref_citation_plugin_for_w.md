---
archives:
- 2008
author: admin
authors:
- admin
categories:
- Blogs
- Citation Formats
- Interoperability
- Metadata
- News Release
date: 2008-02-09
title: Crossref Citation Plugin (for WordPress)
x-version: 0.0.0
---

OK, after a number of delays due to everything from indexing slowness to router problems, I’m happy to say that the first public beta of our [WordPress][1] citation plugin is available for [download via SourceForge][2]. A [Movable Type][3] version is in the works.

And congratulations to Trey at OpenHelix who became laudably impatient, [found the SourceForge entry for the plugin][4] back on February 8th and seems to have been testing it since. He has a nice description of how it works (along with screenshots), so I won’t repeat the effort here.

Having said that, I do include the text of the README after the jump. Please have a look at it before you install, because it might save you some mystification.

<!--more-->

## Description

A WordPress plugin that allows you to search Crossref metadata using citations or partial citations. When you find the reference that you want, insert the formatted and DOI-linked citation into your blog posting along with supporting [COINs][5] metadata. The plugin supports both a long citation format and a short (op. cit.) format.

## Warnings, Caveats and Weasel Words

Please note the following about this plugin:

  1. We are releasing this as a test. It is running on R&#038;D equipment in a non-production environment and so it may disappear without warning or perform erratically. If it isn’t working for some reason, come back later and try again. If it seems to be broken for a prolonged period of time, then please report the problem to us via sourceforge.
  2. There is currently a 20 item limit on the number of hits returned per query. This might seem arbitrary and stingy, but please remember- we are not trying to create a fully blown search engine- we’re just trying to create a citation lookup service. Of course, if, after looking at how the service is used, it looks like we need to up this limit, we will.
  3. If you look in the plugin options (or at the code), you will see that the system includes an API key. At the moment we have no restrictions on use of this service, but have included this in case we need to protect the system from abuse.
  4. The bulk of the functionality we have developed is actually at the back-end. This plugin is just a lightweight interface to that back-end. You can examine the guts of the plugin in order to easily figure out how to create similar functionality for your favorite blog platform, wiki, etc. If you do create something, please let us know. We’d love to see what people are building.
  5. We are continuing to experiment with the metadata search function in order to increase its accuracy and flexibility. Again, this might result in seemingly inconsistent behavior. Did we mention that this is a test?
  6. Please note that this API is not meant for bulk harvesting of Crossref metadata. If you need such facilities, then please look at our web site for information about our metadata services.
  7. The data currently behind the plugin is \*just\* a December 2007 snapshot of our our complete journal article metadata. We have not added books or proceedings yet. We will do so soon and we will start updating the metadata weekly.

We welcome your ideas for tools that we can provide to help researchers. Please, please, please send comments, requests, queries and ideas to us at:

citation-plugin@crossref.org

 [1]: http://wordpress.org/
 [2]: https://sourceforge.net/projects/crossref-cite/
 [3]: http://www.movabletype.org/
 [4]: https://web.archive.org/web/20080216002622/http://www.openhelix.com/blog/?p=128
 [5]: https://web.archive.org/web/20090927174724/http://ocoins.info/