---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Handle
date: 2008-10-01
slug: handle-clients-1-2-3
title: 'Handle Clients #1, #2, #3'
x-version: 0.0.0
---

<img border=0 usemap="#GraffleExport" alt="clients-123.png" src="/wp/blog/images/clients-123-0.png/clients-123.png" width="340" height="294" />

Three alternate clients for viewing a Handle (or DOI): #1 (sky - text), #2 (black - tuples), #3 (white - cards) - the image above is clickable. When Handle clients become JavaScript-able, one really can have it one’s own way. (The JavaScript library is here, the demo service interface here - the code for setting up a new service interface can be got from the [OpenHandle project][5].)

_Noted: As of February 2023, most of the links in this blog are not longer available._


 [5]: https://github.com/tonyhammond/openhandle