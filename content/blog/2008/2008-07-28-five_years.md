---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Identifiers
date: 2008-07-28
title: Five Years
x-version: 0.0.0
---

Oh wow! A rather remarkable plea [here][1] from Dan Brickley on the [public-lod][2] mailing list which calls for the registrant of the [dbpedia.org][3] DNS entry to top it up with another 5+ years worth of clocktime. Some quotes:

> _&#8220;The idea of such a cool RDF namespace having only 6 months left on the DNS registration gives me the worries.&#8221;</p>
>
> &#8220;If you could add another 5-10 years to the DNS registration I’d sleep easier at night.&#8221;</p>
>
> &#8220;Let me stress I’m not suggesting that this domain is actually at risk. Just that the not-at-risk-ness isn’t readily evident from a quick look in the DNS.&#8221;</p>
>
> &#8220;Those in the know are probably confident this is all in hand, but as the SW gets bigger I suspect we ought to establish practices such as &#8220;vocabularies that seek global adoption should always have 5+ years on their DNS registries&#8221;.&#8221;_

Yes, and maybe those cool URIs should have [kite marks][4], too. 😉

(Btw, for those who may not already know the maximum length of time that _any_ DNS name may be leased out in a single registration is 10 years, see the [FAQ][5] put out by ICANN.)

So, pity the poor user of a given semantic web application who may not know what the expectancy is behind the nodes in an RDF graph of assertions. Shifting sands, indeed.

 [1]: http://lists.w3.org/Archives/Public/public-lod/2008Jul/0120.html
 [2]: http://lists.w3.org/Archives/Public/public-lod/
 [3]: http://dbpedia.org/
 [4]: http://en.wikipedia.org/wiki/Kite_mark
 [5]: https://web.archive.org/web/20110318010717/http://www.icann.org/en/faq