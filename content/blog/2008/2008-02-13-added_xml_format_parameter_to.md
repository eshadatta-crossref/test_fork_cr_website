---
archives:
- 2008
author: Chuck Koscher
authors:
- Chuck Koscher
categories:
- XML
date: 2008-02-13
title: Added XML format  parameter to Crossref’s OpenURL resolver
x-version: 0.0.0
---

From the beginning our OpenURL resolver has had a non standard feature of returning metadata in response to a request instead of redirecting to the referrent. This feature returned one of our older XML formats which is a bit limited as to the fields it contains.

Sometime after our resolver was deployed we introduced a more verbose XML format for DOI metadata called &#8216;UNIXREF&#8221;. This was always available to regular queries against the Crossref system but was never introduced to the OpenURL resolver (for no particular reason).

We’ve since learned that some user’s are relying on the OpenURL’s metadata feature to build proper references in situations where they have a DOI and that the older XML format is insufficient. Therefor I’ve added a &#8216;format’ parameter to our OpenURL resolver which allows one to request the more verbose UNIXREF. (see [www.crossref.org/openurl][1])

As always please feel free to contact us regarding new features or changes to existing features that might be helpful.

Regards,

Chuck

 [1]: http://www.crossref.org/openurl