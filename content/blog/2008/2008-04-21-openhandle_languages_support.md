---
archives:
- 2008
author: thammond
authors:
- thammond
categories:
- Handle
date: 2008-04-21
title: 'OpenHandle: Languages Support'
x-version: 0.0.0
---

Following up the earlier [post][1] on [OpenHandle][2], there are now a number of language examples which have been contributed to the project. The diagram below shows the OpenHandle service in schematic with various languages support. Briefly, OpenHandle aims to provide a web services interface to the Handle System to simplify access to the data stored for a given Handle.

(Note that the diagram is an HTML imagemap and all elements are &#8220;clickable&#8221;.)

_Note: this diagram is no longer available online as of 2023. We show the code here for reference._

```
<map name="GraffleExport">
  <area shape=poly coords="302,133,273,117,244,133,266,149,261,157,274,150,302,133" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeLisp"> <area shape=poly coords="359,93,330,77,302,93,324,109,318,117,332,110,359,93" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeFSharp"> <area shape=poly coords="186,93,157,77,129,93,151,109,145,117,159,110,186,93" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeAppleScript"> <area shape=poly coords="244,93,215,77,186,93,208,109,203,117,217,110,244,93" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeCSharp"> <area shape=poly coords="244,174,215,157,186,174,208,189,203,197,217,190,244,174" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodePython"> <area shape=poly coords="244,133,215,117,186,133,208,149,203,157,217,150,244,133" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeJavaScript"> <area shape=poly coords="186,174,157,157,129,174,151,189,145,197,159,190,186,174" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodePhp"> <area shape=poly coords="302,93,273,77,244,93,266,109,261,117,274,110,302,93" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeErlang"> <area shape=poly coords="359,133,330,117,302,133,324,149,318,157,332,150,359,133" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodePerl"> <area shape=poly coords="302,174,273,157,244,174,266,189,261,197,274,190,302,174" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeRuby"> <area shape=poly coords="359,174,330,157,302,174,324,189,318,197,332,190,359,174" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeSmalltalk"> <area shape=poly coords="186,133,157,117,129,133,151,149,145,157,159,150,186,133" href="http://code.google.com/p/openhandle/wiki/OpenHandleCodeJava"> <area shape=poly coords="255,237,255,260,266,260,244,272,222,260,233,260,233,237,222,237,244,225,266,237,255,237" href="http://www.ietf.org/rfc/rfc2616.txt"> <area shape=poly coords="174,527,174,495,196,491,217,495,217,527,196,531,174,527" href="http://hdl.handle.net/"> <area shape=poly coords="270,527,270,495,292,491,314,495,314,527,292,531,270,527" href="http://hdl.handle.net/"> <area shape=poly coords="152,268,210,268,210,300,152,304,152,268" href="http://nascent.nature.com/openhandle/handle?id=10100/nature&#038;mimetype=text/plain&#038;format=rdf"> <area shape=poly coords="201,307,258,307,258,339,201,343,201,307" href="http://nascent.nature.com/openhandle/handle?id=10100/nature&#038;mimetype=text/plain&#038;format=n3"> <area shape=poly coords="267,297,325,297,325,329,267,333,267,297" href="http://nascent.nature.com/openhandle/handle?id=10100/nature&#038;mimetype=text/plain&#038;format=json"> <area shape=poly coords="255,426,255,450,266,450,244,461,222,450,233,450,233,426,222,426,244,414,266,426,255,426" href="http://www.ietf.org/rfc/rfc3651.txt"> <area shape=poly coords="262,355,262,388,226,388,226,355,262,355" href="http://nascent.nature.com/openhandle/handle?id=10100/nature&#038;mimetype=text/plain&#038;format=rdf"> <area shape=poly coords="277,223,292,208,320,208,344,220,330,235,301,235,277,223" href="http://nascent.nature.com/openhandle/handle?id=10.1000/1&#038;mimetype=text/plain&#038;format=json"> <area shape=poly coords="148,244,162,229,191,229,215,241,201,256,172,256,148,244" href="http://nascent.nature.com/openhandle/handle?id=10100/nature&#038;mimetype=text/plain&#038;format=json"> <area shape=poly coords="222,507,222,475,244,471,266,475,266,507,244,511,222,507" href="http://hdl.handle.net/"> <area shape=poly coords="49,215,89,102,191,62,305,79,401,143,393,282,315,350,198,363,105,317,49,215,120,481,140,501,178,501,199,474,186,442,152,436,122,447,120,481,49,215,57,569,79,571,89,547,71,534,53,548,57,569,49,215,57,569" href="http://code.google.com/p/openhandle/">
</map> 
```

 [1]: /blog/openhandle-google-code-project/
 [2]: http://code.google.com/p/openhandle/