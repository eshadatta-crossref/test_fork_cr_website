---
archives:
- 2017
author: Ginny Hendricks
authors:
- Ginny Hendricks
categories:
- Member Briefing
- APIs
- Metadata
- Policy
- Agreements
date: 2017-10-09
draft: false
title: Changes to the 2018 membership agreement for better metadata distribution
x-version: 0.0.0
---

We are making a change to section 9b of the standard Crossref membership agreement which will come into effect on January 1, 2018. This will not change how members register content, nor will it affect membership fees in any way. [The new 2018 agreement is on our website](/membership/2018-agreement/), and the exact wording changes are highlighted below. The new membership agreement will automatically replace the previous version from January 1, 2018 and members will not need to sign a new agreement.

<!--more-->

## What’s changing?

At its July meeting the [Crossref board](/board-and-governance/) unanimously approved recommendations from the [Membership and Fees Committee](/committees/membership-and-fees/) to update Crossref’s metadata delivery offerings. One of the recommendations was to remove the option for case-by-case opt outs of metadata delivery through the OAI-PMH channel used for Enhanced Crossref Metadata Services.

This opt-out was only used by a small number of our members (around 40 of nearly 9,000), who have been contacted directly. This means that for the vast majority of members there is no change in how Crossref makes their metadata available but we wanted to make everyone aware of the change to the membership agreement.

So, as is currently the case, all metadata registered with Crossref is available via all the Metadata APIs under an appropriate agreement with the user or terms and conditions for the service. The one exception to this is how references are distributed - we will contact members next week about the options for references.


## Why are we making this change?

1. Our [metadata services](/services/metadata-retrieval/) have become very popular with users of all kinds throughout scholarly communications--including search and discovery platforms, libraries, other publishers, reference managers, sharing services, and analytics providers. More and better metadata means more and better discoverability of publisher content.
2. The change also brings this service into line with our mission to improve scholarly communications through quality metadata and related infrastructure services, removing the need for bilateral agreements between publishers and third parties.
3. Many members complained when we contacted them about opt-outs whenever a new OAI-PMH user came on board. It is better for our members and for our staff if there is a common standard across the board.


## Changes to 2018 membership agreement

**9) Sharing of Metadata by PILA**

a) _Local Hosting_. [no change]

b) _Other Metadata Services_. Subject to compliance **by the entity receiving the Metadata and Digital Identifiers** with the terms and conditions ~~set forth in a separate agreement between~~ **established by** PILA **for the particular service through which access is provided,** and ~~the entity receiving the Metadata and Digital Identifiers~~, PILA may ~~license~~ **authorize** third parties to receive and use ~~bulk deliveries of~~ Metadata and Digital Identifiers from ~~the~~ PILA ~~System from members who have chosen to participate in Metadata Services,~~ which PILA shall provide directly to such third parties. ~~At least thirty (30) days prior to making such Metadata delivery PILA will notify each PILA Member whose Metadata and Digital Identifiers are intended to be included in such delivery of the anticipated delivery date, the identity of the third party and the purpose for which the delivery is being made. Metadata and Digital Identifiers belonging to any PILA Member who notifies PILA in writing prior to the specified delivery date of its desire to be excluded from such delivery will be excluded or removed from such delivery.~~

---

Please [contact our membership specialist](mailto:member@crossref.org) if you have any feedback or questions.