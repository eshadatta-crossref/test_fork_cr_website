---
archives:
- 2017
author: Jennifer Lin
authors:
- Jennifer Lin
categories:
- Peer Review
- Content Types
- Schema
- Member Briefing
date: 2017-10-24
draft: false
title: Peer reviews are open for registering at Crossref
x-version: 0.0.0
---

[About 13-20 billion researcher-hours](https://authorservices.taylorandfrancis.com/peer-review-global-view/) were spent in 2015 doing peer reviews. What valuable work! Let's get more mileage out of these labors and make these expert discussions citable, persistent, and linked up to the scholarly record. As we [previously shared](/blog/making-peer-reviews-citable-discoverable-and-creditable/) during Peer Review week, Crossref is launching a new content type to support the registration of peer reviews. We’re one step closer to changing that. Today, we are excited to announce that we’re open for deposits.

<!--more-->

{{% imagewrap left %}}
<img src="/images/blog/televisionset.png" alt="tv set" width="60px" class="img-responsive" />
{{% /imagewrap %}}

If you missed the first episode, here’s a recap:

Publishers have been registering reviews with us for a while (ex: [Example 1](https://doi.org/10.1016/j.engfracmech.2015.01.019), [Example 2](https://doi.org/10.5194/wes-1-177-2016), and [Example 3](https://doi.org/10.14322/PUBLONS.R518142)). But these have been shoehorned into other content: article, dataset, or component. So we are extending Crossref’s infrastructure to properly treat this special scholarly artifact. This includes a range of outputs made publicly available from the peer review history (referee reports, decision letters, author responses, community comments) across any and all review rounds. We welcome scholarly discussions of journal articles before or after publication (e.g. “post-publication reviews”).

We collect metadata that characterizes the peer review asset (for example: recommendation, type, license, contributor info, competing interests). We also collect metadata, which offers a view into the review process (e.g. pre/post-publication, revision round, review date).

This special set will support the discovery and investigation of peer reviews as it is linked up to the article discussed. It will also enable the following:

* Enable tracking of the evolution of scholarly claims through the lineage of expert discussion
* Support enrichment of scholarly discussion
* Enable reviewer accountability
* Credit reviewers and editors for their scholarly contribution
* Support publisher transparency
* Connect reviews to the full history of the published results
* Provide data for analysis and research on peer review

Please come check out our [documentation on the new Crossref content type](https://support.crossref.org/hc/en-us/articles/115005255706) for more information.

As publishers are implementing this, we are finishing up the delivery of this metadata for machine and human access, across all the Crossref interfaces ([REST API](https://api.crossref.org/), [OAI-PMH](https://support.crossref.org/hc/en-us/articles/213679866-OAI-PMH-subscriber-only-), [Crossref Metadata Search](https://search.crossref.org/)) to enable discoverability across the research ecosystem. We are also working to make it possible for members to get Cited-by data for the peer reviews they register.


If you are interested in registering your peer review content with us, please [get in touch](mailto:support@crossref.org).