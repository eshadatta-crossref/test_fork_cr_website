---
archives:
- 2017
author: Lisa Hart Martin
authors:
- Lisa Hart Martin
categories:
- Board
- Member Briefing
- Governance
- Election
- Crossref LIVE
date: 2017-04-28
draft: false
title: Want to be on our Board?
x-version: 0.0.0
---

> Do you want to affect change for the scholarly community?

Our Nominating Committee is inviting expressions of interest to serve on the Board as it begins its consideration of a slate for the November 2017 election. <!--more-->

Key responsibilities of the Board are setting the strategic direction for the organization, providing financial oversight, and approving new policies and services. Some of the decisions the board has made in recent years include:

* Establishing [The OI Project](/blog/the-oi-project-gets-underway-planning-an-open-organization-identifier-registry) to create a persistent Organization Identifier;
* Inclusion of [preprints in the Crossref metadata](/news/2016-11-02-crossref-now-accepts-preprints); and
* The approval to [develop Event Data](/services/event-data) which will track online activity from multiple sources.<!--more-->

## Any member can express interest in serving on the Board

We are seeking people who know about scholarly communications and would like to be part of our future. If you have a vision for the international Crossref community, we are interested in hearing from you. Crossref members that are eligible to vote, and would like to be considered, can [express their interest](https://docs.google.com/forms/d/e/1FAIpQLSdwqraD2fjb3eqZgLpTQWsMYPQvvz4LARLq6k8H8mA7xGbZAw/viewform) together with statements of interest from you and from your organization. The form should be completed and sent to us before 01 June 2017.

## The role of the Nominating Committee

The Nominating Committee meets to discuss change, process, criteria, and potential candidates, ensuring a fair representation of membership. The Committee is made up of three board members not up for election, and two non-board members.

Current Nominating Committee members:

* John Shaw, Sage (Chair)
* Mark Patterson, eLife
* Paul Peters, Hindawi
* Chris Fell, Cambridge University Press
* Rebecca Lawrence, F1000 Research

## About the election and our Board

We have a principle of [one member, one vote](/truths); our board comprises a cross-section of members and it doesn’t matter how big or small you are, every member gets a single vote. Board terms are three years, and one third of the Board is eligible for election every year. There are six seats up for election in 2017. The board meets in a variety of international locations in March, July, and November each year. View a list of the [current Crossref Board members and a history of the decisions they’ve made (motions)](/board-and-governance). The election opens online in late September 2017 and voting is done by proxy online or in person at the annual business meeting during Crossref LIVE in November 2017. Election materials and instructions for voting will be available to all Crossref members online in late September 2017. The board needs to be truly representative of Crossref’s global and diverse membership of organizations who publish.

Please [express interest using the form](https://docs.google.com/forms/d/e/1FAIpQLSdwqraD2fjb3eqZgLpTQWsMYPQvvz4LARLq6k8H8mA7xGbZAw/viewform), or [email me](mailto:lhart@crossref.org) with any questions.