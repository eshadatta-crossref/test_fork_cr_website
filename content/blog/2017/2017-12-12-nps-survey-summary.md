---
archives:
- 2017
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
- Ginny Hendricks
categories:
- Member Experience
- User Experience
- Net Promoter Score
- Community
- Survey
date: 2017-12-11
draft: false
title: And our survey says...
x-version: 0.0.0
---

Earlier this year we sent out a short survey inviting members to rate our performance. We asked what you think we do well, what we don’t do so well, and one thing we could do to improve our rating.

<!--more-->

We were delighted to receive 313 responses and relieved that 93% of those were positive (phew!). It was very useful to hear your thoughts and to get such a variety of comments covering Product, Outreach, Marketing and Member Experience. There were a few recurring themes, three of which we’d like to address here:

## 1. Providing information in different languages
Not surprisingly, given the growing diversity of our member base, some respondents asked us to share information in languages other than English. We have been aware of this growing need for some time and have been working on a few developments in this area:

* In January 2018 we will be launching a series of seven service videos in six different languages—French, Spanish, Brazilian Portuguese, Chinese, Korean, and Japanese.
* January also sees the launch of a new initiative called the Ambassador Program. Ambassadors will work closely with Crossref to help spread the word about our services, and support our global members in their own languages.
* During 2017 we hosted two webinars in Brazilian Portuguese and one in Turkish, and aim to increase this in 2018.

## 2. Member-to-member discussion forum
Some respondents asked for a facility to enable members to reach out to each other, giving direct opportunity for discussions and/or sharing experiences online (and in their own languages). We have been working for a few months now to provide a member-to-member discussion area, which is planned for 2018. Following a soft launch covering a few areas/topics, we’ll broaden the scope to include technical support, too.

## 3. Registering metadata more easily using the web deposit form
Many respondents requested a more user-friendly process for registering metadata through our webform. Our Product and DevOps teams have been working on this for some time and have created a new interface called the Metadata Manager, which is currently in Beta but scheduled to launch in Q1 of 2018.

Finally, we’d like to thank you for participating in our survey. Your valuable feedback and suggestions help us understand your experience, improve our service, shape the course of particular projects and even direct our future strategy.

_As this survey was anonymous, we are unable to respond to anyone on an individual basis, however, if you’d like to have your particular comments addressed, [we would love to hear from you directly.](mailto:feedback@crossref.org)_