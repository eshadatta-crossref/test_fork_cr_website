---
archives:
- 2017
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
categories:
- Community
- Event Data
date: 2017-09-26
draft: false
title: BestBlogsRead
x-version: 0.0.0
---

We know that **research communication happens everywhere**, and we want your help in finding it!

From October 9th we will be collecting links sent in by you through a social campaign across Twitter and Facebook called **#BestBlogsRead.**

**Simply send us links to the blogs YOU like to read**
It’s easy to participate, all you have to do is watch out for the daily tweets and facebook posts and then send us links to the blogs (and news sites) you read.

From gardening to gaming, recipes to rock climbing, tennis to taxidermy - whatever blogs you read, we want to hear about them!

**Because research happens everywhere!**
And you’ll be surprised where it **is** mentioned - for example:

* We found [a Wiley](http://onlinelibrary.wiley.com/doi/10.1111/j.1600-0498.1969.tb00136.x/abstract?systemMessage=Wiley+Online+Library+will+be+unavailable+on+Saturday+7th+Oct+from+03.00+EDT+%2F+08%3A00+BST+%2F+12%3A30+IST+%2F+15.00+SGT+to+08.00+EDT+%2F+13.00+BST+%2F+17%3A30+IST+%2F+20.00+SGT+and+Sunday+8th+Oct+from+03.00+EDT+%2F+08%3A00+BST+%2F+12%3A30+IST+%2F+15.00+SGT+to+06.00+EDT+%2F+11.00+BST+%2F+15%3A30+IST+%2F+18.00+SGT+for+essential+maintenance.+Apologies+for+the+inconvenience+caused+.) article mentioned in a blog about [the eclipse](http://www.atlasobscura.com/articles/thales-predicts-eclipse-mystery-ancient-greece?utm_source=Atlas+Obscura+Daily+Newsletter&utm_campaign=810eff404b-EMAIL_CAMPAIGN_2017_08_09&utm_medium=email&utm_term=0_f36db9c480-810eff404b-66765933&ct=t(Newsletter_8_9_2017)&mc_cid=810eff404b&mc_eid=4e0067d656)

* An [American Chemical Society](http://pubs.acs.org/doi/abs/10.1021/tx9002726) article in a blog about [food allergies ](http://www.allergy-insight.com/free-from-at-bellavita/)

* A blog about Neanderthals on the [Atlantic](https://www.theatlantic.com/science/archive/2017/09/neanderthals-lost-history/540507/) links to and article from the [American Association for the Advancement of Science](http://doi.org/10.1126/science.1174462)

So, watch out for the campaign on Twitter and Facebook, and tell us about your #BestBlogsRead.