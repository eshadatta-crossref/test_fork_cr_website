---
archives:
- 2017
author: Jennifer Kemp
authors:
- Jennifer Kemp
categories:
- Event Data
- Identifiers
- Citation
- Collaboration
- DataCite
- Wikipedia
- Bibliometrics
date: 2017-07-05
draft: false
title: Event Data enters Beta
x-version: 0.0.0
---

We’ve been talking about it at events, blogging about it on our site, living it, breathing it, and even sometimes dreaming about it, and now we are delighted to announce that Crossref Event Data has entered Beta. <!--more-->

<img src="http://assets.crossref.org/logo/crossref-event-data-logo-200.svg" alt="Crossref Event Data logo" width="200" height="83" />



A collaborative initiative by Crossref and DataCite, Event Data offers transparency around the way interactions with scholarly research occur online, allowing you to discover where it’s bookmarked, linked, liked, shared, referenced, commented on etc., across the web, and beyond publisher platforms.

The name Event Data reflects the nature of the service, as it collects and stores digital actions that occur on the web, from the quick and simple, such as bookmarking and referencing, through to deeper interconnectivity such as exposing the links between research artifacts. Each individual action is timestamped and recorded in our system as an Event, and made available to the community via an API.

Event Data will be available for absolutely anyone to use; publishers, third party vendors, editors, bibliometricans, researchers, authors, funders etc., and with tens of thousands of events occurring every day, there’s a wealth of insight to be gained for those interested in analyzing and interpreting the data.

It’s important to note that Event Data does not provide metrics. What is does provide is the raw data to help you facilitate your own analysis, giving you the freedom to integrate the data into your own systems.

We are currently working very closely with a few organizations with specific use cases who are helping us to test and refine Beta before we launch our production service later this year. If you decide to take a look at Beta yourself, all the data you collect from Event Data is licensed for public sharing and reuse [according to our Terms of Use.](/services/event-data/terms/)

*Until Event Data is in production mode, we do not recommend building any commercial or customer-based tools off the data.*
 
If you are not in the Beta test group but are interested in participating, please contact me below. For more information about Event Data, [please see our user guide.](https://www.eventdata.crossref.org/guide/index.html)

Please contact me, [Jennifer Kemp](mailto:eventdata@crossref.org)---Outreach Manager for Event Data---with any questions.