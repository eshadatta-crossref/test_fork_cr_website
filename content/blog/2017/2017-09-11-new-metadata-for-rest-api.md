---
archives:
- 2017
author: Kirsty Meddings
authors:
- Kirsty Meddings
categories:
- APIs
- Citation
- Preprints
- News Release
date: 2017-09-11
draft: false
title: More metadata for machines-citations, relations, and preprints arrive in the
  REST API
x-version: 0.0.0
---

Over the past few months we have been adding to the metadata and functionality of our <a href="https://api.crossref.org" target="_blank">REST API</a>, Crossref’s public machine interface for the metadata of all 90 million+ registered content items. Much of the work focused on a review and upgrade of the API’s code and architecture in order to better support its rapidly growing usage. But we have also extended the <a href="https://github.com/CrossRef/rest-api-doc/blob/master/api_format.md" target="_blank">types of metadata</a> that the API can deliver.

<!--more-->

One of the biggest changes is that [references](/services/reference-linking/) are now available if the publisher has made them public (a simple [email instruction](mailto:support@crossref.org) to us). Currently 45% of all publications with deposited references are now accessible. For example:

* [This article](http://doi.org/10.1073/pnas.1402289111) studying fluid ejection from animals has 55 references and they are all in the [metadata here](https://api.crossref.org/v1/works/10.1073/pnas.1402289111). You can also see that the article has an `is-referenced-by` count of 6.

* [This article](https://doi.org/10.1371/journal.pone.0070585) exploring whether people bitten by their cat are more likely to develop depression has 142 references and is [referenced by 12](https://api.crossref.org/v1/works/10.1371/journal.pone.0070585).

We recently announced that we would be [accepting preprints](/blog/preprints-are-go-at-crossref/), and the metadata for 15,000 preprints [registered to date](https://api.crossref.org/v1/works?facet=type-name:*&rows=0) is now in the API, labelled as `posted-content`. Over 4,000 have been subsequently published in a journal, and the Crossref metadata now links these preprints to their respective articles (and vice versa). For example [this article](https://doi.org/10.1101/098947) in Biorxiv has since been [published in a journal](http://dx.doi.org/10.1093/molbev/msx056), and this relationship is recorded in its [metadata](https://api.crossref.org/v1/works/10.1101/098947) as `is-preprint-of`.

### Also new to the API:

* Cited-by counts - the number of times each work has been referenced by other content registered with us. Look for [`is-referenced-by-count`](https://api.crossref.org/v1/works/10.1063/1.4870777) within a record.

* [This article](https://doi.org/10.1038/171737a0) from 1953 about a fairly notable discovery has been [cited 4832 times](https://api.crossref.org/v1/works/10.1038/171737a0), but the two [most](https://api.crossref.org/v1/works/10.1038/227680a0) [cited](https://api.crossref.org/v1/works/10.1016/0003-2697(76)90527-3) articles both have over 100,000 citations and thousands have been cited more than Watson and Crick.

* Abstracts for over [1 million works](https://api.crossref.org/v1/works?query=has-abstract:true&rows=0).

* Similarity Check URLs--the ones that Turnitin crawl to add content to the database--are now showing so that participating publishers can check that they are including them in their [metadata deposits](https://api.crossref.org/v1/works/10.5740/jaoacint.10-223).

* Subject categories have been added for an additional 7000 journal titles, taking the total number of classified titles to ~45,000.

Are you already using our Metadata APIs for your system or project? We’re always keen to [hear new use cases and happy to answer any questions](mailto:feedback@crossref.org).

*You may need to install a JSON viewer extension in your browser to render API queries in a human-friendly way.*