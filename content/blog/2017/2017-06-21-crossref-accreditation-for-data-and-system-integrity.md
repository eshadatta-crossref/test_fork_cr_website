---
archives:
- 2017
author: Lisa Hart Martin
authors:
- Lisa Hart Martin
categories:
- Governance
- Security
- Data Management
date: 2017-06-21
draft: false
title: Crossref receives SOC accreditation for data integrity and security
x-version: 0.0.0
---

We are delighted to announce that Crossref has been awarded the Service Organization Control (SOC) 2® accreditation after an independent assessment of our controls and procedures by the American Institute of CPA’s (AICPA).

 <!--more-->

{{% imagewrap right %}} <img src="/images/blog/soc-logo-2.jpg" alt="SOC logo" width="200px" /> {{% /imagewrap %}}

The SOC 2® accreditation is awarded to service organizations that have passed standard trust services criteria relating to the security, availability, and processing integrity of systems used to process users’ data and the confidentiality and privacy of the information processed by these systems.

The AICPA’s assessment also reviewed our vendor management programs, internal corporate governance and risk management processes, and regulatory oversight.

[Find out more about the SOC accreditation structure](https://www.ssae-16.com/soc-2/)