---
archives:
- 2017
author: Vanessa Fairhurst
authors:
- Vanessa Fairhurst
categories:
- Outreach
- Crossref LIVE
- Community
date: 2017-10-22
draft: false
title: What happened at last month's LIVE local in London
x-version: 0.0.0
---

So much has happened since we held LIVE16 (our annual meeting) in London last year that we wanted to check-in with our UK community and share the year’s developments around our tools, teams and services ahead of [LIVE17](/crossref-live-annual) next month in Singapore.

<!--more-->
And so, on 26th September we held a half-day '[LIVE local](/events)', covering a wide range of strategic topics, well-attended by a diverse representation of our UK community of publishers, funders, researchers, and tool-makers.

What we discussed on the day:


* Ed Pentz, Crossref's Executive Director, kicked the day off with ['What’s new at Crossref'](https://www.slideshare.net/CrossRef/whats-new-at-crossref-ed-pentz-london-live-2017)
* Geoffrey Bilder, Strategic Director, talked us through ['Crossref's Strategic Initiatives'](https://www.slideshare.net/CrossRef/new-initiatives-geoffrey-bilder-london-live-2017)
* Ginny Hendricks, Director of Member and Community Outreach introduced ['Metadata 2020'](https://www.slideshare.net/CrossRef/metadata-2020-ginny-hendricks-london-live-2017)
* Rachael Lammey, Head of International Outreach discussed the ['Global reach of Crossref metadata'](https://www.slideshare.net/CrossRef/global-reach-of-crossref-metadata-rachael-lammey-london-live-2017)
* Jure Triglav from Coko Foundation presented some interesting ['Metadata Use Case Studies'](http://slides.com/jure/metadata-collaboration)
* Jennifer Lin, Director of Product Management, spoke about Crossref's ['New Product Developments'](https://www.slideshare.net/CrossRef/new-product-developments-jennifer-lin-london-live-2017)
* Ed Pentz concluded the day leading a discussion on ['Crossref's Future Direction'](https://www.slideshare.net/CrossRef/crossref-future-direction-ed-pentz-london-live-2017)

<iframe src="/pdfs/crossref-london-live.pdf" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0"></iframe>

This event was one in a series of smaller, regional events which aim to better cater to our global membership and provide a tailored program of activities. You can read more about this series of events on our [LIVE locals](/events) page, and if you are interested in hosting an event near you or have suggestions for one in your region then please [contact me](mailto:feedback@crossref.org) to get involved.