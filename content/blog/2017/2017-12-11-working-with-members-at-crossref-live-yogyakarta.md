---
archives:
- 2017
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- Outreach
- Crossref LIVE
- Member Experience
- Education
- Community
- Collaboration
date: 2017-12-11
draft: false
title: Working with universities at Crossref LIVE Yogyakarta
x-version: 0.0.0
---

Following on from our LIVE Annual Meeting in Singapore, my colleague, Susan Collins, and I held a local LIVE event in Yogyakarta thanks to support from Universitas Ahmad Dahlan (UAD), Universitas Muhammadiyah Sidoarjo and one of Crossref's new Sponsoring Affiliates, Relawan Jurnal Indonesia.

<!--more-->
Over the past two years, we've seen accelerated growth in our membership in Asia Pacific (making up a quarter of all new members in the last two years). A lot of those new members have come from Indonesia, so it was great to have the opportunity to meet up, answer questions and to share knowledge between all our different organizations.

{{% divwrap align-right %}}<img src="/images/blog/2017/yogyakarta-blog.jpg" alt="graph of number of new members per region" width="250px"/>{{% /divwrap %}}

We welcomed speakers such as Dr. Muhammad Dimyati, from the Directorate General of Strengthening for Research and Development, Ministry of Research, Technology and Higher Education. Dr. Dimyati talked about the importance of Indonesian research and presented statistics on its growth, but also its coverage in different databases like Scopus and DOAJ.

Dr. Lukman from LIPI, the Indonesian Institute of Sciences also joined us to explain the importance of identifiers within the research ecosystem. As any identifier buff will know, we're keen to talk more about how organizations are using Crossref metadata and identifiers, and the importance of providing good, complete metadata ([Metadata2020](http://www.metadata2020.org/)) so this, plus a remote presentation from Nobuko Miyari from [ORCID](https://orcid.org/) helped provide great context for the day.

Metadata and identifiers are of course just one part of the process, and Mr. Tole Sutikno from UAD gave an overview of good practice publishing by looking  at some of the wider issues that journal editors (and researchers) need to know.

We had time in the afternoon to talk to our audience about Crossref - our different services, OJS integrations, funding data and our APIs, and thanks to our moderators we were able to take lots of questions from members who had specific questions about Crossmark, Cited-by and depositing references.

{{% divwrap align-right %}}<img src="/images/blog/2017/yogyakarta2-blog.jpg" alt="image of stage" width="250px" />{{% /divwrap %}}

A few weeks later, and I'm still absorbing all of the things that happened on our (too) quick trip to Yogyakarta.

Thanks again to our members and hosts for attending the event and sharing their questions, ideas and plans with us, and we plan to come back to continue to build on these in future.