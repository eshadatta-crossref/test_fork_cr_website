---
archives:
- 2017
author: Lisa Hart Martin
authors:
- Lisa Hart Martin
categories:
- Board
- Member Briefing
- Governance
- Election
- Crossref LIVE
- Annual Meeting
date: 2017-08-17
draft: false
title: 2017 election slate
x-version: 0.0.0
---

### Slate of 2017 board candidates announced, and it’s going to be exciting

Crossref is always evolving and the board knows it must evolve with us so we can continue to provide the right kind of services and support for you, as members of the research community.

This year two things happened for the first time: we used our updated bylaws [see article VII, section 2](/board-and-governance/bylaws/) agreed by the board last year, to allow more candidates than available seats; and secondly, to issue an [open call for expressions of interest](/blog/want-to-be-on-our-board). Many members of the current board felt it was vital to move to this more transparent process.

With Crossref developing new services for new types of members at a rapid pace, it’s an exciting time to be on the board of directors. With 25 expressions of interest it seems we’re not the only ones who think so!

From these 25 applications, the Nominating Committee has proposed the following nine candidates to fill the six seats open for election to our board of directors:

**American Institute of Physics (AIP)**, Jason Wilde, USA<br>
**F1000 Research**, Liz Allen, UK<br>
**Institute of Electronic and Electrical Engineers (IEEE)**, Gerry Grenier, USA<br>
**The Institution of Engineering and Technology (IET)**, Vincent Cassidy, UK<br>
**Massachusetts Institute of Technology Press (MIT Press)**, Amy Brand, USA<br>
**OpenEdition**, Marin Dacos, France<br>
**SciELO**, Abel Packer, Brazil<br>
**SPIE**, Eric Pepper, USA<br>
**Vilnius Gediminas Technical University Press (VGTU Press)**, Eleonora Dagiene, Lithuania<br>

### [Read the candidates’ organizational and personal statements](/board-and-governance/elections/2017-slate)

<br>Candidates were chosen based on the following criteria:

 - That board representation should be reflective of membership<br>
 - A balance of types and sizes of organizations<br>
 - That all committee choices and recommendations were unanimous<br>

## You can be part of this important process, by voting in the election
If your organization is a member of Crossref on September 15 2017, you are eligible to vote when voting opens on September 28 (affiliates, however, are not eligible to vote).

## How can you vote?
On September 28, your organization’s designated voting contact will receive an email with a link to the formal Notice of Meeting and Proxy Form with concise instructions on how to vote.  An additional email will be sent with a username and password along with a link to our online voting platform. It is important to make sure your voting contact is up-to-date.

## Want to add your voice?
We are accepting independent nominations until 7 November 2017. Organizations interested in standing as an independent candidate should contact me by this date with the endorsements of ten other Crossref members.

The election itself will be held at [LIVE17 Singapore](/crossref-live-annual), our annual meeting, on 14 November 2017. We hope you’ll be there to hear the results.