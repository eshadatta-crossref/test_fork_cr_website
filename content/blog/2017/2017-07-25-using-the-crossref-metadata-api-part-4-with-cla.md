---
archives:
- 2017
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- APIs
- Research Funders
- Identifiers
- Interoperability
- API Case Study
date: 2017-07-25
title: Using the Crossref REST API. Part 4 (with CLA)
x-version: 0.0.0
---

As a follow-up to our [blog posts](/categories/api-case-study/) on the Crossref REST API we talked to the Copyright Licensing Agency (CLA) about the work they’re doing, and how they’re using the Crossref REST API as part of their workflow.
<!--more-->

<span ><b>Alex Cole, Senior Business Analyst at the Copyright Licensing Agency introduces the DCS</b></span>

The Digital Content Store (DCS) is an innovative rights, technology and content platform for UK Higher Education Institutions (HEIs), which was developed collaboratively with HEIs, publishers and technology partners. The platform is included in the CLA annual licence fee and is an optional tool for licensees.

At its core, the system is a searchable repository of digital copies that have been created under the licence by HEIs (the CLA Digital Content Store), it also functions as a workflow management tool.  When extracts are digitised by HEIs under the CLA Licence, they are uploaded directly to the DCS. Once an extract is uploaded and assigned to a course, students are able to access the extract via a secure link. Every year HEIs are obliged to report all of these digitised items to CLA as part of the terms of their copyright blanket licence. Prior to the DCS, HEIs were having to submit this data manually, a process that could take days, if not weeks. The system removes the need for annual census reporting to CLA, reducing the data collection burden on the HE sector and creating administrative efficiencies through streamlining the digital course pack creation process.<br>

<span ><b>Can you talk about how you're using the [Crossref REST API](https://www.cla.co.uk/blog-crossref-api#_msocom_1) within CLA Digital Content Store (DCS)?</b></span>

When a DCS user adds a new extract to a course they need to include relevant metadata. This metadata is necessary, as it ultimately helps CLA in correctly identifying the copyright owner of the extract so that we can make sure they receive fair payment in our royalties distributions.
The Crossref REST API supplies the DCS user with article and journal metadata so that they can provide the correct information about the content they are uploading. Using the API saves the user the time they would have otherwise spent searching for this data, streamlining their workflow and making the process more efficient.

Searching for and adding content in the DCS
<img src="/images/blog/CLA_blog.jpg" alt="Screen shot" class="img-responsive"/>


<span ><b>What are your future development plans?</b></span>

We’re continuing to develop the DCS in order to improve user experience for our customers. We’re currently looking into opening up access for our users by allowing academics to submit requests to
the DCS via a web-form and our own DCS Course Content URL API. We are also looking into incorporating the Crossref REST API into some of our back office workflows to improve efficiency and simplify our workflow. The metadata that we can retrieve from Crossref can help us match customer usage to our rights database.

<span ><b>What else would you like to see in [Crossref metadata](https://www.cla.co.uk/blog-crossref-api#_msocom_1)?</b></span>

Going forward we’d like to see:<br>
* More books included in the database.<br>
* Indicating if an ISSN is associated with the print or digital edition of a journal.<br><br>
Thanks Alex!