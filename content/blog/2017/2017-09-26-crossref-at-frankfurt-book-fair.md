---
archives:
- 2017
author: Christine Cormack Wood
authors:
- Christine Cormack Wood
categories:
- Meetings
- Community
date: 2017-09-26
draft: false
title: Crossref at the Frankfurt Book Fair
x-version: 0.0.0
---

We’ll be at booth M82 in the Hotspot area of Hall 4.2 and would love to meet with you. Let us know if you’re interested in chatting with one of us - about anything at all.

<!--more-->

[Kirsty Meddings](mailto:kmeddings@crossref.org), **Product Manager**: Here to help with Crossref services such as Crossmark and funding data, and happy to talk about your metadata and how you can deposit more.

[Paul Davis](mailto:pdavis@crossref.org), **Support Specialist**: Any issues with metadata deposit, or anything technical, I’m your man.

[Susan Collins](mailto:scollins@crossref.org), **Publisher Outreach Manager:** If you’re a member and have questions about how things are going, or try out additional services, I can help.

[Jennifer Kemp](mailto:jkemp@crossref.org), **Affiliate Outreach Manager**: Come to me if you want to get Metadata from Crossref, or discuss our imminent new service for social mentions and data links: [Event Data (in Beta)](/blog/event-data-enters-beta).

[Ginny Hendricks](mailto:ghendricks@crossref.org), **Member & Community Outreach Director**: I’d love to talk to publishers and platforms about the new [Metadata 2020](http://www.metadata2020.org) initiative.

[Amanda Bartell](mailto:abartell@crossref.org), **Head of Member Experience**: This will be my first day at Crossref! If there is something you’d like the Membership team to do or change, please let me know.

[Chrissie Cormack-Wood](mailto:ccormackwood@crossref.org), **Head of Marketing Communications**: I’ll be acting as "host" so ask me anything about our booth and activities at the Fair. Ideas for joint campaigns or co-promotion are welcome too.

If some of these topics are on your agenda, or if you’re not sure who to contact, [please let me know](mailto:ccormackwood@crossref.org) and I’ll set up a 30-minute meeting at our booth, M82 in Hall 4.2.

{{% divwrap blue-highlight %}}And, if you don’t get a chance to visit us at our stand, make sure you don’t miss Ginny’s [Metadata 2020](http://www.metadata2020.org) talk at 2.30pm on Wednesday 11th, at the Hot Spot stage in the corner of Hall 4.2, area N99.{{% /divwrap %}}

We hope you have a great Book Fair!