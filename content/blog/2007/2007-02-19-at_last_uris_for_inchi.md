---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Web
- InChI
date: 2007-02-19
title: At Last! URIs for InChI
x-version: 0.0.0
---

The [info registry][1] has now added in the [InChI][2] namespace (see registry entry [here][3]) which now means that chemical compounds identified by InChIs ([IUPAC][4]&#8216;s International Chemical Identifiers) are expressible in URI form and thus amenable to many Web-based description technologies that use URI as the means to identify objects, e.g. XLink, RDF, etc. As an example, the InChI identifier for [naphthalene][5] is

InChI=1/C10H8/c1-2-6-10-8-4-3-7-9(10)5-1/h1-8H

and can now be legitimately expressed in URI form as

info:inchi/InChI=1/C10H8/c1-2-6-10-8-4-3-7-9(10)5-1/h1-8H

The info URI scheme exists to support legacy namespaces get a leg up onto the Web. Registered namespaces include PubMed identifiers, DOIs, handles, ADS bibcodes, etc. Increasingly we’ll be expecting to see identifiers (both new and old) represented in a common form - URI.

 [1]: http://info-uri.info/
 [2]: http://iupac.org/inchi/
 [3]: http://info-uri.info/registry/OAIHandler?verb=GetRecord&#038;metadataPrefix=reg&#038;identifier=info:inchi/
 [4]: http://iupac.org/
 [5]: http://en.wikipedia.org/wiki/Naphthalene