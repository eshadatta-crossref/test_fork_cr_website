---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Identifiers
date: 2007-10-01
title: Using ISO URNs
x-version: 0.0.0
---

(**Update - 2007.10.02:** Just realized that there were some serious flaws in the post below regarding publication and form of namespace URIs which I’ve now addressed in a subsequent post [here][1].)
  
By way of experimenting with a use case for ISO URNs, below is a listing of the document metadata for an arbitrary PDF. (You can judge for yourselves whether the metadata disclosed here is sufficient to describe the document.) Here, the metadata is taken from the information dictionary and from the document metadata stream (XMP packet).
  
The metadata is expressed in RDF/N3. That may not be a surprise for the XMP packet which is serialized in RDF/XML, as it’s just a hop, skip and a jump to render it as RDF/N3 with properties taken from schema whose namespaces are identified by URI. What may be more unusual is to see the document information dictionary metadata (the &#8220;normal&#8221; metadata in a PDF) rendered as RDF/N3 since the information dictionary is not nodelled on RDF, not expressed in XML, and not namespaced. Here, in addition to the trusty HTTP URI scheme, I’ve made use of two particular URI schemes: &#8220;[iso:][2]&#8221; URN namespaces, and &#8220;[data:][3]&#8221; URIs.
  
(Continues.)

<!--more-->


  
As far as I am aware, there is no formal identifier for entries in the document information dictionary as specified by the [PDF Reference][4] from Adobe Systems, so it may be appropriate to use the HTTP URI for the Adobe homepage for the PDF Reference manual, from which specific editions are available.
  
For the PDF/X keys which are specified in the ISO standard ISO 15930-1 2001, I have used an ISO URN. (I don’t expect this to be correct in all details but it should give some idea of how it might be used. It may be that the URI should express the term itself, rather than the document from which the term was defined.) And finally, for the one additional user-supplied key here I have made use of a &#8220;data:&#8221; URI with no body (i.e. I’m speechless). One could have provided some text within the body of the &#8220;data:&#8221; URI if one wanted to differentiate between alternate user keys or to otherwise annotate these keys.
  
Note that the prefixes used in the information dictionary and in the metadata stream are unrelated, as are the mappings of property elements to schemas.
  
Well, that’s all really just for fun but it may show two things: 1) how a general description might be described with RDF and how general properties can be mapped to URIs (with possibly limited machine utility), and 2) how an ISO URN might be used.

<pre># document infodict (object 58: 476983):
@prefix: pdfx: &lt;urn:iso:std:iso-iec:15930:-1:2001&gt; .
@prefix: pdf: &lt;http://www.adobe.com/devnet/pdf/pdf_reference.html&gt; .
@prefix: _usr: &lt;data:,&gt; .
&lt;>   _usr:Apag_PDFX_Checkup "1.3";
pdf:Author "Scott B. Tully";
pdf:CreationDate "D:20020320135641Z";
pdf:Creator "Unknown";
pdfx:GTS_PDFXConformance "PDF/X-1a:2001";
pdfx:GTS_PDFXVersion "PDF/X-1:2001";
pdf:Keywords "PDF/X-1";
pdf:ModDate "D:20041014121049+10'00'";
pdf:Producer "Acrobat Distiller 4.05 for Macintosh";
pdf:Subject "A document from our PDF archive. ";
pdf:Title "Tully Talk November 2001";
pdf:Trapped "False" .
# document metadata stream (object 41: 472418):
@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
@prefix pdf: &lt;http://ns.adobe.com/pdf/1.3/&gt; .
@prefix pdfx: &lt;http://ns.adobe.com/pdfx/1.3/&gt; .
@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpMM: &lt;http://ns.adobe.com/xap/1.0/mm/&gt; .
&lt;>     pdf:Keywords "PDF/X-1";
pdf:Producer "Acrobat Distiller 4.05 for Macintosh";
pdfx:Apag_PDFX_Checkup "1.3";
pdfx:GTS_PDFXConformance "PDF/X-1a:2001";
pdfx:GTS_PDFXVersion "PDF/X-1:2001";
xmp:CreateDate "2002-03-20T13:56:41Z";
xmp:CreatorTool "Unknown";
xmp:MetadataDate "2004-10-14T12:10:49+10:00";
xmp:ModifyDate "2004-10-14T12:10:49+10:00";
xmpMM:DocumentID "uuid:bd7ae9a1-1110-43c0-8e84-632f2dbb55ab";
dc:creator  [
a rdf:Seq;
rdf:_1 "Scott B. Tully" ];
dc:description  [
a rdf:Alt;
rdf:_1 "A document from our PDF archive. "@x-default ];
dc:format "application/pdf";
dc:title  [
a rdf:Alt;
rdf:_1 "Tully Talk November 2001"@x-default ] .
</pre>

 [1]: /blog/oh-no-not-you-again/
 [2]: https://tools.ietf.org/html/draft-goodwin-iso-urn-02.html
 [3]: http://www.ietf.org/rfc/rfc2397.txt
 [4]: http://www.adobe.com/devnet/pdf/pdf_reference.html