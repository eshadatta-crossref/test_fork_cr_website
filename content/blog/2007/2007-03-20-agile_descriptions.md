---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Meetings
date: 2007-03-20
title: Agile Descriptions
x-version: 0.0.0
---

Apologies to blog yet another of my posts to Nascent, this time on [Agile Descriptions][1] - a talk I gave the week before last before the LC Future of Bibliographic Control WG. (Don’t worry - I shan’t be making it a habit of this.) But certain aspects of the talk (powerpoint is [here][2]) may be interesting to this readership, in particular the slides on microformats and how these are tentatively being deployed on [Nature Network][3], and also a detailed anatomy of [OTMI][4] files.

 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2007/03/agile_descriptions_new.html
 [2]: https://web.archive.org/web/20070630200452/http://nurture.nature.com/tony/ppt/agile-descriptions.ppt
 [3]: https://web.archive.org/web/20070609090207/http://network.nature.com/
 [4]: http://opentextmining.org/