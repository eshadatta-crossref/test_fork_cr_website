---
archives:
- 2007
author: admin
authors:
- admin
categories:
- Programming
date: 2007-02-20
title: Kay Sera Sera
x-version: 0.0.0
---

Not specifically publishing-related, but here is a fun <strike>rant </strike>[interview][1] with [Alan Kay][2] titled _The PC Must Be Revamped—Now._
  
My favorite bit&#8230;
  
&#8220;&#8230;in the last few years I’ve been asking computer scientists and programmers whether they’ve ever typed E-N-G-E-L-B-A-R-T into Google-and none of them have. I don’t think you could find a physicist who has not gone back and tried to find out what Newton actually did. It’s unimaginable. Yet the computing profession acts as if there isn’t anything to learn from the past, so most people haven’t gone back and referenced what Engelbart thought. &#8221;

 [1]: http://www.cioinsight.com/print_article2/0,1217,a=200162,00.asp
 [2]: http://en.wikipedia.org/wiki/Alan_Kay