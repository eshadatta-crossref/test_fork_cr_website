---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Citation
- Data
date: 2007-03-30
title: Citing Data Sets
x-version: 0.0.0
---

This [D-Lib paper][1] by Altman and King looks interesting: _&#8220;A Proposed Standard for the Scholarly Citation of Quantitative Data&#8221;_. (And thanks to [Herbert Van de Sompel][2] for drawing attention to the paper.) Gist of it (Sect. 3) is

> _&#8220;We propose that citations to numerical data include, at a minimum, six required components. The first three components are traditional, directly paralleling print documents. &#8230; Thus, we add three components using modern technology, each of which is designed to persist even when the technology changes: a unique global identifier, a universal numeric fingerprint, and a bridge service. They are also designed to take advantage of the digital form of quantitative data.

> An example of a complete citation, using this minimal version of the proposed standards, is as follows:

> **Micah Altman; Karin MacDonald; Michael P. McDonald, 2005, &#8220;Computer Use in Redistricting&#8221;,

> hdl:1902.1/AMXGCNKCLU UNF:3:J0PkMygLPfIyT1E/8xO/EA==

> `http://id.thedata.org/hdl%3A1902.1%2FAMXGCNKCLU`

> &#8220;_

<!--more-->



So the abbreviated citation (author, date, title, unique ID) is supplemented by a [UNF][3] which fingerprints the data. UNFs would appear to be a sort of super MD5 in providing a signature of the data content independent of the data serialization to a filestore.

> _&#8220;Thus, we add as the fifth component a Universal Numeric Fingerprint or UNF. The UNF is a short, fixed-length string of numbers and characters that summarize all the content in the data set, such that a change in any part of the data would produce a completely different UNF. A UNF works by first translating the data into a canonical form with fixed degrees of numerical precision and then applies a cryptographic hash function to produce the short string. The advantage of canonicalization is that UNFs (but not raw hash functions) are format-independent: they keep the same value even if the data set is moved between software programs, file storage systems, compression schemes, operating systems, or hardware platforms.

> &#8230;

> Finally, since most web browsers do not currently recognize global unique identifiers directly (i.e., without typing them into a web form), we add as the sixth and final component of the citation standard a bridge service, which is designed to make this task easier in the medium term.&#8221;_

Certainly looks promising. I’m not sure if there’s any other contestants in this arena.

 [1]: http://www.dlib.org/dlib/march07/altman/03altman.html
 [2]: http://public.lanl.gov/herbertv/
 [3]: https://web.archive.org/web/20061006030921/http://cran.r-project.org/src/contrib/Descriptions/UNF.html