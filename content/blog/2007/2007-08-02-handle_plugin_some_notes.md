---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Linking
date: 2007-08-02
title: 'Handle Plugin: Some Notes'
x-version: 0.0.0
---

The first thing to note is that this demo (the Acrobat plugin) is an application. And that comes with its own baggage, i.e. this is a Windows only plugin and is targeted at Acrobat Reader 8. On a wider purview the application merely bridges an identifier embedded in the media file and the handle record filed against that identifier and delivers some relevant functionality. The data (or metadata) declared in the PDF and in the associated handle if rich enough and structured openly can also be used by other applications. I think this is a key point worth bearing in mind, that the demo besides showing off new functionalities is also demonstrating how data (or metadata) can be embedded at the respective endpoints (PDF, handle).

Some initial observations follow below.

<!--more-->



**_Install problems_**

As noted in my previous post I had to haul out the old HP laptop and engage in a dialog with our IT folks to get both Acrobat Reader 8 and the plugin installed as I did not have admin privileges on my own machine. Wasn’t pretty but they were kind.

**_Useability_**

I don’t know what’s happening here but from our network it seems as if the first attempts to contact the handle server are timing out and the handle client in the plugin is failing over to an alternate route (HTTP?). So, the plugin doesn’t work as expected since the user has to wait an untenably long time (somewhere between 60s and 90s). Of course, if a certain network access policy is required that would need to be specified and implemented by institutions for their users.

I used both Firefox and Internet Explorer browsers and ran into occasional Acrobat plugin crashes which would lock up the browser. Due to the severe network access problems noted above I wasn’t able to rigorously test this further apart from to note that it was &#8220;buggy&#8221;.

**_Functionality_**

I tested most of the demo cases, but was hampered by the useability restrictions noted above. I didn’t see the &#8220;Related Links&#8221; or get the &#8220;Collections&#8221; to work but did see all the other cases and tried the buttons provided.

One thing of note is that the Crossref metadata record was spoofed and returned from a stored data file rather than an active query to Crossref. A real query would have been been interesting to guage the impact of network latency, although the lookup point is made by hardwiring a response.

**_PDF Metadata_**

OK, so the document DOI is embedded in the PDF both in the document information dictionary and in the (document) metadata stream within an XMP packet. This is great although I do have some specific comments about how the DOI is actually disclosed. See my [Metadata in PDF: 2. Use Cases][1] post for details.

**_Handle Data_**

Handle types are generally a matter for the handle administrators to oversee, although the unregulated use of new types is not going to help foster interoperability between handle applications. In passing I note that the handles used in this demo

<pre>10.5555/pdftest-collection
10.5555/pdftest-collection-item1
10.5555/pdftest-collection-item2
10.5555/pdftest-collection-item3
10.5555/pdftest-crossref
10.5555/pdftest-kernelmetadata
10.5555/pdftest-multires
10.5555/pdftest-rights
10.5555/pdftest-version
</pre>

make use of the following handle types (periods and underscores used as below)

<pre>COLLECTION
COLLECTION_ITEM
HS_ADMIN
HS_MODIFIED
HDL_MD
HDL.RIGHTS
HDL.XREF

</pre>

There is some degree of variability here which presumably will be managed better with a central handle type registry.

**_DOI/Handle_**

And lastly, this demo raises questions again about DOI and handle boundaries. From a handle viewpoint a DOI is nothing more than a branded handle, whereas from a DOI viewpoint a DOI is a specific handle profile with governance and policies, and its own service portfolio. The two terms should not be used interchangeably which I fear is where some of the demo details would lead us. As a very crude analogy (and with apologies to Bob Kahn) but I would see the relationship between DOI and handle as not being dissimilar from that between TCP and IP.

 [1]: /blog/metadata-in-pdf-2.-use-cases