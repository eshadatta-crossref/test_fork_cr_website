---
archives:
- 2007
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Identifiers
date: 2007-12-14
title: Zotero and the IA
x-version: 0.0.0
---

Dan Cohen at Zotero reports ([Zotero and the Internet Archive Join Forces][1]) on a very interesting tie up that will allow researchers using Zotero to deposit content in the Internet Archive and have OCR done on scanned material for free under a two year Mellon grant. Each piece of content will be given a &#8220;permanent URI that includes a time and date stamp in addition to the URL&#8221; ( would Handle or DOI add value here?) and be part of Zotero Commons (things can also be kept private within a group).
  
Zotero Commons is related to but different from Nature Precedings and WebCite in that it’s intended focus is on public domain stuff on researchers hard drives rather than someone else’s material or website that is cited (WebCite) or preprints, datasets, technical reports that are given at least an initial screening (Nature Precedings).

 [1]: http://www.dancohen.org/2007/12/12/zotero-and-the-internet-archive-join-forces