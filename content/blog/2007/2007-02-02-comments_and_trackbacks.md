---
archives:
- 2007
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Blog
date: 2007-02-02
title: comments and trackbacks
x-version: 0.0.0
---

Due to spam the comments and trackbacks were turned off on the blog since last week. Comments can be moderated so they have now been turned back on. Glad to see postings picking up.