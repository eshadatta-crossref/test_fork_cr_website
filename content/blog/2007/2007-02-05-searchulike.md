---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Search
date: 2007-02-05
title: SearchULike
x-version: 0.0.0
---

Nelson Minar has a short [post][1] on Google’s [Search History][2] &#8216;feature’ and how it can be used to enhance your search experience. I guess that should be SearchULike.

 [1]: http://www.somebits.com/weblog/tech/bad/googleSearchHistory.html
 [2]: http://www.google.com/searchhistory