---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Standards
date: 2007-01-29
title: An Open PDF?
x-version: 0.0.0
---

Adobe announces today the following:

_&#8220;SAN JOSE, Calif. — Jan. 29, 2007 — Adobe Systems Incorporated (Nasdaq:ADBE) today announced that it intends to release the full Portable Document Format (PDF) 1.7 specification to AIIM, the Enterprise Content Management Association, for the purpose of publication by the International Organization for Standardization (ISO).&#8221;_

The full press release is [here][1].

(Via [Oleg Tkachenko’s Blog][2].)

 [1]: https://web.archive.org/web/20070202072839/http://www.adobe.com/aboutadobe/pressroom/pressreleases/200701/012907OpenPDFAIIM.html
 [2]: http://www.tkachenko.com/blog/archives/000657.html