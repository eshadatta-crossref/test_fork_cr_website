---
archives:
- 2007
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Interoperability
date: 2007-09-07
title: 'connecting things: bioGUID, iSpiders and DOI'
x-version: 0.0.0
---

David Shorthouse and Rod Page have developed some great tools for linking references by tying together a number of services and using the Crossref OpenURL interface amongst other things. See David’s post - [Gimme That Scientific Paper Part III][1] and Rod’s post on OpenURL and using ParaTools - &#8220;[OpenURL and Spiders][2]&#8220;.
  
Unfortunately our planned changes to the Crossref OpenURL interface (the 100 queries per day limit in particular) caused some concern for David (&#8220;[Crossref Takes a Step Back][3]&#8220;) - but make sure you read the comments to see my response!
  
We decided to drop the 100 per day query limit for the OpenURL interface and there will be no charges for non-commercial use of the interface - <https://apps.crossref.org/requestaccount/>

<!--more-->


  
We want to encourage innovative uses of Crossref services and disseminate DOIs as effectively as possible so we appreciate feedback and encourage the type of development David and Rod are doing. It will be interesting to see if what they are doing has wider applicability. Maybe Crossref could host a webpage to point to tools like this and encourage more development?

 [1]: http://ispiders.blogspot.com/2007/08/gimme-that-scientific-paper-part-iii.html
 [2]: http://iphylo.blogspot.com/2007/05/openurl-and-spiders.html
 [3]: http://ispiders.blogspot.com/2007/09/crossref-takes-step-back.html