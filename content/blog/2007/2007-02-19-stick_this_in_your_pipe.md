---
archives:
- 2007
author: admin
authors:
- admin
categories:
- Programming
date: 2007-02-19
slug: stick-this-in-your-pipe
title: Stick this in your pipe&#8230;
x-version: 0.0.0
---

[Rob Cornelius][1] has a [practical little demo][2] of using Yahoo! pipes against some Ingenta feeds.
  
Like Tony, I keep experiencing speed/stability problems while accessing pipes so I haven’t yet become a [crack-pipes-head][3].

 [1]: http://www2.blogger.com/profile/12568103499947976875
 [2]: http://allmyeye.blogspot.com/index.html
 [3]: http://www.researchbuzz.org/wp/2007/02/12/yahoo-ruins-my-life-with-yahoo-pipes/