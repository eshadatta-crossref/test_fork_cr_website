---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Linking
date: 2007-03-29
title: Markup for DOIs
x-version: 0.0.0
---

Following up on his earlier [post][1] (which was also blogged to CrossTech [here][2]), Leigh Dodds is now [Following up on his earlier [post][1] (which was also blogged to CrossTech [here][2]), Leigh Dodds is now][3] the possibility of using machine-readable auto-discovery type links for DOIs of the form

<tt><br /> <link rel="bookmark" title="DOI" href="http://dx.doi.org/10.1000/1"/><br /> </tt>

These `LINK` tags are placed in the document `HEAD` section and could be used by crawlers and agents to recognize the work represented by the current document. This sounds like a great idea and we’d like to hear feedback on it.

Concurrently at Nature we have also been considering how best to mark up in a machine-readable way DOIs appearing _within_ a document page `BODY`. Current thinking is to do something along the following lines:

<tt><br /> <a href="http://dx.doi.org/10.1038/nprot.2007.43"><br /> <abbr title="Digital Object Identifier">doi</abbr>:<br /> <abbr class="uri" id="doi" title="info:doi/10.1038/nprot.2007.43">10.1038/nprot.2007.43</abbr><br /> </a><br /> </tt>

which allows the DOI to be presented in the preferred Crossref citation format (`doi:10.1038/nprot.2007.43`), to be hyperlinked to the handle proxy server (`<a href="http://dx.doi.org/10.1038/nprot.2007.43">http://dx.doi.org/10.1038/nprot.2007.43</a>`), and to refer to a validly registered URI form for the DOI (`info:doi/10.1038/nprot.2007.43`). Again, we would be real interested to hear any opinions on this proposal for inline DOI markup as well as on Leigh’s proposal for document-level DOI markup.

(Oh, and btw many congrats to Leigh on his recent [promotion][4] to CTO, Ingenta.)

 [1]: http://allmyeye.blogspot.com/2007/03/persistent-linking-web-crawlers-and.html
 [2]: /blog/indexing-urls/
 [3]: http://allmyeye.blogspot.com/2007/03/persistent-links-in-bookmarks.html
 [4]: https://web.archive.org/web/20081014005906/http://eyetoeye.ingenta.com/publisher/issue18/news-dodds.htm