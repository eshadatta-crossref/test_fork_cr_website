---
archives:
- 2007
author: rkidd
authors:
- rkidd
categories:
- Standards
- Web
- InChI
date: 2007-02-01
title: RSC launches semantic enrichment of journal articles
x-version: 0.0.0
---

The RSC has gone live today with the results of [Project Prospect][1], introducing semantic enrichment of journal articles across all our titles. I’m pretty sure we’re the first primary research publisher to do anything of this scope.

We’re identifying chemical compounds and providing synonyms, InChIs (IUPAC’s Chemical Identifier), downloadable CML (Chemical Markup Language), SMILES strings and 2D images for these compounds. In terms of subject area we’re marking up terms from the IUPAC Gold Book, and also Open Biomedical Ontology terms from the Gene, Cell, and Sequence Ontologies. All this stuff is currently available from an enhanced HTML view, with the additional information and links to related articles accessed via highlights in the article and popups.

The mark-up tools have been developed together with UK academics based at the Unilever Centre of Molecular Informatics and the Computing Laboratory at Cambridge University.

At launch we have about 100 articles from our 2007 publications, with the enhanced views currently free-to-air. Feel free to take a look.

 [1]: https://web.archive.org/web/20070812060042/http://www.rsc.org/Publishing/Journals/ProjectProspect/index.asp