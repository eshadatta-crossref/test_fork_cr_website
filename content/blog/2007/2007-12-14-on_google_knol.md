---
archives:
- 2007
author: admin
authors:
- admin
categories:
- ORCID
- Publishing
- Search
date: 2007-12-14
title: On Google Knol
x-version: 0.0.0
---

The recently discussed (announced?) [Google Knol][1] project could make Google Scholar look like a tiny blip in the the scholarly publishing landscape.

I love the comment an authority:

&#8220;Books have authors’ names right on the cover, news articles have bylines, scientific articles always have authors &#8212; but somehow the web evolved without a strong standard to keep authors names highlighted. We believe that knowing who wrote what will significantly help users make better use of web content.&#8221;

And so I suppose this means they are assigning author identifiers&#8230;.

 [1]: https://web.archive.org/web/20210926222403/https://googleblog.blogspot.com/2007/12/encouraging-people-to-contribute.html