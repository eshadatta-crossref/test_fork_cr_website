---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Publishing
date: 2007-03-23
slug: welcome-to-otmi-discuss
title: Welcome to &#8220;Otmi-discuss&#8221;
x-version: 0.0.0
---

Just a quick note to mention that we’ve now set up a new mailing list [otmi-discuss@crossref.org][1] for public discussion of [OTMI][2] - the Open Text Mining Interface proposed by Nature. See the list information page [here][1] for details on subscribing to the list and to access the mail archives.

And many thanks to the Crossref folks for hosting this for us!

 [1]: https://web.archive.org/web/20070503161800/http://mailserv.crossref.org:8800/mailman/listinfo/otmi-discuss
 [2]: http:/opentextmining.org/