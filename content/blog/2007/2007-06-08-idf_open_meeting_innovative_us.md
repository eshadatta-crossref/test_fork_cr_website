---
archives:
- 2007
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Conference
date: 2007-06-08
title: 'IDF Open Meeting: Innovative uses of the DOI system'
x-version: 0.0.0
---

Please see the details of the IDF Annual Meeting and a related Handle System Workshop in Washington, DC on June 21 which may be of interest - [http://www.crossref.org/crweblog/2007/06/international\_doi\_foundation_a.html][1]

 [1]: http://www.crossref.org/crweblog/2007/06/international_doi_foundation_a.html