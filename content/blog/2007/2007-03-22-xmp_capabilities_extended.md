---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Metadata
date: 2007-03-22
title: XMP Capabilities Extended
x-version: 0.0.0
---

This [post][1] on Adobe’s Creative Solutions PR blog may be worth a gander:

> _&#8220;This new update, the Adobe XMP 4.1, provides new libraries for developers to read, write and update XMP in popular image, document and video file formats including: JPEG, PSD, TIFF, AVI, WAV, MPEG, MP3, MOV, INDD, PS, EPS and PNG. In addition, the rewritten XMP 4.1 libraries have been optimized into two major components, the XMP Core and the XMP Files.

> The XMP Core enables the parsing, manipulating and serializing of XMP data, and the XMP Files enables the reading, rewriting, and injecting serialized XMP into the multiple file formats. The XMP Files can be thought of as a &#8220;file I/O&#8221; component for reading and writing the metadata that is manipulated by the XMP Core component.

> Supported development environments for Adobe’s XMP 4.1 are: XCode 2.3 for Macintosh universal binaries, Visual Studio 2005 (VC8) for Windows, and Eclipse 3.x on any available platform. The XMP Core is available as C++ and Java sources with project files for the Macintosh, Windows and Linux platform. A Java version of XMP Files is under consideration for a future update.&#8221;_

And now I just read that last sentence again: **_&#8220;A Java version of XMP Files is under consideration for a future update.&#8221;_** So, how hard do they really want to make uptake of XMP be? Am surprised they’re even still considering offering full Java support, and not offering also anything in the way of support for glue languages such as Perl, Python, or Ruby.

Which leads to the question: Is anybody here using XMP and had any success to relate or lessons for the rest of us?

 [1]: https://web.archive.org/web/20070908031425/http://blogs.adobe.com/creativesolutionspr/2007/03/adobe_extends_xmp_capabilities.html