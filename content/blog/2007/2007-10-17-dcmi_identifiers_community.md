---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Identifiers
date: 2007-10-17
title: DCMI Identifiers Community
x-version: 0.0.0
---

Another DCMI invitation. And a list. Lovely.
  
See [this message][1] (copied below) from Douglas Campbell, National Library of New Zealand, to the [dc-general][2] mailing list.
  
(Continues)

<!--more-->

> _&#8220;Hi all,
  
> I would like to alert members of this list to the new DCMI Identifiers Community established at the recent Dublin Core Metadata Initiative (DCMI) Advisory Board meeting in Singapore. It is moderated by Douglas Campbell (National Library of New Zealand).
  
> The community is a forum for individuals and organisations with an interest in the design and use of identifiers in metadata. It also serves as a liaison channel for those involved in identifier efforts in other domains.
  
> There was a lot of interest in identifiers at the recent DCMI conference. Identifiers are fundamental to the Web and for managing digital content, but most of us don’t know where to begin in designing and assigning them. The level of confusion can be seen in the number of meetings and workshops held just about identifiers. DCMI is in a unique position to bring together the thinking (and doing) around identifiers from multiple domains.
  
> I would like to encourage you to share your identifier efforts and thinking amongst the DCMI community on our Identifiers wiki at:
  
> <http://dublincore.org/identifierswiki>
  
> You can join the community by signing up to our JISCMAIL list, linked from our community homepage at:
  
> <http://www.dublincore.org/groups/identifiers/>
  
> or by going direct to jiscmail:
  
> <http://www.jiscmail.ac.uk/cgi-bin/webadmin?SUBED1=dc-identifiers&#038;A=1>
  
> Thanx,
  
> Douglas&#8221;_

 [1]: http://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind0710&#038;L=dc-general&#038;T=0&#038;F=&#038;S=&#038;P=1223
 [2]: http://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=dc-general