---
archives:
- 2007
author: admin
authors:
- admin
categories:
- Interoperability
- Standards
date: 2007-02-08
title: Microsoft to Support OpenID
x-version: 0.0.0
---

Kim Cameron, Microsoft’s Identity Czar and member of the [Identity Gang][1], [comments on][2] Microsoft’s announcement that they will support [OpenID][3]. Another sign that [federated identity][4] schemes are gaining traction and OpenID is likely to emerge as a standard the publishers are going to want to grapple with soon.

This follows Doc Searl’s comments on the notion of &#8220;[Creator Relationship Management][5]&#8221; where he speculates that the techniques being used in federated identity schemes and the Creative Commons can be combined to create a new &#8220;silo-free&#8221; value chain amongst creators, producers and distributors.

 [1]: https://web.archive.org/web/20070826193937/http://www.identitygang.org/
 [2]: http://www.identityblog.com/?p=668
 [3]: http://openid.net/
 [4]: http://en.wikipedia.org/wiki/Federated_identity
 [5]: http://www.linuxjournal.com/node/1000180