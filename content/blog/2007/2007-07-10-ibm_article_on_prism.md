---
archives:
- 2007
author: thammond
authors:
- thammond
categories:
- Metadata
date: 2007-07-10
title: IBM Article on PRISM
x-version: 0.0.0
---

Nice entry article on PRISM [here][1] by Uche Ogbuji, Fourthought Inc. on IBM’s DeveloperWorks.

 [1]: http://www.ibm.com/developerworks/xml/library/x-think13.html