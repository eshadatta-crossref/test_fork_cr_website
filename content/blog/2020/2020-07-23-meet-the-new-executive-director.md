---
Archives:
- 2020
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Member Briefing
- Board
- Strategy
date: 2020-07-23
draft: false
title: Meet the new Crossref Executive Director
x-version: 0.0.0
---

It’s me! Back in January I wrote, [The one constant in Crossref’s 20 years has been change](/blog/crossref-is-20). This continues to be true, and the latest change is that I’m happy to say that I will be staying on as Executive Director of Crossref. At the recent Crossref board meeting, I rescinded my resignation and the board happily accepted this.  
<!--more-->
What happened? Well, a lot has changed since [I announced that I was leaving](/blog/leaving-crossref) back in February. The pandemic has upended “business as usual” and everyone is rethinking pretty much everything. It’s clear that as a result of the crisis, there will be greater economic pressure on our community. These are difficult times and they are going to continue for the foreseeable future.

The people at Crossref are amazing and I’ve been impressed and inspired by everyone’s resilience and creativity in responding to these unusual challenges. Crossref has a very special organizational culture and I want to remain a part of it and continue to develop it.

I’ve also been inspired by the board. In particular, at its July meeting they passed a progressive motion based on a proposal from the leadership team:

> RESOLVED: Crossref should proactively lead an effort to explore, with other infrastructure organizations and initiatives, how we can improve the scholarly research ecosystem. Crossref is committed to the collaborative development of open scholarly infrastructure for the benefit of our members and the wider research community.

This is the result of a process that started back in 2019. In the [A turning point is a time for reflection](/blog/a-turning-point-is-a-time-for-reflection) blog post, we took a step back as we approached Crossref’s 20th anniversary. We conducted research into [the perceived value of Crossref](http://bit.ly/crvalue), reflected on what we had achieved, and what the future holds for Crossref. At our [annual meeting, "the strategy one"](/crossref-live-annual) and in our [annual report fact file](https://doi.org/10.13003/y8ygwm5), we reminded people of the organization’s original founding purpose:   

> To promote the development and cooperative use of new and innovative technologies to speed and facilitate scientific and other scholarly research.

Following on from 2019, as the pandemic hit, we held virtual strategic sessions with the board in March, May and June. These culminated in the motion above, which allows Crossref to fully embrace this simple, but ambitious, vision. This was a game changer for me, and I realized there was nothing else I wanted to do or that better suited my skills and experience than to continue to lead Crossref and work with the community through the next phase of transformation.

This is not the time for “business as usual”. We live in an interconnected, interdependent world and open infrastructure organizations have to collaborate more deeply and look at doing things differently in order to improve the scholarly research ecosystem. So - more to come!