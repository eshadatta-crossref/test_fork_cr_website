---
archives:
- 2020
author: Vanessa Fairhurst
authors:
- Vanessa Fairhurst
categories:
- Outreach
- Community
- Crossref LIVE
- Education
- Ambassadors
date: 2020-06-29
draft: false
title: Community Outreach in 2020
x-version: 0.0.0
---

2020 hasn’t been quite what any of us had imagined. The pandemic has meant big adjustments in terms of working; challenges for parents balancing childcare and professional lives; anxieties and tensions we never had before; the strain of potentially being away from co-workers, friends, and family for a prolonged period of time. Many have suffered job losses and around the world, many have sadly lost their lives to the virus.
<!--more-->
I’ve been very fortunate that my family and friends remain in good health and very grateful to work for a supportive and caring organization such as Crossref. I don’t usually work from home every day, so adjusting to the ‘new normal’ these last few months has been difficult at times. I certainly miss seeing my colleagues in the Oxford office day-to-day, and now have a new appreciation for the challenges our remote working members of staff face, particularly when it comes to feeling quite isolated at times. I’ve also learnt about the importance of good communication and building in greater flexibility to projects, especially when you are not able to see people face-to-face.

My role as Outreach Manager is all about people; it often involves organising and attending industry events as well as running our own educational days, which we call our [Crossref LIVE events](/events/). The global health crisis brought the majority of international travel to an abrupt halt, something the environment may thank us for, but that also requires a dramatic reimagining of how we can effectively and empathetically engage with our members and the wider community.

As our planned in-person events have been postponed, for now, we converted our LIVE events into an online format, which we have so far run in [Arabic](https://youtu.be/McDIrEpWph4), [Spanish](https://community.crossref.org/t/crossref-virtual-live-spanish/1324?u=vanessa), [Korean](https://community.crossref.org/t/crossref-live-korea/1351/2?u=vanessa) and [Brazilian Portuguese](https://community.crossref.org/t/crossref-virtual-live-brazil/1323/2?u=vanessa) with help from [our Ambassadors](/community/our-ambassadors/) and technical support team. We have had better attendance and engagement than we ever dreamed, with lots of thoughtful questions and positive feedback. While an online format has its limitations it also brings new opportunities, particularly by enabling us to reach many members who would not be able to attend a physical event. We have more in the works for the rest of the year, so keep a lookout on our [webinar](https://www.crossref.org/webinars/) and [events](https://www.crossref.org/events/) pages.

We have all had to adapt to new ways of living and working this year, but vital research continues to be done and new content continues to be published. We embrace new ways of engaging with our international membership so we can continue to support them in their roles and in working with our systems, despite the uncertain circumstances we find ourselves in.

### Lessons learned:

- Online events need to be much shorter than physical ones. Zoom fatigue is real, no one can stay focused for long periods of time at the screen.

- Flexibility is key, running events in multiple languages and time-zones make them more accessible for a geographically diverse audience, but also ensuring recordings and other materials are readily available means people can engage with the content in their own time. And they do. Our Spanish LIVE on May 19 saw 335 people attend, and a further 304 (so far) watch [the recording](https://youtu.be/kQNwWzcWeH8) in their own time.

- Don’t forget to build in time for breaks.

- Although it’s impossible to replicate the natural human interaction that occurs at a physical event, an online format can still bring hearts as well as minds together. Break-out rooms, polls, and clever use of chat functionality all help to build engagement and turn a passive audience into active participants.

- People love an online quiz.

- Partner with others –– an interesting guest speaker can bring a whole new dynamic to your planned content.

- Take the opportunity to be a little more experimental. We can’t do business as usual right now, so embrace new ideas and see what works!


Hoping you all stay safe and healthy, and that we can meet again in person in 2021.