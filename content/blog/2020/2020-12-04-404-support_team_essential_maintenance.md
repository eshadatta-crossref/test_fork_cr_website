---
Archives:
- 2020
author: Amanda Bartell
authors:
- Amanda Bartell
categories:
- Community
- Member Briefing
- Staff
- Support
date: 2020-12-04
draft: false
title: '404: Support team down for essential maintenance'
x-version: 0.0.0
---

2020 has been a very challenging year, and we can all agree that everyone needs a break. Crossref will be providing very limited technical and membership support from 21st December to 3rd January to allow our staff to rest and recharge. We’ll be back on January 4th raring to answer your questions. Amanda explains more about why we made this decision.

<!--more-->

As we all know, 2020 has been an unprecedented year, with the COVID-19 pandemic affecting lives across the globe.

It’s been amazing to watch our members pivot their working practices and continue to publish content and register it with Crossref to keep the wheels of research and scholarly communications moving.

Since January, we’ve seen 9,079,082 items registered with Crossref, up 13% on 2019. 2628 new members have also joined during that time and we now have almost 13.5k members from 139 countries. We’ve seen over 337 million requests to our REST API on average per month in 2020, a 9% increase over 2019 (and over 600 million total metadata queries on average per month across all our APIs and services).

Of course, all this activity brings an increasing number of requests for help and support. Since the start of 2020, we have answered almost 24,000 support tickets from the community. Sometimes these just need a quick answer or a link to our documentation. Sometimes it's a straightforward new member application or a routine query. But sometimes a prospective member needs a lots of advice, sometimes a long-standing member or user needs in-depth investigations and consultancy. Sometimes the request highlights a problem in one of our systems that needs input from our product and development colleagues. But either way, it’s keeping our small team of five full-time employees very busy.

Vanessa [wrote](/blog/community-outreach-in-2020/) earlier in the year about how our Community Outreach team has changed its working practices this year. As Head of Member Experience I’ve been incredibly impressed by the way our membership, support and billing staff have done the same - remaining really focused on the needs of the Crossref community while (at the same time) balancing this with the demands of working from home, childcare, home-schooling, and supporting those affected by the pandemic in their own community. Isaac’s thoughtful [post on our forum](https://community.crossref.org/t/my-first-week-working-from-home-during-the-coronavirus-covid-19-pandemic/1236) about his first week working at home because of the pandemic really highlighted some of these challenges.

We take work/life balance seriously at Crossref. We want to make sure that we’re are able to continue to help the Crossref community effectively in 2021, but are also able to continue to look after ourselves, our families, and our own communities in this difficult time. We all hope that 2021 will be a very different year, but there’s still likely to be disruption ahead for all of us, and one thing is sure: there will continue to be plenty more requests coming in for our small team to stay on top of in the meantime.

With this in mind, we want to make sure that our support staff are able to properly rest and recharge during what is a holiday period for many of us coming up. We’ll be operating with just one person each on the technical support and membership support side between 23rd December and 3rd January. This means that while we’ll be able to answer urgent queries, **non-urgent questions will be left unanswered until 4th January. And we’ll not take on any new members between 21st December and 3rd January too.**

We know many of you will be continuing to work during this period. If you have a non-urgent question, do take a look at our [support documentation](/education/) in the meantime, or see if other members (or our amazing Ambassadors) are able to [help on our forum](https://community.crossref.org/). If you can’t find what you’re looking for and it's urgent, we hope that the limited staff who are on call will still be able to help you out.

Colleagues in the US have recently celebrated their Thanksgiving, and I remain enormously thankful for our team here at Crossref, and for you all in the scholarly community for your enthusiasm for working together collectively to help the world find, cite, link, assess, and reuse scholarly content. We all really appreciate your patience while we reset ready for 2021. Happy Holidays!