---
archives:
- 2020
author: Lucy Ofiesh
authors:
- Lucy Ofiesh
categories:
- Board
- Member Briefing
- Governance
- Elections
- Crossref Live
- Annual Meeting
date: 2020-09-28
draft: false
title: 2020 Board Election
x-version: 0.0.0
---

This year, Crossref’s Nominating Committee assumed the task of developing a slate of candidates to fill six open board seats. We are grateful that in the midst of a challenging year, we received over 70 expressions of interest from all around the world, a 40% increase from last year’s response. It was an extraordinary pool of applicants and a testament to the strength of our membership community.

There are six seats open for election (two large, four small), and the  [Nominating Committee](/committees/nominating/) is pleased to present the following slate.  

## The 2020 slate

Candidate organizations, in alphabetical order, for the Small category (four seats available):

* **Beilstein-Institut**, Wendy Patterson
* **Korean Council of Science Editors**, Kihong Kim
* **OpenEdition**,  Marin Dacos
* **Scientific Electronic Library Online (SciELO)**, Abel Packer,
* **The University of Hong Kong**, Jesse Xiao

Candidate organizations, in alphabetical order, for the Large category (two seats available):

* **AIP Publishing**, Jason Wilde,
* **Oxford University Press**, James Phillpotts,
* **Taylor & Francis**, Liz Allen

{{% divwrap blue-highlight %}}

### [Here are the candidates' organizational and personal statements](/board-and-governance/elections/2020-slate/)

{{% /divwrap %}}

## You can be part of this important process, by voting in the election

If your organization is a voting member in good standing of Crossref as of September 14, 2020, you are eligible to vote when voting opens on September 30, 2020.

## How can you vote?

On September 30, 2020, your organization's designated voting contact will receive an email with the Formal Notice of Meeting and Proxy Form with concise instructions on how to vote.  You will also receive a user name and password with a link to our voting platform.

The election results will be announced at LIVE20 **virtual** meeting on November 10, 2020.