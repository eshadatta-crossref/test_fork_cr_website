---
archives:
- 2020
author: Paul Davis
authors:
- Paul Davis
categories:
- Content Registration
- Identifiers
- Metadata
date: 2020-04-27
draft: false
title: Memoirs of a DOI detective...it’s error-mentary dear members
x-version: 0.0.0
---

Hello, I’m Paul Davis and I’ve been part of the Crossref support team since May 2017. In that time I’ve become more adept as a DOI detective, helping our members work out *whodunnit* when it comes to submission errors.

If you have ever received one of our error messages after you have submitted metadata to us, you may know that some are helpful and others are, well, difficult to decode. I'm here to help you to become your own DOI detective.

<!--more-->

### Motive: ridding the world of bad metadata  
When depositing xml files to us, there can be a plethora of error messages returned to you in the submission logs. Wait, what are submission logs? If that is the first thing that came to mind, then you’re in the right place; do keep reading.
### Means: XML deposits  
After each content registration or update is received into our deposit admin system, it is initially placed in the submission queue and later, once its time comes, is processed. Whether that deposit comes from the [web deposit form](https://www.crossref.org/webDeposit/), [Metadata Manager](https://www.crossref.org/metadatamanager/), or a good old fashioned [XML deposit](/documentation/content-registration/metadata-deposit-schema/), a submission log is created in our system. This log contains important information about the deposit and its success or failures.

I will go through how you will find and receive this log later on.
At the bottom of the submission log you will see a status message that looks like this:
~~~
  <batch_data>
      <record_count>***</record_count>
      <success_count>***</success_count>
      <warning_count>***</warning_count>
      <failure_count>***</failure_count>
  </batch_data>
~~~
To some, this might look a bit like a crime scene. If the status report displays the same number in the  ``<record_count>`` and the ``<success_count>``, then no crime (against deposits) has been committed. Everything you have tried to register or update has been successful and we are all free as DOI detectives to knock off early.

At some point you will probably come across an error or failure in the submission logs, where the failure count is 1.
~~~
   <batch_data>
      <record_count>1</record_count>
      <success_count>0</success_count>
      <warning_count>0</warning_count>
      <failure_count>1</failure_count>
   </batch_data>
~~~
For the purposes of this blog, this type of message means a *“crime”* has been committed. The worst kind of crime - a metadata crime. In the real world, outside of this blog, it just means that your deposit has failed and you need to take some action to fix it. You will also receive accompanying error messages (an evidence log) with details about what went wrong with your submission. We’ll deliver these submission details to you as well in the following ways:

* For those submitting via the web deposit form, to the email address used to register your submission
* On screen and within the admin tool using the submission ID for those submitting via Metadata Manager
* For those submitting XML, to the email included in the ``<email_address>`` element of your deposit XML

* You can also find the submission log in the admin system at any point

More information on [viewing past deposits](/education/content-registration/verify-your-registration/submission-queue-and-log/#00143) in the admin system can be found on our support site.

### The usual suspects
Those serial offenders, when it comes to failed deposits, are:
#### Timestamps
* **Misdemeanor** - Every deposit has a `<timestamp>` value, and that value needs to be incremented each time the DOI is updated. This is done automatically for you in Metadata Manager, the Web Deposit Form and the OJS plugin. But if you’re updating an existing DOI by sending us the whole XML file again, you need to make sure that you update the timestamp as well as the field you’re trying to update.
~~~
Error: <msg>Record not processed because submitted version: 201907242206 is less or equal to previously submitted version 201907242206</msg>
~~~

* **Rehabilitation** - simply resubmit your XML file, but make sure that you increment the timestamp value to be larger than the current timestamp value.

#### Titles
* **Misdemeanor** - These need to match exactly between what we have on the system against the ISSN/ISBN and what is in the deposit file.
~~~
Error: <msg>Deposit contains title error: the deposited publication title is different than the already assigned title</msg>
~~~
or
~~~
Error: <msg>ISSN "123454678" has already been assigned, issn (123454678) is assigned to another title (Journal of Metadata)</msg>
~~~
* **Rehabilitation** - you can check the title we have on the system against the ISSN/ISBN on the [title list](https://www.crossref.org/titleList/) and make the necessary changes, or contact [support](mailto:support@crossref.org) for us to check the title in our system and make changes to match the title in the deposit to the one in the system, if known.

#### Title level DOIs
* **Misdemeanor** - These also need to match up exactly in both system and deposit
~~~
Error: <msg>Deposit contains title error: The journal has a different DOI assigned; If you want to change the journal's DOI please contact Crossref support: title=Journal of Metadata; current-doi=10.14393/JoM; deposited-doi=10.14393/JoM.1.1</msg>
~~~

* **Rehabilitation** - contact us to change the journal level DOI in the system or change the DOI in the deposit yourself to match the one already registered for the title.

#### Errors in the xml
* **Misdemeanor** - Poor formatting, self closing tags, invalid values.
~~~
Error: <msg>Deposited XML is not well-formed or does not validate: Error on line 538</msg>
~~~
* **Rehabilitation** - update the xml file that was deposited as it was not well formed against our schema or as an xml file in general. Check you have saved the file correctly (as an .xml file), edited it in an xml editor and not a word processor and if that fails, then contact [support](mailto:support@crossref.org) and we will try to assist. We also have a collection of [new xml examples](https://gitlab.com/crossref/schema/-/tree/master/examples) you may use as a template.

### Forensics
There are a few tools we offer to help with the deciphering of the error messages –– we think of these as our magnifying glass(es).

* The **[Title list](https://www.crossref.org/titleList/)**:  A list of all of the titles in our database, you can check against the ISSN/ISBN to see what the title on our system is and whether it matches the title you have in your deposit.

* The **[Depositor Report](https://www.crossref.org/06members/51depositor.html)**:  Shows all journals, books, and conference proceedings against each member. The report includes all DOIs for each journal, book, conference; the most recently used timestamps; and citation counts for each DOI.

* The **[Reports tab](https://doi.crossref.org/servlet/reports)** in the admin system:  You can find out the history behind a DOI by searching against this in the admin console.

* Our **[common error messages](/education/content-registration/verify-your-registration/troubleshooting-submissions/#00152)** are documented within our support documentation. You can always find out more about most of the error messages are system displays at the link above.

* You can find the current **[xml metadata against a DOI](http://doi.crossref.org/search/doi?pid=support@crossref.org&format=unixsd&doi=10.5555%2F12345678)** by adding the DOI to the end of this link http://doi.crossref.org/search/doi?pid=support@crossref.org&format=unixsd&doi=
(you might need an xml viewer browser extension to view the xml in a more readable format).

### Calling for backup

We’ll also soon be adding more leads to our submission logs and error messages for the best of our detectives. These improvements will point our DOI detectives to better documentation about interpreting error messages and taking the appropriate action to resolve those errors.  

But there are a lot more error messages out there. If you have trouble deciphering any error message you encounter, then please do send the case number (submission ID) over to CSI (Crossref Support Investigations) at [support@crossref.org](mailto:support@crossref.org).  

You can also find lots of great information in the pages of our new [documentation](/education/).