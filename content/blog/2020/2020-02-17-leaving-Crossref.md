---
archives:
- 2020
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Members
- Member Briefing
date: 2020-02-14
draft: false
title: Leaving Crossref
x-version: 0.0.0
---

## Where does the time go...

In my [blog post on January 14th](/blog/crossref-is-20/) about Crossref’s 20th anniversary I said, “The one constant in Crossref’s 20 years has been change”. It’s true that there has been constant change, but there has been another constant at Crossref –– me (and DOIs, to be fair). I started as Crossref’s first employee and Executive Director on February 1st, 2000, so I just marked my [20th anniversary with the organization](/news/2000-02-02-journal-reference-linking-service-names-executive-director-board-of-directors-new-members-and-a-go-live-timetable/).  

<!--more-->

This milestone prompted me to reflect on where I am and where I’m heading. After 20 years leading the organization, I’ve decided to leave Crossref. It’s time for a new challenge.  I’m still very committed to the mission and very proud of my time at Crossref, the culture we’ve created and what the organization has achieved. It’s been an honor serving as Executive Director and a pleasure working with so many great people over the years. And to be clear –– I’m not ill, being pushed or having a midlife crisis (yet).

It’s a difficult and emotional decision but I think the transition can be positive for me, the staff, the board, and the organization. I’ll be working with the Crossref board, Chair, Treasurer and staff on the transition –– the plan is for me to be around through September or October to enable the recruitment and handover to a new Executive Director. There will be more information about the transition and recruitment process after the Crossref board meeting March 11-12 in London.

Crossref has a bright future and many opportunities to do new things. Crossref provides essential, open scholarly infrastructure and services that benefit its members and the wider scholarly research ecosystem ––  and we’ve got a lot of interesting things in development and ambitious plans. To anyone who might be interested in being Crossref’s next Executive Director, I can honestly say it is fantastic, challenging, fun, and very fulfilling –– that’s why I’ve done it for 20 years.

What’s next for me? I don’t know but it’s something I’ll be thinking about over the coming months. I do know that working for a mission driven organization and staying involved with scholarly communications and research –– a fascinating and worthy field ––  will be top of my list.

Anyway - it’s back to work and full steam ahead for Crossref!