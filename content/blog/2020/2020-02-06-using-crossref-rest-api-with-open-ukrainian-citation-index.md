---
archives:
- 2020
author: Rachael Lammey
authors:
- Rachael Lammey
categories:
- Metadata
- Citation Data
- APIs
- API Case Study
date: 2020-02-05
draft: false
title: Using the Crossref REST API (with Open Ukrainian Citation Index)
x-version: 0.0.0
---

Over the past few years, I've been really interested in seeing the breadth of uses that the research community is finding for the Crossref REST API. When we ran Crossref LIVE Kyiv in March 2019, Serhii Nazarovets joined us to present his plans for the Open Ukrainian Citation Index, an initiative he explains below.

But first an introduction to Serhii and his colleague Tetiana Borysova.  

Serhii Nazarovets is a Deputy Director for Research at the State Scientific and Technical Library of Ukraine. Serhii has a Ph.D. in Social Communication Science. His research interests lie in the area of scientometrics and library science. Serhii is the Associate Editor for DOAJ ([www.doaj.org](http://www.doaj.org/)) and the Regional Editor for E-LIS (Eprints in Library and Information Science). Serhii has worked in different scientific libraries of Ukraine for more than 10 years. Tetiana Borysova is a Senior Researcher at the State Scientific and Technical Library of Ukraine. Her research interests are focused on topics such as research data management, journal management and scientometrics.

## Introducing OUCI

OUCI ([Open Ukrainian Citation Index](http://ouci.dntb.gov.ua/en/)) is a new search engine and a citation database based on publication metadata from Crossref members.

OUCI is intended to simplify the search of scientific publications, to attract the editors' attention to the problem of completeness and quality of the metadata of Ukrainian scholarly publications, and will allow bibliometricians to freely study the relations between authors and documents from various disciplines, in particular in the field of social sciences and humanities. OUCI is open for every user in the world without any restrictions.

OUCI launched in November 2019. The project is being implemented by the [State Scientific and Technical Library of Ukraine](https://dntb.gov.ua/en/science) with the support of the Ministry of Education and Science of Ukraine.

In Ukraine, we do not have a national citation database, and this significantly impedes the search and analysis of information about Ukrainian publications. According to preliminary estimates, more than 3,000 titles of scientific journals are currently published in Ukraine. At the same time, only around 100 Ukrainian journal titles are indexed in authoritative citation databases, such as Scopus and Web of Science Core Collection. Thus, researchers and managers lack this citation data to understand the impact of Ukrainian journals and their demand in the scientific communication system. Our approach is that OUCI database contains metadata from all publishers that use the Crossref's [Cited-by](/services/cited-by/) service and who support the [Initiative for Open Citations](https://i4oc.org/) by making the reference metadata they publish with Crossref openly available.

## How is Crossref metadata used in OUCI?

A publication can only be indexed in OUCI if there is a DOI. At first glance, the idea of creating an index of national publications based on this condition may seem too optimistic. However, in January 2018, a new requirement was adopted by the [List of scientific publications of Ukraine](https://zakon.rada.gov.ua/laws/main/z0148-18) (a list of Ukrainian journals recognized by experts as qualitative for publishing their research results for a scientific degree), which listed a DOI as one of the requirements for inclusion. After that, the number of publishers who received the DOI prefix from Crossref has tripled, to 352 in November 2019.

Another important feature of OUCI is that publishers have to use Crossref's [Cited-by](/services/cited-by/) service and support the [Initiative for Open Citations.](https://i4oc.org/) We are working to build a new fair infrastructure where everyone who is interested in the dissemination of scientific knowledge can present their publications to the community, develop expert judgment skills and access citations to explore the links between documents. The philosophy of the index is to use only open resources to fill it.

In addition to standard filters from Crossref metadata (such as publisher, publication, type, year), OUCI offers to refine search results by:

-   indexation in Web of Science and/or Scopus,
-   journal category (A or B according to the List of scientific publications of Ukraine),
-   the field of knowledge and scientific specialties (according to the Ukrainian legislation) and other aspects important to Ukrainian users characteristics.   

{{< figure src="/images/blog/2020/ouci_blog_filters.png" caption="Figure 1: OUCI search and filter options" width="75%" >}}  

Beyond the ability to search articles, OUCI displays profiles for Ukrainian journals (the titles of these journals will include hyperlinks in the search results). Administrators can manage them, add and edit information about their journals: web-site, aims and scope, scientific fields of the journal according to the Ukrainian classification. Also, you can see some quantitative characteristics of journals: number of publications, number of citations, h-index, i10-index etc.  

{{< figure src="/images/blog/2020/ouci_blog_profiles.png" caption="Figure 2: Display of journal information in [OUCI](http://ouci.dntb.gov.ua/en/editions/xmnGEm0L/)" width="75%" >}}

In addition, we have implemented an analytics module. Using the data about the number of articles and citations from Crossref, it allows users to analyze Ukrainian journals by field.  

{{< figure src="/images/blog/2020/ouci_blog_analysis.png" caption="Figure 3: Publication and citation information" width="75%" >}}  

## What are the future plans for OUCI?

In the near future, we plan to add:

-   the ability to export search results for further analysis;
-   integration with [Unpaywall](https://unpaywall.org/);
-   alternative metrics from [Crossref Event Data](/services/event-data/terms/).  

In the ideal future for our index, every Ukrainian article will be registered with Crossref and have open references. We plan to promote the importance of reach and quality metadata in Crossref among Ukrainian publishers. We also encourage all publishers to support the [Initiative for Open Citations](https://i4oc.org/).

## What else would OUCI like to see in Crossref metadata?

One of the main problems we encountered when creating OUCI was the metadata about the authors. Very few publications contain data about the author's ORCID iD. Focusing publishers on the need to transmit full metadata to Crossref, as well as monitoring their quality is a must for the resources like this. Also we look forward to the growing usage of ROR ([Research Organization Registry](https://ror.org/)) - identifiers for research organizations, similar to the way that ORCID offers identifiers for researchers. We believe that the ROR will help to obtain reliable data for analyzing the scientific activity of Ukrainian institutions.

Another issue we've identified in some Ukrainian journals that some of the small publishers that register content via Crossref Sponsors did not take care getting their own prefix, so it can be difficult to see their publications - this is something that showing the metadata via an index can help them see and therefore fix.

## Questions?

We've had lots of questions about OUCI in the run up to the launch and now that it's live. Here is a selection of our FAQs, [all available on our website](http://ouci.dntb.gov.ua/en/about/faq/). You can also [get in touch](mailto:nazarovets@gntb.gov.ua) directly if you have another question we haven't answered yet.