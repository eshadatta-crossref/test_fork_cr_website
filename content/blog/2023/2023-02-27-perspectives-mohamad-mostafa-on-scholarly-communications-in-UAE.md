---
aliases: /blog/perspectives-mohamad-mustafa-on-scholarly-communications-in-uae
archives:
- 2023
author: Mohamad Mostafa
authors:
- Mohamad Mostafa
- Rosa Clark
- Kora Korzec
categories:
- Community
- Perspectives
date: 2023-02-27
draft: false
title: 'Perspectives: Mohamad Mostafa on scholarly communications in UAE'
x-version: 0.0.0
---

{{% imagewrap left %}} <img src="/images/blog/2022/perspectives.png" alt="sound bar logo" width="150px" class="img-responsive" /> {{% /imagewrap %}}  

Our Perspectives blog series highlights different members of our diverse, global community at Crossref. We learn more about their lives and how they came to know and work with us, and we hear insights about the scholarly research landscape in their country, the challenges they face, and their plans for the future.  
<div style="text-align: right"> 
تسلط سلسلة مدونة توقعات - وجهات نظر الخاصة بنا الضوء على أعضاء مختلفين من مجتمعنا العالمي المتنوع في كروس رف .نتعلم المزيد عن حياتهم وكيف تعرفوا وعملوا معنا، ونسمع رؤى حول مشهد البحث العلمي في بلدهم، والتحديات التي يواجهونها، وخططهم للمستقبل.

</div> <br>

<!--more-->

As we continue with our Perspectives blog series, today, we meet Mohamad Mostafa, Crossref Ambassador in the UAE and Production Manager at Knowledge E. Mohamad is passionate about helping improve the discoverability of research through rich metadata. We invite you to read and listen to what Mohamad has to say!
<div style="text-align: right"> 
بينما نواصل سلسلة مدونة توقعات - وجهات نظر الخاصة بنا، نلتقي اليوم مع محمد مصطفى، سفير كروس رف في الإمارات العربية المتحدة ومدير الإنتاج في نوليدج اي . محمد متحمس للمساعدة في تحسين إمكانية اكتشاف البحث من خلال البيانات الوصفية الغنية. ندعوكم لقراءة ما يقوله محمد والاستماع إليه!

</div> <br>

{{% row %}} {{% column %}}

**<p align="center">English</p>**

<script src="https://fast.wistia.com/embed/medias/it8it7gvyy.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:54.37% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_it8it7gvyy videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/it8it7gvyy/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

{{% /column %}}
{{% column %}}

**<p align="center"> عربي </p>**

<script src="https://fast.wistia.com/embed/medias/wokgplg6hd.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:54.37% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_wokgplg6hd videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/wokgplg6hd/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

{{% /column %}}{{% /row %}}

<br><br>

> Tell us a bit about your organization, your objectives, and your role

<div style="text-align: right"> 

> أخبرنا قليلاً عن مؤسستك وأهدافك ودورك
</div><br>
My name is Mohamad Mostafa, and I am the Production Manager at Knowledge E. Within our publishing program, we publish around 2000 articles across 13 titles that are fully Open Access, which is something that I really value.  

<div style="text-align: right"> 
اسمي محمد مصطفى، وأنا مدير الإنتاج في نولدج إي. ضمن برنامج النشر الخاص بنا، ننشر حوالي 2000 مقالة عبر 13 عنوانًا مفتوح الوصول بالكامل، وهو أمر أقدره حقًا.
</div><br>
In a world that’s moving faster than ever, the availability, quality, and pursuit of knowledge are fundamental for advancement. Knowledge E, in line with its vision of developing a more knowledgeable world, helps institutions advance the quality of their research; move towards teaching excellence; upgrade library technology, services, and practices; and advance scholarship through journal publication, management, and training. In other words, it works with higher education institutions, research centres, ministries, publishers, and scholars to solve our society’s most significant challenges.   

<div style="text-align: right"> 
في عالم يتحرك بشكل أسرع من أي وقت مضى، يعد توافر المعرفة وجودتها والسعي وراءها أمورًا أساسية للتقدم. إن نوليدج إي، تماشياً مع رؤيتها لتطوير عالم أكثر معرفة ودراية، تساعد المؤسسات على تحسين جودة أبحاثها؛ التحرك نحو التميز في التدريس؛ ترقية مكتباتها الرقمية والخدمات والممارسات المتعلقة بها؛ ودعم المنح الدراسية المتقدمة من خلال نشر المجلات وإدارتها والتدريب. بمعنى آخر، تعمل شركة نولدج إي مع مؤسسات التعليم العالي ومراكز البحث والوزارات والناشرين والعلماء لحل أهم التحديات التي تواجه مجتمعنا.</div><br>

I am also a Crossref Ambassador. As part of the ambassador program, we aim to raise awareness about Crossref services among librarians, publishers, editors, and authors in the Middle East and North Africa region. As part of this, we run workshops in English and Arabic, emphasizing the importance of comprehensive metadata and persistent identifiers. We also help research communities improve their understanding of how to use Crossref services. The importance of making regional research objects easy to find, cite and reuse encouraged me to join the ambassador program.  
<div style="text-align: right"> 
أنا أيضًا سفير كروس رف. كجزء من برنامج السفراء، نهدف إلى زيادة الوعي حول خدمات Crossref بين أمناء المكتبات والناشرين والمحررين والمؤلفين في منطقة الشرق الأوسط وشمال إفريقيا. وكجزء من هذا، فإننا ندير ورش عمل باللغتين الإنجليزية والعربية، للتأكيد على أهمية البيانات الوصفية الشاملة والمعرفات المستمرة. نحن أيضًا نساعد مجتمعات البحث على تحسين فهمهم لكيفية استخدام خدمات .Crossref شجعتني أهمية تسهيل العثور على عناصر البحث الإقليمية والاستشهاد بها وإعادة استخدامها على الانضمام إلى برنامج سفراء كروس رف.</div><br>

> What is one thing that others should know about your country and its research activity?
<div style="text-align: right"> 

> ما هو الشيء الذي يجب أن يعرفه الآخرون عن بلدك ونشاطه البحثي؟
</div><br>
A lot of regional research is being produced (in Arabic) and even without proper infrastructure (the lack of language support within the international publishing ecosystems such as peer review systems, indexes, citations databases, submissions systems, etc.)  and the inadequate awareness about the various services (such as Crossref solutions) that can help with the discoverability and visibility of this research, the Arab region is increasingly recognised as a global leader in research outputs. Generally, these are some of the challenges and frustrations associated with the MENA (Middle East/North Africa) region.   
<div style="text-align: right"> 
يتم إنتاج الكثير من الأبحاث الإقليمية (باللغة العربية) وحتى بدون بنية تحتية مناسبة (نقص الدعم اللغوي داخل أنظمة النشر الدولية مثل أنظمة مراجعة الأقران، والفهارس، وقواعد بيانات الاستشهادات، وأنظمة التقديم، وما إلى ذلك) وعدم كفاية الوعي حول الخدمات المختلفة (مثل حلول(Crossref  التي يمكن أن تساعد في اكتشاف هذه البحوث وإبرازها، يتم الاعتراف بالمنطقة العربية بشكل متزايد كرائد عالمي في مخرجات البحث. بشكل عام، هذه بعض التحديات والإحباطات المرتبطة بمنطقة الشرق الأوسط وشمال إفريقيا.
</div><br>

> Are there trends in scholarly communications that are unique to your part of the world?
<div style="text-align: right"> 

>هل توجد اتجاهات في الاتصالات العلمية فريدة من نوعها في الجزء الذي تعيش فيه من العالم؟
</div><br>
In general, Open Access and Open Research are getting more and more attention in our region currently. We have recently launched the Forum for Open Research in MENA to raise awareness about all the new scholarly communications trends and support the Middle East and North Africa movement towards Open Science.    
<div style="text-align: right"> 
بشكل عام، يحظى الوصول الحر والبحث المفتوح باهتمام متزايد في منطقتنا حاليًا. لقد أطلقنا مؤخرًا منتدى الأبحاث المفتوحة في منطقة الشرق الأوسط وشمال إفريقيا لزيادة الوعي حول الاتصالات العلمية الجديدة ودعم حركة الشرق الأوسط وشمال إفريقيا نحو العلوم المفتوحة.
</div><br>
The Forum for Open Research in MENA (FORM) is a non-profit membership organisation supporting the advancement of open science policies and practices in research communities and institutions across the Arab world.  
<div style="text-align: right"> 
منتدى البحوث المفتوحة في الشرق الأوسط وشمال إفريقيا (FORM) هو منظمة غير ربحية ذات عضوية تدعم النهوض بسياسات وممارسات العلوم المفتوحة في المجتمعات والمؤسسات البحثية في جميع أنحاء العالم العربي.
</div><br>
We believe the Arab world has the resources and capability to play a pivotal role in the global transition towards more accessible, sustainable, and inclusive research and education models. And we want to support all our research communities and stakeholder groups in the journey towards a more ‘open’ world.  Our vision is to help unlock research for and in the Arab world. Our mission is to support the advancement of open science practices in research libraries and universities across the Arab world by facilitating the exchange of actionable insights and developing practical policies.  
<div style="text-align: right"> 
نعتقد أن العالم العربي لديه الموارد والقدرة على لعب دور محوري في التحول العالمي نحو نماذج بحث وتعليم أكثر سهولة واستدامة وشمولية. ونريد دعم جميع مجتمعاتنا البحثية ومجموعات أصحاب المصلحة في رحلتنا نحو عالم أكثر "انفتاحًا". رؤيتنا هي دعم الوصول الحر والبحوث المفتوحة في العالم العربي. ومهمتنا هي دعم تقدم ممارسات العلوم المفتوحة في مكتبات البحث والجامعات في جميع أنحاء العالم العربي من خلال تسهيل تبادل الأفكار القابلة للتنفيذ وتطوير السياسات العملية.
</div><br>
Our first Annual Forum was held in Cairo in October 2022 (as part of the global Open Access Week initiative). The event was a huge success, with over 1,100 delegates from over 48 countries across the globe. The next Annual Forum will be hosted in the UAE in October 2023, and details will be available shortly on our website.  
<div style="text-align: right"> 
عقد المنتدى السنوي الأول في القاهرة في أكتوبر 2022 (كجزء من مبادرة أسبوع الوصول الحر العالمي). حقق الحدث نجاحًا كبيرًا، حيث حضره أكثر من 1100 مندوب من أكثر من 48 دولة حول العالم. سيتم استضافة المنتدى السنوي القادم في دولة الإمارات العربية المتحدة في أكتوبر 2023، وستتوفر التفاصيل قريبًا على موقعنا.
</div><br>


> How would you describe the value of being part of the Crossref community; what impact has your participation had on your goals?
<div style="text-align: right"> 

 > كيف تصف قيمة أن تكون جزءًا من مجتمعCrossref ؟ ما هو تأثير مشاركتك على أهدافك؟
</div><br>
I have been a Crossref ambassador for more than 5 years now, and I can really say that it has been a great experience being part of such an amazing and collaborative community. We got the chance to interact with different publishers and service providers and participate in different Crossref annual events.  It’s also perfectly aligned with our vision of supporting Open Research.   
<div style="text-align: right"> 
لقد كنت سفيرًا لـ Crossref لأكثر من 5 سنوات حتى الآن، ويمكنني حقًا أن أقول إنها كانت تجربة رائعة أن أكون جزءًا من هذا المجتمع المذهل والتعاوني. لقد أتيحت لنا الفرصة للتفاعل مع مختلف الناشرين ومقدمي الخدمات والمشاركة في الأحداث السنوية المختلفة لـ .Crossref كما أنه يتماشى تمامًا مع رؤيتنا لدعم البحث المفتوح.
</div><br>
Recently, we have delivered a series of three Arabic webinars that offered basic metadata information and advanced insights about the role of metadata and how Crossref services can help an institution. These webinars have been well received by the community of regional publishers, university presses, and librarians. Dozens of questions have been answered, and technical enquires have been resolved. It was a great experience, and it was good to see that kind of interest in our community. Also, more educational webinars are yet to come!   
<div style="text-align: right"> 
قدمنا مؤخرًا سلسلة من ثلاث ندوات عربية عبر الإنترنت تمحورت حول معلومات البيانات الوصفية الأساسية ورؤى متقدمة حول دور البيانات الوصفية وكيف يمكن لخدمات Crossref أن تساعد المؤسسات البحثية. لقيت هذه الندوات عبر الإنترنت استحسان مجتمع الناشرين الإقليميين دور النشر الجامعية وأمناء المكتبات. تمت الإجابة على عشرات الأسئلة، وتم الرد على الاستفسارات الفنية. لقد كانت تجربة رائعة، وكان من المفرح أن نرى هذا النوع من الاهتمام في مجتمعنا. بالإضافة إلى ذلك، سيتم تقديم المزيد من الندوات التعليمية على الإنترنت في المستقبل.
</div><br>

> For you, what would be the most important thing Crossref could change (do more of/do better in)?
<div style="text-align: right"> 

> بالنسبة لك، ما هو الشيء الأكثر أهمية الذي يمكن لـ Crossref تغييره (القيام بالمزيد / القيام بعمل أفضل في)؟
</div><br>

Language is still a barrier in some parts of the Arab region, so producing more educational content in different formats (webinars, flyers, videos with subtitles, etc.) would be highly appreciated here.   
<div style="text-align: right"> 
لا تزال اللغة تشكل حاجزًا في بعض المناطق العربية، لذا سيكون إنتاج المزيد من المحتوى التعليمي بتنسيقات مختلفة (ندوات عبر الإنترنت، ونشرات، ومقاطع فيديو مع ترجمة، وما إلى ذلك) موضع تقدير كبير هنا.
</div><br>

> Which other organizations do you collaborate with or are pivotal to your work in open scholarship?
<div style="text-align: right"> 

> ما هي المنظمات الأخرى التي تتعاون معها أو التي تلعب دورًا محوريًا في عملك في مجال الابحاث المفتوحة؟
</div><br>
We work closely with ORCiD and invite them to our events, support DOAJ via our
charitable Foundation, and rely heavily on PKP products mainly the Open Journal
Systems (OJS) with plans to expand and start using Open Monograph Press (OMP).  
<div style="text-align: right"> 
إننا نعمل عن كثب مع ORCiD ونقدر دعمهم لفاعلياتنا، كما ندعم DOAJ عبر موقعنا ومؤسستنا الخيرية، ونعتمد بشكل كبير على منتجات مشروع المعرفة العامة وخاصة المجلة المفتوحة أنظمة (OJS) كما أننا نود التوسع والبدء في استخدام Open Monograph Press (OMP).  
</div><br>

> What are the post-pandemic challenges/hopes you are facing and how are you adapting to them/what you’re looking forward to?
<div style="text-align: right"> 

> ما هي التحديات / الآمال التي تواجهها في فترة ما بعد الجائحة وكيف تتكيف معها / ما الذي تتطلع إليه؟
</div><br>
We aim for more face-to-face meetings and onsite workshops/conferences as
the world opens up again. In addition, we have launched the Forum for Open Research
in MENA (FORM) (a non-profit membership organisation supporting the advancement of Open Science policies and practices in research communities and institutions across the  Arab region.)    
<div style="text-align: right"> 
نحن نهدف إلى المزيد من الاجتماعات وجهًا لوجه وورش العمل / المؤتمرات. بالإضافة إلى ذلك، أطلقنا منتدى البحث المفتوحMENA (FORM) ، وهي منظمة غير ربحية ذات عضوية تدعم النهوض بسياسات وممارسات العلوم المفتوحة في مجتمعات ومؤسسات البحث في جميع أنحاء المنطقة العربية.
</div><br>
A catalyst for positive action, we work with key stakeholders to develop and implement a pragmatic programme to facilitate the transition toward more accessible, inclusive, and sustainable research and education models in the Arab region. Our driving focus is on building the resources, the membership, the organisational structures, and the broader community to support the advancement of Open Science in research communities and research institutions across the Arab world.  

<div style="text-align: right"> 
كمحفز للعمل الإيجابي، نحن نعمل مع أصحاب المصلحة الرئيسيين للتطوير وتنفيذ برنامج عملي لتسهيل الانتقال نحو المزيد من نماذج البحث والتعليم الشاملة والمستدامة والتي يسهل الوصول إليها في المنطقة العربية. ينصب تركيزنا الدافع على بناء الموارد، والعضوية، والهياكل التنظيمية، والمجتمع الأوسع لدعم تقدم العلوم المفتوحة في المجتمعات البحثية والمؤسسات البحثية عبر العالم العربي.
</div><br>

Following the huge success of our 2022 Annual Forum (held in Cairo with the support and endorsement of UNESCO and the Egyptian Knowledge Bank), which attracted over 1100 delegates from 48 countries, our 2023 Annual Forum will be held in Abu Dhabi in the UAE. For more details about the event and the call for papers, see our website: [https://forumforopenresearch.com](https://forumforopenresearch.com)

<div style="text-align: right"> 

بعد النجاح الكبير لمنتدى 2022 السنوي (الذي عقد في القاهرة مع دعم وتأييد اليونسكو وبنك المعرفة المصري)، التي اجتذبت أكثر من 1100 مندوب من 48 دولة، المنتدى السنوي لعام 2023 سيعقد في أبو ظبي في دولة الإمارات العربية المتحدة. لمزيد من التفاصيل حول الحدث والدعوة للمشاركة، راجع موقعنا على الإنترنت:[https://forumforopenresearch.com](https://forumforopenresearch.com) 

</div><br>

> What are your plans for the future?
<div style="text-align: right"> 

> ما هي خططك المستقبلية؟
</div><br>
Keep working with different global and regional stakeholders to help the transition of our region towards Open Science.  
<div style="text-align: right"> 
استمر في العمل مع مختلف الشركاء العالميين والإقليميين للمساعدة في انتقال منطقتنا العربية نحو العلوم المفتوحة.
</div><br>
<br>
Thank you, Mohamad!
<div style="text-align: right"> 
شكرا لك يا محمد!
</div><br>