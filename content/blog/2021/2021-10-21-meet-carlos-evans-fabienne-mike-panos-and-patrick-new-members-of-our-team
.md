---
archives:
- 2021
author: Lindsay Russell
authors:
- Lindsay Russell
categories:
- Member Briefing
- Community
- Staff
date: 2021-10-21
draft: false
title: More new faces at Crossref
x-version: 0.0.0
---

Looking at the road ahead, we’ve set some [ambitious goals](/strategy) for ourselves and continue to see new members join from around the world, now numbering 16,000. To help achieve all that we plan in the years to come, we’ve grown [our teams](https://www.crossref.org/people/) quite a bit over the last couple of years, and we are happy to welcome Carlos, Evans, Fabienne, Mike, Panos, and Patrick.    

<!--more-->

Our Software Development team has seen the most growth with the addition of Carlos, Mike, Panos, and Patrick; collectively, they bring specialist skills that are helping us to pay down technical debt, modernize our underlying infrastructure, and prepare for a consistent front-end experience. As a member of the Product team, Fabienne has a fresh take on our Similarity Check service, steering the upgrade to iThenticate v2. And Evans brings a scientific researcher perspective to our Member Experience team along with experience as a member who’s worked with our tools.    

And now some words from each of them.   

## Carlos Del Ojo Elias

{{% imagewrap left %}}{{< figure src="/images/blog/2021/carlos-bw-blog.jpg" alt="image of Carlos" width="300px" >}}{{% /imagewrap %}}

I am a computer scientist with a master’s degree in Bioinformatics. Previously I used to work as a security auditor. I've got experience in research and software development both in academia and industry. It's very exciting for me to join Crossref as a Senior software developer on the technology team. My current project involves working on the authentication and authorization subsystems, exploring state-of-the-art technologies in order to improve our services. I have always enjoyed contributing to the open-source community, so it is a pleasure for me to work in an organization that promotes the principles of openness and transparency of software and data.  <br><br>

---

## Evans Atoni

{{% imagewrap right %}}{{< figure src="/images/blog/2021/evans-bw-blog.jpg" alt="image of Evans" width="250px" >}}{{% /imagewrap %}}

I am a member of the Technical Support team having joined Crossref just a few weeks ago. I’m passionate about advancing open access and POSI. Helping our members sort through knotty technical queries and building relations with them to service their very diverse needs is what I’m most excited about in my role.  In my spare time, I enjoy anything outdoors, family time, and traveling. I work remotely from Nairobi, Kenya.    <br><br><br><br><br><br><br><br>

---

## Fabienne Michaud

{{% imagewrap left %}}{{< figure src="/images/blog/2021/fabienne-blog.jpeg" alt="image of Fabienne" width="300px" >}}{{% /imagewrap %}}


I joined Crossref in April 2021 as a Product Manager for scholarly stewardship which includes the content comparison tool Similarity Check and I am thrilled to be a member of such a lovely, supportive and international team. I have a background in teaching and have worked in academic, research, and not-for-profit libraries in the UK for over 20 years in academic liaison, customer services, and management roles. These experiences have given me a user-centered approach and a drive to find collaborative, reliable, and pertinent technological solutions to support the research and scholarly community. Since starting at Crossref and, through my work with the Similarity Check Advisory Group, I have developed a good understanding of the current ethical issues facing the publishing sector (such as paper mills and other manipulations of the publication process) and a particular interest in how AI and automation tools can play a part in addressing these challenges.   <br><br>

---

## Mike Gill

{{% imagewrap right %}}{{< figure src="/images/blog/2021/mike-gill-blog.jpeg" alt="image of Mike" width="300px" >}}{{% /imagewrap %}}

I’ve been a software developer for twenty years, having studied software engineering at university. During my career, I have worked mostly in the banking and engineering industries so this is my first time working in scholarly publishing. I confess that before joining Crossref I wasn’t aware that the community existed so I was excited to see how I could ply my trade in this new (to me!) field. The role also appealed as, having primarily been a team leader/line manager in my recent career, this was an opportunity to be hands-on again and work with modern languages such as Kotlin. In the end, though, what really sealed it for me was reading on the Crossref website that ‘we take the work seriously but not necessarily ourselves’ which basically sums me up. So I knew I’d be in good company and that has proven to be the case!  

---

## Panos Pandis

{{% imagewrap left %}}{{< figure src="/images/blog/2021/panos3.jpg" alt="image of Panos" width="300px" >}}{{% /imagewrap %}}

I  joined Crossref as a Senior Software Developer in 2020, in the middle of the coronavirus pandemic. Moving to Crossref has been a much-needed breath of fresh air. I'm a big fan of open-source, and at Crossref, it just feels like home. Even more so after our recent commitment to the Principles of Open Scholarly Infrastructure (POSI). My main focus at the moment is Crossref's Event Data service. I'm fascinated by the potential of Event Data and the broad audience I get to support and communicate with through the project. So if you spot me in a room, feel free to ask me anything about Clojure/Kotlin, Event Data, obscure technology, or kombucha recipes.

 <br><br><br><br>

---

## Patrick Vale

{{% imagewrap right %}}{{< figure src="/images/blog/2021/patrick-bw-blog.jpg" alt="image of Patrick" width="300px" >}}{{% /imagewrap %}}

I'm delighted to have joined Crossref as the first Frontend Developer. My role covers the inauguration of a scalable framework in which we can build future User Interfaces, and generally making people's lives easier as they interact with our products and services - if a human uses it, I'm interested! It's my intention to provide a platform on which we can quickly iterate to build and adapt our interfaces to suit the rapidly changing needs of our community. It's been a pleasure to learn about the impact Crossref has across the scholarly spectrum; and to work with a team of open, practical, and downright friendly colleagues is a privilege. Outside of work, I enjoy cycling, growing things, and most recently, avoiding two small cats while moving from anywhere to anywhere around the house.    <br><br><br><br>

Your contributions have been impactful and it will be fun to see all that you will surely contribute to our road ahead!