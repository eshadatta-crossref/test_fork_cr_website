---
Archives:
- 2021
author: Ginny Hendricks
authors:
- Ginny Hendricks
- Rachael Lammey
- Patricia Feeney
categories:
- Affiliations
- Schema
- ROR
- Metadata
date: 2021-07-26
draft: false
title: Some rip-RORing news for affiliation metadata
x-version: 0.0.0
---

We’ve just added to our input schema the ability to include affiliation information using ROR identifiers. Members who register content using XML can now include ROR IDs, and we’ll add the capability to our manual content registration form, participation reports, and metadata retrieval APIs in the near future. And we are inviting members to a [Crossref/ROR webinar](https://crossref.zoom.us/webinar/register/WN_M5EFzTZCSBqsnbWiMMmMLQ) on 29th September at 3pm UTC.

## The background

We’ve been working on the [Research Organization Registry (ROR)](https://ror.org) as a community initiative for the last few years. Along with the California Digital Library and DataCite, our staff has been involved in setting the strategy, planning governance and sustainability, developing technical infrastructure, hiring/loaning staff, and engaging with people in person and online. In our view, it’s the best current model of a collaborative initiative between like-minded [open scholarly infrastructure (OSI)](http://openscholarlyinfrastructure.org) organizations.

Last year, Project Manager Maria Gould described [the case for publishers adopting ROR](/blog/publishers-are-you-ready-to-ror) and ROR was ranked the number one priority at our last in-person annual meeting. Now it’s time that Crossref’s services themselves took up the baton to meet the growing demand.

The inclusion of ROR in the Crossref metadata will help everyone in the scholarly ecosystem make critical connections more easily. For example, research institutions need to monitor and measure their output by the articles and other resources their researchers have produced. Journals need to know with which institutions authors are affiliated to determine eligibility for institutionally sponsored publishing agreements. Funders need to be able to discover and track the research and researchers they have supported. Academic librarians need to easily find all of the publications associated with their campus.

Earlier this month, GRID and ROR [announced](https://www.digital-science.com/press-release/grid-passes-torch-to-ror) that after working together to seed the community-run Research Organization Registry, GRID would be retiring from public service and handing the proverbial torch over to ROR as the scholarly community’s reliable universal open identifier for affiliations. That means that our members who have been using GRID now need to consider their move to ROR and think about how they can add ROR IDs into the metadata that they manage and share through Crossref.  

## The plan

We’ve been able to include ROR IDs for our grant metadata schema as affiliation information for two years, since July 2019. And the Australia Research Data Commons (ARDC) was the first member to add ROR IDs to the Crossref system in 2020. In early July, we completed the work to accept ROR IDs for affiliation assertions for all other types of records with an `affiliation` or `institution` element, such as journal articles, book chapters, preprints, datasets, dissertations, and many more.

Next, we will commence the plans to support ROR in our other tools and services, such as Participation Reports. We’ll work on alignment with the Open Funder Registry and share our plans to collect the information via the [new user interface we’re developing for registering and managing metadata](/blog/next-steps-for-content-registration). Open Journal Systems (OJS) already has a ROR Plugin, developed by the German National Library of Science and Technology (TIB). This supports the collection of ROR IDs and future releases of this plugin and the OJS DOI plugin will allow including ROR IDs in the metadata sent to Crossref, to support thousands of our members to share ROR IDs via their Crossref metadata.
We also aim to add ROR to our metadata retrieval options, including the REST API, which recently saw the start of an unblocking with our [move to a more robust technical foundation](/blog/behind-the-scenes-improvements-to-the-rest-api).

## The call for participation

Many Crossref publishers, funders, and service providers are already planning to integrate ROR with their systems, [map their affiliation data to ROR](https://ror.readme.io/docs/map-other-organization-id-types-to-ror), and include ROR in Crossref metadata. In addition to publishers and funders, libraries, repositories, and other stakeholders are developing support for ROR. For example, the [Plan S Journal Checker tool](https://journalcheckertool.org) uses ROR IDs to let people check whether a particular journal is compliant with an author's funder and institutional open access policies. In addition, the ROR website shows a growing list of [active and in-progress ROR integrations](https://ror.org/integrations).

{{< figure src="/images/blog/2021/crossref-ror-workflow-diagram.png" width="100%" >}}

Crossref members registering research grants via Altum’s ProposalCentral system can already add ROR IDs. Now those registering articles, books, preprints, datasets, dissertations, and other research objects, can start including much clearer and all-important affiliation metadata as part of their content registration going forward. As with all newly-introduced metadata elements, we recommend adding ROR IDs from now and ongoing, but planning a distinct project to backfill older records. We know that more than 80% of records have been updated and enriched at least once with additional and cleaner metadata, so as members do this routinely, they can include ROR IDs alongside updating URLs, license or funding information, and other metadata.

For information on how ROR will be supported in the Crossref metadata, take a look at [our latest schema release (version 5.3.0) ](https://gitlab.com/crossref/schema/-/releases/0.2.0) or in this [journal article example XML](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/journal.article5.3.0.xml).

Join the discussion in our forum below and register for the [Crossref/ROR webinar on September 29th at 3pm UTC](/services/content-registration/) to learn all you need to know about incorporating ROR into your Crossref metadata.