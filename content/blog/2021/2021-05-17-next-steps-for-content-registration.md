---
Archives:
- 2021
author: Sara Bowman
authors:
- Sara Bowman
categories:
- Content Registration
- Product
- Community
date: 2021-05-17
draft: false
title: Next steps for Content Registration
x-version: 0.0.0
---

UPDATE, 20 December 2021

_We are delaying the Metadata Manager sunset until 6 months after release of our new content registration tool. You can expect to see the new tool in production in the first half of 2022. For more information, see this [post](https://community.crossref.org/t/delaying-metadata-manager-sunset-date/2304) in the Community Forum._

---

Hi, I’m Sara, one of the Product Managers here at Crossref. I joined the team in April 2020, primarily tasked with looking after Content Registration mechanisms. Prior to Crossref, I worked on open source software to support scientific research. I’ve learned a lot in the last year about how our community works with us, and I’m looking forward to working more closely with you in the coming year to improve Content Registration tools.

Just over a year ago, we updated you on the status of [Metadata Manager](https://www.crossref.org/blog/metadata-manager-update/). TL;DR: We learned that our approach with the tool wasn’t flexible enough to easily and quickly add other content types or update the input schema, and paused new development. We’re back with another update on Metadata Manager and our strategy for Content Registration user interfaces (UIs) going forward.

## Our helper tools for Content Registration

The bulk of content registered with us is done so programmatically; that is, our members’ (or their service providers’) machines talking to our machines using our APIs. But, there are plenty of our members that don’t have the technical expertise to work with us this way. For those members, we provide various helper tools to assist with manual content registration.

We offer a variety of interfaces for registering many different types of content, including Web Deposit form for most content types, Metadata Manager for journal content, and Simple Text Query to register references. Each of these has its own use cases and limitations, leading to a confusing and inconsistent experience for members who are manually depositing metadata. From our perspective, maintaining this many interfaces in different codebases is inefficient, in part because an update to the schema likely leads to separate updates in each of them. A unified user interface to register content would both improve and simplify the user experience for you, our community, and make updates quicker and more efficient. The original goal of Metadata Manager was to be this unified interface. But we’ve learned that the approach we took was flawed: there have been problems reported by users, and the tool itself isn’t flexible enough to easily and quickly add new content types or support new fields when our input schema changes.

## A new approach to helper tools

So we’ve decided to build something new and retire the old. We’ll be focusing on creating a brand new Content Registration user interface that will eventually replace Metadata Manager, the Web Deposit form, and Simple Text Query. And what we’ve learned from our experiences with Metadata Manager and Web Deposit has greatly influenced our strategy going forward. The new tool will:

### Have a Community focus
* **Design for small** - Our membership demographic is evolving. A large (and growing) number of our members are very small, often with a single publication and no technical resources. Creating XML can be a barrier to participating in Crossref, and our helper tools are designed to lower that barrier.
* **Accessibility and localization support** - All of our UIs should support major international accessibility guidelines and translation into local languages, to meet the needs of our global membership.  
* **Open source code** - Build in the open, so that others can contribute. This could mean an entire UI that we haven’t prioritized, or adding a new translation file, or tweaking some CSS.

### Follow user-centered design processes
* **Unified user interface** - Improve user experience and simplify tools and services by providing members with one place to go to register content via a UI.
* **Rapid iteration** - Focus on a technical solution that allows for rapid development of UIs to support new content types and updates to our schema.
* **Building the right features for the right users** - The needs of our large members and smaller members are different. Experience has shown us that the core audience for a helper tool is smaller members; we’ll tailor the features to solve the challenges of our smaller members.

### Allow us to build content for the future
* **Tactical approach to content types** - Quickly build UIs in a strategic order. We can’t build support for every content type at once, so we want to identify and build in the areas of highest impact/lowest effort first.
* **Deliberate approach to supported fields** - Not all members will supply metadata for all fields in our schema. Building a UI to support all fields for a specific content type before moving on to another slows progress on that next content type. We’ll identify the most-used and most-useful fields to support first, and add more in a future iteration if needed.

## Deprecating Metadata Manager

In order to free up the resources to develop the new Content Registration UIs, we need to stop doing other things - that means not adding to, supporting, or bug-fixing other Content Registration tools. We’re setting an aggressive goal of sunsetting Metadata Manager by the end of 2021, with a commitment to a smooth transition to our new tool. This means that new members should not start using Metadata Manager. New members who need a helper tool have a few choices:

- those who use the OJS platform from PKP to host their journals (OJS V3 and above) should use the third party Crossref OJS plugin to register their content.
- other new members should use the Web Deposit form
- current members who are using Metadata Manager may continue to do so, but are advised that we won’t be doing bug fixes or further development on the tool, and that support will be scaled back. If possible, you should transition over to using the Web Deposit form.

This wasn’t a decision made lightly, but one made after considering multiple options and all the data available to us about member usage and internal resources.

To highlight some of the data that led to this decision: the Support team tracks the types of support tickets they handle. In 2020, the 3rd most common ticket type was Metadata Manager-related. But less than 4% of metadata records registered with us are registered using Metadata Manager. Supporting Metadata Manager requires resources disproportionate to the amount of use the tool gets. For comparison, twice as many records are registered using the Web Deposit Form, but it generates far fewer Support tickets. To fix the bugs and issues reported about Metadata Manager requires an equally disproportionate amount of developer resources. So far, we have been unable to free up resources we would need to fix them all. Continuing to maintain this tool is effectively preventing us from building something new that will better meet the needs of our smaller members.

We know this will surprise and concern some of you, especially heavy users of Metadata Manager. We’re committed to making this a smooth transition, and over the coming months, we’ll provide more guidance to help current members migrate to our other tools.

## Involving the community
Building a tool that allows us to create and adapt content registration forms based on example input files is an exciting new approach - one that will allow us to better serve the needs of our smaller members across multiple content types and support those who want to adapt our tools to their own needs. We’ve already begun work on a proof-of-concept tool aligned with this new strategy and I’m excited to drive it to production. As this project develops, we’ll keep in close contact with members, conducting user interviews, feedback sessions, and using usage data to help guide our decision-making on features and design. As we’ll be building in the open, we’ll have prototypes to share along the way as we iterate to produce a tool that will stand the test of time as well as scale to support even more content and members in future. We welcome your feedback over on our [Community Forum](https://community.crossref.org/t/feedback-on-new-helper-tool/1721), where we’ve set up a dedicated category to discuss this topic.