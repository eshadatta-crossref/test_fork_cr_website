---
archives:
- 2021
author: Lucy Ofiesh
authors:
- Lucy Ofiesh
categories:
- Board
- Member Briefing
- Governance
- Elections
- Crossref Live
- Annual Meeting
date: 2021-09-28
draft: false
title: 2021 Board Election
x-version: 0.0.0
---

We are pleased to share the 2021 board election slate.  Crossref’s Nominating Committee received over 60 submissions from members worldwide to fill five open board seats.  It was a fantastic group of applicants and showed the strength of our membership community.

There are five seats open for election (three small, two large), and the [Nominating Committee](/committees/nominating/) presents the following slate.  

## The 2021 slate

Candidate organizations, in alphabetical order, for the Small category (three seats available):

* **California Digital Library, University of California**, Lisa Schiff
* **Center for Open Science**, Nici Pfeiffer
* **Melanoma Research Alliance**, Kristen Mueller
* **Morressier**, Sebastian Rose
* **NISC**, Mike Schramm

Candidate organizations, in alphabetical order, for the Large category (two seats available):

* **AIP Publishing (AIP)**, Penelope Lewis
* **American Psychological Association (APA)**, Jasper Simons
* **Association for Computing Machinery (ACM)**, Scott Delman

{{% divwrap blue-highlight %}}

### [Here are the candidates' organizational and personal statements](/board-and-governance/elections/2021-slate/)

{{% /divwrap %}}

## You can be part of this important process by voting in the election

If your organization is a voting member in good standing of Crossref as of September 20, 2021, you are eligible to vote when voting opens on September 29, 2021.

## How can you vote?

On September 29, 2021, your organization's designated voting contact will receive an email with the Formal Notice of Meeting and Proxy Form with concise instructions on how to vote.  You will also receive a user name and password with a link to our voting platform.

 The election results will be announced at the [LIVE21 **online** meeting](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0051/t/page/fm/0) on November 9, 2021.  Save the date!