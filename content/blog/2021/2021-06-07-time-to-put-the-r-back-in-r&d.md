---
Archives:
- 2021
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Labs
- R&D
date: 2021-06-07
draft: false
title: Time to put the "R" back in "R&D"
x-version: 0.0.0
---

It is time to put the 'R' back into R&D.

The Crossref R&D team was originally created to focus on the kinds of research projects that have allowed Crossref to make transformational technology changes, launch innovative new services, and engage with entirely new constituencies. Some Illustrious projects that had their origins in the R&D group include:

{{% imagewrap right %}}{{< figure src="/images/blog/2021/labs-logo-ribbon.svg" align="right" width="100%" >}}{{% /imagewrap %}}

*   DOI Content Negotiation
*   Similarity Check (originally CrossCheck)
*   ORCID (originally Author DOIs)
*   Crossmark
*   The Open Funder Registry
*   The Crossref REST API
*   Linked Clinical Trials
*   Event Data
*   Grant registration
*   ROR

And for each project that has graduated, there have been several that have not. Some projects were simply designed to gather data. Others just didn’t generate enough interest. You are not truly experimenting if you don’t fail occasionally too.

Recently we’ve been doing very little experimenting of any kind. Instead, the R&D team has mostly been seconded to the software development team to help them through a period of organizational and process change. We would not have made it through the past two years without their help.

But now we’re ready to focus on more ‘R’ and less ‘D’. And to that end, we are increasing the size of the team as well. [Rachael Lammey](/people/rachael-lammey/) will be joining the team as Head of Strategic Initiatives. She will work alongside our Principal R&D Developers, [Esha Datta](/people/esha-datta/) and [Dominika Tkaczyk](/people/dominika-tkaczyk/). Together they will be able to engage with new communities and immediately start experimenting with ways in which Crossref might be able to address their needs and use-cases.

We hope to soon add to our list of distinguished R&D project alumni.

## Rationale & details
{{% imagewrap left %}}{{< figure src="/images/blog/2021/creature1.svg" align="right" width="100%" >}}{{% /imagewrap %}}
The Crossref R&D group (AKA "Labs") has been the incubator of many services that are now in production and which form a fundamental part of Crossref's identity and value. Similarity Check, ORCID, Crossmark, Open Funder Registry, The REST API, Linked Clinical Trials, and Event Data all started as R&D projects. More recently the enhancement of our reference matching infrastructure and the development and launch of ROR were also R&D projects.

And prior to the formation of the outreach group in 2015, the R&D group also led a critical function engaging with communities that, at the time, Crossref only had tangential connections with: [PKP](https://pkp.sfu.ca/); [DOAJ](https://doaj.org/); funders; and the data and altmetrics communities.

But since the R&D group merged with the technology team back in 2019, we have done very little "R." and very little community engagement of our own. Instead, the R&D team has supported the development team through a period of major cross-cutting projects and organisational change. Dominika has led the REST API rewrite and Esha---when she is not acting as technical lead on ROR---has also worked on the API rewrite and has kept Crossref metadata search on its feet. We would not have been able to make it through the past few years without their help.

Throughout this period, Rachael Lammey has continued the vital work of identifying, engaging with, and advocating for members of our community who we previously didn't even know were members of our community.

{{% imagewrap right %}}{{< figure src="/images/blog/2021/creature2.svg" align="right" width="100%" >}}{{% /imagewrap %}}

The strength of the R&D group was that it combined outreach, product, and development functions. It was not only able to engage with new constituencies, but to quickly experiment with ways in which Crossref might be able to serve them. Previously, members of the R&D team would return from a conference or workshop that no Crossref member had ever attended before with a set of new contacts and ideas for new services and tools. They'd form interest groups and develop prototypes. Sometimes the interest groups would lead nowhere and sometimes the prototypes would be discarded. But critically, some of them would turn into the major services and organisations that now form a foundational part of open scholarly infrastructure.

And this is why it makes so much sense for Rachael to join the R&D team. The group is most effective when it is able to engage with new communities and immediately start experimenting with ways in which Crossref might be able to address their needs and use-cases. Rachael's extensive experience in both product management and outreach---combined with Esha and Dominika's experience leading development projects---is exactly what we need to reinvigorate the group and put the R back into R&D.

To kick off, we are going to be working on some small-ish, discrete projects. These include:

*   Better matching and linking of preprints to published articles;
*   Extending our journal title classification to cover all journal and conference proceedings titles; and
*   Tools to allow us to community-source structured metadata correction information and feed it back to our members.

{{% imagewrap left %}}{{< figure src="/images/blog/2021/creature3.svg" align="right" width="80%" >}}{{% /imagewrap %}}
We will consult with and update the community on the kinds of projects we are working on through regular tech updates and a revitalised [Labs](/labs) area of our website.

Oh- and we will certainly be designing some new Labs creatures.     
--G