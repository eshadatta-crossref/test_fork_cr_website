---
archives:
- 2006
author: thammond
authors:
- thammond
categories:
- RSS
date: 2006-10-02
title: Wiley Does RSS, Too!
x-version: 0.0.0
---

[This post][1] blogged by Rafael Sidi at [EEI][2]. Wiley are now dishing out RSS feeds. And moreover from a cursory inspection (see e.g. here for the _American Journal of Human Biology_) it seems like they are putting out RSS 1.0 (RDF) and DC/PRISM metadata. Don’t know if there’s anyone from Wiley who can comment on this. But this really is the best news. (Now, who else can we get to join the party. 😉

[_Editor's update: Link to Wiley was broken and removed. January 2021_]

 [1]: http://rafaelsidi.blogspot.com/2006/10/rss-feeds-in-wiley-journals.html
 [2]: http://www.ei.org/