---
archives:
- 2006
author: Ed Pentz
authors:
- Ed Pentz
categories:
- News Release
date: 2006-10-12
slug: journal-supply-chain-efficienc
title: Journal Supply Chain Efficiency Improvement Pilot
x-version: 0.0.0
---

This project - <https://web.archive.org/web/20061004011422/www.journalsupplychain.com/> - (which needs a new name or clever acronym) has released a [Mid Year Report][1]. The pilot is being extended into 2007 and there is clearly value for publishers in having an unique ID for institutions at the licensing unit level. Ringgold, one of the project partners, has a great database with a validated hierarchy of institutions from consortia down to departments - I had a demo at Frankfurt. The report has some info on benefits for publishers and on possible business models. I think a central, neutral registry of unique IDs would be a real benefit to the industry.

<!--more-->



From the report:

&#8220;Publishers

Certainly publishers are already using an institutional identifier internally with major

marketing and customer communication benefits. The main areas where the proposed

identifier could add value to the communication between the publisher and customer

should be in areas such as:

• accurate COUNTER usage reports

• institutional renewals being unrecognized as such and therefore appearing as

new subscriptions

• easier ability to track institutional end-users of consolidated subscriptions

(especially those where the agent does not deliver orders via ICEDIS

structured FTP with Type 2 addresses incorporated in the complete record)&#8221;

On business models:

&#8220;A sensible business model would have those that receive the most economic benefit

from a respective service providing a respective level of funding to support costs. It is

clear that publishers are the primary beneficiaries of the institutional identifier, with

clear benefits, thereby suggesting they should bear the proportionate cost. Ultimately

the subscriber pays anyway; economies are reflected in reduced cost to the subscriber

in a competitive market.

Other participants would see service improvements, but not the same clear benefits. It

would therefore be reasonable to ask the publishers to bear the major cost of the

establishment of such an identifier, and to a certain extent they have already done so

by subscribing selectively to Ringgold’s existing auditing and database services.

The various and relevant business revenue streams might be reflected as follows:

• Free service: limited search only, with number of searches per day restricted,

possibility of searchers to edit or input information using a “response form”

designed for such purposes

• Basic subscription: unlimited search access to the database

• Database license for hosting services: download of standard selected metadata

• Database license for publishers: access for download of selected metadata, and

automatic receipt of alerts for changes&#8221;

 [1]: https://web.archive.org/web/20060904075439/http://www.journalsupplychain.com/press_files/JSCEI%20Pilot%20mid-year%20report%20external%2027Sep06.pdf