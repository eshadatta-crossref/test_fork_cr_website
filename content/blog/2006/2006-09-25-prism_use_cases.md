---
archives:
- 2006
author: thammond
authors:
- thammond
categories:
- Discussion
date: 2006-09-25
title: PRISM Use Cases
x-version: 0.0.0
---

At last week’s PRISM Face to Face meeting at Time Inc. (NY), Linda Burman raised the question of how (STM) publishers were using PRISM beyond RSS. I gave a brief presentation of how we at Nature were using PRISM: RSS (well you all know about that), Connotea (our social bookmarking tool), SRU (Search/Retrieve by URL), and OTMI (Open Text Mining Interface - which we’ll shortly be making available for wider comment). Be interested to learn if anyone else is using PRISM in other ways.