---
archives:
- 2006
author: thammond
authors:
- thammond
categories:
- Blogs
date: 2006-10-03
title: Blogs, Well Duh!
x-version: 0.0.0
---

Steve Rubel has a reponse [here][1] to Lexis-Nexis’ [survey][2] on consumers preferred outlets for breaking news and their rubbishing of blogs as a credible publishing forum. It’s something called, er, the [Long Tail][3] by Chris Anderson at _Wired Magazine_.

 [1]: http://www.micropersuasion.com/2006/10/duh_of_course_c.html
 [2]: http://news.com.com/2100-1025_3-6121778.html?part=rss&#038;tag=6121778&#038;subj=news
 [3]: http://en.wikipedia.org/wiki/Long_tail