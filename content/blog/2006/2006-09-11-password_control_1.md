---
archives:
- 2006
author: Ed Pentz
authors:
- Ed Pentz
categories:
- Blog
date: 2006-09-11
slug: password-control-1
title: password control
x-version: 0.0.0
---

We’ve taken the top level access control off the site. This means that anyone can read the blog but posting will be limited to those with an account (Crossref members and invited participants). This will make it possible to include the CrossTech feed in your regular RSS reader/aggregator. We’ll soon be posting some general terms and conditions for this blog and also sending a message to all Crossref members about joining so we should see membership (and activity) pick up.