---
archives:
- 2006
author: thammond
authors:
- thammond
categories:
- XML
date: 2006-11-28
title: And Just Relax
x-version: 0.0.0
---

Nice piece of advocacy [here][1] by Tim Bray for [RELAX][2]. High time to see someone standing up for RELAX - a much friendlier XML schema language.

 [1]: http://www.tbray.org/ongoing/When/200x/2006/11/27/Choose-Relax
 [2]: http://relaxng.org/