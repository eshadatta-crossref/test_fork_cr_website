---
archives:
- 2006
author: thammond
authors:
- thammond
categories:
- RSS
date: 2006-10-03
title: Couple Web Feeds to Note
x-version: 0.0.0
---

Sorry to be somewhat backwards, but just in case any folks didn’t already know there’s a couple new feeds set up recently (or at least they’re newish to me 🙂

  * [News from STM][1] (from the [STM Association][2])
      * [eFoundations][3] (from Andy Powell and Pete Johnston at [Eduserv Foundation][4] in the UK) </ul>

 [1]: https://web.archive.org/web/20060923073323/http://www.stm-assoc.org/home/rss.xml
 [2]: http://www.stm-assoc.org/
 [3]: http://efoundations.typepad.com/
 [4]: https://web.archive.org/web/20061002052838/http://www.eduserv.org.uk/foundation/