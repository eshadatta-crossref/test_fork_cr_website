---
archives:
- 2012
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Citation Formats
- Crossref Labs
- Metadata
- PDF
date: 2012-04-17
title: PDF-Extract
x-version: 0.0.0
---

# PDF-EXTRACT

[Crossref Labs][1] is happy to announce the first public release of &#8220;[pdf-extract][2]&#8221; an open source set of tools and libraries for extracting citation references (and, eventually, other semantic metadata) from PDFs. We first demonstrated this tool to Crossref members at our annual meeting last year. See the [pdf-extract labs page][2] for a detailed introduction to this new set of tools.

If you are unable to download and install the tool, [you can play with a experimental web interface called &#8220;Extracto.&#8221;][3] Be warned, **Extracto is running on very feeble server using an erratic and slow internet connection**. The only guarantee that we can make about using it is that **it will repeatedly fall over and annoy you.** _The weasel has spoken._
  
<!--more-->

 [1]: http://labs.crossref.org/
 [2]: http://labs.crossref.org/pdfextract/
 [3]: http://extracto.labs.crossref.org/