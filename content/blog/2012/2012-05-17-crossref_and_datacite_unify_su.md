---
archives:
- 2012
author: admin
authors:
- admin
categories:
- Content Negotiation
- DataCite
date: 2012-05-17
title: Crossref and DataCite unify support for HTTP content negotiation
x-version: 0.0.0
---

Last year Crossref and DataCite announced support for HTTP content negotiation for DOI names. Today, we are pleased to report further collaboration on the topic. We think it is very important that the two largest DOI Registration Agencies work together in order to provide metadata services to DOI names.

The current implementation is documented in detail at http://citation.crosscite.org/

The documentation explains HTTP content negotiation as implemented by both Registration Agencies and provides a list of supported content types.

An example application of HTTP content negotiation is a citation formatting service. You can try it at http://citation.crosscite.org/.

This service will accept DOIs from both Crossref and DataCite, unlike the previous formatting service which accepted only Crossref DOI names.

This is possible because Crossref and DataCite support a shared, common metadata format. When you input a DOI into the formatting service, it doesn’t know where the DOI was registered. The service will make an

HTTP content negotiation request to the global DOI resolver specifying which format of the metadata should be returned in the HTTP Accept header. The global DOI resolver will notice (Accept header!) that this is not a regular DOI resolution request; it will turn to Crossref or DataCite accordingly for the relevant metadata instead of redirecting to a landing page. The format of metadata is shared between both registration agencies so the formatting service can interpret it without knowledge of the DOI origin.

In summary HTTP content negotiation lets you process a DOI’s metadata without knowledge of its origin or specifics of the registration agency.

If you have any problems, email us at
<tech@datacite.org> or <labs@crossref.org>. For general discussion please kindly leave a comment below.