---
archives:
- 2010
author: thammond
authors:
- thammond
categories:
- Linked Data
date: 2010-02-10
title: The Response Page
x-version: 0.0.0
---

(**Update - 2010.02.10:** I just saw that I posted [here][1] on this same topic over a year ago. Oh well, I guess this is a perennial.)

I am opening a new entry to pick up one point that John Erickson made in his [last comment][2] to the previous entry:

> _&#8220;I am suggesting that one &#8220;baby step&#8221; might be to introduce (e.g.) RDFa coding standards for embedding the doi:D syntax.&#8221;_

Yea!

It might be worth consulting the latest Crossref [DOI Name Information and Guidelines][3] to see what that has to say about this. *Section 6.3 - The response page* has these two specific requirements for publishers:

  1. When metadata and DOIs are deposited with Crossref, the publisher **must** have active response pages in place so that they can resolve incoming links.
      * A minimal response page **must** contain a full bibliographic citation displayed to the user. A response page without bibliographic information should never be presented to a user.</ol>
        What is truly shocking about these requirements is that this are purely user focussed. There is no mention whatsoever of machines. One might have thought that with the [Linked Data][4] gospel in full swing there would at least be a nod to machine-readable metadata. But there’s none. I’m not saying that there should be any requirement, or even any recommendation. But a mention might have been useful to chivvy us all along.

        I agree with John that publishers could be encouraged (or even just reminded) that machine-readable metadata could be made available through various mechanisms: HTML META tags (such as we currently provide at Nature - and as blogged [here][5] earlier), COinS objects, RDF/XML comments, or best of all RDFa markup as John mentions.

        The Web is getting semantic. It’s about time that Crossref members joined the wave. And would be helpful if Crossref were there to help us with some new guidelines too!

 [1]: /blog/machine-readable-are-we-there-yet
 [2]: /blog/doi-what-do-we-got
 [3]: https://doi.org/10.13003/5jchdy
 [4]: https://web.archive.org/web/20100413223720/http://linkeddata.org/
 [5]: /blog/natures-metadata-for-web-pages