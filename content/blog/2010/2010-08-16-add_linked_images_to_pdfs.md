---
archives:
- 2010
author: Geoffrey Bilder
authors:
- Geoffrey Bilder
categories:
- Crossref Labs
- PDF
date: 2010-08-16
title: Add linked images to PDFs
x-version: 0.0.0
---

While working on an internal project, we developed &#8220;[pdfstamp][1]&#8220;, a command-line tool that allows one to easily apply linked images to PDFs. We thought some in our community might find it useful and have [released it on github.][2] Some more PDF-related tools will follow soon.

 [1]: http://labs.crossref.org/pdfstamp/
 [2]: http://github.com/Crossref/pdfstamp