---
archives:
- 2019
author: Patricia Feeney
authors:
- Patricia Feeney
categories:
- Identifiers
- Grants
- Research Funders
- Persistence
date: 2019-02-07
draft: false
title: Request for feedback on grant identifier metadata
x-version: 0.0.0
---

We first announced plans to investigate identifiers for grants in 2017 and are almost ready to violate the [first rule of grant identifiers](/blog/global-persistent-identifiers-for-grants-awards-and-facilities/
) which is “they probably should not be called grant identifiers”. Research support extends beyond monetary grants and awards, but our end goal is to make grants easy to cite, track, and identify, and ‘Grant ID’ resonates in a way other terms do not. The truth is in the metadata, and we intend to collect (and our funder friends are prepared to provide) information about a number of funding types. Hopefully we encompass all of them.

Our technical & metadata working group (a subset of the broader [Funder Advisory Group](/working-groups/funders)) includes folks from Children's Tumor Foundation, Europe PMC, European Research Council, JST, OSTI (DOE), Smithsonian, Swiss National Science Foundation, UKRI, Wellcome, as well as colleagues at DataCite and ORCID.

They have provided a wealth of funding data and feedback, and together we’ve come up with a metadata schema that works for us. Just as important - does this set of metadata meet your needs? Did we miss something? Let us know.

### The details

For those of you familiar with Crossref Content Registration, Grant IDs will have their own dedicated schema that differs from our publication schema. The Grant ID schema will follow some of the same conventions as we’ll be using the same system to process the files (which will be XML) but since we are collecting metadata for a new community and moving beyond published content, this is an opportunity to rethink how we handle some basics like person names and dates.

Each Grant ID can be assigned to multiple projects. The metadata within each project includes basics like titles, descriptions, and investigator information (including affiliations) as well as funding information. Funders will supply funder information (including funder identifiers from the Crossref Funder Registry) as well as information about funding types and amounts.

A major accomplishment of the group was to develop a simple taxonomy of types of funding. Supported types are:

* award
* contract
* grant
* salary-award
* endowment
* secondment
* loan
* facilities
* equipment
* seed-funding
* fellowship
* training-grant
* other

Funding involves more than monetary grants or awards and we’ve attempted to capture the broad categories of funding types. This list is taken from types of funding as defined by our participating funder organizations. We anticipate this list will evolve over time.

Ready to dig in? The schema and documentation are [available on GitHub](https://github.com/CrossRef/grantID-schema/). We will actively take feedback until the end of February 2019. We hope to begin implementation soon after that. Please let us know what you think through GitHub, or feel free to contact me via [feedback@crossref.org](mailto:feedback@crossref.org).