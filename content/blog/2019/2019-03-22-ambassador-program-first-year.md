---
archives:
- 2019
author: Vanessa Fairhurst
authors:
- Vanessa Fairhurst
categories:
- Education
- Ambassadors
- Community
- Collaboration
date: 2019-03-22
draft: false
title: Here’s to year one!
x-version: 0.0.0
---

Our Ambassador Program is now one year old, and we are thrilled at how the first 12 months have gone. In 2018 we welcomed 16 [ambassadors](/community/our-ambassadors/) to the team, based in Australia, Brazil, Colombia, India, Indonesia, Mexico, Nigeria, Peru, Russia, Singapore, South Korea, UAE, Ukraine, USA, and Venezuela.

Our ambassadors are volunteers with a good knowledge of Crossref and the wider scholarly community, they are well connected and passionate about the work that we do. Participating in the ambassador program is complementary to people’s existing roles and enables those who already work with Crossref to have a mechanism to feed back to us and to provide support for their communities.

We reflected on the successes and challenges of the first 12 months and discovered quite a lot has been achieved so far.

{{% quotecite "Woei Fuh Wong, Research 123, Singapore" %}}
The Ambassador Program better equips me to support researchers to conduct outreach and collaborate in multidisciplinary discovery!
{{% /quotecite %}}

{{% quotecite "Andrii Zolkover, Internauka, Ukraine" %}}
Within the framework of the Ambassador Crossref program, I ran a seminar, webinar, and held several meetings in Ukrainian scientific organizations.
{{% /quotecite %}}

{{% quotecite "Maxim Mitrofanov, NEICON, Russia" %}}
In my role as ambassador, I am able to provide a greater level of support in Russian. Alongside translated materials, we have also received over 400 tickets to our Russian electronic support system and made over 300 consultations by phone.
{{% /quotecite %}}

{{% quotecite "Edilson Damasio, Department of Mathematics Library of State University of Maringá-UEM, Brazil" %}}
Being an ambassador has enabled me to increase knowledge of Crossref within my community.
{{% /quotecite %}}

{{% quotecite "Mohamad Mostafa, Knowledge E, UAE" %}} The ambassador program has helped in vastly raising the awareness of Crossref and its services all over the world. Based in the Middle East, I see the need in the Arab region to know more about Crossref in their mother tongue (Arabic). The program has proven success and its positive impact is tangible.
{{% /quotecite %}}

## Highlights

Over the course of 2018, there were a number of big achievements which would not have been possible without the help of our ambassadors.

Due to your feedback, we’re very keen to expand the level of multi-language support we offer our diversifying community. In addition to translating key messages, slide decks, and other educational materials, our ambassadors (and some members - thanks also!) helped us in the production of a series of short videos. We now have videos available for each of the Crossref services in nine languages including English, French, Spanish, Brazilian Portuguese, Chinese, Japanese, Korean, Arabic, and Bahasa Indonesia. You can see in the chart below, that although our English videos have the most views (this is the default language), others have also experienced a lot of visitors, particularly notable are the Chinese and Spanish language videos. This underscores the importance of further support in non-English languages, as our series of multi-lingual webinars also demonstrated.

<p align="center">
<img src="/images/blog/Service-video-plays-ambassador-blog.png" alt="Service video plays" width="500px" />
</p>

In 2018 we ran webinars in Russian, Brazilian Portuguese, Spanish and Arabic. Several ambassadors have taken a lead in running these webinars in their local languages with assistance from Crossref staff on producing materials and answering questions on the day. Spanish language webinars saw record numbers of attendees from a range of different countries, and our Russian webinar recordings have been viewed over 200 times. We will be continuing to offer more webinars in different time zones and languages, and the [recordings](/webinars/) are always available for anyone who can’t attend on the day.

<p align="center">
<img src="/images/blog/Arabic-webinar-ambassador blog.png" alt="Arabic webinar" width="500px" />
</p>

Our ambassadors have also been helping us improve and expand our [LIVE local events](/events/). Last year we held events in Japan, South Africa, Russia, Germany, Brazil and India. Ambassadors help by providing recommendations on venues, accommodation, guest speakers, or even attending and speaking at the event themselves. Some run their own Crossref events which we can help provide materials and also represent Crossref at related industry events in their region. You may have had the chance to meet some of our ambassadors at our annual event in Toronto last November as well.

As our ambassadors are our representatives, acting as our eyes and ears in the wider community, it is important that they are kept up to date with new developments and have good opportunities to report back to us. The ambassador team has participated in beta-testing of a number of new initiatives including our new [Metadata Manager](/education/member-setup/metadata-manager/) and [Participation Reports](https://www.crossref.org/members/prep/) and our upcoming Community Forum. By providing feedback from their own user perspective and from how they anticipate those in their communities will view and use these tools, it enables us get better insights into how an initiative might work before launching it more widely.

## Future Plans

Initial feedback on the program has been overwhelmingly positive, both from the ambassadors themselves and the wider Crossref community, so we’re looking at what we can do to hone the program over time. In 2019 we will be welcoming some more ambassadors to the team to further support our global community. We want to support our ambassadors, so we don’t foresee the group growing to the point where there are too many ambassadors for us to be able to engage with. You can read about the team and where they are based, as well as all about the new ambassadors we have welcomed so far this year, on [Our Ambassadors](/community/our-ambassadors/) page.

This year our ambassadors will be involved when we launch our online community forum (more to come on that soon). They’re already helping with the task-force that is advising on our new documentation, and we’ll be providing them with further training on Crossref tools and services. We also have more webinars and LIVE locals in the pipeline. Keep an eye on our [webinars](/webinars/) and [events](/events/) pages for more details as they come.

So a final thank you to our ambassador team - it has been great to work with you over the last year, and we look forward to how we can continue to work together!