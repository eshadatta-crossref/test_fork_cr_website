---
archives:
- 2019
author: Ginny Hendricks
authors:
- Ginny Hendricks
- Helena Cousijn
- Maria Gould
- Alice Meadows
- Stephanie Harley
categories:
- PIDapalooza
- Persistence
- Identifiers
- Collaboration
- Community
- Meetings
date: 2019-08-18
draft: false
title: We'll be rocking your world again at PIDapalooza 2020
x-version: 0.0.0
---

The official countdown to PIDapalooza 2020 begins here! It's 163 days to go till our flame-lighting opening ceremony at the fabulous Belem Cultural Center in Lisbon, Portugal. Your friendly neighborhood PIDapalooza Planning Committee---Helena Cousijn (DataCite), Maria Gould (CDL), Stephanie Harley (ORCID), Alice Meadows (ORCID), and I---are already hard at work making sure it’s the best one so far!

<!--more-->

{{% divwrap align-right %}}<div style="width:195px; text-align:center;" ><iframe  src="https://www.eventbrite.com/countdown-widget?eid=60971406117" frameborder="0" height="212" width="195" marginheight="0" marginwidth="0" scrolling="no" allowtransparency="true"></iframe><div style="font-family:Helvetica, Arial; font-size:12px; padding:10px 0 5px; margin:2px; width:195px; text-align:center;" ></div></div>{{% /divwrap %}}
We have a shiny [new website](https://pidapalooza.org), with loads more information than before, including spotify playlists (please add your PID songs to [the 2020 one](https://open.spotify.com/playlist/1oJtbpTzF9I3MewQ1Yasml?si=D0TKdR8BTJSL-GA3X_LwVQ)!), an instagram photo gallery, and of course registration information. Look out for updates there and on [Twitter](https://twitter.com/pidapalooza).

And, led by Helena, the Program Committee is starting its search for sessions that meet PIDapalooza’s goals of being PID-focused, **fun**, informative, and interactive.  If you’ve a PID story to share, a PID practice to recommend, or a PID technology to launch, the Committee wants to hear from you. Please send them your ideas, using [this form](https://forms.gle/oeSeiZEni3cPipKm6), by September 27. We aim to finalize the program by late October/early November.

## Don’t forget to tie your proposal into one of the six festival themes:

#### Theme 1: Putting Principles into Practice
FAIR, Plan S, the 4 Cs; principles are everywhere. Do you have examples of how PIDs helped you put principles into practice? We’d love to hear your story!

#### Theme 2: PID Communities
We believe PIDs don’t work without community around them. We would like to hear from you about best practice among PID communities so we can learn from each other and spread the word even further!

#### Theme 3: PID Success Stories
We already know PIDs are great, but which strategies worked? Share your victories! Which strategies failed? Let’s turn these into success stories together!

#### Theme 4: Achieving Persistence through Sustainability
Persistence is a key part of PIDs, but there can’t be persistence without sustainability. Do you want to share how you sustain your PIDs or how PIDs help you with sustainability?

#### Theme 5: Bridging Worlds - Social and Technical
What would make heterogeneous PID systems 'interoperate' optimally? Would standardized metadata and APIs across PID types solve many of the problems, and if so, how would that be achieved? And what about the social aspects? How do we bridge the gaps between different stakeholder groups and communities?

#### Theme 6: PID Party!
You don’t just learn about PIDs through powerpoints. What about games? Interpretive dance? Get creative and let us know what kind of activity you’d like to organize at PIDapalooza this year!

## PIDapalooza: the essentials

**What?** [PIDapalooza 2020](https://pidapalooza.org) - the open festival of persistent identifiers    
**When?** 29-30 January 2020 (kickoff party the evening of January 28)    
**Where?**  Belem Cultural Center, Lisbon, Portugal ([map](https://goo.gl/maps/HEmmQUjkJcEoqFTZ7))    
**Why?**  To think, talk, live persistent identifiers for two whole days with your fellow PID people, experts, and newcomers alike!    

We hope you’re as excited about PIDapalooza 2020 as we are and we look forward to seeing you in Lisbon.