---
archives:
- 2019
author: Jennifer Lin
authors:
- Jennifer Lin
- Amanda Bartell
- Kathleen Luschek
categories:
- Similarity Check
- Member briefing
date: 2019-05-30
draft: false
title: Similarity Check is changing
x-version: 0.0.0
---

## Tl;dr

Crossref is taking over the service management of Similarity Check from Turnitin. That means we're your first port of call for questions and your agreement will be direct with us. This is a very good thing because we have agreed and will continue to agree the best possible set-up for our collective membership. Similarity Check participants need to take action to confirm the new terms with us as soon as possible and before 31st August 2019. Instructions will be circulated early June via email.

## Background

Many of our members use [Similarity Check](/services/similarity-check) which gives their editors reduced-rate access to Turnitin’s iThenticate system for plagiarism checking. Some use Similarity Check directly and some as part of a submission system.

The service launched in 2008 when we [announced our initial partnership with Turnitin](/news/2008-06-19-crosscheck-plagiarism-screening-service-launches-today). Since then it's gone from strength to strength and now has over 60 million full-text documents (from over 87 thousand titles) available for text comparison and almost 1500 members using the service.

The way that the Similarity Check arrangement works is changing, and it’s important that users know what’s happening. We have worked with Turnitin to set up a process that will transition participants easily and swiftly into the upgraded service with no access interruptions to iThenticate access.


## So, what is changing and why?

We know that Similarity Check is a critical service for our members, and we want to improve people's experience of using it. So, in consultation with members, we’ve strengthened the service by updating our relationship with Turnitin to consolidate all the components of the service under our care and stewardship. From next week, Similarity Check participants will move from having an agreement with Turnitin to one with Crossref. And at Crossref, we have a new agreement with Turnitin as the technology provider for the service.

The new arrangement puts us in a strong position to improve support and drive future improvements of the system. Representing our collective membership, we’ve agreed better terms than what people have today and what members would get acting individually.

There are five key changes specifically:

1. Members' Similarity Check service agreement will be with us and not Turnitin.
2. Per-document checking fees will be invoiced by us, and not Turnitin. They’ll be included in members' regular invoices, reducing international transfer fees for many.
3. The first 100 documents checked each year will be free of charge.
4. Turnitin will operate as a vendor for Crossref. We’ve already agreed a range of additions to their technology roadmap. Turnitin will remain responsible for fixing any bugs or technical issues with the system, but we're in a stronger position to ensure these are fixed quickly.
5. Users will get training and on-boarding support from Crossref. This will cover both how to use the interface and how to interpret the results.

## What’s staying the same?

*   **The system itself and how it's accessed** - people's logins will stay exactly the same and nothing will change about how participants have their systems set up.
*   **The [fees](/fees/#similarity-check-fees)** - the annual Similarity Check fee and the per-document checking fees will remain at the same level (although under the new arrangement users will get the first 100 documents each year for free - see "what's changing" above!)
*   **Your service obligations** - members still need to make at least 90% of all their journal article content available for Turnitin to index. This is achieved through the dedicated full-text URLs that members register in their metadata.
*   **Licensing and privacy** - there are no changes to the licensing of members' content or the privacy requirements for Turnitin’s use of member content.

## For existing users

We’ve worked closely with Turnitin to ensure an easy transition to the [new Crossref terms](/services/similarity-check/terms/). You can transition to the new terms at any stage from next week through to 31st August, and Turnitin will end your contract with them in the month you take that action.

Next week, we’ll email your main Crossref membership contact with a link to a form asking them to click-through accept the new terms. This will confirm and commence the transition process.

You’ll then need to:

*   Pay your final Turnitin invoice, which will be sent at the end of the month you complete the form. This will cover your per-document checking fees up to the 25th of that month.
*   Continue to use iThenticate as usual.

Your service agreement will officially move from the Turnitin agreement to the Crossref agreement on the 25th of the month that you complete the transition form. The next Similarity Check invoices you receive will be from Crossref in January 2020 and will include your Similarity Check annual fee and your per-document checking fees for the remainder of 2019.

If you haven’t transitioned to the new agreement by 31st August, you risk losing access to the iThenticate system as Turnitin will not be able to automatically renew your direct contract.

If you have any questions about these changes, do contact our [membership specialist](mailto:member@crossref.org). We’ll be in touch next week with a link to a new form where you’ll be able to check your details and accept the new agreement directly with us.

## For prospective users

When you apply to participate in Similarity Check you will be accepting terms directly with Crossref and not Turnitin. Eligible members can apply any time from next week.

## Any questions?

There are many benefits to this new set-up, but we understand these things can be a bit of a hassle. We've welcomed a new colleague (say hello to [Kathleen](/people/kathleen-luschek)!) to help people transition and get the best from their use of Similarity Check. Please [contact her via Support](mailto:support@crossref.org) with any questions.

> [Update June 5th: we've added a new [FAQ page](/faqs/similarity-check-transition/) for members who signed up for Similarity Check prior to June 2019]