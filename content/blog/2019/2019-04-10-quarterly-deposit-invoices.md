---
archives:
- 2019
author: Isaac Farley
authors:
- Isaac Farley
categories:
- Content Registration
- Invoices
- Membership
- Metadata
- Identifiers
date: 2019-04-10
draft: false
title: 'Quarterly deposit invoices: avoiding surprises'
x-version: 0.0.0
---

Whenever we send out our quarterly deposit invoices, we receive queries from members who have registered a lot of backlist content, but have been charged at the current year’s rate. As the invoices for the first quarter of 2019 have recently hit your inboxes, I thought I’d provide a timely reminder about this in case you spot this problem on your invoice.

<!--more-->
This problem is usually the result of metadata being registered that makes it look as though the content was current, despite the fact that it was backlist. This post will show you what to do if you spot this problem in your latest invoice - and more importantly, how you can avoid this situation in the future.

### About current and backlist Content Registration fees
There are different fees for registering content depending on whether it’s current (this year and the previous two years - 2017, ‘18, and ‘19) or backlist (older than that). As an example, it’s $1 each for a current journal article, and $0.15 for each backlist article. So, if you’ve incorrectly registered your content as published in 2019 when actually it was published in 2012, your quarterly invoice will overcharge you based on the metadata discrepancy.

<center><img src="/images/blog/quarterly_invoice_test.png" alt="Sample quarterly deposit invoice" width="550" class="img-responsive" /></center>

1. We send you the quarterly deposit invoice at the end of each quarter. This example is an invoice for all deposits of the first quarter of 2018 for username ‘test’ - months January, February, and March.
2. The BY code represents backlist (or, back year) content (journal article, in this example). Backlist content is charged at $0.15 per content item.
3. The CY code represents current year content (journal article, in this example, although you can see that this invoice has charges for other content items as well). Current year content is charged at $1 per content item.

### Determining whether content is current or backlist
A record is determined to be either a backlist or current year deposit based on the metadata that you deposit with us. If you use our helper tools - [Metadata Manager](https://www.crossref.org/metadatamanager/) or the [web deposit form](https://apps.crossref.org/webDeposit/) - the system looks at the information you’ve entered into the “publication date” field. If you deposit XML with us, it looks at the date in the  `<publication_date>` element. And we look at each individual item separately—so even if you’ve put a publication date at journal level, you still need to put it at the journal article level too.

Additionally, sometimes we find that deposits mistakenly include the deposit date in place of the publication date. These two dates - the deposit date and the publication date - are not necessarily one and the same, especially if you are depositing backlist content. Please take care to double check this before you submit your deposit(s).

### What to do if you think you’ve registered the wrong publication date
As you can only update a publication date by running a full redeposit, it’s important to get it right the first time. If you’ve registered the wrong publication date and have received an invoice for the wrong amount, please redeposit your content and then [get in contact with us](mailto:support@crossref.org). If you do this as soon as you spot the error, we’ll be able to send a new invoice for the correct amount.