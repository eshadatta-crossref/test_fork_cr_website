---
archives:
- 2019
author: Isaac Farley
authors:
- Isaac Farley
categories:
- Content Registration
- Title transfers
- Membership
- Metadata
- Identifiers
date: 2019-02-25
draft: false
title: Before, during, and after - a journey through title transfers
x-version: 0.0.0
---

In January, I wrote about how we’ve [simplified the journal title transfer process](/blog/resolutions-2019-journal-title-transfers-metadata-manager/) using our new Metadata Manager tool. For those disposing publishers looking for an easy, do-it-yourself option for transferring ownership of your journal, I suggest you review that blog post. But, whether you choose to process the transfer yourself via Metadata Manager or need some help from [Paul](/people/paul-davis/), [Shayn](/people/shayn-smulyan/), or myself, there’s more to a transfer than just the click of a transfer button or the submission of an email to [support@crossref.org](mailto:support@crossref.org ), as I’m sure those of you who have been through a title transfer can attest.

<!--more-->
### Prepping your title transfer
Sometimes members get on the other side of a [title transfer](/blog/what-can-often-change-but-always-stays-the-same/) and find you’re encountering problems even if you followed the process for transferring titles. You might find you can register new content for the new title against your own prefix without any issues. But you are not able to update the metadata for backfile content after we’ve made the transfer.

When we investigate, the problem is usually that the DOIs you’re trying to update don’t exist in our system yet. This means the deposit isn’t considered an update to the content, it’s considered a new deposit. And you don’t have permission to do that, since you’re effectively attempting to register new content to a prefix that is not your own.

This problem is because the former publisher didn’t ever register the DOIs with us - even though they’ve been displaying them on their website. This is bad practice and isn’t in keeping with our membership terms, but it does sometimes happen.

Before you request a title transfer, do check with the former publisher that they’ve definitely registered all the DOIs that they’ve been displaying and distributing to their readership. You can spot check this yourself by following a few of the DOI links and checking that they resolve to the right place. If you want a full list of DOIs registered to a journal title, our [depositor reports](https://www.crossref.org/06members/51depositor.html) are the place to start. Depositor Reports list all DOIs deposited for a title on a publisher-by-publisher basis. Or, alternatively, if you know the journal cite ID, the unique internal, Crossref identifier for the journal, you can bypass the publisher-by-publisher title list (in my example you’d need to replace my fictional 123456 journal ID with your journal’s cite ID):

<center>`http://data.crossref.org/depositorreport?pubid=J123456`</center>

### Top tips for a pain-free title transfer
If your organization has gained new titles, you’ve checked the depositor report for your new journal and are happy that all the existing DOIs have been registered, then you’re ready to process the transfer. Here are three key steps to ensure a pain-free transfer.

1. If you are not acquiring all existing journal articles as part of this transfer, you’ll need to contact us at [support@crossref.org](mailto:support@crossref.org ) to confirm the details. Once we have those details sorted, we'll transfer ownership for the select, specified articles.

2. Carefully check the existing metadata associated with your new titles - some metadata provided for [text and data mining](/education/retrieve-metadata/rest-api/text-and-data-mining) or [Similarity Check](/services/similarity-check/) are publisher-specific and must be updated or [removed](https://support.crossref.org/hc/en-us/articles/115003564483-Removing-metadata-from-a-record) when content is acquired by another member.

3. If the metadata supplied is fine, you just need to update the URLs to direct DOIs to your content. You can do this by sending us a [URL update file](https://support.crossref.org/hc/en-us/articles/213022526) or by [redepositing the metadata](https://support.crossref.org/hc/en-us/articles/213022486) with the correct URLs.

If you need to update more than the URLs, you should redeposit the metadata with the correct information plus the correct URLs.

* Note: If you, as the disposing publisher, are prepared to transfer your journal to an acquiring publisher, and would like to transfer ownership of the journal and all existing journal articles, please try your new [title transfer via Metadata Manager](/blog/resolutions-2019-journal-title-transfers-metadata-manager/).

### On the other side
If you follow the steps I’ve outlined above, you should get to the other side of your title transfer with few problems and are likely to encounter smooth metadata seas ahead. That said, some of our members follow these steps to a tee and still are faced with occasional transfer-related problems.

Perhaps the previous journal owner used a different scheme to assign timestamps and now you’re receiving [mysterious timestamp errors](https://support.crossref.org/hc/en-us/articles/215789303-Error-and-warning-messages-) when you deposit. Or, that same previous owner made a mistake with a previous deposit and accidentally submitted more than one journal title record. Or, you encounter a strange, new error in Metadata Manager when working with your new titles (yes, we’re still in beta!). If so, please reach out to us at [support@crossref.org](mailto:support@crossref.org ) and we’ll help solve what are surely confounding problems, since you’ve undoubtedly read this post in its entirety and taken heed of the above advice.

As always, if you have questions, need guidance as you’re working through this process, or have recommendations on how we can improve title transfers, please contact us at [support@crossref.org](mailto:support@crossref.org ).