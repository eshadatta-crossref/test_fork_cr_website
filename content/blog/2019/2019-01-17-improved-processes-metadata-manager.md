---
archives:
- 2019
author: Shayn Smulyan
authors:
- Shayn Smulyan
categories:
- Metadata Manager
- Content Registration
- Citations
- Identifiers
date: 2019-01-17
draft: false
title: Improved processes, and more via Metadata Manager
x-version: 0.0.0
---

Hi, Crossref blog-readers. I’m [Shayn](/people/shayn-smulyan/), from Crossref’s support team. I’ve been fielding member questions about how to effectively deposit metadata and register content (among other things) for the past three years. In this post, I’ll take you through some of the improvements that Metadata Manager provides to those who currently use the [Web Deposit form](https://apps.crossref.org/webDeposit/).

<!--more-->

[We recently announced the launch of Metadata Manager](/blog/metadata-manager-members-represent/), a new tool from Crossref that makes it easier for you to submit robust, accurate, and thorough metadata for the content you register. Metadata Manager already covers journals and articles; more content types will be supported soon. It offers some extra features that will make your experience less stressful, make your metadata better, and ultimately make your content more discoverable.

Metadata Manager has the potential to improve your metadata registration experience in a number of ways:

- by correcting one-off errors in previously registered metadata
- by directly allowing you to add references, license data, funder information, or any other ancillary metadata to items that have previously been registered
- by updating Crossmark data, in the case of a retraction or withdrawal

## Login first, not last

With the Web Deposit form, you finish entering your metadata for a new issue of your journal, and then get asked for your password, and of course that's when you realize you've forgotten it (it happens a lot!). With [Metadata Manager](https://www.crossref.org/metadatamanager/), the very first step is to log in, so you know your login credentials are accurate before you get down to the task of entering your metadata.

## Easily import journals, or add new ones

When you switch to Metadata Manager, you can import the journals already associated with your account. Simply go to the search bar on the Home screen, search for your journal by title, then click ’Add’. If you are registering your first article for a journal that you’ve not registered before, you can add the journal information on the Home screen, by clicking “New Publication”.

<center><img src="/images/blog/shayn-mm2.png" alt="metadata manager home screen" width="600" class="img-responsive" /></center>

## Adding a Journal DOI

In the Web Deposit form, the Journal DOI is optional, as long as you include a valid ISSN. However, with Metadata Manager, **a Journal DOI must be created for each journal you register**. So, you need to enter a Journal DOI and a Journal URL for each of your journals before your deposits can be submitted. The Journal DOI won’t become active until you submit your first successful deposit for an article within that journal.  

If you’ve never registered a Journal DOI before and are unsure what to use for your Journal DOI’s suffix, take a look at our suggested [best practice for constructing DOI suffixes](https://support.crossref.org/hc/en-us/articles/214669823-Constructing-your-identifiers).

## Adding new articles

Once your journal is added, the process of adding articles in Metadata Manager should be familiar, as it’s similar to the Web Deposit form process. You type in or paste as plain text (without formatting) all your relevant, accurate, and thorough metadata into the appropriate fields in the form.

## Save your work as you go

In Metadata Manager there is no need to complete a full issue’s worth of articles at once. And, you don’t need to worry about losing your progress if you accidentally close your browser window, or your laptop runs out of battery while you’re in the middle of a deposit. You can simply and easily ‘save-as-you-go’, one article at a time, until you’re ready to submit them all. You can even review your saved metadata to make sure there aren’t any errors before the deposit is finalized.

## Other metadata fields you didn’t know you needed (but you do!)

Have you ever wanted to add an abstract to your content’s metadata? How about license information, so that other organizations know what they can and can’t do with the work? Does your journal use article ID numbers instead of page numbers? These are all elements that can be added to Metadata Manager that were not available in the Web Deposit form. Additionally, you can add funding data, Similarity Check links, and [relationships between your articles and other content](https://support.crossref.org/hc/en-us/articles/214357426-Relationships-between-DOIs-and-other-objects). These types of metadata are hugely valuable for [building a robust, interconnected web of scholarly communication](/blog/the-research-nexus-better-research-through-better-metadata/).

## Adding references

Unlike the Web Deposit form, Metadata Manager allows you to easily add references to your article’s metadata—this is an important requirement for participating in our [Cited-by](/services/cited-by/) service.

To add references to an article’s metadata, you can copy and paste its reference list into the references field on the same screen as the rest of the article metadata (as per the image below).

<center><img src="/images/blog/shayn-mm1.png" alt="metadata manager home screen" width="600" class="img-responsive" /></center>

Metadata Manager will match DOIs to those references (where available), and include the full list in your record. So, if you’ve been putting off participating in Cited-by because the reference deposit requirement was too much of a hassle, we hope this will help ease the way! The more references everyone registers, the more robust our Cited-by counts and Cited-by data become.

## Edit mistakes without having to re-enter all your metadata

Mistakes happen. Sometimes you put an author’s first name in the last name field.  Sometimes you copy and paste some stray HTML tags into your abstract. You might break a link by leaving a space in the middle of a URL, or enter the first-page number as 3170 instead of 317.  

> With Metadata Manager you can fix any errors quickly and easily right in the interface, then just click to redeposit the article with its metadata corrected. You won’t need to re-enter all the metadata or worry about editing the XML files directly.

We’ll have another blog post coming soon that will be devoted entirely to updating, correcting, or otherwise editing metadata for already-registered DOIs in Metadata Manager.  

## Find out immediately if your registration was successful

When you have finished adding the metadata for your articles, navigate to the “To deposit” section and click ‘Deposit’ to submit them. Instead of having to wait for your content to go through our processing queue, you’ll get immediate feedback. The number of Accepted and Failed deposits show immediately. Any articles which have failed are clearly marked with a red triangle icon and an explanation for the error. If you don’t understand an error message or how to correct the metadata, please contact us at [support@crossref.org](mailto:support@crossref.org).

To get started with Metadata Manager take a look at our [full help documentation](/education/member-setup/metadata-manager/).


---