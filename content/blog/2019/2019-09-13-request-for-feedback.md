---
archives:
- 2019
author: Patricia Feeney
authors:
- Patricia Feeney
categories:
- Metadata
- Schema
- Content Registration
- Member Briefing
date: 2019-09-13
draft: false
title: 'Request for feedback: Conference ID implementation'
x-version: 0.0.0
---

We’ve all been subject to floods of conference invitations, it can be [difficult to sort the relevant from the not-relevant](/blog/taking-the-con-out-of-conferences/)  or (even worse) sketchy conferences competing for our attention.  In 2017, DataCite and Crossref started a [working group](/working-groups/conferences-projects/) to investigate creating identifiers for conferences and projects.  Identifiers describe and disambiguate, and applying identifiers to conference events will help build clear durable connections between scholarly events and scholarly literature.  

Chaired by Aliaksandr Birukou, the Executive Editor for Computer Science at Springer Nature, the group has met regularly over the past two years, collaborating to create use cases and define metadata to identify and describe conference series and events.  We first asked for input on metadata specifications in [April 2018](/blog/pids-for-conferences-your-comments-are-welcome/). Technical implementation kicked off in February with a workshop at CERN to discuss the mechanics of making PIDs for conferences a reality.

## We’ve reached another milestone and want your feedback
Crossref has supported a number of conference publication-related PIDs for years - members can currently register PIDs for conference series publications, conference proceedings, and of course individual conference papers - and that won’t change, but we will also be supporting DOI registration for conferences. A crucial step towards this is of course integrating the new identifier into our metadata input schema.  

## The details

We currently collect some limited metadata describing the conference itself such as theme, location, and dates as part of the conference series or proceeding metadata, but do not apply a DOI to that information.  The new Conference ID records will include expanded metadata as [defined by the working group](https://docs.google.com/document/d/1URIvkUpzcfjSd2YFIS-rdRIrOyrKSbFfhkdpGPRTAFI/edit). You'll be able to register a distinct metadata record for a single conference.  You'll also be able to register a record for a conference series, and connect Conference IDs to conference proceeding metadata records and DOIs.  

Changes to the conference-specific metadata are backwards compatible. Members will be able to register event metadata per usual, or can instead use the new event metadata to register an identifier for their conference event and/or series. This means a member can:

* Register conference, conference series, proceedings series, proceedings, and papers in one submission
* Register proceedings or proceedings series and papers without a Conference ID included
* Register Conference IDs only
* Update an existing conference record with a Conference PID

I’ve written up our proposal [in this google doc](https://docs.google.com/document/d/17hKUa2WHxeUpqEe9H0I022Ggod4ID5bmuDDNmvZQn58/edit#) and we want your feedback before we proceed with implementation.  Please comment directly in the Google doc, open a Gitlab issue, or [feedback@crossref.org](mailto:feedback@crossref.org).  We’ll keep the document open for comments until September 30.