---
archives:
- 2019
author: Ed Pentz
authors:
- Ed Pentz
- Lucy Ofiesh
categories:
- Community
- Operations
- Staff
date: 2019-12-09
draft: false
title: Introducing our new Director of Finance & Operations
x-version: 0.0.0
---

I'm happy to announce that Lucy Ofiesh has joined Crossref as our new Director of Finance and Operations. Lucy has experience supporting the sustainability and governance of not-for-profit organizations having held roles such as Executive Vice President of the Brooklyn Children's Museum and for the last few years as Chief Operating Officer at Center for Open Science, a Crossref member.

<!--more-->

At Center for Open Science, Lucy built her knowledge of the research communications community; she is knowledgeable about how diverse this community has become and the challenges of planning and scale that this comes with. She knows how to manage the complexities of an expanding global operation, where members, users––and staff––in several locations need fair, timely, and accurate information, whether it’s about how invoices relate to their use of our services or information about our approach to health benefits.

Finance underpins all that Crossref does and is crucial to long term sustainability while ‘Operations’ is a varied function and it is only becoming more so as Crossref grows. The role encompasses human resources, organization culture, governance (including serving as secretary of the organization), and working as part of the senior leadership team. Lucy will bring community focus to our operations, putting member experience first so that it becomes easier to work with us, from implementing systems and processes that work for multiple languages and currencies to providing personable billing support.

She will also play a vital role on the Crossref leadership team, working with me and the other directors Bryan, Ginny, and Geoffrey to hone the strategies, goals, and metrics that will allow us to track progress and meet our ambitious goals.

## A word from Lucy…

I am excited to be joining Crossref as its next Director of Finance and Operations. I previously worked for an organization that was a Crossref member and two qualities stood out to me: first, the focus with which Crossref has provided solutions to shared challenges across scholarly publishing; and second, the ways Crossref operates transparently and from a values-driven perspective.

My past experience has been in helping organizations run as effectively as they can, navigate change and growth, and build and support high functioning teams. Specifically, my work has focused on strategic and sustainability planning, financial forecasting, organizational governance, and staff management. My goal in finance and operations is to ensure that the working experience at Crossref––both for external partners and members and internal staff––is as frictionless as possible so we can have the greatest impact on our community.

I am only the second person to step into this role. Lisa Hart Martin has led finance and operations for the first twenty years of Crossref's existence. I am fortunate to be overlapping with her for a couple of weeks and grateful for the trust of the Crossref team to help guide us into our third decade. I really want to hear from our members so please [reach out to me with your thoughts on Crossref's finance and operations](mailto:lofiesh@crossref.org).

_Please join us in welcoming Lucy to the Crossref community!_