---
archives:
- 2019
author: Ed Pentz
authors:
- Ed Pentz
- Bryan Vickery
categories:
- Community
- Product
- Staff
date: 2019-08-19
draft: false
title: Introducing our new Director of Product
x-version: 0.0.0
---

I'm happy to announce that [Bryan Vickery](/people/bryan-vickery) has joined Crossref today as our new Director of Product. Bryan has extensive experience developing products and services at publishers such as Taylor & Francis, where he led the creation of the open-access platform Cogent OA. Most recently he was Managing Director of Research Services at T&F, including Wizdom.ai after it was acquired.
<!--more-->

He previously held a range of roles from Publisher to Chief Operations Officer at BioMedCentral, as well as online community and technology leadership roles at Elsevier.

Bryan is a great addition to Crossref and we are lucky to have him. The product team is keen to progress the long list of wishes from our community with his guidance. Bryan will bring focus and clarity to our roadmap and our development processes, making it easier for people to adopt and participate in our services, and ensuring that we are working on the issues that are most important to our members.

He will also be a vital part of the leadership team, working with me and the other directors Geoffrey, Ginny, and Lisa to help us take the organization forward in a transparent way that serves our mission and empowers our excellent staff.

## And now a few words from Bryan…

I’m thrilled to be joining Crossref as Director of Product at a time of considerable change in scholarly communication. I’ve worked in, and around, scholarly publishing for more than 20 years.

This is a challenging role. We have many exciting services and collaborations to progress, and also technical debt to address (like everyone else) to upgrade our existing services - it’s essential we balance these. My priority is to stay on top of the issues of the highest value to the scholarly community, now and in the future, and ensure we deliver services that are both useful and usable.

I will be attending Crossref LIVE19 “The strategy one” along with other staff and look forward to meeting many of our members then. In the meantime, I'd love to hear your thoughts on where we’ve been (what it’s like working with us and using our services) and where we're going (what you’d like to see from us). You can [reach me via our feedback email](mailto:feedback@crossref.org).

_Please join us in welcoming Bryan to the Crossref community._