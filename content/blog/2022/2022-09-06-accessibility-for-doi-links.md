---
archives:
- 2022
author: Jennifer Kemp
authors:
- Jennifer Kemp
categories:
- DOIs
- Linking
- Interoperability
- Accessibility
- DOI Display Guidelines
date: 2022-09-06
draft: false
title: 'Accessibility for Crossref DOI Links: Call for comments on proposed new guidelines'
x-version: 0.0.0
---

Our entire community -- members, metadata users, service providers, community organizations and researchers -- [create](https://www.crossref.org/documentation/member-setup/constructing-your-dois/) and/or [use](https://www.crossref.org/services/metadata-retrieval/) DOIs in some way so making them more accessible is a worthy and overdue effort.  

For the first time in five years and only the second time ever, we are recommending some changes to our [DOI display guidelines](https://www.crossref.org/display-guidelines/) (the changes aren’t really for display but more on that below). We don’t take such changes lightly, because we know it means updating established workflows. We appreciate the questions that prompted us to make this recommendation and we know it’s critical that we get community input on the proposed updates.


## TL;DR

Here is a quick overview:

* DOIs and URLs themselves don’t really tell readers much. People with visual impairments rely on screen readers to read out loud the contents of a page. We’re asking for the title of each DOI to be added, in an [ARIA](https://www.w3.org/WAI/standards-guidelines/aria/) (Accessible Rich Internet Applications) attribute, so these users understand what these links are for.
* Accessible text, as this kind of description is known, should be included for all links, but at this time, we’re specifically recommending it for landing pages of newly registered records.
* It’s not required, yet. We’re proposing a 2 year recommendation period and we want your feedback on the particulars, including timing and how we can help. Please take a [short survey](https://forms.gle/K6zWQ3f1dmYUkj9T6) and/or [get in touch](mailto:feedback@crossref.org) and share your thoughts.
* We’ll finalize these recommendations after assessing the feedback. Please check back for updates.



## What is changing, when and why

The proposed updates are meant to improve overall usability, particularly for people with visual impairments, by aligning our guidelines with modern accessibility requirements such as the new [W3C recommendations](https://www.w3.org/2021/09/UX-Guide-metadata-1.0/principles/) and the [European Accessibility Act](https://inclusivepublishing.org/blog/what-does-the-european-accessibility-act-mean-for-global-publishing/). This means that assistive technologies such as screen readers can interpret DOI links.

**Why are changes being recommended?**  
  
DOIs are [unique](https://www.crossref.org/documentation/member-setup/constructing-your-dois/#whyopaque) and persistent links to items in the scholarly record so it makes sense that they link to the full URLs for the associated content –for example, a journal article. The issue for people who rely on screen readers is that a DOI link doesn’t provide title or other information to give that link context. Users of screen readers need to know what the destination of a link is.

These users often lack the context that other users have; in fact, they may be presented with links in a document as a list. That's why all links, not just DOI links, need what is called "accessible text.” Providing additional information for links requires [ARIA](https://www.w3.org/WAI/standards-guidelines/aria/) (Accessible Rich Internet Applications) techniques. This speaks to the Web Content Accessibility Guidelines (WCAG), the standard guidelines for accessibility across the web, specifically [success criterion 2.4.4](https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context) - Link Purpose (In Context), which aims to ‘help users understand the purpose of each link so they can decide whether they want to follow the link.’

**For your feedback: recommended draft changes**  

We recommend the addition of an _aria-label_ attribute for DOI links, containing as its value the descriptive title of the content represented by the DOI, so that screen readers can interpret DOI links. This means that, _while the DOI display itself doesn’t actually change_, the link is enhanced with additional, contextual information for the user of assistive technology, in one of two ways, either:  

* **an aria-label attribute**, described as ‘a way to place a descriptive text label on an object,’ identifying the destination, or
* **an aria-describedby attribute** pointing to where the destination is identified in the surrounding text.

The updated HTML for a journal article*, for example, would be:

```<a href="https://doi.org/10.5555/12345678" aria-label="DOI for Toward a Unified Theory of High-Energy Metaphysics: Silly String Theory">https://doi.org/10.5555/12345678</a>```

Here the aria-label has been set to the value of the ‘title’ property as retrieved from the Crossref REST API at https://api.crossref.org/v1/works/10.5555/12345678.

*Note that fields may vary slightly for different [content types](https://www.crossref.org/documentation/research-nexus/).

This proposed solution allows screen readers to read aloud to users the value of the aria-label attribute, instead of the full DOI in the link text.

_At this time, we are recommending the change for landing pages in particular_, but it can and should be applied to wherever DOI links appear, whenever feasible (more on this below).

Our guidelines will continue to state that the DOI should always be displayed as a full URL link--that will not change. Neither will [content registration](/services/content-registration/)--we are not asking for additional information in your deposits.

**It’s not perfect, but it’s very worthwhile**  

This recommendation has some limitations worth noting but it must be said that there is no perfect solution.

DOI links appear in lots of places - PDFs for one notable example. We reviewed and tested the recommendation with Bill Kasdorf, Principal, Kasdorf & Associates, LLC, Richard Orme, CEO, DAISY Consortium, and George Kerscher, Chief Innovations Officer, DAISY Consortium-Senior Officer, Global Literacy, Benetech, who graciously provided their time and expertise. EPUBs and websites proved to be easy to update; other formats, notably PDFs, less so. Widespread adoption of accessible DOIs is so important and we don’t want confusion or frustration to get in the way of making progress. We support and welcome efforts to include an ARIA attribute wherever DOI links appear, but we recommend focusing on landing pages, for now.

Patrick Vale, Crossref Senior Front End Developer, explains that:
>”DOI links serve a very specific purpose: to provide the persistent link to an item in the scholarly record. And as such, they present an unusual set of requirements when balancing accurately presenting the information they encode - the persistent link - and making that link accessible, and understandable. With these proposed changes, we hope to strike this balance.“

We know it will be a challenge (more on that below) but we think it’s absolutely a worthwhile effort. Indeed, we are undertaking a project to update our own website to meet these recommendations and to review overall accessibility.

As Bill Kasdorf notes:
>“Most people have no idea how many people with visual impairments there are. Not only is it unfair to those people not to provide accessible text for links, the authors and publishers of the linked resource are missing a lot of readers. This update is a great move by Crossref, and every bit aligned with its mission to make scholarly content discoverable and consumable.”

**We propose the following timeline, also for your feedback**  

Once finalized, following community feedback, the updated guidelines will be issued as a recommendation for a suggested period of two years starting next year, 2023. Beginning in 2025, the changes will be required for landing pages of newly registered content (and strongly recommended for existing registered content). Feedback on this approach and timeline is also encouraged.

## Help us help you

We are conscious that adding descriptive information to DOI links places a significant responsibility on the members and Service Providers creating and hosting these links. Therefore, we are also considering the creation of a tool to help with implementation. Initial discussions suggest this could be a JavaScript helper tool, which could be included on member websites. We also welcome feedback as to how such a tool might be implemented, and how it would best integrate with existing sites and workflows.

## Call for comments - by 1st November  

We hope that this proposal is a welcome one and that the timing is good for moving forward together toward greater accessibility of the scholarly record.
**We welcome questions, feedback and suggestions through 1st November via the [survey](https://forms.gle/7diHy46Cu5J52q417) below or by email to feedback@crossref.org**

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScqLWIycofCUbGXxZRcOjkDM43zsIsfLdO2ZqhVVHiwDQUSeQ/viewform" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0" >Loading...</iframe>

## Small changes, big impact 

We’re excited to make changes that improve accessibility and we look forward to the community’s response to our proposal. We will share aggregated feedback in an updated post later this year.

## A note on language

Multiple sources were consulted to find the most appropriate and inclusive term(s) for users of screen readers in this context. “Print disabled,” for example, seemed to be a good candidate but was ultimately deemed likely to be confusing to a very global publishing audience, who often don’t physically print anything. Sources differ slightly, for example between the US and UK and of course, this English text may well be translated into other languages. Feedback on the terms used here is also very welcome.

## Additional resources
* [The Inclusive Publishing Hub](https://inclusivepublishing.org/about-the-inclusive-publishing-hub/) (DAISY Consortium)
* [National Center on Disability and Journalism](https://ncdj.org/style-guide/) (Arizona State University, US)
* [Inclusive Language guidance](https://www.gov.uk/government/publications/inclusive-communication/inclusive-language-words-to-use-and-avoid-when-writing-about-disability) (UK government)
* [The American Psychological Association (APA) Bias-Free Language Disability Guide](https://apastyle.apa.org/style-grammar-guidelines/bias-free-language/disability)
* [The Open Access Books Network](https://hcommons.org/groups/open-access-books-network/forum/topic/accessibility-of-oa-books/?view=all#post-57431) (OABN)