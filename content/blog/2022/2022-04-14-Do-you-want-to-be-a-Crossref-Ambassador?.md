---
archives:
- 2022
author: Vanessa Fairhurst
authors:
- Vanessa Fairhurst
categories:
- Education
- Ambassadors
- Community
- Collaboration
date: 2022-04-14
draft: false
title: Do you want to be a Crossref Ambassador?
x-version: 0.0.0
---

## A re-cap


We kicked off our Ambassador Program in 2018 after consultation with our members, who told us they wanted greater support and representation in their local regions, time zones, and languages.

We also recognized that our membership has grown and changed dramatically over recent years and that it is likely to continue to do so. We now have over 16,000 members across 140 countries. As we work to understand what’s to come and ensure that we are meeting the needs of such an expansive community, having trusted local contacts we can work closely with is key to ensuring we are more proactive in engaging with new audiences and supporting existing members.

We know that Crossref still remains inaccessible to many around the world, and in line with our strategic goal to engage communities, we want to lower the barriers to participation. Our Ambassadors are essential to us achieving this goal as we look to develop additional content in languages other than English, identify organizations to work closer with to support local research ecosystems, provide more in-person and online events in local time zones and languages, and do more in terms of open support via our community forum.

{{% imagewrap center %}}
{{< figure src="/images/blog/2022/Crossref_AmbsdrsLogo_RGB.png" alt="Ambassadors program logo" width="350" align="right">}}
{{% /imagewrap %}}

## What are our ambassadors up to now?

We currently have a [team](https://www.crossref.org/community/our-ambassadors/) of 30 ambassadors, spanning Indonesia, Turkey, Ukraine, India, Bangladesh, Colombia, Mexico, Tanzania, Cameroon, Nigeria, Russia, Brazil, USA, UAE, Australia, China, Malaysia, Mongolia, Singapore, and Taiwan. The program is reviewed annually, welcoming new faces and sometimes sadly saying goodbye to others. This enables us to continue improving how we work together and ensures the Ambassador team remains a diverse group of committed individuals that have the time and support from Crossref to fully participate in the program.

Over the last 3 years, we’ve had some great successes alongside a few challenges, not least of which has been working across 15 countries during a pandemic. We have all experienced the additional personal and professional strain that COVID-19 brought along, including shifts in the way we work and anxieties in the way we go about our lives. Of course, it has also meant that all our interactions have been restricted to Zoom, which has many benefits but doesn’t compare to face-to-face interactions when it comes to building strong working relationships, particularly across language and cultural barriers.

Despite this, our ambassador team helped us run 15 multi-lingual webinars last year, including Content Registration in Arabic, Getting Started with Books in Brazilian Portuguese, and an Introduction to Crossref in Chinese. They also helped us translate various materials and content into other languages, provided feedback on our new developments, took part in beta-testing, provided support to members on our community forum, and participated in calls to contribute to the program's future.

{{% quotecite %}}I love helping people get to know Crossref's products and services.{{% /quotecite %}}

{{% quotecite %}}I was proud to work as Ambassador and give an online Chinese webinar to introduce Crossref and the services in Oct. 2021.{{% /quotecite %}}

{{% quotecite %}}I am glad to be of help to Spanish speakers who are not able to grasp all the Crossref information correctly because of a language barrier or because they don't have the time to read and explore all the information available.{{% /quotecite %}}

{{% quotecite %}}Muy contento de poder formar parte como Embajador y con ello poder promover el uso y aprovechamiento de los productos de Crossref.{{% /quotecite %}}

{{% quotecite %}}I feel so blessed meeting with many diverse friends in Crossref ranging from Europe to Asia continents.{{% /quotecite %}}

{{% quotecite %}}Feeling happy by giving back knowledge to my regional community.{{% /quotecite %}}

## The future is ours to co-create

As countries are slowly dropping restrictions and we are taking our first cautious steps into a potential ‘post-pandemic’ world, our Community Engagement and Communication team has been looking at what this means for our activities in 2022 and beyond.

A big part of this is identifying local communities and groups to engage with to learn what challenges our members are facing, what barriers to participation in Crossref still exist, and how we can overcome these together. This practice is also fundamental to our vision of the Research Nexus––a rich and reusable open network of relationships connecting research organizations, people, things, and actions––which can only become a reality if everyone can fully contribute to the scholarly record.

As such, we would like to expand our Ambassador Program and particularly encourage applications from those based in the following countries:  

{{% row %}}
{{% column %}}
Argentina  
Chile  
Canada  
Croatia  
El Salvador  
Germany  
Ghana   
{{% /column %}}
{{% column %}}
Iraq   
Kenya  
Nicaragua  
Nigeria  
Peru  
Poland  
Vietnam  
{{% /column %}}   
{{% /row %}}

By being one of our ambassadors, you will become a key part of the Crossref community; our first port of call for updates or to test out new products or services, become well connected to our wide network of members, and work closely with us to make scholarly communications better for all.

If you are interested in participating, please read more on our  [Ambassadors page](/community/ambassadors/). You can submit an [application](/community/ambassadors/#apply-to-become-an-ambassador) letting us know why you are interested, how you work with Crossref currently, and a bit more about yourself. We will then follow up with you to discuss your ideas and the program in more detail.

The Ambassador Program is quite flexible, so you can choose how and when you contribute based on your comfort levels and other commitments. However, it does come with some minimum requirements of attending two team calls a year, being responsive and letting us know if anything is preventing you from participating, and completing our annual feedback survey so we can continue to improve the program going forward.
A good level of English and a firm understanding of our services and systems at Crossref is also a must to participate fully in the program and provide support to others in your local community. If you have just joined Crossref or want to learn more about how to work with us, then the Ambassador program may be too much for you right now, but our documentation has lots of helpful information and step-by-step guides, and you could also look at attending one of our events or joining our community forum.

If you have any questions, you can always contact us at feedback@crossref.org. We look forward to hearing from you!