---
archives:
- 2022
author: Ed Pentz
authors:
- Ed Pentz
- Rachael Lammey
categories:
- Product
- Staff
date: 2022-05-12
draft: false
title: 'Announcing our new Director of Product: Rachael Lammey'
x-version: 0.0.0
---

Unfortunately, Bryan Vickery has moved onto pastures new. I would like to thank him for his many contributions at Crossref and we all wish him well.

I’m now pleased to announce that [Rachael Lammey](/people/rachael-lammey) will be Crossref’s new Director of Product starting on Monday, May 16th.

Rachael’s skills and experience are perfectly suited for this role. She has been at Crossref since 2012 and has deep knowledge and experience of all things Crossref: our mission; our members; our culture; and our services.

In all her roles at Crossref Rachael has demonstrated how community-focused product development can be done.

Starting as a Product Manager for Similarity Check and Crossmark, she then led community discussions on text and data mining and taxonomies, introduced our support of preprints, and led the very successful ORCID Auto-update integration. She initiated our important partnership with the Public Knowledge Project including scoping and overseeing the joint plugin development work over the years. She helped to grow the Sponsors program, establish the LIVE informational events, oversaw the founding of our ambassador program, engaged more research funders and institutions, and became a go-to person for data citation expertise in our community.

In her brief time in [our Research & Development team](/blog/time-to-put-the-r-back-in-rd/), she helped to kick off that group’s reinvigoration and has engaged with numerous new community and technical initiatives. Such relationships—together with her knowledge of our systems and API—have enabled her to be a key driver in the development and adoption of ROR and grants - two of the highest strategic priorities of recent years.

Rachael says:

> "Alignment in planning and focusing on delivering outcomes will be my initial priorities. I'm conscious that we have a lot in play and I want to support the product team in their existing and ambitious goals while working with the leadership team and our very diverse community to focus and prioritise our development roadmap. I'm really grateful for this opportunity and I am looking forward to working with our members, users, and other open infrastructure organisations in this new capacity".

Our staff and the board are very enthusiastic about Rachael's appointment and we know our community will be too. Please join us in congratulating Rachael!