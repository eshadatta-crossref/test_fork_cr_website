+++
title = "Standards"
date = "2022-07-11"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "standards",]
identifier = "documentation/principles-practices/standards"
rank = 4
weight = 10116
aliases = []
x-version = "0.0.0"

+++

Our standards model allows you to register records for dated and undated standards, as well as families and sets of standards.

When registering a standard, DO:
* include all [designators](/documentation/content-registration/content-type-markup-guide/standards/#00098) associated with the standard
* include (and keep up to date) the current publication status using the `publication_status` attribute

Do not:
* update an existing DOI record with a new primary as-published designator - SDOs may opt to register DOIs for an undated standard, or to register DOIs for each version or update (as-published) of a standard.

Review our [Standards Markup Guide](/documentation/schema-library/markup-guide-record-types/standards/) for XML and metadata help.