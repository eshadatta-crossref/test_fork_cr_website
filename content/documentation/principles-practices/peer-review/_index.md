+++
title = "Peer review"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "peer-review",]
identifier = "documentation/principles-practices/peer-review"
rank = 4
weight = 10111
aliases = []
x-version = "0.0.0"

+++

You can register [peer review](/documentation/content-registration/content-types-intro/peer-reviews/) metadata and connect reviews to reviewed items via relationships.

When registering peer reviews, DO:

* include a review title - 	if you don’t have a review-specific title convention, we recommend that you include "Review" (or your own term for review) as well as a revision and review number. For example, a review pattern of Review: title of article (Revision number/Review number) will be: **Review: Analysis of the effects of bad metadata on discoverability (R2/RC3)**
* include reviewer information with the [contributor](/documentation/principles-practices/best-practices/#contributors) section, including information about anonymous reviewers
* include relevant stage, type, and recommendation metadata
* include [license](/documentation/principles-practices/best-practices/license/) information
* include relationship metadata linking the review with the item being reviewed (relation type `isReviewOf`).

Do not:
* include metadata specific to the reviewed item, like author and title - that is captured in the record of the reviewed item, which you supply via the `isReviewof` relationship

Other things to know:
* references are not currently supported for peer review records but if someone cites your reviews, those citations will be included in our [cited-by service](/services/cited-by/)

Review our [Peer Review Markup Guide](/documentation/schema-library/markup-guide-record-types/peer-reviews/) for XML and metadata help.