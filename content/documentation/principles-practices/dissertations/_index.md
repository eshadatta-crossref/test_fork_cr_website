+++
title = "Dissertations"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "dissertations",]
identifier = "documentation/principles-practices/dissertations"
rank = 4
weight = 10105
aliases = []
x-version = "0.0.0"

+++

Dissertation records capture information about dissertations or theses.

When registering dissertations, DO:

* include all relevant [funding](/documentation/principles-practices/best-practices/funding/), [license/schema-library//documentation/principles-practices/best-practices/license/), and [relationship/schema-library//documentation/principles-practices/best-practices/relationship/) metadata
* include [contributors](/documentation/principles-practices/best-practices#contributors) metadata, including ORCID iDs and affiliation metadata
* include an [abstract]((/documentation/principles-practices/best-practices#abstracts)
* include identifiers associated with the dissertation - if a DAI has been assigned, it should be deposited in the `identifier` element with the `id_type` attribute set to `"dai"`. If an institution has its own numbering system, it should be deposited in `item_number`, and the `item_number_type` should be set to `"institution"`


Do not:
* use the dissertation model for versions of a dissertations published in a book or journal

Review our [Dissertation Markup Guide](/documentation/schema-library/markup-guide-record-types/dissertations/) for XML and metadata help.