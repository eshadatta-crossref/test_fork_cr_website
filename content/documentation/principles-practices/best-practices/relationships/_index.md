+++
title = "Relationship metadata"
date = "2022-06-01"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "best-practices", "relationships",]
identifier = "documentation/principles-practices/best-practices/relationships"
rank = 4
weight = 10114
aliases = []
x-version = "0.0.0"

+++

Creating relationships helps build a map of scholarly research objects that we call [the research nexus](/documentation/research-nexus). Expressing these relationships in the metadata enables the evolving infrastructure to build on this mapping.

These connections may consist of citations, or refer to publications which do not always exist as a single content item (its parts may be produced, curated, and published by different organizations and separate activities). Making these connections creates *linked data*, which is useful because it establishes associations and context. 

Connections between research objects can be [established through your reference lists](/documentation/reference-linking/), or by asserting a *relationships* type.

We have also introduced other interlinking services that address specific types of relationships:

* [Components](/documentation/content-registration/structural-metadata/components/) allow for the assignment of DOIs to the component parts of a publication (figures, tables, images) which may lead to their reuse.
* [Crossmark](/documentation/crossmark/) supports the connection of *updates* which have a material effect on the original work, for example: updates, corrections, and retractions.
* [Funding data](/documentation/funder-registry/) supports identifying the organization that financially supports the research behind a specific publication.
* [Peer reviews](/documentation/content-registration/content-types-intro/peer-reviews/) support the host of outputs made publicly available about published scholarly content, for example: referee reports, decision letters, and author responses.

These and other services create relationships between metadata records; however, they share two characteristics that restrict their ability to define relationships:

1. Both items involved in a relationship must be identified by Crossref DOIs.
2. The types of relationships are dictated by the mission of the specific service.

The following modifications and new services developed in response to these two limitations:

1. Allow non-Crossref DOIs to be deposited in an item's (article/chapter/paper) list of citations.
2. Support the creation of general typed relationships between items with a Crossref DOI, and other content items with a variety of identifiers.

Review our [Relationships Markup Guide](https://www.crossref.org/documentation/schema-library/markup-guide-metadata-segments/relationships/) for XML and metadata help.