+++
title = "Abstracts"
date = "2022-05-31"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "best-practices", "abstracts",]
identifier = "documentation/principles-practices/best-practices/abstracts"
rank = 4
weight = 10102
aliases = []
x-version = "0.0.0"

+++

You can include JATS-formatted abstracts with your metadata records. All abstracts registered with us will be included in the metadata distributed through our metadata outputs.

Do:

* supply abstracts for journal articles and beyond - most metadata models support abstracts
* supply multiple abstracts where applicable:  if your content has multiple abstracts (different languages, or a simple and complex abstract) supply all of them as separate abstracts
* Use the language tag to identify the language used in each abstract

Do not:
* include multiple abstracts in a single abstracts tag

See our [Abstracts Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/abstracts/) for XML and metadata help.