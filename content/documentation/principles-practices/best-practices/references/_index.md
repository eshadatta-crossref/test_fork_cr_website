+++
title = "References"
date = "2022-06-01"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "principles-practices", "best-practices", "references",]
identifier = "documentation/principles-practices/best-practices/references"
rank = 4
weight = 10113
aliases = []
x-version = "0.0.0"

+++

Registering references means submitting reference lists as part of your metadata deposit. It is optional, but strongly encouraged. Registering references is a prerequisite for participating in [Cited-by](/documentation/cited-by/) and any references you include in your Crossref metadata will be made available through our APIs, including [Event Data](/blog/event-data-now-with-added-references/).

Do:
* **supply DOIs with your references!** This is the most effective way to identify and connect the references supplied with your content metadata.
* supply a full reference or complete marked up metadata for every citation that you do not have a DOI for - we do our best to match these citations to registered DOIs.
* cite data, software, and other materials used to support and supplement the content being registered

Do not:
* omit non-Crossref DOIs from your references - this eliminates data, software, and other types of citations from our records. We aren't able to match citations to non-Crossref DOIs, but we do pass them along to our APIs and they are used by our metadata subscribers.

Review our [References Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/references/) for XML and metadata help.