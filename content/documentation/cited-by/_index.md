+++
title = "Cited-by"
date = "2021-02-09"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "other-services", "cited-by",]
aliases = [ "/02publishers/forward_linking_howto.html", "/education/cited-by", "/education/cited-by/",]
x-version = "0.0.0"

[menu.main]
parent = "Documentation"
identifier = "documentation/cited-by"
weight = 300000

[menu.documentation]
identifier = "documentation/cited-by"
parent = "Documentation"
rank = 4
weight = 300000

+++

{{< snippet "/_snippet-sources/cited-by.md" >}}