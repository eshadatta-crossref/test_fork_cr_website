+++
title = "Exclusions"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "ithenticatev2-MTS-account-setup", "exclusions",]
identifier = "documentation/similarity-check/ithenticatev2-mts-account-setup/exclusions"
rank = 4
weight = 100279
x-version = "0.0.0"

+++

This section of our documentation is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS). This section is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS)

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/settings).
* Using iThenticate v2 through a browser instead? Go to [setting up iThenticate v2 for browser users](/documentation/similarity-check/ithenticatev2-account-setup/exclusions).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

## Exclusions

If you are integrating iThenticate v2 with your MTS, there are some exclusions that you need to set directly in the iThenticate account in the browser before you configure your settings in your MTS integration.

Within iThenticate, you need to click on *Settings* in the left hand menu in iThenticate v2 homepage and go to the *Web and API* tab. Here are the various items you can exclude.

### Preprint Label and Exclusions

iThenticate v2 can automatically identify preprint sources within your Similarity Report. This will allow you to easily identify preprints so your editors can make a quick decision as to whether to investigate this source further or exclude it from the report.

In order to start using this feature you will need to configure it within the iThenticate settings by logging directly into your iThenticate account. You can find instructions on how to configure this feature in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/administrator/settings/preprints.htm).  

You also have the option to automatically exclude all preprint sources from reports. All excluded preprints will still be available within the Similarity Exclusions panel of your Similarity Report and can be reincluded in the report.

Once this is done, you also need to edit your integration configuration within your Manuscript Submission System to exclude preprints.

Further details of how preprints appear within the Similarity Report can be found in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/preprints.htm) .

Here’s [more information about things to consider](/documentation/similarity-check/similarity-report-understand/#00664) when you find a match to a preprint in your Similarity Report.

### Website Exclusions

The Website Exclusions setting allows to automatically exclude all matches to specific websites. Instructions on how to turn on and configure this feature can be found in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/administrator/settings/website-exclusions.htm).

This feature will only exclude matches in the Internet repository. If a journal website is added to the list of excluded websites then all pages which have been crawled and indexed into Turnitin’s Internet repository will be excluded. However, journal articles from that journal which appear in the Crossref repository will not be excluded.

This feature will apply to all submissions made to the iThenticate account; including all web submissions and submissions made through any integration.

All excluded matches will still be available within the Similarity Exclusions panel of your Similarity Report and can be reincluded in the report. Further details of how these exclusions will appear can be found in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/website-exclusions.htm).

### Customized Exclusions

A feature in iThenticate v2 is Customized Exclusions. The Customized Exclusions setting allows administrators to create sections of text that can be excluded from the Similarity Report. Administrators can tailor these keywords and phrases to best meet the needs of their organization (for example, ‘Further Acknowledgments’).

Start by configuring this feature directly in your iThenticate account in the browser . More details can be found in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/administrator/settings/customized-exclusions.htm). Once this is done, you then need to edit the integration configuration in your MTS to exclude customized sections.