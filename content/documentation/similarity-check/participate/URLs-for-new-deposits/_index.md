+++
title = "Adding full-text URLs to new deposits"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "participate", "URLs-for-new-deposits",]
identifier = "documentation/similarity-check/participate/URLs-for-new-deposits"
rank = 4
weight = 100102
aliases = [ "/education/similarity-check/participate/urls-for-new-deposits", "/education/similarity-check/participate/urls-for-new-deposits/", "/education/similarity-check/depositing-full-text-urls-for-similarity-check-for-new-deposits/", "/education/similarity-check/depositing-full-text-urls-for-similarity-check-for-new-deposits",]
x-version = "0.0.0"

+++

If you’re planning to participate in Similarity Check in the future, it’s best to include Similarity Check URLs in your deposits from the start. You can deposit Similarity Check URLs using our helper tools, or by including them in your XML deposits.

### Helper tools<a id='00314' href='#00314'><i class='fas fa-link'></i></a>

Our helper tools all contain a specific field where you can add your full-text URL specifically for Similarity Check:

* [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/): OJS automatically includes the Similarity Check URL as part of your deposit.
* [Web deposit form](/documentation/content-registration/web-deposit-form): select *Add Similarity Check URL*
* Still using the deprecated [Metadata Manager](/documentation/member-setup/metadata-manager)? Add under *Additional information*

### Direct XML deposit<a id='00315' href='#00315'><i class='fas fa-link'></i></a>

The full-text URL can be included as part of your standard metadata. For Similarity Check, the full-text URL needs to be deposited within the *crawler-based* `collection property`, with `item crawler` *iParadigms*. Here's an example:

```
<doi_data>
 <doi>10.5555/sampledoi</doi>
 <resource>http://www.yoururl.org/article1_.html</resource>
 <collection property="crawler-based">
  <item crawler="iParadigms">
   <resource>http://www.yoururl.org/article1_.html</resource>
  </item>
 </collection>
</doi_data>
```

Use our widget to check the [percentage of Similarity Check URLs included in your metadata](/documentation/similarity-check/participate/eligibility/).