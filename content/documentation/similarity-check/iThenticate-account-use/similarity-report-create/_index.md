+++
title = "Creating and finding your Similarity Report"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticate-account-use", "similarity-report-create",]
identifier = "documentation/similarity-check/ithenticate-account-use/similarity-report-create"
rank = 4
weight = 100304
aliases = [ "/education/similarity-check/ithenticate-account-use/similarity-report-create", "/education/similarity-check/ithenticate-account-use/similarity-report-create/",]
x-version = "0.0.0"

+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Creating and finding your Similarity Report](https://help.turnitin.com/crossref-similarity-check/user.htm#TheSimilarityReport)**

**v1 Creating and finding your Similarity Report, keep reading:**


For each document you submit for checking, the Similarity Report provides an overall similarity breakdown. This is displayed in the form of percentage of similarity between the document and existing published content in the iThenticate database. iThenticate’s repositories include the published content provided by Crossref members, plus billions of web pages (both current and archived content), work that has previously been submitted to Turnitin, and a collection of works including thousands of periodicals, journals, publications.

Matches are highlighted, and the best matches are listed in the report sidebar. Other matches are called underlying sources, and these are listed in the content tracking mode. Learn more about the [different viewing modes](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00607) (*Similarity Report* mode, *Content tracking* mode, *Summary report* mode, *Largest matches* mode).

If two sources have exactly the same amount of matching text, the best match depends on which content repository contains the source of the match. For example, for two identical internet source matches, the most recently crawled internet source would be the best match. If an identical match is found to an internet source and a publication source, the publication source would be the best match.

## Accessing the Similarity Report (v1)<a id='00601' href='#00601'><i class='fas fa-link'></i></a>

To access the Similarity Report through iThenticate, start from the folder that contains the submission, and go to the *Documents* tab. In the *Report* column, you will see a button - click this *Similarity Score* to open the document in the Document Viewer.

<figure><img src='/images/documentation/SC-Similarity-Report-access.png' alt='Accessing the Similarity Report' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image85">Show image</button>
<div id="image85" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-access.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## The Document Viewer (v1)<a id='00602' href='#00602'><i class='fas fa-link'></i></a>

The Document Viewer screen opens in the last used viewing mode. There are three sections:

* Along the top of the screen, the *document information* bar shows details about the submitted document. This includes the document title, the date the report was processed, the word count and the number of matching sources found in the selected databases.
* The left panel is the *document text*. This shows the full text of the submitted document, highlighting areas of overlap with existing published content.
* The colors correspond to the matching sources, listed in the *sources* panel on the right.

<figure><img src='/images/documentation/SC-Similarity-Report-document-viewer.png' alt='Document viewer' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image86">Show image</button>
<div id="image86" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-document-viewer.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The layout will depend on your chosen report mode:

* **Match Overview** (show highest matches together) shows the best matches between the submitted document and content from the selected search repositories. Matches are color-coded and listed from highest to lowest percentage of matching word area. Only the top or best matches are shown - you can see all other matches in the *Match Breakdown* and *All Sources* modes.
* **All Sources** shows matches between the submission and a specifically selected source from the content repositories. This is the full list of all matches found, not just the top matches per area of similarity, including those not seen in the *Match Overview* because they are the same or similar to other areas which are better matches.
* **Match Breakdown** shows all matches, including those that are hidden by a top source and therefore don’t appear in *Match Overview*. To see the underlying sources, hover over a match, and click the *arrow* icon. Select a source to highlight the matching text in the submitted document. Click the *back arrow* next to *Match Breakdown* to return to *Match Overview* mode.
* **Side-By-Side Comparison** is an in-depth view that shows a document’s match compared side-by-side with the original source from the content repositories. From the *All Sources* view, choose a source from the sources panel, and a source box highlights on the submitted document similar content within a snippet of the text from the repository source. In *Match Overview*, select the colored number at the start of the highlighted text to open this source box. To see the entire repository source, click *Full Source View*, which opens the full-text of the repository source in the sources panel and all the matching instances. The sidebar shows the source’s full text with each match to the document highlighted in red. Click the *X* icon in the top right corner of the full source text panel to close it.

Use the *view mode* icons to switch between the *Match Overview* (default, left icon) and *All Sources Similarity Report* viewing modes. Click the *right* icon to change the *Similarity Report* view mode to *All Sources*.

<figure><img src='/images/documentation/SC-Similarity-Report-view-mode-icons.png' alt='View mode icons' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image87">Show image</button>
<div id="image87" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-view-mode-icons.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Viewing live web pages for a source (v1)<a id='00393' href='#00393'><i class='fas fa-link'></i></a>

You may access web-based sources by clicking on the source title/URL. If there are multiple matches to this source, use the *arrow* icons to quickly navigate through them.

If a source is restricted or paywalled (for example, subscription-based academic resources), you won’t be able to view the full-text of the source, but you’ll still see the source box snippet for context. Some internet sources may no longer be live.

From *Match Overview*, click the colored number at the start of a piece of highlighted text on the submitted document. A source box will appear on the document text showing the similar content highlighted within a snippet of the text from the repository source. The source website will be in blue above the source snippet - click the link to access it.

<figure><img src='/images/documentation/SC-Similarity-Report-match-overview.png' alt='Match overview' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image88">Show image</button>
<div id="image88" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-match-overview.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

From *Match Breakdown* or *All Sources*, select the source for which you want to view the website, and a diagonal icon will appear to the right of the source. Click this to access it.

<figure><img src='/images/documentation/SC-Similarity-Report-match-breakdown.png' alt='Match breakdown' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image89">Show image</button>
<div id="image89" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-match-breakdown.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>