+++
title = "Doc-to-doc comparison"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticate-account-use", "doc-to-doc-comparison",]
identifier = "documentation/similarity-check/ithenticate-account-use/doc-to-doc-comparison"
rank = 4
weight = 100307
aliases = [ "/education/similarity-check/ithenticate-account-use/doc-to-doc-comparison", "/education/similarity-check/ithenticate-account-use/doc-to-doc-comparison/",]
x-version = "0.0.0"

+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}


Use *doc-to-doc comparison* to compare a primary uploaded document with up to five comparison uploaded documents. Any documents that you upload to doc-to-doc comparison will not be indexed and will not be searchable against any future submissions.

Uploading a primary document to doc-to-doc comparison will cost you a single document submission, but the comparison documents uploaded will not cost you any submissions.

**[v2 Doc-to-doc comparison](https://help.turnitin.com/crossref-similarity-check/user/doc-to-doc-comparison.htm)**

**v1 Doc-to-doc comparison, keep reading:**


## How to use doc-to-doc comparison (v1)<a id='00610' href='#00610'><i class='fas fa-link'></i></a>

Start from *Folders*, go to the *Submit a document* menu, and click *Doc-to-Doc Comparison*.

<figure><img src='/images/documentation/SC-doc-to-doc-comparison.png' alt='Doc-to-doc comparison' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image102">Show image</button>
<div id="image102" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-doc-to-doc-comparison.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The doc-to-doc comparison screen allows you to choose one primary document and up to five comparison documents. Choose the destination folder for the documents you will upload. The Similarity Report for the comparison will be added to the same folder.

For your primary document, provide the author’s first name, last name, and document title. If you do not provide these details, the filename will be used for the title, and the author details will stay blank.

If you have administrator permissions, you can assign the Similarity Report for the comparison to a reporting group by selecting one from the *Reporting Group* drop-down. Learn more about [reporting groups](/documentation/similarity-check/ithenticate-account-setup/manage-users#00576).

Click *Choose File*, and select the file you want to upload as your primary document. See the file requirements for both the primary and comparison documents on the right of the screen.

You can choose up to five comparison documents to check against your primary document. These do not need to be given titles and author details. Each of the filenames must be unique. Click *Choose Files*, and select the files you would like to upload as comparison documents. To remove a file from the comparison before you upload it, click the *X* icon next to the file. To upload your files for comparison, click *Upload*.

Once your document has been uploaded and compared against the comparison documents, it will appear in your chosen destination folder.

This upload will have ‘Doc-to-Doc Comparison’ beneath the document title to show that this is a comparison upload and has not been indexed.

<figure><img src='/images/documentation/SC-doc-to-doc-comparison-document-viewer.png' alt='Document viewer' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image103">Show image</button>
<div id="image103" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-doc-to-doc-comparison-document-viewer.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The upload will be given a Similarity Score against the selected comparison documents, which is also displayed in the report column. Click the similarity percentage to open the doc-to-doc comparison in the Document Viewer.

The Document Viewer is separated into three sections:

* Along the top of the screen, the *paper information bar* shows details about the primary document, including document title, author, date the report was processed, word count, number of comparison documents provided, and how many of those documents matched with the primary document.
* On the left panel is the *paper text* - this is the text of your primary document. Matching text is highlighted in red.
* Your comparison documents will appear in the *sources* panel to the right, showing instances of matching text within the submitted documents.

By default, the doc-to-doc comparison will open the Document Viewer in the *All Sources* view. This view lists all the comparison documents you uploaded. Each comparison document has a percentage showing the amount of content within them that is similar to the primary document. If a comparison document has no matching text with the primary document, it has 0% next to it.

Doc-to-doc comparison can also be viewed in *Match Overview* mode. In this view, the comparison documents are listed with highest match percentage first, and all the sources are shown together, color-coded, on the paper text.