+++
title = "Get help with Similarity Check"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticate-account-use", "help",]
identifier = "documentation/similarity-check/ithenticate-account-use/help"
rank = 4
weight = 100310
alases = [ "/education/similarity-check/ithenticate-account-use/help", "/education/similarity-check/ithenticate-account-use/help/",]
x-version = "0.0.0"

+++

The team at Crossref can help you understand how to use the iThenticate tool - just [contact us](/contact).

However, if you encounter a technical issue (such as an error message or a bug), please contact the Turnitin team directly and copy [support@crossref.org](mailto:support@crossref.org).

Email: [ccsupport@ithenticate.com](mailto:ccsupport@ithenticate.com)

Phone: [+1 866 816 5046](tel:+18668165046), extension 241

To find out about outages or planned maintenance that affect the iThenticate system, check the [Turnitin status page](https://turnitin.statuspage.io) or [Turnitin's Twitter feed](https://twitter.com/TurnitinStatus). These pages display information on service outages, maintenance alerts, or incidents related to the performance of iThenticate. You can also subscribe to the notifications feed to automatically receive updates. For known bugs in the iThenticate software, please visit [Turnitin's known issues page](https://help.turnitin.com/known-issues.htm#ithTab).