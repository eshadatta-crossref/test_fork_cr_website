+++
title = "Account information"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticate-account-setup", "account-info",]
identifier = "documentation/similarity-check/ithenticate-account-setup/account-info"
rank = 4
weight = 100204
aliases = [ "/education/similarity-check/ithenticate-account-setup/account-info", "/education/similarity-check/ithenticate-account-setup/account-info/",]
x-version = "0.0.0"

+++

This section is for Similarity Check account administrators using iThenticate v1.

If you are using iThenticate v2 rather than iThenticate v1, there are separate instructions for you.
* Using iThenticate v2 directly in the browser - go to [setting up iThenticate v2](/documentation/similarity-check/ithenticatev2-account-setup/account-info).
* Integrating iThenticate v2 with your MTS - go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/account-info).

Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).

Not sure whether you're an account administrator? [Check here](/documentation/similarity-check/ithenticate-account-setup/admin-account).

## Manage your admin account
Manage your admin account using the *Account Information* tab. From here, you can make changes to your details in [My Profile](#00571), set up [URL filters](#00572) and [phrase exclusions](#00573) across the whole account, and set up [API access](#00574) to connect your iThenticate account to your manuscript submission system.

<figure><img src='/images/documentation/SC-admins-account-info.png' alt='Account information' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image60">Show image</button>
<div id="image60" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-account-info.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Your admin account profile (v1)<a id='00571' href='#00571'><i class='fas fa-link'></i></a>

{{< snippet "/_snippet-sources/similarity-check-account-info.md" >}}

## URL filters (v1)<a id='00572' href='#00572'><i class='fas fa-link'></i></a>

This tab only appears if you are an account administrator.

Use URL filters to apply URL exclusion filters across your account. Any URLs that you add here will be ignored when the system checks your manuscript against the iThenticate database, and it will apply across your whole account. If you want to let individual users decide which URLs to exclude instead, they can do this themselves at folder level.

URL filters at the account level works in the same way as at the folder level. Learn more about [exclusion settings when setting up a new folder](/documentation/similarity-check/ithenticate-account-use/folders#00586), [editing filters and exclusions in existing folders](/documentation/similarity-check/ithenticate-account-use/folders#00586), [filters and exclusions within the Similarity Report](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604), and [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) and [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573) for account administrators.

Add a URL to be filtered, and click *Add URL*. Don’t forget to include */* at the end of your URL. Click the *X* icon to the right of the URL to remove it.

<figure><img src='/images/documentation/SC-admins-URL-filters.png' alt='URL filters' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image61">Show image</button>
<div id="image61" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-URL-filters.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Phrase exclusions (v1)<a id='00573' href='#00573'><i class='fas fa-link'></i></a>

This tab only appears if you are an account administrator.

Use *Phrase Exclusions* to apply phrase exclusion filters across your account. Any phrases that you add here will be ignored when the system checks your manuscript against the iThenticate database, and it will apply across your whole account. If you want to let individual users decide which phrases to exclude instead, they can do this themselves at folder level.

Phrase exclusions at the account level works in the same way as at the folder level. Learn more about exclusion settings when [setting up a new folder](/documentation/similarity-check/ithenticate-account-use/folders#00586), [editing filters and exclusions in existing folders](/documentation/similarity-check/ithenticate-account-use/folders#00586), [filters and exclusions within the Similarity Report](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604), and [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) and [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573) for account administrators.

Click *Add a new phrase*, enter the phrase you would like to exclude in the *Phrase text* field, and click *Create*. You can add another phrase, go *Back to List*, or go *Back to Account*.

<figure><img src='/images/documentation/SC-admins-phrase-exclusions.png' alt='Phrase exclusions' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image62">Show image</button>
<div id="image62" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-phrase-exclusions.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

From the main Phrase Exclusions page, you can view, edit, or remove a phrase.

## API access (v1)<a id='00574' href='#00574'><i class='fas fa-link'></i></a>

This tab only appears if you are an account administrator.

If you want to connect your iThenticate account to your manuscript submission system, you can do this using the API. Once connected, you’ll be able to submit manuscripts for checking from within your manuscript submission system and see limited results. However, you'll need to visit the iThenticate website to explore the results further.

You’ll need to [contact iThenticate](mailto:ccsupport@ithenticate.com) to set up access to the iThenticate API. Once your account has API access enabled, you’ll see the *API Access IP addresses* option under *Account Info*.

<figure><img src='/images/documentation/SC-admins-API-access.png' alt='API access' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image64">Show image</button>
<div id="image64" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-API-access.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Use the *IP addresses* field to specify the IP address ranges that are allowed access to your account. Talk to your manuscript submission system contact for details of what to include here.

Use the special address `0.0.0.0` to allow access from any IP address. Enter addresses individually, or in Classless Inter-Domain Routing (CIDR) format, such as `192.68.2.0/24`. Add multiple addresses by separating them with a space.

Learn more about the [technical reference specification for the iThenticate API](https://help.turnitin.com/ithenticate/ithenticate-developer/api/api-guide.htm).