+++
title = "Manage users"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticate-account-setup", "manage-users",]
identifier = "documentation/similarity-check/ithenticate-account-setup/manage-users"
rank = 4
weight = 100205
aliases = [ "/education/similarity-check/ithenticate-account-setup/manage-users", "/education/similarity-check/ithenticate-account-setup/manage-users/",]
x-version = "0.0.0"

+++

This section shows Similarity Check account administrators using iThenticate v1 how to set up their users.

If you are using iThenticate v2 rather than iThenticate v1, there are separate instructions for you.
* Using iThenticate v2 directly in the browser - go to [setting up iThenticate v2](/documentation/similarity-check/ithenticatev2-account-setup/manage-users).
* Integrating iThenticate v2 with your MTS - go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/manage-users).

This tab only appears if you are an account administrator. If you can’t see this tab, please start from [your Similarity Check user account](/documentation/similarity-check/ithenticate-account-use/account-info/).

<figure><img src='/images/documentation/SC-admins-manage-users.png' alt='Manage users' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image55">Show image</button>
<div id="image55" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-manage-users.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

On this page, learn more about:

* [Profiles](#00575) - add, remove and update users on your account.
* [Groups](#00576) - create groups and add users
* [Reports](#00577) - access statistics for your account and help plan your budget
* [Sharing preferences](#00578) - manage what sharing options are available to your users
* [Email](#00579) - customise your welcome email

## Profiles (v1)<a id='00575' href='#00575'><i class='fas fa-link'></i></a>

Within user profiles, you can:

* **Add a user**: from *Add User*, click *Add New User*. On the *User Information* form, enter the new user's details. Use the *Reporting Group* drop-down to assign them to a reporting group. Choose a time zone and language (this will be the language they see in the user interface and welcome email). You may upload an image to be attributed to the user - click *Choose File* to choose an image file from your device. Under the *User Permissions* section, choose whether this user may: submit documents or only be a reader of shared documents, select a reporting group to which to assign documents when uploading, share their folders with other users, update their profile information, and whether you would like to make this user an account administrator. Click *Create* to add the user to the iThenticate account.
* **Add a list of users**: from *Add User*, choose *Upload User List*. To see an example of a correctly-formatted user list, click *examples*. Click *Browse*, choose your file, and click *Upload*. Click *View profile* to adjust the settings for each user.
* **Edit a user's information**: click *Edit* to the right of the user's email to make changes to a user’s details and permissions
* **Resend an activation email**: when a new user is added, they are sent an activation email. To resend their activation email, click *Send Activation*.
* **Deactivate a user**: from the *User Information* page, click *Deactivate User*. A deactivated user may no longer log in to iThenticate, but all files associated with them are retained, and still viewable by administrators. Click *Activate User* to reactivate a user and restore their access to the account and all of their submitted documents and folders.
* **Delete a user**: from the *User Information* page, click *Delete User* to permanently delete this user from the account. Once a user has been deleted, all the documents they submitted are no longer accessible by the account administrator or shared users. If you accidentally delete a user, click *undo* in the banner beneath the top menu. If you navigate away from the page, *Delete User* cannot be undone.
* **Search for a user**: enter the user’s name into the search field and click *Search*.

## Groups (v1)<a id='00576' href='#00576'><i class='fas fa-link'></i></a>

Use *Groups* to create reporting groups and add users to groups. By grouping users, you can track usage statistics of a group.

* To create a new group, enter a name for the new group in the *Add New Group* field and click *Add Report Group*.

<figure><img src='/images/documentation/SC-admins-group-add-new.png' alt='Add new group' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image56">Show image</button>
<div id="image56" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-group-add-new.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* Add users to the group by going to the user’s profile, and use the *Reporting Group* drop-down menu to add them to a group.
Delete a reporting group from your account by clicking the *X* icon to the right of the group name, and click *OK* to confirm.

<figure><img src='/images/documentation/SC-admins-group-delete.png' alt='Delete group' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image57">Show image</button>
<div id="image57" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-group-delete.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* Change the name of a group by clicking the group’s name, edit the *Update Group Name* field, and click *Update Group Name* to save the new group name.

<figure><img src='/images/documentation/SC-admins-group-name-edit.png' alt='Edit group name' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image58">Show image</button>
<div id="image58" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-group-name-edit.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## Reports (v1)<a id='00577' href='#00577'><i class='fas fa-link'></i></a>

Under the *Reports* tab, you can access statistics for your account, reporting groups, and individual account users.

<figure><img src='/images/documentation/SC-admins-reports.png' alt='Reports' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image59">Show image</button>
<div id="image59" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-admins-reports.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

* View usage statistics by user/group, month, or date range.
* Click a **group name** to see more detailed usage statistics for the users in that group. Click a **user’s name** within a group to see their individual usage report, including document submissions, page count per month, and total submissions made.
* Click your **organization name** to see your organization’s usage report, including statistics of all submissions by all account users. This will help you [budget](/documentation/similarity-check/ithenticate-account-setup#00038) for the per-document invoice you’ll receive each January for the documents you’ve checked in the previous year. Learn more about [fees for Similarity Check](/fees#similarity-check-per-document-fees).
* Click *change* by a report’s date range to change the **date range**. Enter dates in YYYY-MM-DD format or click the calendar icon to choose a date, then click *Update Date Range*
* Please note that the report will display a maximum of 150,000 lines/submissions. If your volume of submissions checked is higher than this for the time period you've entered, you'll need to adjust the date range to smaller increments.

### Reporting on estimated usage for budgeting (v1)<a id='00644' href='#00644'><i class='fas fa-link'></i></a>

Each January you'll be invoiced two separate [fees for Similarity Check.](/fees/#similarity-check-per-document-fees) There's the annual service fee (which is included in your annual membership invoice) and your annual per document checking fees for all the documents you've checked in the previous year.

We know it’s difficult to keep an eye on how many documents you’re checking, particularly if you have more than one person at your organization using the service. However, do monitor your usage against the budget you set for Similarity Check. As the account administrator, you can keep up-to-date with how many documents have been checked in the *Reports* section under *Manage Users*. This can help you to estimate what you'll be invoiced at the end of the year.

{{< figure-linked
	src="/images/documentation/SC-settings-date-range.png"
	large="/images/documentation/SC-settings-date-range.png"
	alt="Similarity Check: setting a date range for usage reports"
	title="Similarity Check: setting a date range for usage reports"
	id="image118"
>}}

Once in the the *Reports* section under *Manage Users*, click *Set Date Range*, choose your date range, and then click  *Update Date Range*.

What you see next will depend on how you’ve set up your iThenticate account.

All accounts will see an orange link in the name of your account, with the number of submissions and documents checked in the selected date range next to it. You can drill down into more information by clicking on the orange link - this will show documents checked by month across your account, split up by individual users.

If you’ve created groups, you’ll see a list of your groups with the number of submissions and documents checked in the date range for each group. You can drill down into more information by clicking on each group.

{{< figure-linked
	src="/images/documentation/SC-settings-usage-reports.png"
	large="/images/documentation/SC-settings-usage-reports.png"
	alt="Similarity Check: usage reports"
	title="Similarity Check: usage reports"
	id="image119"
>}}

#### The difference between submissions and documents and why this report is just an estimate (v1)<a id='00645' href='#00645'><i class='fas fa-link'></i></a>

There are two key columns on this table - *Submissions* and *Document count*.

The *Submissions* column shows the number of files you’ve submitted in iThenticate in your chosen date range, and the *Document count* shows how many documents these submissions are counted as. Some submissions include files that are so large that they're considered two or more documents. Your per document fees invoice will be based on the *Document count* column.

While this report provides an estimate for the per document fees invoice you'll receive in January, it won't be an exact match. For example, we don't charge you for the first 100 documents you check each year, and we try to avoid charging you if you accidentally submit the same document within a 24 hour period. You can find out more about these differences in our [billing section.](/members-area/all-about-invoicing-and-payment#00542)

## Sharing preferences (v1)<a id='00578' href='#00578'><i class='fas fa-link'></i></a>

From the *Sharing* tab, choose the type of sharing you would like to have for your account:

* View only folders shared by other users (default)
* View ALL users' folders
* View folders of selected users

To change the sharing type, select your preferred sharing type and click *Update Sharing*.

If you select the *View folders of selected users* option, you must also choose the users’ folders to be shared - to select a user, click the check-box next to their name, and click *Update Sharing*.

Set which non-administrator users may share folders by adjusting their permissions (learn more about [user profiles](/documentation/similarity-check/ithenticate-account-setup/manage-users#00575).

## Customize welcome email (v1)<a id='00579' href='#00579'><i class='fas fa-link'></i></a>

A welcome email is sent to new users you add to your account. To customize this welcome message, start from the *Email* tab.

The customized message is prefixed to the automated email, but does not replace it. The text of the automated email cannot be changed, as it contains important information about your account.

Edit the *Custom Email Subject* and *Custom Message* fields as you wish, and click *Set Custom Message*. The *Example "Welcome" Email Message* will update to show you a preview of the welcome email.