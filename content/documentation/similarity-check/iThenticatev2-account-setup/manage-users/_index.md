+++
title = "Manage users and folders"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "similarity-check", "iThenticatev2-account-setup", "manage-users",]
identifier = "documentation/similarity-check/ithenticatev2-account-setup/manage-users"
rank = 4
weight = 100255
x-version = "0.0.0"

+++

This section is for Similarity Check account administrators using iThenticate v2 through the browser.

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/manage-users).
* Integrating iThenticate v2 with your Manuscript Submission System (MTS) instead? Go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/manage-users).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

## Setting up users

You can find instructions on how to set up users [here](https://help.turnitin.com/crossref-similarity-check/administrator/users/adding-users-individually.htm).

## User groups

Users can be added to a User Group in order to help facilitate collaboration. Instructions on how to set up new User Groups can be found [here](https://help.turnitin.com/crossref-similarity-check/administrator/groups.htm).

## User settings

Once a user has been set up they can change exclusion settings in the Similarity report which will apply to all submissions they make. Users should be encouraged to review their settings before making any new submissions. Instructions on how to update these settings can be found [here](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/excluding-search-repositories.htm).

## Moving users from iThenticate v1 to iThenticate v2
If you have recently upgraded to iThenticate v2, you can move your existing users from iThenticate v1 over to your new iThenticate v2 account. See our [upgrade FAQs](/documentation/similarity-check/upgrading/faqs/) for more information.

## Shared folders

Shared Folders can be created when accessing iThenticate through the browser in order to help collaboration between users. Often a Shared Folder will be created for each journal so that all submissions sent to that folder can be accessed by each user from that journal. Folders can be shared either with individual users or with whole User Groups.

Instructions on how to share a folder can be found in [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/user/managing-files-and-folders/sharing-folders.htm).