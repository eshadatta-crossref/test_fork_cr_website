+++
title = "Similarity Check"
date = "2020-04-08"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "other-services", "similarity-check",]
aliases = [ "/education/similarity-check", "/education/similarity-check/", "/get-started/similarity-check", "/get-started/similarity-check/",]
x-version = "0.0.0"

[menu.main]
parent = "Documentation"
identifier = "documentation/similarity-check"
weight = 400000

[menu.documentation]
identifier = "documentation/similarity-check"
parent = "Documentation"
rank = 4
weight = 400000

+++

{{< snippet "/_snippet-sources/similarity-check.md" >}}