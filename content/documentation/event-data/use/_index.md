+++
title = "How can I use Event Data?"
date = "2020-10-06"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "event-data", "use",]
identifier = "documentation/event-data/use"
rank = 4
weight = 130200
aliases = [ "/education/event-data/use", "/education/event-data/use/",]
x-version = "0.0.0"

+++

The main way to access events created by Event Data is via the API, which returns data in [JSON](http://www.json.org) format. For example, the following finds the first 500 events:

```
https://api.eventdata.crossref.org/v1/events?rows=500
```

It is not required, but we recommend that you include your email address. We will not share this information, but can use it to contact you if a problem arises, for example:

```
https://api.eventdata.crossref.org/v1/events?mailto=anon@test.com&rows=500
```

[Full documentation for the API](https://www.eventdata.crossref.org/guide/service/query-api/) is available. Briefly, the results can be filtered by various parameters based on time, source, and object.

The results can also include facets of the data: summary counts of a certain characteristic. For example, to see the 10 news websites that have produced the most events, use the query:

```
https://api.eventdata.crossref.org/v1/events?rows=0&source=newsfeed&facet=subj-id.domain:10
```

If you want to make regular and extensive use of the API, we highly recommend reading the [full documentation](https://www.eventdata.crossref.org/guide/).

Developers are welcome to build applications based on Event Data, however Crossref doesn’t offer a dashboard or plugin to provide Event Data results. Some [Jupyter notebooks are available](https://gitlab.com/crossref/event_data_notebooks) that demonstrate possible uses of Event Data, including accessing all events about a single DOI.

## What is the current status of Event Data?<a id='00631' href='#00631'><i class='fas fa-link'></i></a>

The [Crossref status page](https://status.crossref.org) shows stability of the Event Data service and query response times. You can also track [the latest development tasks](https://gitlab.com/groups/crossref/-/issues?label_name%5B%5D=Service%3A%3AEvent+Data).

A list of the current agents is available on our [Gitlab pages](https://gitlab.com/crossref/event_data_agents).

## Who uses Event Data?<a id='00632' href='#00632'><i class='fas fa-link'></i></a>

There are a large number of uses for Event Data and it is intended as a free and transparent data source for third parties. Some examples of how the data can be used are the following:

* Building links between scholarly outputs, such as [Scholix](http://www.scholix.org)
* Raw data for bibliometrics research, such as [COKI](https://ccat.curtin.edu.au/programs/innovation-knowledge-communication/curtin-open-knowledge-initiative-coki/)
* Tracking the impact of research outputs, such as [HIRMEOS](https://operas.hypotheses.org/projects/hirmeos) from OPERAS; [Atypon](https://www.atypon.com/); [PaperBuzz](http://paperbuzz.org)
* Input for scholarly information services, such as [H1insights](https://www.h1insights.com)

This list is not exhaustive and we are interested to find out how you are using or would like to use Event Data. Please [get in touch](/contact/) if you have an interesting use case.