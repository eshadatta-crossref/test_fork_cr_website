+++
title = "Leveling up as a member"
date = "2020-12-03"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "leveling-up",]
identifier = "documentation/member-setup/leveling-up"
rank = 4
weight = 51000
aliases = [ "/education/member-setup/leveling-up", "/education/member-setup/leveling-up/",]
x-version = "0.0.0"

+++

Once you’ve mastered the three steps of [setting up as a member](/documentation/member-setup/) and have registered and displayed your new DOIs, it doesn’t stop there. You’ll need to continue to register any new content that you publish, and make sure to keep the metadata associated with existing DOIs up to date. There are also a range of other services you could use.

4. [Reference linking](#00655)
5. [Use other services](#00656)
6. [Maintain your DOIs and metadata](#)

## Step 4: Reference linking<a id='00655' href='#00655'><i class='fas fa-link'></i></a>

It’s an obligation of membership that you include other members’ DOIs in your reference lists - and they will do the same for you. Learn more about what this means and how to find DOIs in our [documentation on reference linking](/documentation/reference-linking/).

## Step 5: Use other services<a id='00656' href='#00656'><i class='fas fa-link'></i></a>

Alongside content registration and reference linking we offer a range of other services to help make your metadata richer, help you identify how and where your content is being talked about, and support editorial workflows.

### a) Richer metadata<a id='00657' href='#00657'><i class='fas fa-link'></i></a>

* [Funder Registry](/documentation/funder-registry/): you can use this open and unique registry of persistent identifiers for grant-giving organizations around the world so you can include Funder IDs in your metadata.
* [Crossmark](/documentation/crossmark/): add richer metadata about corrections and retractions and display this information on your content with the Crossmark button.

### b) Citations and more<a id='00658' href='#00658'><i class='fas fa-link'></i></a>

* [Cited-by](/documentation/cited-by/): find and display the number of times your content has been cited, and link to the citing content.
* [Event Data](/documentation/event-data/): when someone links their data online, or mentions research on a social media site, we capture that Event and make it available for anyone to use in their own way. We provide the unprocessed data—you decide how to use it.

### c) Support editorial workflows<a id='00659' href='#00659'><i class='fas fa-link'></i></a>

* [Similarity Check](/documentation/similarity-check/): this service gives Crossref members reduced rate access to the iThenticate service from Turnitin so they can check submitted manuscripts for similarities to published content.

## Step 6: Maintain your DOIs and metadata<a id='00660' href='#00660'><i class='fas fa-link'></i></a>

It’s an obligation of membership to make sure that your DOIs always resolve to a live landing page, so you may need to update your resource resolution URLs if your website moves or if you change platforms. You can also add to or amend existing metadata at any time. Any changes to an existing DOI after its initial registration are free of charge. Learn more about [metadata stewardship](/documentation/metadata-stewardship/).