+++
title = "Planning a platform migration"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "working-with-a-service-provider", "planning-a-platform-migration",]
identifier = "documentation/member-setup/working-with-a-service-provider/planning-a-platform-migration"
rank = 4
weight = 50903
aliases = [ "/blog/planning-platform-move", "/affiliates/migrating-platforms", "/education/member-setup/working-with-a-service-provider/planning-a-platform-migration", "/education/member-setup/working-with-a-service-provider/planning-a-platform-migration/", "/service-providers/migrating-platforms/", "/service-providers/migrating-platforms",]
x-version = "0.0.0"

+++

We understand that migrating your hosting platform is an extremely stressful time. There’s so much to think about and plan for, and the last thing you want to be worrying about is whether your DOIs will resolve after the move and what this will mean for the quality of your metadata.

Members often ask us for advice on the best way to handle a platform migration. This handy guide will help you prepare for the move, and gives hints and tips for managing the actual process as smoothly as possible. It also includes a very useful [checklist](/documentation/member-setup/working-with-a-service-provider/checklist-for-platform-migration) to include in your request for proposal (RFP) to make sure you’re asking the right questions of your vendors and making life easier for everyone involved.

## Selecting your new service provider<a id='00509' href='#00509'><i class='fas fa-link'></i></a>

A key element in selecting your new service provider will be ensuring they can do everything you need them to do with Crossref.

There’s a big difference between just depositing skeleton metadata and depositing full, rich metadata and participating in all our services. And there’s sometimes a big difference in cost from different providers. You’ll want to be clear on what you need up front so your providers know what you’re expecting and you don’t get any surprises further down the line. This also means you’ll be able to accurately compare the different service providers, and have a smoother working relationship with your chosen service provider in the future. So where do you start?

### Make sure you know how you’re participating in Crossref today<a id='00510' href='#00510'><i class='fas fa-link'></i></a>

The first place to start is to be clear on how you’re participating with us right now. This means your starting point is a metadata audit. We know you probably won’t want to do this with everything else that’s going on with a platform move. However, members who haven’t done this up front have said that they wish they had. They ended up having to do one anyway but further down the process and with less time, adding to the stress. It’s definitely better to start this early.

The easiest way to see what metadata you’re already depositing with us is to use our new [Participation Reports](https://www.crossref.org/members/prep/) tool. It may surprise you to see what you are and aren’t currently depositing, or what you current platform provider is or isn’t depositing on your behalf.

### Think about how you want to participate in Crossref in the future<a id='00511' href='#00511'><i class='fas fa-link'></i></a>

Once you’re clear on how you’re participating currently, think about what you may want to do in the future.

What [content types](/services/content-registration/) are you registering with us right now, and what you might want to register in the future? Perhaps you’re only registering journal articles right now, but are planning to register preprints in the future.

What metadata elements are you registering with us right now, and what you may want to register later? Are you registering references? They’re a key addition for discoverability and will be vital if you want to use the [Cited-by](/documentation/cited-by) service in the future. Are you registering full-text URLs and license information for text and data mining? What about full-text URLs for the [Similarity Check](/documentation/similarity-check/) service?

Learn more about [all our services](/services/).

### Ask the right questions of your service providers up front<a id='00512' href='#00512'><i class='fas fa-link'></i></a>

Members in the community have told us that even when some service providers have confirmed in their contract that they "support Crossref content registration" or "deposit metadata with Crossref" there have still been surprises when working together in terms of the amount of metadata the service provider can actually deposit, or costs for doing this fully.

To help ask Crossref-related questions of service providers, we’ve created a [checklist](/documentation/member-setup/working-with-a-service-provider/checklist-for-platform-migration). This contains specific questions to ask about content registration. You can add the checklist into your RFP so you’re absolutely clear on what your prospective service providers can and can’t provide. Your metadata audit and your thinking about what you want to do in the future will help you to select the right sections from the checklist.

## Planning the changeover process<a id='00513' href='#00513'><i class='fas fa-link'></i></a>

Once you’ve selected your new provider and are starting to think about the changeover process and timings, please [let us know](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691/).

Confirm with us which platform you’re moving to, and who we’ll be working with when you actually migrate. The best model for success is for us to work with a single contact. It doesn’t matter if that contact works for you or your new service provider, the key thing is that we keep the same contact consistently through your migration process.

If you need to update a lot of URLs (over 50,000) please let us know as soon as possible when you’re planning on doing this. This will slow down our deposit queue for other members, and we’ll need to avoid doing any maintenance while your update is running, so we’ll need to coordinate carefully with you on this.

This is a great opportunity to review the other contacts we hold for you, and make sure our billing, voting, technical, metadata quality, and primary (formerly business) contacts are up-to-date.

### Account details and permissions<a id='00514' href='#00514'><i class='fas fa-link'></i></a>

You need to think carefully about how your service provider will register (and update) content on your behalf.

* If your service provider will be using their own Crossref account credentials to register your content with us, do let us know in advance so we can give them permission to update your prefix using their credentials. We also may need to remove the account credentials for your old service provider.
* If your new service provider will be sharing your Crossref account credentials, we may need to update passwords so that your old service provider doesn't accidentally continue to update your records.

### Updating the URLs for your existing DOIs<a id='00515' href='#00515'><i class='fas fa-link'></i></a>

You’ll probably need to update the URLs for all your existing DOIs when you move. Updating multiple DOIs at a time in our system is a process, and can’t happen at a single moment in time. Do make sure that your platforms can coordinate redirection while this is happening.

The good news is that this update won’t cost you anything extra with Crossref - you’re only charged registration fees the first time you register a DOI. Although if you’ve uncovered backlist DOIs that were never registered as part of your metadata audit you will need to pay registration fees for them.

Don’t forget that you may have other types of URLs in the metadata that will need updating as well as the main URL where the DOI resolves to. If you’ve previously registered text and data mining URLs, or full-text URLs for Similarity Check, these will also need updating.

If you’re planning on updating your URLs, let us know and we can give you an estimate for how long this will take and help you manage the process efficiently.

### Updating contacts for reports<a id='00516' href='#00516'><i class='fas fa-link'></i></a>

We send out a range of reports to help our members manage their metadata. Please confirm with us which named contacts (and email addresses) should receive Crossref reports going forward. These reports include:

* Conflict report
* DOI error report
* Preprint version of record report
* Resolution report
* Title report

Don’t forget you may already have reports set up to go to your existing service provider, so do ask us to delete these.

Learn more about [reports](/documentation/reports/).

## After the migration<a id='00517' href='#00517'><i class='fas fa-link'></i></a>

Once your migration is complete, do keep an eye on your [Participation Reports](https://www.crossref.org/members/prep/) to make sure everything is happening as you think it should.