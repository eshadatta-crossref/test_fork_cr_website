+++
title = "Suffixes containing special characters"
date = "2021-12-03"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "constructing-your-dois", "suffixes-containing-special-characters",]
identifier = "documentation/member-setup/suffixes-containing-special-characters"
rank = 4
weight = 50702
aliases = [ "/education/member-setup/constructing-your-dois/suffixes-containing-special-characters", "/education/member-setup/constructing-your-dois/suffixes-containing-special-characters/", "/education/member-setup/constructing-your-dois/building-urls-for-dois-containing-special-characters/", "/education/member-setup/constructing-your-dois/building-urls-for-dois-containing-special-characters",]
x-version = "0.0.0"

+++

You might see (or inherit responsibility for) older DOIs which contain other characters, and require special treatment in a URL:  

* Encode *hash* or *pound* sign # as ```%23```
* Do not encode *left bracket* (or *less than*) < as ```&lt;``` and *right bracket* (or *greater than*) > as ```&gt;``` when resolving DOIs or retrieving metadata from our [REST API](https://api.crossref.org/swagger-ui/index.html) to retrieve the metadata (see below)
* Do not encode *forward slash* / when resolving DOIs or retrieving metadata from our REST API

For example, use the following when resolving DOIs with special characters:

```
https://doi.org/10.1002/(SICI)1521-3951(199911)216:1<135::AID-PSSB135>3.0.CO;2-%23
```

instead of:

```
https://doi.org/10.1002/(SICI)1521-3951(199911)216:1<135::AID-PSSB135>3.0.CO;2-#
```

And, to retrieve the metadata in our [REST API](https://api.crossref.org/swagger-ui/index.html), using those same DOI examples use:

```
https://api.crossref.org/works/10.1002/(SICI)1521-3951(199911)216:1%3C135::AID-PSSB135%3E3.0.CO;2-%23
```

instead of:

```
https://api.crossref.org/works/10.1002/(SICI)1521-3951(199911)216:1<135::AID-PSSB135>3.0.CO;2-#
```