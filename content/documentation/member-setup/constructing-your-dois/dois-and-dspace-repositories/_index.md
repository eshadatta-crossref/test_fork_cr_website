+++
title = "DOIs and DSpace repositories"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "constructing-your-dois", "dois-and-dspace-repositories",]
identifier = "documentation/member-setup/constructing-your-dois/dois-and-dspace-repositories"
rank = 4
weight = 50703
aliases = [ "/education/member-setup/constructing-your-dois/dois-and-dspace-repositories", "/education/member-setup/constructing-your-dois/dois-and-dspace-repositories/",]
x-version = "0.0.0"

+++

We encourage members with institutional repositories to assign DOIs to their original non-duplicative works.

Here are some simple guidelines for repositories based on [DSpace](https://duraspace.org/dspace/). DSpace and the DOI both use the Handle system for identifiers. When a DSpace repository is configured it must be registered with [CNRI](http://www.cnri.reston.va.us/), which provides the repository with a Handle prefix (typically a sequence of numbers). This is not a DOI prefix (also a sequence of numbers which begin with *10.*).

When constructing a Crossref DOI for your repository content, use the DSpace suffix as the DOI suffix. For example, a member with a DOI prefix of `10.1575` would construct a DSpace DOI like this: `10.1575/1912/1099`, where:

* `10.1575` = DOI prefix
* `1912` = DSpace prefix
* `1099` = DSpace suffix

The URL registered for the DOI should be the Handle URL, which uses the form http\://hdl.handle.net/DSpace-prefix/DSpace-suffix. So in this example, the URL registered for the DOI is: [http://hdl.handle.net/1912/1099](http://hdl.handle.net/1912/1099), and the DOI link is: [https://doi.org/10.1575/1912/1099](https://doi.org/10.1575/1912/1099).

You may also be interested in [recommendations for using ORCID in repositories](https://orcid.org/blog/2019/02/27/recommendations-using-orcid-repositories).