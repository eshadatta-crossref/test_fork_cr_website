+++
title = "Suggested DOI registration workflow, including suffix generator"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "constructing-your-dois", "suggested-doi-registration-workflow-including-suffix-generator",]
identifier = "documentation/member-setup/constructing-your-dois/suggested-doi-registration-workflow-including-suffix-generator"
rank = 4
weight = 50702
aliases = [ "/suffix-generator", "/suffix-generator/", "/education/member-setup/constructing-your-dois/suggested-doi-registration-workflow-including-suffix-generator", "/education/member-setup/constructing-your-dois/suggested-doi-registration-workflow-including-suffix-generator/",]
x-version = "0.0.0"

+++

Here is a suggested workflow for depositing DOIs and metadata in order to register your content. We also provide a suffix generator tool to help you create suffix strings which comply with best practices (suffixes should be opaque, unique, and short). [Download the suffix generator](/documentation-files/suffix-generator.xlsm) (macro-enabled .xlsm file). When you open it, choose _Read-only_, and _Enable macros_.

1. Create a spreadsheet to keep track of DOIs assigned (master list). Use one tab per prefix. Add columns for suffix, full DOI, and URL (you can add others if you wish)
2. Generate suffixes using our tool (or invent them yourself), and add each suffix and full DOI to the master list
3. Each time you create a new content item, add its URL to a new row. Display the DOI from the same row on your content item, following our [DOI display guidelines](/display-guidelines)
4. Deposit the the DOI and its associated metadata with Crossref. The DOI is only active once it is registered with us.

Checking for duplicates: the suffix generator should be sufficiently random to avoid creating duplicates. However, if when you deposit the metadata with Crossref, if the system says the DOI has already been registered, remove it from your master list, and use a new suffix.