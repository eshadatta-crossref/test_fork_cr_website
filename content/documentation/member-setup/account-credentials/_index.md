+++
title = "Your Crossref account credentials"
date = "2021-03-23"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "member-setup", "account-credentials",]
identifier = "documentation/member-setup/account-credentials"
rank = 4
weight = 50200
aliases = [ "/education/member-setup/changing-your-crossref-account-password", "/education/member-setup/changing-your-crossref-account-password/", "/education/member-setup/account-credentials", "/education/member-setup/account-credentials/",]
x-version = "0.0.0"

+++

To register your content with us you’ll need a set of Crossref account credentials. These credentials will consist of a username and a password.

Depending on when you joined Crossref and how you work with us, your organization might use unique and personal **user credentials** for each user at your organization, or alternatively, everyone at your whole organization might share a single set of **role credentials**. When you apply for membership, we’ll set you up with the best option based on the content registration tool you plan to use, and whether you joined Crossref through a sponsor.

On this page, learn more about:

* [Personal user credentials](#00368)
* [Organization-wide shared role credentials](#00376)
* [Forgotten your password, or want to change your password?](#00620)
  * [Password reset for personal user credentials](#00674)
  * [Password reset for organization-wide shared role credentials](#00675)


## Personal user credentials<a id='00368' href='#00368'><i class='fas fa-link'></i></a>

Using personal user credentials to access our tools and services is the most secure and flexible option.

If your organization will be using personal user credentials, we’ll send an email to each individual who needs to access our systems when we first create your account. This email will allow each user to set their own personal password. They will then use their email address and password to access our services – their username will be their personal email address, and their password will be the personal password that they’ve just set.

Each set of user credentials will be associated with a role – this role gives your users permission to register content on behalf of your organization. For some tools and services, the user will need to specify the role too.

These personal user credentials are unique to each individual user and should not be shared with others. If there are other people at your organization (or a third party) who need to register content on your behalf, you will need to [request that we add them as a user](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152). This request will need to come from one of the main [contacts](/documentation/metadata-stewardship/maintaining-your-membership#00022) we hold on your account to keep things secure.


## Organization-wide shared role credentials<a id='00376' href='#00376'><i class='fas fa-link'></i></a>

If everyone at your organization will be using a single set of shared role credentials to access our tools and services, we’ll create a new role when we first create your account. We'll then send an email to one person at your organization who will create a central, shared password for your organization.

If your organization is a direct member, the person who will set the shared password will be your [Technical contact](/documentation/metadata-stewardship/maintaining-your-membership/#00022). If you are a member of Crossref through a sponsor, then your Sponsor will set the password.

This new role and password can then be shared with anyone who will be registering content with Crossref for your organization. Individual people will all use the same shared role as their username, and the same shared password as the password.

## Forgotten your password, or want to change your password?<a id='00620' href='#00620'><i class='fas fa-link'></i></a>

### Password reset for personal user credentials<a id='00674' href='#00674'><i class='fas fa-link'></i></a>

If you use your personal email address and password to access our tools and services, you can use the "forgotten password" link in the [admin tool](https://doi.crossref.org) or [web deposit form](https://apps.crossref.org/webDeposit/) . This will send you an email with a link to reset your password.

When you change your personal user credentials password, this won’t have any effect on any other users at your organization.

### Password reset for organization-wide shared role credentials<a id='00675' href='#00675'><i class='fas fa-link'></i></a>

If your organization uses a central set of shared role credentials and you need to change the password, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) and we will be able to send a password reset email to one of the main contacts on your account. Crossref staff are not able to view or share the password.

Don’t forget, if you update the password on shared role credentials, then you will need to let all your colleagues know about the new password so they can still access our tools and services.