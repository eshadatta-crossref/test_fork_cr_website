+++
title = "Crossmark - registering updates"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "crossmark", "crossmark-registering-updates",]
identifier = "documentation/crossmark/crossmark-registering-updates"
rank = 4
weight = 110600
aliases = [ "/education/crossmark/crossmark-registering-updates", "/education/crossmark/crossmark-registering-updates/",]
x-version = "0.0.0"

+++

Typically, when an editorially significant update is made to a document, the publisher will not modify the original document, but will instead issue a separate document (such as a correction or retraction notice) which explains the change. This separate document will have a different DOI from the document that it corrects and will there have different metadata.

<figure><img src='/images/documentation/Crossmark-updates-and-updated-by.jpeg' alt='A retraction notice updates an article, which is updated by the retraction notice' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image31">Show image</button>
<div id="image31" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Crossmark-updates-and-updated-by.jpeg" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


In this example, article A (with the DOI 10.5555/12345678) is eventually retracted by a retraction notice RN (with the DOI 10.5555/24242424x). Each document has Crossmark metadata, but the fact that RN *updates* article A is only recorded in the RN's Crossmark deposit. The Crossmark internal API has to tie the two documents together and indicate in metadata of the original document (A), that it has been *updated_by* the second document (RN).

## Example 1: simple retraction<a id='00336' href='#00336'><i class='fas fa-link'></i></a>

This is a simple example of article A being retracted by a retraction notice RN where both A and RN have Crossmark metadata deposited.

First, the PDF is produced and the XML deposited to Crossref.

* [Article A Deposit XML](http://psychoceramics.labs.crossref.org/site/example1/article_a.xml)
* [Article A Landing Page](http://psychoceramics.labs.crossref.org/10.5555-12345678.html)
* [Article A XMP](http://psychoceramics.labs.crossref.org/site/example1/article_a.xmp)
* [Article A PDF](http://psychoceramics.labs.crossref.org/site/example1/article_a.pdf)

When the retraction is issued, it is issued as a separate "retraction notice" with its own DOI, PDF, and Crossref metadata.

* [Retraction Notice of A Deposit XML](http://psychoceramics.labs.crossref.org/site/example1/retraction_of_article_a.xml)
* [Retraction Notice of A Landing Page](http://psychoceramics.labs.crossref.org/10.5555-24242424x.html)
* [Retraction Notice of A XMP](http://psychoceramics.labs.crossref.org/site/example1/retraction_of_article_a.xmp)
* [Retraction Notice of A PDF](http://psychoceramics.labs.crossref.org/site/example1/retraction_of_article_a.pdf)

## Example 2: simple correction<a id='00337' href='#00337'><i class='fas fa-link'></i></a>

This is a simple example of article B being corrected by a correction notice CN where both B and CN have Crossmark metadata deposited. The only real difference between this and the previous example is that we are creating a different kind of update.

* [Article B Deposit XML](http://psychoceramics.labs.crossref.org/site/example2/article_b.xml)
* [Article B Landing Page](http://psychoceramics.labs.crossref.org/10.5555-12345679.html)
* [Article B XMP](http://psychoceramics.labs.crossref.org/site/example2/article_b.xmp)
* [Article B PDF](http://psychoceramics.labs.crossref.org/site/example2/article_b.pdf)
* [Correction notice of article B Deposit XML](http://psychoceramics.labs.crossref.org/site/example2/correction_of_article_b.xml)
* [Correction notice of article B Landing Page](http://psychoceramics.labs.crossref.org/10.5555-25252525x.html)
* [Correction notice of article B XMP](http://psychoceramics.labs.crossref.org/site/example2/correction_of_article_b.xmp)
* [Correction notice of article B PDF](http://psychoceramics.labs.crossref.org/site/example2/correction_of_article_b.pdf)

## Example 3: in-situ correction<a id='00338' href='#00338'><i class='fas fa-link'></i></a>

When a member does not issue a separate update/correction/retraction notice and instead just makes the change to the document (without changing its DOI either), this is called an *in-situ update*. In-situ updates or corrections are not recommended because they tend to obscure the scholarly record. How do you tell what the differences are between what you downloaded and the update? How do you differentiate them when citing them (remember, we are only talking about "significant updates" here)? However, some members need to support in-situ updates, and this is how they can be supported.

* [Article D Deposit XML before correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d_w_in_situ_before_correction.xml)
* [Article D Deposit XML after correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d_w_in_situ_after_correction.xml)
* [Article D Landing Page](http://psychoceramics.labs.crossref.org/10.5555-12345681.html)
* [Article D XMP generated **before** correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d.xmp)
* [Article D XMP generated **after** correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d_w_in_situ_after_correction.xmp)
* [Article D PDF generated **before** correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d.pdf)
* [Article D PDF generated **after** correction issued](http://psychoceramics.labs.crossref.org/site/example4/article_d_w_in_situ_after_correction.pdf)

## Example 4: correction of article that has no Crossmark metadata deposited<a id='00339' href='#00339'><i class='fas fa-link'></i></a>

If you deposit Crossmark metadata for a retraction or and update notice which, in turn, points at an article that does not have Crossmark metadata assigned to it, we will generate a "stub" Crossmark for the item being updated. The stub metadata will simply copy essential Crossmark metadata (*crossmark_domains* and *domain_exclusive*) from the updating metadata. This metadata can be queried via our API, but won’t activate anything on your site unless you add the Crossmark widget to the corresponding page of the item being updated.

* [Article E Deposit XML](http://psychoceramics.labs.crossref.org/site/example5/article_e.xml) (has no Crossmark metadata)
* [Article E Landing Page](http://psychoceramics.labs.crossref.org/10.5555-987654321.html) (again, no Crossmark button)
* Article E XMP (none exists because it doesn’t have Crossmark metadata)
* [Article E PDF](http://psychoceramics.labs.crossref.org/site/example5/article_e.pdf) (has no Crossmark button or metadata)

Still, note that if you query Crossmark metadata for Article E, you will get a Crossmark stub which has been automatically been generated by Crossref.

The procedure for updating the content follows the same pattern as a simple correction or retraction:

* [Correction of Article E Deposit XML](http://psychoceramics.labs.crossref.org/site/example5/correction_of_article_e.xml)
* [Correction of Article E Landing Page](http://psychoceramics.labs.crossref.org/10.5555-29292929x.html)
* [Correction of Article E XMP](http://psychoceramics.labs.crossref.org/site/example5/correction_of_article_e.xmp)
* [Correction of Article E PDF](http://psychoceramics.labs.crossref.org/site/example5/correction_of_article_e.pdf)

## Example 5: correction notice that corrected multiple documents<a id='00340' href='#00340'><i class='fas fa-link'></i></a>

Sometimes members issue correction or clarification notices which provide corrections for multiple documents. This too can be supported by Crossmark. In the following example, one correction/clarification document provides updates to two documents (F and G)

* [Article F Deposit XML](http://psychoceramics.labs.crossref.org/site/example6/article_f.xml)
* [Article F Landing Page](http://psychoceramics.labs.crossref.org/10.5555-777766665555.html)
* [Article F XMP](http://psychoceramics.labs.crossref.org/site/example6/article_f.xmp)
* [Article F PDF](http://psychoceramics.labs.crossref.org/site/example6/article_f.pdf)
* [Article G Deposit XML](http://psychoceramics.labs.crossref.org/site/example6/article_g.xml)
* [Article G Landing Page](http://psychoceramics.labs.crossref.org/10.5555-666655554444.html)
* [Article G XMP](http://psychoceramics.labs.crossref.org/site/example6/article_g.xmp)
* [Article G PDF](http://psychoceramics.labs.crossref.org/site/example6/article_g.pdf)
* [Correction Notice of F and G Deposit XML](http://psychoceramics.labs.crossref.org/site/example6/correction_of_articles_f_and_g.xml)
* [Correction Notice of F and G Landing Page](http://psychoceramics.labs.crossref.org/10.5555-3030303030x.html)
* [Correction Notice of F and G XMP](http://psychoceramics.labs.crossref.org/site/example6/correction_of_articles_f_and_g.xmp)
* [Correction Notice of F and G PDF](http://psychoceramics.labs.crossref.org/site/example6/correction_of_articles_f_and_g.pdf)