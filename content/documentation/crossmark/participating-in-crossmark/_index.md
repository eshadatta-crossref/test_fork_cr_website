+++
title = "Participating in Crossmark"
date = "2023-02-17"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "crossmark", "participating-in-crossmark",]
identifier = "documentation/crossmark/participating-in-crossmark"
rank = 4
weight = 110200
aliases = [ "/get-started/crossmark", "/education/crossmark/participating-in-crossmark", "/education/crossmark/participating-in-crossmark/",]
x-version = "0.0.0"

+++

Implementing Crossmark includes several stages, which require increasing technical knowledge. If you are not able to finish the process, that's ok, make a start and continue when you have the expertise to do so.
Full implementation means that you'll need to include Crossmark-specific metadata for each DOI that you register. And you'll need to add the DOI in a html meta-tag in each article landing page where Crossmark is implemented, as well as include a script in the source code for each page that calls the Crossmark dialog box.

On this page, learn more about:

* [Step one: Designate a Crossmark policy page and assign it a DOI](#00277)
* [Step two: Deposit the minimum Crossmark metadata for all your content](#00278)
* [Step three: Add metadata that reflects any updates to specific items](#00279)
* [Step four: Apply the Crossmark button to your HTML pages](#00280)
  * Add the button
  * Don't forget to record the DOI in the HTML metadata
* [Step five: Apply the Crossmark button to your PDF content](#00328)
* [Further options: adding more information to the Crossmark button](#00329)

## Step one: Designate a Crossmark policy page and assign it a DOI<a id='00277' href='#00277'><i class='fa fa-link'></i></a>

You'll need a way to explain Crossmark to your readers. To do this, you'll need a page on your website explaining that you are participating in the Crossmark service and explaining how you maintain and update your content. This page should be registered and have a DOI to enable persistent linking. It must include your policies on corrections, retractions, withdrawals and other updates, and may contain definitions and explanations of any additional custom metadata fields that are being used. The page could also include links to other relevant policies such as author submission guidelines, and peer review guidelines. You may already have a suitable page on your website, but don't forget to register it with us so it has a DOI.

Learn more about [creating a Crossmark policy page](/documentation/crossmark/crossmark-policy-page).

## Step two: Deposit the minimum Crossmark metadata for all your content<a id='00278' href='#00278'><i class='fa fa-link'></i></a>

It’s important to apply the Crossmark button to all of your current content, not just content that has updates. The problem with partial implementation of Crossmark is that when an item of content is published, you won’t know if it might need to be updated at some point in the future. Therefore, a researcher may download a PDF article today without a Crossmark button, but if the article is subsequently updated and the Crossmark button is added, the researcher has no way of knowing if their locally-saved article is still current, as it had no Crossmark button at the point when they downloaded it. If you're using the Crossmark service, we'd expect you to display the Crossmark button on all your content, whether or not it has an update.

The minimum metadata required for the Crossmark button to display is:

* The DOI of the content the Crossmark is being applied to
* The DOI for your Crossmark Policy Page

### Deposit Crossmark metadata by direct deposit of XML

If you usually register your content with us by uploading XML files into the admin tool or sending us XML by HTTPS POST, then you can include Crossmark metadata in your initial deposit. You can also add Crossmark metadata to existing DOIs using a [resource-only-XML deposit](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/resource_crossmark_4.8.0.xml). Some members find our [XML samples](/xml-samples/) helpful for full and [resource-only deposits](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/).

### Deposit Crossmark metadata the web deposit form (or if you’re still using the deprecated Metadata Manager)


Simply add your Crossmark metadata into the relevant fields.

In Metadata Manager, you need to add the Crossmark Policy Page up at journal level. Go to the [journal-level record](/documentation/content-registration/metadata-manager/setting-up-a-new-journal-in-your-workspace) for your publication, add your Crossmark policy page DOI, and click *Save*.

<figure><img src='/images/documentation/Crossmark-Metadata-Manager.png' alt='Add Crossmark policy page DOI in Metadata Manager' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image44">Show image</button>
<div id="image44" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Crossmark-Metadata-Manager.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Deposit Crossmark metadata using the Crossref plugin for OJS

This is not currently available, but should be coming soon.

## Step three: Add metadata that reflects any updates to specific items<a id='00279' href='#00279'><i class='fa fa-link'></i></a>

If there is an update to a content item, you need to add extra Crossmark metadata to indicate this. Updates should only be deposited for editorially significant changes - those that are likely to affect the interpretation or crediting of the work, and not for minor changes such as spelling corrections or formatting changes.

There are 12 defined types of accepted *update* within Crossmark:

* addendum
* clarification
* correction
* corrigendum
* erratum
* expression_of_concern
* new_edition
* new_version
* partial_retraction
* removal
* retraction
* withdrawal

If an update does not fall into one of these categories, it should instead be placed in the *more information* section of the pop-up box by being deposited as an *assertion*. Please note - @order is an optional attribute. If @order is absent, it will return results in the order in which you list them in your deposit, but this is not guaranteed. If you want to be *sure* of the order, then you can use @order. Learn more about the Crossmark deposit elements (including what is optional) in the [schema](https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html).

When deposited content corrects or updates earlier content, the DOI(s) of the corrected content must be supplied in the Crossmark metadata. See the [Crossref unixref documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.0/unixref.html) section on *updates* for examples of how this is recorded in the Crossmark metadata.
When a correction is made in situ (that is, it replaces the earlier version completely), then the DOI of the corrected content will be the same as the DOI for the original Crossref deposit. In situ updates are not considered best practice as they obscure the scholarly record.

## Step four: Apply the Crossmark button to your HTML pages<a id='00280' href='#00280'><i class='fa fa-link'></i></a>

### Add the button

We supply a templated HTML/JavaScript code widget which will embed the Crossmark button and functionality into your web pages. The latest version of the widget (v2.0) is below. Do ensure you are using the latest version and that it points to our production server. Do not alter the script or host the button locally.

```
<!-- Start Crossmark Snippet v2.0 -->
<script src="https://crossmark-cdn.crossref.org/widget/v2.0/widget.js"></script>
 <a data-target="crossmark"><img src="https://crossmark-cdn.crossref.org/widget/v2.0/logos/CROSSMARK_BW_horizontal.svg" width="150" /></a>
<!-- End Crossmark Snippet -->

```

There are a number of variations of the [Crossmark button](http://crossmark.crossref.org/widget/v2.0/readme.html) available so that you can choose one that fits well on your site. You can change the Crossmark button that is used simply by changing the `src` attribute of the `img` element to point to one of the following:

* [CROSSMARK_Color_square.eps](https://assets.crossref.org/logo/CROSSMARK_Color_square.eps)
* [CROSSMARK_Color_horizontal.eps](https://assets.crossref.org/logo/CROSSMARK_Color_horizontal.eps)

Alternatively, this you can view source on [this page](http://crossmark.crossref.org/widget/v2.0/readme.html) to see the correct code snippets for each style of button.

The button can be resized according to your design needs by changing the image width in the image tag but do follow the [Crossmark button guidelines](/documentation/crossmark/crossmark-button-guidelines).

### Don't forget to record the DOI in the HTML metadata

The Crossmark button needs to have a DOI to reference in order to pull in the relevant information. This needs to be embedded in the head of the HTML metadata for all content to which Crossmark buttons are being applied as follows:

`<meta name="dc.identifier" content="doi:10.5555/12345678"/>`

## Step five: Apply the Crossmark button to your PDF content<a id='00328' href='#00328'><i class='fa fa-link'></i></a>

At a minimum, this metadata must include the DOI of the content and the optional Crossmark domain(s). A minimal XMP file for the above PDF would look like this:

```
<?xml version="1.0" encoding="UTF-8"?>
<?xpacket begin="?" id="W5M0MpCehiHzreSzNTczkc9d"?>
<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 4.0-c316 44.253921, Sun Oct 01 2006 17:14:39">
<rdf:RDF xmlns:rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:pdfx = "http://ns.adobe.com/pdfx/1.3/"
xmlns:pdfaid = "http://www.aiim.org/pdfa/ns/id/"
xmlns:xap = "http://ns.adobe.com/xap/1.0/"
xmlns:xapRights = "http://ns.adobe.com/xap/1.0/rights/"
xmlns:dc = "http://purl.org/dc/elements/1.1/"
xmlns:dcterms = "http://purl.org/dc/terms/"
xmlns:prism = "http://prismstandard.org/namespaces/basic/2.0/"
xmlns:crossmark = "http://crossref.org/crossmark/2.0/">
<rdf:Description rdf:about="">
<dc:identifier>doi:10.5555/12345678</dc:identifier>
<prism:doi>10.5555/12345678</prism:doi>
<prism:url>http://dx.doi.org/10.5555/12345678</prism:url>
<crossmark:MajorVersionDate>2015-08-14</crossmark:MajorVersionDate>
<crossmark:DOI>10.5555/12345678</crossmark:DOI>
<pdfx:doi>10.5555/12345678</pdfx:doi>
<pdfx:CrossmarkMajorVersionDate>2015-08-14</pdfx:CrossmarkMajorVersionDate>
</rdf:Description>
</rdf:RDF>
</x:xmpmeta>
<?xpacket end="w"?>
```

It may appear redundant to apply Crossmark elements both in their own *Crossmark* namespace as well in the *pdfx* namespace, but the latter is necessary to ensure the Crossmark elements appear in the PDF dictionary, a specific requirement for some search engines. Any metadata found in the *pdfx* namespace will be copied over to the document info dictionary. Simply make sure that Crossmark metadata is in the *pdfx* namespace in the XMP provided to the tool.

The link structure for Crossmark links from PDFs looks like this:

```
https://crossmark.crossref.org/dialog/?doi=10.5555/12345678&amp;domain=pdf&amp;date_stamp=2008-08-14
```

where:

* *doi* is the DOI of the content item
* *domain* tells the Crossmark system what kind of static content the link is coming from, and will change for different static formats (such as epub)
* *date_stamp* tells the Crossmark system the date on which a last *Major Version* of the PDF was generated. In most cases, this will be the date the article was published. However, when a member makes significant corrections to a PDF *in-situ* (no notice issued, and no new version of the work with a new DOI) then the *date_stamp* should reflect when the PDF was regenerated with the corrections. The system will then use the *date_stamp* in order to tell whether the reader needs to be alerted to updates or not. The *date_stamp* argument should be recorded in the form YYYY-MM-DD (learn more about [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html)).

## Further options: adding more information to the Crossmark button<a id='00329' href='#00329'><i class='fas fa-link'></i></a>

The Crossmark box has a section for you to show any additional non-bibliographic information about the content. You decide what to include here (you are not required to add anything). In this section, Crossmark participants often include publication history dates, details of the peer review process used, and links to supporting information.

Use Metadata Manager to add custom metadata, or use the [assertion element](http://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#assertion) in your XML.

Several metadata elements will automatically display in the Crossmark box if you are registering them:

* Author names and their ORCID iDs (learn more about [contributors](/documentation/content-registration/descriptive-metadata/contributors/))
* Funding information (learn more about [funding information](/documentation/schema-library/markup-guide-metadata-segments/funding-information/))
* License URLs (learn more about [license information](/documentation/content-registration/administrative-metadata/license-information/))

If you are already registering this additional metadata at the time you implement Crossmark, there is nothing more you need to do. If you start to register these metadata elements after you have set up Crossmark, they will automatically be put into the Crossmark box.