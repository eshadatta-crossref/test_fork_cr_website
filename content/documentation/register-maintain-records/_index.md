+++
title = "Register and maintain your records"
date = "2022-07-22"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "content-registration",]
aliases = [ "/education/content-registration", "/faqs/depositing-metadata/", "/education/content-registration/", "/faqs/depositing-metadata/", "/faqs/depositing-metadata", "/documentation/content-registration/ojs-plugin/",]
x-version = "0.0.0"

[menu.main]
parent = "Documentation"
identifier = "documentation/content-registration"
weight = 150000

[menu.documentation]
parent = "Documentation"
identifier = "documentation/content-registration"
rank = 4
weight = 150000

+++

{{< snippet "/_snippet-sources/content-registration.md" >}}

## Types of metadata<a id='00116' href='#00116'><i class='fas fa-link'></i></a>
We collect many different types of metadata. You can [read more in our documentation](/documentation/research-nexus).

## Getting Started with Content Registration
Read through [Getting started as a new Crossref member](/documentation/member-setup) in our documentation.