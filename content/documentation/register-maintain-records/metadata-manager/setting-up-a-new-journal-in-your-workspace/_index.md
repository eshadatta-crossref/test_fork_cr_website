+++
title = "Setting up a new journal in your workspace"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "register-maintain-records", "metadata-manager", "setting-up-a-new-journal-in-your-workspace",]
identifier = "documentation/register-maintain-records/metadata-manager/setting-up-a-new-journal-in-your-workspace"
rank = 4
weight = 50501
aliases = [ "/education/member-setup/metadata-manager/setting-up-a-new-journal-in-your-workspace", "/education/member-setup/metadata-manager/setting-up-a-new-journal-in-your-workspace/", "/documentation/member-setup/metadata-manager-setting-up-a-new-journal-in-your-workspace/", "/documentation/member-setup/metadata-manager-setting-up-a-new-journal-in-your-workspace", "/documentation/content-registration/metadata-manager/setting-up-a-new-journal-in-your-workspace", "/documentation/content-registration/metadata-manager/setting-up-a-new-journal-in-your-workspace/",]
x-version = "0.0.0"

+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

When you log in using your [account credentials](/education/member-setup/account-credentials/), you’ll see a view of all the publications that have been added to your workspace.

To add a publication for which you have already registered metadata with Crossref, enter its title or title-level DOI into the search bar, and click *Add*. Once added to your workspace, you can update the title record by hovering your mouse over the publication title and select *Edit*, which will take you to the *Edit journal record* screen. If your publication does not already have a title-level DOI, you will need to add one. Learn more about [title-level DOIs](/documentation/principles-practices/journals/). Provide additional metadata for the publication record if available (the blue/asterisk \* mark indicates a required field).

To bring an article into your workspace, click into the chosen journal, and enter the article title into the *Article search* field.

To add a publication for which you have never registered metadata with Crossref, click *New publication*. On the *Edit journal record* screen, add details for the publication (the blue/asterisk \* mark indicates a required field).

<figure><img src='/images/documentation/MM-create-journal-record.png' alt='Edit journal record' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image42">Show image</button>
<div id="image42" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-create-journal-record.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Click *Save*, then *Close* to return to the journal list. The publication will now appear in your workspace.

<figure><img src='/images/documentation/MM-publications-in-workspace.png' alt='Publications in your workspace' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image43">Show image</button>
<div id="image43" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-publications-in-workspace.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>