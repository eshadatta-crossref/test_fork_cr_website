+++
title = "Review and submit"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "register-maintain-records", "metadata-manager", "review-and-submit",]
identifier = "documentation/register-maintain-records/metadata-manager/review-and-submit"
rank = 4
weight = 50504
aliases = [ "/education/member-setup/metadata-manager/review-and-submit", "/education/member-setup/metadata-manager/review-and-submit/", "/documentation/member-setup/metadata-manager/review-and-submit", "/documentation/member-setup/metadata-manager/review-and-submit/", "/documentation/content-registration/metadata-manager/review-and-submit", "/documentation/content-registration/metadata-manager/review-and-submit/",]
x-version = "0.0.0"

+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

The *Review* selection provides a condensed view of all the metadata you’ve provided in the form, so you can check it before submitting the record for deposit. Click *Continue* at the top of the form, and select *Review*. You can also *Review All* submissions on the *To deposit* screen before submitting the deposit.

## Submitting a deposit<a id='00487' href='#00487'><i class='fas fa-link'></i></a>

When you have finished adding article metadata and would like to deposit, click *Continue* from the article form, and select *Add to deposit*.

You can also do this from the *Record List* - select the article(s) you would like to deposit by checking the box to the left of the article title. You will then see the *Action* menu, and you can select *Add to deposit*. You can also *move to*, *duplicate*, and *remove* selected records using these buttons in the *Action* menu. If you select *Remove* for a record that has not been deposited, it will be erased from Metadata Manager. Records previously deposited will not be deleted from our system, only removed from the Metadata Manager workspace. If you created an article outside of a volume or issue, you can associate it with a volume or issue using *Move to*.

<figure><img src='/images/documentation/MM-action-menu.png' alt='Action menu with options to deposit, move, duplicate, remove, and transfer title' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image49">Show image</button>
<div id="image49" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-action-menu.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To submit your item(s) for deposit, click *To deposit* at the top of the screen. Please submit a maximum of 20 articles at a time. This will reduce the chance of an error with Metadata Manager.

Here, you can collect and review all your journal-specific records using *Review all*. The system will display any errors with a red flag by the respective record(s). You must correct these errors before you can deposit. If there are no errors, the *Deposit* button will be activated. Click *Deposit* and the system will immediately process your deposit request.

<figure><img src='/images/documentation/MM-deposit-menu.png' alt='Metadata Manager deposit menu' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image50">Show image</button>
<div id="image50" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/MM-deposit-menu.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>