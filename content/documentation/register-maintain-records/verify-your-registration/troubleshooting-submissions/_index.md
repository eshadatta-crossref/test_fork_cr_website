+++
title = "Troubleshooting submissions"
date = "2022-07-22"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "register-maintain-records", "verify-your-registration", "troubleshooting-submissions",]
identifier = "documentation/register-maintain-records/verify-your-registration/troubleshooting-submissions"
rank = 4
weight = 60703
aliases = [ "/education/content-registration/verify-your-registration/troubleshooting-submissions/", "/education/content-registration/verify-your-registration/troubleshooting-submissions", "/documentation/content-registration/verify-your-registration/troubleshooting-submissions/", "/documentation/content-registration/verify-your-registration/troubleshooting-submissions",]
x-version = "0.0.0"

+++

If you register your content with us using the web deposit form, XML file upload using our admin tool, or XML deposit using HTTPS POST, you’ll receive a [submission log email](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00641) from us. This email lets you know if your submission was successful, and if not, will provide error messages to explain more.

If you register your content with us using the third party Crossref XML plugin for OJS, or if you’re still using the deprecated Metadata Manager, you won’t automatically receive a submission log, and instead the success o r error message will show in your interface. However, you can still log in to the [admin tool](https://doi.crossref.org/) using your [Crossref account credentials](/documentation/member-setup/account-credentials/) to retrieve submission logs if you wish.

Here are some [examples of submission logs](/documentation/register-maintain-records/verify-your-registration/interpret-submission-logs/) - read on to find out more about a specific error message and how to fix the problem.

DOIs that return a *Warning* status in your submission logs have been deposited, but may need extra attention. A *Failure* status means that either a particular DOI has not been deposited, or the entire deposit file was unable to be processed due to some error.

{{% row %}}
{{% column %}}

* [Warnings](#00153)
* [Errors in title records](#00156)
  * [Errors in titles](#00634)
  * [Errors in title-level DOIs](#00635)
  * [Errors in title ownership](#00158)
  * [General title record discrepancy error](#00157)
  * [Invalid ISSN or ISBN error](#00159)

  {{% /column %}}
  {{% column %}}

* [Errors in DOI suffixes](#00626)
* [Errors in the XML](#00636)
* [Errors in timestamps](#00173)
* [Other types of errors](#00154)

{{% /column %}}
{{% /row %}}

## Warnings<a id='00153' href='#00153'><i class='fas fa-link'></i></a>

|Warning|Meaning|Solution|
|---|---|---|
|Added with conflict|The DOI was deposited; however, there was already another DOI in our system with identical metadata. This often occurs when an article is published ahead of print and deposited with no page numbers|Review DOIs with warnings and resolve all [conflicts](/documentation/reports/conflict-report)|

## Errors in title records<a id='00156' href='#00156'><i class='fas fa-link'></i></a>

In order to prevent duplicate records, and to ensure that DOIs are being assigned by the appropriate member, our deposit system includes certain checks on titles, ISSNs, and ISBNs.

When you submit the very first deposit for a work associated with a title, a title record is created in our system. This record includes the title, ISSNs or ISBNs, (optional) title-level DOI, content type, and the details of the associated member.

In all subsequent deposits, the title, ISSNs or ISBNs, and (if included) title-level DOI must match this existing record exactly.

There are several errors that you may see in your submission logs that may indicate that there isn’t a match.

### Errors in titles<a id='00634' href='#00634'><i class='fas fa-link'></i></a>

`ISSN "[ISSN]" has already been assigned, issn ([ISSN]) is assigned to another title ([title])`

Problem: The title in your submission is slightly different from the existing title record we hold.

You may think that the title you are depositing is the same as the title record we hold, but there are some common reasons why it may not be, such as:

* Punctuation, easily-confused characters, and punctuation marks that look similar:
  * An **em-dash** looks like this &#8212;, its UTF-8 (hex) representation is e2 80 94 and in [Unicode](http://www.unicode.org/charts/) it’s `U+2014`. An **en-dash** looks like this &#8211;, its UTF-8 (hex) representation is e2 80 93 and in Unicode it’s `U+2013`. The default dash-like character on some keyboards is a hyphen, but on others is an en-dash, so they can easily be confused. A **hyphen** looks like this &#8208;, its UTF-8 (hex) representation is e2 80 90, and in Unicode it’s `U+2010`.
  * Use of Cyrillic &#1061; (Cyrillic Ha) instead of Latin X. While the **Cyrillic Х** (`U+0425` in Unicode) looks almost exactly like the **Latin X** (`U+0058` in Unicode), they are not the same letter.
* Language: if your journal is in more than one language, you need to choose one version of the title as the master entry.
* Typos, such as `The Journal of Thigns`, in the spelling of your journal title
* Variant spellings of your journal title, such as `The Journal of Things` and `Journal of Things`

Solution 1: Change the title in your current submission to match the previously registered title and resubmit.

Solution 2: Change the title in the existing title record - you’ll need to [contact us for help with this](/contact).

### Errors in title-level DOIs<a id='00635' href='#00635'><i class='fas fa-link'></i></a>

`Deposit contains title error: The journal has a different DOI assigned; If you want to change the journal's DOI please contact Crossref support: title=Journal of Metadata; current-doi=10.14393/JoM; deposited-doi=10.14393/JoM.1.1`

Problem: The journal level DOI that you have in your submission is slightly different from the journal level DOI in our title record.

Solution 1: Change the journal level DOI in your submission to match the previously registered journal level DOI and resubmit.

Solution 2 : Change the journal level DOI in the existing title record - you’ll need to [contact us for help with this](/contact).

### Errors in title ownership<a id='00158' href='#00158'><i class='fas fa-link'></i></a>

`ISSN "{ISSN}" has already been assigned to a different publisher {publisher name}({publisher prefix})`

`ISBN "{ISBN}" has already been assigned to a different publisher {publisher name}({publisher prefix})`

`ISSN "[ISSN]" has already been assigned, title/issn: [journal title]/[issn] is owned by publisher: [prefix]`

Problem: These errors indicate that the title you are depositing is owned by another member or prefix in our system.

Solution: If you are the correct member for the title being deposited, please follow the procedures indicated in establishing and transferring ownership to have the [title record transferred to your member/prefix](/documentation/register-maintain-records/creating-and-managing-dois/transferring-responsibility-for-dois/).

### General title record discrepancy error<a id='00157' href='#00157'><i class='fas fa-link'></i></a>

`ISSN "{ISSN}" has already been assigned to a different title/publisher/content type`

Problem: This error indicates the ISSN(s), title, or publisher in your deposit do not match the data we have on record for that ISSN.

Solution: To verify the data in the title record with a given ISSN, please search for that ISSN on the [Crossref title list](https://www.crossref.org/titleList/). Resubmit with the correct information. If you need to change your title, [get in touch with us](/contact/). If there are any discrepancies between the title, ISSN(s), publisher, or content type that need to be updated, please [contact Support](/contact/) and include the submission ID for your deposit.

### Invalid ISSN or ISBN error<a id='00159' href='#00159'><i class='fas fa-link'></i></a>

`ISSN "{ISSN}" is invalid`

`ISBN "{ISBN}" is invalid`

Problem: This error indicates that the ISSN or ISBN in your deposit is incorrect. All valid ISSNs and ISBNs use a [check digit](http://www.loc.gov/issn/check.html) which is algorithmically validated by our deposit system.

Solution: If your deposit fails with this error, please verify the ISSN or ISBN, and resubmit the deposit with the correct number.

## Errors in DOI suffixes<a id='00626' href='#00626'><i class='fas fa-link'></i></a>

If you see `<msg>DOI: 10.5555/2411?3417 , contains invalid characters</msg>` in your submission log, your deposit has failed because within your DOI suffix `2411?3417`, there is a non-approved character, indicated by the `?`. You can learn more about [approved characters for DOI suffixes](/documentation/member-setup/constructing-your-dois#00007), but here are the two most common problems:

Problem 1: Using an em-dash or en-dash instead of a hyphen. An **em-dash** looks like this &#8212;, its UTF-8 (hex) representation is e2 80 94 and in [Unicode](http://www.unicode.org/charts/) it’s `U+2014`. An **en-dash** looks like this &#8211;, its UTF-8 (hex) representation is e2 80 93 and in Unicode it’s `U+2013`. The default dash-like character on some keyboards is a hyphen, but on others is an en-dash, so they can easily be confused.

Solution 1: reconfigure your DOI so it doesn't include an em- or en-dash, or replace the em- or en-dash with a hyphen. A **hyphen** looks like this &#8208;, its UTF-8 (hex) representation is e2 80 90, and in Unicode it’s `U+2010`.

Problem 2: Use of Cyrillic &#1061; (Cyrillic Ha) instead of Latin X. While the **Cyrillic Х** (`U+0425` in Unicode) looks almost exactly like the **Latin X** (`U+0058` in Unicode), they are not the same letter.

Solution 2: change the &#1061; to the Latin X or another approved character, and then resubmit your deposit.

## Errors in the XML<a id='00636' href='#00636'><i class='fas fa-link'></i></a>

`Deposited XML is not well-formed or does not validate: Error on line 538`

Problem: This error means that the XML is poorly formatted against our schema, or as an XML file in general. For example, it may contain self-closing tags, or invalid values.

Solution: Check your XML file for mistakes. Be sure to edit it in an XML editor and not a word processing program. Check you have saved the file correctly (as an .xml file), and deposit it again. If it still fails, [contact Support](/contact/) for help. We also have a [collection of XML examples](https://gitlab.com/crossref/schema/-/tree/master/examples) you may use as a template.

## Errors in timestamps<a id='00173' href='#00173'><i class='fas fa-link'></i></a>

`Record not processed because submitted version: 201907242206 is less or equal to previously submitted version 201907242206`

Problem: The timestamp in this deposit file is numerically smaller than the timestamp in a previous deposit for this same DOI.

Solution: Every deposit has a `<timestamp>` value, and that value needs to be incremented each time the DOI is updated. This is done automatically for you in the Crossref XML plugin for OJS, the web deposit form, or if you’re still using the deprecated Metadata Manager. But if you’re updating an existing DOI by sending us the whole XML file again, you need to make sure that you update the timestamp as well as the field you’re trying to update.

To fix this,  simply increment the timestamp value to be larger than the current timestamp value, and resubmit your XML file. Timestamps can be found by [reviewing past deposits](/documentation/register-maintain-records/verify-your-registration/submission-queue-and-log#00143), in the [depositor report](/documentation/reports/depositor-report), or by [retrieving the DOI's metadata record](/documentation/retrieve-metadata/xml-api/doi-to-metadata-query).

## Other types of errors<a id='00154' href='#00154'><i class='fas fa-link'></i></a>

When processing a metadata deposit, we do a number of checks to prevent the introduction of bad data to our system.

|Error|Meaning|Solution|
|---|---|---|
|User with ID: {0} can't submit into handle, please contact the Crossref admin|The handle system username and password assigned to this prefix is incorrect in the Crossref system|This is usually a clerical error. Please [contact Support](/contact/) and include the submission ID in your email|
|User not allowed to add records for prefix: {0}|The role that was used to submit this deposit does not have permissions to deposit DOIs beginning with this prefix|Confirm that you are using the correct prefix and Crossref credentials. If you’re still having trouble, please [contact Support](/contact/) and include the submission ID in your email|
|All prefixes in a submission must match (DOI[{0}])|All DOIs included in a single deposit submission must have the same prefix, regardless of ownership|Revise submission, and split the single file into multiple deposits, each with a single prefix. Then resubmit the new deposit files|
|title "{title}" was previously deleted by a Crossref admin|The title record being deposited or updated was deleted from our system, usually at the publisher's request|Review your title and compare to previous deposits for that type of content. If you’re still having trouble, please [contact Support](/contact/) and include the submission ID in your email|
|user not allowed to add or update records for the title "{title}"|The Crossref account that was used to submit this deposit does not have permissions to deposit for this title|Review title to confirm that you are using the appropriate account and prefix. If you’re still having trouble, please [contact Support](/contact/) and include the submission ID in your email|
| [error] :286:24:Invalid content starting with element {element name}'. The content must match '(("https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html": item_number) {0-3}, ("https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html": identifier) {0-10}) | This is an example of a parsing error being reported in the log file. Since this output comes directly from the Xerces parser the actual message will vary depending on the error | Review file at line / column indicated (in this example: line 286 column 24), edit, and resubmit. If you’re still having trouble, please [contact Support](/contact/) and include the submission ID in your email |
| org.jdom.input.JDOMParseException: Error on line 312 of document file:///export/home/resin/journals/crossref/inprocess/395032106: The content of elements must consist of well-formed character data or markup | Unacceptable markup in file | Review the file as indicated, correct, and resubmit |
| [fatal error] :1:1: Content is not allowed in prolog | Characters precede the XML declaration. This is almost always a Byte Order Mark (BOM) which most often occurs when word processing programs are used to edit XML files | Open file in a text or XML editor and remove characters (usually ï»¿\). If the encoding is shown as UTF-8 With BOM, change this to UTF-8 or UTF-8 Without BOM. Then resubmit the deposit. |
| java.io.UTFDataFormatException: invalid byte 1 of 1-byte > UTF-8 sequence (0x92) | There is a badly encoded character. Locate and correct the bad character encoding | Learn more about [using special characters in your XML](/documentation/member-setup/direct-deposit-xml#00662) |
| java.sql.SQLException: ORA-00001: unique constraint (ATYPON.NDX1_CIT_RELS) violated | Two files containing the same DOIs have been submitted simultaneously. The system attempts to process both deposits, but only one deposit will be successful. The unsuccessful deposit will generate this error | Review DOI metadata to be sure it was updated correctly |
| java.lang.NullPointerException | Most often this means a citation-only deposit or multiple resolution resource-only deposit has been uploaded as a metadata deposit (or vice-versa) | Resubmit deposit as DOI Resources or doDOICitUpload. If this does not apply to your deposit, please [contact Support](/contact/) and include the submission ID in your email |
| Submission version NULL is invalid | Schema declaration is incorrect | Resubmit with correct schema declaration |
| Invalid namespace/version | Wrong operation type, or submitted file is not XML | Submit with [correct operation type](/documentation/member-setup/direct-deposit-xml/https-post/) |
| cvc-pattern-valid: Value 'https://orcid.org/0000-0001-XXX-XXX' is not facet-valid with respect to pattern 'https?://orcid.org/[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[X0-9]{1}' for type 'orcid_t' | The ORCID ID you have included doesn't match the expected pattern. The most common reason for this is a trailing space | Remove the space (or update the ORCID ID to the correct format) and re-submit |
| cvc-enumeration-valid: Value 'VoR' is not facet-valid with respect to enumeration '[vor, am, tdm]'. It must be a value from the enumeration. | Some elements have a pre-defined list of values in our schema, and the submission must match these values exactly - they are even case sensitive. Here, the submission included the attribute of 'version of record' as *VoR*, but the schema defines that value as *vor*. | Correct to *vor* and resubmit.|