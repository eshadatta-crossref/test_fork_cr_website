+++
title = "Changing or deleting DOIs"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "register-maintain-records", "creating-and-managing-dois", "changing-or-deleting-dois",]
identifier = "documentation/register-maintain-records/creating-and-managing-dois/changing-or-deleting-dois"
rank = 4
weight = 60801
aliases = [ "/education/content-registration/creating-and-managing-dois/changing-or-deleting-dois", "/education/content-registration/creating-and-managing-dois/changing-or-deleting-dois/", "/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois", "/documentation/content-registration/creating-and-managing-dois/changing-or-deleting-dois/",]
x-version = "0.0.0"

+++

{{< snippet "/_snippet-sources/DOI-can't-delete.md" >}}

However, since mistakes happen, here are some work-around solutions to common problems.

## Your deposited DOI does not match the DOI you published<a id='00136' href='#00136'><i class='fas fa-link'></i></a>

If you've registered the wrong DOI for an item, it’s not possible to change the suffix of a DOI after it has been deposited. Instead, you'll need to do the following:

1. Deposit the correct DOI
2. [Alias](/documentation/reports/conflict-report#00241) the incorrect DOI to the correct DOI (or ask support to do this for you)

The incorrect DOI will then be redirected to the newly-registered DOI.

If you need to update a title-level book or journal DOI please [contact us](/contact/) as we will need to make adjustments to allow the update submission to pass.

## You deposited a DOI by mistake<a id='00137' href='#00137'><i class='fas fa-link'></i></a>

It’s not possible to fully delete a DOI, but in some circumstances we can direct a DOI to a [deleted DOI page](/_deleted-doi/). The metadata for the DOI is updated to disconnect the DOI from any identifying metadata. This process should only be applied to DOIs that have not been distributed or otherwise used for linking. DOIs for items that have been retracted or otherwise withdrawn should be directed to a retraction or withdrawal notice on the publisher's website.

If you think your DOI needs to be deleted, please [contact us](/contact/) with details.