+++
title = "Transferring prefixes between different Registration Agencies"
date = "2022-04-14"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
education_section = [ "register-maintain-records", "creating-and-managing-dois",]
identifier = "document/register-maintain-records/creating-and-managing-dois/prefix-transfers-other-ra"
rank = 4
weight = 60803
aliases = [ "/documentation/content-registration/creating-and-managing-dois/prefix-transfers-other-ra", "/documentation/content-registration/creating-and-managing-dois/prefix-transfers-other-ra/",]
x-version = "0.0.0"

+++

Alongside Crossref, there are other agencies of the DOI Foundation. Many focus on specific regions of the world (mEDRA, JaLC, CNKI, KISTI, et al) or on the needs of institutional repositories rather than publishers (eg DataCite).

It's important to carefully research which agency you want to join so you start with the right agency for you and continue to work with them for the long term. If you do start to work with one agency and need to move to another agency later, this is possible and prefixes can be transferred between Registration Agencies. But there's extra work for you to make this happen, so it's much better to start with the right agency. Do [contact us](/contact) for advice.

If you do wish to move between agencies and transfer your prefix:
* Contact the Registration Agency that you are moving to - the one that will be receiving the prefix. So to transfer a prefix from DataCite to Crossref for example, contact us. To transfer a prefix from Crossref to DataCite, contact DataCite.
* The two agencies will work together to confirm if the prefix can be transferred. This is usually possible, but if the prefix is shared or belongs to a generalist repository then it won't be able to be transferred and we'll need to take a different approach.
* If the prefix can be transferred, then the two Registration Agencies will liaise to make this happen.
* Once the prefix has been transferred, you will need to re-register all your existing DOIs with the new Registration Agency. This ensures that all metadata relating to your titles is with the same registration agency, you can manage all your DOIs in one central place, all your citation tracking will be centralized, and you can be sure that the thousands of organizations and individuals using Crossref metadata via our API have full information about your titles, helping to improve your discoverability. Re-registration is not optional - it's an obligation of the transfer. At Crossref, we will work with you to ensure that you are not charged for any content that you are re-registering with us after transferring a prefix from another agency.