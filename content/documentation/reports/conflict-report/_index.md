+++
title = "Conflict report"
date = "2022-08-01"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reports", "conflict-report",]
identifier = "documentation/reports/conflict-report"
rank = 4
weight = 140202
aliases = [ "/education/metadata-stewardship/reports/conflict-report", "/education/metadata-stewardship/reports/conflict-report/", "/education/reports/conflict-report/", "/education/reports/conflict-report", "/documentation/metadata-stewardship/reports/conflict-report", "/documentation/metadata-stewardship/reports/conflict-report/",]
x-version = "0.0.0"

+++

> #### The conflict report shows where two (or more) DOIs have been submitted with the same metadata, indicating that you may have duplicate DOIs. You’ll start receiving conflict reports if you have at least one conflict.

As you know, a DOI is a unique identifier — so there should only ever be one DOI for each content item.

Fix conflicts as soon as you can as they could lead to problems in the future. Having two separate DOIs for the same content means researchers won’t know which one to cite, and this risks splitting your citation count. You may also forget you have two DOIs, and update only one of them if your URLs change. This means anyone using the DOI you haven’t updated will come to a dead link. The good news is that it’s very quick to eliminate this bad metadata and solve the problem.

Conflicts most often occur for two reasons:

1. The metadata registered for content isn't sufficient to distinguish between two items. For example, items like *Book Reviews*, *Letters*, and *Errata* often share a single page and have no author
2. Two or more records have the same metadata (but different identifiers), suggesting that duplicate records have been created.

Conflicts are flagged in your submission log when a conflict is created. We also record all current conflicts in the [conflict report](https://www.crossref.org/06members/59conflict.html) on our website - if you do not see your member name on the conflict report page, you have no outstanding conflicts. If you have active conflicts, we’ll email you a reminder each month. However, if your conflict level has increased by 500+ then we’ll let you know right away, as this indicates a bigger problem. If your organization has more than one prefix, you’ll receive a separate email for each prefix.

## What should I do with my conflict report?<a id='00238' href='#00238'><i class='fas fa-link'></i></a>

On the [conflict reports page](https://www.crossref.org/06members/59conflict.html), you can locate your organization to see the conflicts. Please be patient, as the page can take a long time to load. You can view conflict details as an XML file or by title (see *View conflicts by title* below) as a simple .txt report.

Click your organization’s name to see which titles have the problem.

<figure><img src='/images/documentation/Conflict-report-conflicts-by-member.png' alt='Conflicts by member' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image11">Show image</button>
<div id="image11" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Conflict-report-conflicts-by-member.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Click each title to show a report that displays the DOIs in conflict.

Alternatively, you can see the conflict reports for your whole prefix by clicking on the .xml link.

Other information includes:

* *conflict ID* is the unique ID number for the conflict.
* *cause ID* is the deposit submission of the DOI causing the conflict.
* *other ID* is the deposit submission of the affected DOI.

## Example conflict report XML<a id='00239' href='#00239'><i class='fas fa-link'></i></a>

```
<conflict_report prefix="10.3201" date="Sep 20,2016">
 	<conflict id="4532830" created="2013-09-09 15:26:11.0" causeID="1361422379" otherIDs="1360220986,">
 	<doi_data>
   	<doi>10.3201/eid.1811.121112</doi>
   	<metadata>
     	<journal_title>Emerging Infectious Diseases</journal_title>
     	<volume>18</volume>
     	<issue>11</issue>
     	<first_page/>
     	<year>2012</year>
     	<article_title/>
   	</metadata>
   	<other_conflicts>
    	<conflict id="4532830" status="N"/>
   	</other_conflicts>
	</doi_data>
	<doi_data>
  	<doi>10.3201/eid1811.121112</doi>
  	<metadata/>
  	<other_conflicts>
     	<conflict id="4532830" status="N"/>
  	</other_conflicts>
	</doi_data>
 </conflict>
```

## View conflicts by title<a id='00240' href='#00240'><i class='fas fa-link'></i></a>

You can also examine the conflicts for a particular publication by clicking on the title in the expanded view. This will display a text file where:

* *ConfID* is the unique ID number for the conflict.
* *CauseID* is the deposit submission of the DOI causing the conflict.
* *OtherID* is the deposit submission of the affected DOI.
* *JT* is the publication’s title.
* *MD* is metadata for the DOIs. Metadata for DOIs in conflict will be the same.
* *DOI* is the DOI involved in the conflict.
* *Parenthetical value following the DOI* (such as *Journal* and *4508537-N* in this example) lists all the conflicts in which the DOI is involved and the resolution status of that conflict.
* *ALERT*, if it appears, indicates that the DOIs have more than one conflict, which can occur if they were deposited repeatedly with the same metadata. This field lists the other conflict IDs and their status:
    * null – Not resolved
    * A – Made an alias
    * P – Made a prime
    * U – Resolved by a metadata update
    * R – Manually erased or resolved

<figure><img src='/images/documentation/Conflict-report-conflicts-by-title.png' alt='Conflicts by publication title' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image12">Show image</button>
<div id="image12" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Conflict-report-conflicts-by-title.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


## Resolving conflicts<a id='00241' href='#00241'><i class='fas fa-link'></i></a>

There are three scenarios to cause two (or more) DOIs to be submitted with the same metadata.

Scenario 1: You assigned two DOIs to distinct content items, but accidentally submitted the same metadata for both of them. In this case, one of the DOIs has incorrect metadata. If you update and resubmit the deposit to correct that DOI's metadata, the conflict will be resolved.

Scenario 2: You assigned two DOIs to the same content item. In this case, you can resolve the conflict by assigning one of the DOIs as *primary* and the other as its *alias*. The alias DOI will automatically redirect to the primary DOI, so you'll only need to maintain the primary. Learn more about [creating aliases between DOIs](/documentation/reports/conflict-report#00243).

Scenario 3: The two DOIs refer to different content items, but their metadata is so similar that a conflict was flagged. This happens when items have very little metadata included. The best thing to do is to register more metadata to remove the conflict. If you can’t do this, you can accept the conflict - learn more about [accepting conflicts as-is](/documentation/reports/conflict-report#00246).

If you have any further questions about your conflict report, please [contact us](/contact/).

## Update your metadata<a id='00242' href='#00242'><i class='fas fa-link'></i></a>

If a conflict exists because the metadata you've deposited is sparse, you should [re-register your content](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00168) with additional metadata. The conflict status will be resolved when one (or both) items are re-registered with distinctive metadata. To be sure that the system updates the correct record, include the relevant DOI in your submission.

When making an update, you must supply all the metadata for the content item, not just the fields that need to be changed. During the update process, the system completely overwrites the existing metadata with the information you submit, including inserting null values for any fields not supplied in the update.

If the new metadata resolves the conflict, the system returns a message such as this one (which resulted from a redeposit of the metadata for DOI 10.50505/200702271050-conflict):

```
<record_diagnostic status="Success">
<doi>10.50505/200702271050-conflict</doi>
<msg>Successfully updated</msg>
**<resolved_conflict_ids>352052</resolved_conflict_ids>**
</record_diagnostic>
```

## Create aliases between DOIs<a id='00243' href='#00243'><i class='fas fa-link'></i></a>

If you have registered multiple records for the same content, you can *alias* the duplicate items to the (primary) record you intend to maintain. Records are aliased at the identifier (DOI) level. When DOIs are aliased, one DOI is flagged as the 'primary' DOI - the DOI you intend to maintain in the future. The remaining DOIs are aliased to the primary DOI at the DOI resolver level. This means that when someone clicks on an aliased DOI link, the user is automatically redirected to the URL registered for the primary DOI.

For example, if the metadata for `10.1103/PhysRev.69.674` and `10.1103/PhysRev.69.674.2` are the same, you might make `10.1103/PhysRev.69.674` the primary DOI. In this case, metadata queries that match both DOIs will resolve to `10.1103/PhysRev.69.674`, and DOI queries for either `10.1103/PhysRev.69.674` or `10.1103/PhysRev.69.674.2` will both return results.

You can assign primary status to DOIs in conflict one-by-one using the admin tool, or you can assign primary or [alias](/documentation/reports/conflict-report) status to multiple DOIs by uploading a .txt file.

Conflicts involving DOIs owned by other members must be resolved by Crossref - please [contact us](/contact/) for help with this.

### Assigning primary status from within the admin tool<a id='00244' href='#00244'><i class='fas fa-link'></i></a>

1. Log in to the [admin tool](https://doi.crossref.org) using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
2. Click *Metadata Admin* tab
3. Click *Conflict* tab (if necessary)
4. In the appropriate box, enter the *submission ID*, the *conflict ID*, or the *DOI*
5. Click *Submit* to see the DOIs associated with the conflict
6. Select the DOI that you want to make primary
7. Click *Make selected DOI primary in all conflicts*
<figure><img src='/images/documentation/Conflict-report-make-selected-DOI-primary-in-all-conflicts.png' alt='Conflict report: Make selected DOI primary in all conflicts' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image13">Show image</button>
<div id="image13" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Conflict-report-make-selected-DOI-primary-in-all-conflicts.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

8. If you make a mistake, you can undo it by returning to this page and clicking *Unresolve all conflicts*

### Assigning primary or alias status to multiple DOIs by uploading a .txt file<a id='00245' href='#00245'><i class='fas fa-link'></i></a>

The status you assign applies to all conflicts that involve the DOIs.

1. To assign DOIs primary status, create a .txt file with the header *H:email={email address};op=primary*, for example:

```
H:email=[your_email_address];op=primary
[your DOIs with conflicts, for example:
10.1016/0032-1028(80)90001-2
10.1016/0032-1028(80)90002-4
10.1016/0032-1028(80)90003-6
10.1016/0032-1028(80)90004-8
10.1016/0032-1028(80)90005-X
10.1016/0032-1028(80)90006-1
10.1016/0368-3281(63)90014-7]
```

Use *op=alias* when the primary DOIs are not known. If there are more than two DOIs involved in the conflict, the operation will be rejected because the system cannot determine which DOI to make primary.

2. Log in to the [admin tool](https://doi.crossref.org) using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
3. Click *Submissions* tab
4. Click *Upload* tab (if necessary)
5. Locate and select the metadata file
6. Select *Live*
7. Select *Conflict Management*
8. Click *Upload*

All the DOIs listed in the file will be assigned the status you specified in the *op* element. The system will send you a message like this one for an individual DOI:

```
<record_diagnostic doi="10.1088/0368-3281/5/6/313">
<conflict status="Success" ids="48983,49365,49783,50243,51067">
<msg>Marked as alias</msg>
<doi_list>
<doi>10.1016/0368-3281(63)90014-7</doi>
</doi_list>
</conflict>
</record_diagnostic>
or this one for multiple DOIs:
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch_diagnostic>
<submission_id>1181263946</submission_id>
<record_diagnostic doi="10.5555/prime">
<conflict status="Success" ids="23135669,2311211">
<msg>Marked as alias</msg>
<doi_list>
<doi>10.5555/a1</doi>
<doi>10.5555/a2</doi>
</doi_list>
</conflict>
</record_diagnostic>
</doi_batch_diagnostic>
```

## Accept conflicts as-is<a id='00246' href='#00246'><i class='fas fa-link'></i></a>

If you've determined that the content flagged with conflicts are not duplicate items, you can remove the conflict status by setting the status to 'resolved'. This has no impact on the metadata records or DOIs but will remove the conflicts from our conflict report.

In some cases, you may want to leave conflicting or ambiguous records in our metadata database. You can do this within our admin tool, or by uploading a .txt file to our admin tool.

### Accepting a conflict as-is using the admin tool<a id='00247' href='#00247'><i class='fas fa-link'></i></a>

To accept conflicts:

1. Log in to the [admin tool](https://doi.crossref.org) using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
2. Click *Metadata Admin* tab
3. Click *Conflict* tab (if necessary)
4. In the appropriate box, enter the *submission ID*, the *conflict ID*, or the *DOI*
5. Select *Show Consolidated Conflicts* (if available)
6. Click *Submit* to see the DOIs associated with the conflict.
7. Click *Mark All Conflicts as Resolved*

### Accepting a conflict as-is by uploading conflict management submissions to the admin tool<a id='00248' href='#00248'><i class='fas fa-link'></i></a>

If you have a large number of DOIs to resolve, you can submit a text file to the admin tool.

1. Create a .txt file with the following header (include your email address):

```
H:email=[your_email_address];op=resolve
[your DOIs with conflicts, for example:
10.1016/0032-1028(80)90001-2
10.1016/0032-1028(80)90002-4
10.1016/0032-1028(80)90003-6
10.1016/0032-1028(80)90004-8
10.1016/0032-1028(80)90005-X
10.1016/0032-1028(80)90006-1
10.1016/0368-3281(63)90014-7]
```

2. Log in to the [admin tool](https://doi.crossref.org) using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
3. Click *Submissions* tab
4. Click *Upload* tab (if necessary)
5. Locate and select the metadata file
6. Select *Live*
7. Select *Conflict Management*
8. Click *Upload*

Your conflict resolution file will be added to our submission queue and processed. A log will be sent to the email address you provided in the file header. Be sure to review the log to make sure your conflicts were resolved correctly.

## Forcing prime/alias<a id='00249' href='#00249'><i class='fas fa-link'></i></a>

You can force a DOI to be an alias of another DOI even if the DOIs are not in conflict. Please [contact us](/contact/) to discuss if this would be a suitable solution for your situation.

Extreme care MUST be taken when using this feature. Normally two DOIs are put into a prime/alias pair when their metadata is the same and a conflict is created. In this case, a metadata query will find both DOIs but because of the forced aliasing will return the prime DOI. If an aliased DOI has very different metadata from a primary DOI, the match may be a false positive.

To force an alias between two DOIs, create a text file as described below and upload to the admin tool.

1. Create the .txt file with tab-separated pairs of DOIs as follows:

```
H:email=youremail@address.com;op=force_alias;delim=tab
10.xxxx/primary1 10.xxxx/alias1
10.xxxx/primary2 10.xxxx/alias2
10.xxxx/primary3 10.xxxx/alias3
```

2. Log in to the [admin tool](https://doi.crossref.org) using your [Crossref account credentials](/documentation/member-setup/account-credentials/)
3. Click *Submissions*
4. Click *Upload*, if necessary
5. Next to FileName, select *Choose File*
6. Locate and select the force alias file
7. Select *Type Conflict Management*
8. Click *Upload*

### Removing a forced prime and/or alias ("un-aliasing")<a id='00250' href='#00250'><i class='fas fa-link'></i></a>

Only DOIs that have been aliased need to be un-aliased. Supplying *op=unalias* allows you to unalias previously forced aliases.

```
H:email=youremail@address.com;op=unalias;delim=tab
10.xxxx/alias1
10.xxxx/alias2
10.xxxx/alias3
```