+++
title = "DOI crawler report"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reports", "doi-crawler-report",]
identifier = "documentation/reports/doi-crawler-report"
rank = 4
weight = 140204
aliases = [ "/education/metadata-stewardship/reports/doi-crawler-report", "/education/metadata-stewardship/reports/doi-crawler-report/", "/education/reports/doi-crawler-report/", "/education/reports/doi-crawler-report",]
x-version = "0.0.0"

+++

We test a broad sample of DOIs to ensure resolution. For each journal crawled, a sample of DOIs that equals 5% of the total DOIs for the journal up to a maximum of 50 DOIs is selected. The selected DOIs span prefixes and issues.

The results are recorded in crawler reports, which you can access from the depositor report expanded view. If a title has been crawled, the last crawl date is shown in the appropriate column. Crawled DOIs that generate errors will appear as a bold link:

<figure><img src='/images/documentation/DOI-crawler-report.png' alt='DOI crawler report' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image17">Show image</button>
<div id="image17" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-crawler-report.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Click *Last Crawl Date* to view a crawler status report for a title:

<figure><img src='/images/documentation/DOI-crawler-status-report.png' alt='Crawler status report for a title' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image18">Show image</button>
<div id="image18" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-crawler-status-report.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


The crawler status report lists the following:

* Total DOIs: Total number of DOI names for the title in system on last crawl date
* Checked: number of DOIs crawled
* Confirmed: crawler found both DOI and article title on landing page
* Semi-confirmed: crawler found either the DOI or the article title on the landing page
* Not Confirmed: crawler did not find DOI nor article title on landing page
* Bad: page contains known phrases indicating article is not available (for example, *article not found*, *no longer available*)
* Login Page: crawler is prompted to log in, no article title or DOI
* Exception: indicates error in crawler code
* httpCode: resolution attempt results in error (such as 400, 403, 404, 500)
* httpFailure: http server connection failed

Select each number to view details. Select *re-crawl* and enter an email address to crawl again.