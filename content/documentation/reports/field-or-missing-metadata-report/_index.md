+++
title = "Field or missing metadata report"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reports", "field-or-missing-metadata-report",]
identifier = "documentation/reports/field-or-missing-metadata-report"
rank = 4
weight = 140206
aliases = [ "/education/metadata-stewardship/reports/field-or-missing-metadata-report", "/education/metadata-stewardship/reports/field-or-missing-metadata-report/", "/education/reports/field-or-missing-metadata-report/", "/education/reports/field-or-missing-metadata-report",]
x-version = "0.0.0"

+++

The field or missing metadata report gives details on metadata completeness and can be accessed by selecting the <img src="/images/documentation/Icon-green-arrow-right.png" alt="Green arrow right icon" height="23" > icon next to each member name in the [depositor report](/documentation/reports/depositor-report). The fields checked are volume, issue, page, author, article title, and Similarity Check URL.

To see your field or missing metadata report, use this URL but replace `10.5555` with your prefix:
```
https://apps.crossref.org/myCrossref?report=missingmetadata&datatype=j&prefix=10.5555
```

<figure><img src='/images/documentation/Field-missing-DOIs-per-title.png' alt='Field or missing metadata report by title' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image15">Show image</button>
<div id="image15" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Field-missing-DOIs-per-title.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Select a title to retrieve a list of DOIs for the title, and flagged fields for each DOI. For example, the DOIs in this report lack page and author information:

<figure><img src='/images/documentation/Field-missing-flagged-fields.png' alt='Flagged fields for each DOI' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image16">Show image</button>
<div id="image16" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Field-missing-flagged-fields.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Although the [deposit section of the schema](/documentation/content-registration/metadata-deposit-schema/xsd-schema-quick-reference) specifies that some bibliographic metadata is optional for content registration purposes, we strongly encourage members to register comprehensive metadata for each item registered.