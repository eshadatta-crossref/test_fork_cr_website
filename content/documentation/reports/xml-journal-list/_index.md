+++
title = "XML journal list"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reports", "xml-journal-list",]
identifier = "documentation/reports/xml-journal-list"
rank = 4
weight = 140211
aliases = [ "/education/reports/xml-journal-list", "/education/reports/xml-journal-list/", "/education/metadata-stewardship/reports/xml-journal-list", "/education/metadata-stewardship/reports/xml-journal-list/",]
x-version = "0.0.0"

+++

The XML journal list is an XML-formatted list of journal titles registered with Crossref. You can [download it as an MDDB.xml file](https://www.crossref.org/xref/xml/mddb.xml) (~10 MB file). The file includes journal titles, title abbreviations, ISSNs, year range, volumes, and the assigned DOI prefix for all journals registered with Crossref.

```
<journal title="ISPRS Journal of Photogrammetry and Remote Sensing"abbr="ISPRS Journal of Photogrammetry & Remote Sensing|ISPRS J Photogramm Remote Sens"issn="09242716|" years="1989-2007" volumes="44-62" prefix="10.1016"/>
```

Journal title and coverage information is also available - you can [download it as a .csv file](http://ftp.crossref.org/titlelist/titleFile.csv) (>5 MB file). This file is updated weekly.