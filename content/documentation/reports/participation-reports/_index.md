+++
title = "Participation Reports"
date = "2020-04-08"
draft = false
author = "Anna Tolwinska"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reports", "participation-reports",]
identifier = "documentation/reports/participation-reports"
rank = 4
weight = 140208
aliases = [ "/participation", "/participation/", "/education/metadata-stewardship/reports/participation-reports", "/education/metadata-stewardship/reports/participation-reports/", "/education/reports/participation-reports/", "/education/reports/participation-reports", "/faqs/participation-reports/",]
x-version = "0.0.0"

+++

[Participation Reports](https://www.crossref.org/members/prep/) are a visualization of the metadata that’s available via our free REST API. There’s a separate Participation Report for each member, and is shows what percentage of that members content includes ten key metadata elements. As a member, you can use Participation Reports to see for yourself where the gaps in your metadata are, and perhaps compare your performance to others. Participation Reports are free and open to everyone.	 

## How a Participation Report works<a id='00188' href='#00188'><i class='fas fa-link'></i></a>

There’s a separate Participation Report for each member. Visit [Participation Reports](https://www.crossref.org/members/prep/) and start typing the name of a member under *Find a member*. A list of member names will appear for you to select from. Behind the scenes, our REST API will pull together a report and output it in a clear, visual way.

The report shows what percentage of the member’s deposits have each of ten key metadata elements registered. These key elements add context and richness, and help to open up content to easier discovery and wider and more varied use.

This page describes the _what, why, where,_ and _how_, for each of the ten current checks:

{{% row %}}
{{% column %}}

* [References](#00189)
* [ORCID iDs](#00197)
* [Funder Registry IDs](#00201)
* [Funding award numbers](#00205)
* [Crossmark-enabled](#00209)

{{% /column %}}
{{% column %}}

* [Text-mining URLs](#00213)
* [License URLs](#00217)
* [Similarity Check URLs](#00221)
* [Abstracts](#00225)

{{% /column %}}
{{% /row %}}

### References<a id='00189' href='#00189'><i class='fas fa-link'></i></a>

Percentage of content items that include reference lists in their metadata.

#### Why is this important?<a id='00190' href='#00190'><i class='fas fa-link'></i></a>

Your references are a big part of the story of your content, highlighting its provenance and where it sits in the scholarly map. References give researchers and other users of Crossref metadata a vital data point through which to find your content, which in turn increases the chances of your content being read and used. They also enable you to use our Cited-by service, which means you can query for publications that cite your work, as well as show citation counts and lists on your articles.

#### Where can I learn more?<a id='00191' href='#00191'><i class='fas fa-link'></i></a>

* [Cited-by](/documentation/cited-by/) service

#### How can I improve my percentage?<a id='00192' href='#00192'><i class='fas fa-link'></i></a>

Whenever you register content with us, make sure you include your references in the submission. [Find out more here](/documentation/content-registration/descriptive-metadata/references/).  

You can also [add references to your existing content](/documentation/register-maintain-records/maintaining-your-metadata/add-references/).

### ORCID iDs<a id='00197' href='#00197'><i class='fas fa-link'></i></a>

Percentage of content containing [ORCID iDs](https://support.orcid.org/hc/en-us/articles/360006897334). These persistent identifiers enable users to precisely identify a researcher’s work - even when that researcher shares a name with someone else, or if they change their name.

#### Why is this important?<a id='00198' href='#00198'><i class='fas fa-link'></i></a>

Researcher names are inherently ambiguous. People share names. People change names. People record names differently in different circumstances.

Governments, funding agencies, and institutions are increasingly seeking to account for their research investments. They need to know precisely what research outputs are being produced by the researchers that they fund or employ. ORCID iDs allow this reporting to be done automatically and accurately.

For some funders, ORCID iDs are critical for their research investment auditing, and they are starting to mandate that researchers use ORCID iDs.

Researchers who do not have ORCID iDs included in their Crossref metadata risk not being counted in these audits and reports.

#### Where can I learn more?<a id='00199' href='#00199'><i class='fas fa-link'></i></a>

* [ORCID](https://orcid.org/)
* [Open letter: list of funders supporting ORCID](https://orcid.org/organizations/funders/open-letter)
* [Open letter: list of publishers supporting ORCID](https://orcid.org/content/requiring-orcid-publication-workflows-open-letter)
* [ORCID adoption through national consortia in Italy, New Zealand and Norway](https://orcid.org/blog/2018/04/30/establishing-orcid-consortia-center)
* [Ten reasons to get - and use - an ORCID iD!](https://www.elsevier.com/connect/authors-update/ten-reasons-to-get-and-use-an-orcid-id!)

#### How can I improve my percentage?<a id='00200' href='#00200'><i class='fas fa-link'></i></a>

Make sure you ask your authors for their ORCID iD through your submission system and include them when you register your content. There’s a specific [element in the xml for ORCID iDs](/documentation/content-registration/descriptive-metadata/contributors#00008) if you register via XML. If you use the [web deposit form](https://apps.crossref.org/webDeposit/) or if you’re still using the deprecated [Metadata Manager](https://www.crossref.org/metadatamanager/), there’s a specific field to complete.

To add ORCID iDs to existing content, you need to [update your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata).

### Funder Registry IDs<a id='00201' href='#00201'><i class='fas fa-link'></i></a>

The percentage of registered content that contains the name and Funder Registry ID of at least one of the organizations that funded the research.

#### Why is this important?<a id='00202' href='#00202'><i class='fas fa-link'></i></a>

Funding acknowledgements give vital context for users and consumers of your content. Extracting these acknowledgements from your content and adding them to your metadata allows funding organizations to better track the published results of their grants, and allows publishers to analyze the sources of funding for their authors and ensure compliance with funder mandates. And, by using the unique funder IDs from our central [Funder Registry](/documentation/funder-registry/), you can help ensure the information is consistent across publishers.

#### Where can I learn more?<a id='00203' href='#00203'><i class='fas fa-link'></i></a>

* [Funder Registry](/documentation/funder-registry/)

#### How can I improve my percentage?<a id='00204' href='#00204'><i class='fas fa-link'></i></a>

Make sure you collect funder names from authors via your submission system, or extract them from acknowledgement sections. Match the names with the corresponding Funder IDs from our [Funder Registry](/documentation/funder-registry/) and make sure you [include them in your future Crossref deposits](/documentation/funder-registry/funding-data-overview).

If your funder isn’t yet in the Funder Registry, please [let us know](/contact/).

To add funder information to content you’ve already registered, you can do a [full metadata redeposit](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata) (update), or use our [supplemental metadata upload method](/documentation/content-registration/web-deposit-form#00318).

### Funding award numbers<a id='00205' href='#00205'><i class='fas fa-link'></i></a>

The percentage of registered content that contains at least one funding award number - a number assigned by the funding organization to identify the specific piece of funding (the award or grant).

#### Why is this important?<a id='00206' href='#00206'><i class='fas fa-link'></i></a>

* Funding organizations are able to better track the published results of their grants
* Research institutions are able to track the published outputs of their employees
* Publishers are able to analyze the sources of funding for their authors and ensure compliance with funder mandates
* Everyone benefits from greater transparency on who funded the research, and what the results of the funding were.

#### Where can I learn more?<a id='00207' href='#00207'><i class='fas fa-link'></i></a>

* [Funder Registry](/documentation/funder-registry/)

#### How can I improve my percentage?<a id='00208' href='#00208'><i class='fas fa-link'></i></a>

Make sure you collect grant IDs from authors via your submission system, or extract them from acknowledgement sections. Make sure you include them in your future [Crossref deposits](/documentation/funder-registry/funding-data-overview) and add them to your existing content using our [supplemental metadata upload method](/documentation/content-registration/web-deposit-form#00318).

### Crossmark-enabled<a id='00209' href='#00209'><i class='fas fa-link'></i></a>

Percentage of content using the Crossmark service, which gives readers quick and easy access to the current status of an item of content - whether it’s been updated, corrected, or retracted.

#### Why is this important?<a id='00210' href='#00210'><i class='fas fa-link'></i></a>

Crossmark gives quick and easy access to the current status of an item of content. With one click, you can see if the content has been updated, corrected, or retracted and can access extra metadata provided by the publisher. It allows you to reassure readers that you’re keeping content up-to-date, and showcases any additional metadata you want readers to view while reading the content.

#### Where can I learn more?<a id='00211' href='#00211'><i class='fas fa-link'></i></a>

* [Crossmark](/documentation/crossmark/)

#### How can I improve my percentage?<a id='00212' href='#00212'><i class='fas fa-link'></i></a>

If you aren’t yet using Crossmark, you can ask us to enable Crossmark for you - simply [contact us](/contact/). Learn more about [participating in Crossmark](/documentation/crossmark/participating-in-crossmark).

### Text-mining URLs<a id='00213' href='#00213'><i class='fas fa-link'></i></a>

The percentage of registered content containing full-text URLs in the metadata to help researchers easily locate your content for text and data mining.

#### Why is this important?<a id='00214' href='#00214'><i class='fas fa-link'></i></a>

Researchers are increasingly interested in carrying out text and data mining of scholarly content - the automatic analysis and extraction of information from large numbers of documents. If you can make it easier for researchers to mine your content, you will massively increase your discoverability.

There are technical and logistical barriers to text and data mining for scholarly researchers and publishers alike. It is impractical for researchers to negotiate many different websites to locate the full-text that they need. And it doesn’t make sense for each publisher to have a different set of instructions about how to best find the full-text in the required format. All parties benefit from the support of standard APIs and data representations in order to enable text and data mining across both open access and subscription-based publishers.

Our API can be used by researchers to locate the full text of content across publisher sites. Members register these URLs - often including multiple links for different formats such as PDF or XML - and researchers can request them programmatically.

The member remains responsible for actually delivering the full-text of the content requested. This means that open access publishers can simply deliver the requested content, while subscription publishers use their existing access control systems to manage access to full-text content.

#### Where can I learn more?<a id='00215' href='#00215'><i class='fas fa-link'></i></a>

* [Text and Data Mining information](http://libereurope.eu/text-data-mining/) from [LIBER](https://libereurope.eu/)
* Crossref support for [text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining)

#### How can I improve my percentage?<a id='00216' href='#00216'><i class='fas fa-link'></i></a>

Make sure you include full-text URLs in your future [Crossref deposits](/documentation/funder-registry/funding-data-overview) and add them to your existing content using a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/).

### License URLs<a id='00217' href='#00217'><i class='fas fa-link'></i></a>

The percentage of registrations that contain URLs that point to a license that explains the terms and conditions under which readers can access content.

#### Why is this important?<a id='00218' href='#00218'><i class='fas fa-link'></i></a>

Adding the full-text URL into your metadata is of limited value if the researchers can’t determine what they are permitted to do with the full text. This is where the license URLs come in. Members include a link to their use and reuse conditions: whether their own proprietary license, or an open license such as [Creative Commons](https://creativecommons.org/).

#### Where can I learn more?<a id='00219' href='#00219'><i class='fas fa-link'></i></a>

* [License information](/documentation/content-registration/administrative-metadata)

#### How can I improve my percentage?<a id='00220' href='#00220'><i class='fas fa-link'></i></a>

Make sure you include license URLs in your future [Crossref deposits](/documentation/funder-registry/funding-data-overview), and add them to your existing content using a [resource-only deposit](/documentation/register-maintain-records/maintaining-your-metadata/resource-only-deposit/), or by using a [supplemental metadata upload](/documentation/content-registration/web-deposit-form#00318).

### Similarity Check URLs<a id='00221' href='#00221'><i class='fas fa-link'></i></a>

The percentage of content registered that includes full-text links for the Similarity Check service.

#### Why is this important?<a id='00222' href='#00222'><i class='fas fa-link'></i></a>

The Similarity Check service helps you to prevent scholarly and professional plagiarism by providing editorial teams with access to Turnitin’s powerful text comparison tool.

Similarity Check members contribute their own published content to iThenticate’s database of full-text literature via Similarity Check URLs, and this is an obligation of using the service. If members aren’t registering these, they can’t take part in the Similarity Check service.

#### Where can I learn more?<a id='00223' href='#00223'><i class='fas fa-link'></i></a>

* [Similarity Check](/documentation/similarity-check/)

#### How can I improve my percentage?<a id='00224' href='#00224'><i class='fas fa-link'></i></a>

For future content, make sure you include these URLs as part of your standard metadata deposit. They need to be deposited within the *crawler-based* collection property, with item crawler *iParadigms*.

You can add these URLs into your already-deposited DOIs using a resource-only deposit, or by using the *Supplemental-Metadata Upload* option available with our [web deposit form](https://apps.crossref.org/webDeposit/).

### Abstracts<a id='00225' href='#00225'><i class='fas fa-link'></i></a>

Percentage of content that includes the abstract in the metadata, giving further insights into the content of the work.

#### Why is this important?<a id='00226' href='#00226'><i class='fas fa-link'></i></a>

The abstract gives more information to the user about your content, making your items more discoverable.

#### Where can I learn more?<a id='00227' href='#00227'><i class='fas fa-link'></i></a>

* [Abstracts](/documentation/content-registration/descriptive-metadata)

#### How can I improve my percentage?<a id='00228' href='#00228'><i class='fas fa-link'></i></a>

Make sure you include abstracts when you register your content - it’s available for everything other than dissertations and reports. For existing content, you can add abstracts by running a full [metadata redeposit](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00168) (update).