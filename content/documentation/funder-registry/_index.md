+++
title = "Funder Registry"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "other-services", "funder-registry",]
aliases = [ "/education/funder-registry", "/education/funder-registry/", "/services/content-registration/funding-data/", "/services/content-registration/funding-data",]
x-version = "0.0.0"

[menu.documentation]
identifier = "documentation/funder-registry"
parent = "Documentation"
rank = 4
weight = 350000

+++

{{< snippet "/_snippet-sources/funder-registry.md" >}}