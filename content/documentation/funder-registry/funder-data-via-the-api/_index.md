+++
title = "Funder data via the API"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "funder-registry", "funder-data-via-the-api",]
identifier = "documentation/funder-registry/funder-data-via-the-api"
rank = 4
weight = 90500
aliases = [ "/education/funder-registry/funder-data-via-the-api", "/education/funder-registry/funder-data-via-the-api/",]
x-version = "0.0.0"

+++

Funding data can be accessed via the [Crossref REST API](https://api.crossref.org/). The API is openly available, and there is no requirement to register or be a Crossref member in order to use it. Funder IDs are DOIs that share a common prefix, so for API queries, only the DOI suffix should be used. Learn more about [the structure of a DOI](/documentation/member-setup/constructing-your-dois/).

## Useful queries<a id='00309' href='#00309'><i class='fas fa-link'></i></a>

* All funders listed in the Open Funder Registry - [https://api.crossref.org/funders](https://api.crossref.org/funders)
* Find the funder ID for a specific funding body - https\://api.crossref.org/funders?query={name}. For example: [https://api.crossref.org/funders?query=wellcome](https://api.crossref.org/funders?query=wellcome)
* List of DOIs associated with a specific funder - https\://api.crossref.org/funders/{funder ID}/works. For example: [https://api.crossref.org/funders/100004440/works](https://api.crossref.org/funders/100004440/works)
* Metadata for DOIs that cite a specific award/grant number - https\://api.crossref.org/works?filter=award.number:{grant number}. For example: [https://api.crossref.org/works?filter=award.number:CBET-0756451](https://api.crossref.org/works?filter=award.number:CBET-0756451)

Learn more in our [REST API documentation](https://github.com/CrossRef/rest-api-doc), including detailed instructions on constructing further API queries.