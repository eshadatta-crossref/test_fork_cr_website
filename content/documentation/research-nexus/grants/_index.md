+++
title = "Introduction to grants"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "research-nexus", "grants",]
identifier = "documentation/research-nexus/grants"
rank = 4
weight = 60505
aliases = [ "/education/content-registration/content-types-intro/grants", "/education/content-registration/content-types-intro/grants/", "/documentation/registering-grants", "/documentation/registering-grants/", "/education/registering-grants", "/education/registering-grants/", "/documentation/content-registration/content-types-intro/grants", "/documentation/content-registration/content-types-intro/grants",]
x-version = "0.0.0"

+++

Funders are joining Crossref to register their grants so that they can more easily and accurately track the outputs connected to the research they support.

Once you're a member, registering grants with us means giving us information about each awarded grant, including a DOI which uniquely and persistently identifies each record. You can use the [grant registration form](/documentation/register-maintain-records/grant-registration-form) or [direct XML deposit methods](/documentation/member-setup/direct-deposit-xml/) to deposit and update grant metadata. This section focuses on [grants](/documentation/content-registration/content-types-intro/grants/), but research funders can also register [other content types](/documentation/content-registration/content-types-intro/) such as reports, data, and working papers.

### Something to consider before you begin<a id='00018' href='#00018'><i class='fas fa-link'></i></a>

Decide which grants to register first, as you get into the swing of things. For example, pilot a particular country, or area of support. It’s better to start with newly-awarded grants, and then move on to older or long-running awards - these are cheaper to register, and are more likely to have produced research papers, so they’re great for demonstrating the full potential of connected research metadata.

### Constructing your identifiers (DOIs)<a id='00021' href='#00021'><i class='fas fa-link'></i></a>

A DOI is made up of a DOI resolver, a prefix, and a suffix. When you join Crossref as a member, we give you a DOI prefix. You combine this with a suffix of your choice to create a DOI. Although some funders choose to use their internal grant identifier as the DOI suffix, we advise you to make your suffix opaque, meaning that it does not encode or describe any information about the work. Your DOI becomes active once it is successfully registered with us. Read more about [constructing your DOIs](/documentation/member-setup/constructing-your-dois/).

### Grant landing pages<a id='00023' href='#00023'><i class='fas fa-link'></i></a>

Your grant metadata records should link to a landing page where you can find information about the grant. Examples: [https://doi.org/10.37717/220020589](https://doi.org/10.37717/220020589), [https://doi.org/10.35802/107769](https://doi.org/10.35802/107769). Read more about [landing pages](/documentation/member-setup/creating-a-landing-page#landing-pages-for-grants).

Should a grant move to a new landing page, the URL in the grant’s metadata is updated to point to the new location. There’s no charge to update metadata for existing deposits.

### Registering grant metadata<a id='00030' href='#00030'><i class='fas fa-link'></i></a>

Grants can be registered for all sorts of support provided to a research group or individual, such as awards, use of facilities, sponsorship, training, or salary awards.
Here’s the [section of our schema for grant metadata](https://gitlab.com/crossref/schema/-/blob/master/schemas/grant_id0.0.1.xsd). If you’re working with a third-party system, such as Proposal Central or EuroPMC, they may be able to help with this piece of work.

#### Registering grant metadata using the grant registration form<a id='00031' href='#00031'><i class='fas fa-link'></i></a>

You can use the [grant registration form](https://beta.crossref.org/records) to register grants, with no prior knowledge of XML. You fill out the form and the XML is created for you in the background. You enter your account credentials and the metadata is submitted directly.

#### Formatting grant metadata for direct deposit<a id='00024' href='#00024'><i class='fas fa-link'></i></a>

If you'd prefer to work directly with XML, you may be able to map your own data and identifiers to our schema. See our [example deposit file](https://gitlab.com/crossref/schema/-/blob/master/examples/grantid_full_example.xml) - this is a full example, and many of the fields it contains are optional, but we encourage you to provide as much information as you can. Rich metadata helps maximum reuse of the grant records you register with Crossref. This [.xsd file](https://www.crossref.org/schema/grant_id0.0.1.xsd) helps explain what goes into each field, and the parameters (length, format) of what is accepted in each field. [Here’s a less techy version](https://docs.google.com/document/d/1qO2WFhxyCH7vk59emBw4SdY81Q2whdd4SLVKOCUkvCI/edit).

When you’ve created your XML files, use our [checker](https://www.crossref.org/02publishers/parser.html) to test them - this will show any potential errors with your files. For help with resolving problems, send your XML file and the error message to [Support](/contact/).

#### Uploading your files to Crossref<a id='00025' href='#00025'><i class='fas fa-link'></i></a>

Once you’re happy with your files, upload them to us using the [admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/), or submit them through [HTTPS POST](/documentation/member-setup/direct-deposit-xml/https-post/).

[Verify your registration](/documentation/content-registration/verify-your-registration/), check the [submission queue and log](/documentation/content-registration/verify-your-registration/submission-queue-and-log/), and [troubleshoot any errors](/documentation/content-registration/verify-your-registration/troubleshooting-submissions/).

Once your submission is successful, your grant DOIs are ‘live’ and ready to be used. It’s good practice to add the grant DOI to the landing page for the grant, as in this example for [https://doi.org/10.37717/220020589](https://doi.org/10.37717/220020589):

<figure><img src='/images/documentation/Example-grant-DOI.png' alt='Example grant DOI' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image104">Show image</button>
<div id="image104" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Example-grant-DOI.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Spread the word about your grant identifiers<a id='00036' href='#00036'><i class='fas fa-link'></i></a>

Let your grant submission systems, awardees, and other parties know you are supporting Crossref grant identifiers, and that they should start collecting these identifiers too. Crossref grant metadata (including grant DOIs) is made openly available through our APIs, so it can be used by third parties (including publishers, grant tracking systems) to link grants to related research outputs.