+++
title = "Introduction to pending publications"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "content-registration", "content-types-intro", "pending-publications",]
identifier = "documentation/content-registration/content-types-intro/pending-publications"
rank = 4
weight = 60508
aliases = [ "/education/content-registration/content-types-intro/pending-publications", "/education/content-registration/content-types-intro/pending-publications/", "/help/pending-publication/", "/help/pending-publication",]
x-version = "0.0.0"

+++

Pending publication is a way of creating a DOI and depositing metadata for a content item any time after a manuscript has been accepted but before it is published online. This is possible for all standard content types (such as articles, books, conference proceedings).

Because a pending publication has not yet been published, its DOI will resolve to a publicly-available Crossref-hosted landing page. Once the work is published online, this same DOI will resolve to the URL for that content.

The pending publication content type serves as a temporary placeholder for your content - like a "coming soon" or preview of the great work to come. For a pending publication, you register basic metadata for your content item before registering all the formal metadata that comes with a version of record. Take care not to share a DOI before it has been deposited with us, or it will not resolve for your readers, and will lead to a failed resolution in your [resolution report](/documentation/reports/resolution-report). Learn more about the [pending publication consultation](/blog/community-responses-to-our-proposal-for-early-content-registration/).

### Use cases for pending publication<a id='00103' href='#00103'><i class='fas fa-link'></i></a>

Before the pending publication content type existed, we recommended you to register DOIs at the time content was published online, or shortly after. As the communication needs of our members (researchers, funders, institutions, and publishers) evolve, we have created this new solution to aid you and your work, and allow you to register DOIs before content is published online. With pending publication:

* Members can:
    * address timing issues related to press embargos
    * publicly establish scholarly precedence for their articles
    * meet the conditions in full for new funder policies and mandates, which focus on acceptance as a key event to report on
    * ensure that institutional repositories use the DOI to link to the member-stewarded copy
* Researchers can provide formal evidence of all publications in employment and grant applications
* Funders can fully track all publications funded by their research grants
* Institutions can fully track the scholarly output of their faculty members
* Technology vendors that support scholarly research management can account for all outputs

### How does pending publication work?<a id='00104' href='#00104'><i class='fas fa-link'></i></a>

When registering your publication as *pending* there are two things you need to do:

1. Register a subset of the metadata (as a minimum: member name, journal title, and accepted date) under the Pending Publication content type.
After you do this, the DOI will resolve to a Crossref-hosted landing page displaying your logo, a banner showing the manuscript has been accepted for publication, and the metadata you’ve provided. As with all registered content, pending publication metadata will be publicly available in our APIs (and updated as you update your metadata records).
2. Once your work is published, you need to register the full metadata for the work - this is not an automatic process. You must [update](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/) the metadata for each pending publication DOI, so that each DOI will resolve directly to the content (and not the pending publication landing page).

### Pending publication workflow diagram<a id='00105' href='#00105'><i class='fas fa-link'></i></a>

Crossmark participants please note that you can deposit Crossmark metadata at any point, but during the Beta version of the pending publication rollout, the Crossmark badge will not be displayed to readers.

<figure><img src='/images/documentation/Pending-publication-workflow.png' alt='Pending publication workflow' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image5">Show image</button>
<div id="image5" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Pending-publication-workflow.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

### Fees for pending publications<a id='00111' href='#00111'><i class='fas fa-link'></i></a>

[Content Registration (metadata deposit) fees](/fees#content-registration-fees) still apply, but there are no additional fees for using pending publication.  So, you’ll be charged once when you register the pending publication, but any subsequent updates, including the update on publication, are not charged.

### History<a id='00112' href='#00112'><i class='fas fa-link'></i></a>

Pending publication has been supported since 2019 and was designed in response to community feedback:
* [Proposal PDF](/pdfs/pp-report.pdf)
* [Original proposal blog post](/blog/rfc-registering-content-before-online/)
* [Community responses to proposal blog post](/blog/community-responses-to-our-proposal-for-early-content-registration/)

## Key links
* How to register content using [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/)
* [Pending publication markup guide](/documentation/schema-library/markup-guide-record-types/pending-publications/)