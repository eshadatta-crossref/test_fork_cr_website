+++
title = "Affiliations and ROR"
date = "2022-08-19"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "markup-guide-metadata-segment", "ror",]
identifier = "documentation/schema-library/markup-guide-metadata-segment/ror"
rank = 4
weight = 60604
aliases = []
x-version = "0.0.0"

+++

This guide gives markup examples for members registering affiliations by direct deposit of XML. As of schema version 5.3.0 we’ve introduced a new <affiliations> tag that supports both affiliated institution names and select identifiers, [including ROR](/blog/some-rip-roring-news-for-affiliation-metadata/). This change is made across contributor metadata in all content types.

Affiliation metadata consists of a repeatable <institution> element that contains the following:

|Element| Description | Limits |
|-|-|-|
| institution | container for institution metadata | repeatable |
| institution_name | The full name of an institution | repeatable, either institution_name or institution_id required |
| institution_id and attribute(s): `@type` (values are `ror`, `wikidata`, `isni`) | Identifier for an institution or organization | ID | value must begin with https://
| institution_acronym | The acronym of an institution | optional|
| institution_place | The primary city location of an institution | 1 allowed, xsd:string|
| institution_department | The department within an institution | 1 allowed, xsd:string|

### Requirements:

* For each affiliation, you _must_ at minimum include an institution identifier (`institution_id`) or an institution name (`institution_name`).  A ROR ID is recommended as we plan to integrate [ROR](https://ror.org) data into our APIs in the future.
* You _should_ include an identifier wherever possible, to improve discovery, disambiguate, and make affiliations machine actionable.
* You _may_ include optional metadata including an acronym (institution_acronym), a place (institution_place), and a department (institution_department).  Most of this metadata is made redundant by identifiers, so include only if an identifier is not available or if the identifier is not sufficiently granular (as with departments).

### Institution identifiers
We currently support 3 institution identifiers: ROR, Wikidata, and ISNI. We do some basic validation for each identifier provided in your XML:

* ROR: must begin with `https://ror.org/`  (full regex used for validation is `https://ror\\.org/0[^ilo]{6}\\d{2}`)
* Wikidata: must begin with `https://www.wikidata.org/entity/`  (full regex used for validation is `https://www\\.wikidata\\.org/entity/([qQ]|[pP]|[lL])\\d+`)
* ISNI: must begin with `https://www.isni.org/isni` (full regex used for validation is `https://www\\.isni\\.org/isni/\\d{15}(x|[0-9]`)

### Affiliation examples

There are multiple ways to mark up an affiliation depending on what metadata is available -
a ROR ID may be provided on its own as it's all we need to identify an organization:

``` XML
<affiliations>
  <institution>
    <institution_id type="ror">https://ror.org/05gq02987</institution_id>
  </institution>
```


This example includes department information to supplement the ROR ID:

``` XML
<affiliations>
  <institution>
    <institution_id type="ror">https://ror.org/01bj3aw27</institution_id>
    <institution_department>Office of Environmental Management</institution_department>
  </institution>
</affiliations>
```


This affiliation does not have an identifier, so additional metadata is useful:

``` XML

<institution>
    <institution_name>Tinker Fan Club</institution_name>
    <institution_acronym>TinFC</institution_acronym>
    <institution_place>Boston, MA</institution_place>
    <institution_department>Office of Environmental Management</institution_department>
</institution>
```
As mentioned above a ROR identifier is preferred, but ISNI and Wikidata identifiers are also supported and will be passed on to our metadata users via our REST and  XML APIs.

### Crossref and JATS
Crossref affiliation metadata easily maps to [JATS](https://jats.nlm.nih.gov/) and the [JATS4R affiliation recommendations](https://jats4r.org/authors-and-affiliations/).   For example, a basic affiliation with name only is tagged in JATS as:

``` XML

<contrib-group>
<contrib contrib-type=”author”>
<name>
<surname initials=”M”>Mitchell</surname>
<given-names initials=”AP”>Aaron P.</given-names>
</name>
<aff>Carnegie Mellon University</aff>
</contrib>
</contrib-group>
```

and should be tagged for Crossref use as

``` XML
<affiliations>
 	<institution>
		<institution_name>Carnegie Mellon University</institution_name>
	</institution>
</affiliations>
```

This example contains a JATS-tagged affiliation with an institution ID:

``` XML
<aff id="aff1">
<label>a</label>
<institution-wrap>
<institution-id institution-id-type=”ror”>https://ror.org/03vek6s52
</institution-id>
<institution>Harvard University</institution></institution-wrap>
<institution-wrap>
<institution-id institution-id-type=”ror”>https://ror.org/000cs1t14
</institution-id>
<institution>Harvard NeuroDiscovery Center</institution>
</institution-wrap>
</aff>
```

and should be tagged for Crossref use as:

``` XML
<affiliations>
 	<institution>
		<institution_name>Harvard University</institution_name>
		<institution_id type=”ror”>https://ror.org/03vek6s52</institution_id>
	</institution>
	<institution>
		<institution_name>Harvard NeuroDiscovery Center</institution_name>
		<institution_id type=”ror”>https://ror.org/000cs1t14</institution_id>
	</institution>
</affiliations>
```

Samples of full XML files containing our new affiliation metadata are available on our [Example XML metadata](https://www.crossref.org/xml-samples/) page.