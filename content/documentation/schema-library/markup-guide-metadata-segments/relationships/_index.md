+++
title = "Relationships"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "content-registration", "markup-guide-metadata-segment", "relationships",]
identifier = "documentation/schema-library/markup-guide-metadata-segment/relationships"
rank = 4
weight = 60628
aliases = [ "/education/content-registration/structural-metadata/relationships", "/education/content-registration/structural-metadata/relationships/",]
x-version = "0.0.0"

+++

We maintain an expansive set of relationship types to support the various content items that a research object, like a journal article, might link to. For data and software, we ask you to provide the following information:

* identifier of the dataset/software
* identifier type: DOI, Accession, PURL, ARK, URI, Other (additional identifier types are also accepted beyond those used for data or software, including ARXIV, ECLI, Handle, ISSN, ISBN, PMID, PMCID, and UUID)
* relationship type: *isSupplementedBy* or *references* (use the former if it was generated as part of the research results)
* description of dataset or software

We and [DataCite](https://datacite.org) both use this kind of linking. Data repositories which register their content with DataCite follow the same process and apply the same metadata tags. This means that we achieve direct data interoperability with links in the reverse direction (data and software repositories to journal articles).

You can see illustrations and examples of this schema in our [data and software citation guide](/documentation/reference-linking/data-and-software-citation-deposit-guide).

### Declaring relationship types<a id='00041' href='#00041'><i class='fas fa-link'></i></a>

The possible relationship types between content items can be as varied as the items themselves. We use a controlled vocabulary to define these relationships, in order to construct an orderly mapped network of content.

This is achieved by (i) an implicit approach where the relation type is a function of a specific service and is declared in the structure of the deposited XML, and (ii) in an explicit approach where the relation type is selected as a value within the deposited metadata.

1. [Reference linking](/documentation/reference-linking/) and [Cited-by](/documentation/cited-by/): implicitly creates `cites` and `isCitedBy` relationships between a content item and the items in its bibliography
2. [Crossmark](/documentation/crossmark/): explicit creation of [update relations](https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#update) between an item and other items that materially affect it (for example, a retraction)
3. [Funding data](/documentation/funder-registry/): implicit creation of `isFundedBy` and `hasAward` relationships between an item and the funding source that supported the underlying research
4. Linked clinical trials: implicit creation of a `belongsTo` relationship between and item and a registered clinical trial
5. [Components](/documentation/content-registration/structural-metadata/components/): implicit creation of a `isChildOf` relationship between an item and its elemental parts that are assigned their own DOI (limited [parent relation typing](https://data.crossref.org/reports/help/schema_doc/4.4.2/schema_4_4_2.html#component))
6. General typed relations: explicitly typed relation between an item with a Crossref DOI and an item with one of several possible identifiers.

### Relationship types for associated research objects: intra-work (within a work)<a id='00042' href='#00042'><i class='fas fa-link'></i></a>

|Description|Reciprocal relationship types|
|--- |--- |
|Expression|<code>isExpressionOf, hasExpression</code>|
|Format|<code> isFormatOf, hasFormat </code>|
|Identical|<code>isIdenticalTo</code>|
|Manifestation|<code>isManifestationOf, hasManifestation</code>|
|Manuscript|<code>isManuscriptOf, hasManuscript</code>|
|Preprint|<code>isPreprintOf, hasPreprint</code>|
|Replacement|<code>isReplacedBy, Replaces|</code>
|Translation|<code>isTranslationOf, hasTranslation</code>|
|Variant|<code>isVariantFormOf, isOriginalFormOf</code>|
|Version|<code>isVersionOf, hasVersion</code>|

### Relationship types for associated research objects: inter-work (between works)<a id='00044' href='#00044'><i class='fas fa-link'></i></a>

|Description|Reciprocal relationship types|
|--- |--- |
|Basis|<code>isBasedOn, isBasisFor</code>|
|Comment|<code>isCommentOn, hasComment</code>|
|Continuation|<code>isContinuedBy, Continues</code>|
|Derivation|<code>isDerivedFrom, hasDerivation</code>|
|Documentation|<code>isDocumentedBy, Documents</code>|
|Funding |<code>finances, isFinancedBy|
|Part|<code>isPartOf, hasPart</code>|
|Peer review|<code>isReviewOf, hasReview</code>|
|References|<code>references, isReferencedBy</code>|
|Related material, such as a protocol|<code>isRelatedMaterial, hasRelatedMaterial</code>|
|Reply|<code>isReplyTo, hasReply</code>|
|Requirement|<code>requires, isRequiredBy</code>|
|Software compilation|<code>isCompiledBy, compiles</code>|
|Supplement, such as a dataset generated as part of research results|<code>isSupplementTo, isSupplementedBy</code>|</code>


### General typed relations<a id='00045' href='#00045'><i class='fas fa-link'></i></a>

This service allows for the creation of a [typed relationship](https://data.crossref.org/reports/help/schema_doc/4.4.2/relations_xsd.html#inter_work_relation) between an item with a Crossref DOI and another content item. The other item may be represented by another Crossref DOI, a DOI from some other Registration Agency, or an item not identified with a DOI. When DOIs are used, the deposit process will fail if the DOI does not exist. Non-DOI identifiers are not verified.

```
<xsd:attributeGroup name="relations_type.atts">
  <xsd:attribute name="identifier-type" use="required">
    <xsd:simpleType>
      <xsd:restriction base="xsd:string">
        <xsd:enumeration value="doi"/>
        <xsd:enumeration value="issn"/>
        <xsd:enumeration value="isbn"/>
        <xsd:enumeration value="uri"/>
        <xsd:enumeration value="pmid"/>
        <xsd:enumeration value="pmcid"/>
        <xsd:enumeration value="purl"/>
        <xsd:enumeration value="arxiv"/>
        <xsd:enumeration value="ark"/>
        <xsd:enumeration value="handle"/>
        <xsd:enumeration value="uuid"/>
        <xsd:enumeration value="ecli"/>
        <xsd:enumeration value="accession"/>
        <xsd:enumeration value="other"/>
      </xsd:restriction>
    </xsd:simpleType>
  </xsd:attribute>
```

When DOIs are used, a bidirectional relation is automatically created by us when a relation is created in the deposit of one item in a pair. The DOI with metadata creating the relation is said to be the *claimant*, the other item does not need to have its metadata directly contain the relationship.

### Example: translated article<a id='00047' href='#00047'><i class='fas fa-link'></i></a>

A single journal article is published in two languages with each being assigned its own DOI. In this  example, both are published in the same journal. The original language instance has metadata that contains no indication of the translation instance. The alternative language instance includes in its metadata a relation to the original language instance. Here is a screenshot of the relevant section in the code. Please refer to the code snippet below to see it in context.

<figure><img src='/images/documentation/Relations-translated-article.png' alt='Relation to the original language instance' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image2">Show image</button>
<div id="image2" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Relations-translated-article.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


```
<journal_article publication_type="full_text">
  <titles>
     <title>Um artigo na língua original, que passa a ser o inglês</title>
     <original_language_title language="en">An article in its original language which happens to be English</original_language_title>
   </titles>
   <contributors>
     <person_name sequence="first" contributor_role="author">
       <given_name>Daniel</given_name>
       <surname>Stepputtis</surname>
       <ORCID authenticated="true">http://orcid.org/0000-0003-4824-1631</ORCID>
     </person_name>
  </contributors>
  <publication_date media_type="online">
     <month>02</month>
     <day>28</day>
     <year>2013</year>
  </publication_date>
  <program xmlns="https://www.crossref.org/relations.xsd">
     <related_item>
       <description>Portuguese translation of an article</description>
       <intra_work_relation relationship-type="isTranslationOf" identifier-type="doi">10.5555/original_language</intra_work_relation>
     </related_item>
  </program>
  <doi_data>
     <doi>10.5555/translation</doi>
     <resource>https://www.crossref.org/</resource>
  </doi_data>
</journal_article>
```

### Example: book review<a id='00048' href='#00048'><i class='fas fa-link'></i></a>

This example has a book review published as an article in the journal The Holocene. The article's title, taken from the publisher's site is "Book Review: Understanding the Earth system: compartments, processes and interactions" where this book has the DOI [https://doi.org/10.1007/978-3-642-56843-5](https://doi.org/10.1007/978-3-642-56843-5).

#### A: The current metadata for the review article gives no indication of the actual book being reviewed:

```
<journal>
    <journal_metadata language="en">
        <full_title>The Holocene</full_title>
        <abbrev_title>The Holocene</abbrev_title>
        <issn media_type="print">0959-6836</issn>
        <issn media_type="electronic">1477-0911</issn>
    </journal_metadata>
    <journal_issue>
        <publication_date media_type="online">
            <month>07</month>
            <day>27</day>
            <year>2016</year>
        </publication_date>
        <publication_date media_type="print">
            <month>05</month>
            <year>2002</year>
        </publication_date>
        <journal_volume>
            <volume>12</volume>
        </journal_volume>
        <issue>4</issue>
    </journal_issue>
    <journal_article publication_type="full_text">
        <titles>
            <title>Book Review: Understanding the Earth system: compartments, processes and interactions</title>
        </titles>
        <contributors>
            <person_name sequence="first" contributor_role="author">
                <given_name>Ian</given_name>
                <surname>Fairchild</surname>
                <affiliation>Keele University</affiliation>
            </person_name>
        </contributors>
        <publication_date media_type="online">
            <month>07</month>
            <day>27</day>
            <year>2016</year>
        </publication_date>
        <publication_date media_type="print">
            <month>05</month>
            <year>2002</year>
        </publication_date>
        <pages>
            <first_page>505</first_page>
            <last_page>505</last_page>
        </pages>
        <publisher_item>
            <identifier id_type="doi">10.1191/0959683602hl565xx</identifier>
        </publisher_item>
        <ai:program xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" name="AccessIndicators">
            <ai:license_ref applies_to="tdm">http://journals.sagepub.com/page/policies/text-and-data-mining-license</ai:license_ref>
        </ai:program>
        <doi_data>
            <doi>10.1191/0959683602hl565xx</doi>
```

#### B: Modifications to the review's metadata show how it would include a relationship to the book

Here is a screenshot of the relevant section in the code. Please refer to the code snippet below to see it in context.

<figure><img src='/images/documentation/Relations-inter-work-relation.png' alt='inter_work_relation relationship-type' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image3">Show image</button>
<div id="image3" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Relations-inter-work-relation.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


```
<journal>
    <journal_metadata language="en">
        <full_title>The Holocene</full_title>
        <abbrev_title>The Holocene</abbrev_title>
        <issn media_type="print">0959-6836</issn>
        <issn media_type="electronic">1477-0911</issn>
    </journal_metadata>
    <journal_issue>
        <publication_date media_type="online">
            <month>07</month>
            <day>27</day>
            <year>2016</year>
        </publication_date>
        <publication_date media_type="print">
            <month>05</month>
            <year>2002</year>
        </publication_date>
        <journal_volume>
            <volume>12</volume>
        </journal_volume>
        <issue>4</issue>
    </journal_issue>
    <journal_article publication_type="full_text">
        <titles>
            <title>Book Review: Understanding the Earth system: compartments, processes and interactions</title>
        </titles>
        <contributors>
            <person_name sequence="first" contributor_role="author">
                <given_name>Ian</given_name>
                <surname>Fairchild</surname>
                <affiliation>Keele University</affiliation>
            </person_name>
        </contributors>
        <publication_date media_type="online">
            <month>07</month>
            <day>27</day>
            <year>2016</year>
        </publication_date>
        <publication_date media_type="print">
            <month>05</month>
            <year>2002</year>
        </publication_date>
        <pages>
            <first_page>505</first_page>
            <last_page>505</last_page>
        </pages>
        <publisher_item>
            <identifier id_type="doi">10.1191/0959683602hl565xx</identifier>
        </publisher_item>
        <program xmlns="http://www.crossref.org/relations.xsd">
            <related_item>
                <inter_work_relation relationship-type="isReviewOf" identifier-type="doi">
                    10.1007/978-3-642-56843-5
                </inter_work_relation>
            </related_item>
        </program>
        <doi_data>
            <doi>10.1191/0959683602hl565xx</doi>
```

#### C: Meanwhile, the book's deposited metadata shows no indication of the relation to the review article:

```
<book book_type="other">
    <book_metadata language="en">
        <contributors>
            <person_name contributor_role="editor" sequence="first">
                <given_name>Eckart</given_name>
                <surname>Ehlers</surname>
            </person_name>
            <person_name contributor_role="editor" sequence="additional">
                <given_name>Thomas</given_name>
                <surname>Krafft</surname>
            </person_name>
        </contributors>
        <titles>
            <title>Understanding the Earth System</title>
        </titles>
        <publication_date media_type="print">
            <year>2001</year>
        </publication_date>
        <isbn media_type="print">978-3-540-67515-0</isbn>
        <isbn media_type="electronic">978-3-642-56843-5</isbn>
        <publisher>
            <publisher_name>Springer Berlin Heidelberg</publisher_name>
            <publisher_place>Berlin, Heidelberg</publisher_place>
        </publisher>
        <ai:program xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" name="AccessIndicators">
            <ai:license_ref applies_to="tdm">http://www.springer.com/tdm</ai:license_ref>
        </ai:program>
        <doi_data>
            <doi>10.1007/978-3-642-56843-5</doi>
```
#### D: Book DOI's metadata showing the relationship

Here is a screenshot of the relevant section in the code. Please refer to the code snippet below to see it in context.

<figure><img src='/images/documentation/Relations-review-relationship.png' alt='' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image4">Show image</button>
<div id="image4" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Relations-review-relationship.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


```
<query status="resolved">
     <doi type="journal_article">10.7554/eLife.42135</doi>
     <crm-item name="publisher-name" type="string">eLife Sciences Publications, Ltd</crm-item>
     <crm-item name="prefix-name" type="string">eLife Sciences Publications, Ltd.</crm-item>
     <crm-item name="member-id" type="number">4374</crm-item>
     <crm-item name="citation-id" type="number">104997326</crm-item>
     <crm-item name="journal-id" type="number">189365</crm-item>
     <crm-item name="deposit-timestamp" type="number">20190402090010</crm-item>
     <crm-item name="owner-prefix" type="string">10.7554</crm-item>
     <crm-item name="last-update" type="date">2019-04-02T09:00:31Z</crm-item>
     <crm-item name="created" type="date">2019-02-25T13:00:23Z</crm-item>
     <crm-item name="citedby-count" type="number">3</crm-item>
     <crm-item name="relation" type="doi" claim="isPreprintOf">10.1101/425587</crm-item>
     <crm-item name="relation" type="doi" claim="isReviewOf">10.3410/f.735157928.793558703</crm-item>
     <doi_record>
       <crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
         <journal>
           <journal_metadata language="en">
             <full_title>eLife</full_title>
```

### Example: linked dataset<a id='00049' href='#00049'><i class='fas fa-link'></i></a>

An article with a Crossref DOI identifies that data represented by a DataCite DOI was used in the research and was mentioned in the article's acknowledgment section.

The [article's Crossref deposited XML](https://api.crossref.org/works/10.5555/12345681/transform/application/vnd.crossref.unixsd+xml):

```
<doi_record>
<crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
<journal>
<journal_metadata language="en">
<full_title>Journal of Psychoceramics</full_title>
<abbrev_title>Journal of Psychoceramics</abbrev_title>
<issn media_type="electronic">0264-3561</issn>
</journal_metadata>
<journal_issue>
<publication_date media_type="online">
<month>05</month>
<day>06</day>
<year>2012</year>
</publication_date>
<publication_date media_type="print">
<month>05</month>
<day>06</day>
<year>2012</year>
</publication_date>
<journal_volume>
<volume>5</volume>
</journal_volume>
<issue>11</issue>
</journal_issue>
<journal_article publication_type="full_text">
<titles>
<title>
Dog: A Methodology for the Development of Simulated Annealing
</title>
</titles>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Josiah</given_name>
<surname>Carberry</surname>
</person_name>
</contributors>
<publication_date media_type="online">
<month>05</month>
<day>06</day>
<year>2012</year>
</publication_date>
<publication_date media_type="print">
<month>05</month>
<day>06</day>
<year>2012</year>
</publication_date>
<pages>
<first_page>1</first_page>
<last_page>3</last_page>
</pages>
<crossmark>
<crossmark_policy>10.5555/crossmark_policy</crossmark_policy>
<crossmark_domains>
<crossmark_domain>
<domain>psychoceramics.labs.crossref.org</domain>
</crossmark_domain>
</crossmark_domains>
<crossmark_domain_exclusive>false</crossmark_domain_exclusive>
<updates>
<update type="correction" date="2012-05-12">10.5555/12345681</update>
</updates>
<custom_metadata/>
</crossmark>
<program xmlns="http://www.crossref.org/relations.xsd">
<related_item>
<description>Acknowledgement mention of dataset use.</description>
<inter_work_relation relationship-type="isBasedOn" identifier-type="doi">10.5284/1000389</inter_work_relation>
</related_item>
</program>
<doi_data>
<doi>10.5555/12345681</doi>
<timestamp>201601211508</timestamp>
<resource>
http://psychoceramics.labs.crossref.org/10.5555-12345681.html
</resource>
</doi_data>
</journal_article>
</journal>
</crossref>
</doi_record>
</query>
</body>
```

You can also see the [article's deposited metadata in JSON](https://api.crossref.org/works/10.5555/12345681).