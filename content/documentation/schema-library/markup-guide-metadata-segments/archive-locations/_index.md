+++
title = "Archive locations"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "markup-guide-metadata-segment", "archive-locations",]
identifier = "documentation/schema-library/markup-guide-metadata-segment/archive-locations"
rank = 4
weight = 60606
aliases = [ "/education/content-registration/administrative-metadata/archive-locations", "/education/content-registration/administrative-metadata/archive-locations/", "/documentation/content-registration/administrative-metadata/archive-locations/", "/documentation/content-registration/administrative-metadata/archive-locations",]
x-version = "0.0.0"

+++

Digital preservation is a combination of policies, strategies, and actions that ensure persistent access to digital content over time. It includes archiving arrangements. The [Digital Preservation Coalition](https://www.dpconline.org/)’s [Digital Preservation Handbook](https://www.dpconline.org/handbook/introduction/how-to-use-the-handbook) gives a good introduction to practicalities and best practices in archiving arrangements.

Under our [member obligations](/membership/terms/), you are asked to make best efforts to have your content archived by an archiving organization, and you are encouraged to include information about your designated archive in your metadata. This helps us work with archives to ensure your DOIs continue to resolve to your content, even if your organization ceases.

The archives currently listed in our deposit schema section are:

* [CLOCKSS](https://clockss.org/)
* [Deep Web Technologies (DWT)](https://www.deepwebtech.com)
* [Internet Archive](https://archive.org/index.php)
* [Koninklijke Bibliotheek (KB)](https://www.kb.nl/)
* [LOCKSS](https://www.lockss.org/)
* [Portico](https://www.portico.org/)

Another archiving service is [PKP Preservation Network (PKP PN)](https://pkp.sfu.ca/pkp-pn/).

There's a useful list of archive providers on the [Keepers Registry](https://keepers.issn.org/keepers).

Please [contact us](/contact/) if you have archiving arrangements with an organization that is not listed.

To include archiving metadata, insert the relevant archive information into your metadata above the `doi_data` section, for example:

``` XML
<archive_locations>
    <archive name="CLOCKSS"/>
    <archive name="Internet Archive"/>
    <archive name="Portico"/>
    <archive name="KB"/>
</archive_locations>
<doi_data>
    <doi>10.32013/12345678</doi>
    <resource>https://www.crossref.org/xml-samples/
</doi_data>
```