+++
title = "Markup guides for metadata segments"
date = "2023-02-23"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "markup-guide-metadata-segments",]
identifier = "documentation/schema-library/markup-guide-metadata-segments"
rank = 4
weight = 60402
aliases = [ "/education/content-registration/content-type-markup-guide", "/education/content-registration/content-type-markup-guide/", "/education/content-registration/recommendations-by-content-type/", "/education/content-registration/recommendations-by-content-type", "/documentation/content-registration/content-type-markup-guide", "/documentation/content-registration/content-type-markup-guide/",]
x-version = "0.0.0"

+++

For a deeper dive into each metadata segment, an introduction and markup guide for each are below:

* [Abstracts](/documentation/schema-library/markup-guide-metadata-segments/abstracts)
* [Affiliations and ROR](/documentation/schema-library/markup-guide-metadata-segments/affiliations)
* [Archive locations](/documentation/schema-library/markup-guide-metadata-segments/archive-locations)
* [Article IDs](/documentation/schema-library/markup-guide-metadata-segments/article-ids)
* [Contributors](/documentation/schema-library/markup-guide-metadata-segments/contributors)
* [Face markup within titles](/documentation/schema-library/markup-guide-metadata-segments/face-markup)
* [Full-text URLs](/documentation/schema-library/markup-guide-metadata-segments/full-text-urls)
* [Funding information](/documentation/schema-library/markup-guide-metadata-segments/funding-information)
* [ISSN and ISBN](/documentation/schema-library/markup-guide-metadata-segments/issn-isbn)
* [License Information](/documentation/schema-library/markup-guide-metadata-segments/license-information)
* [MathML](/documentation/schema-library/markup-guide-metadata-segments/mathml)
* [Multi-language content and translations](/documentation/schema-library/markup-guide-metadata-segments/multi-language)
* [References](/documentation/schema-library/markup-guide-metadata-segments/references)
* [Relationships](/documentation/schema-library/markup-guide-metadata-segments/relationships)
* [Titles](/documentation/schema-library/markup-guide-metadata-segments/titles)

Each record model also has its own [markup guides](/documentation/schema-library/markup-guide-record-types).