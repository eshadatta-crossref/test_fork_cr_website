+++
title = "Pending publications markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "markup-guide-record-types", "pending-publications",]
identifier = "documentation/schema-library/markup-guide-record-types/pending-publications"
rank = 4
weight = 60622
aliases = [ "/education/content-registration/content-type-markup-guide/pending-publications", "/education/content-registration/content-type-markup-guide/pending-publications/", "/education/content-registration/recommendations-by-content-type/pending-publications/", "/education/content-registration/recommendations-by-content-type/pending-publications", "/documentation/content-registration/content-type-markup-guide/pending-publications/", "/documentation/content-registration/content-type-markup-guide/pending-publications",]
x-version = "0.0.0"

+++

This guide gives markup examples for members registering pending publications by direct deposit of XML. It is not currently possible to register the pending publications content type using one of our helper tools.

## How to make changes to records<a id='00557' href='#00557'><i class='fas fa-link'></i></a>

### How to update records - for Crossmark users<a id='00106' href='#00106'><i class='fas fa-link'></i></a>

Crossmark service users can add the withdrawal to the Crossmark metadata as a scholarly update assertion (using the update type *withdrawal*). Update the landing page by updating the metadata record you provide us, using the same DOI in the assertion. See [example of a full deposit](/documentation/schema-library/markup-guide-record-types/pending-publications/#00113) for an XML example. If you choose, you can publish a separate update, and then link to the new DOI in the Crossmark metadata.

The green banner saying *Manuscript has been accepted* will change to a red banner saying *Accepted manuscript has been withdrawn*. See the following [withdrawn pending publication example landing page](https://apps.crossref.org/pendingpub/pendingpub.html?doi=10.5555%2Fcrossmark-withdrawal-test-for-pendingpub) with its [associated metadata deposit](/documentation/schema-library/markup-guide-record-types/pending-publications/).

### How to update records - for non Crossmark users<a id='00107' href='#00107'><i class='fas fa-link'></i></a>

Our system will not be able to identify your pending publication as *withdrawn*, which means the green *Manuscript has been accepted* banner will remain on the landing page. Therefore it is critical that you write a clear statement about the withdrawal in the *Intent to Publish* statement. The *Intent to Publish* statement is supplied in the XML element - see the default *Intent to publish statement* message below.

## Customizations<a id='00108' href='#00108'><i class='fas fa-link'></i></a>

You can personalize the display of the Crossref-hosted landing page with the following information:

* member/society/journal logo
* custom wording for the *intent to publish* statement
* display of all provided optional extra metadata such as article title, funder identifiers, ORCID iDs, license information
* Crossmark to handle the rare occasions when a member rescinds acceptance.

If you’d like to display a custom logo on your pending publication landing page, please [email us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) the logo, and include your member name, prefix, and a note indicating that the logo is to be used for pending publication. We accept both JPEG and PNG files (dimensions should be 112px by 112px).

### *Intent to publish* statement<a id='00109' href='#00109'><i class='fas fa-link'></i></a>

This is the default *intent to publish* statement shown on the landing page:

>This paper has been accepted for publication so its publisher has pre-registered a Crossref DOI. This persistent identifier and link [DOI INSERTED HERE] can already be shared by authors and readers, as it will redirect to the published article when available.

We encourage you to provide your own custom statement in the metadata which will replace the default statement.

The *intent to publish* statement can be used to convey any information you’d like to share about the forthcoming publication event (such as process, timeline). In the event that you delay publication or withdraw the publication, this statement may be very informative for your community - both the readers on your platform as well as the systems that consume Crossref metadata.

## Metadata reference examples for pending publication<a id='00110' href='#00110'><i class='fas fa-link'></i></a>

Here are three examples of pending publication records. The *full* record contains the full range of metadata accepted for pending publication. The *basic* record has the bare minimum metadata required. The *withdrawn* record is for a pending publication that the member has decided not to publish.

|DOI|Description|Links|
|--- |--- |--- |
|[Full](https://apps.crossref.org/pendingpub/pendingpub.html?doi=10.5555%2Fpending-publication-multi-author-funder-test)|This record contains the full range of metadata accepted for a pending publication.|[Deposit XML](/documentation/schema-library/markup-guide-record-types/pending-publications/#00113), [XML API](https://doi.crossref.org/search/doi?pid=jlin@crossref.org&format=unixsd&doi=10.5555/pending-publication-multi-author-funder-test)|
|[Basic](https://apps.crossref.org/pendingpub/pendingpub.html?doi=10.5555%2Fpending-publication-with-multiple-authors)|This basic record contains only the required metadata for a pending publication. We apply the default _Intent to Publish_ statement in the absence of a custom message. Here, some authors have ORCID iDs with affiliation data; others do not.|[Deposit XML](/documentation/schema-library/markup-guide-record-types/pending-publications/#00114), [XML API](https://doi.crossref.org/search/doi?pid=jlin@crossref.org&format=unixsd&doi=10.5555/pending-publication-with-multiple-authors)|
|[Withdrawn](https://apps.crossref.org/pendingpub/pendingpub.html?doi=10.5555%2Fcrossmark-withdrawal-test-for-pendingpub)|In this record of a withdrawn pending publication, the member has registered a pending publication and then updated the record to reflect the withdrawal. This example is an in situ scholarly update with no separate update published.|[Deposit XML](/documentation/schema-library/markup-guide-record-types/pending-publications/#00115), [XML API](https://doi.crossref.org/search/doi?pid=jlin@crossref.org&format=unixsd&doi=10.5555/crossmark-withdrawal-test-for-pendingpub)|

## Example of a full deposit<a id='00113' href='#00113'><i class='fas fa-link'></i></a>

``` XML
<doi_batch xmlns= "[https://www.crossref.org/schema/4.4.2"](http://www.crossref.org/schema/4.4.2")
xmlns:xsi="[http://www.w3.org/2001/XMLSchema-instance"](http://www.w3.org/2001/XMLSchema-instance")
xmlns:fr="[https://www.crossref.org/fundref.xsd"](http://www.crossref.org/fundref.xsd")
xsi:schemaLocation=&quot;[https://www.crossref.org/schema/4.4.2https://www.crossref.org/depositSchema/crossref4.4.2.xsd"](http://www.crossref.org/schema/4.4.2http://www.crossref.org/depositSchema/crossref4.4.2.xsd")
version="4.4.2"
xmlns:ai="[https://www.crossref.org/AccessIndicators.xsd">](http://www.crossref.org/AccessIndicators.xsd">)
<head>
<doi_batch_id>org.crossref.early.001</doi_batch_id>
<timestamp>000001</timestamp>
<depositor>
<depositor_name>Crossref</depositor_name>
<email_address>jhanna@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>
<body>
<pending_publication>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Josiah</given_name>
<surname>Carberry</surname>
<ORCID>https://orcid.org/0000-0002-1825-0097</ORCID>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Megan</given_name>
<surname>Strongjackplum</surname>
<affiliation>University of Los Angeles</affiliation>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Sonder</given_name>
<surname>Meander</surname>
</person_name>
<person_name sequence="additional" contributor_role="editor">
<given_name>Matt</given_name>
<surname>Techthespian</surname>
<affiliation>New York Institute of Technology</affiliation>
</person_name>
</contributors>
<publication>
<full_title>Journal of Psychoceramics</full_title>
<doi>10.5555/1234567890</doi>
</publication>
<titles>
<title>Processing Fragmented Postmodernity: Cake for the Disenchanted</title>
</titles>
<acceptance_date>
<month>07</month>
<day>27</day>
<year>2018</year>
</acceptance_date>
<intent_statement>  
This article has been peer reviewed and accepted for publication by Crossref University Press. It is slated to publish on Dec 11.</intent_statement>
<fr:program xmlns:fr="[https://www.crossref.org/fundref.xsd"](http://www.crossref.org/fundref.xsd") name="fundref">
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">
National Science Foundation
<fr:assertion name="funder_identifier">[https://doi.org/10.13039/100000001</fr:assertion>](https://doi.org/10.13039/100000001</fr:assertion>)
</fr:assertion>
<fr:assertion name="award_number">CNS-1228930</fr:assertion>
<fr:assertion name="award_number">CNS-2345567</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">
Eva Crane Trust
<fr:assertion name="funder_identifier">10.13039/100012660</fr:assertion>
</fr:assertion>
<fr:assertion name="award_number">ECTA20160303</fr:assertion>
<fr:assertion name="award_number">ECTA30498</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">Financial Authority</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">Federal Wilderness Commission</fr:assertion>
</fr:assertion>
<fr:assertion name="fundgroup">
<fr:assertion name="funder_name">Foundation of Minor Health</fr:assertion>
<fr:assertion name="award_number">MED00001234</fr:assertion>
</fr:assertion>
</fr:program>
<ai:program name="AccessIndicators">
<ai:license_ref applies_to="vor" start_date="2018-12-11">[https://creativecommons.org/licenses/by/4.0/</ai:license_ref>](https://creativecommons.org/licenses/by/4.0/</ai:license_ref>)
</ai:program>
<doi>10.5555/pending-publication-multi-author-funder-test</doi>
</pending_publication>
</body>
</doi_batch>
```

## Example of a basic deposit<a id='00114' href='#00114'><i class='fas fa-link'></i></a>

``` XML
<doi_batch xmlns="[https://www.crossref.org/schema/4.4.2"](http://www.crossref.org/schema/4.4.2") xmlns:xsi="[http://www.w3.org/2001/XMLSchema-instance"](http://www.w3.org/2001/XMLSchema-instance") xmlns:fr="[https://www.crossref.org/fundref.xsd"](http://www.crossref.org/fundref.xsd") xsi:schemaLocation="[https://www.crossref.org/schema/4.4.2](http://www.crossref.org/schema/4.4.2)[ https://www.crossref.org/depositSchema/crossref4.4.2.xsd"](http://www.crossref.org/depositSchema/crossref4.4.2.xsd") version="4.4.2">
<head>
<doi_batch_id>org.crossref.early.001</doi_batch_id>
<timestamp>000002</timestamp>
<depositor>
<depositor_name>Crossref</depositor_name>
<email_address>jhanna@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>
<body>
<pending_publication>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Josiah</given_name>
<surname>Carberry</surname>
<ORCID>https://orcid.org/0000-0002-1825-0097</ORCID>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Fran</given_name>
<surname>Whitmorsup</surname>
<affiliation>University of Los Angeles</affiliation>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Sandra</given_name>
<surname>Questcheck</surname>
</person_name>
</contributors>
<publication>
<full_title>Journal of Psychoceramics</full_title>
<doi>10.5555/1234567890</doi>
</publication>
<titles>
<title>Theorizing Interfering Appropriation: Headphones for All Ears</title>
</titles>
<acceptance_date>
<month>07</month>
<day>28</day>
<year>2018</year>
</acceptance_date>
<doi>10.5555/pending-publication-multi-author-crossmark-test</doi>
</pending_publication>
</body>
</doi_batch>
```

## Example of a withdrawal<a id='00115' href='#00115'><i class='fas fa-link'></i></a>

``` XML
<doi_batch xmlns="[https://www.crossref.org/schema/4.4.2"](http://www.crossref.org/schema/4.4.2") xmlns:xsi="[http://www.w3.org/2001/XMLSchema-instance"](http://www.w3.org/2001/XMLSchema-instance") xmlns:fr="[https://www.crossref.org/fundref.xsd"](http://www.crossref.org/fundref.xsd") xsi:schemaLocation="[https://www.crossref.org/schema/4.4.2](http://www.crossref.org/schema/4.4.2)[ https://www.crossref.org/depositSchema/crossref4.4.2.xsd"](http://www.crossref.org/depositSchema/crossref4.4.2.xsd") version="4.4.2">
<head>
<doi_batch_id>org.crossref.early.001</doi_batch_id>
<timestamp>000001</timestamp>
<depositor>
<depositor_name>Crossref</depositor_name>
<email_address>jhanna@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>
<body>
<pending_publication>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Josiah</given_name>
<surname>Carberry</surname>
<ORCID>https://orcid.org/0000-0002-1825-0097</ORCID>
</person_name>
</contributors>
<publication>
<full_title>Journal of Psychoceramics</full_title>
<doi>10.5555/1234567890</doi>
</publication>
<titles>
<title>Categorizing Belligerent Violence: Rockets and/in the Other
</title>
</titles>
<acceptance_date>
<month>07</month>
<day>28</day>
<year>2018</year>
</acceptance_date>
<intent_statement>The article has been withdrawn according to the Crossref Policy on Article in Press Withdrawal.</intent_statement>
<crossmark>
<crossmark_version>1</crossmark_version>
<crossmark_policy>10.5555/crossmark_policy</crossmark_policy>
<crossmark_domains>
<crossmark_domain>
<domain>psychoceramics.labs.crossref.org</domain>
</crossmark_domain>
</crossmark_domains>
<crossmark_domain_exclusive>true</crossmark_domain_exclusive>
<updates>
<update type="withdrawal" date="2018-07-28">10.5555/crossmark-withdrawal-test-for-pendingpub</update>
</updates>
</crossmark>
<doi>10.5555/crossmark-withdrawal-test-for-pendingpub</doi>
</pending_publication>
</body>
</doi_batch>
```