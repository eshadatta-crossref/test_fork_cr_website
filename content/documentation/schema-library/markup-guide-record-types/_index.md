+++
title = "Markup guides for record types"
date = "2022-05-20"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "markup-guide-record-types",]
identifier = "documentation/schema-library/markup-guide-record-types"
rank = 4
weight = 60402
aliases = [ "/education/content-registration/content-type-markup-guide", "/education/content-registration/content-type-markup-guide/", "/education/content-registration/recommendations-by-content-type/", "/education/content-registration/recommendations-by-content-type", "/documentation/content-registration/content-type-markup-guide", "/documentation/content-registration/content-type-markup-guide/",]
x-version = "0.0.0"

+++

For a deeper dive into each record model, an introduction and markup guide for each are below:

* [Books and chapters markup guide](/documentation/schema-library/markup-guide-record-types/books-and-chapters/)
* [Components markup guide](/documentation/schema-library/markup-guide-record-types/components/)
* [Conference proceedings markup guide](/documentation/schema-library/markup-guide-record-types/conference-proceedings/)
* [Datasets markup guide](/documentation/schema-library/markup-guide-record-types/datasets/)
* [Dissertations markup guide](/documentation/schema-library/markup-guide-record-types/dissertations/)
* [Grants markup guide](/documentation/schema-library/markup-guide-record-types/grants/)
* [Journals and articles markup guide](/documentation/schema-library/markup-guide-record-types/journals-and-articles/)
* [Peer reviews markup guide](/documentation/schema-library/markup-guide-record-types/peer-reviews/)
* [Pending publications markup guide](/documentation/schema-library/markup-guide-record-types/pending-publications/)
* [Posted content (includes preprints) markup guide](/documentation/schema-library/markup-guide-record-types/posted-content-includes-preprints/)
* [Reports and working papers markup guide](/documentation/schema-library/markup-guide-record-types/reports-and-working-papers/)
* [Standards markup guide](/documentation/schema-library/markup-guide-record-types/standards/)

Many segments of metadata are repeatable across record models and have their own [markup guides](/documentation/schema-library/markup-guide-metadata-segments).