+++
title = "Resource-only deposit schema 4.4.2"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "resource-only-deposit-schema-4-4-2",]
identifier = "documentation/schema-library/resource-only-deposit-schema-4-4-2"
rank = 4
weight = 60408
aliases = [ "/education/content-registration/metadata-deposit-schema/resource-deposit-schema-4-4-2/", "/education/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-4-2", "/education/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-4-2/", "/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-4-2/", "/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-4-2",]
x-version = "0.0.0"

+++

Schema: [doi_resources4.4.2.xsd](https://data.crossref.org/schemas/doi_resources4.4.2.xsd)
Full documentation: [doi_resource4.4.2](https://data.crossref.org/reports/help/schema_doc/doi_resources4.4.2/index.html)

doi_resources4.4.2.xsd is included in [bundle 0.1.0](https://gitlab.com/crossref/schema/-/releases) and imports

* [common4.4.2.xsd](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](http://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)