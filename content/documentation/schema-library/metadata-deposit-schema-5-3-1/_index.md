+++
title = "Metadata deposit schema 5.3.1"
date = "2021-10-05"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "schema-library", "metadata-deposit-schema-5-3-1",]
identifier = "documentation/schema-library/metadata-deposit-schema-5-3-1"
rank = 4
weight = 60405
x-version = "0.0.0"

+++

Beginning with deposit schema version `4.4.2`, all Crossref schema releases are available in our GitLab schema repository as a bundle. [Bundle 0.3.1](https://gitlab.com/crossref/schema/-/releases/0.3.1) contains schema version `5.3.1` and associated files.

Schema: [crossref5.3.1.xsd](https://data.crossref.org/schemas/crossref5.3.1.xsd)
Full documentation: [5.3.1](https://data.crossref.org/reports/help/schema_doc/5.3.1/index.html)


Crossref included schema:

* [common5.3.1.xsd](https://data.crossref.org/schemas/common5.3.1.xsd)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)

External imported schema:

* [MathML](http://www.w3.org/Math/XMLSchema/mathml3/mathml3.xsd)
* [JATS](https://jats.nlm.nih.gov/publishing/1.2/xsd/JATS-journalpublishing1.xsd)

Changes from `4.8.1`

* replace `<affiliation>` tag with `<affiliations>` tag to support new affiliations structure
* add `<institution_id>` element to support ROR and other org IDs
* make either `<institution_id>` or `<institution_name>` required within institution metadata
* relax regex rules for `<given_name>` element