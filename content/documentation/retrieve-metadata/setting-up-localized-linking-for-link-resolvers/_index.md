+++
title = "Setting up localized linking for link resolvers"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "setting-up-localized-linking-for-link-resolvers",]
identifier = "documentation/retrieve-metadata/setting-up-localized-linking-for-link-resolvers"
rank = 4
weight = 30800
aliases = [ "/education/retrieve-metadata/setting-up-localized-linking-for-link-resolvers", "/education/retrieve-metadata/setting-up-localized-linking-for-link-resolvers/",]
x-version = "0.0.0"

+++

To take advantage of localized linking, your localized linking server must have a recognized BASE-URL. BASE-URLs are collected from library metadata users when they sign up with Crossref, and forwarded to the [IDF](http://www.doi.org/) for registration.

Once the BASE-URL is registered, go to

```
http://www.doi.org/cgi-bin/pushcookie.cgi?BASE-URL={YOUR-BASE-URL}
```

to download a cookie, which contains the URL for your local content server. This cookie makes your browser OpenURL-enabled, which means that we will redirect relevant requests to your local resolver.

Learn more about [BASE-URL and the cookie pusher script](https://www.exlibrisgroup.com/products/primo-library-discovery/sfx-link-resolver/).

If you upgrade or change your local link resolver, please [send us your new BASE-URL](/contact/).