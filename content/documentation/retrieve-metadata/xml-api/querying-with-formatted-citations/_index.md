+++
title = "Querying with formatted citations"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-api", "querying-with-formatted-citations",]
identifier = "documentation/retrieve-metadata/xml-api/querying-with-formatted-citations"
rank = 4
weight = 31013
aliases = [ "/education/retrieve-metadata/xml-api/querying-with-formatted-citations", "/education/retrieve-metadata/xml-api/querying-with-formatted-citations/",]
x-version = "0.0.0"

+++

Formatted citations are sometimes referred to as *plain text references*. For example, an APA-formatted citation for the DOI [https://doi.org/10.1038/nclimate1398](https://doi.org/10.1038/nclimate1398) is:

> Hungate, B. A., and Hampton, H. M. (2012). Ecosystem services: Valuing ecosystems for climate. Nature Climate Change, 2(3), 151-152. https://doi.org/10.1038/nclimate1398

Crossref has two interfaces that can be used for making formatted citation queries:

1. [Simple Text Query](/documentation/retrieve-metadata/simple-text-query): a tool which allows a user to cut-and-paste references into an interactive form and receive instant results, or upload a text file and receive HTML format results via email.
2. [XML API](/documentation/retrieve-metadata/xml-api/): supports XML-formatted queries via [HTTPS POST](/documentation/member-setup/direct-deposit-xml/https-post/) and [GET](/documentation/retrieve-metadata/xml-api/using-https-to-query).