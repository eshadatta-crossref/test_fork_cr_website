+++
title = "Querying with special characters"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-api", "querying-with-special-characters",]
identifier = "documentation/retrieve-metadata/xml-api/querying-with-special-characters"
rank = 4
weight = 31014
aliases = [ "/education/retrieve-metadata/xml-api/querying-with-special-characters", "/education/retrieve-metadata/xml-api/querying-with-special-characters/",]
x-version = "0.0.0"

+++

Crossref performs some fuzzy matching, but for best results, queries for metadata which includes special characters should also include special characters.

For example, DOI [https://doi.org/10.1260/095830506778119452](https://doi.org/10.1260/095830506778119452) was deposited with the metadata shown below. Notice that the first author surname (ámon) has a numerical character entity, in this case the character á.

```
<journal_article publication_type="full_text">
  <titles>
	<title>Twenty years after chernobyl in Hungary: the Hungarian perspective of energy policy and the role of nuclear power</title>
  </titles>
  <contributors>
	<person_name sequence="first" contributor_role="author">
  	<given_name>Ada</given_name>
  	<surname> &#x00E1;mon </surname>
	</person_name>
  </contributors>
  <publication_date ="print">
	<month>7</month>
	<day>1</day>
	<year>2006</year>
  </publication_date>
  <publication_date ="online">
	<month>7</month>
	<day>29</day>
	<year>2009</year>
  </publication_date>
  <pages>
	<first_page>383</first_page>
	<last_page>399</last_page>
  </pages>
  <publisher_item>
	<identifier id_type="pii">C8612851662R7W4H</identifier>
  </publisher_item>
  <doi_data>
	<doi>10.1260/095830506778119452</doi>
  </doi_data>
```

If we formulate a query that does not include page number, we must include the author using the entity and not some other form of character, for example:

**No match:**
```
  <query key="mykey" enable-multiple-hits="false">
      <journal_title>Energy &amp; Environment</journal_title>
       <author>ámon</author>
  	     <volume>17</volume>
      <issue>3</issue>
      <year>2006</year>
  </query>
```

**No match:**
```
<query key="mykey" enable-multiple-hits="false">
        <journal_title>Energy &amp; Environment</journal_title>
         <author>amon</author>
  	       <volume>17</volume>
        <issue>3</issue>
        <year>2006</year>
   </query>
```

**Match found:**
```
<query key="mykey" enable-multiple-hits="false">
        <journal_title>Energy &amp; Environment</journal_title>
         <author>&#x00E1;mon</author>
  	       <volume>17</volume>
        <issue>3</issue>
        <year>2006</year>
   </query>
```