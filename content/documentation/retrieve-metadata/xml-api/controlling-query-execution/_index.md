+++
title = "Controlling query execution"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-api", "controlling-query-execution",]
identifier = "documentation/retrieve-metadata/xml-api/controlling-query-execution"
rank = 4
weight = 31006
aliases = [ "/education/retrieve-metadata/xml-api/controlling-query-execution", "/education/retrieve-metadata/xml-api/controlling-query-execution/",]
x-version = "0.0.0"

+++

Some query elements have optional properties that control how a query is executed by the XML API. Our query engine operates by evaluating several logic rules in series. Each rule focuses on certain fields in the query, with the first rule processing the entire query. If any rule returns a single metadata record as its output, this DOI is taken as the result for the query, and rule processing terminates.

Controls include:

* [Match](/documentation/retrieve-metadata/xml-api/using-the-match-attribute): use to specify level of fuzzy or exact matching. This attribute may be applied to several elements (ISSN, author, issue, article_title)
* [Enable-multiple-hits](/documentation/retrieve-metadata/xml-api/allowing-multiple-hits): allows or prevents matches returning more than one DOI
* [Secondary-query](/documentation/retrieve-metadata/xml-api/secondary-queries): instructs the system to perform a specific query if the default query mode does not produce a result. Current options include author/title, multiple hits, and author/title multiple hits.

## Table of query controls<a id='00444' href='#00444'><i class='fas fa-link'></i></a>

|Element|Property|Value|Purpose|
|--- |--- |--- |--- |
|query|key|string|to track a query to its results|
|query|enable-multiple-hits|false (default)|have the system reduce the query to one DOI and return nothing if it can not do so|
|query|enable-multiple-hits|true|have the system return one DOI for each query rule it executes|
|query|enable-multiple-hits|multi_hit_per_rule|have the system return many DOIs for each query rule it executes - will produce up to 50 candidate DOIs which partially match the query|
|query|enable-multiple-hits|exact|rule processing is disabled, all DOIs matching a simple comparison to query values are returned|
|query|forward-match|false (default)|no query is stored|
|query|forward-match|true|store this query and re-run it automatically and notify via email any matches that are found|
|query|list-components|false (default)|components not included in results|
|query|list-components|true|list the DOIs of any components that have deposited which are linked to this DOI|
|query|expanded-results|false (default)|will not include article title in results|
|query|expanded-results|true|include article title in the results (only applicable when results format=XML_XSD)|
|query|[secondary-query](/documentation/retrieve-metadata/xml-api/secondary-queries)|author-title|perform author-title search if metadata search fails|
|query|[secondary-query](/documentation/retrieve-metadata/xml-api/secondary-queries) |multiple-hits|returns multiple hits (if present)|
|query|[secondary-query](/documentation/retrieve-metadata/xml-api/secondary-queries) |author-title-multiple-hits|performs author/title and multiple hits search if initial search fails|
|issn|match|optional|value may be missing from deposited metadata|
|issn|match|exact|match exactly as it appears in the query|
|journal_title|match|optional|not required|
|journal_title|match|fuzzy (default)|use fuzzy string matching|
|journal_title|match|exact|match exactly as it appears in the query|
|author|match|optional|instructs the query engine that this field may be ignored|
|author|match|fuzzy (default)|use fuzzy string matching|
|author|match|null|match if author is missing in the metadata|
|author|match|exact|match exactly as it appears in the query|
|issue|match|fuzzy (default) / exact|-- see above --|
|first_page|match|fuzzy (default) / exact|-- see above --|
|year|match|optional (default) / exact|-- see above --|
|article_title|match|fuzzy (default) / exact|-- see above --|