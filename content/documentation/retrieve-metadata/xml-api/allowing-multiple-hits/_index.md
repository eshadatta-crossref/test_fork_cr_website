+++
title = "Allowing multiple hits"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-api", "allowing-multiple-hits",]
identifier = "documentation/retrieve-metadata/xml-api/allowing-multiple-hits"
rank = 4
weight = 31008
aliases = [ "/education/retrieve-metadata/xml-api/allowing-multiple-hits", "/education/retrieve-metadata/xml-api/allowing-multiple-hits/",]
x-version = "0.0.0"

+++

Our query engine operates by processing many different rules in order. Normally if the output of any rule is a single DOI, processing terminates and that DOI is returned as the result of the query. If no rule can produce only one DOI, the query fail results (its results are an ambiguous set). This behavior is necessary because most processes querying Crossref are automated and are incapable of deciding what to do with multiple DOI records.

However, in an editorial environment where a person is involved, getting multiple results may be valuable if the system is unable to reduce the match to a single item.

To enable Crossref to return more than result, set the `enable-multiple-hits` attribute for query to *true*, *exact*, or *multi_hit_per_rule* for a given query item.

To retrieve a candidate list of DOIs, use the enable-multiple-hits attribute:

```
<query key="key" enable-multiple-hits="true/false/multi_hit_per_rule/exact">
```

Each option invokes the following responses:

* **True**: Instructs the query function to return the single DOI produced by all the various rules. The output of a rule that produces more than one DOI will not be included.
* **MULTI_HIT_PER_RULE**: Instructs the query engine to return all DOIs uncovered by each rule.
* **Exact**: Disables the normal rule processing and instructs the query engine to perform a simple comparison to the query values and return all DOIs that match (up to a limit of 40).