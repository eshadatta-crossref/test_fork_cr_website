+++
title = "Providing full-text links to TDM tools"
date = "2022-01-03"
draft = false
type = "documentation"
author = "Isaac Farley"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "rest-api", "providing-full-text-links-to-TDM-tools",]
identifier = "documentation/retrieve-metadata/rest-api/providing-full-text-links-to-TDM-tools"
rank = 4
weight = 30106
aliases = [ "/education/retrieve-metadata/rest-api/providing-full-text-links-to-TDM-tools", "/education/retrieve-metadata/rest-api/providing-full-text-links-to-TDM-tools/",]
x-version = "0.0.0"

+++

Our API makes use of [content negotiation](/documentation/retrieve-metadata/content-negotiation) (the possibility to serve different versions of a document) to provide a common mechanism for TDM users to locate the full-text of articles on a member’s site. The member is responsible for delivering the content to the user, applying any access control, and applying usage statistics data to content accessed in this way.

Normally, when you follow a DOI link, the browser sends a signal to the web server asking for the content to be returned in HTML for display in the browser. Therefore, when you access a DOI using a browser, you are shown the member’s landing page.

With content negotiation, you can write programs that specify that, instead of returning a human-readable HTML representation of the landing page, the server should return a machine-readable representation of the data connected with the DOI.

To support TDM, members need include full-text URL(s) in their metadata for each content item with a DOI. Anybody using our API to query will be able to retrieve these URLs and follow them directly to the full-text. Members who want to be able to support multiple formats of the full-text of the article will be able to do so - for example, they could support the retrieval of either PDF, XML, or HTML, or just one of these formats.

There are two methods that members can use to register links to full-text content: for members who do (method 1) and do not (method 2) support content negotiation on their platforms.

## Method 1: Member provides a URL which points to content negotiation resource<a id='00373' href='#00373'><i class='fas fa-link'></i></a>

This method is for members who support content negotiation on their platforms.

This section of XML illustrates how to specify a single URL end-point where the member platform supports content negotiation:

```
http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/525252
```

In this case, the following cURL HTTP GET request:

```
curl -L -iH "Accept: text/turtle" http://dx.doi.org/10.5555/525252
```

will return the following in the HTTPS link header:

```
Link: <https://data.crossref.org/fulltext/10.5555%2F525252>; rel="http://id.crossref.org/schema/fulltext"; anchor="http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/525252"
```

[Example deposit file](https://gitlab.com/crossref/schema/-/blob/be649453e6c6f73f24980611e0d0279442d743f2/examples/525252.xml) for DOI 10.55555/525252.

## Method 2: Member provides specific URLs for each mime-type they support<a id='00374' href='#00374'><i class='fas fa-link'></i></a>

This method is for members who do not support content negotiation on their platforms.

This section of XML for the DOI 10.5555/515151 illustrates how to specify separate full-text URLs for separate mime types.

```
http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/515151.pdf
http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/515151.xml
```

In the above case, the following content negotiation request (using cURL) on the DOI 10.5555/515151:

```
curl -L -iH "Accept: text/turtle" http://dx.doi.org/10.5555/515151
```

would return the following in the HTTPS LINK header:

```
Link: <https://data.crossref.org/fulltext/10.5555%2F515151>; rel="http://id.crossref.org/schema/fulltext"; type="application/pdf"; anchor="http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/515151.pdf", <https://data.crossref.org/fulltext/10.5555%2F515151>; rel="http://id.crossref.org/schema/fulltext"; type="application/xml"; anchor="http://annalsofpsychoceramics.labs.crossref.org/fulltext/10.5555/515151.xml"
```

which would, in turn, direct the requestor to the appropriate URLs for whatever full-text representations are supported.

[Example deposit file](https://gitlab.com/crossref/schema/-/blob/be649453e6c6f73f24980611e0d0279442d743f2/examples/515151.xml/) for DOI 10.5555/515151.