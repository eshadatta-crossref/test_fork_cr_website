+++
title = "A non-technical introduction to our API"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "rest-api", "a-non-technical-introduction-to-our-API",]
identifier = "documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-API"
rank = 4
weight = 30101
aliases = [ "/education/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-API", "/education/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-API/",]
x-version = "0.0.0"

+++

You can query our API to find answers to questions about our metadata. Copy and paste these URLs into a browser to find out what has been registered with us.

You can modify the parameters in each URL, for example: if `https://api.crossref.org/members?rows=100` gives the first 100 accounts, modify it to `https://api.crossref.org/members?rows=200` to give the first 200. There is a maximum row limit of 2000.

* How many accounts do wehave? (This includes members and others, both inactive and active) [https://api.crossref.org/members?rows=0](https://api.crossref.org/members?rows=0)
* Who are they? Let’s look at the first 100 accounts [https://api.crossref.org/members?rows=100](https://api.crossref.org/members?rows=100)
* And the second 100 accounts [https://api.crossref.org/members?rows=100&offset=100](https://api.crossref.org/members?rows=100&offset=100)
* How many records do we have? [https://api.crossref.org/works?rows=0](https://api.crossref.org/works?rows=0)
* What content types do we have? [https://api.crossref.org/types](https://api.crossref.org/types)
* How many journal article DOIs do we have? [https://api.crossref.org/types/journal-article/works?rows=0](https://api.crossref.org/types/journal-article/works?rows=0)
* How many conference proceedings records do we have? [https://api.crossref.org/types/proceedings-article/works?rows=0](https://api.crossref.org/types/proceedings-article/works?rows=0)
* How can I see all the records registered under a given prefix? [https://api.crossref.org/prefixes/10.21240/works?select=DOI&rows=1000](https://api.crossref.org/prefixes/10.21240/works?select=DOI&rows=1000) And how can I see all the  works registered under a given prefix? [https://api.crossref.org/prefixes/10.35195/works](https://api.crossref.org/prefixes/10.35195/works) If your prefix has more than 1,000 DOIs registered, not all of them will display on one page, so it’s best to query for them using [machine retrieval from the REST API](https://www.crossref.org/education/retrieve-metadata/rest-api/)
* But eventually you will probably want to start looking at metadata records. Let’s search for records that have the word "blood" in the metadata and see how many there are. [https://api.crossref.org/works?query=blood&rows=0](https://api.crossref.org/works?query=blood&rows=0)
* Let’s look at some of the results. [https://api.crossref.org/works?query=%22blood%22&](https://api.crossref.org/works?query=%22blood%22&)
* Now let’s look at one of the records [https://api.crossref.org/works/10.1155/2014/413629](https://api.crossref.org/works/10.1155/2014/413629). Interesting. The record has ORCID iDs, full-text links, and license links. You need license and full-text links to text and data mine the content.
* How many works have license information? [https://api.crossref.org/works?filter=has-license:true&rows=0](https://api.crossref.org/works?filter=has-license:true&rows=0)
* How many license types are there? [https://api.crossref.org/licenses?rows=0](https://api.crossref.org/licenses?rows=0)
* OK, let’s see how many records with the word "blood" in the metadata also have license information and full-text links [https://api.crossref.org/works?filter=has-license:true,has-full-text:true&query=blood&rows=0](https://api.crossref.org/works?filter=has-license:true,has-full-text:true&query=blood&rows=0)
* And here's how to look up the XML behind a DOI [https://api.crossref.org/works/10.5555/487hjd.xml](https://api.crossref.org/works/10.5555/487hjd.xml). Remove .xml to see results in JSON [https://api.crossref.org/works/10.5555/487hjd](https://api.crossref.org/works/10.5555/487hjd).

Learn more about [constructing API queries by combining components, identifiers, and parameters](https://github.com/CrossRef/rest-api-doc#resource-components).