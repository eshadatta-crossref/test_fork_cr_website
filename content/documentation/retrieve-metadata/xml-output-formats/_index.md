+++
title = "XML output formats"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-output-formats",]
identifier = "documentation/retrieve-metadata/xml-output-formats"
rank = 4
weight = 31100
aliases = [ "/education/retrieve-metadata/xml-output-formats", "/education/retrieve-metadata/xml-output-formats/",]
x-version = "0.0.0"

+++

XML output formats include:

* [UNIXSD query output format](/documentation/retrieve-metadata/xml-output-formats/unixsd-query-output-format) | [UNIXSD metadata](/documentation/retrieve-metadata/xml-output-formats/unixsd-query-output-format) | [UNIXSD schema](/documentation/retrieve-metadata/xml-output-formats/unixsd-query-output-format)
* [UNIXREF query output format](/documentation/retrieve-metadata/xml-output-formats/unixref-query-output-format) | [UNIXREF schema](/documentation/retrieve-metadata/xml-output-formats/unixref-query-output-format)
* [XSD XML query output format](/documentation/retrieve-metadata/xml-output-formats/xsd-xml-query-output-format)