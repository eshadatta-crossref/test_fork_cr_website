+++
title = "XSD XML query output format"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "xml-output-formats", "xsd-xml-query-output-format",]
identifier = "documentation/retrieve-metadata/xml-output-formats/xsd-xml-query-output-format"
rank = 4
weight = 31103
aliases = [ "/education/retrieve-metadata/xml-output-formats/xsd-xml-query-output-format", "/education/retrieve-metadata/xml-output-formats/xsd-xml-query-output-format/",]
x-version = "0.0.0"

+++

The XSD_XML format follows the [Crossref query output schema](http://data.crossref.org/schemas/crossref_query_output3.0.xsd). It provides basic citation metadata that has been processed by Crossref. For doi-to-metadata XML querying, some controls are available for including expanded data and/or components in results.

## XSD XML examples<a id='00457' href='#00457'><i class='fas fa-link'></i></a>

The default query set will return basic citation metadata only:

```
<query key="q1">
<doi>10.1107/s1600536814013208</doi>
</query>
```

Set expanded-results="true" to return additional author, article title, and date information:

```
<query list-components="false" expanded-results="true" key="expanded">
   <doi>10.1107/s1600536814013208</doi>
</query>
```

Set list-components="true" to include components in results:

```
<query list-components="true" expanded-results="true" key="expanded-with-components">
   <doi>10.1107/s1600536814013208</doi>
</query>
```

Example files: [xsd_xml.out.xml](/xml-samples/xsd_xml.out.xml) and [xsd_xml.in.xml](/xml-samples/xsd_xml.in.xml)