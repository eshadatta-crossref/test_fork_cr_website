+++
title = "Content negotiation"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "retrieve-metadata", "content-negotiation",]
identifier = "documentation/retrieve-metadata/content-negotiation"
rank = 4
weight = 30700
aliases = [ "/education/retrieve-metadata/content-negotiation", "/education/retrieve-metadata/content-negotiation/",]
x-version = "0.0.0"

+++

Content negotiation means the ability to specify the format in which a record will be returned.

Crossref and [DataCite](https://datacite.org/) support content-negotiated DOIs via their respective DOIs. Learn more about [content negotiation](https://citation.crosscite.org/docs.html).

Content negotiation responses are returned by one of our three API pools, just like any other REST API response. Requests that include mailto identification in the user agent header will be serviced by the polite pool, and requests that include a token for Metadata Plus subscribers will be serviced by the Plus pool. All other requests will be serviced by the public pool.

## Examples using [cURL](http://curl.haxx.se/)<a id='00412' href='#00412'><i class='fas fa-link'></i></a>
```
curl -D - -L -H "Accept: application/rdf+xml" "https://doi.org/10.1126/science.1157784"

curl -D - -L -H "Accept: text/turtle" "https://doi.org/10.1126/science.1157784"

curl -D - -L -H "Accept: application/atom+xml" "https://doi.org/10.1126/science.1157784"
```

The Crossref-specific 'unixref' format can be used as well:
```
curl -D - -L -H "Accept: application/unixref+xml" "https://doi.org/10.1126/science.1157784"
```

Content negotiation also supports formatted citations via the text/x-bibliography content type. A list of [all supported citation styles](https://api.crossref.org/v1/styles) is available via our API.