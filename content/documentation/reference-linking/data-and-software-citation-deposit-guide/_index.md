+++
title = "Data and software citation deposit guide"
date = "2022-06-07"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = [ "reference-linking", "data-and-software-citation-deposit-guide",]
identifier = "documentation/reference-linking/data-and-software-citation-deposit-guide"
rank = 4
weight = 70200
aliases = [ "/education/reference-linking/data-and-software-citation-deposit-guide", "/community/linking-data/", "/education/reference-linking/data-and-software-citation-deposit-guide/",]
x-version = "0.0.0"

+++

As well as providing persistent links to scholarly content, we also provide community infrastructure by linking publications to associated content, making research easy to find, cite, link, and assess. Data citations are a core part of this service, linking publications to their supporting data, making both the research itself and the research process more transparent and reproducible.

Data citations are references to data, just as bibliographic citations make reference to other scholarly sources.

Members deposit data citations by including them in their metadata as [references](/documentation/content-registration/descriptive-metadata/references/) and/or [relationship types](/documentation/content-registration/structural-metadata/relationships/). Once deposited, data citations across journals (and publishers) are then aggregated and made freely available for the community to retrieve and reuse in a single, shared location.

There are two ways for members to deposit data citation links:

1. Bibliographic references: The main mechanism for depositing data and software citations is to insert them into an article’s reference metadata. Data citations are included in the deposit of bibliographic references for each publication. Follow the [general process for depositing references](/documentation/metadata-stewardship/maintaining-your-metadata/add-references/) and apply tags as applicable.

2. Relationship type: data links are asserted in the [relationship](/documentation/content-registration/structural-metadata/relationships/) section of the metadata deposit, where they connect the publication to a variety of associated online resources (such as data and software, supporting information, protocols, videos, published peer reviews, preprint, conference papers) in a structured way, making discovery more powerful and accurate. Here, publishers can identify data which are direct outputs of the research results if this is known. This level of specificity is optional, but can support scientific validation and research funding management.

The two methods are independent, and can be used individually or together.

| Method | Benefits | Limitations |
|---|---|---|
| Bibliographic references | <ul><li>Data and software citation is automatically deposited when included with publisher’s reference deposit</li></ul> | <ul><li>Limited to datasets with DataCite DOIs. Others cannot be identified and validated from references deposit</li><li>Noise: not all DataCite DOIs linked are datasets/software (they could be other content types such as articles, slides, preprints)</li></ul> |
| Relation type | <ul><li>Precise identification of data, differentiated from other content</li><li>Dataset differentiation between those generated as part of research results from those cited by the research</li></ul> | <ul><li>None</li></ul> |

Sending this metadata to Crossref makes it easier for the research community to see links between different research outputs and work with these outputs. It also makes it easier to see these citations, so that researchers can get credit for their data and the sharing of that data.

We collect these citations, and make them freely available via our APIs in multiple interfaces (REST, OAI-­PMH, OpenURL) and formats (XML, JSON). Data is made openly available to a wide range of organizations and individuals across the extended research ecosystem including funders, research organisations, technology and service providers, indexers, and many others.

## Bibliographic references<a id='00260' href='#00260'><i class='fas fa-link'></i></a>

As part of content registration, members add data and software citations into the bibliographic references, following the [general process for depositing references](/documentation/content-registration/descriptive-metadata/references#00177).

Full data or software citations can be deposited as unstructured references. See FORCE11’s community best practice: [Joint Declaration of Data Citation Principles](https://doi.org/10.25490/a97f-egyk), [Software Citation Principles](https://doi.org/10.7717/peerj-cs.86), and [advice on placement of citations](https://www.force11.org/node/4771).

You can employ any number of reference tags currently accepted by Crossref, but as good practice we'd recommend tagging the identifier for the code or dataset as shown below:

```
<citation key="ref2">
  <doi>10.5061/dryad.684v0</doi>
  <cYear>2017</cYear>
  <author>Morinha F, Dávila JA, Estela B, Cabral JA, Frías Ó, González JL, Travassos P, Carvalho D, Milá B, Blanco G</author>
</citation>
```

Existing reference tags were originally established to match article and book references and do not readily apply to data or software. We are exploring [JATS4R recommendations](http://jats4r.org/data-citations) to expand the current collection and better support these citations. Please [contact us](/contact/) if you would like to make additional suggestions.

## Relationship type<a id='00261' href='#00261'><i class='fas fa-link'></i></a>

Establishing data and software citations via relation type enables precise tagging of the dataset and its specific relationship to the research results published.

To tag the data and software citation in the metadata deposit, we ask for the description of the dataset and software (optional), dataset and software identifier and identifier type (DOI, PMID, PMCID, PURL, ARK, Handle, UUID, ECLI, and URI), and [relationship type](https://data.crossref.org/reports/help/schema_doc/4.4.2/relations_xsd.html#inter_work_relation). In general, use the relation type *references* for data and software resources.

To specify that the data or software resource was generated as part of the research results, use *isSupplementedBy*. Being this specific is optional, but can support scientific validation and research funding management. See the [list of controlled options for accepted identifier types](/documentation/content-registration/structural-metadata/relationships/).

## Examples of asserting a relationship to data and software in the metadata deposit<a id='00262' href='#00262'><i class='fas fa-link'></i></a>

| Dataset | Snippet of deposit XML containing link |
| --- | ---|
|**Dataset or software generated as part of research article:** <br/> Data from: Extreme genetic structure in a social bird species despite high dispersal capacity. <br/> **Database:** Dryad Digital Repository<br/>**DOI:** https://doi.org/10.5061/dryad.684v0  |  ` <program xmlns="http://www.crossref.org/relations.xsd"> ` <br/> ` <related_item> ` <br/> ` <description>Data from: Extreme genetic structure in a social bird species despite high dispersal capacity</description> ` <br/> ` <inter_work_relation relationship-type="isSupplementedBy" identifier-type="doi">10.5061/dryad.684v0</inter_work_relation> ` <br/> ` </related_item> ` <br/> ` </program> `
|**Associated dataset or software:**<br/> NKX2-5 mutations causative for congenital heart disease retain functionality and are directed to hundreds of targets <br/>**Database:** Gene Expression Omnibus (GEO) <br/> **Accession number:** GSE44902 <br/> **URL:** https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE44902 |  ` <program xmlns="http://www.crossref.org/relations.xsd"> ` <br/> ` <related_item> ` <br/> ` <description>NKX2-5 mutations causative for congenital heart disease retain and are directed to hundreds of targets</description> `<br/> ` <inter_work_relation relationship-type="references" identifier-type="Accession">GSE44902</inter_work_relation> ` <br/> ` </related_item> ` <br/>` </program> `|

## Example of data citation as relationship (full metadata deposit)<a id='00263' href='#00263'><i class='fas fa-link'></i></a>

```
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.4.0" xmlns="http://www.crossref.org/schema/4.4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.4.0 http://www.crossref.org/schemas/crossref4.4.0.xsd">
  <head>
	<doi_batch_id>20170807</doi_batch_id>
	<timestamp>2017080715731</timestamp>
	<depositor>
   	<depositor_name>Crossref</depositor_name>
   	<email_address>support@crossref.org</email_address>
	</depositor>
	<registrant>Crossref</registrant>
  </head>
  <body>
	<journal>
   	<journal_metadata language="en">
      	<full_title>Molecular Ecology</full_title>
      	<abbrev_title>Mol Ecol</abbrev_title>
      	<issn>09621083</issn>
   	</journal_metadata>
   	<journal_issue>
      	<publication_date media_type="print">
         	<month>05</month>
         	<year>2017</year>
      	</publication_date>
      	<journal_volume>
         	<volume>26</volume>
      	</journal_volume>
      	<issue>10</issue>
   	</journal_issue>
   	<journal_article publication_type="full_text">
      	<titles>
         	<title>Extreme genetic structure in a social bird species despite high dispersal capacity</title>
      	</titles>
      	<contributors>
         	<person_name contributor_role="author" sequence="first">
           	<given_name>Francisco</given_name>
           	<surname>Morinha</surname>
           	<affiliation>Laboratory of Applied Ecology; Centre for Research and Technology of Agro-Environment and Biological Sciences (CITAB); University of Trás-os-Montes and Alto Douro (UTAD); Quinta de Prados 5000-801 Vila Real Portugal</affiliation>
<affiliation>Morinha Lab - Laboratory of Biodiversity and Molecular Genetics; Rua Dr. José Figueiredo, lote L-2, Lj B5 5000-562 Vila Real Portugal</affiliation>
         	</person_name>
         	<person_name contributor_role="author" sequence="additional">
            	<given_name>José A.</given_name>
            	<surname>Dávila</surname>
            	<affiliation>Instituto de Investigación en Recursos Cinegéticos; IREC (CSIC, UCLM, JCCM); Ciudad Real Spain</affiliation>
         	</person_name>
         	<person_name contributor_role="author" sequence="additional">
            	<given_name>Estela</given_name>
            	<surname>Bastos</surname>
            	<affiliation>Laboratory of Applied Ecology; Centre for Research and Technology of Agro-Environment and Biological Sciences (CITAB); University of Trás-os-Montes and Alto Douro (UTAD); Quinta de Prados 5000-801 Vila Real Portugal</affiliation>
<affiliation>Department of Genetics and Biotechnology; School of Life and Environmental Sciences; University of Trás-os-Montes and Alto Douro (UTAD); Quinta de Prados 5000-801 Vila Real Portugal</affiliation>
         	</person_name>
      	</contributors>
      	<publication_date media_type="print">
         	<month>05</month>
         	<year>2017</year>
      	</publication_date>
      	<publication_date media_type="online">
         	<month>03</month>
         	<day>13</day>
         	<year>2017</year>
      	</publication_date>
      	<pages>
         	<first_page>2812</first_page>
         	<last_page>2825</last_page>
      	</pages>
      	<program xmlns="http://www.crossref.org/relations.xsd">
         	<related_item>
           	<description>Data from: Extreme genetic structure in a social bird species despite high dispersal capacity</description>
           	<inter_work_relation relationship-type="references" identifier-type="doi">10.5061/dryad.684v0</inter_work_relation>
         	</related_item>
      	</program>
      	<archive_locations>
         	<archive name="Portico"/>
      	</archive_locations>
      	<doi_data>
         	<doi>10.1111/mec.14069</doi>
         	<resource>http://doi.wiley.com/10.1111/mec.14069</resource>
      	</doi_data>
   	</journal_article>
	</journal>
  </body>
</doi_batch>
```

## Example of data citation as relation (resource-only deposit)<a id='00264' href='#00264'><i class='fas fa-link'></i></a>

```
<?xml version="1.0" encoding="UTF-8"?>
  <doi_batch version="4.4.2" xmlns="http://www.crossref.org/doi_resources_schema/4.4.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.4.2 https://data.crossref.org/schemas/doi_resources4.4.2.xsd">
    <head>
       <doi_batch_id>123456</doi_batch_id>
          <depositor>
          <depositor_name>Crossref</depositor_name>
          <email_address>support@crossref.org</email_address>
       </depositor>
    </head>
    <body>
       <doi_relations>
          <doi>10.1111/xxxx.xxxx</doi>
          <program xmlns="https://www.crossref.org/relations.xsd">
             <related_item>
             <description>Data from: Extreme genetic structure in a social bird species despite high dispersal capacity</description>
             <inter_work_relation relationship-type="references" identifier-type="doi">10.5061/dryad.684v0</inter_work_relation>
             </related_item>
          </program>
       </doi_relations>
    </body>
  </doi_batch>
```