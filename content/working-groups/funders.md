+++
title = "Funder advisory group"
date = "2021-02-07"
draft = false
author = "Jennifer Kemp"
rank = 3
aliases = [ "/fundref.crossref.org/working-groups/",]
x-version = "0.0.0"

[menu.main]
parent = "Working groups"
weight = 1

+++

The Funder Advisory Group was originally formed to help with the development of our funding data and [Funder Registry](/services/funder-registry) capabilities. As those services matured, the group re-convened in 2017 to discuss ways in which funders can take advantage of Crossref's infrastructure to register grant metadata and engage more officially by becoming members. As the result of work completed in 2018, research funders are now able to join, [register their grants with metadata and DOIs](/documentation/research-nexus/grants/) and reap the full benefits of membership. Funders, publishers and the entire community can contribute to and benefit from [linking grants to outputs](https://www.crossref.org/blog/the-more-the-merrier-or-how-more-registered-grants-means-more-relationships-with-outputs/).

The current Advisory Group is a mix of members, prospective members and representatives of related initiatives, and meets as needed to discuss Crossref developments as well as changes to the broader landscape, such as the recent [OSTP memo](https://www.crossref.org/blog/how-funding-agencies-can-meet-ostp-and-open-science-guidance-using-existing-open-infrastructure/).

#### Some use cases for registering grants

1. **Multi-country funding** e.g. the Australian Research Council wants to know which other countries their awardees get additional funding from.
2. **Government vs. private funding relationships** e.g. which private funders work with which governments to support what kind of research?
3. **Co-funding** e.g. which other funders do my grantees tend to receive support from as well as us?
4. **Portfolio analysis** e.g. a funder invests in centers and individual scientists; which effort generates more products?

More on use cases and example queries is provided in this [blog post overview](https://www.crossref.org/blog/come-and-get-your-grant-metadata/).

## What the group is working on  

In 2022, the group:
* Reviewed the new [grant registration form](https://www.crossref.org/documentation/register-maintain-records/grant-registration-form/)
* Discussed the recent [OSTP memo](https://www.crossref.org/blog/how-funding-agencies-can-meet-ostp-and-open-science-guidance-using-existing-open-infrastructure/)
* Completed a survey of needs
* Convened a sub-group tasked with making recommendations to Crossref based on the survey results:
  * Reduce barriers to volume registration of older grants
  * Better [matching of grants to outputs](https://www.crossref.org/blog/the-more-the-merrier-or-how-more-registered-grants-means-more-relationships-with-outputs/)
  *  Improve [evidence and awareness](https://www.crossref.org/blog/dont-take-it-from-us-funder-metadata-matters/) of the value of registering grants

As Crossref works toward those recommendations, the group will meet again in 2023 to determine its next priorities, in addition to ongoing engagement work.

* **Chair and facilitator: Ginny Hendricks, Crossref**

{{% row %}}
{{% column %}}
* Diego-Valerio Chialva, European Research Council
* Kevin Dolby, Medical Research Council, UK
* Maisie England, UKRI, UK
* Patricia Feeney, Crossref
* Steve Fitzmier, John Templeton Foundation
* Nina Frentrop, Wellcome, UK
* Melissa Harrison, EMBL-EBI, UK
* Brian Haugen, NIH, USA
* Ginny Hendricks, Crossref
* Andreas Kyriacou,	Swiss National Science Foundation, Switzerland
* Ashley Moore, UKRI, UK
* Adam Jones, The Gordon and Betty Moore Foundation, USA
* Adrian Burton, ARC, Australia
* Justin Withers, ARC, Australia
* Ashley Farley, The Gates Foundation, USA
* Ashton Ferrara, The ALS Association, USA
* Cátia Laranjeira, Foundation for Science and Technology, Portual
* Cindy Danielson, National Institutes of Health, USA
* Carly Strasser, CZI, USA
* David Vestergaard Eriksen, Ministry of Higher Education and Science, Denmark
* Mogens Sandfaer, Ministry of Higher Education and Science, Denmark
* M. Brent Dolezalek, James S. McDonnell Foundation, USA
* Josh Greenberg, Sloan Foundation, USA
* Katharina Rieck, Austrian Science Fund, Austria
* Kristen Ratan, Strategies for Open Science (Stratos), USA
* Linda Kee, American Cancer Society, USA
* Maryrose Franko, Health Research Alliance, USA
* Patti Biggs, The Francis Crick Institute, UK
* Rita Barata, CAPES, Brazil
* Sheila Rabun, LYRASIS, USA
* Alexis-Michel Mugabushaka, European Research Council, France
* Lisa Murphy,	Science Foundation Ireland, Ireland
* Alicia Smyth, Science Foundation Ireland, Ireland
{{% /column %}}
{{% column %}}

* Ritsuko	Nakajima, JST, Japan
* Masashi Hara, JST, Japan
* Yoshiro Hirao, JST, Japan
* Lucy Ofiesh, Crossref
* Dominika Tkaczyk, Crossref
* Erin  McKiernan, Open Research Funders Group
* Michael Parkin, EMBL-EBI, UK
* Steve Pinchotti, Altum
* Falk Rekling, FWF, Austria
* Carly	Robinson,	DOE, USA
* Elizabeth Agee, DOE, USA
* Natasha Simons, ARDC, Australia
* Gerald Steeman,	NASA, USA
* Raven Edgerton, NASA, USA
* Ginger Strader Minkiewicz, Smithsonian Institution, USA
* Ann Fust, Swedish Research Council, Sweden
* Beata Moore, USDA, USA
* Cynthia Parr, USDA, USA
* Scott Hanscom, USDA, USA
* Vicky Crone, USDA, USA
* Benjamin Missbach, Vienna Science and Technology Fund, Austria
* Clifford Tatum, SURF, Netherlands
* Guntram Bauer, Human Frontier Science Program, USA
* Hans de Jonge, NWO (Dutch Research Council), Netherlands
* Maria Cruz, NWO (Dutch Research Council), Netherlands
* Angela Holzer, DFG (German Research Foundation), Germany
* Richard Heidler, DFG (German Research Foundation), Germany
* Tobias Grimm, DFG (German Research Foundation), Germany
* Martin Halbert, National Science Foundation (NSF), USA
* Shawna Sadler, ORCID
{{% /column %}}
{{% /row %}}


## Original working group participants

A big thanks to the group of volunteers who at various stages jumped in to help make the registration of grants a reality.

## Links & more information

- [Crossref for funders](/community/funders)
- [Registering research grants](/education/content-registration/content-types-intro/grants/)
- [Metadata schema for grants](https://gitlab.com/crossref/schema/-/blob/master/schemas/grant_id0.0.1.xsd)

---
Please contact [Ginny Hendricks](mailto:feedback@crossref.org) with any questions.