+++
title = "Books interest group"
date = "2019-10-10"
draft = false
author = "Jennifer Kemp"
x-version = "0.0.0"

[menu.main]
parent = "Working groups"
weight = 4

+++

The Books Interest Group serves as a resource for Crossref and its participants to surface, discuss and make progress on metadata and workflow issues unique to book publishers. The group comprises Crossref members and nonmembers, and is led by a Chair and Crossref staff facilitators.

## What we're working on

This year, the group is focused on:

  * Reviewing book and chapter types in our metadata schema, as well as books representation in JSON outputs.
  * [Best practice for books](/education/content-registration/content-types-intro/books-and-chapters/). This guide has had some recent revisions and work on it will continue this year.
  * Bringing others from the industry into discussions on topics of particular interest, such as books on multiple platforms.
  * Education and outreach: ongoing, with use cases of particular interest and most recently including [Publisher Participation Reports](/blog/321-its-lift-off-for-participation-reports)

Suggestions for topics are welcome.

## Participants

Chair: David Woodworth, OCLC  
Facilitators: Jennifer Kemp

{{% row %}}
{{% column %}}
* Diane Needham, ACS
* Emily Ayubi, American Psychological Association (APA)
* Eva Winer, American Psychological Association (APA)
* Timothy McAdoo, APA
* Fatima Abulawi, Atypon
* Dawn Ingram, Atypon
* Elli Rapti, Atypon
* Dan Vernooj, Brill
* Mike Eden, Cambridge University Press
* Rachael Kendall, Cambridge University Press
* Saskia Wenzel, De Gruyter
* Mike Taylor, Digital Science
* Allison Belan, Duke University Press
* Patty Chase, Duke University Press
* Patty Van, Duke University Press
* Keara Mickelson, Edinburgh University Press
* Melissa Kreitzer, Elsevier
* Marc Segers, Geoscienceworld
* Jim Beardow, IMF
* Patricia Loo, IMF
* Bruce Rosenblum, Inera
* Wendy Queen, Johns Hopkins University Press
* Lauren Lissaris, JSTOR
* Jabin White, JSTOR
* Bill Kasdorf, Kasdorf & Associates, LLC
* Ardie Bausenbach, Library of Congress
{{% /column %}}
{{% column %}}

* Sharla Lair, LYRASIS
* Todd Carpenter, NISO
* Christina Drummond, OAeBU
* Ursula Rabar, OAeBU/OPERAS
* Ronald Snijder, OAPEN
* Claire Holloway, OCLC
* David Woodworth, OCLC
* Rupert Gatti, Open Book Publishers
* Shawna Sadler, ORCID
* Mark Dunn, Oxford University Press
* Amber Fischer, Oxford University Press
* Matthew Treskon, Project MUSE
* Giovanna Brito Castelhano, SciELO
* Stephanie Dawson, ScienceOpen
* Nina Tscheke, ScienceOpen
* Nicola Parkin, Taylor and Francis
* John Normansell, The University of Manchester
* Paige MacKay, Ubiquity Press
* Tom Mowlam, Ubiquity Press
* Erich Van Rijn, University of California Press
* Krista Coulson, University of Chicago Press
* Charles Watkinson, University of Michigan Press
* Jeremy Morse, University of Michigan Press
* Peter Potter, Virginia Tech/TOME
* Pascal Ssemaganda, World Bank Publications
* Ron Denton
* Sun Huh
{{% /column %}}
{{% /row %}}


## How the group works (and the guidelines)

The group meets quarterly and comprises a staff facilitator, a chair and number of members and subscribers who commit to regular attendance and participation. Guest attendance is welcome on a per-meeting basis.

---
Please contact [Jennifer Kemp](mailto:feedback@crossref.org) with any questions or to join the calls.