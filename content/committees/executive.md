+++
title = "Executive committee"
date = "2020-04-10"
draft = false
rank = 3
x-version = "0.0.0"

[menu.main]
parent = "Committees"
author = "Ed Pentz"
weight = "4"

+++

The Executive Committee is made up of the Chair, Treasurer and three other board members, one who has to be a representative of a non-profit member. The Executive committee has three major functions, to:

1. **Steer:** create and review agendas for discussion and decision by the Board.
2. **Oversee:** evaluate key performance indicators and suggest corrective actions
between Board meetings.
3. **Expedite:** take any decisions delegated to it by the board. This usually happens
after full board discussion reaches a consensus on a major initiative and wants open details resolved before the next board meeting.

## Executive Committee members

* Lisa Schiff, California Digital Library (Chair)
* Rose L'Huillier, Elsevier (Treasurer)
* Wendy Patterson, Beilstein-Institut
* James Phillpotts, Oxford University Press
* Christine Stohn, Clarivate