+++
title = "Membership & Fees committee"
date = "2021-04-09"
author = "Amanda Bartell"
draft = false
rank = 3
x-version = "0.0.0"

[menu.main]
parent = "Committees"
weight = 4

+++

The Membership & Fees Committee (M&F committee) was established in 2001 and plays an important role in our governance. Made up of 15 organizations that include members, sponsored members, a sponsor, a metadata user, and metadata users - some of whom are also board members. The group makes recommendations to the board about fees and policies for all of our services and procedures in relation to fees and community work.

They review existing fees to discuss if any changes are needed. They also review new services while they are being developed, to assess if fees should be charged and if so, what those fees should be, in line with [our fee principles](/operations-and-sustainability/#fee-principles). In addition, the board can also delegate specific issues about policies and services to the M&F committee.

In 2019 the committee undertook a regular fee review which involved a few specific cases being recommended to the board:

1. Removing the Crossmark fee
2. Removing the fees for versions and translations (registered by the same member)
3. Updating a set of principles for guiding fee-setting

After a period of strategic review of the research nexus vision focusing on relationships, the M&F Committee's agenda in 2023 is:

1. Proposed evolving of our Fee Assistance Program into a Global Equitable Membership (GEM) program which will include fee waivers for all members in 59 IDA countries.
2. Fees and policies related to peer review reports as a relationship type.
3. Fees for members who register their research grants (there is a separate sub-group of the Funder Advisory Group whose recommendations will feed into the M&F discussion).

If your organization meets the criteria for the vacancies listed below, please get in touch with Amanda Bartell.

## M&F committee members

Crossref facilitator: Amanda Bartell

|Committee member|Representative|Country|Size (records)|Org type|
|:--|:--|:--|--:|:--|
|ACM*|Scott Delman| USA | 600,000|Society, Publisher  |
|Association of Lithuanian Serials|Vincas Grigas| Lithuania | N/A |Society, Sponsor  |
|Clarivate Analytics*|Nandita Quaderi| USA | 11,000|Publisher, Service Provider  |
|eLife*|Damian Pattinson| UK | 40,000|Publisher |
|Elsevier*|Rose L'Huillier| Netherlands |20,000,000 | Publisher  |
|Frontiers|Marie Souliere| Switzerland | 400,000| Publisher |
|Liverpool University Press|Clare Hooper| UK | 46,000|University, Publisher  |
|Scholastica |Brian Cody| USA | N/A |Service Provider |
|Sociedade Brasileira de Cardiologia|Mariana da Silveira Machado Pereira| Brazil | 3000|Society, Publisher  |
|Swiss National Science Foundation|Andreas Kyriacou| Switzerland | 65|Funder  |
|Wits University Press|Andrew Joseph| South Africa | 1,800|University, Publisher  |
|Vacancy: Small publisher (<250 records)|TBD| TBD | TBD |TBD  |
|Vacancy: Library publisher |TBD| TBD | TBD |TBD  |

_(*) indicates current Crossref board member_

## About committee participation

The M&F Committee meets via one-hour conference calls about four times a year, although this can vary depending on what issues the committee is considering and what topics the board has delegated to them. Often proposals are developed by staff and then reviewed and discussed by the committee – so there is reading to do in preparation for the calls. The committee Chair is a board member and acts as a link between the two groups, presenting M&F recommendations to the board for them to vote.

This is very important work and it's essential the committee is broadly representative of Crossref’s diverse membership of over 17,500 organizations in 148 countries. Appointments are for one year and participants can sometimes serve multiple terms.

---

Please contact the [community team](mailto:feedback@crossref.org) with any questions.