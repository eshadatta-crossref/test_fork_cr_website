+++
title = "The Public Knowledge Project and Crossref Collaborate to Improve Services for Publishers using Open Journal Systems"
date = "2014-10-02"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*02 October 2014*

**2 October 2014, Lynnfield, MA, USA and Vancouver, BC, Canada** – Crossref and the Public Knowledge Project (PKP) are collaborating to help publishers and journals using the Open Journal Systems (OJS) platform take better advantage of Crossref services.

The collaboration involves an institutional arrangement between Crossref and PKP, and new software features. Features include an improved Crossref plugin for OJS that will automate Digital Object Identifier (DOI) deposits, as well as plans to create new tools for extracting references from submissions. To facilitate Crossref membership, PKP has also become a Crossref Sponsoring Entity, which will allow OJS-based publishers to join Crossref through PKP.

The latest release of OJS [version 2.4.5](https://pkp.sfu.ca/ojs/ojs_download/) includes a new Crossref plugin with improved support for Crossref deposits, the process by which Crossref member publishers can assign DOIs (persistent, actionable identifiers) to their content. A Crossref deposit includes the bibliographic metadata about an article or other scholarly document, the current URL of the item, and the DOI. Publishers need only update the URL at Crossref if the content’s web address changes. The cited DOI will automatically direct readers to the current URL.

OJS 2.4.5 includes several general improvements that benefit Crossref members directly and indirectly. First, OJS now allows for automatic deposits to the Crossref service – manually uploading data via Crossref’s web interface is no longer necessary. Second, users of the plugin will be able to deposit Open Researcher and Contributor Identifiers (ORCIDs), which OJS can now accept during the author registration and article submission processes.

Additionally, this release also allows OJS publishers to more easily participate in the LOCKSS archiving service of their choice (including the forthcoming [PKP PLN Service)](https://pkp.sfu.ca/pkp-lockss/).

Finally, this new release will serve as the foundation for further integration of other Crossref features and services, such as the deposit of FundRef funding data, and the Crossmark publication record service.

“The release of OJS 2.4.5 signals a new strategic direction for PKP in the provision of enhanced publishing services, such as the new Crossref plugin,” said Brian Owen, Managing Director of PKP. “Our collaboration with Crossref has enabled us to move up the development of features that our publishers have been asking for. The partnership doesn’t end here, either. We’re looking forward to supporting publishers more directly now that we are a Sponsoring Entity and to jointly develop tools that will make it easier for publishers to comply with Crossref’s outbound reference linking requirements.”

Crossref Executive Director Ed Pentz noted, “The profile of Crossref’s member publishers has changed significantly over the years. We are growing by hundreds of members each year. Many of these publishers are small institution-based journals from around the world. And many are hosted by the open source OJS software. It has been challenging for some of these organizations to meet our membership obligations like outbound reference linking and arranging for long-term archiving. And many have not been able to participate in newer services, because they require the ability to deposit additional metadata. We want all of our publishers to have a level playing field, regardless of their size. Our cooperation with PKP will help make that happen.”

Journals and publishers that use OJS and that already have established a direct relationship with Crossref, or those that have an interest in becoming members through PKP, may take advantage of the enhanced features in the new Crossref plugin by upgrading to OJS 2.4.5. And starting now, eligible journals can apply for a PKP-sponsored Crossref membership for free DOI support. See [PKP’s Crossref page](https://pkp.sfu.ca/crossref/) for more information.

About PKP
The Public Knowledge Project was established in 1998 at the University of British Columbia. Since that time PKP has expanded and evolved into an international and virtual operation with two institutional anchors at Stanford University (http://www.stanford.edu/) and Simon Fraser University Library [(http://www.lib.sfu.ca/)](http://www.lib.sfu.ca/ ). OJS is open source software made freely available to journals worldwide for the purpose of making open access publishing a viable option for more journals, as open access can increase a journal’s readership as well as its contribution to the public good on a global scale. More information about PKP and its software and services is available at [pkp.sfu.ca](http://pkp.sfu.ca/).

About Crossref
Crossref (www.crossref.org) serves as a digital hub for the scholarly communications community. A global not-for profit membership organization of scholarly publishers, Crossref's innovations shape the future of scholarly communications by fostering collaboration among multiple stakeholders. Crossref provides a wide spectrum of services for identifying, locating, linking to, and assessing the reliability and provenance of scholarly content.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.