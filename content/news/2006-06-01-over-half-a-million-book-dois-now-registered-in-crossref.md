+++
title = "Over Half a Million Book DOIs Now Registered In Crossref"
date = "2006-10-30"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*01 June 2006*

**Lynnfield, MA June 1, 2006**  Crossref, the reference-linking service for scholarly and professional content, announced today that it had surpassed the half-million mark in the number of book title and chapter DOIs registered in its database. By virtue of being in Crossref, these books and chapters are readily inter-linkable with the 20 million-plus other content items (mainly journal articles) publishers have registered. This enables the reader of online works to go directly from a journal article to a cited book chapter, or vice versa, just by clicking on a reference.

Crossref has been adding books and major reference works (MRWs) to its cross-publisher linking network since 2004. Any scholarly or professional book can be registered at the title or chapter level, and registration of works with multiple chapters is steeply discounted. OUP’s Oxford Dictionary of National Biography (ODNB, at http://www.oxforddnb.com) is one example of an MRW containing DOIs assigned at the granular entry level; the ODNB alone contains over 65,000 DOIs. Other publishers actively registering books with Crossref include Springer-Verlag, Taylor & Francis, Elsevier, the American Psychological Association, Humana Press, Karger, and John Wiley & Sons.

According to Craig Van Dyck, Vice President of STM Operations at Wiley, "Crossref has a done great job of enhancing the online journal literature with reference linking and other important functionality. Now Crossref is making significant progress with the scholarly book literature as well. Books are more complex and so will require commitment by Crossref members to provide users with the linking functionality that they desire and that Crossref can enable. Multiple Resolution, with DOIs pointing to a variety of relevant resources and linking options, is especially important for books."

Crossref began offering Multiple Resolution “Crossref-MR” – services on a trial basis late last year. The majority of today’s DOIs employ single resolution, where a DOI is associated with one URL that typically points to online content at a publisher website. Crossref-MR makes links more intelligent by providing a standardized way of associating a variety of resources with a DOI and making links to those resources available to many different services. Crossref-MR thus enables the dissemination of enriched, publisher-controlled linking information, which can be pulled in and used by A&I databases, subscription agents, OpenURL resolvers, and other third parties. Crossref envisions widespread use of Crossref-MR as instances of content being hosted in multiple locations increase. Other applications include linking to supplemental or related information, and to purchase options and rights clearance services.





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.