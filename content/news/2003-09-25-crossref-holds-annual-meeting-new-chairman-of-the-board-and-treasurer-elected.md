+++
title = "Crossref Holds Annual Meeting - New Chairman of the Board and Treasurer Elected"
date = "2003-09-24"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*25 September 2003*

Crossref Holds Annual Meeting - New Chairman of the Board and Treasurer Elected

**LYNNFIELD, MA, September 25, 2003** - At its third annual meeting on September 15, 2003 in London Crossref announced that it had grown to over 240 member publishers with 8.9 million DOIs registered from 8,400 scholarly journals. That's dramatic growth from the 6.5 million DOIs registered in 2002 and Crossref's founding in 2000 with 12 initial members. User clicks on reference links enabled by the Crossref linking network have been running at over 3.5 million per month since February 2003. Crossref is having a demonstrable impact for readers of online scholarly content.

At its Board of Directors meeting following the Annual Meeting, the Publishers International Linking Association, the non-profit membership organization that runs the Crossref reference linking service, elected Anthony Durniak as Chairman and President. Durniak, Staff Executive of Publications for IEEE, has served as Crossref's Treasurer since 2002. He replaces Pieter Bolman, who was Crossref's founding Treasurer before being named Chair of the Crossref Board last year. Also elected were Robert Campbell, President of Blackwell Publishing, as Treasurer, and Edward Pentz, Executive Director, Crossref, as Secretary. A complete listing of the board is available.





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.