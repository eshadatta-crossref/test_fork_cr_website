+++
title = "Crossref™ Search Pilot Adds 16 New Publishers"
date = "2004-07-08"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*08 July 2004*

**LYNNFIELD, MA, July 8, 2004** -- Crossref announced today that its new pilot initiative in collaboration with Google™ search technologies has added 16 additional publishers. Crossref Search now enables users to search the full text of high-quality, peer-reviewed journal articles, conference proceedings, and other resources covering the full spectrum of scholarly research from 25 leading publishers.

Crossref Search is available to all users, free of charge, on the websites of participating publishers, and encompasses current journal issues as well as back files. The results are delivered from the regular Google index but filter out everything except the participating publishers’ content, and will link to the content on publishers’ websites via DOIs (Digital Object Identifiers) or regular URLs. Crossref itself doesn’t host any content or perform searches – Crossref works behind the scenes with Google to facilitate the crawling of content on publishers’ sites and sets the policies and guidelines governing publisher participation in the initiative. As well as enabling Crossref Search, the partnership with Google also means that full-text content from the publishers is also referenced by the main Google.com index in its more general searches. Participating publishers now include:

American Physical Society   
Annual Reviews   
Ashley Publications  
Association for Computing Machinery  
BioMed Central  
Blackwell Publishing  
BMJ Publishing Group  
Cold Spring Harbor Laboratory Press  
FASEB  
INFORMS  
Institute of Physics Publishing  
International Union of Crystallography  
Investigative Opthamology and Visual Science  
Journal of Clinical Oncology  
Lawrence Erlbaum Associates  
Medicine Publishing Group  
Nature Publishing Group  
Oldenbourg Wissenschaftsverlag  
Oxford University Press  
PNAS  
Royal College of Psychiatrists  
University of California Press  
University of Chicago Press  
Vathek Publishing  
John Wiley & Sons  

The Crossref Search pilot will run through 2004 to evaluate functionality and to gather feedback from scientists, scholars and librarians for the purpose of fine-tuning the program. Participating publishers are also investigating how DOIs can be used to improve indexing of content and enable persistent links from search results to the full text of content at publishers’ sites. Crossref is also in discussion with other search engines.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.