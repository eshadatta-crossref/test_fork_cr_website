+++
title = "FundRef funder identification service from Crossref to help connect funding and publications to improve public access"
date = "2013-07-18"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*18 July 2013*

**18 July, Lynnfield, MA** — Crossref announced today that it supports the use of its funder identification service, [FundRef](/education/funder-registry/), to enable the transparent tracking of funding and publications by organizations responding to government public access policies.
FundRef includes several components:

1. A registry of 4000 worldwide funder names, an extension of Crossref’s metadata schema to support funder and grant numbers;
2. A user interface and open application programming interfaces (API) to allow funders, researchers, publishers, and the general public to locate articles and other scholarly documents funded by particular agencies or private funding bodies; and
3. The distribution of this funding metadata through Crossref’s regular channels to providers of discovery services and others.

Crossref has already made the FundRef Registry (the taxonomy of funder names) available for open use via a Creative Commons Zero (CC0) license. Crossref has now announced plans to extend the FundRef service to meet the needs of organizations working to respond to new public access requirements.

As a first step, Crossref is working with the Clearinghouse for the Open Research of the United States (CHORUS) initiative on how to use the existing Crossref System, and further develop FundRef, to support a distributed infrastructure that will allow readers to easily and freely access peer reviewed publications that result from funding provided by US government agencies. Crossref is open to working with any organizations or initiatives anywhere in the world that want to use the Crossref infrastructure and FundRef data. This includes organizations working on other approaches in response to the US White House Office of Science and Technology Policy (OSTP) memo of February 2013.

In 2012 Crossref coordinated a successful FundRef Pilot involving collaboration between publishers, funders, and Crossref. FundRef was launched in May 2013 and is currently accepting funding metadata from publishers. FundRef also provides discovery tools for funding agencies and others.  The FundRef model is designed to benefit multiple users by taking advantage of the existing Crossref infrastructure of more than 4400 international publishers, 30,000 journal titles, and 61 million linked documents.

“We look forward to working with all parties in all countries to further our mission of improving scholarship through community standards, collaboration and sustainable infrastructure,” said Crossref Executive Director Ed Pentz. “Crossref has always maintained neutrality when it comes to our members’ business models. While CHORUS is the first organization to specifically ask for our help in creating a solution working with publishers and US agencies, we look forward to sharing what we learn with others throughout the process.”

[More information about FundRef is available](/education/funder-registry/)

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.