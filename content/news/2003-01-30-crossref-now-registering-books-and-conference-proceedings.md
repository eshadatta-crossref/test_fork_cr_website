+++
title = "Crossref Now Registering Books and Conference Proceedings"
date = "2003-01-30"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*30 January 2003*

**LYNNFIELD, MA, January 30, 2003** -- Crossref, the DOI registration agency for scholarly and professional publications, has begun adding books and conference proceedings to its extensive linking network. The Crossref system currently spans 180 publishers, 7,000 journals, and nearly 7 million content items. To date, 300,000 proceedings chapters have been registered.

180 publishers can't be wrong. Crossref harnesses collaboration among publishers to provide the scholarly community with easier access to online research content. As a Crossref member, you have the unique opportunity to benefit both from a business infrastructure for linking with other content providers, and from a technology infrastructure for DOI-based links.

About the DOI. The DOI, or digital object identifier,serves as a persistent, actionable identifier for online content. The DOI itself is a NISO standard and is entirely compatible with the OpenURL. But unlike a URL, which becomes obsolete when an object changes location, the DOI is a permanent name for an object, associated with its location in a readily updateable directory. DOIs can be assigned at any level of granularity, and therefore provide publishers with an extensible platform for a variety of online applications.

Crossref was established by scholarly publishers in 1999 as an independent, non-profit membership association. Membership is open to publishers in all scholarly and professional areas. You can count on affordable, persistent links; increased traffic to your content; and the opportunity to collaborate with other publishers dedicated to enhancing user services.

For more information about membership in Crossref or DOI registration for books, please email [info@crossref.org](mailto:info@crossref.org).

**If you are already a Crossref member, remember to: **

**-Include DOIs in all data feeds to third parties**

**-Use DOIs as your preferred means of linking to FT**

**-Display DOIs in online & print articles as a standard part of bibliographic data**




---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.