+++
title = "No Charge to Libraries for DOI and Metadata Lookup"
date = "2003-08-05"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*21 May 2003*

**LYNNFIELD, MA, May 21, 2003** -- Crossref, a publisher collaborative that enables researchers to navigate online content via DOI-based citation links, announced today that the $500 fee for library affiliates will be waived for the rest of this year and eliminated in 2004. Crossref wants to encourage the use of DOIs and make it easy for libraries to create full text links and integrate DOIs into their OpenURL link resolvers. In addition, library systems and link resolver vendors such as Ex Libris, EBSCO, Endeavor Information Systems, Fretwell Downing, TD Net and others will be able to integrate Crossref access into their systems with no extra charges to libraries. Libraries will simply sign a basic agreement to get a Crossref account, but there will be no charge for the account.

The Crossref system now covers over 7,600 journals and books, and 7.9 million individual content items.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.