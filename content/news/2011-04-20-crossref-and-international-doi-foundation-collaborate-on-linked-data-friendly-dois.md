+++
title = "Crossref and International DOI Foundation Collaborate on Linked-Data-Friendly DOIs"
date = "2011-04-20"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*20 April 2011*

**20 April 2011, Lynnfield, MA USA**—Crossref and the International DOI Foundation (IDF) have announced that all 46 million Crossref Digital Object Identifiers (DOIs) are now enabled for use in linked data applications, effective immediately.

The term "linked data" describes a set of best practices for exposing data in machine-readable form using the standard HTTP web protocol. These best practices support the development of tools to link and make use of data from multiple web sources without the need to deal with many different proprietary and incompatible application programming interfaces (APIs).

“We are delighted with this collaboration,” says Geoffrey Bilder, Director of Strategic Initiatives at Crossref. “By making sure that Crossref DOIs can be used according to linked data principles, we hope to encourage linked data applications to make use of Crossref DOIs as the basis for standard, persistent citations to the content our members publish on the web. We look forward to other DOI Registration Agencies (RAs) enabling similar functionality so that their members’ content also be persistently linked to by linked data tools.”

Norman Paskin of the IDF commented, “We feel that a significant advantage of applying Linked Data principles and technologies to DOI-registered material is that it is ‘data worth linking to’: it is curated, value-added, data, which is managed, corrected, updated and consistently maintained by Registration Agencies. It is also persistent, so avoiding ‘bit-rot’. This significant announcement by Crossref is part of improvements the International DOI Foundation is continuing to make to facilitate more sophisticated uses of a DOI.”

How It Works
The Corporation for National Research Initiatives (CNRI), the IDF, and Crossref have enabled the DOI web proxy (which is located at http://dx.doi.org) to support content negotiation for DOIs. In the early days of the web, human beings were following most URLs, and it made sense that the DOI web proxy only resolved Crossref DOIs to human-readable web pages. Now, however, a program is just as likely to follow a URL as a person is. The program may have to scrape the HTML landing page that was designed for humans for data that it needs. This aproach is suboptimal and error-prone. The solution for this problem until now has been that Crossref and other RAs have provided a variety of APIs that allow programs to query for DOIs and receive machine-readable content in return. Some examples of these APIs include OpenURL and piped queries.

The problem with these APIs is that they require programmers to familiarize themselves with sometimes extensive API documentation that varies from one data provider to the next. This approach is not scalable when dealing with many sources of information. Content negotiation helps because it is a standard part of the HTTP protocol that underlies the web. A program can now resolve a DOI through the standard web proxy and specify that the data should be returned in a machine-readable format, and the programmer can use the same methods they use for querying for data on any linked-data-enabled site.

Current users will notice no difference in the behavior of the DOI resolver system unless they explicitly start using content negotiation, thus ensuring backwards compatibility.

An important feature of the implementation is that either the RA responsible for the DOI or the organization to whom the content identified by the DOI belongs can respond to any content negotiation request. Initially, programs requesting machine-readable data from Crossref DOIs using content negotiation will receive the metadata that the Crossref member publisher has registered at Crossref. As publishers start to implement content negotiation on their own sites, they may want to return richer and more complete representations of their content, at which point Crossref can direct content-negotiated requests directly to the publishers’ sites.

For further details and examples please visit http://www.crossref.org/CrossTech/2011/04/content_negotiation_for_crossr.html

**About Crossref**
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 1999, Crossref has provided reference linking services for more than 46 million content items, including journal articles, conference proceedings, books, book chapters, reference entries, technical reports, standards, and data sets. Crossref's goal is to be a trusted collaborative organization with broad community connections; authoritative and innovative in support of a persistent, sustainable infrastructure for scholarly communication.


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.