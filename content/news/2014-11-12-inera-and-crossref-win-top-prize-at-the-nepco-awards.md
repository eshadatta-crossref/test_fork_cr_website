+++
title = "Inera Inc. and Crossref Win Top Prize at the NEPCo Awards"
date = "2014-11-12"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*12 November 2014*

**November 12, 2014, Belmont and Lynnfield, MA, USA** — Inera Inc. and Crossref took first place at the first New England Publishing Collaboration (NEPCo) Awards on October 21 in Cambridge, MA. The NEPCo Awards, sponsored by Bookbuilders of Boston [(www.bbboston.org)](https://web.archive.org/web/20141217031522/https://www.bbboston.org/), highlight achievements by two or more organizations working as partners, with at least one collaborator having a presence in New England.

“We’re thrilled to be recognized for our truly bi-directional partnership with Crossref,” said Bruce Rosenblum, CEO of Inera, who delivered the winning presentation at the awards ceremony. The award submission described the 14-year partnership between Inera and Crossref, highlighting the creation of the Crossref metadata deposit schema, the integration of Inera's automated software testing with Crossref's quality assurance procedures, and the adoption and expansion of the DOI standard throughout the scholarly publishing community. The criteria for the awards were results achieved, industry significance, depth of collaboration, and presentation.

“Our collaboration with Inera is of long standing and has benefited the entire scholarly communications community,” said Ed Pentz, Executive Director of Crossref. “Based as it is on standards, the work we have done together has made the robust system of scholarly reference linking we have today possible. It also has allowed us to expand important services to readers of content beyond journals, to help scholars find relevant quality, and to expand the interoperability of tools in the scholarly ecosystem.”

Added Rosenblum, “Inera and Crossref are extremely proud of the joint work our organizations have done to address the needs of the scholarly publishing community in a rapidly changing technological environment.”

About Inera  
Inera [(www.inera.com)](http://www.inera.com/) is widely recognized as a global leader in publishing technology. Inera’s seasoned team of publishing and software professionals develop and license the eXtyles family of Word-based editorial and XML tools and the Edifix online bibliographic reference solution. Publishers of scholarly journals and books, standards, and government documents worldwide rely on Inera’s software solutions to drive modern electronic and print publication workflows.

**About Crossref**  
Crossref serves as a digital hub for the scholarly communications community. A global not-for profit membership organization of scholarly publishers, Crossref's innovations shape the future of scholarly communications by fostering collaboration among multiple stakeholders. Crossref provides a wide spectrum of services for identifying, locating, linking to, and assessing the reliability and provenance of scholarly content.





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.