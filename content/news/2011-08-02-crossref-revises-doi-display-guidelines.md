+++
title = "Crossref Revises DOI Display Guidelines"
date = "2011-08-02"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*02 August 2011*

**2 August 2011, Lynnfield, MA USA**—Crossref, the association of scholarly publishers that provides reference linking, today announced an update to its Digital Object Identifier (DOI) online display guidelines.

The new guidelines encourage Crossref member publishers, affiliates, and others in the scholarly community to display Crossref DOIs as full URLs in the online environment. (A Crossref DOI is a persistent link to scholarly content.) To create a DOI URL, anyone can simply prepend http://dx.doi.org/ to any DOI.

“The new format will make it easier for researchers to know what to do with a Crossref DOI wherever they encounter it,” according to Crossref Executive Director Ed Pentz. “Whether or not they know what Crossref is, or what a DOI is, they will be able to simply click on the link.”

This change will allow users to copy permanent Crossref DOI links from HTML pages to emails, blogs, reference management software and other applications. In addition, mobile device users will be able to take advantage of the citation linking system already in place for scholarly publications on the web.

“Displaying Crossref DOIs as links simplifies navigation in scholarly publications for humans and machines alike. Though these guidelines are recommendations, and not requirements, we encourage everyone who uses Crossref DOIs in online content and applications to adopt them,” Pentz added.

The guidelines supersede the previous practice of displaying “doi:” in front of Crossref DOIs. Early DOI adopters had anticipated that “doi:” would become a formal URI scheme so that WWW browsers would link Crossref DOIs in this form, which has not happened.

“The old format was confusing for users, since it did not automatically become a link in web browsers,” noted Pentz.

The revised guidelines also include several examples of how Crossref DOIs should be displayed in reference lists.

“We understand that different publishers have different publications styles and different usability concerns, so we have made the guidelines flexible enough to accommodate these varying needs. A group of publishers and other organizations have given careful thought to this change to ensure that Crossref DOIs are as useful as possible to scholars,” Pentz explained.

The [complete Crossref DOI Display Guidelines](https://doi.org/10.13003/5jchdy) can be found on the Crossref web site.

**About Crossref**
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 2000, Crossref has provided reference linking services for nearly 48 million content items, including journal articles, books and book chapters, conference proceedings, reference entries, technical reports, standards, and data sets. Crossref's goal is to be a trusted collaborative organization with broad community connections; authoritative and innovative in support of a persistent, sustainable infrastructure for scholarly communication.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.