+++
title = "Geoffrey Bilder To Join Crossref"
date = "2006-12-05"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*05 December 2006*

**Lynnfield, MA. December 05, 2006** -- Crossref, the publisher linking association, is extremely pleased to announce the addition of Geoffrey Bilder to its management team. Bilder will assume the newly created role of Director of Strategic Initiatives at Crossref in January of 2007.

According to Ed Pentz, Crossref's Executive Director, "Geoffrey is an excellent addition to the Crossref team - he is very smart and has a deep understanding of new technologies and how they affect scholarly publishing.
He will be able to play a large role in moving forward with several new Crossref initiatives."

Bilder has been a Publishing Technology Consultant at Scholarly Information Strategies Ltd. since October of 2005. He has over 15 years experience as a technical leader in scholarly technology. He co-founded Brown University's Scholarly Technology Group in 1993 to provide advanced technology consulting on issues related to academic research, teaching and scholarly communication. He later served as head of R&D in the IT department of Monitor Group, a management consulting firm based in Cambridge, MA. From 2002 to 2005, Geoffrey was Chief Technology Officer at Ingenta. He has since worked and consulted extensively with publishers and librarians on how emerging social software technologies are likely to affect scholarly and professional researchers.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.