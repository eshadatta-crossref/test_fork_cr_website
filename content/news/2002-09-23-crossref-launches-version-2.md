+++
title = "Crossref Launches Version 2.0 of its Reference Linking System and Expands Membership Base"
date = "2002-09-23"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*23 September 2002*

**BURLINGTON, MA, September 23, 2002** - Crossref, a publisher collaborative that enables researchers to navigate online journals via DOI-based citation links, announced today the launch of Version 2.0 of its metadata database resolution service as well as expansion of its publisher and affiliate memberships.

Development of the new system began in 2001, when Crossref joined forces with Atypon Systems (www. Atypon.com) for their superior technological capabilities in the e-publishing industry. Thanks to the system’s robust “fuzzy” logic, there has been a significant increase in positive query results. This second-generation system uses a scalable architecture and the latest Web technologies, resulting in more resolved queries and expanded reference content.

Chuck Kosher, Director of Technology for Crossref, says, “In just the first three weeks of operation, the system’s overall matching rate approached 25% on almost 2.3 million queries. This is a remarkable increase over the old system, which typically had about a 4% matching rate.” Version 2.0 also supports the new Crossref XML Deposit Schema, which provides a more robust vocabulary for journal metadata and conference proceedings. Reference material can now be deposited. Members will have Web access to a complete set of statistics and reports that detail their deposit and query activity, and the extendable platform readily allows the addition of new features and services.

In addition, Crossref’s membership base continues to grow. It now has 152 members whose content represents over 6,400 journals and almost 5 million article records. Informs, a 12,000-member society representing professionals in the fields of Operations Research and the Management Sciences, is the most recent member. Other key expansion developments include:

* Library affiliate program has grown to 49 with the recent addition of NIH.

* A linking solutions partner agreement was signed with Fretwell Downing, for their plans to integrate Crossref into their Z PORTAL product offering for libraries that will automatically build links between citation information and digital content.

* ProQuest has committed to support linking to the full-text via Crossref, and will be beta testing dynamic retrievals using Crossref’s new system.

* Project Muse, which offers subscription access to the full text of more than 200 scholarly journals in the humanities and social sciences, announced it plans to actively participate in Crossref beginning in 2003.

Amy Brand, Director of Business Development for Crossref, says, “These rapid developments show Crossref has become the linking standard for scholarly publications online. Perhaps the best testament to this is end-user clicks on DOI’s, which currently average 1-2 million per month.”


---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.