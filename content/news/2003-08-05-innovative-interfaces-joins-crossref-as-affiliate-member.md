+++
title = "Innovative Interfaces Joins Crossref as Affiliate Member"
date = "2003-08-05"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*05 August 2003*

**Emeryville, CA, August 5, 2003** - Innovative Interfaces, the leader in information technology for libraries, announced that the company has joined the Crossref organization as an affiliate member. This partnership seamlessly integrates WebBridge—a component of the MAP portal solution—with DOIs (Digital Object Identifiers) and Crossref linking to assist libraries in providing complete access to available resources.

Betsy Graham, Vice President, Product Management at Innovative, noted, “Crossref is an important service for libraries, publishers, and software providers. It will enhance resource access and the accuracy of MAP by cross-checking links, expanding content access, and increasing usage of existing content without increasing cost.”

"We at Crossref are delighted to include Innovative Interfaces among our vendor affiliates that service libraries,” said Amy Brand, Director of Business Development at Crossref. “Now that Crossref participation is free to all libraries, and librarians are increasingly sophisticated in their understanding of the DOI's role in persistent links as well as local link servers, we look forward to ever-widening adoption of the DOI as a standard linking resource."

MAP (Millennium Access Plus), which manages access to information resources, is made up of three key modules: WebBridge, MetaFind, and Web Access Management. WebBridge offers a smart linking capability, enabling libraries to link information resources together when appropriate. MetaFind is a universal search interface that allows access to multiple resources with a single search, returning all results in a consistent interface. Finally, Web Access Management is a remote-patron authentication tool that links patrons anywhere in the world directly to external Web servers and databases.


Innovative Interfaces is the leader in providing Web-based automated library systems. Innovative's library automation systems are installed in thousands of libraries in 36 countries around the world. The company is headquartered in Emeryville, California, with offices in Australia, Canada, France, Korea, Portugal, Spain, Sweden, Taiwan, Thailand, and the United Kingdom.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.