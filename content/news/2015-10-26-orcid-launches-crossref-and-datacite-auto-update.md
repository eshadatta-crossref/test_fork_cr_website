+++
title = "ORCID launches Crossref and DataCite Auto-Update"
date = "2015-03-26"
parent = "News"
weight = 3
x-version = "0.0.0"

+++

*26 October 2015*
_Researchers can now opt to have their ORCID record automatically updated when their work is made public_

ORCID, the non-profit organization that is working to address the name ambiguity problem in scholarly communications by providing a registry of persistent identifiers for researchers, today announced the launch of Auto-Update functionality, in collaboration with Crossref and DataCite.

Now, ORCID registrants who use their unique ORCID identifier (iD) when submitting a manuscript or dataset can opt to have their ORCID record automatically updated when their work is made public.

In addition, other systems that have integrated the ORCID registry and connected a researcher's ORCID record -- their faculty profile system, library repository, webpage, funder reporting system -- can also choose to receive alerts from ORCID, allowing research information to move easily and unambiguously across multiple systems.

Crossref and DataCite, both non-profit organizations, are leaders in registering DOIs (Digital Object Identifiers - a unique alphanumeric string assigned to a digital object) for research publications and datasets. Each DOI is associated with a set metadata and a URL pointer to the full text, so that it uniquely identifies the content item and provides a persistent link to its location on the internet. Between them, Crossref and DataCite have already received almost a half a million works from publishers and data centers that include an ORCID iD validated by the author/contributor. With Auto-Update functionality in place, provided the researcher includes her/his ORCID iD at submission, information about these works can transit (with the researcher's permission) to her/his ORCID record.

ORCID Executive Director, Laure Haak, warmly welcomed this new development: "Auto-update's benefit to scholarly communications is something that publishers, librarians, funders, and researchers can all agree on: it is a game changer."

Ed Pentz, Executive Director of Crossref, agrees: "This integration with ORCID and our ongoing collaboration with DataCite aims to build on existing infrastructure, tying systems together to save time for researchers. We have a drive to encourage even more publishers to deposit ORCID iDs with us and would like to invite people to a webinar to learn more".

And Patricia Cruse, Executive Director of DataCite adds: "We all share the goal of "good science" and making it easier for researchers to share their work is a big part of that goal. By ORCID, Crossref and DataCite working together on Auto-update we are able to provide an easy way for researchers to further expose their work."

---

More information is available on the [ORCID blog](https://info.orcid.org/auto-update-has-arrived-orcid-records-move-to-the-next-level/). Please contact [our communications team](mailto:news@crossref.org) with any questions.