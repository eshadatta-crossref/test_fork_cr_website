+++
title = "Sermo Partners with Crossref to use DOIs as Linking Standard for Physician Discussion of Medical Research"
date = "2008-03-07"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*07 March 2008*

**Lynnfield, MA. March 7, 2008** — Crossref, the publisher linking service, announced today that it has entered into a strategic partnership with Sermo (http://www.sermo.com), the largest online physician community. Through this new partnership, Sermo will make use of the DOIR (Digital Object Identifier) to link real-time physician discussions about current research to the source medical journal articles.

DOIs enhance Sermo's "Discuss on Sermo" initiative, which facilitates post publication discussion of thousands of medical research articles. Through this program, publishers can:

* Capture a "collective letter to the editor" with input from hundreds or thousands of readers.
* Promote medical journal content to the most active online audience of US
physicians.
* Leverage collective discussions of medical content to augment and inform
original research.

"Our relationship with Sermo promotes the DOI as the linking standard for medical and other professional literature," says Amy Brand, Crossref's Director of Business and Product Development. "Crossref is also invested in making its members aware of the Discuss on Sermo initiative, which links high-quality online discussions with their medical content in a very easy way."

"Using DOIs, Sermo can effectively tie its physicians' discussions of current research with the appropriate source journal articles," said Alex Frost, Sermo Vice President for Research. "As we expand the Discuss on Sermo program, we hope to work closely with Crossref to help enable more advances in effectively linking research content from diverse sources."

**About Sermo:**
Launched in September 2006, Sermo is already the largest online physician community in the US, with over 50,000 physician members. On Sermo, physicians exchange knowledge with each other and gain potentially life saving insights directly from colleagues. Sermo harnesses the power of collective wisdom and enables physicians to discuss new clinical findings, report unusual events, and work together to improve patient care. Through its unique business model, Sermo is free to physicians and has no advertising or promotion. Learn more at http://www.sermo.com.


_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_

_Crossref® and Crossref.org® are registered trademarks and the Crossref logo is a trademark of PILA (Publishers International Linking Association_



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.