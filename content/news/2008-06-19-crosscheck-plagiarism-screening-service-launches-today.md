+++
title = "CrossCheck Plagiarism Screening Service Launches Today"
date = "2008-06-19"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*19 June 2008*

**LYNNFIELD MA, June 19, 2008** – Crossref announced today the official launch of CrossCheck, a new initiative to aid publishers in verifying the originality of scholarly content. Following on the success of Crossref’s recent pilot of CrossCheck, this service is now open to all Crossref member publishers.

Crossref partnered with iParadigms, LLC, in creating CrossCheck powered by iThenticate (https://crosscheck.ithenticate.com). Crossref members who choose to participate can verify the originality of works submitted for publication using the iParadigms iThenticate service to check against a vast database of proprietary as well as open web content.

The CrossCheck database is already slated to cover over 20 million journal articles from the following participating publishers: Association for Computing Machinery, American Society of Neuroradiology, BMJ Publishing Group, Elsevier, Institute of Electrical & Electronics Engineers, International Union of Crystallography, Nature Publishing Group, Oxford University Press, Sage, Informa UK (Taylor & Francis), and Wiley Blackwell. With the launch today, both publisher participation and the CrossCheck database should grow quickly.

Until now, there was no automated way to check submissions against previous publications because the published literature had not been indexed and “text fingerprinted” for this purpose. According to Ed Pentz, Crossref’s Executive Director, “CrossCheck is only as strong as the collective whole. It was important that the pilot publishers joined on but now we need the rest of the Crossref members to join this initiative. We are happy that publishers such as Sage, Oxford University Press and Nature Publishing Group have agreed to participate in addition to the pilot publishers. We anticipate expanding the database of content very rapidly.”

**About iParadigms, LCC**
Headquartered in Oakland, CA, iParadigms, LLC (http://www.iparadigms.com) is the leading provider of web-based solutions to check documents for originality and plagiarism.

_Crossref® and Crossref.org® are registered trademarks and the Crossref logo is a trademark of PILA (Publishers International Linking Association)_

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.