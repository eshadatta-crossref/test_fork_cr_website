+++
title = "Crossref Announces FundRef Pilot to Standardize Funding Source Information for Scholarly Publications"
date = "2012-05-02"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*02 May 2012*

**2 May, Washington, DC, USA**—Crossref has announced FundRef, a pilot collaboration between scholarly publishers and funding agencies that will standardize the names of research funders and add grant numbers attributed in journal articles or other scholarly documents. The collaboration would allow researchers, publishers, and funding agencies to track the published research that results from specific funding bodies.

The international FundRef pilot participants include seven publishers (American Institute of Physics, American Psychological Association, Elsevier, IEEE, Nature Publishing Group, Oxford University Press, and Wiley) and four funding organizations (US Department of Energy, Office of Scientific and Technical Information [DOE/OSTI]; US National Aeronautics and Space Administration [NASA]; US National Science Foundation [NSF]; and Wellcome Trust).

Walter Warnick, Director of DOE/OSTI, said, “We see great value in this collaboration. Being able to track the scholarly publications that result from funding activities will provide us with an important measure of our impact on research progress.”

Fred Dylla, Executive Director of the American Institute of Physics, added, “This project is a great example of a productive collaboration involving scholarly publishers, funding agencies, and Crossref that will provide an important new service to the research community—the ability to link publications on publisher’s sites to related information such as grantee awards and grantee reports on funding agency sites.”

> “Although publishers almost always include an acknowledgements section in their journals, where authors credit their funding sources, the lack of standardization of funding organizations names and their abbreviations make it very difficult to use that information for reporting or analysis,” Crossref Executive Director Ed Pentz noted. “FundRef will address this challenge.”

The participants in the pilot project plan to create a proof of concept system that will demonstrate the workflow for how funding agency names and grant and award numbers will be standardized and linked to publications. The proof of concept should be available by October 2012.

More information is available at [http://www.crossref.org/services/funder-registry](/services/funder-registry).

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.