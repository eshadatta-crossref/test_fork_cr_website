+++
title = "Crossref Announces Improved Multiple Resolution Service"
date = "2008-08-04"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*08 August 2008*

**LYNNFIELD, MA – August 04, 2008** - Crossref, the reference-linking network for scholarly and professional publishers, is pleased to announce that its improved multiple resolution service (MR) has launched, enabling publishers to assign multiple URLs to a single DOI.

As of May 2008 the Crossref system is supporting the assignment of more than one URL to a single DOI, a concept known as multiple resolution (MR).  Crossref's MR service works by providing an interim page solution, which presents a list of link choices to the end user. Each choice represents a location at which the published journal article or another type of published content may be obtained.

Since 2006 Crossref operated a pilot MR program with virtually the same service. The pilot explored various multiple resolution techniques and brought to light a range of technical challenges. It also revealed operational needs that have been reflected in the design of the production service.

The key to the updated MR service is the enforcement of roles and permissions. The MR service gives the content’s primary owner full control while allowing secondary providers to supply only the data essential to their platform. The production service builds on what was learned during the pilot exercise and offers those services which meet the immediate needs expressed by our membership.

According to Chuck Koscher, Crossref’s Director of Technology:  “Our production multiple resolution service is a first step at moving beyond simple one DOI one URL resolution and begins to take advantage of the DOI’s fuller capabilities. Early in our pilot project we recognized that metadata management would be the key behind a successful multiple resolution offering and this was our focus for the production service. These transactions add some complexity to the deposit process and increased automation is essential. We believe we are in the early days of MR and as more publishers avail themselves of such services we will discover better methods.”

For more information on the new multiple resolution service or to sign up for the service please visit: https://www.crossref.org/.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.