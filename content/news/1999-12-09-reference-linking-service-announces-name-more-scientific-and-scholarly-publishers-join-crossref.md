+++
title = "Reference Linking Service Announces Name - More Scientific and Scholarly Publishers Join Crossref Publishers Join Crossref"
date = "1999-12-09"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*09 December 1999*

**London, England. 9 December 1999** A group of leading scientific and scholarly publishers today announced a name for their reference linking service to link their journal articles Crossref as well as the names of four more publishers who have joined this innovative initiative.

The University of Chicago Press, the Institute of Physics Publishing, World Scientific, and Taylor & Francis will be joining this unprecedented cooperative effort, which also includes Academic Press, a Harcourt Science and Technology Company (NYSE:H); American Association for the Advancement of Science (the publisher of Science); American Institute of Physics (AIP); Association for Computing Machinery (ACM); Blackwell Science; Elsevier Science (NYSE:ENL) (NYSE:RUK); IEEE (The Institute of Electrical and Electronics Engineers, Inc.); Kluwer Academic Publishers (a Wolters Kluwer Company); Nature Publishing Group; Oxford University Press; SpringerVerlag; and John Wiley & Sons, Inc. (NYSE:JWa) (NYSE:JWb).

Active discussions are underway with many scientific and scholarly primary journal publishers to make this a broadbased, industrywide initiative. More are expected to sign on before the service launches during the first quarter of 2000. Crossref was initially announced on November 16.

Commenting on Crossref, Robert Shirrell, Journals Manager, The University of Chicago Press, said, "Reference links are a critical part of the utility of electronic publications. This linking service, a voluntary cooperative effort based on an open standard, provides the means for enhancing electronic publications across all scholarly fields."

Jerry Cowhig, Managing Director, Institute of Physics Publishing, added, "One of the key objectives of the Institute's Royal Charter is the dissemination of scientific information to physicists worldwide. This exciting, collaborative initiative will further that objective and help us to realize our vision of edistributed publishing. By working together publishers should at last achieve seamless linking between online content, wherever it resides."

Doreen Liu, Managing Director, World Scientific, said, "This is the most exciting initiative from the STM group of publishers I can remember during my 18 years in the business. World Scientific was set up with the objective of serving the scientific community and I believe this initiative will greatly enhance our ability to do so."

Researchers will be able to move easily from a reference in a journal article to the content of a cited journal article, typically located on a different server and published by a different publisher. At the outset, more than three million articles across thousands of journals will be linked through Crossref, and more than half a million more articles will be linked each year thereafter. This will enhance the efficiency of browsing and reading the primary scientific and scholarly literature. Such linking will enable readers to gain access to logically related articles with one or two clicks an objective widely accepted among researchers as a natural and necessary part of scientific and scholarly publishing in the digital age.

Crossref will be run from a central facility which will be managed by an elected Board and will operate in cooperation with the International Digital Object Identifier (DOI) Foundation. It will contain a limited set of metadata, allowing the journal content and links to remain distributed at publishers' sites. Each publisher will set its own access standards, determining what content is available to the researcher following a link (such as access to the abstract or to the full text of an article, by subscription, document delivery, or payperview, etc.). Crossref is being organized as a not-for-profit entity to safeguard the independence of each participating publisher to set their own access standards and conditions.

The service, which is based on a prototype developed by Wiley and Academic Press, was developed in cooperation with the International DOI Foundation and builds on work by the Association of American Publishers and the Corporation for National Research Initiatives. It takes advantage of the DOI standard and other World Wide Web standards and Internet technology. By taking a standards based approach the international initiative is confident that the sophisticated demands of the readers of scientific and scholarly journals for linking of references can be implemented broadly and rapidly.

PLEASE NOTE: There will be an update and presentation on Crossref on Friday, 10 December 1999, from 15:15 to 16:00 at the Copthorne Tara Hotel, Scarsdale Place, London W8 5SR. tel +44 171 937 7211 (Underground: High Street Kensington). Senior executives from among those publishers who are the initial participants will take questions. The press is invited to attend this session of the STM Innovations Seminar, which will include a project demonstration, a status report, and a question and answer period. For more information, contact Chris Gardner at +44 171 496 3318.

**CROSSREF PARTICIPANTS**   

ACADEMIC PRESS  
(A HARCOURT SCIENCE AND TECHNOLOGY COMPANY)  
[www.academicpress.com](https://web.archive.org/web/20020605133402/http://www.academicpress.com/)  
Contact:  
Ken Metzner  
001 (619) 6996830  
[kmetzner@acad.com](mailto:kmetzner@acad.com)  

AMERICAN ASSOCIATION FOR THE ADVANCEMENT OF SCIENCE  
(THE PUBLISHER OF SCIENCE)  
http://www.sciencemag.org  
Contact:  
Nan Broadbent  
Director, News and Information  
001 (202) 3266440  
[nbroadbe@aaas.org](mailto:nbroadbe@aaas.org)  

AMERICAN INSTITUTE OF PHYSICS (AIP)  
http://www.aip.org  
Contact:  
Tim Ingoldsby  
Director of Business Development  
(631) 576 2266  

TINGOLDSBY@AIP.ORG  
ASSOCIATION FOR COMPUTING MACHINERY  
http://www.acm.org  
Contact:  
Bernard Rous  
ACM Deputy Director of Publications  
001 (212) 6260660  
[rous@hg.acm.org](mailto:rous@hg.acm.org)  

BLACKWELL SCIENCE  
https://www.blackwellpublishing.com/  
Contact:  
Robert Campbell  
Managing Director  
001 (212) 6260660  
robert.campbell@blacksci.co.uk  

ELSEVIER SCIENCE  
http://www.elsevier.com  
Contact:  
Karen Hunter  
Senior Vice President, Elsevier Science  
001 (212) 6333787  
[k.hunter@elsevier.com](mailto:k.hunter@elsevier.com)  

IEEE  
(THE INSTITUTE OF ELECTRICAL AND ELECTRONICS ENGINEERS, INC.)  
http://www.ieee.org  
Contact:  
Anthony Durniak  
Staff Executive, Publications  
001 (732) 562 3998  
[h.horwitz@ieee.org](mailto:h.horwitz@ieee.org)  

INSTITUTE OF PHYSICS PUBLISHING  
https://ioppublishing.org/  
Contact:  
Terry Hulbert  
Head of Electronic Marketing  
+44 (0) 117 930 1047  
[terry.hulbert@ioppubli shing.co.uk](mailto:terry.hulbert@ioppublishing.co.uk)  

INTERNATIONAL DIGITAL OBJECT IDENTIFIER FOUNDATION  
http://www.doi.org  
Contact:  
Norman Paskin  
Director, International DOI Foundation  
+44 (0) 1865 843798  
[n.paskin@doi.org](mailto:n.paskin@doi.org)  

KLUWER ACADEMIC PUBLISHERS  
(A WOLTERS KLUWER COMPANY)  
http://www.wolterskluwer.com  
Contact:  
Alexander Schimmelpenninck   
Director Corporate Communications  
Wolters Kluwer N.V.  
+31 20 60 70 335  
[aschimmelpenninck@wolterskluwer.com](mailto:aschimmelpenninck@wolterskluwer.com)  

OXFORD UNIVERSITY PRESS  
http://www.oup.co.uk  
Contact:  
Martin J Richardson  
Publishing Director  
+44 (0)1865 267780  
[richarm@oup.co.uk](mailto:richarm@oup.co.uk)  

NATURE PUBLISHING GROUP  
www.nature.com  
Contact:  
Stefan von Holtzbrinck  
Managing Director, Nature Publishing Group  
+44 (0) 171 843 4632  

SPRINGERVERLAG  
[springerny.com](https://web.archive.org/web/20071019231804/http://link.springerny.com/)  
https://www.springer.com/de 
Contact:  
Howard Ratner  
Director, Electronic Publishing & Production  
SpringerVerlag  
New York, Inc.  
001 (212) 4601615  
[hratner@springerny.com](mailto:hratner@springerny.com)  

TAYLOR & FRANCIS  
arakhne.tandf.co.uk/index.htm  
Contact:  
Stephen Neal  
Group Publishing Director  
+44 (0) 171 583 9855  
[stephen.neal@tandf.co.uk](mailto:stephen.neal@tandf.co.uk)  

THE UNIVERSITY OF CHICAGO PRESS  
http://www.journals.uchicago.edu  
Contact:  
Evan Owens  
Electronic Publishing Manager, Journals Division  
001 (773) 753 3375  
[eowens@journals.uchicago.edu](mailto:eowens@journals.uchicago.edu)  

JOHN WILEY & SONS, INC.   
http://www.wiley.com  
Contact:  
Susan Spilka  
Corporate Communications Director  
(001) (212) 8506147  
[sspilka@wiley.com](mailto:sspilka@wiley.com  )

WORLD SCIENTIFIC  
http://www.worldscientific.com  
Contact:  
Chi Wai (Rick) Lee  
Deputy Director, Electronic Publishing  
+65 4665775  
[cwlee@wspc.com.sg](mailto:cwlee@wspc.com.sg)  





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.