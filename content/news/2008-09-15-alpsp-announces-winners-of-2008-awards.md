+++
title = "ALPSP Announces winners of 2008 awards"
date = "2008-09-15"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*15 September 2008*

FOR IMMEDIATE RELEASE  
**15 September 2008**  

**ALPSP announces winners of 2008 Awards**  

The winners of the 2008 Awards were announced at the ALPSP International Conference Dinner on Thursday 11 September.

ALPSP Award for Service to Scholarly Publishing Was awarded by the Council of ALPSP to **Kurt Paulus** as a great ‘unsung hero’ of scholarly publishing – who has quietly made an enormous contribution both at the publisher he served for almost all of his career, in the community and in his work for ALPSP as Treasurer and Editor of Learned Publishing.

**ALPSP Award for Publishing Innovation**  
Was awarded to **[CrossCheck](https://crosscheck.ithenticate.com/)** from the Crossref/iParadigms Partnership. This new antiplagiarism service enhances the crucial issue of trust necessary in a robust scholarly publishing system, and provides a further demonstration of the value and reassurance which the formal publication process can offer to authors, readers, and research funders.

A Highly Commended Certificate was awarded to **_Bioscience Horizons_** from Oxford Journals. This new journal potentially unlocks the value of research undertaken by undergraduates by giving unprecedented global exposure to the best of it.

Also shortlisted were: [OECD.Stat](https://stats.oecd.org/) from the Organisation for Economic and Co-operation Development; [Ringgold Institutional Identifier](http://www.openidentify.com/) from Ringgold Ltd; and Routine authoring and publication of enhanced figures from the International Union of Crystallography.

**ALPSP Award for Best New Journal**  **
Was awarded to the **[Journal of Informetrics](http://www.elsevier.com/locate/joi)** published by **Elsevier. The judges commented that this is an excellent example of the research and planning needed when launching a new journal, combined with the attention needed to integrate it into a particular research community.

The judges awarded a Highly Commended Certificate to **[ACS](https://web.archive.org/web/20101230104617/http://pubs.acs.org/journal/ancac3)** Nano published by the American Chemical Society which demonstrated impressive implementation and results from a highly professional new journal launch.

Also shortlisted were: [Industrial and Organizational Psychology: Perspectives on Science and Practice (IOP)](https://doi.org/10.1111/%28ISSN%291754-9434), published by Wiley-Blackwell for the Society for Industrial and Organizational Psychology and the [International Journal of Managing Projects in Business](http://www.emeraldinsight.com/ijmpb.htm) published by Emerald Group Publishing.

Panel of Judges for the ALPSP Awards
**Richard Gedye**, Research Director, Oxford Journals (Chair)
**Geoffrey Bilder**, Director of Strategic Initiatives, Crossref
**Sue Corbett**, General Manager for Medicine, Wiley-Blackwell
**Hugh Look**, Senior Consultant, Rightscom
**Mark Ware**, Director, Mark Ware Consulting
**8Hazel Woodward**, University Librarian & Director of Cranfield Press

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.