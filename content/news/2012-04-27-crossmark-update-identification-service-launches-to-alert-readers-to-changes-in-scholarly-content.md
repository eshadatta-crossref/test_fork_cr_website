+++
title = "Crossmark Update Identification Service Launches to Alert Readers to Changes in Scholarly Content"
date = "2012-02-23"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*27 April 2012*

**27 April, Lynnfield, MA USA**—Crossref today launched the Crossmark update identification service. The Crossmark system will alert researchers to important changes that may occur to published scholarly content and will highlight important publication record information. The Crossmark service has been piloted by several Crossref member publishers for the past year.

“Finding update and corrections for scholarly documents can be a frustrating business for researchers, especially in an environment where content is available from so many channels,” said Crossref Executive Director Ed Pentz. “Scholarly publishers routinely note changes to their content, but how this is done varies from publisher to publisher, which can make them difficult to locate. Before Crossmark, researchers had no way to tell if any changes had occurred to a PDF that they had downloaded months earlier. Now by simply clicking a single, recognizable button, any reader can have access to this important information.”

Clicking on the Crossmark button launches a pop-up box that provides status information, for example, that the document is up to date, or that it has a correction, update, retraction, or other change that could affect the interpretation or crediting of the work. It also provides a permanent link, via the Crossref Digital Object Identifier (DOI) to both the publisher-maintained version of the content and the update.

_Sample Crossmark Status Tab_

Another tab in the box can include optional publication record information, which is non-bibliographic data that is useful for the reader. It may include information on peer review, publication history, funding disclosures, links to associated datasets, or rights.

_Sample Crossmark Record Tab_

The cost of the Crossmark system will be borne by the Crossref participating publishers, with the result that the status and publication record information will be openly available to readers, libraries and any organization that might find the data useful.

Almost 20,000 documents from 21 journals have live Crossmark buttons on their web sites, and working buttons will be added to these journals’ PDF files shortly. Of those documents, 290 have noted updates.

The publishers who have implemented Crossmark to date include a diverse representation of Crossref members: small international open access publishers, large commercial publishers, established society publishers, and university presses.

The following publishers have live Crossmark links on their sites:

1. Elsevier Science
2. International Union for Crystallography
3. Oxford University Press
4. The Royal Society
5. Vilnius Gediminas Technical University

Several additional members of the Crossmark pilot group, including Wiley-Blackwell and IEEE, are also working on their implementations.

“As an open access publisher with very liberal reposting rights, we appreciate the fact that wherever our documents are found, whether on an author home page or in an institutional repository, readers will still be able to use the Crossmark button to get authoritative information about the current status of that document,” said Eleonora Dagienė, Director of the Vilnius Gediminas Technical University Press.

"While articles rarely have to be retracted, we are concerned that the evidence suggests that erroneous citations to retracted articles can continue for a very long time," added Stuart Taylor, Commercial Director of The Royal Society. "Crossmark helps us to maintain the integrity of our published content, and thus of the scientific record."

Any Crossref member publisher in good standing that agrees to the Crossmark terms and conditions can now display Crossmark buttons and metadata for their content. More information and [examples are available](/services/crossmark).

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.