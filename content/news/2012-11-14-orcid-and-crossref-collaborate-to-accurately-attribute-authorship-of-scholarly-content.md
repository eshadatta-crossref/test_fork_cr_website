+++
title = "ORCID and Crossref Collaborate to Accurately Attribute Authorship of Scholarly Content"
date = "2012-11-14"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*28 November 2012*

**14 November, 2012, Bethesda, MD, USA and London, UK**—Two not-for profit organizations, ORCID and Crossref, have collaborated to solve the problem of ambiguous author names in scholarly content. ORCID began assigning unique identifiers to researchers in October. As part of the ORCID Registry, individuals can search the metadata from Crossref, the largest organization assigning Digital Object Identifiers (DOIs) to scholarly content, and add their works to their personal ORCID records.

Connecting researchers with their research has been hampered by lack of data standards, and in particular a standard for identifying individuals.  ORCID provides researchers a Registry to create a unique personal identifier, and has been working with the research community to embed these identifiers in research workflows.

One key component of the ORCID interface is the ability to import metadata on research works to populate an individual’s ORCID record.  As an affiliate member of Crossref, ORCID provides its users the ability to search and import bibliographic metadata from Crossref. “Linking the unique document identifier (the Crossref DOI) with the unique person identifier (the ORCID iD) is essential for ensuring accurate attribution of authors and other content creators,” said Ed Pentz, Executive Director of Crossref and a member of the ORCID board.

In addition to importing past works, ORCID is working with Crossref and the publishing community to ensure that ORCID identifiers collected during the manuscript submission process are incorporated into article metadata and included with the information submitted to Crossref.  “As important as importing past works, is implementing an automated process for updating the ORCID record with new publication information.  This is one way researchers will truly benefit from having an ORCID identifier,” said Laure Haak, Executive Director of ORCID. Already at the launch of the ORCID Registry, a number of publishers and manuscript tracking system vendors had integrated ORCID identifiers into their author profile and submission processes (for a list, see the [ORCID Launch Partners](https://info.orcid.org/orcid-community/#Launch_Partners) web page).

In order to access the Crossref bibliographic metadata, ORCID has become an affiliate of Crossref. Likewise Crossref is a founding member of ORCID. Crossref has modified its metadata schema so that publishers can include ORCID iDs with their bibliographic metadata deposits. The Crossref system will allow querying for ORCIDs from its records early in 2013.

In an article published in [_Learned Publishing_](http://onlinelibrary.wiley.com/doi/10.1087/20120404/abstract;jsessionid=1A1110505BE2E045BD964172C7A61647.f04t01) in October, Haak, Pentz, and co-authors described recommended best practices for collecting and displaying ORCID identifiers in research papers (See [http://dx.doi.org/10.1087/20120404](http://dx.doi.org/10.1087/20120404))).  Since it is not possible to deduce name or other identifying information from the ORCID identifier, it is important to visually display the identifier with the author name: as a footnote or in-line in the HTML and PDF versions of published manuscripts; in the article metadata used on journal websites; in the article metadata sent to Crossref and bibliographic databases such as Pub-Med; and in downloadable reference lists using the RIS, BibTeX or Endnote format. The ultimate goal is to have the ORCID unique identifier associated with the article DOI whenever a scholarly contribution is made or reported.

**ORCID**, was established in 2010 as non-profit organization serving the research community. For more information on ORCID, please see http://www.orcid.org. You may contact the Executive Director Laure Haak at [l.haak@orcid.org](mailto:l.haak@orcid.org) and follow @ORCID_Org on Twitter.

Crossref (http://www.crossref.org) is a not-for-profit membership association of scholarly publishers. Since its founding in 2000, Crossref has provided reference linking services for over 56 million content items, including journal articles, books and book chapters, conference proceedings, reference entries, technical reports, standards, and data sets. Crossref also provides additional collaborative services designed to improve trust in the scholarly communications process, including Cited-By Linking, CrossCheck plagiarism screening powered by iThenticate, and Crossmark update identification.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.