+++
title = "Crossref Surpasses 10 Million DOI Mark"
date = "2004-01-19"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*19 January 2004*

**LYNNFIELD, MA, January 19, 2004** -- Crossref, the cross-publisher reference linking service, announced today that it has surpassed the 10 million DOI mark. This million-mark milestone is significant because over 10 million content items have been registered in the Crossref system since 2001. The majority of these DOIs are assigned to journal articles. However, Crossref is also registering DOIs for a wide variety of content types, including several hundred thousand conference proceeding and book DOIs already in its database.

The 10 millionth DOI registered was http://dx.doi.org/10.1265/ehpm.8.184 from the Japanese Society for Hygiene.

Among the other content types now included in Crossref are the new Molecule Pages produced by Nature Publishing Group and the Alliance for Cellular Signaling. To see a DOI in action from the Molecule Pages, click on http://dx.doi.org/10.1038/nature01307.

Growth in Crossref has been steady and significant. According to Ed Pentz, Crossref’s Executive Director, “Growth in DOI links is also expected to continue increasing at a steady rate because of Crossref’s ability to integrate with the OpenURL and our recent decision to drop DOI retrieval fees in 2004.” To date 265 publishers, 218 libraries, and 36 vendors currently participate in Crossref.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.