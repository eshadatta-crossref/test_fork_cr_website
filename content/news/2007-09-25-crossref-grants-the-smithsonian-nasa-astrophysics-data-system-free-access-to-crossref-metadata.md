+++
title = "Crossref Grants The Smithsonian/NASA Astrophysics Data System Free Access To Crossref Metadata"
date = "2007-09-25"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*25 September 2007*

**LYNNFIELD MA, September 25, 2007** -- Crossref, the organization behind scholarly publishing’s shared linking
infrastructure, announced that the Smithsonian/NASA Astrophysics Data System (or NASA ADS) had submitted the
winning proposal in response to an RFP for Innovative Uses of Crossref Metadata issued in June of this year.

The metadata in Crossref’s database consists of basic bibliographic information for each content item, including author(s),
title, journal name, ISSN, volume, issue, page, and DOI-URL pair. It is generally only accessible on a paid subscription
basis through Crossref Web Services, a suite of tools for authorized partners to collect the cross-publisher records via
an OAI-PMH interface. Crossref issued the Request for Proposals as part of a broader initiative to partner with the
academic and library communities on the development of new research tools that process and display article-level data.

The NASA/ADS project, submitted by Drs. Alberto Accomazzi and Michael Kurtz, proposes to use the Crossref metadata
to add DOI® links-to-fulltext to millions of records in the ADS database and to facilitate the exchange of information
between the ADS and the various publishers whose content it indexes. In addition, the ADS will share its own data with
Crossref as a basis for alerting Crossref of missing items and possible data errors.

**About the ADS:** ADS is a NASA-funded project housed at the Harvard-Smithsonian Center for Astrophysics which maintains
three bibliographic databases containing more than 5.8 million records: Astronomy and Astrophysics, Physics, and arXiv
e-prints. The main body of data in the ADS consists of bibliographic records, which are searchable through its Abstract
Service query forms, and full-text scans of much of the astronomical literature which can be browsed though a Browse
interface. Integrated in its databases, the ADS provides access and pointers to a wealth of external resources, including
electronic articles, data catalogs and archives.


_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.