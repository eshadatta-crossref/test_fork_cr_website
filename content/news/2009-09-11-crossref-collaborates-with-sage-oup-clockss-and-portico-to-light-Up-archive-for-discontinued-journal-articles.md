+++
title = "Crossref Collaborates with SAGE, OUP, CLOCKSS and Portico to Light Up Archive for Discontinued Journal Articles"
date = "2009-09-11"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*11 September 2009*

**11 September 2009, Lynnfield, MA USA**—Crossref has collaborated with archiving organizations and publishers to ensure that several journals that have ceased publication remain linkable with the Crossref DOIs (Digital Object Identifiers) originally assigned to the articles. The titles include Auto/Biography and Graft from SAGE and Brief Treatment and Crisis Intervention from Oxford University Press (OUP). All three titles are now available through both CLOCKSS and Portico.  

An archive "trigger event" occurs when a published journal or other content is no longer available from the publisher. Trigger events can occur for a variety of reasons. Both SAGE and OUP have had agreements in place with archive organizations for several years, but the discontinuation of these titles marked the first time those arrangements had been implemented with real-world cases.

"Two important tenets of Crossref’s mission are persistence and cooperation," said Ed Pentz, Executive Director of Crossref. "Making sure that the Crossref DOIs that have been assigned to content that has moved from a publisher journal platform to an archive still resolve to the articles is an important part of that persistence. Persistence is not only achieved through technology but by cooperation: Crossref, publishers, journal hosting services, and the archiving organizations have all worked together to ensure continued access to the scholarly record. These journals are particularly strong examples of the system in action as there are multiple archives available to guarantee ongoing access."

"Graft articles were made available in Portico at the end of 2007 and Auto/Biography in mid-2008," said Carol Richman, Director of Licensing at SAGE. "We believe that we have a responsibility to ensure scholarly content and research materials remain accessible in the future, and have therefore made long-term commitments with a number of archive partners. Crossref’s multiple resolution service allows users following a Crossref DOI link to choose whether to access the archived copies through Portico or CLOCKSS."

"Our first triggered journal is Brief Treatment and Crisis Intervention," said Claire Kearney, Head of IT, Oxford Journals, Oxford University Press.
"We appreciated the opportunity to demonstrate our commitment to archiving by enabling multiple resolution links that lead to both CLOCKSS and Portico. Not only did we need to work out the technical issues of ensuring persistent linking to archived content, but the agreements we have with the archiving organizations and with Crossref provide a "publication will" that ensures this information will continue to be available to scholars."

"The CLOCKSS Archive, the community-governed archiving initiative with broad support from publishers large and small, Crossref, and the library community, has made all three journals openly available from two geographically separate sites," notes Gordon Tibbitts, Co-Chair CLOCKSS Board of Directors. "CLOCKSS truly serves the world's scholars by ensuring content no longer available from any publisher is available to everyone for free." CLOCKSS triggered content is available at http://www.clockss.org/clockss/Triggered_Content.

"Our aim is for researchers to be able to reliably and persistently locate published research when it is no longer made available from other sources," said Eileen Fenton, Portico Managing Director. "It is one thing to turn on access, but another thing altogether to make sure the links that worked yesterday still work tomorrow.  Through close coordination with Crossref, we have been able to verify the accuracy of metadata and ensure that DOIs exist for all preserved content, and ultimately to provide a seamless transition for users from publisher-based to archival access." Access to triggered content is available to the more than 600 Portico participants at https://web.archive.org/web/20091130025322/http://www.portico.org/news/trigger.html.

The following are live examples of Crossref DOIs from each of the archived journals:

Auto/Biography: http://dx.doi.org/10.1191/0967550706ab044oa

Brief Treatment and Crisis Intervention: http://dx.doi.org/10.1093/brief-treatment/mhg012

Graft: http://dx.doi.org/10.1177/1522162802239753

**About Crossref**

Crossref (http://www.crossref.org) is a not-for-profit membership association founded and directed by publishers. Its mission is to enable easy identification and use of trustworthy electronic content by promoting the cooperative development and application of a sustainable infrastructure. Crossref provides reference linking services for more than 37 million content items.




---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.