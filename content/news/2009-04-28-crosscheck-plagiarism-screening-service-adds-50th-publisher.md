+++
title = "CrossCheck Plagiarism Screening Service Adds 50th Publisher"
date = "2009-04-28"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*28 April 2009*

**Lynnfield, MA, April 28, 2009** -- Crossref welcomed the [American Psychological Association](http://www.apa.org/) (APA) this week as its 50th participant in [CrossCheck](/services/similarity-check), the plagiarism screening service powered by [iThenticate](https://www.ithenticate.com/).

CrossCheck, a cooperative service from [Crossref](http://www.crossref.org/) and [iParadigms](https://web.archive.org/web/20100306045319/http://www.iparadigms.com/), launched in June 2008. It provides publishers with web-based text analysis and a unique and growing database of scholarly publications against which to compare manuscripts and papers for duplication.

 “Ensuring that our publications contain original, trustworthy content is paramount,” noted Gary VandenBos, Publisher for the APA. “Editors and reviewers read widely but cannot recall everything written in their domains. CrossCheck provides a level of automated assistance that, while no substitute for editorial expertise, is an excellent screening tool.”

 “CrossCheck’s success is directly proportional to the number of participating publishers, so we are delighted to reach this milestone just nine months after launch,” said Crossref Executive Director Ed Pentz. “Clearly, tools to assist in identifying possible misconduct are valuable to publishers of all sizes and in all disciplines. Participants also recognize that CrossCheck deters authors from submitting duplicate content.”

The growing CrossCheck database contains 13.8 million items from 23,000 journals, books, and conference proceedings. In addition to checking against documents from scholarly publishers, participants compare papers against relevant sources from the web and proprietary databases.
According to Bob Creutz, General Manager at iParadigms, “The CrossCheck database is quickly reaching critical mass. No other plagiarism detection system has such a comprehensive repository of content. That content is critical in making CrossCheck work for publishers. The combination of content, iParadigms’ sophisticated text analysis software, and the intuitive iThenticate interface creates an extremely powerful tool for publishers.”

**About iParadigms, LCC**
Headquartered in Oakland, CA, iParadigms, LLC (http://www.iparadigms.com) is the leading provider of web-based solutions to check documents for originality and plagiarism.

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.