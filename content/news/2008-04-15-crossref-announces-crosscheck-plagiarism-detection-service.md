+++
title = "Crossref Announces CrossCheck Plagiarism Detection Service"
date = "2008-04-15"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*15 April 2008*

**Lynnfield MA, April 15, 2008** – Crossref announces an agreement with iParadigms, LLC to launch the CrossCheck service to aid in verifying the originality of scholarly content. Following on the success of Crossref’s recent pilot of CrossCheck, the service is scheduled to go live in June.

Crossref is partnering with iParadigms, LLC to offer its members — leading scholarly and professional publishers — the opportunity to verify the originality of works submitted for publication using the iThenticate service to check against a vast database of proprietary as well as open web content. Until now, there was no automated way to check submissions against previous publications because the published literature had not been indexed and “text fingerprinted” for this purpose. The CrossCheck database will include the full-text journals of leading academic publishers, and is expected to grow very rapidly over the coming months as Crossref member publishers sign up for the service.

CrossCheck will be available to all Crossref members who opt to contribute their content to the database. To use the service publishers will need to integrate the checking tool into their editorial processes and develop suitable policies and guidelines. Crossref is working with iParadigms, member publishers, and editorial system software producers on appropriate technical information and guidelines for CrossCheck.

Ed Pentz, Crossref’s Executive Director said, “CrossCheck builds on the collaborative nature of Crossref itself, and will provide an important tool for ensuring the originality of scholarly content. The system doesn’t actually detect plagiarism – plagiarism implies intent and that’s not something computers can identify. Instead it identifies matching text in different documents and then a knowledgeable person has to look at the results as part of the editorial screening process. We are pleased to be working with iParadigms and using the iThenticate service. Ultimately, the whole research community will benefit from CrossCheck.”

John Barrie, President and Founder of iParadigms, LLC added, "iParadigms is the world’s leading provider of web-based software solutions that analyze the originality of textual materials. Our services, including Turnitin and iThenticate, are used by over 8400 institutions in 106 countries, to help professionals such as academics, editors and publishers ensure that their work adheres to the rigorous standards of integrity and timeliness in our fast-paced, web-connected world. The inclusion of Crossref member scholarly and scientific content in our database makes our services even more effective than ever. This is a win for both of our organizations.”

**About iParadigms, LLC:** Headquartered in Oakland, CA, iParadigms, LLC (http://www.iparadigms.com) is the leading provider of web-based solutions to check documents for originality and plagiarism. The company’s products include Turnitin, a web-based service used by tens of millions of students and faculty for the digital assessment of academic work, and iThenticate, an internet service which enables companies to determine originality and check documents for misappropriation.

_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_

_Crossref® and Crossref.org® are registered trademarks and the Crossref logo is a trademark of PILA (Publishers International Linking Association_

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.