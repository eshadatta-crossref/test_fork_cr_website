+++
title = "Crossref™ and Atypon Announce Forward Linking Service"
date = "2004-06-08"
parent = "News"
weight = 1
x-version = "0.0.0"

+++

*08 June 2004*

**LYNNFIELD, MA** – June 8, 2004. -- Crossref, the cross-publisher reference linking service, and technology partner Atypon announced today the launch of Crossref’s new Forward Linking service.

In addition to using Crossref to create outbound links from their references, Crossref member publishers can now retrieve “cited-by” links -- links to other articles that cite their content. This new service is being offered as an optional tool to allow Crossref members to display cited-by links in the primary content that they publish.

According to Ed Pentz, Crossref’s Executive Director, "Forward Linking is a natural extension of the Crossref linking network and will provide a better online reading environment for researchers and scholars." Several publishers, including John Wiley & Sons, the American Physical Society, Blackwell Publishing, Annual Reviews, Institute of Physics Publishing, and the American Geophysical Union, have stated that they will participate and will complete their implementations of the new service later this year.

As part of the same functionality powered by Atypon, Crossref is also offering a new Forward-Match feature that eliminates the need for users to query Crossref repeatedly for citations that do not initially return a match. When a query is marked to enable alerts, the Crossref system automatically sends an email containing the matched results once the relevant content gets registered in Crossref.

Georgios Papadopoulos, President of Atypon, states: "Forward Matching offers online publishers truly dynamic linking. Publishing systems can now retrieve the latest linking information directly from Crossref as soon as the information is available.”

Forward Linking and Forward Matching represent a major upgrade to Crossref’s scientific and scholarly linking network. With these developments, Crossref publishers now have access to full citation chains and truly dynamic citation linking, better enabling researchers to cross system boundaries as they navigate the research literature online.

Atypon is a leading software and services provider for the STM/scholarly information community, providing business-to-business solutions designed to help publishing companies leverage the Internet. Find out more at [www.atypon.com](http://www.atypon.com).



---


 Please contact [our communications team](mailto:news@crossref.org) with any questions.