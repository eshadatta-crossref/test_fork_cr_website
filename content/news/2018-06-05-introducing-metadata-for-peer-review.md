+++
title = "Introducing metadata for peer review"
draft = false
date = "2018-06-05"
parent = "News"
weight = 3
x-version = "0.0.0"

+++

_05 June 2018_
### Crossref facilitates the use of essential peer review information for scholarly communications
_The new exposure of peer review information through its public API provides opportunities for discoverability, analysis, and integration of tools._

In response to requests from the community for flexible registering of peer review content (and associated discussions of published content), Crossref has extended its infrastructure to not only support the registration of this content, but also the easy retrieval, use and reuse of it, through its public API. This is a major advancement for the discoverability and usability of peer review reports.

Late last year, after collaboration with like-minded member organizations, Crossref opened a new deposit schema to enable the registration of peer reviews, referee reports, decision letters, author responses, and community comments. Metadata to characterize both the peer review type and stage—to accurately reflect the history of the review—can now be deposited and searched for.

Metadata for over 10,000 peer reviews is now openly available and easily retrievable for analysis and integration into other tools and services, through Crossref’s public API.

{{% quotecite "Jennifer Lin, Crossref Director of Product Management" %}}
Peer review metadata is critical to the research enterprise and we’re pleased to extend the Crossref open infrastructure to address this need. Making pre- and post-publication peer reviews available in the Crossref scholarly map will support the persistence and discovery of critical discussions about published research. It will further credit reviewers and editors for their contribution as well as support publishing transparency and provide data for analysis and research on scholarly communications. {{% /quotecite %}}

Organizations such as BMJ, ASAPBio, PeerJ, and ScienceOpen have been driving the development of peer review Content Registration . They recognize the importance of persistent records for peer review content; and understand this supporting metadata enriches scholarly discussion, reviewer accountability, transparency, and peer review analysis.

Stephanie Dawson, CEO of ScienceOpen, notes:

> Rich metadata is absolutely key to discoverability - for research articles, preprints, books, conference proceedings, and now for peer review reports. At ScienceOpen we offer our users a Crossref DOI for every review they publish on our platform on any of 42 million articles and preprints. With their new schema Crossref is making those reviews easier to identify and find, which translates into more impact for researchers and publishers.

As the data is not subject to copyright and no sign-up is required to use it, anyone can retrieve the information necessary for their own integration and analysis. Institutions and researchers will now be able to build a stronger picture around the role of the peer review in scholarly communications as a whole, in addition to identifying and assessing their own contributions.

ASAPbio Director, Jessica Polka explains their position:

> Peer review is an important form of scholarship, and it deserves to be published, cataloged, uniquely identified, and linked to other pieces of the scholarly record. Therefore, Crossref's offering enables peer review to be treated in a way that reflects its importance in improving the quality of knowledge.

Quick to respond to the shifting needs around scholarly communications, Crossref is committed to the expansion of their infrastructure to support the demands of scholarly progression and scientific discovery. The availability of peer review data is just one of the many progressions the nonprofit organization has made in recent months to aide facilitation of transparency and governance, and to put research in context.

---

Please contact our [communications team](mailto:news@crossref.org) with any questions.