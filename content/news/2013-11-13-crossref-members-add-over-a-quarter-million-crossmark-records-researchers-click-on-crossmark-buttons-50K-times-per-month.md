+++
title = "Crossref Members add over a quarter million Crossmark records; Researchers click on Crossmark buttons 50k times per month"
slug = "crossref-members-add-over-a-quarter-million-crossmark-records"
date = "2013-11-13"
parent = "News"
weight = 1
aliases = [ "/news/2013-11-13-crossref-members-add-over-a-quarter-million-crossmark-records-researchers-click-on-crossMark-logos-50K-times-per-month", "/news/2013-11-13-crossref-members-add-over-a-quarter-million-crossmark-records-researchers-click-on-crossMark-logos-50K-times-per-month/",]
x-version = "0.0.0"

+++

*13 November 2013*

**November 13, 2013, Cambridge, Massachusetts, USA** - Crossref announced today that more than 270,000 documents now have Crossmark version status information, and most display the distinctive logo that provides researchers information both about existing changes to scholarly articles and about important publication record information. More than 100,000 of these records were added in the month of October alone. Scholars click on Crossmark records displayed on PDF and HTML articles an average of 50,000 times a month.

Crossref accepted its first Crossmark data from publishers eighteen months ago. In that time, the number of participating publishers has grown to 24 from the small group that led the two-year pilot. The publishers with the highest number of Crossmark records include those from the original pilot, such as Elsevier Science, the International Union of Crystallography (IUCr), the American Institute of Physics, The Royal Society, and Wiley. Wiley currently has the highest number of reader views (or clicks) per Crossmark record.

Participants who have joined since the original pilot are also contributing large numbers of Crossmark records. For example Proceedings of the National Academy of Sciences (PNAS), deposited almost 50,000 Crossmark records since mid-October. Another publisher that joined the program this year, F1000 Research, uses the optional Crossmark record tab to deposit and display reviewer reports.

“We’re very pleased to see the increased uptake of Crossmark among our members,” commented Crossref’s Executive Director Ed Pentz. “These publishers have worked hard—sometimes on their own, sometimes with their vendors—to codify content changes and to locate and standardize important non-bibliographic metadata. Now they are beginning to see the benefits, as their readers access this important information in a standard way across publishers. We’re also encouraged by the increasing support from vendors in our affiliate community, who are making this valuable service possible.”

As an example, Stanford University’s HighWire Press, a longtime Crossref Affiliate that implements Crossref Services for many members, has begun making Crossmark deposits for publishers such as the American Diabetes Association, Bioscientifica, the British Medical Journal Group (BMJ Group), Oxford University Press, and PNAS. HighWire also continues to work with pilot participant The Royal Society in implementing support for additional information to be displayed on the publication record tab.

John Sack, HighWire’s Founding Director, commented, “We’re used to our computer software telling us when it needs an update. Crossmark makes content smart like that: the reader can click to find out whether an update is available. HighWire knows from its end-user research that people store PDFs in their local systems, so it is particularly important that the user can easily query for an update from within a PDF.”

FundRef, a funder identification service launched by Crossref this spring, (https://www.crossref.org/services/funder-registry) has also spurred the growth of Crossmark. So far, 26 publishers have joined FundRef; participants have deposited 44,000 bibliographic records containing funding data. For publishers participating in both Crossmark and FundRef, the FundRef data automatically displays on the publication record tab of the Crossmark popup box.
The following Crossref DOI links provide examples of Crossmark implementation on publisher websites:
On publisher platforms:

F1000 Research
Contains an update and publication record information
http://dx.doi.org/10.12688/f1000research.2-198.v1

IUCr
Status is “Current” and contains publication record information
http://dx.doi.org/10.1107/S2052519213000183

*On HighWire platform:*  
Status is “Current” for all examples  

* American Diabetes Association
http://dx.doi.org/10.2337/dc13-0492
* Bioscientifica: http://dx.doi.org/10.1530/JOE-13-0302
* The Royal Society: http://dx.doi.org/10.1098/rspb.2013.1913

More information about Crossmark is available at https://www.crossref.org/services/crossmark.

**About Crossref**  
Crossref (http://www.crossref.org) is a not-for-profit membership association of publishers. Since its founding in 2000, Crossref has provided reference linking services for over 63 million scholarly content items, including journal articles, books and book chapters, data sets, conference proceedings, reference entries, technical reports, and standards. Crossref also provides collaborative services to improve trust in the scholarly communications process, including Cited-By linking, CrossCheck plagiarism screening, Crossmark update and publication record identification, and the FundRef funder identification service.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.