+++
title = "Sanctions compliance"
date = "2023-02-14"
draft = false
author = "Lucy Ofiesh"
rank = 4
x-version = "0.0.0"

[menu.main]
parent = "Membership operations"
weight = 100

+++

Crossref's mission is to support global research in and between all countries and we will always do that to the maximum extent possible. But we are also bound by laws in the US, the UK, and the EU. Where sanctions apply, we have to comply with these.

Due to extensive sanctions, we’re currently unable to accept applications for membership from organizations based in the following countries or regions:

* Cuba
* Iran
* North Korea
* Syria

And because of sanctions instituted in response to the war in Ukraine, we are currently unable to accept applications for membership from organizations:

* That are part of the Russian Government, or are under sanctions by the US, UK or EU.
* That are located or have physical operations in the four oblasts of Crimea, Donetsk, Kherson, Luhansk or Zaporizhzhia.

All member applications from organizations based in Russia or Belarus are subject to sanctions checks, and in some cases a more detailed and extended review will be necessary. Because of this, these applications may take longer to process than other new member applications.

Member organizations based in other countries are able to work with journals and authors in these countries, but they are obligated to check the organizations and individuals that they are working with extremely carefully. By accepting our [membership terms](/membership/terms), all members confirm that:

> “...neither it nor any of its affiliates, officers, directors, employees, or members is (i) a person whose name appears on the list of Specially Designated Nationals and Blocked Persons published by the Office of Foreign Assets Control, U.S. Department of Treasury ("OFAC"), (ii) a department, agency or instrumentality of, or is otherwise controlled by or acting on behalf of, directly or indirectly, any such person; (iii) a department, agency, or instrumentality of the government of a country subject to comprehensive U.S. economic sanctions administered by OFAC; or (iv) is subject to sanctions by the United Nations, the United Kingdom, or the European Union.

Sponsoring Organizations (and any remaining Sponsoring Members) are also required to ensure that their engagement with any Sponsored Member or Sponsored Organization is fully compliant with sanctions in the US, UK and EU.

If we discover that an existing member may be subject to sanctions and/or has misrepresented their location, identity, ownership or any other material information on their application form in order to evade sanctions or a prior revocation of membership, or any other illicit reason, we will follow our [defined processes to revoke membership](/operations-and-sustainability/membership-operations/revocation/). Sanctions also change over time, and we periodically review our membership against the list of sanctioned organizations and individuals. This may lead to membership suspension or revocation of membership in accordance with these same processes.