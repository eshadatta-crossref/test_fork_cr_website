+++
title = "Membership operations"
date = "2022-09-13"
draft = false
author = "Amanda Bartell"
rank = 4
x-version = "0.0.0"

[menu.main]
parent = "Operations & sustainability"
weight = 10

+++

Your organization needs to be a [member of Crossref](/membership) in order to get a DOI prefix so you can create Crossref DOIs and register content. All members agree to our [membership terms](/membership/terms) to help ensure the persistence of our infrastructure.

On this page, find out more about our membership operations:
* [New member applications](/operations-and-sustainability/membership-operations/#new-member-applications)
* [The membership terms](/operations-and-sustainability/membership-operations/#membership-terms)
* [Applications to become a sponsor](/operations-and-sustainability/membership-operations/#applications-to-become-a-sponsor)
* [Our billing cycle](/operations-and-sustainability/membership-operations/#our-billing-cycle)
* [Canceling your membership](/operations-and-sustainability/membership-operations/#canceling-your-membership)
* [When Crossref revokes membership](/operations-and-sustainability/membership-operations/#revoking-membership)
* [Membership and legal sanctions](/operations-and-sustainability/membership-operations/#membership-and-legal-sanctions)
* [Organizations that claim to be Crossref members](/operations-and-sustainability/membership-operations/#organizations-that-claim-to-be-crossref-members)

## New member applications

We ask new applicants to complete an application form, and we then check that:

1. You meet our membership criteria and can commit to fulfil the member terms.
2. We are legally able to accept your organization as a member.
3. Your organization hasn’t previously been a member of Crossref whose membership was revoked.
4. Your organization hasn’t misrepresented themselves in the application.
5. Your organization is not already a member of Crossref.

Membership in Crossref is open to organizations that produce professional and scholarly materials and content, and we treat this broadly - “come one, come all” is one of our [organizational truths](/truths). We’re a global community of members with content in all disciplines, in many formats, with all kinds of business models - research institutions, publishers, government agencies, research funders, museums and many more. But it’s important that members are able to meet the [obligations of membership](/membership/#member-obligations-and-benefits) and work in a way which reflects our [code of conduct](/code-of-conduct/).

As an organization that’s based in the US (and with significant activities in the UK and Europe) there are also some legal limits on our activity - for example, we cannot accept applications for membership from organizations based in some countries due to sanctions. [Find out more](/operations-and-sustainability/membership-operations/sanctions) about sanctions which impact on Crossref membership and the countries which are affected.

We may not be able to accept applications for membership from organizations which have previously had their [membership revoked](/operations-and-sustainability/membership-operations/revocation) or who have misrepresented themselves in their application.

And finally, if your organization is already a member of Crossref (for example, a different department of the same university) we will add you to the existing membership. This ensure that the same organization is not paying multiple membership fees.

## Membership terms

New Crossref members agree to these [member terms](/membership/terms/) by ticking a box in the application form. These terms then remain in effect permanently - there is no need to renew each year. The terms will only be canceled if one of the following things happen:

1. The member cancels their membership - [how to cancel your membership.](/members-area/canceling)
2. Crossref revokes your membership - [find out more.](/operations-and-sustainability/membership-operations/revocation)
3. The terms are updated.

### Updating the membership terms

If we change the Crossref membership terms, we will email the contact that each active member organization has identified as their Primary contact (previously known as "Business contact") to let them know about the change. This will happen no fewer than sixty days prior to effectiveness. Members may [cancel their membership](/documentation/metadata-stewardship/maintaining-your-membership/canceling/) if they don’t want to accept the new terms.

## Applications to become a sponsor

Some small organizations want to register metadata for their research and participate in Crossref but are not able to join directly due to financial, administrative, or technical barriers. Sponsors are organizations who facilitate membership for these organizations by providing  administrative, billing, technical, and—if applicable—language support to these organizations. There is quite a high bar to becoming a Crossref sponsor, so not all organizations who apply will be eligible.

[Find out more about becoming a sponsor](/community/sponsors/)

## Our billing cycle

We send out annual membership fees (for members and sponsors) and annual subscription fees (for service providers and those subscribing to Metadata Plus or other paid-for metadata services) each January. We invoice for content registration on a quarterly basis. All invoices have a term of 45 days. We recommend that members pay using a credit or debit card through our payment portal, but other payment methods are available.

Find out more about:
* [Our billing process](/members-area/all-about-invoicing-and-payment/)
* [What happens if you don’t pay your invoices](/operations/revoking-membership)

## Canceling your Membership

You need to contact us to cancel your membership. You aren’t able to pause your membership, so if you just stop using the service and don’t tell us that you want to cancel, we will still continue to send you annual membership fee invoices. And if you then want to start using the service again in future years, you will need to pay these outstanding membership invoices. If you actually contact us to cancel, we can stop these annual membership invoices from being created.

[How to cancel your membership](/members-area/canceling/).

## Revoking membership

As an organization that’s obsessed with persistence, we really try to avoid revoking membership. However, there are limited times when we have to. [Find out more](/operations-and-sustainability/membership-operations/revocation).

## Membership and legal sanctions

Crossref’s mission is to support global research in and between all countries and we will always do that to the extent possible. But we are also bound by laws in the US, the UK, and the EU, and where sanctions apply we have to comply with these. [Find out more](/operations-and-sustainability/membership-operations/sanctions/)

## Organizations that claim to be Crossref members

Occasionally authors contact us as a publisher they are working with has claimed to be a Crossref member, or has claimed to register DOIs, but instead just displays unregistered, DOI-like strings on their website.

We don't currently have a list of Crossref members, and as it's the parent publishing organization that joins Crossref (rather than individual journals) it's sometimes hard to work out if a journal is published by a Crossref member or not.

If you are an author planning to submit a manuscript to a publisher who claims to be a Crossref member, check the DOIs that they currently display on their website. Copy the DOI as a link, and paste this into a browser. If the DOI resolves to an article, then that DOI has been registered with us, or with another Registration Agency. If the DOI resolves to DOI Foundation Error page, then that DOI has not been registered.

For example of a DOI that has NOT been registered, copy and paste [https://doi.org/10.1234/56yuip](https://doi.org/10.1234/56yuip) into a browser. You will arrive at the DOI Foundation Error page, rather than a scholarly work.

Always refer to the [Think, Check, Submit website](https://thinkchecksubmit.org/) to help you decide whether to submit to a particular journal. If you have any problems with a publisher, do speak to your university of the team at the [Committee On Publication Ethics (COPE)](https://publicationethics.org/) for support and guidance.

This is by no means comprehensive, but we do keep a [list of websites](https://docs.google.com/spreadsheets/d/1TmT2VnoaGnvBZ-DY-p2g3HNIw0KSSyfbsrWfTjhW4cw/edit#gid=0) where we have been informed that they are displaying fake DOIs (eg unregistered, DOI-like strings), and/or claiming to be Crossref members. (Do [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) if you are one of these organizations and wish to be removed from the list).

We also hold a [list of ex-members](https://docs.google.com/spreadsheets/d/1cCkdvtqEM1urmrUQZ4-LGz_Omf5812aVkRFJc5UryHw/edit#gid=1891705587) who have had their membership revoked as they contravened the membership terms.