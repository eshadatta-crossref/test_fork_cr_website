+++
title = "Financials"
date = "2022-10-10"
draft = false
author = "Lucy Ofiesh"
rank = 4
x-version = "0.0.0"

[menu.main]
parent = "Operations & sustainability"
weight = 5

+++

In recent years, we operate on a budget of around $10 million (USD). About one-third of our revenue comes from annual dues (e.g., membership fees, subscriptions) and two-thirds from services (e.g., Content Registration, Similarly Check document checking). Our fees are set and reviewed by the [Membership & Fees committee](/committees/membership-and-fees/), which includes our staff, board, and community members. This group also created a set of [fee principles](/fees/#fee-principles) which were approved by the board in 2019.

About two-thirds of our expenses are related to people - staff, benefits, and contracted support. One-third of our costs are everything else - hosting costs, licensing fees, events, and costs to do business like banking fees and insurance.

Each year we strive to generate a small operating net and have been able to do so nearly every year.

We also maintain a reserve fund to support long-term sustainability. An Investment Committee was formed in 2021 to update our investing policies, and we will share more later this year.

Below is a look at how our operations have changed over time.

{{<figure src="/images/financials/2020-yearend-chart-finances.png" alt="10-year financials" width="100%" >}}

## Annual financial reporting

As a not-for-profit, we are tax-exempt, and to maintain that status, we undergo a financial audit each year by an independent accounting firm. Our auditors prepare our Form 990, which the US IRS requires and is made publicly available. It gives an overview of what we do, how we are governed, and detailed financial information.

Below are our recent Form 990s.

[2017 Form 990](/pdfs/finance_2017-990.pdf)  
[2018 Form 990](/pdfs/finance_2018-990.pdf)  
[2019 Form 990](/pdfs/finance_2019-990.pdf)  
[2020 Form 990](/pdfs/finance_2020-990.pdf)  
[2021 Form 990](/pdfs/Public-2021-Form-990.pdf)