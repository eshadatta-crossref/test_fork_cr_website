+++
title = "Content conventions"
name = "Content conventions"
date = "2016-05-16T12:00:00-00:00"
draft = false
tags = [ "help", "structure",]
x-version = "0.0.0"

+++

- Markdown files in Hugo should end in the suffix `.md`. For example, `about.md`

- Files **and** directory names should be all lowercase for example. So instead of `Crossref.md` or `arXiv.md` or `Crossmark`, you would use `crossref.md` and `arxiv.md` and `crossmark` (but no BOLD!)

- Files and directory names should not include spaces. Replace spaces with “-“ (chain-case). For example `crossref-metadata.md`, `lucy-lawless.png`, `the-naming-of-cats.md`

- if file or directory starts with number, the number should always be four digits and always left padded with zeros. For example:

```
0008-biffy.md
0009-wangotango.md
0010-splort.md
```

This will ensure that files sort properly when viewed in a directory.

## Images and image file names:

Bitmap images should always be saved in `png` format and should always have the `.png` extension.

Bitmap filenames should always end with their dimension in pixels written as `_nnnxnnn.png`. For example `beach_256x265.png` and `beach-512x512.png`

Vector graphics should always be saved in `SVG` format and should always have the `.svg` extension. For example, `rocket.svg`