---
author: Zach Anthony
date: '2018-10-26'
draft: false
tags:
- help
- other
title: How to block content from searches
x-version: 0.0.0
---

## Overview

There are times when you may wish to block certain content from being found in the in-site search results, or in the results of external search engines like Google. Both are possible but are approached in a different way. If you won't to block content in both places you must follow the directions in both sections below.

### Block content from the in-site Search
The search tool used to provide in-site search is called Algolia, and it has been configured to ignore any page(s) that are found within a folder that starts with an **underscore** (not that this only works on folder names; pages that start with an underscore are not blocked, with the exception of _index.html). Why would you want to block pages from coming up in the in-site search? Perhaps because they aren't meant for public viewing, like these How To files, or because they are something that could be confusing if a user were to arrive at the page directly.

This very How To page is found in the ```/content/_editors``` folder, which means that it, and all the other files in the folder are ignored by Algolia and will not come up in a site search. 

**Blocking files from the in-site Search *does not* prevent Google and other search engines from indexing the page however.** See the next section for more information about this. 

### Block content from external search engines like Google
To fully block content from external search engines you must edit the site configuration TOML files: ```live.toml```, ```staging.toml``` and ```webdev.toml``` (for testweb). Each of these files may be different, for example because the staging and testweb sites would normally be entirely blocked from indexing by search engines. 

The relevant portion of the .toml file looks like so:
```
  robots = [
    "User-Agent: LinkChecker",
    "Allow: /",
    "User-agent: *",
    "Disallow: /"
  ]
```
This example is from ```webdev.toml``` and shows firstly that the LinkChecker is allowed to index the root folder and everything beneath it (i.e. the whole site) but that anything else, including all search engines spiders, are disallowed from indexing anything. 

The structure of this block follows the [Robots Exclusion Standard](https://en.wikipedia.org/wiki/Robots_exclusion_standard) but with added, specific formatting to make it a valid TOML file. 

**If you wish to make a change or addition here please be aware that a formatting error is likely to stop builds and could bring the entire website down.** For this reason so we recommend you be very careful. Alternately you may wish to contact Ginny for assistance.