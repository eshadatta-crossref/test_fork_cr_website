+++
title = "Authors / Our People How To"
name = "Blogs How-to"
date = "2016-08-16T12:00:00-00:00"
draft = false
tags = [ "help", "template", "blog",]
x-version = "0.0.0"

+++

## How to Add or Edit a Staff Member
The information that is used to  build the info about each member of staff on the [Our People](/people/) page, as well as the short author bios shown next to each blog entry by a staffer on the main [Blog](/blog/) and at the top of the page listing all of an author's blog posts (including a 'more info' to the individual detail page of the staffer; see [Ed Pentz' blog page](/authors/ed-pentz/) as an example) is contained in a data file called ```biographies.json``` which can be found in ```/data/team/```.

To add a new staffer just create a new block as shown below, paying special attention to proper formatting, especially commas (see "The Importance of Formatting" below). To edit, simply find the appropriate block and make your changes. For each staffer you must also create the individual detail page in ```/content/people/```.

```
{
   "[author handle - usually same as full name]" : {
      "fullname" : "[full name]",
      "discourseusername"   : "[valid Discourse username for the author]",
      "department" : "[department name]",
      "title" : "[job title]",
      "thumb" : "[path to author thumbnail image, should be 100px x 100px]",
      "image" : "[path to large version of author image, should be 720px x 720px]",
      "bio" : "[short bio text - one or two sentences is ideal but can be longer]",
      "staff" : [true or false; must be true for person to appear on Our People page],
      "email" : "[email address]",
      "twitter" : "[@twitter account]",
      "orcid" : "[ORCID iD number]"
   }
```

*See "Sample Data" below for a sample of actual data from ```biographies.json```.*

The "handle" field **must exactly match (including spaces and capital letters) the author name as it appears in blog entries** (in the ```author``` frontmatter for the given blog entry/entries) in order to automatically display the bio block alongside blog entries.

If no matching author handle is found, the "unknown" entry at the bottom of ```biographies.json``` will be used instead. This can be edited too including the links to the default images used when no staff image is available.

The structure of data in the biographies.json file is as follows. Each item is known as a ```string```, and each string is configured with a ```value```.

Its important to add both a thumbnail and full sized image of each person, as both are used in different places within the site.

For proper integration with Discrouse, its also key that the ```discourseusername``` field have a valid Discourse username in it (if one is available; otherwise a generic 'Crossref Team' user is set to the author of any content in Discourse).

Also note that a staffer's Twitter address and their ORCID iD are automatically turned into clickable links on each staffer's individual detail page (but they can be omitted if not relevant).

For each staffer you must also create a file in ```/content/people/``` with a file name [author-handle.md] (that is the author handle, lowercase with any spaces filled by hyphens, must be used as the filename). Within the file only the following content is required:

```
+++
Title="[author handle]"
+++

&#123;&#123;< staffblock >&#125;&#125;
```

```Title``` must match the author handle used in ```biographies.json``` (as well as in the frontmatter of any blog entries).

*See "Sample Data" below for a sample of an actual [author-handle.md] file.*

## How to Add or Edit a Blog Author
This process is very similar to the one outlined above, but a bit simpler. Blog authors do not require the full set of information in ```biographies.json``` that a staffer does, as only their name, a thumbnail image and the short bio are shown next to each entry on the main [Blog](/blog/) page and at the top of the page listing all of an author's blog posts (but without the 'more info' link that would feature if the author were a staffer). There is also no need to create an individual detail page in ```/content/people```.

One key difference: in ```biographies.json```, ```staff``` must be set to ```false```.

## The importance of formatting
**JSON data must be precisely formatted or it can stop the site from building correctly.** For example, note the use of commas in the above example: a comma follows each individual string **except the last one** (in the above example that's '''image'''). Similarly, a comma should follow each person's block unless they are the last one in the list, so be careful when adding a person - if you add at the end of the file be sure you add a comma to the proceeding entry for example.

## Sample data
Here is a sample of actual ```biographies.json``` data.


```
{
   "Jon Stark" : {
      "department" : "Technology team",
      "title" : "Software Developer",
      "thumb" : "/images/staff/jon_thumbnail.png",
      "bio" : "Jon Stark has been part of the Crossref software development team since 2004. Prior to Crossref, Jon had worked at Northern Light Technology and EBSCO Publishing. Aside from work, Jon enjoys time with his family, a love of nature and the outdoors, woodworking, flying (with the use of an airplane, not jumping off cliffs), and serving in his local church community.",
      "staff" : true,
      "fullname" : "Jon Stark",
      "email" : "email@crossref.org",
      "image" : "/images/staff/jon_720px.jpg"
   },
      "Shauna Lee" : {
      "department" : "Finance & Operations team",
      "thumb" : "/images/staff/shauna_thumbnail.png",
      "title" : "Office Manager",
      "bio" : "Shauna Lee is the office manager at Crossref. She is responsible for keeping the office caffeinated and helping to organize board meetings. In her free time she can be found chasing after her two young kids.",
      "staff" : true,
      "fullname" : "Shauna Lee",
      "email" : "email@crossref.org",
      "image" : "/images/staff/shauna_720px.jpg"
    },
   "Christine Cormack Wood" : {
   "bio" : "Based in Oxford, Christine is responsible for all Marketing and Communications activity at Crossref. Before joining Crossref she worked with a number of global multinational corporations, bringing business strategies to life through innovative and integrated omni-channel marketing. Having lived and worked across Asia and the Middle East for almost twenty years, she has extensive global experience and a truly international outlook. With a savvy approach to tactical planning, Christine is now happily bringing her skills to supporting the Member and Community Outreach team to meet Crossref's educational and awareness objectives.",
   "staff" : true,
   "discourseUsername" : "ccormackwood",
   "fullname" : "Christine Cormack Wood",
   "twitter" : "@ChrisCormack3",
   "orcid" : "0000-0002-8104-8078",
   "email" : "ccormackwood@crossref.org",
   "image" : "/images/staff/blank-profile-picture.png",
   "department" : "Member & Community Outreach team",
   "title" : "Marketing & Communications",
   "thumb" : "/images/staff/blank_thumbnail.png"
   },
"Melissa Harrison" : {
      "fullname"     :     "Melissa Harrison",
      "thumb"          :     "/images/blog/melissa-elife-thumbnail.png",
      "bio"          :     "Melissa Harrison is Head of Production Operations at eLife Sciences Publications, Ltd.",
      "staff"          :     false,
     }

```

Here is a sample of an actual [author-handle.md] file.

```
+++
Title="Ginny Hendricks"
+++

&#123;&#123;< staffblock >&#125;&#125;
```

## Fixing category capitalizations

HUGO does it's best to keep categories as they have been typed but in a couple of situations it changes things to sentence case. So APIs becomes Apis and DOIS becomes Dois. This only occurs on the blog category listings at the right hand side of the screen and on the 'In xxx' line below each blog title/author. To fix this particular issue there is a toml file in the data directory called ```category_casefix.toml``` which has a simple old/new structure. So if there is another category causing issues then simple put what it is showing up as erroneously in the ```old``` field and place what you want it to show in the ```new``` field.