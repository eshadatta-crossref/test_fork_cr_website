+++
title = "Our People How To"
name = "Our People How To"
date = "2016-05-27T12:00:00-00:00"
draft = false
x-version = "0.0.0"

+++

## How to manage staff on the Our People page

This page describes how to add, edit or delete content on the Our People page, found in the About section.

Staff data such as biographies, twitter @name, ORCID iD and speaking topics are all managed in a single data file: ```/data/staff/biographies.json```. The actual About > Our People page and the detail pages for each staff member are all built from the data contained in this file.

Additionally, to enable the detail page for a staff member, a small Markdown file must exist in ```///people/[staff-name].md```.

### Add a new staff member
The first step to add a new staff member is to update ```/data/staff/biographies.json``` with their details. Follow the model of existing staff data in the file, which looks like this:

```
	"gbilder" : {
		"fullname"	:	"Geoffrey Bilder",
		"thumb"		:	"/images/staff/geoff_thumbnail.png",
		"image"		:	"/images/staff/geoff__720px.jpg",
		"bio"		:	 "Geoffrey Bilder is Director of Strategic Initiatives at Crossref, where he has led the technical development and launch of a number of industry initiatives including Similarity Check (formerly CrossCheck), Crossmark, ORCID and the Open Funder Registry (formerly FundRef). He co-founded Brown University's Scholarly Technology Group in 1993, providing the Brown academic community with advanced technology consulting in support of their research, teaching and scholarly communication. He was subsequently head of IT R&D at Monitor Group, a global management consulting firm. From 2002 to 2005, Geoffrey was Chief Technology Officer of scholarly publishing firm Ingenta, and just prior to joining Crossref, he was a Publishing Technology Consultant at Scholarly Information Strategies.",
		"title"		:	"Director of Strategic Initiatives",
		"twitter"	:	"@gbilder",
		"department":	"Labs",
		"staff"		:	true,
		"email"     :   "email@crossref.org",
		"phone"     :   "xxx-xxx-xxxx",
		"orcid"		: "0000-0003-1315-5960",
	    "topics"	: [
	      "future of scholarly communication",
	      "scholarly infrastructures",
	      "altmetrics"
	    ]
	},
```

Most fields are optional.

Once the staff members's data has been added to biographies.json, just create a small Markdown file in ///people/. This file is used by Hugo to build the detail page for each staff member. The file name should be an 'urlized' version of the staff member's name, e.g. Ginny Hendricks becomes 'ginny-hendricks.md'. The contents of the file are very simple: just the staff member's name in the ```Title``` field of the Frontmatter, and then a generic shortcode. See below.

Note that the name in the ```Title``` field must *exactly* match the ```fullname``` field in ```biographies.json```.

```
+++
Title="Geoffrey Bilder"
+++

&#123;&#123;< staffblock >&#125;&#125;
```

The ```staffblock``` shortcode pulls the additional information about the staff member into the detail page.

### Edit a staff member
In nearly all cases, to update a staff member's details you'd just make changes in the appropriate section of the ```biographies.json``` file.

The one exception to this would be if the staff member's name changes, in which case make the update in ```biographies.json``` **and** then be sure to also change the file name and ```Title``` field of the associated ```[staff-name].md``` file.

### When a staff member leaves
When a staff member leaves, in the appropriate section of the ```biographies.json``` file, change ```"staff": true,``` to ```"staff": false,```.

### Delete a staff member
To delete a staff member, just remove their entire block from the ```biographies.json``` data file, and the related ```[staff-name].md```.