+++
title = "Images How To"
name = "Images How To"
date = "2016-09-27"
draft = false
author = "Zach Anthony"
x-version = "0.0.0"

+++

## Images How To
This document talks about the basic requirements for adding images to the site. If you're looking to learn how to add an image, you can also find information on the [markdown examples page.](https://www.crossref.org/_editors/markdown-examples/#images).

### Homepage images
The large banner image on the homepage should be a minimum of ```1920px x 450px``` in order to ensure that even on large screens it remains of sufficient quality.

Images used in the 6 slots underneath the banner are either a "mask" image, which are the semi-transparent images that overlay a background image and move in and out on hover, or a background image.

Mask images have a very specific format not just in terms of dimensions, but also transparency and the use of the Crossref logo shape. The best way to create these is to base them closely on the existing masks, which can be found in /static/images/homepage-images/homepage-slot-images/masks.

The background images are pulled directly from whatever banner image is related to the page referenced in the slotfile - more on their minimum dimensions below.

### Standard page banner images
These should be a minimum of ```1920px x 982px``` in order to ensure that even on large screens they remain of sufficient quality.

### Optimization of images
Remember that images should be "optimised" before they are deployed (added to the /static/images folder and committed via Git). By optimisation we mean the following:

* Of the correct dimensions (see above)
* Cropped to show the best/most appropriate part of the image
* Compressed into a JPG or other file format so that the overall file size is as small as possible (while preserving image quality, i.e. not pixelated/blurry)   

### How to hyperlink an image


{{% imagewrap left %}}{{< figure src="/images/_editors/image-hyperlink-code.png" width="60%" >}}</a> {{% /imagewrap %}}


{{% imagewrap left %}} <a href="https://community.crossref.org/" target="_blank">{{< figure src="/images/_editors/how-to-hyperlink-image.png" width="60%" >}}</a> {{% /imagewrap %}}