+++
title = "Copy guidelines"
date = "2021-02-26"
author = "Ginny Hendricks"
rank = 1
weight = 5
x-version = "0.0.0"

+++

This guide helps to establish a consistent voice across all Crossref communications---online, in emails, on slides, and in print.

By using consistent phrases from agreed key messaging for all Crossref communications, we can achieve parallelism. Parallelism means telling our stories in the same way, all the time, everywhere. This helps with site search but also with improving understanding amoung all corners of the community, which in turn helps us to convey value and meet our mission by making scholarly communications better.

## Content

- Use key messaging wherever possible. Each service and most tools have key messaging. Key messaging is an agreed paragraph or set of statements about the purpose and benefit of the subject. You shouldn't need to paraphrase or rephrase key messaging.
- Cutting or replacing superfluous words is a key component to improving the clarity of our voice. We must edit ourselves mercilessly and cut, cut, cut.
- Write in short sentences. More than 12 words per sentence is usually too many. Aim for 8-10 max. Try removing words up until the last point that the meaning remains.
- Use words that are easily translatable to other languages. Use short, clear words. Avoid idiomatic expressions.
- Use action verbs like "get", "need", "tell" (e.g. rally, tag, run, play, make...) and limit the use of adjectives.


## Tone

- Be humble. It’s possible you don’t know _everything_.
- Don’t be too slick. We are not a corporation, it’s a good thing to not sound promotional.
- Be friendly. Not frivolous (e.g. “fun”! “happy”!), just professionally friendly (“hello”, “thanks”). There is usually no need for exclamation marks if the content is good.
- Try to say it aloud and if it sounds natural in conversation then it’s probably fine.
- Write in the first person, so “we” the writer (Crossref person) and “you” the reader. You will rarely need to write the word “Crossref” as the context is already set by being within the Crossref website. 


## Grammar & structure

- We always use sentence case for titles and any label that isn’t a service/product name.
- We use American English. Sometimes. We’re not that strict about it.
- We like the Oxford comma.
- We don’t like to use bold - it’s shouty and often means the copy isn’t emphatic enough; instead use bullets or numbering. Or, the headings go down as low as level H6. 


## Crossref phrases & key messaging

Read through and re-use phrases from already approved wording in key messaging such as for [our truths](/truths), our [mission and key words (rally, tag, run, make, and play)](/community/about), any of the opening paragraphs on a service's main page, the video scripts for each service video, or [become a member](/membership).

### Encouraged words and phrases in addition to key messaging

- **Metadata**. We need to own this word.
- We have a service called [**Content Registration**](/services/content-registration). Please talk about registering content, the content you have registered, content that's been registered with us, etc.
- **Community** (not just members). Members and users or members and others in the community.


### Discouraged words and phrases

- Crossref. Say **we** or **our**. Yes, try to not write Crossref, the context is already set by being on our website (or in our newsletter, or printed on headed paper).
- Publishers. We have members - they are not all publishers. For real. We also interact with way more people than just our members. We have a community of users so try **members and users** or **community, including members**.
- Instead of DOI deposit say **metadata deposit**.
- Deposit. It's usually fine but try **send** or **include**.
- Instead of DOI assignment say **Content Registration**.
- Instead of DOI try to say "metadata" or more generically "persistent identifier" (lowercase). Only use DOI if you are specifically talking about the resolver or the actual link/string.
- Don't use "Requirements" or "rules". We only have "obligations" now. Remember we are a community of people on a mission, not a corporation.
  - Note that obligations are different than "Best practice." Obligations are prerequisites for participation in something *at all*. "Best practice" are guidelines on how to participate in something *well.*
- Don't use Publishers International Linking Association, Inc. (PILA) or PILA. Ever. We are **always** Crossref.
- Mint or minting. DataCite does that - we don't.
- Indexing content or purchasing DOIs - these are not things we do.
- We are not a "supplier", "service provider", or "vendor".
- Please don't use "piece of research" or a "piece of content" but research output, object or "item of content".

---

## Website-specific stuff

Check out the list of all the [website editors' guides and templates](/_editors).

- We are aiming for **as few pages as possible** in this website. Where can you merge content and ask to remove superfluous pages? We must avoid duplication. We know that people don't mind scrolling through long pages if it's well sectioned/formatted and has header anchor links. Speaking of which:
- Familiarize yourself with the site map - is what you want to write already on (or could be) in another section or page. We do not need any more duplication. Please search atom for any key phrases you might be using.
- Check out the tips in each template e.g. the [service page templates](/_editors/about-service-template) or the [frontmatter template](/_editors/frontmatter-template). In general please do not start with a heading as the "title" defined in the frontmatter is already displayed as the heading. Never use an H1 tag (#) as it fights with the page title, always start with an h2 header (##)
- If you have yet to complete a section and you need to come back to it, label it with the string [TK](https://en.wikipedia.org/wiki/To_come_(publishing)) a journalistic habit meaning there is something more "to come". Please do not forget to come back to it. You can search in atom.io for TK within certain folders (cmd+f). You can also search for your name as author.
- Bullets are good when you're not being specific about a list of e.g. criteria and the list may grow. Numbers are better when you know there are few but important statements or criteria that will be unlikely to grow. E.g. "There are three important attributes of a member" would need a numbered list. "There are many characteristics that define a member" would need a bulleted list.
- Try to start with the "why this matters" point rather than the "what" or "how". Don't just fill in the facts but start with an intro. The first sentence of a page will be highlighted on other pages so it's important to get to the point upfront.
- Note we will not have separate policy pages anymore, we include policies within the relevant service or content type and they will usually come under the heading **Obligations** and (where appropriate), **limitations**.

### Constructing your page

- Don't start a new page without asking Ginny or being confident in your knowledge of the information architecture. Compare the frontmatter from a related page or ask Ginny or Rosa to check it.
- Update the date - this is important as it gets displayed as the last modified date at the bottom of every page.
- Make the author the person who owns the content of the page, even if you've written it.
- Always try to link to the relevant internal pages across the site. Never link the words 'click here' or even 'here'.
- Limit use of PDFs, convert to markdown wherever possible. Some things we will provide as downloadable PDFs if they're heavily designed and supposed to be shared/printed but generally no PDFs is the rule
- Put yourself in the position of the reader. What do you think they'll want to do next? Think about where to link out to elsewhere on the site.
- Keep the purpose of the page in mind (is there a call to action at the end?), and remember the audience: mostly non-native-English readers and people new to Crossref.
- Most pages end with `---`to make a line break before then a contact-us line - all email addresses should be generic, never a person's name (even if the link text is a name), and should link the team name and the generic mailto, e.g. `consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org)`

---

Thank you for following these guidelines! Please ask Ginny if you're unsure.