+++
title = "Crossref REST API Notes"
draft = false
author = "Bruce Murray"
date = "2019-03-15"
tags = [ "help",]
x-version = "0.0.0"

+++

## Image figure test here


### The minimum
&#123;&#123;&lt; figure-linked <br />
	src="/images/documentation/metadata-users-uses.png" <br />
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"<br />
	id="image31"<br />
&gt;&#125;&#125;

### With large and title parameters included
&#123;&#123;&lt; figure-linked <br />
	src="/images/documentation/metadata-users-uses.png" <br />
	large="/images/documentation/metadata-users-uses.png" <br />
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"<br />
	title="This is the title"
	id="image31"<br />
&gt;&#125;&#125;

Need some sort of unique id field too so that the button opens the right popup.

The modal width in the documentation section is set to much wider than standard here so it will popup to about 90% of the screen width.


{{< figure-linked
	src="/images/documentation/metadata-users-uses.png"
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"
	id="image31"
>}}