+++
title = "Website editors' reference"
draft = false
toc = true
author = "Ginny Hendricks"
date = "2021-01-26"
x-version = "0.0.0"

+++

This is the reference section for those of us creating and editing content on the Crossref website.

## Key information

- [How to publish content](/_editors/how-to-publish-content): A comprehensive set of instructions on how to add, edit and publish content **and key guidelines for content editors**
- [*preview-local* How To](/_editors/preview-local-how-to): A guide to using the *preview-local* command on your Mac for quicker and more efficient content publishing

## Editorial guidelines

- [Copy guidelines](/_editors/copy-guidelines): Guidelines on tone, language and style
- [Brand guidelines (PDF)](https://assets.crossref.org/private/docs/Crossref_BrandGuides_March2016.pdf): The official Crossref Brand Guidelines document

## Page templates

Page templates for several content types. You can copy the contents of one of these templates when you need to create a new page of the respective type. Each template includes appropriate frontmatter for the page type. We also link to a guide that explains how to add, edit and manage frontmatter in case you need to manually tweak the frontmatter for a special case.

- [Service page template](/_editors/about-service-template)
- [Blog post template](/_editors/blogs-how-to)
- [Frontmatter guide](/_editors/frontmatter-guide): a guide to adding, editing and managing frontmatter. Note that page templates already include appropriate frontmatter. This should only be needed if you need to manually tweak frontmatter for an special case

## Site structure guidelines

- [Content Conventions](/_editors/conventions): Guidelines for consistent naming of content and image files
- [Directory structure](/_editors/directory-structure): Directory structure and file naming conventions for website content directory. Includes instructions for creating versions of pages in other languages
- [Homepage How To](/_editors/homepage-how-to): How-To instructions for managing the Highlight Boxes on the homepage, and the example (self-typing) queries in the How Can We Help box
- [In Page Nav How To](/_editors/in-page-nav): How-To instructions for managing the "in page navigation", aka "child or grandchild nav" - the nav that appears as grey boxes on the right of most pages
- [Documentation section How To](/_editors/docs-how-to): How-To instructions specifically for the Documentation section of the website.

## Markdown tips

Content on the website is created using Markdown. Here are some Crossref-specific markdown tips. You can also refer to complete Markdown documentation.

- [Markdown examples](/_editors/markdown-examples): Quick and dirty guide to Markdown and, in particular, its idiocyncracies in Hugo

## Hugo tips

We use [Hugo](http://gohugo.io/) as our web site generator. This section provides Crossref-specific hints about using Hugo. More complete documentation for Hugo is available in the Documents section of the [Hugo website](http://gohugo.io/documentation/).

- [Related content](/_editors/related-content): Information about how Related Content Linking works, and how to create & configure links
- [Shortcodes reference](/_editors/shortcodes-reference): Information about how to use some of the custom styling created for this site

## Other information

The site is deployed automatically via [Travis CI](https://travis-ci.com/) which extracts and builds the source and content from the appropriate github repository when a change is detected.

- [How to block content from searches](/_editors/how-to-block-content): A summary of how to block content from the in-site search, from search engines like Google, or both.
- [REST API notes](/_editors/api-notes): Reference notes on how and where calls to the Crossref REST API are made from this site
- [dashboard dev notes](/_editors/dashboard-dev-notes): How the dashboard rebuild is setup

## List of all the help pages available to editors

{{% row %}}
{{% column-thirds %}}
[About service template](/_editors/about-service-template/)  
[Aliases how to](/_editors/aliases-how-to/)  
[API notes](/_editors/api-notes/)  
[Blogs how to](/_editors/blogs-how-to/)  
[Conventions](/_editors/conventions/)  
[Copy guidelines](/_editors/copy-guidelines/)  
[Dashboard dev notes](/_editors/dashboard-dev-notes/)  
[Deep linking](/_editors/deep-linking/)  
[Dev test](/_editors/dev-test/)  
[Directory structure](/_editors/directory-strucuture/)  
[Documentation section](/_editors/docs-how-to/)   
{{% /column-thirds %}}

{{% column-thirds %}}
[Embed videos](/_editors/embed-videos/)  
[Figure test](/_editors/figure-test/)  
[Frontmatter guide](/_editors/frontmatter-guide/)  
[Highlights](/_editors/highlights/)  
[Homepage how to](/_editors/homepage-how-to/)  
[How to block content](/_editors/how-to-block-content/)  
[How to publish content](/_editors/how-to-publish-content/)  
[Images how to](/_editors/images-how-to/)  
[In-page nav](/_editors/in-page-nav/)  
[Link report](/_editors/link-report/)  
[Markdown examples](/_editors/markdown-examples/)  
{{% /column-thirds %}}

{{% column-thirds %}}
[Our people how to](/_editors/our-people-how-to/)  
[Preview local how to](/_editors/preview-local-how-to/)  
[Quote test copy](/_editors/quote-test-copy/)  
[Quote test](/_editors/quote-test/)  
[Redirects](/_editors/redirects/)  
[Related content](/_editors/related-content/)  
[Shortcodes Reference](/_editors/shortcodes-reference/)  
[Style guide](/_editors/style-guide/)  
[Styles](/_editors/styles/)  
[Transition notes](/_editors/transition_notes/)    
{{% /column-thirds %}}
{{% /row %}}