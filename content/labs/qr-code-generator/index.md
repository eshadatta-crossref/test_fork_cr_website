---
author: Crossref
date: 2013-01-20
title: QR Code Generator
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}

_You might also be interested in [this handy resource](http://www.businessinsider.com/here-is-a-helpful-and-hilarious-flow-chart-to-help-you-decide-whether-to-use-a-qr-code-2013-4) for determining if you should be using QR Codes._

QR Codes are a form of barcode that can be easily scanned and used by mobile phones, web-cams, etc. A [QR code][1] can include encoded data, so, for instance, they are often used in Japan and other countries to encode URLs for display in magazines, on billboards, on packaging, etc.

For example, the following is the QR Code for the article “Strategic Reading, Ontologies, and the Future of Scientific Publishing” by Allen Renear and Carole Palmer.

<img class="alignnone size-full wp-image-37" alt="Sample QR code" src="/wp/labs/uploads/sample_qr_code.jpg" width="228" height="228" />

**API**

Crossref has created a sample API that will take a Crossref DOI and turn it into a QR Code. The QR Code will include the title of the document pointed to by the Crossref DOI as well as the “htttp://dx.doi.org” URL of that will allow you to link to that item. Most mobile phone QR Code readers will allow you to bookmark, share or link to the URLs thus encoded.

The API is pretty straight-forward and is of the form:

```
http://qrcode.labs.crossref.org/[doi]
```

So the URL to generate the above QR Code was:

```
http://qrcode.labs.crossref.org/10.1126/science.1157784
```

If you want to try out this technology, you might try installing the iPhone application <a style="line-height: 1.714285714; font-size: 1rem;" href="">QuickMark</a> from the Apple app store.

Note that the API doesn’t currently let you vary the size or encoding level of the QR Code. If there is enough interest in this functionality, we might add it.

**Applications**

So what can the qr-encoded Crossref DOI be used for? Damn if we know. Minimally, they could be added to the cover pages of PDFs so that researchers could easily bookmark a link to any paper articles they are reading. Perhaps they could also be added to the posters on poster sessions at conferences? Heck, maybe we could persuade researchers to print the QR-codes of their publications on t-shirts that they could then wear to conferences...

Or maybe not.

But seriously, we did this because it was easy and pretty cool. We’re not sure of the applications yet, so that’s why we thought we should release it and see what people can think of.

 [1]: http://en.wikipedia.org/wiki/QR_Code

{{% /labsection %}}