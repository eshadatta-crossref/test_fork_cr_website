---
author: Crossref
date: 2013-01-20
title: pmid2doi
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}
Crossref has worked with the [Concept Web Alliance](http://conceptweblog.wordpress.com/) to use Crossref Metadata Search to attempt to locate the DOIs for the 15 million items in PubMed that do not contain one. Using this technique, we have located the DOIs for an additional 3 million items and have combined them with existing pmid to doi mapping for a total of 7.7 million pmid to doi mappings. Using this data we have, in turn, deployed a pmid2doi mapping service on the Crossref Labs site. The service can be used interactively, via a web form as well as via content negotiation. So, for instance, using the ‘curl’ command line tool, one can issue the following content-negotiated querie:

```
curl -H "Accept: application/json" "http://pmid2doi.labs.crossref.org/5" -D - -L
```
and get the following response:

``` js
{“mapping":{"doi":"10.1016/0006-291X(75)90508-2","pmid”:”5”}}
```

or use the following query:

```
curl -H "Accept: application/pmiddoi+xml" "http://pmid2doi.labs.crossref.org/5" -D - -L
```

and get the following response:

```
10.1016/0006-291X(75)90508-25
```


**The Weasel Speaks&#8230;**

This service is based on a database snapshot taken in November, 2010. We are not yet updating the database. If there is enough interest in the service, we may work with the CWA to figure out how we can keep the database updated as efficiently as possible.

If you have any comments, suggestions, bug reports or (hint, hint) patches, please fee free to send them to us here at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


&nbsp;

{{% /labsection %}}