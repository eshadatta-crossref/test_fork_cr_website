---
author: Crossref
date: 2017-01-31
title: Removed
x-version: 0.0.0
---

{{% labsection %}}

This was a Labs experiment that has now either graduated or been deprecated. Check out the [other stuff we're working on](/labs) too.

{{% /labsection %}}