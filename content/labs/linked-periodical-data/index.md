---
author: Crossref
date: 2013-01-22
title: Linked Periodical Data
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}

We already make a list of Crossref members and titles available for free on our web site, including a downloadable coma-separated list] of them (caution: this file is large ~5MB).  But this format isn’t really amenable to easy integration with linked-data.

We worked with [Talis][3] on a project to create a linked periodical data resource on their Data Incubator platform. This has since been superseded by <a title="Crossref Labs: DOI Content Negotiation" href="https://crosscite.org" rel="self">our support for content negotiation.</a>


 [3]: http://www.talis.com/


{{% /labsection %}}