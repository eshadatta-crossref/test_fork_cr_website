---
author: Crossref
date: 2013-06-10
title: Open Funder Reconciliation Service
x-version: 0.0.0
---

{{% labsection %}}

## What?

The Open Funder Registry Reconciliation Service is designed to help members (or anybody) more easily clean-up their funder data and map it to the [Open Funder Registry](/services/funder-registry/). It is built on <a href="http://openrefine.org/" target="_blank">Open Refine</a> and <a href="http://search.crossref.org/funding" target="_blank">Funding Data Search.</a>

If you are impatient and want to see it working, then skip to the short <a href="http://oxford.crossref.org/documents/fundref/fundref_recon/" target="_blank">15-minute video tutorial</a>.

Or read on&#8230;

## Why?

Some of our members have been collecting data related to the funding of publications for years. Ideally, we would like them to submit Open Funder Registry identifiers for their backfiles into the Crossref system. However, we realize that there are at least two major barriers to doing this:

1. Publisher&#8217;s funder data may not have used a controlled vocabulary and may be inconsistent (e.g. they list &#8220;NASA&#8221; under both &#8220;NASA&#8221; and &#8220;National Aeronautics and Space Administration&#8221;)
2. Publishers have no easy way to map their existing, home-grown funder identifiers to the new standard Open Funder identifiers.

## How?

We have created the Open Funder Reconciliation Service to work with The <a href="http://openrefine.org/" target="_blank">Open Refine</a> desktop application. The Open Refine application provides a number of powerful tools that allow one to clean-up messy metadata and the Open Funder Reconciliation Service will allow one to take that cleaned-up data and semi-automatically map it to the latest version of the Open Funder Registry.

Basically, you start with a tab-delimited text file which includes every funder name you have recorded along with whatever *internal identifier* you use for that funder. For example:

```
publisher_id	name
1000001	University of Oxford
1000002	Oxford University
1000003	Welcome Trust
1000004	US Department of Transportation
1000005	National Aeronautics and Space Administration
1000006	National Institutes of Health
1000007	U.S. Department of Transportation
1000008	US Department of Transport
1000009	NASA
1000010	Wellcome Trust
1000011	Sloan Foundation
1000012	National Institute of Health
1000013	All Souls College, University of Oxford
1000014	Rinky Dink Foundation
```

Once you load that text file into Open Refine and process it, you will be left with a file that looks like this:

```
publisher_id name fundref_funder_id
1000001 University Of Oxford http://dx.doi.org/10.13039/501100000769
1000002 University Of Oxford http://dx.doi.org/10.13039/501100000769
1000003 Wellcome Trust http://dx.doi.org/10.13039/100004440
1000004 U.S. Department of Transportation http://dx.doi.org/10.13039/100000140
1000005 National Aeronautics and Space Administration http://dx.doi.org/10.13039/100000104
1000006 National Institutes of Health http://dx.doi.org/10.13039/100000002
1000007 U.S. Department of Transportation http://dx.doi.org/10.13039/100000140
1000008 U.S. Department of Transportation http://dx.doi.org/10.13039/100000140
1000009 National Aeronautics and Space Administration http://dx.doi.org/10.13039/100000104
1000010 Wellcome Trust http://dx.doi.org/10.13039/100004440
1000011 Alfred P. Sloan Foundation http://dx.doi.org/10.13039/100000879
1000012 National Institutes of Health http://dx.doi.org/10.13039/100000002
1000013 All Souls College, University of Oxford http://dx.doi.org/10.13039/501100000524
1000014 Rinky Dink Foundation
```

You can then use this latter file to convert all of your internal funder names and identifiers into Opne FUnder-compatible names and identifiers.

You can see a complete demo of how the system works by watching this <a href="http://oxford.crossref.org/documents/fundref/fundref_recon/" target="_blank">15-minute video tutorial</a>. The Open Refine website also has some more general tutorials on using the tool that you may find helpful.

Once you have watched the video, you can practice with the tool yourself. Simply <a href="http://openrefine.org/" target="_blank">download the Open Refine application</a> (runs of OSX, Windows and Linux) and our short, <a href="http://oxford.crossref.org/documents/fundref/publisher_funder_names_with_local_ids.txt" target="_blank">sample file of publisher funder data</a>. Two key pieces of information you may may not want to re-type are:

**The URL for the Open Funder Reconciliation Service**

`http://recon.labs.crossref.org/reconcile`

**The &#8220;Refine Expression Language&#8221; command for creating a new column of Open Funder Identifiers:**

`cell.recon.match.id`

Obviously, we welcome your feedback. Send comments/questions/gripes to:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

[2]: /services/funder-registry

{{% /labsection %}}