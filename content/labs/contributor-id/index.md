---
author: Crossref
date: 2017-01-22
title: Contributor ID
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}

Crossref Contributor ID started out in 2007 as a project to assign "author DOIs." [Initial discussions](/blog/crossref-author-id-meeting/) were held in a meeting in Washington DC on February 14th 2007. The project eventually metamorphosed into what is known today as [ORCID](http://www.orcid.org). Watch this space as we will eventually collect and link to documents and presentations that trace the genisis and history of this important initiative.

{{% /labsection %}}