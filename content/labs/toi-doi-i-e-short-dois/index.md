---
author: Crossref
date: 2013-01-22
title: TOI DOI (i.e. Short DOIs)
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}
“TOI DOIs” (pronounced “toy doi”) were an illustration of what a DOI shortening service my look like .

Please note that this project has actually “graduated” from Crossref labs and has now been implemented by the IDF as <a href="http://shortdoi.org/" target="_blank" rel="external">shortDOI</a>™

{{% /labsection %}}