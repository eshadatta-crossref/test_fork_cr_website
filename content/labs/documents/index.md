---
author: Karl Ward
date: 2014-11-03
title: Documents
x-version: 0.0.0
---

{{% labsection %}}
  * [Projects Using Crossref REST API][1]

 [1]: http://labs.crossref.org/projects-using-the-crossref-rest-api/ "Projects Using the Crossref REST API"

{{% /labsection %}}