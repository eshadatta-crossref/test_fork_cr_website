+++
date = "2016-03-28T22:33:07+02:00"
title = "RSS Recommendations"
type = "Labs"
x-version = "0.0.0"

+++

{{% labsection %}}
## RSS Recommendations

<object data="/pdfs/rss.pdf"></object>

{{% /labsection %}}