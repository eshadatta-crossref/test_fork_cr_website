---
author: Crossref
date: 2013-01-22
title: pdfstamp
x-version: 0.0.0
---

{{% labsection %}}
**Overview**

<a href="http://github.com/Crossref/pdfstamp" target="_blank" rel="external">Pdfstamp is an open source</a> command-line tool that allows you to add an image or &#8220;stamp&#8221; to any location on a PDF and to link said image to a URL of your choice.

This means that, when a user opens a &#8220;stamped PDF&#8221; and clicks on the image, they will (assuming they are connected to the internet) have their browser open and take them to the URL pointed to.

**Why?**

Well, we needed this functionality for a project that we are developing internally and we thought it might be useful to others in the community.

**Syntax**

The pdfstamp tool gives you pretty granular control over the positioning and resolution of the placed image. It also allows you to specify how to name files for batch processes.

<pre>Usage: pdfstamp [options] |
-d N : Optional. Target DPI. Defaults to 300.
-e EXT : Optional. Extension appended to the PDF filename.
-i FILE : Required. Image file containing image of the stamp.
-l X,Y : Required. Location on page to apply stamp.
-o FILE : Optional. Output directory.
-p P1,P2... : Optional. Page numbers to stamp. -1 is the last page.
-r : Optional. Descend recursively into directories.
-u URL : Optional. Target URL of the stamp.
-v : Optional. Verbose output.</pre>

**Notes**

Pdfstamp is non-destructive. It will always generate a new PDF with the stamp added to it. However, If you are using pdfstamp on a &#8220;linearized&#8221; (aka &#8220;optimised&#8221; or &#8220;fast web view enabled&#8221;) PDF, the resulting PDF will need to be re-linearized using ghostscript’s `pdfwrite` device, and specifying `-dFastWebView` or any similar tool (e.g. Acrobat Pro).

Pdfstamp is designed for batch-processing and, to give you an idea of performance, we were able to stamp, link and re-linearize (using pdfopt) 500 journal article PDFs in about eight seconds on a recent-vintage Macbook Pro. Performance very much depends on the resolution and size of the image that you are adding to the PDF and the size of the PDF itself.

**What next?**

If you have any comments, suggestions, bug reports or (hint, hint) patches, please fee free to send them to us here at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


Or just complain bitterly on Twitter.

{{% /labsection %}}