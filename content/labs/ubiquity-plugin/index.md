---
author: Crossref
date: 2013-01-22
title: Ubiquity Plugin
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}

Mozilla’s Ubiquity is an excellent proof-of-concept tool that allows one to easily create extensions to the FireFox browser. Crossref has created a sample command set that allows you to either:

  * Select a citation in a web page and automatically resolve the citation to the online version of the item cited.
  * Select a citation in a web page and automatically show the Crossref metadata for the item cited.

Obviously, this will only work if the cited item is actually online (duh) and if the item has been assigned a Crossref DOI. In practice this applies to the vast majority of scholarly and professional journals and conference proceedings.

In order to use the Crossref command you need to install Ubiquity into your copy of FireFox and then return to this page. Once you have returned to this page, you should see an option at the top of the page that will allow you to install the Crossref command. Note that when you install a Ubiquity command you will be given all sorts of dire security warnings. The Ubiquity site explains these warnings and what they mean. It is up to you as to whether you trust Crossref ;-).

Once the Crossref command is installed, you need to:

  * Select the citation that interests you (for practice, we have included a few samples below)
  * Invoke Ubiquity (typically by typing control/alt + space) and type in the crossref command.
  * Wait until you get a list of potential hits. Once you have a list of hits, pressing enter will take you to the first document cited. Otherwise, you can click on either the Resolve or the Metadata of any listed citation.

This Ubiquity command makes use of the metadata search system which lies behind the WordPress and Moveable Type plugins [that we released previously][1]. The search system is very experimental and you should be careful to note that it can sometimes return the wrong item. Also note that the index only includes journal articles and conference proceedings. It does NOT yet contain books.

Finally, be aware that Ubiquity itself is very much a proof-of-concept. &#8220;In flux&#8221;, as they say. The same applies to this Crossref command. This is a proof-of-concept and it is almost guaranteed to break as Ubiquity evolves. In fact, the few people who have tested this have had a variety of inconsistent results. It seems that you really need to make sure that you have current versions of FireFox and Ubiquity. It also seems to be temperamental about other FireFox extensions. Having said all that you can see the source (MPL) and try to fix and/or evolve it yourself. Any comments, questions, suggestions, fixes, etc. can be directed to: <citation-plugin@crossref.org>

Sample References To Play With

Free Form Text

<pre>Allen Renear's "What is Text Really?"</pre>

Citation Which Already Includes An Unlinked DOI

<pre>Pavol Hell , Jaroslav Nesetril, The core of a graph, Discrete Mathematics, v.109 n.1-3, p.117-126, Nov. 12, 1992 [ doi:10.1016/0012-365X(92)90282-K ]</pre>

Citation Which Contains No DOI

<pre>Miklos Ajtai , Yuri Gurevich, Datalog vs. first-order logic, Journal of Computer and System Sciences, v.49 n.3, p.562-588, Dec. 1994</pre>

 [1]: http://sourceforge.net/projects/crossref-cite/

{{% /labsection %}}