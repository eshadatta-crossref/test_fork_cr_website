---
author: Crossref
date: 2013-01-22
title: Reverse Domain Lookup
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}
##

_This service has graduated and metamorphised into [Event Data Reverse](https://github.com/CrossRef/event-data-reverse). I know, we suck at naming things._

Take a domain name and find out whether it belongs to a Crossref member. Why on earth would you want to do such a thing? Well, you might be interested in analysing your log files and determining whether links to your site are coming from scholarly publishers. Or you might be building some snazzy new “<a href="https://web.archive.org/web/20190330190038/http://altmetrics.org/manifesto/" target="_blank" rel="external">alt-metrics</a>” tools and want to be able to detect when people link **to** a scholarly publisher. In either case, this now allows you to take a domain name (e.g. plosbiology.org) and quickly determine whether it belongs to a Crossref member (i.e. a scholarly publisher) as well as the name of said publisher.

We’ve developed an <a href="" rel="self">example service</a>, which you can call dynamically, a browser button, which allows you to play with the service interactively, and we’ve also created a downloadable list of the domain names (hashed) which you can use locally if speed is of the essence.

The service page linked to above contains documentation and plenty of weasel words explaining the limitations of the system.

If you have any comments, suggestions or bug reports please fee free to send them to us here at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


{{% /labsection %}}