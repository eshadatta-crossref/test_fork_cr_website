---
author: Crossref
date: 2013-01-20
title: InChI Lookup
x-version: 0.0.0
---

{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}

{{% labsection %}}
The idea is to create a mechanism that would allow Crossref publishers to record [InChIs][1] in their submitted Crossref metadata. This, in turn, would allow us to provide a service that would allow users to:

  * Lookup the published articles that mention a particular InChI.
  * Lookup the InChIs mentioned in a published article.

Similar services could, conceivably, be provided for other types of semantic metadata.

The following is a demonstrator of what an DOI2InChI lookup service might look like. Please note that the XML representation of the results is very basic and is not best-practice for linked-data.

The demonstrator currently only holds DOIs and InChIs for a few publishers. A summary of the contents of the database can be found on the status page

```
http://inchi.crossref.org/status
```

A list of all the Crossref DOIs that contain InChIs can be seen here:

```
http://inchi.crossref.org/dois
```

A list of all the InChIs that have been registered with Crossref can be seen here:

```
http://inchi.crossref.org/inchis
```

The system provides the following API calls:

Return all the DOIs that have been registered with a given InChI

```
http://inchi.crossref.org/dois/InChI=1S/C4H6O2/c1-3-6-4(2)5/h3H,1H2,2H3
```

Return all the InChIs that have been registered for a given DOI

```
http://inchi.crossref.org/inchis/10.1038/nchem.215
```

 [1]: http://en.wikipedia.org/wiki/International_Chemical_Identifier

{{% /labsection %}}