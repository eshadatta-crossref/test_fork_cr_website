+++
title = "Canceling your membership"
date = "2022-11-23"
draft = false
author = "Amanda Bartell"
identifier = "members-area/canceling"
rank = 4
weight = 140403
aliases = [ "/documentation/metadata-stewardship/maintaining-your-crossref-membership/canceling", "/documentation/metadata-stewardship/maintaining-your-crossref-membership/canceling/",]
x-version = "0.0.0"

+++

We expect organizations to remain members of Crossref for the long run - by committing to our [membership terms](/membership/terms), you’ve committed to the long-term stewardship of your metadata and content.

However, there are sometimes reasons why members need to cancel. Perhaps your organization has ceased publishing, or been acquired, or perhaps you are no longer able to afford the fees.

If you can no longer afford the fees, do take a look at our [Global Equitable Membership (GEM) Program page](/gem/) before canceling, to see if you qualify for assistance, or if working with a sponsor will help.

## Important: you need to tell us that you want to cancel

To cancel, contact us by [filling out this form](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) and providing as much information about your account as possible (account name, prefix, etc) and the reason for the cancellation. This is extremely important - you aren’t able to pause your membership, so if you just stop using the service and don’t tell us that you want to cancel, we will still continue to send you annual membership fee invoices. And if you then want to start using the service again in future years, you will need to pay these outstanding membership invoices. But if you actually contact us to cancel your membership, we can stop these annual membership invoices from being created.

## What happens to your DOIs after you cancel your membership?

We have responsibilities after you cancel your membership. We will ensure that the DOIs that you have already registered with us will continue to exist and to resolve to your registered landing page, and that your metadata will continue to be openly shared through our APIs. That’s what makes your DOI a persistent identifier! However, after you cancel, you won’t be able to update this metadata, or register any more DOIs.

You also have responsibilities after you cancel your membership. You must ensure that you don’t display any new, unregistered DOIs. You also need to make sure that your existing DOIs continue to resolve to a live landing page - and we can help with that.

## How to ensure that your DOIs continue to resolve to a live landing page
There are different options here depending on your situation.

### 1. You’re not going to be publishing any new content but are continuing to host your existing content
If you continue hosting your content in the same location, this is fine. The DOIs that you’ve already registered will continue to resolve to the resource resolution URL that you’ve registered with us. If this changes in future, please get in contact. We will be able to help update your resource resolution URLs, even if you are no longer a member.

### 2. You’re not going to be publishing any new content and will no longer be hosting your existing content
We recommend that all members [work with an archive provider](/documentation/content-registration/administrative-metadata/archive-locations/). If you can no longer host your content, we will be able to work with the archive to get your resource resolution URLs to resolve to the archive version, ensuring that the DOIs continue to resolve to a version of the content.

### 3. One or more of your journals has been acquired by another publisher
If your content has been acquired by a different publisher, we will be able to work with you and the acquiring publisher to transfer ownership of your title(s) to the new owner in our system.

This will transfer ownership of all your existing DOIs to the new publisher. The new publisher will then be able to update the metadata (and resource resolution URLs) for these existing DOIs, even though these DOIs are based on your DOI prefix. This means that the DOIs that you originally registered for this content will remain the persistent identifier for the content for the long term. And if the new publisher needs to register new DOIs for new content on their own prefix, they can do this. You can read more about [our title transfer policy here](documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/). Our colleague Isaac also gives more background on this subject in [this blog post](/blog/what-can-often-change-but-always-stays-the-same/).

### 4. 	Your organization has been acquired by another publisher
If your organization has been acquired by another publisher, we will be able to work with you and the acquiring publisher to transfer ownership of your prefix(es) and existing DOIs to them. They can then choose whether they want to continue to use your prefix to register new DOIs, or transfer your titles to their prefix and use their own prefix for future DOIs. Either way, your existing DOIs will continue to work and should continue to be used. Find out more about [prefix transfers](documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois/).

[Back to members area](/members-area)