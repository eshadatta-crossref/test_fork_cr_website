+++
title = "Hello, members"
date = "2022-10-19"
image = "/images/banner-images/members-area-all-welcome.jpg"
maskcolor = "crossref-red"
weight = 1
rank = 4
aliases = [ "/01company/credit_card_payment.html", "/06members/", "/06members/index.html", "/members_only",]
x-version = "0.0.0"

+++

## Welcome to the Crossref member's area

{{% row layout=equal %}}
{{% column %}}
{{% divwrap lightgrey-highlight %}}

## Haven't joined us yet?
You need to be a member of Crossref in order to get a DOI prefix so you can create Crossref DOIs and register content. Membership allows you to connect your content with a global network of online scholarly research, currently over 17,000 other organizational members from 140 countries. It’s so much more than just getting a DOI.

Please note: You don’t need to be a member to just use others’ metadata - if that's all you need, read more about our [open metadata retrieval tools](/services/metadata-retrieval/).

### Apply to become a Crossref member   
If you haven't joined Crossref yet, you can [read more about membership and apply to join](/membership) here.          
<br><br>

{{% /divwrap %}}
{{% /column %}}

{{% column %}}
{{% divwrap blue-highlight %}}
## New member?

### Start registering your content
Follow our [new member setup guide](/documentation/member-setup).

### Get help
You can get help from our detailed [support docs](/documentation), [request support](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) from our small team, or head to the [community forum](https://community.crossref.org) to ask others (there is a [specific category for new members](https://community.crossref.org/c/new-crossref-members/14)).

Some key documentation that you might be interested in:
* How to [construct your DOIs](https://www.crossref.org/documentation/member-setup/constructing-your-dois/)
* [Web deposit form](https://www.crossref.org/documentation/register-maintain-records/web-deposit-form/) to manually [register your metadata records](https://apps.crossref.org/webDeposit/)
* Manage your records in our [Content Registration system](https://doi.crossref.org/servlet/home)
* [Verify your registration](/documentation/register-maintain-records/verify-your-registration)
* [Understand the reports that we send by email](/documentation/reports)
* [Add references to your metadata](/documentation/register-maintain-records/maintaining-your-metadata/add-references)
{{% /divwrap %}}
{{% /column %}}
{{% /row %}}

{{% row layout=equal %}}
{{% column %}}
{{% divwrap red-highlight %}}
## Been a member for a while?
Alongside the basics of registering your content, there are other services you can take advantage of:
* [Funder Registry](/services/funder-registry)
* [Similarity Check](/services/similarity-check)
* [Cited by](/services/cited-by/)
* [Crossmark](/services/crossmark/)
* [Reference Linking](/services/reference-linking)

It's also important to keep the metadata of your existing records up to date - more on [maintaining your metadata](/documentation/register-maintain-records/maintaining-your-metadata/). And finally, don't forget to [include references in the content you register with us](/documentation/register-maintain-records/maintaining-your-metadata/add-references/).
{{% /divwrap %}}
{{% /column %}}

{{% column %}}
{{% divwrap yellow-highlight %}}
## Maintaining your membership
There are several things you need to do in order to maintain your membership.
* Pay your invoices - more on our [billing FAQs page](/members-area/all-about-invoicing-and-payment).
* Plan any platform migrations carefully - read our [platform migration guide](/documentation/member-setup/working-with-a-service-provider/planning-a-platform-migration).
* [Let us know](/members-area/update-contacts) if any of your contact details change
* Continue to meet your member obligations - you can find [a reminder of these here](/membership).
* Respond to the reports we send you and fix any errors - [more on reports](/documentation/reports).
{{% /divwrap %}}
{{% /column %}}
{{% /row %}}

{{% row layout=equal %}}
{{% column %}}
{{% divwrap blue-highlight %}}
## Get more involved
* [Sign up](/subscribe-newsletter/) to our bi-monthly newsletter. You can [read the latest issue here](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0036/t/page/fm/0).
* Vote in our board elections - more about our [board and governance](/board-and-governance)
* Attend a webinar or event - [our latest events](/events)
* Join an advisory group or working group - [find out more](/working-groups)
* Help and advise other members on our [community forum](https://community.crossref.org/)
{{% /divwrap %}}
{{% /column %}}

{{% column %}}
{{% divwrap darkgrey-highlight %}}
## Cancel your membership
By committing to our membership terms, you’ve committed to the long-term stewardship of your metadata and content. However, there are sometimes reasons why members need to cancel. It's important that you tell us if you wish to cancel - otherwise we'll continue to send you annual membership fee invoices and you'll continue to be responsible for them. [Find out more](/members-area/canceling).  
{{% /divwrap %}}
{{% /column %}}
{{% /row %}}