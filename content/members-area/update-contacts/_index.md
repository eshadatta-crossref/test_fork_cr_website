+++
title = "Keep your account details up-to-date "
date = "2022-07-28"
draft = false
author = "Amanda Bartell"
identifier = "members-area/update-contacts"
rank = 4
weight = 140200
aliases = [ "/education/metadata-stewardship/maintaining-your-crossref-membership", "/education/metadata-stewardship/maintaining-your-crossref-membership/", "/documentation/metadata-stewardship/maintaining-your-crossref-membership", "/documentation/metadata-stewardship/maintaining-your-crossref-membership/",]
x-version = "0.0.0"

+++

To make sure that we are contacting the right people at your organization, and to make sure that we include the correct information on your invoices, we need to know if anything changes for you.

Please let us know by completing our [contact form](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) if any of the following changes:
* Your organization name
* Your mailing or billing address (If you are using our payment portal, you can update the billing address associated with your credit card there, but if you need a different billing address to appear on your invoice, you will need to let us know.)
* A significant change to your organizations's annual publishing revenue (this may mean that we need to change your [annual member fee](/fees/#annual-membership-fees-non-funders) tier.)
* A change to one or more of the key contacts on your account.

### Five key contacts for each account<a id='00022' href='#00022'><i class='fas fa-link'></i></a>
When you join Crossref, you provide contact details for (ideally) at least three different people at your organization to undertake five roles. These are key to making your relationship with Crossref and the rest of the community a success, so do think carefully about who will take on each of these roles. These will be the people we contact to confirm any changes to your account.

* The **Primary** contact - this person will be our key contact at your organization. They receive product and service updates, and we contact about things like changes to terms or service agreements. They also receive our monthly [resolution reports](/documentation/reports/resolution-report) showing failed resolutions on your DOIs. (Those of you who have been members for a while will know this contact as the Business contact).
* The **Voting** contact - this person will vote in our Board elections. The Voting contact is often the same person as the Primary contact.
* The **Technical** contact - this person will receive technical updates, [DOI error reports](/documentation/reports/doi-error-report), and [conflict reports](/documentation/reports/conflict-report) to help you solve problems with your content quickly. We encourage you to use a shared, generic email address for this contact.
* The **Metadata quality** contact - this person will be responsible for fixing any metadata errors that are spotted by the scholarly community. The Metadata Quality contact is often the same person as the Technical contact.
* The **Billing contact** - this person will receive invoices from us and pay the [annual membership and ongoing content registration fees](/fees). They will also receive reminder emails about unpaid invoices. We encourage you to use a shared, generic email address for this contact.

[Back to members area](/members-area)