+++
title = "Certificate of Incorporation"
date = "2016-11-23"
draft = false
author = "Ed Pentz"
x-version = "0.0.0"

[menu.main]
parent = "Board & governance"
weight = 5

+++

**CERTIFICATE OF INCORPORATION**

OF

PUBLISHERS INTERNATIONAL LINKING ASSOCIATION, INC.

Under Section 402 of the Not-For-Profit Corporation Law

 <span class="rule01"></span>

The undersigned, being a natural person of at least eighteen years of age and acting as the incorporator of the corporation hereby being formed under the Not-For-Profit Corporation Law, certifies that:

<span class="rule01"></span>

<span class="bold">FIRST:</span> The name of the corporation is PUBLISHERS INTERNATIONAL LINKING ASSOCIATION, INC. (the "_Corporation_").

 <span class="rule01"></span>

<span class="bold">SECOND:</span> The Corporation is a corporation as defined in Subparagraph (a)(5) of Section 102 of the Not-For-Profit Corporation Law.

 <span class="rule01"></span>

<span class="bold">THIRD:</span> The Corporation shall be a Type B corporation under Section 201 of the Not-For-Profit Corporation Law.

 <span class="rule01"></span>

<span class="bold">FOURTH:</span> The Corporation is formed for the following purposes:

To promote the development and cooperative use of new and innovative technologies to speed and facilitate scientific and other scholarly research; and shall have in furtherance of its not-for-profit corporate purposes, all of the powers conferred upon corporations organized under the Not-For-Profit Corporation Law subject to any limitations thereof contained in this Certificate of Incorporation or in the laws of the State of New York.

<span class="rule01"></span>

<span class="bold">FIFTH:</span> The office of the Corporation is to be located in New York County, New York.

 <span class="rule01"></span>

<span class="bold">SIXTH:</span> (a) The name and the address of each of the initial directors of the Corporation are as follows:

<table cellspacing="0" border="0" width="431">

<tbody>

<tr>

<td width="31%" valign="top">

Pieter Bolman

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Academic Press, Inc.

525 B Street, Suite 1900

San Diego, CA 92101

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Michael Spinella

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

The American Association for the Advancement of Science

1200 New York Avenue, N.W.

Washington, DC 20005

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Marc Brodsky

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

American Institute of Physics

One Physics Ellipse

College Park, MD 20742

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

John R. White

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Association for Computing Machinery

1515 Broadway, 17th Floor

New York, NY 10036

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

John Strange

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Blackwell Science

Osney Mead

Oxford OX2 OEL, England

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

John Regazzi

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Elsevier Science

655 Avenue of the Americas

New York, NY 10010

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Anthony Durniak

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

IEEE

445 Hoes Lane

Piscataway, NJ 08855-1331

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Jeffrey K. Smith

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Kluwer Academic Publishers

Spuiboulevard 50

3311 GR Dordrecht, The Netherlands

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Stefan von Holtzbrinck

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Nature Publishing Group

4-6 Crinan Street

London N1 GXW, England

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

</tbody>

</table>

<table cellspacing="0" border="0" width="431">

<tbody>

<tr>

<td width="31%" valign="top">

Martin Richardson

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Oxford University Press

Great Clarendon Street

Oxford OX2 6DP, England

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Ruediger Gebauer

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

Springer Verlag

175 Fifth Avenue

New York, NY 10010

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

<tr>

<td width="31%" valign="top">

Eric Swanson

</td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top">

John Wiley & Sons, Inc.

605 Third Avenue

New York, NY 10158-0012

</td>

</tr>

<tr>

<td width="31%" valign="top"> </td>

<td width="3%" valign="top"> </td>

<td width="66%" valign="top"> </td>

</tr>

</tbody>

</table>

(b) If any person appointed or elected to be a director of the Corporation (i) resigns from the Board of Directors in writing, (ii) becomes sufficiently disabled so that, in the reasonable discretion of the balance of the Board of Directors, said person is unable to fulfill his or her duties as a director, or (iii) ceases to be employed by the entity which employed him or her at the time of such appointment or election (unless such entity wishes said person to remain a director and he or she agrees to do so), then said person shall be deemed to have resigned from the Board of Directors of the Corporation effective at the time at which said written resignation is received by an officer of the Corporation, the Board of Directors makes such a determination of disability, or said employment ceases, as the case may be, and the entity which employed said person at the time of his or her appointment or election shall have the right, power and authority to designate his or her successor who, subject to ratification by the balance of the Board of Directors, shall serve the balance of his or her then extant term as a director. Notwithstanding any other provision of this Certificate of Incorporation, if any entity which employs a director of the Corporation ceases to be a member of the Corporation, then said director shall be deemed to have resigned from the Board of Directors effective as of the date on which said entity ceases to be a member of the Corporation and the balance of the Board of Directors shall have the right, power and authority, as set forth in the By-Laws of the Corporation, to fill the vacancy created by such deemed resignation and the person selected by the Board to fill said vacancy shall serve the balance of the then-extant term of the director so deemed to have resigned.

\(c\) Each director appointed above (or his or her duly appointed successor under the preceding paragraph) shall serve as a director for an initial term of two (2) years. At the annual meeting of members of the Corporation to be held after the first anniversary and before the second anniversary of the date of incorporation, there shall be an election for that number of directors of the Corporation designated by the Board of Directors, one-third of whom shall be elected for a term of one (1) year, one-third of whom shall be elected for a term of two (2) years, and the last third of whom shall be elected for a term of three (3) years. At each annual meeting thereafter, a number of directors equal to that of those whose terms have expired shall be elected for the term of three (3) years. At the expiration of any term of three (3) years, any director may be re-elected. In any event, each director's term shall be deemed extended or shortened until such time as his or her successor shall be duly elected and have qualified.

 <span class="rule01"></span>

<span class="bold">SEVENTH:</span> The duration of the Corporation is to be perpetual.

<span class="rule01"></span>

<span class="bold">EIGHTH:</span> The Secretary of State is designated as the agent of the Corporation upon whom process against the Corporation may be served. The post office address within the State of New York to which the Secretary of State shall mail a copy of any process against the Corporation served upon him is:

Kay Collyer & Boose LLP, One Dag Hammarskjold Plaza, New York, New York 10017, Attention: Claude P. Goetz, Esq.

 <span class="rule01"></span>

<span class="bold">NINTH:</span> The management of the Corporation and the conduct of its affairs shall be vested in its Board of Directors to the fullest extent permitted by the Not-For-Profit Corporation Law. Such right, power and authority shall include, but not be limited to, amending the Corporation's by-laws, appointing committees of the Board of Directors, each of which, when duly appointed shall act with the delegated power and authority of the entire Board of Directors to the fullest extent permitted by the Not-For-Profit Corporation Law, establishing classes of membership and the respective rights, designations and preferences, if any, of each such class, determining criteria for membership in the Corporation and in any such class (including determining and/or varying the terms and conditions of any standard agreement between and/or among the Corporation and its members), and setting, amending and/or waiving membership dues generally and with respect to any particular member.

 <span class="rule01"></span>

<span class="bold">TENTH:</span> The personal liability of the directors and officers of the Corporation is hereby eliminated to the fullest extent permitted by Sections 719, 720 and 720-a of the Not-For-Profit Corporation Law, as the same may be amended and supplemented, from time to time.

 <span class="rule01"></span>

<span class="bold">ELEVENTH:</span> The Corporation shall, to the fullest extent permitted by Sections 721 <u>et</u> <u>seq</u>. of the Not-For-Profit Corporation Law, as the same may be amended and supplemented from time to time, indemnify any and all persons whom it shall have power to indemnify under said sections from and against any and all of the expenses, liabilities or other matters referred to in, or covered by, said sections, and the indemnification provided for herein shall not be deemed exclusive of any other rights to which those indemnified may be entitled under any by-law, agreement, vote of members or disinterested directors or otherwise, both as to action in his or her official capacity and as to action in another capacity while holding such office, and shall continue as to a person who has ceased to be a director, officer, employee or agent and shall inure to the benefit of the heirs, executors and administrators of any such person. Notwithstanding the foregoing, no indemnification may be made to or on behalf of any director or officer if a judgment or other final adjudication adverse to the director or officer establishes that his or her acts were committed in bad faith or the result of active and deliberate dishonesty and were material to the cause of action so adjudicated, or that he or she personally gained in fact a financial profit or other advantage to which he or she was not legally entitled.

 <span class="rule01"></span>

<span class="bold">TWELFTH:</span> Membership in the Corporation shall be open to all publishers of original scientific, technical, medical or other scholarly material which otherwise meet the terms and conditions of membership set from time to time by the Board of Directors and to such other entities as the Board of Directors shall determine from time to time.

 <span class="rule01"></span>

<span class="bold">THIRTEENTH:</span> Upon any non-judicial dissolution of the Corporation, subject to the applicable provisions of Section 516 of the Not-For-Profit Corporation Law, as the same may be amended and supplemented from time to time, any assets remaining after payment of all creditors and retention of a reserve deemed to be appropriate by the Corporation's Board of Directors shall be distributed to the members of the Corporation at dissolution in the proportion that each such member's aggregate dues during the three (3) years immediately preceding the date of such dissolution (or such lesser time during which said entity was a member of the Corporation) bears to the aggregate amount of all dues collected by the Corporation during the three (3) years immediately preceding the date of such dissolution (or such lesser time during which the Corporation was in existence) from members at the date of such dissolution.

Signed on January 18, 2000

___________________________________

Claude P. Goetz, Sole Incorporator

Kay Collyer & Boose LLP

One Dag Hammarskjold Plaza

New York, New York 10017

</div>
</html>