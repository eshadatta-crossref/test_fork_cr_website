+++
title = "Election process and results"
date = "2022-09-15"
draft = false
author = "Lucy Ofiesh"
x-version = "0.0.0"

[menu.main]
parent = "Board & governance"
weight = 1
rank = 4

+++

## About board elections

Our board terms are three years, and one third of the board is eligible for election every year. There is a [Nominating Committee](/committees/nominating/) consisting of three board members not up for re-election, and two Crossref members that are not on the board. The purpose of this committee is to review and create the slate each year for nominations to the board, ensuring a fair representation of membership.

The Nominating Committee meets to discuss the charge, process, criteria, and potential candidates, and puts forward a slate which is at least equal to the number of board seats up for election. Each year members are notified of the election schedule but generally the election opens online in late September and election results are announced at our annual meeting in November.

In 2017 the Nominating Committee introduced two things for the first time: to issue an open call for expressions of interest; and to propose a slate with more candidates than seats available.

In 2018, the board voted to balance the board with equal numbers of small and large revenue category members starting from the 2019 election.

Please see the [full current board list](/board-and-governance).

The election takes place in October or November each year during the Annual Meeting. Each member organization designates a voting contact when they join, and that member receives a unique link each September to place their vote on behalf of their organization.    


## Past elections 

### 2022 election
The following organizations were elected to the board for three-year terms commencing March 2023:

* Pan Africa Science Journal, Oscar Donde
* Clarivate, Christine Stohn
* Elsevier, Rose L’Huillier  
* The MIT Press, Nick Lindsay
* Springer Nature, Anjalie Nawaratne  



[Candidate statements](/board-and-governance/elections/2022-slate/)  | [Election procedures](/pdfs/Election-procedures-2022.pdf) | Proxy: in [English](/pdfs/2022proxy-EN.pdf), [em Português](/pdfs/2022proxy-PT-BR.pdf), [en Español](/pdfs/2022proxy-ES.pdf), [한국어](/pdfs/2022proxy-KO.pdf), [en Français](/pdfs/2022proxy-FR.pdf)  

### 2021 election
The following organizations were elected to the board for three-year terms commencing March 2022:
* California Digital Library, University of California, Lisa Schiff
* Center for Open Science, Nici Pfeiffer
* Melanoma Research Alliance, Kristen Mueller
* AIP Publishing (AIP), Penelope Lewis
* American Psychological Association (APA), Jasper Simons

[Candidate statements](/board-and-governance/elections/2021-slate/) | [Election procedures](/pdfs/elections-2021-procedures.pdf) | Proxy: in [English](/pdfs/proxy2021-EN.pdf), [em Português](/pdfs/proxy2021-BR.pdf), [en Español](/pdfs/proxy2021-ES.pdf), [한국어](/pdfs/proxy2021-KO.pdf), [en Français](/pdfs/proxy2021-FR.pdf)  

### 2020 election

The following organizations were elected to the board for three-year terms commencing March 2021:

* OpenEdition, Marin Dacos (France)
* Korean Council of Science Editors, Kihong Kim (South Korea)
* Scientific Electronic Library Online (SciELO), Abel Packer (Brazil)
* Beilstein-Institut, Wendy Patterson (Germany)
* Taylor & Francis/F1000, Liz Allen (United Kingdom)
* Oxford University Press, James Phillpotts (United Kingdom)

[Candidate statements](/board-and-governance/elections/2020-slate/) | [Election procedures](/pdfs/elections-2020-procedures.pdf)  

### 2019 elections

The following organizations were elected to the board for three-year terms:  

* Clarivate Analytics, Nandita Quaderi
* eLife, Melissa Harrison
* Elsevier, Chris Shillum
* Springer Nature, Reshma Shaikh
* Wiley, Todd Toler

[Candidate statements](/board-and-governance/elections/2019-slate) | [Election procedures](/pdfs/elections-2019-procedures.pdf)

### 2018 election

The following organizations were elected to the board for three-year terms:

* African Journals OnLine, Susan Murray
* California Digital Library, Catherine Mitchell
* Association for Computing Machinery, Scott Delman
* Hindawi, Paul Peters
* American Psychological Association, Jasper Simons  

[Proxy](/pdfs/elections-2018-proxy.pdf) | [Candidate statements](/board-and-governance/elections/2018-slate) | [Election procedures](/pdfs/elections-2018-procedures.pdf)

### 2017 election

The following organizations were elected to the board for three-year terms:

* AIP Publishing Inc., Jason Wilde
* F1000, Liz Allen
* MIT Press, Amy Brand
* SciELO, Abel Packer
* Vilnius Gediminas Technical University Press, Eleonora Dagiene

[Proxy](/pdfs/elections-2017-proxy.pdf) | [Candidate Statements](/board-and-governance/elections/2017-slate) | [Election Procedures](/pdfs/elections-2017-procedures.pdf)

### 2016 election

The following organizations were elected to the board for three-year terms:

* BMJ, Helen King
* eLife, Mark Patterson
* Elsevier, Chris Shillum
* IOP, James Walker
* Springer Nature, Wim van der Stelt

[Proxy](/pdfs/proxy2016.pdf) | [Notice](/pdfs/notice2016.pdf) | [Candidates](/pdfs/board-candidates.pdf) | [Candidate Statements](/pdfs/candidate-statements.pdf) | [Election Procedures](/pdfs/election-procedures.pdf)

### 2015 election

The following organizations were elected to the board for three-year terms:

* Ian Bannerman, Informa UK
* Paul Peters, Hindawi
* Bernie Rous, ACM
* Peter Marney, Wiley
* John Shaw, Sage Publications

### 2014 election

The following organizations were elected to the board for three-year terms:

* Jason Wilde, AIP Publishing Inc.
* Gary VandenBos, American Psychological Association
* Gerry Grenier, IEEE
* Eleonora Dagiene, Vilnius Gediminas Technical University Press
* Carsten Buhr, Walter de Gryuter
* Y. H. (Helen) Zhang, Zhejiang University Press

### 2013 election

The following organizations were elected to the board for three-year terms:

* Chris Shillum, Elsevier
* James Walker, IOP Publishing
* Kathleen Keane, Johns Hopkins University Press
* Kristen Fisher Ratan, PLOS
* Wim van der Stelt, Springer

### 2012 election

The following organizations were elected to the board for three-year terms:

* Ian Bannerman, Informa UK (Chair)
* Bernard Rous, ACM (Treasurer)
* Ahmed Hindawi, Hindawi
* Robert Campbell, John Wiley & Sons, Inc.
* Carol Richman, Sage Publications
* Sven Fund, Walter de Gruyter has replaced Rebecca Simon, University of California Press and will serve out their term expiring in 2014.

### 2011 election

The following organizations were elected to the board for three-year terms:

* Terry Hulbert, American Institute of Physics
* Linda Beebe, American Psychological Association
* Gerry Grenier, IEEE
* Patricia Shaffer, INFORMS
* Rebecca Simon, University of California Press
* Chi Wai Lee, World Scientific Publishing

### 2010 election

The following organizations were elected to the board for three-year terms:

* Karen Hunter, Elsevier
* Steven Hall, IOP Publishing
* Howard Ratner, Nature Publishing Group
* Stuart Taylor, The Royal Society
* Wim van der Stelt, Springer Science + Business Media

### 2009 election

The following organizations were elected to the board for three-year terms:

* Bernard Rous, ACM
* Ian Bannerman, Informa UK
* Robert Campbell, John Wiley & Sons Inc.
* Carol Richman, Sage Publications
* Ahmed Hindawi, Hindawi

### 2008 election

The following organizations were elected to the board for three-year terms:

* Tim Ingoldsby, American Institute of Physics
* Linda Beebe, American Psychological Association
* Paul Reekie, CSIRO Publishing
* Anthony Durniak, IEEE
* Patricia Shaffer, INFORMS
* Rebecca Simon, University of California Press

### 2007 election

The following organizations were elected to the board for three-year terms:

* Beth Rosner, AAAS (Science)
* Karen Hunter, Elsevier
* Howard Ratner, Nature Publishing Group
* Thomas Connertz, Thieme Publishing Group
* Wim van der Stelt, Springer Science + Business Media
* Jerry Cowhig, IOP, who was appointed by the board to fill the Blackwell vacancy, was elected to serve out the term expiring in 2008.

### 2006 election

The following organizations were elected to the board for three-year terms:

* John R. White, Association for Computing Machinery
* Nawin Gupta, University of Chicago Press
* Carol Richman, Sage Publications
* Ian Bannerman, Informa UK Limited (Taylor & Francis)
* Eric A. Swanson, John Wiley & Sons, Inc.

### 2005 election

The following organizations were elected to the board for three-year terms:

* Marc Brodsky, American Institute of Physics
* Linda Beebe, American Psychological Association
* Robert Campbell, Blackwell Publishing
* Anthony Durniak, IEEE
* Rebecca Simon, University of California
* Press Paul Weislogel, Wolters Kluwer

### 2004 election

The following organizations were elected to the board for three-year terms:

* Beth Rosner, AAAS (Science)
* Karen Hunter, Elsevier Science
* Annette Thomas, Nature Publishing Group
* Thomas Connertz, Thieme Publishing Group
* Ruediger Gebauer, Springer Science + Business Media

---
Please contact [Lucy Ofiesh](mailto:vote@crossref.org) with any questions.