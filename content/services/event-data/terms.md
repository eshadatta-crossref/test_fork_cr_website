+++
title = "Event Data terms of use"
date = "2017-11-10"
draft = false
author = "Martyn Rittman"
doi = "https://doi.org/10.13003/CED-terms-of-use"
x-version = "0.0.0"

[menu.main]
rank = 1
parent = "Event Data"

+++

Version 1.0

## Who are we?

[Crossref](/) is a not-for-profit membership organization providing certain technology services to the scholarly community on a noncommercial basis.

## What is Crossref Event Data?
[Crossref Event Data](/services/event-data/) [the “Service”] is a Crossref service which provides a unique record of the relationship between web activity and specific scholarly content items (e.g. a journal article which has been referenced on a Wikipedia page). The Service is a hub for the collection, storage and distribution of data concerning these relationships. When a relationship is observed between a Crossref-registered content item and a specific web activity, the data is expressed in the Service as an “Event”.  An “Event” is the record of a relationship between an item of registered content and a specific activity. The Service provides Events from a variety of web sources. Each web source is referred to as a “Data Contributor”. A “Data Contributor” means the location where a relationship was observed. The Events, and all original data from the Data Contributor, are available in the Service via an API.


## What are the Terms of Use for Crossref Event Data?
To facilitate data access and maximal reuse, all Events provided in the Service are tagged with a license that is conformant with the principles laid out in the <a href="http://opendefinition.org/od" target="_blank">Open Definition</a>. This ensures that data made available in the Service can be made publicly available and reused, in accordance with the conditions of the specific license associated with each Event. To this end, the service will only include data from Data Contributors who can provide this to the service via a license conformant with the Open Definition.

Consumers of the Service must ensure they abide by the conditions set out in the license tagged to each Event. In addition, consumers must also ensure to comply with any conditions set out in both the Data Contributor’s Privacy Policy as well as the [Crossref Privacy Policy](/operations-and-sustainability/privacy).

The Crossref Event Data service will respect the restrictions provided in robots.txt to ensure the service does not follows links when directed not to by these files. In addition, like search engines, Crossref runs software which visits websites. Events from our Newsfeeds, Reddit Links and Web sources are derived in this way.

Data from all Data Contributors is made available in the Service via the <a href="https://creativecommons.org/publicdomain/zero/1.0/legalcode " target="_blank">Creative Commons CC-0 1.0</a> license waiver, with the exception of the following Data Contributors listed below:

| Data Contributor  | Terms of Use / License
|:--------------|:----------|
|Crossref Metadata |Made available without restriction |
|Cambia Lens|<a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">Creative Commons CC-BY-SA 4.0</a>|
|Stack Exchange Network   |<a href="https://creativecommons.org/licenses/by/4.0/legalcode " target="_blank">Creative Commons CC-BY 4.0</a>|


## Privacy
In addition to the license associated with each Event, consumers of the Service must also ensure to comply with any conditions set out in the both the [Crossref Privacy Policy](/operations-and-sustainability/privacy/) as well as the Data Contributor’s own Privacy Policy. It’s a Service requirement that you adhere to the conditions stated in these policies. Every Event we provide via an API will contain the link to the Crossref Terms of Use page, where all the information and links you need regarding use and reuse will be available.

Where there are specific privacy policies of which Crossref is aware, that are associated with a particular Data Contributor, they are listed below.

| Data Contributor      | Privacy Policy  |
|:--------------------- |:----------------|
|Cambia Lens |[Cambia Lens Privacy Policy](https://www.lens.org/about/policies/#privacypolicy)|
|Crossref Metadata |[Crossref Privacy Policy](/operations-and-sustainability/privacy)|
|DataCite Metadata|<a href="https://www.datacite.org/privacy.html " target="_blank">DataCite Privacy Policy</a>|
|Hypothes.is   |<a href="https://hypothes.is/privacy/   " target="_blank"> Hypothesis Privacy Policy</a>|
|Reddit|<a href="https://www.reddit.com/help/privacypolicy/  " target="_blank">Reddit Privacy Policy</a>|
|Stack Exchange Network|<a href="http://stackexchange.com/legal/privacy-policy   " target="_blank"> Stack Exchange Privacy Policy</a>|
|Twitter  |<a href="http://twitter.com/tos  " target="_blank">Twitter TOS</a>|
|Wikipedia|<a href="https://wikimediafoundation.org/wiki/Privacy_policy " target="_blank">Wikimedia Foundation Privacy Policy</a>|
|Wordpress.com    |<a href="https://automattic.com/privacy/   " target="_blank"> Automattic Privacy Policy</a>|


## Need to contact us?
Please email us at [eventdata@crossref.org](mailto:eventdata@crossref.org) with any questions or feedback.