+++
date = "2019-03-21"
title = "Apply to join Crossref as a sponsored member"
type = "join-sponsored"
x-version = "0.0.0"

+++

Apply to join Crossref as a sponsored member.