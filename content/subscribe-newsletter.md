+++
title = "Receive updates from us"
date = "2018-03-19"
draft = false
x-version = "0.0.0"

+++

If you are already a Primary or Technical contact on a Crossref member or Sponsor account, you will receive our bi-monthly newsletter automatically. Otherwise, sign up here.

Subscribing to email updates from us means you’ll receive our bi-monthly community newsletter which aims to keep you up-to-date with our latest developments. We may also send you the occasional blog post or other (relevant) communication.

You can unsubscribe from these communications at anytime using the ‘unsubscribe’ link located at the bottom of each newsletter.


<iframe src="https://outreach.crossref.org/acton/media/16781/receive-updates-from-us-landing-page" width="100%" height="650" frameborder="0" scrolling="no"></iframe>