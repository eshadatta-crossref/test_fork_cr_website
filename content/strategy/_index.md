+++
title = "Strategic agenda"
date = "2023-03-09"
draft = false
author = "Ginny Hendricks"
image = "/images/blog/crossref-strategic-direction-star-go-kart.jpg"
x-version = "0.0.0"

[menu.main]
parent = "About us"
weight = 15
rank = 5

+++

Welcome to our new strategic narrative---finalised January 2023---which sets out Crossref's priorities through 2025. It encompasses all our high-level activities, from open governance and sustainability, our work collaborating with different parts of the ever-diversifying scholarly community, through to expansion of metadata and relationships.

Like others, we envision a rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

Crossref makes research objects easy to find, cite, link, assess, and reuse.

We’re a not-for-profit membership organization that exists to make scholarly communications better.  We **rally** the community; **tag** and share metadata; **run** an open infrastructure; **play** with technology; and **make** tools and services---all to help
put scholarly content in context.

It’s as simple---and as complicated---as that.

{{% keyword "rally" %}}
{{% keyword "tag" %}}
{{% keyword "run" %}}
{{% keyword "play" %}}
{{% keyword "make" %}}

<br>

Read on to learn more about where Crossref is heading and let us have your thoughts by [starting or joining a discussion](https://community.crossref.org/c/strategy) in the strategy section of our community forum. You can dig into [our public roadmap](http://bit.ly/crossref-roadmap) too which shows the status and progress of all projects mentioned on this page, and more. Review the archived strategic narratives for [2020-2022](/strategy/archive-2021) and [2018-2020](/strategy/archive-2018), and read background on our strategy [on our blog](/categories/strategy/).

<br>

{{% strategy-section wrapper %}}

{{% strategy-section %}}

## The strategic landscape

Governments, funders, institutions, and researchers---groups who once had tangential involvement in scholarly publishing---are taking a more direct role in shaping how research is recorded, shared, contextualised, and assessed.

We now have more members that self-identify as universities or research institutions than as publishers and we have seen a rise in library- and scholar-led publishing. Many research funders are playing their part by supporting open infrastructure, registering their records with Crossref as members, and seeing this as a direct way of measuring reach and return on their grants and awards.

As more people contribute to an evolving scholarly record, so Crossref must capture provenance and relationships through metadata.

We have to scale our systems, tools, and resources to manage this and we have to do it all in the open, demonstrating our committment to POSI, to add a level of assurance that research is being properly supported, and so we can more easily integrate and co-create with others.

With a more complete picture of the scholarly record available in the open, everyone will be able to examine the integrity, impact, and outcomes of our collective efforts to progress science and society.

{{% strategy-table table landscape %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### [Contribute to an environment where the community identifies and co-creates solutions for broad benefit](#we-want-to-contribute-to-an-environment-in-which-the-scholarly-research-community-identifies-shared-problems-and-co-creates-solutions-for-broad-benefit)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [A sustainable source of complete, open, and global scholarly metadata and relationships](#we-want-to-be-a-sustainable-source-of-complete-open-and-global-scholarly-metadata-and-relationships)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Publicly accountable to the Principles of Open Scholarly Infrastructure (POSI) practices of sustainability, insurance, and governance](#we-want-to-be-held-publicly-accountable-to-the-principles-of-open-scholarly-infrastructure-posi-practices-of-governance-insurance-and-sustainability)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Foster a strong team---because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it](#we-want-to-foster-a-strong-team---because-reliable-infrastructure-needs-committed-people-who-contribute-to-and-realise-the-vision-and-thrive-doing-it)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="create" %}}

{{% divwrap service-blue %}}

## We want to contribute to an environment in which the scholarly research community identifies shared problems and co-creates solutions for broad benefit

> _We do this in all teams through research and engagement with our expanding community._

{{% /divwrap %}}

Some problems benefit from collective action. Scholarly communications changes rapidly and we need to be proactive with our community to understand how we can help solve shared problems. We continue to focus on collaboration with new and long standing partners, and our revived R&D team allows us to move nimbly, try new things, and involve the community as we do.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Released [new form](/documentation/register-maintain-records/grant-registration-form/) for funders to register grant records
* [New recommendations from the Preprints Working Group](https://community.crossref.org/t/share-your-thoughts-on-preprint-metadata/2800)
* [Brought operational management of ROR in-house](https://ror.org/blog/2022-10-10-strengthening-sustainability/), shared across DataCite, CDL, and Crossref

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Progress [Integrity of the Scholarly Record (ISR)](/categories/research-integrity/)
* [Develop Similarity Check](/blog/similarity-check-whats-new-with-ithenticate-v2/) as a solution to publisher integrity concerns
* [Extend support for grant metadata](https://docs.google.com/document/d/1CVB9UzsRVj5Hr_yDj7LEz3jlivSopeZTcn3AMJzGipo/edit?usp=sharing) and relationships, and [grow adoption](https://api.crossref.org/types/grant/works?rows=0&facet=funder-name:*)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

#### 2023

* Align Funder Registry with ROR
* Identify patterns in and match metadata

#### Pending

* [Community consultation](https://doi.org/10.31222/osf.io/6z7s3) regarding Crossmark

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* How can we convene and facilitate understanding between Funders and Publishers
* How do we best gather changes and notify changes to metadata?

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="nexus" %}}

{{% divwrap service-red %}}

## We want to be a sustainable source of complete, open, and global scholarly metadata and relationships

> _We are working towards this vision of a ‘Research Nexus’ by demonstrating the value of richer and connected open metadata, incentivising people to meet best practices, while making it easier to do so._

{{% /divwrap %}}

Building a more complete picture of the scholarly record means thinking about our metadata outside the more rigid structures once provided by content types. In line with community needs, we will build in more flexibility, clearer assertions of metadata provenance, and charge ourselves with improving the accuracy, transparency, and downstream usage of the metadata we collect and ingest from a range of sources. We will support our members in improving the provision of key metadata fields so that they can easily contribute to the growing network of metadata and relationships. 

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Metadata records with [ROR IDs added to our API](/blog/a-ror-some-update-to-our-api/)
* Launched [Global Equitable Membership (GEM) program](/blog/introducing-our-new-global-equitable-membership-gem-program/)
* [Opened references by default](/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/#reference-distribution-preferences)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Adoption activities to focus on top metadata adoption priorities which are: references; abstracts; grants; ROR
* Build new content system and move or deprecate as we go
* Develop 'item graph' to reflect nuanced relationships in the API and to ensure future schema flexibility
* Improve metadata delivery via the REST API
* Broad adoption of the [GEM Program](/gem) to include more of the world's metadata
* Develop clear metadata development strategy, priorities, and [roadmap](https://trello.com/b/JaB7xxgw/crossref-metadata)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

#### 2023

* Launch relationships API endpoint

#### Pending

* Extend new registration form to include articles and other content types and retire Metadata Manager tool
* (Co)create resources and proactive support for metadata users

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="posi" %}}

{{% divwrap service-yellow %}}

## We want to be held publicly accountable to the Principles of Open Scholarly Infrastructure (POSI) practices of governance, insurance, and sustainability

> _We do this by focusing on broad community governance, organisational resilience, open forkable code, and transparent operations._

{{% /divwrap %}}

We will invest time and resources in embedding open and transparent practices in our organisation so that you know what we know. We use POSI as a decision framework to help us evaluate conversations, prioritise collaborations, plus improve our own way of working. We’ll keep revisiting these areas, and uncovering more as we evolve our own strategy and continue to evaluate the principles themselves as we work with POSI adopters and potential partnerships, prioritising those organisations who also commit to POSI. 

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Improved 'broad stakeholder governance', 'open data', and improved efforts toward '12-month contingency' (See [self-assessments](/categories/posi/))

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* 2022/3 POSI self-assessment update
* Publishing openly all internal policies and documentation
* Engaging with other [POSI-adoptee infrastructures](https://openscholarlyinfrastructure.org/posse/) and refining POSI together
* Plan the move from data center to cloud
* Releasing the 2023 public data file


{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

#### 2023

* More transparent tools and information about billing

#### Pending

* Reach 12-month contingency goal (see: [Sustainability](https://openscholarlyinfrastructure.org/))
* Deprecate closed code and open-source more/all code (see: [Insurance](https://openscholarlyinfrastructure.org/))

{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* Is our governance structure representative and are the processes efficient? (see: [Governance](https://openscholarlyinfrastructure.org/))

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="team" %}}

{{% divwrap service-green %}}

## We want to foster a strong team---because reliable infrastructure needs committed people who contribute to and realise the vision, and thrive doing it

> _We do this through fair policies and working practices, a balanced approach to resourcing, and accountability to each other._

{{% /divwrap %}}

Our commitment to collaboration and transparency is reflected internally and externally. Collaboration is at the centre of everything we do – we find shared solutions that address the scholarly research community’s needs and benefit from a collective approach. This is reflected in how we operate as a team as well. By making our operations more transparent we can ensure that our approach is applied consistently and equitably; potential candidates can get a sense of how we operate; and other organisations can adapt and reuse policies if they wish. 

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

* Completed our 2022 hiring. [Meet our team](https://www.crossref.org/people/)
* Agreed and published our new [travel and events](https://www.crossref.org/blog/rethinking-staff-travel-meetings-and-events/) policy

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus

* Goal planning and aligning team priorities
* Developing a framework to better track our carbon emissions
* Planning an all-staff retreat in person
* Reviewing recruitment and compensation practices

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Up next

#### 2023

* Make our employment practices more transparent
* Undertake 2023 recruitment

#### Pending


{{% /strategy-table %}}

{{% strategy-table cell %}}

### Under consideration

* How does our resourcing support Crossref's growth?

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% /strategy-section %}}