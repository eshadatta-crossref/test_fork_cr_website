+++
title = "For librarians"
date = "2017-03-02"
draft = false
rank = 5
weight = 24
aliases = [ "/01company/07libraries.html", "/01company/free_services_agreement.html", "/03libraries/16lib_how_to.html", "/03libraries/index.html", "/03libraries/27forlibraries.html",]
x-version = "0.0.0"

+++

Enhance your metadata and connect your discovery and linking services with over 100 million records---they’re all available in XML and JSON through [open APIs](https://api.crossref.org) and [search](https://search.crossref.org).

Libraries and Crossref are a winning combination. Our shared goal is to improve discoverability of content for researchers. For our part we look after our members' metadata and run a registry of persistent links. We also offer services that help systems and people to make connections.

We currently look after over {{< total-results "/works?rows=0" >}} content items from theses, dissertations, preprints, and reports-- through to of course journal articles and books/book chapters.

## Retrieve metadata into your library discovery system using our OpenURL service

Many librarians come to Crossref for an account to use to retrieve metadata using OpenURL - you'll need a free Open URL account if you need credentials for your Alma service for example. Please read through the terms of service below.

### OpenURL terms of use for Libraries

* The Library may retrieve Crossref metadata and DOIs (Digital Object Identifiers) and their associated metadata, which may be used by The Library:
 * to make persistent links to full-text works online in library records, content and link resolvers and other Library systems
 * for scholarly, research, educational, personal or non-commercial purposes
* The Library will make a reasonable effort to use DOIs to link prominently to their intended target, without any ‘interrupting technologies,’ like pop ups.
* The Library may not use Crossref DOIs or metadata in ways that may infringe copyright.
* Crossref metadata does not grant or imply Library access to publisher full text
* Term of agreement is 12 (twelve) months but The Library or Crossref can terminate with 30 (thirty) days notice to the other. Otherwise, it automatically renews annually.
* If the agreement changes we will alert the designated Primary Contact (previously known as "Business Contact").
* Using Crossref trademarks and brand: The Library is welcome to associate the Crossref name and trademark with activities described in this agreement, for its duration.
* Library use is nonexclusive and revocable.
* Any other use, including by non-Library parties, requires prior written consent from Crossref.
* Limitations and Disclaimers: Sometimes, despite best efforts, things will go wrong:
* Crossref won’t be held liable for damages resulting from the use of Crossref systems or data.
* Metadata is provided ‘as-is.’
* Crossref disclaims any and all other warranties, conditions or representations (express, implied, oral or written), relating to the DOIs and metadata, the Crossref system or any parts thereof.
* Waiver: Delays or omissions by either Crossref or The Library will not be construed as a waiver.
* Entire Agreement: Finally, this is the entire agreement and supersedes any previous one(s).

If you agree to these terms, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=6312592628621) to set up an account. Please include your library name and address and other requested information, and confirm that you agree to the OpenURL terms of use. We will create your account and send you login details within a couple of days.

## The metadata we take in

We have over 16,000 members registering journal articles, books, dissertations, preprints, and other [content types](/education/content-registration#00535) with us.

Our members register content with us including as much metadata as possible, they then use each others' metadata to link their references and commit to maintaining these links for the long-term. There are some figures at our [dashboard](/dashboard), a live display of data using our REST API. Our members update that metadata all the time.

Increasingly we are being asked to take in other scholarly outputs such as peer review reports, videos, blogs, and other. Watch our [blog](/blog) for news of these additions or sign up to our newsletter via the link in the footer of this website.

## Other ways to get metadata

One of our responsibilities is to make metadata available to the systems and people who need it. The benefits to libraries in using our metadata are that you can ensure persistent links, which lead to increased discoverability of your resources. And it also means you don't have to sign individual linking agreements with each publisher or keep track of their different ways of linking.

We have a number of [metadata retrieval](/services/metadata-retrieval) options, in addition to OpenURL. The most visible is [search.crossref.org](https://search.crossref.org) (for humans!) which uses our [REST API](/education/retrieve-metadata/rest-api/) (for machines!). Both interfaces are open and free to use without registering.

From [search.crossref.org](https://search.crossref.org) you can search for a DOI, an article, an author, an ORCID iD, etc. You can search right from our [homepage](/) on the second tab 'search metadata' or go directly to search.crossref.org.

We have a browsable [title list](https://apps.crossref.org/titleList/) of all titles registered with us, by journal, book series, or conference proceeding.

## Library publishers

A growing number of libraries are becoming publishers themselves too. Many of the approximately 180 members that join every month are, for example, a scholar-publisher or a library-publisher. If you'd like to register content and contribute metadata please take a look at the [member terms](/membership/terms) and if you can meet them, [apply to join](/membership).

## Other services

You may also be interested in some of our [other services](/services), such as the [Funder Registry](/services/funder-registry) which provides a standard way to report funding sources for published scholarly research.

---
Contact our [outreach team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) to set up an account or ask any questions. [Technical support](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) is also available from our metadata experts.