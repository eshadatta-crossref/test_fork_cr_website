+++
title = "Ambassadors"
date = "2021-03-10"
draft = false
author = "Vanessa Fairhurst"
image = "/images/banner-images/ambassador-banner-fish.jpeg"
x-version = "0.0.0"

[menu.main]
parent = "Get involved"
rank = 3
weight = 26

+++

Crossref Ambassadors are our trusted contacts who work within our communities (as librarians, researchers, publishers, societies, technologists, innovators) around the world and who share a great enthusiasm and belief in our work. Alongside their varied professional roles, they volunteer to support the scholarly community in their locales with ongoing communication, interactive workshops and training from Crossref. They are Crossref’s eyes and ears in the community, and a special part of our team. [Meet our Ambassadors and find out more about them.](/community/our-ambassadors)

## What is it like to be an Ambassador?

By being one of our ambassadors you will become a key part of the Crossref community; our first port of call for updates or to test out new products or services, well connected to our wide network of members and you will work closely with us to make scholarly communications better for all.

The benefits to ambassadors are:

- The opportunity to expand your network and forge new relationships with the scholarly research community.
- Training opportunities in a range of skills and Crossref services.
- The inside scoop on all things Crossref and happenings in research communications.
- A sneak preview of new Crossref services and initiatives.
- Official endorsement and recognition of your role.

## How we support them

We want to make sure our ambassadors have the help they need to succeed in their roles. We’ll contact you with all the information to bring you on-board and we’ll schedule regular catch-ups to give any updates, answer any questions and generally check-in with you. We’ll also give you access to resource packs to assist your outreach activities (including slide decks and handouts), special edition ambassador digital badges and other goodies. We will also help support your attendance at relevant Crossref meetings so that you can see what we’re working on and what’s coming next!

## How they support our communities

We’re setting up the ambassador program as we’ve heard from members and users they would like...

- A local expert on-hand to provide support as and when required.
- Increased training events both online and in person, in your local region, timezone and language.
- Representatives from Crossref at events in your region.
- A conduit for other members and stakeholders in the region.
- A liaison to the Crossref teams in the US and Europe.

## Apply to become an Ambassador

If you are interested in working with us please fill out the form below to give us a little information about yourself. We’ll then get back to you to follow-up and discuss your plans and ideas. The program is very flexible so you can pick and choose what you’d like to be involved in based on your other commitments - we know that people are busy!   

If you're a brand new member and still getting to grips with everything Crossref, then this role may be a little too advanced for you. However, our [services](/services/) page is full of helpful information and you could look into attending one of our [webinars](/webinars/) or [LIVE local events](/events/) to find out more.

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdZDP3_uZJ5r6WVyBPjunYv2vedsMpr4Q5DiISs6aiOsgSrXw/viewform?usp=sf_link" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>  

If you are unable to access the form above, please [download this pdf](/pdfs/crossref-ambassador-application-form.pdf) and [send it back to us](mailto:feedback@crossref.org).

---

Please contact our [outreach team](mailto:feedback@crossref.org) with any questions.