+++
title = "DataCite"
date = "2019-07-05"
draft = false
author = "Amanda Bartell"
parent = "Get involved"
rank = 4
weight = 3
aliases = [ "become-a-member/why-choose-crossref/crossref-datacite", "become-a-member/why-choose-crossref/crossref-datacite/",]
x-version = "0.0.0"

+++

## The basics

‘One size fits all’ never quite works, does it? This is why there are different [DOI Registration Agencies](https://www.doi.org/RA_Coverage.html) to serve the needs of different interest groups. Crossref and DataCite constitute two of these Registration Agencies, but we overlap more than most in terms of our missions and our communities.  

We understand why it might be confusing trying to decide who to join, or whether to join both. We want to help, so that you can get the services that are the best fit for your organization and the type of content you want to register.

* Crossref makes research outputs easy to **find, cite, link, assess, and reuse**.
We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.
It’s as simple—and as complicated—as that.

* DataCite’s mission is to be the world's leading provider of persistent identifiers for research. Through our portfolio of services, we provide the means to **create, find, cite, connect, and use** research. We seek to create value and develop community-driven, innovative, open, integrated, useable, and sustainable services for research.

If you’ve been following the work we’ve been doing, you’ll know that we’ve been making [joint announcements](/news/2014-11-10-crossref-and-datacite-announce-new-initiative-to-accelerate-the-adoption-of-dois-for-data-publication-and-citation/) for some time. We also collaborate on numerous initiatives that aim to provide foundational infrastructure for research outputs.

## Who to join

Here are some things to think about, to help you decide which organization is the right one for you.

### What type of organization are you?
Thinking about how you classify your organization can be helpful; what type of organization/department/initiative/project are you? Do you see your organization as one that sits within a certain community?

Crossref members are organizations who publish content, or register research grants. These include publishers, research institutions, university presses, societies and funders. In order to become a member, register content and deposit metadata and DOIs, you’ll need to meet the criteria set out in our governing [by-laws](/board-and-governance/bylaws/). Membership in Crossref is open to organizations that produce professional and scholarly materials and content. In addition, applicants should be able to meet the [terms and conditions](/membership/terms/) of membership.

With DataCite, membership is open to all organisations that share their mission. DataCite’s members work with data centers, stewards, libraries, archives, universities, publishers and research institutes that host repositories and who have responsibility for managing, holding, curating, and archiving data and other research outputs. Members agree with the DataCite [statutes](https://datacite.org/documents/statutes.html). You can see current DataCite members [here](https://datacite.org/members.html).

### Specific services
It’s [not just about ‘getting a DOI’](/education/why-crossref#00152). Crossref and DataCite have expertise and services that support and enhance the specific needs of our communities and how they work with their content.

Crossref provides [services](/services/) like:   
* [Reference Linking](/services/reference-linking/): Reference linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things.  
* [Cited-by](/services/cited-by/): Cited-by shows how work has been received by the wider community; displaying the number of times it has been cited, and linking to the citing content.  
* [Similarity Check](/services/similarity-check/): A service provided by Crossref and powered by iThenticate—Similarity Check provides editors with a user-friendly tool to help detect plagiarism.

DataCite provides services like:
* [DOI Fabrica](https://doi.datacite.org/): DOI management platform, perfect for manual DOI creation or for human curation of automatically-created DOIs. Register your first DOI in less than a minute.  
* [Link checker](https://support.datacite.org/docs/link-checker): Automatically checks your DOIs to make sure they are still resolving correctly.   
* [Data metrics badge](https://support.datacite.org/docs/displaying-usage-and-citations-in-your-repository): Embed citation metrics for any DataCite DOI on your own website.

Importantly, both organizations make the metadata you register available via APIs. This metadata is used by thousands of different tools and services. So if you’re registering content with an organization that isn’t the best fit for your content, then you might wonder why it isn’t appearing in specific databases e.g. Google Dataset Search for data, Dimensions for articles. This is why we have specific metadata schemas for different content types, to fit the communities we work with, so that an organization can work with us to get access to all of the data they’re interested in, in a standard format and in one place.

### Your role  
What role do you have in relation to the content being registered?
* Do you follow a publishing workflow? I.e. are there editorial processes involved in selecting and stewarding the content e.g. issuing updates like corrections. We know there are lots of definitions of a publisher so it’s hard to be exact, but this might help you think about how you work. If this sounds like you then you should explore joining Crossref.  
* Or are you depositing content? This could be different types of research outputs using different kinds of platforms, e.g. researchers posting content to an institutional repository. In that case, the DataCite community is a good fit for you.  

### Joining both  
We’re also working with an increasing number of organizations who have content types that are best served by being members of both organizations, like a publisher who publishes a mix of journal articles and data, or a university that has a publishing program and an institutional repository.

The Center for Open Science provides a good example of a member who works with both of our organizations to meet the needs of their community:

> <span> “We hear from lots of users about how important Digital Object Identifiers (DOIs) are to their work. DOIs ensure persistent links to content and enhance discoverability of one’s research. At the behest of our users, we began issuing DOIs in 2015, first to public registrations, then to public projects in September 2016, and recently to preprints in July 2017. In all, over 22,000 DOIs were registered for content on the OSF.
The DOIs issued on the OSF have historically been registered with DataCite, through the California Digital Library’s EZID. Earlier this year, we learned that [EZID’s services are evolving](https://blog.datacite.org/ezid-doi-service-is-evolving/), and COS was faced with the choice of a new registration agency for DOIs.
This has given us the opportunity to explore how best to support our users and the diverse research outputs they share via OSF. Ultimately, COS decided to pursue registering DOIs with two separate agencies to provide users with services tailored to their needs: registering DOIs for preprints with Crossref and DOIs for projects and registrations with DataCite.”</span>  

### Avoid assigning multiple DOIs to one research object
For identifiers and metadata to work to their full potential, both Crossref and DataCite require that only one identifier be assigned to a research object. Multiple persistent and identifiers for an object reduce the chance of it being identified and discovered, and can split usage, citation information over multiple instances of a work.

If a persistent identifer for an object already exists, you should continue to use that identifier for the content and not reassign another identifier for that object under your own prefix.

### Still not sure?
We’ve created a diagram to try to help you try to decide who to join, based on the workflow the content you work with follows:

<img src="/images/community-images/datacite_flowchart.png" alt="Crossref DataCite flowchart" width="60%">  

---
We’re also happy to discuss specific needs directly. Contact the [Crossref membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) or [DataCite support](mailto:support@datacite.org) with information on what you’re trying to achieve and we’ll help you!