+++
title = "Distributed usage logging collaboration"
date = "2020-11-24"
draft = false
author = "Martyn Rittman"
weight = 100
x-version = "0.0.0"

+++

## Current status

In November 2020, Crossref's board decided to scale down our involvement in the distributed usage logging (DUL) initiative and sought a new lead organisation. In our view, others were better-placed to progress the work and increase participation, building on the proof-of-concept created.

Since December 2021, [STM Solutions](https://www.stm-assoc.org/stm-solutions/dul/) has maintained the public key registry. Crossref will continue to support DUL endpoints included in our members' metadata.

## Background

Researchers are increasingly using “alternative” (non-publisher) platforms to store, access and share literature, e.g. via:

* Institutional and subject repositories
* Aggregator platforms (EBSCOhost, IngentaConnect)
* Researcher-oriented networking sites (e.g. Academia.edu, ResearchGate, Mendeley)
* Reading environments and tools (e.g. ReadCube, Utopia Docs)

Use of content on these platforms is by researchers who also have access to the same content via institutional subscription agreements. However, publishers are unable to report this use in their COUNTER-compliant reports, because it does not occur on their own platforms.  This means:

* Publishers are unable to demonstrate the full value of their content to library customers. They are also unable to provide authors will a full picture of usage of their articles
* Because use is distributed, institutions do not have a complete picture of usage

The distributed usage logging collaboration was established as an R&D experimental initiative between [COUNTER](https://www.projectcounter.org), Crossref members, and scholarly technology & service providers. The initiative, driven by the [DUL working group](/working-groups/distributed-usage-logging/), has explored and implemented private peer-to-peer channels for the secure exchange and processing of COUNTER-compliant private usage records from hosting platforms to publishers. All data provided back to the original publisher is anonymized, preserving individual user privacy.

## Resources

* [COUNTER’s Distributed Usage Logging stakeholder demand report](https://web.archive.org/web/20200803145432/https://www.projectcounter.org/distributed-usage-logging-report-stakeholder-demand/)
* [COUNTER Code of Practice v5](https://www.projectcounter.org/counter-code-practice-release-5-update/) compliance requirements for processing and reporting data from non-publisher usage sources
* [DUL proof of concept reference implementation](https://github.com/CrossRef/dul-tool) of the end-to-end transaction pipeline with validation credentials

---

For general questions about this initiative, please contact [Martyn Rittman](mailto:dul@crossref.org).