+++
title = "Meet our ambassadors"
date = "2021-03-10"
draft = false
author = "Vanessa Fairhurst"
x-version = "0.0.0"

[menu.main]
parent = "Ambassadors"
rank = 4
weight = 10

+++

<a id="top"></a>  

The Crossref Ambassador Program is an exciting and important program initiated in early 2018, and one which fully embraces a key strategic focus---to adapt to expanding constituencies.

Our Ambassadors are enthusiastic volunteers who work within the global academic community in a variety of ways---as librarians, researchers, publishers, and societies,---and all of whom share a strong belief in the mission-driven work we do to improve scholarly research communication. They support us by using their industry expertise, local knowledge, and translation skills to represent Crossref at regional events---providing training to our members in different languages, locations and time zones.

Select from the drop down list below to find out who is based in your region:  

{{% accordion %}} {{% accordion-section "Africa" %}}

### Cameroon - Audrey Kenni Nganmeni
{{% imagewrap left %}} <img src="/images/ambassadors/Audrey-Kenni.png" alt="Audrey Kenni" width="200px" class="img-responsive" /> {{% /imagewrap %}}   


I am Audrey Kenni Nganmeni, editor at the Pan African Medical Journal in charge of legal affairs and focal person of the journal with Crossref, a journal based in Cameroon, Africa. I am very glad to be part of Crossref ambassadors where I will learn more about Crossref services, benefit of diverses training and help Crossref with my French skills.

Je suis Audrey Kenni Nganmeni, éditrice au Pan African Medical Journal en charge des affaires juridiques et point focal de la revue avec Crossref. Je suis basée au Cameroun en Afrique. Je suis très heureuse de faire partie des ambassadeurs Crossref où je pourrai en savoir plus sur les services Crossref, bénéficier de diverses formations et aider Crossref avec mes compétences en français.    
<br><br>

### Kenya - Oscar Donde
{{% imagewrap left %}} <img src="/images/ambassadors/Oscar-Donde.jpg" alt="Oscar Donde" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Dr. Oscar O. Donde is a researcher, academician, manager, and publisher, who has extensive interaction with many people and organizations within the field of research, academia and publications. Oscar is currently affiliated with Egerton University as a lecturer (Assistant professor) and is Editor in Chief of the Pan Africa Science Journal. Oscar is delighted to join this dedicated team of specialists under the umbrella of the Crossref family, to synergistically make a contribution that will continue to strengthen Crossref’s services across the globe. As an ambassador, Oscar’s service to Crossref will be anchored in the slogan “bring Crossref home” for a better scientific community.

Dr Oscar O. Donde ni mtafiti, msomi, meneja, na mchapishaji, ambaye ana mwingiliano mkubwa na watu wengi na mashirika ndani ya uwanja wa utafiti, wasomi na machapisho. Oscar kwa sasa anahusishwa na Chuo Kikuu cha Egerton kama mhadhiri (Profesa Msaidizi) na ni Mhariri Mkuu wa Jarida la Sayansi la Pan Africa. Oscar anafurahi kujiunga na timu hii ya kujitolea ya wataalamu chini ya mwavuli wa familia ya Crossref, kutoa mchango ambao utaendelea kuimarisha huduma za Crossref kote ulimwenguni. Kama balozi, huduma ya Oscar kwa Crossref itatia nanga katika kauli mbiu "kuleta Crossref nyumbani" kwa jamii bora ya kisayansi.  


### Nigeria - Blessing Abumere
{{% imagewrap left %}} <img src="/images/ambassadors/blessing-abumere.jpg" alt="Blessing Abumere" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Blessing Oghenero Abumere is the Executive Director of Journal Issues, a growing global open access publisher based in Nigeria. His passion for information dissemination and sharing led him into publishing. He has been in academic publishing since 2009. Blessing ensures that his team work with authors all over the world to provide intellectually viable content, with an emphasis on developing countries. He has developed end-to-end publishing processes for journals.

Blessing is delighted with Crossref’s persistent and tireless efforts in developing novel tools to make content easy to find, cite, link, and access. He is constantly exploring new technologies, methodologies and practices to improve the quality and dissemination of scholarly content. He enjoys reading and listening to music in his spare time.   

### Tanzania - Baraka Manjale Ngussa
{{% imagewrap left %}} <img src="/images/ambassadors/Baraka-Ngussa.jpg" alt="Baraka Ngussa" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Baraka Manjale NGUSSA holds a PhD in Education (Curriculum and Teaching) from the University of Eastern Africa Baraton, Kenya. He is an experienced educator, researcher, and administrator in higher learning institutions particularly at the University of Arusha in Tanzania where he currently serves as the Director of Human Resources and Administration. He has been the President for Tanzania Adventist Authors and Writers Association (TAAWA) since 2019. He has a wide experience in teaching, publication and supervision and has authored over 60 publications including books, journal articles, encyclopedia sections, and book chapters. Baraka is the founder, CEO and Chief Editor of the East African Journal of Education and Social Sciences (EAJESS) which is indexed by African Journals Online (AJOL). His research areas include curriculum and teaching, educational management, and leadership. As Crossref Ambassador, Baraka’s passion is to provide his expertise in supporting academic journals in Africa to acquire and maintain high quality standards.

Baraka Manjale NGUSSA ana Shahada ya Uzamivu katika Elimu (Mitaala na Ufundishaji) ya Chuo Kikuu cha Afrika Mashariki, Baraton kilichoko Kenya. Baraka ana uzoefu mwingi katika ufundishaji, utafiti na uongozi katika Elimu ya Juu hasa Katika Chuo Kikuu cha Arusha ambako kwa sasa ni Mkurugenzi wa Rasilimali watu na Utawala. Amekuwa Rais wa Chama cha Watunzi wa Waandhishi wa Kiadventista nchini Tanzania tangu mwaka 2019. Ana uzoefu mpana katika kufundisha, uandhishi na usimamizi wa tafiti, na ana machapisho zaidi ya 60 ikiwa ni pamoja na vitabu, sura za vitabu pamoja na makala mbalimbali. Baraka ni muasisi na mhariri mkuu wa jarida la Afrika Mashariki la Elimu na Sayansi Jamii ambalo limewekwa katika African Journals Online (AJOL). Maeneo yake ya utafiti ni katika mitaala na ufundishaji, utawala wa elimu pamoja na uongozi. Akiwa Balozi wa Crossref, lengo lake ni kutoa uzoefu wa kitaalamu ili kuwezesha majarida yaliyoko Afrika Kushiriki katika mtandao wa Crossref wa kiulimwengu wa majarida  ya kitaaluma.   


{{% /accordion %}} {{% /accordion-section %}}

{{% accordion %}} {{% accordion-section "Americas" %}}

### Argentina - Evangelos Vlachos
{{% imagewrap left %}} <img src="/images/ambassadors/Evan_Vlachos.jpg" alt="Evan Vlachos"  height="200px" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Evangelos (Evan) Vlachos is a Greek/Argentine paleontologist working as a CONICET researcher on the Museo Paleontológico Egidio Feruglio, Trelew, Chubut, Patagonia, Argentina. Born and raised in Greece, Evan completed his studies with a PhD in Vertebrate Paleontology in the School of Geology, Aristotle University of Thessaloniki, Greece, specializing in fossil turtles and tortoises. Since 2015, he lives permanently in Argentina, focusing on the past diversity of turtles and the extinction events that shaped it. He is an active contributor to the PaleoBiology Database, and actively involved in the production process of the peer-reviewed journals of the Asociación Paleontológica Argentina, including management of the Open Journal Systems and Crossref metadata deposition.    

Evangelos (Evan) Vlachos es un paleontólogo griego/argentino que trabaja como investigador del CONICET en el Museo Paleontológico Egidio Feruglio, Trelew, Chubut, Patagonia, Argentina. Nacido y criado en Grecia, Evan completó sus estudios con un doctorado en Paleontología de Vertebrados en la Facultad de Geología de la Universidad Aristóteles de Tesalónica, Grecia, especializándose en tortugas fósiles. Desde 2015, vive permanentemente en Argentina, centrándose en la diversidad pasada de tortugas y los eventos de extinción que le dieron forma. Es un colaborador activo de PaleoBiology Database y participa activamente en el proceso de producción de las revistas revisadas por pares de la Asociación Paleontológica Argentina, incluida la gestión de Open Journal Systems y el depósito de metadatos Crossref.  

Ο Ευάγγελος (Βαγγέλης) Βλάχος είναι ένας Έλληνας/Αργεντίνος παλαιοντολόγος που εργάζεται ως ερευνητής στο CONICET στο Museo Paleontológico Egidio Feruglio, Trelew, Chubut, Παταγονία, Αργεντινή. Γεννημένος και μεγαλωμένος στην Ελλάδα, ο Βαγγέλης ολοκλήρωσε τις σπουδές του με διδακτορικό στην Παλαιοντολογία Σπονδυλωτών στο Τμήμα Γεωλογίας του Αριστοτελείου Πανεπιστημίου Θεσσαλονίκης, με ειδίκευση στις απολιθωμένες χελώνες. Από το 2015, ζει μόνιμα στην Αργεντινή, εστιάζοντας στην ποικοιλότητα των χελωνών κατά το παερλθόν και στα γεγονότα εξαφάνισης που τη διαμόρφωσαν. Συνεισφέρει ενεργά στη βάση δεδομένων PaleoBiology Database και συμμετέχει ενεργά στη διαδικασία δημοσίευσης των περιοδικών της Asociación Paleontológica Argentina, συμπεριλαμβανομένης της διαχείρισης του Open Journal Systems και της κατάθεσης μεταδεδομένων στο Crossref.   

### Brazil - Edilson Damasio
{{% imagewrap left %}} <img src="/images/ambassadors/edilson-demasio-sq.png" alt="Dr. Edilson Damasio"  height="200px" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Edilson Damasio has been a librarian since 1995, holding a PhD in Information Science from the Federal University of Rio de Janeiro-UFRJ/IBICT. He currently works in the Department of Mathematics Library of State University of Maringá-UEM, Brazil. With 20 years’ experience in scientific metadata and publishing, is expertise is wide-ranging including knowledge of scientific communication, Crossref services, research integrity, misconduct prevention in science, publishing in Latin America, biomedical information, OJS-Open Journal Systems, Open Access journals, scientific journal quality and indexing, and scientific bibliographical databases. He is enthusiastic about presenting and disseminating information about Crossref services to his community in Brazil and working within the wider community, exchanging ideas and experience. You can contact Edilson via Twitter @edilsondamasio or on LinkedIn.

Eu sou bibliotecário desde 1995, Doutor em Ciência da Informação pela Universidade Federal do Rio de Janeiro-UFRJ/convênio IBICT. Eu trabalho na Biblioteca do Departamento de Matemática da Universidade Estadual de Maringá-UEM. Com 20 anos de experiência em metadados científicos e editoração, entre outros. Meus conhecimentos são diversos sobre comunicação científica, cientometria, metadados XML, serviços Crossref, integridade em pesquisa, prevenção de más condutas na ciência, editoração, editoração na América Latina, informação biomédica, OJS-Open Journal Systems, revistas de Acesso Aberto, qualidade de periódicos científicos e indexação, bases de dados bibliográficas. Gosto de disseminar meu conhecimento a outras regiões e pessoas e de trabalhar em comunidade junto as instituições e outros países, de planejar novas apresentações, de trocar experiências como palestrante ou convidado e trabalhar na disseminação do conhecimento para todos.

### Brazil - Bruna Erlandsson
{{% imagewrap left %}} <img src="/images/ambassadors/Bruna-Erlandsson2.jpg" alt="Bruna Erlandsson" width="200px" height="200px" class="img-responsive" /> {{% /imagewrap %}}

Bruna graduated in 2012 in Editorial Production and since then has been actively involved in scientific publication. As co-owner of the company Linceu Editorial, Bruna has supported Brazilian journals in improving their quality by teaching them the best way of presenting rich metadata. She acted as a facilitator at ABEC-IBICT-Crossref partnership, supporting hundreds of journals to get their own DOI prefix. She has been elected to join the council of The Brazilian Association of Scientific Editors, as well as the editorial board of publication ‘Science Editors’ from The Council of Science Editors (CSE) and is currently in the final stage of the CSE Publication Certificate Program. She expects, as an Ambassador, to improve dissemination of knowledge and teaching on how to explore and use all the tools offered by Crossref.

Bruna graduou-se em 2012 em Produção Editorial e, desde então, tem se envolvido em publicação científica de forma bastante atuante. Sócia-proprietária da empresa Linceu Editorial, tem apoiado periódicos na melhoria da qualidade por meio da adoção de padrões internacionais para apresentação de metadados. Atuou como facilitadora na parceria ABEC-IBICT-Crossref, a qual possibilitou a atribuição de DOI a centenas de periódicos no Brasil. Faz parte do Comitê Editorial da revista Science Editors publicada pelo Council of Science Editors (CSE) e é membro do conselho deliberativo da Associação Brasileira de Editores Científicos (ABEC). Participa também do CSE Publication Certificate Program, encontrando-se na fase final da certificação. Espera, como embaixadora, disseminar o conhecimento e ensinar como explorar e usar todas as ferramentas oferecidas pela Crossref.

### Colombia - Nicolás Mejía Torres
{{% imagewrap left %}} <img src="/images/ambassadors/Nicolas-Mejia Torres.jpg" alt="Nicolás Mejía Torres" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Nicolás Mejía Torres is a professional social communicator, who specializes in editorial production and bibliometrics. Since 2020 he has been an associate editor for Palabra Clave, a social science communications journal. He has been working with the Universidad de La Sabana as Scientific Journal Coordinator, a role that has given him the chance to work with Open Journal Systems (OJS). He has also worked with different high-impact databases for scientific journals, such as Scopus, SciVal, and Web of Science, and has been using Crossref as a tool to improve the value of the metadata of all publications of his institution. He likes to discover new technologies and tools, useful for both his life and work. He is an amateur enthusiast for bibliometric data and wants to use it to explore new paths for the journals he manages and the disciplines they impact.   

Nicolás Mejía Torres es profesional en comunicación social. Se ha especializado en producción editorial y bibliometría. Desde 2020 es editor asociado de Palabra Clave, una revista de comunicación en ciencias sociales. Trabaja con la Universidad de La Sabana como el coordinador de revistas científicas. En ese rol ha podido trabajar con Open Journal Systems (OJS); también, trabaja con bases de datos de alto impacto para revistas científicas, como Scopus, SciVal y Web of Science. Se han involucrado con Crossref como una herramienta que mejora el valor de los metadatos de las publicaciones en su institución. Le gusta descubrir nuevas tecnologías y herramientas que le sirvan en su vida y trabajo. Es un entusiasta por la bibliometría y le gusta explorar con datos nuevos caminos y comportamientos de revistas científicas de su institución y las disciplinas que impactan con lo que publican.   


### Colombia - Arley Soto
{{% imagewrap left %}} <img src="/images/ambassadors/arley-soto.jpg" alt="Arley Soto" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Arley Soto is a professional in Information systems, Librarianship and Archives. He is also the co-founder of BITECA Ltda, a company that provides services for libraries and publishers since 2006, and his current role as innovation manager is to explore new products, services and tools that contribute to enhance the scholarly communication processes in Latin America. He is constantly exploring new technologies, methodologies and practices that improve the quality and dissemination of scholarly content, like new functionality for OJS, OCS, OMP, XML, DOI, ORCID, Crossmark and other editorial activities. He has also completed The European Master in Digital Libraries from the University of Oslo and Akershus, Tallin University and University of Pharma in 2016, with a thesis about the digital preservation of Colombian academic journals. Currently he is exploring the field of Web Archiving as well as Mobile Human-Computer Interaction in Scholarly Contexts.

Arley Soto es profesional en Sistemas de información, bibliotecología y archivística. En 2016, completó el Máster Europeo en Bibliotecas Digitales de la Universidad de Oslo y Akershus, la Universidad de Tallin y la Universidad de Pharma, con una investigación sobre la preservación digital para revistas académicas colombianas. Es cofundador de BITECA Ltda., una compañía que brinda servicios a bibliotecas y editoriales desde el año 2006. Su rol actual como gerente de innovación es explorar y gestionar nuevos productos, servicios y herramientas que contribuyan a mejorar los procesos de comunicación académica en América Latina. Estudia y examina nuevas tecnologías, metodologías y herramientas que permitan mejorar la calidad y difusión del contenido académico, tales como las funcionalidades para OJS, OCS, OMP, XML, DOI, ORCID, Crossmark y otras actividades editoriales. Adicionalmente, se encuentra interesado en analizar temas relacionados con la preservación de sitios web y la interacción con dispositivos móviles en contextos académicos.

### Colombia - Juan Felipe Vargas Martínez

{{% imagewrap left %}} <img src="/images/ambassadors/Juan-Felipe.png" alt="Juan Felipe Vargas Martínez" width="200px" class="img-responsive" /> {{% /imagewrap %}}


Juan Felipe Vargas Martínez, Systems Engineer and Senior Management Specialist, has worked for more than 10 years in contributing to the editorial work of scientific publications. Previously, he was component coordinator of ‘Sistema Nacional de Acceso Abierto al Conocimiento’ (National System of Open Access to Knowledge) in Colombia and has served as assistant and editorial advisor for various scientific publications in Colombia, Ecuador and Spain. Currently, Juan Felipe works as Co-Founder and Director of Journals & Authors, a company that, has supports scientific publications to improve their editorial quality and scientific dissemination through the optimization of editorial processes. Journals & Authors holds a regional meeting of academic journal editors, through which it provides training in editorial work and generates an integrated space within academia to address and discuss the new challenges of scientific publishing. Juan Felipe also acts as coordinator of journal management processes in Open Journals System, metadata deposit in Crossref, Crossmark, and databases.

Juan Felipe Vargas Martínez, Ingeniero de Sistemas, Especialista en Alta Gerencia, ha trabajado por más de 10 años contribuyendo a la labor editorial de las publicaciones científicas. Fue coordinador de componente del Sistema Nacional de Acceso Abierto al Conocimiento (Colombia) y se ha desempeñado como asistente y asesor editorial para diversas publicaciones científicas en Colombia, Ecuador y España. Es cofundador y actualmente director de Journals & Authors, empresa que por más de 5 años viene apoyando las publicaciones científicas en el mejoramiento de la calidad editorial y la difusión científica a través de la creación de metodologías que permitan la optimización de los procesos editoriales. Journals & Authors realiza un encuentro regional de editores de revistas académicas, a través del cual busca capacitar en la labor editorial y generar espacios de integración entre la academia para abordar y discutir los nuevos retos de la edición científica. Coordinador de procesos de gestión de revistas en Open Journals System, depósito de metadatos en Crossref, Crossmark y bases de datos.   

### Mexico - Amanda Falcone
{{% imagewrap left %}} <img src="/images/ambassadors/Amanda-Falcone.png" alt="Amanda Falcone" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Amanda Falcone is an editor and translator. She holds a B.A. in English Studies, particularly interested in literature and postcolonial theory. She also holds a degree in Reading Promotion—keen to know the reading habits of university students, she carried out a research stay on digital reading at the University of Salamanca, Spain. Since 2014 she has worked for the Publishing House of the University of Veracruz in the acquisition of foreign rights and recently, she has focused on the display of scientific works in open access. She speaks Spanish, English, and Italian.

Amanda Falcone es editora y traductora. Estudió la licenciatura en Lengua Inglesa con interés particular por la literatura y la teoría poscolonial. También tiene una especialización en Promoción de la Lectura, motivada por conocer los hábitos lectores de los estudiantes universitarios, lo que la llevó a realizar una estancia de investigación sobre lectura digital en la Universidad de Salamanca, España. Desde 2014 ha trabajado para la Editorial de la Universidad Veracruzana en la gestión de derechos de autor y recientemente se ha enfocado en la puesta a disposición de las obras científicas en acceso abierto. Habla español, inglés e italiano.   

### Mexico - Guillermo Chávez
{{% imagewrap left %}} <img src="/images/ambassadors/Guillermo-Chavez2.jpg" alt="Guillermo Chavez" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Guillermo Chávez is Deputy Director of Academic Journals and Digital Publications at UNAM, where he manages institutional strategies to strengthen and consolidate academic and scientific publications. He has developed several projects on books and online journals; digitalization of collections; repositories and digital libraries; information systems and document management, among others. For more than 15 years, he has promoted open access publishing; the use and exploitation of open source platforms such as the OJS, OMP and DSPACE; the implementation of interoperable standards such as OAI-PMH, DUBLIN CORE and DOI; and the adoption of good practices and international standards. He also serves as Technical Coordinator of the LATINDEX System and a professor at the School of Librarianship of the Faculty of Philosophy and Letters of the UNAM.

Guillermo Chávez es subdirector de Revistas Académicas y Publicaciones Digitales en la UNAM, donde gestiona estrategias institucionales para fortalecer y consolidar las publicaciones académicas y científicas. Ha desarrollado diversos proyectos de libros y revistas en línea; digitalización de acervos; repositorios y bibliotecas digitales; sistemas de información y gestión documental, entre otros. Durante más de 15 años, ha promovido la publicación de acceso abierto; el uso y aprovechamiento de plataformas de código abierto como OJS, OMP y DSPACE; la implementación de estándares interoperables como OAI-PMH, DUBLIN CORE y DOI; y la adopción de buenas prácticas y estándares internacionales. También se desempeña como Coordinador Técnico del Sistema LATINDEX y profesor en el Colegio de Bibliotecología de la Facultad de Filosofía y Letras de la UNAM.   

### Mexico - Maria Ramos-Escamilla
{{% imagewrap left %}} <img src="/images/ambassadors/Maria-Ramos.jpg" alt="Maria Ramos" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Maria Ramos-Escamilla holds a PhD in Economics from the Instituto Politécnico Nacional with a research stay at the Universidad de Santiago de Compostela. She has provided training to more than 5000 postgraduates and at a variety of science and technology centers around the world. Maria has authored more than 200 titles in international economics and fractal modelling, including books, software, and conferences. She has also participated in several international research groups focusing on fractal modelling in economics. Maria has received national and international awards and distinctions in the field of economics and finance. She is editor of more than 100 indexed and refereed journals in America, Europe, Asia and Africa with 25 years of experience. She is currently General Director of ECORFAN-MEXICO, S.C. with RENIECYT registration from CONACYT. Her mission at Crossref is to support researchers to conduct written science within a global network.   

Maria Ramos-Escamilla es doctora en Economía por el Instituto Politécnico Nacional con una estancia de investigación en la Universidad de Santiago de Compostela. Ha capacitado más de 5000 posgraduados y centros de ciencia y tecnología en diferentes países, es autora de más de 200 títulos en economía internacional y modelización fractal, incluyendo libros, software y conferencias. También ha formado parte de varios grupos de investigación internacionales centrados en la modelización fractal en economía. María ha sido galardonada con  premios  y distinciones nacionales e internacionales en el campo de la economía y las finanzas. Es editora de más de 100 revistas indexadas y arbitradas en América, Europa, Asía y África con 25años de experiencia. Actualmente es directora general de ECORFAN-MEXICO, S.C. con registro RENIECYT de CONACYT. Su misión en Crossref es apoyar a los investigadores a realizar Ciencia escrita dentro de una red global.   

### USA - Lauren Lissaris
{{% imagewrap left %}} <img src="/images/ambassadors/lauren-lissaris.jpg" alt="Lauren Lissaris" width="200px" class="img-responsive" /> {{% /imagewrap %}}


Lauren Lissaris  has dedicated much of her career to the dissemination of valuable content on a robust platform. She takes pride in her achievements as the Digital Content Manager at JSTOR. JSTOR provides access to more than 10 million academic journal articles, books, and primary sources in 75 disciplines. JSTOR is part of ITHAKA, a not-for-profit organization helping the academic community use digital technologies to preserve the scholarly record and to advance research and teaching in sustainable ways.

Lauren successfully works with all aspects of journal content to effectively assist publishers with their digital content. This includes everything from XML markup, content registration/multiple resolution, and HTML website updates. Lauren has been involved in hosting current content on JSTOR since the program's launch in 2010. She continues to collaborate with organizations to successfully contribute to the evolution of digital content. The natural spread from journals to books has set Lauren up for developing and planning the book Content Registration program for JSTOR. She is a member of the Crossref Books Advisory Group and she helped successfully pilot Crossref's new Co-access book deposit feature.

{{% /accordion %}} {{% /accordion-section %}}

{{% accordion %}} {{% accordion-section "Asia" %}}

### Bangladesh - Md Jahangir Alam
{{% imagewrap left %}} <img src="/images/ambassadors/Md Jahangir Alam.png" alt="Md Jahangir Alam" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Dr. Md Jahangir Alam is an academician and researcher currently affiliated with the Department of Japanese Studies, Faculty of Social Sciences, University of Dhaka, Bangladesh. He completed his Ph.D from the Graduate School of International Cooperation Studies, Kobe University, Japan. Dr. Alam’s experiences embrace collaborating with international organizations, especially Japan International Cooperation Agency (JICA), International Labor Organization (ILO), International Organization for Migration (IOM), United Nations Development Programme (UNDP), Hiroshima Peacebuilders Center (HPC), Citizenship & Immigration Canada (CIC) and the Japan Foundation. In 2019, Dr. Alam received several international awards for his outstanding research contributions to international education development. Dr. Alam is enthusiastic about disseminating knowledge and transmitting familiarity with Crossref services to his community in Bangladesh. He supports journals and publishers in joining Crossref and using the tools and services offered by Crossref. Dr. Alam speaks Bengali, English, and Japanese.    

ড. মো. জাহাঙ্গীর আলম একজন শিক্ষাবিদ ও গবেষক। বর্তমানে তিনি ঢাকা বিশ্ববিদ্যালয়ের সামাজিক বিজ্ঞান অনুষদের জাপানিজ স্টাডিজ বিভাগে শিক্ষকতা করছেন। তিনি জাপানের কোবে বিশ্ববিদ্যালয়ের গ্র্যাজুয়েট স্কুল অব ইন্টারন্যাশনাল কো-অপারেশন স্টাডিজ থেকে পিএইচডি সম্পন্ন করেছেন। ড. আলম বিভিন্ন আন্তর্জাতিক সংস্থা- বিশেষ করে জাপান ইন্টারন্যাশনাল কো-অপারেশন এজেন্সি (জাইকা), ইন্টারন্যাশনাল লেবার অর্গানাইজেশন (আইএলও), ইন্টারন্যাশনাল অর্গানাইজেশন ফর মাইগ্রেশন (আইওএম), ইউনাইটেড নেশনস ডেভেলপমেন্ট প্রোগ্রাম (ইউএনডিপি), হিরোশিমা পিসবিল্ডার্স সেন্টার (এইচপিসি), সিটিজেনশিপ, ইমিগ্রেশন কানাডা (সিআইসি) এবং জাপান ফাউন্ডেশনের সাথে কাজ করেছেন। তিনি আন্তর্জাতিক শিক্ষা উন্নয়নে তাঁর অসামান্য গবেষণা অবদানের জন্য ২০১৯ সালে বেশ কয়েকটি আন্তর্জাতিক পুরস্কার লাভ করেছেন। জ্ঞানের আলো ছড়াতে এবং বাংলাদেশে নিজ সমাজের কাছে ক্রসরেফ পরিষেবাগুলির পরিচিতি প্রেরণে উৎসাহী ড. আলম। তিনি জার্নাল এবং প্রকাশকদের ক্রসরেফে যোগদান করতে ও ক্রসরেফের দেওয়া সরঞ্জাম এবং পরিষেবাগুলি ব্যবহার করতে সহায়তা করেন। তিনি বাংলা, ইংরেজি এবং জাপানি ভাষায় পারদর্শী। ড.    


### Bangladesh - Shaharima Parvin
{{% imagewrap left %}} <img src="/images/ambassadors/shaharima-parvin.jpg" alt="Shaharima Parvin" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Shaharima Parvin is Assistant Librarian at East West University, Dhaka, Bangladesh with more than 10 years’ experience in information science and library management. Her current role includes managing the acquisitions life cycle of electronic resources including subscriptions, access, troubleshooting, usage analysis, and budgeting. She obtained a BA and MA in Information Science and Library Management from University of Dhaka, Bangladesh.
Shaharima is an independent researcher with interests in Open Access, Open Education, Creative Commons, the Open Science Framework and Open Data. She has held numerous diverse positions including SIG-USE Recruitment/Membership Officer of Association for Information Science and Technology (ASIS&T), Country Ambassador of The Center for Open Science, USA, Country Ambassador of CORE, UK, and Country Ambassador of International Librarians Network (ILN).
Shaharima is keen to promote the benefits of Crossref services not only among her university community but also among library and information science (LIS) professionals in her country. She is enthusiastic about travel, reading and writing. She loves working with diverse groups of people and appreciates taking on new challenges and exploring unique experiences.   

শাহারিমা পারভীন বর্তমানে ইস্ট ওয়েস্ট ইউনিভার্সিটি, ঢাকা, বাংলাদেশের সহকারী গ্রন্থাগারিক হিসেবে কর্মরত। তথ্য বিজ্ঞান এবং গ্রন্থাগার ব্যবস্থাপনা বিষয়ে তার দশ বছরেরও বেশি অভিজ্ঞতা রয়েছে। সহকারী গ্রন্থাগারিক হিসাবে তার বর্তমান দায়িত্ব হল ইলেকট্রনিক রিসোর্স নির্বাচন, সাবস্ক্রিপশন, অ্যাক্সেস, ট্রাবলশুটিং, ব্যবহার বিশ্লেষণ এবং বাজেট সহ যাবতীয় বিষয় তদারকি করা। তিনি ঢাকা বিশ্ববিদ্যালয় থেকে তথ্য বিজ্ঞান এবং গ্রন্থাগার ব্যবস্থাপনা বিষয়ে স্নাতক এবং স্নাতকোত্তর ডিগ্রি অর্জন করেন। শাহারিমা একজন স্বাধীন গবেষক যার আগ্রহ ওপেন অ্যাক্সেস, ওপেন এডুকেশন, ক্রিয়েটিভ কমন্স, ওপেন সায়েন্স ফ্রেমওয়ার্ক এবং ওপেন ডেটা।
তিনি SIG-USE মেম্বারশিপ অফিসার, অ্যাসোসিয়েশন ফর ইনফরমেশন সায়েন্স অ্যান্ড টেকনোলজি (ASIS&T), কান্ট্রি অ্যাম্বাসেডর, দ্য সেন্টার ফর ওপেন সায়েন্স, মার্কিন যুক্তরাষ্ট্র, কান্ট্রি অ্যাম্বাসেডর, CORE, যুক্তরাজ্য এবং ইন্টারন্যাশনাল লাইব্রেরিয়ান নেটওয়ার্কের কান্ট্রি অ্যাম্বাসেডর সহ অসংখ্য বিভিন্ন পদে অধিষ্ঠিত হয়েছেন।
শাহরিমা কেবল তার বিশ্ববিদ্যালয় সম্প্রদায়ের মধ্যেই নয়, তার দেশের গ্রন্থাগার এবং তথ্য বিজ্ঞান (LIS) পেশাদারদের মধ্যেও Crossref এর পরিষেবার সুবিধাগুলি প্রচার করতে আগ্রহী। তিনি ভ্রমণ, পড়া এবং লেখার বিষয়ে উত্সাহী। তিনি বিভিন্ন গোষ্ঠীর মানুষের সাথে কাজ করতে পছন্দ করেন, নতুন চ্যালেঞ্জ গ্রহণ এবং অনন্য অভিজ্ঞতা অন্বেষণ করতে পছন্দ করেন।   

### Bhutan - Norbu Gyeltshen
{{% imagewrap left %}} <img src="/images/ambassadors/Norbu Gyeltshen.png" alt="Norbu Gyeltshen" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Mr. Norbu Gyeltshen is currently working as an Assistant Research Officer in the Department of Academic and Research (DeAR) at the Royal University of Bhutan. He coordinates the Higher Degree Research Division and assists in the monitoring and administration of research degrees at the University. He has been managing one of the premier journals of the university called Bhutan Journal of Research and Development (BJRD) for more than three years in the capacity of Production Editor. He is actively involved in the introduction of the Open Journal System (OJS) for different journals published across the university and strong advocate for making scholarly communications more credible and better by creating different guidelines in journal publication, following international standards and scholarly practice. He serves as the focal person for the Crossref services in the Royal University of Bhutan. Before his present post, he worked as an Assistant Lecturer at Sherubtse College, one of the constituent colleges under the Royal University of Bhutan for almost a year. He holds a major in Botany from the University of Kerala, India.

ངོ་མིང་ ནོར་བུ་རྒྱལ་མཚན་འདི་ ད་ལྟོ་རྒྱལ་འཛིན་གཙུག་ལག་སློབ་སྡེ་གི་ཤེས་ཡོན་དང་ ཞིབ་འཚོལ་ལས་ཁུངས་ནང་ ཞིབ་རོགས་ཞིབ་འཚོལ་འགོ་དཔོན་སྦེ་ ཕྱག་ཞུ་བའི་བསྒང་ཡོདཔ་ཨིན། ཁོ་གིས་ གཙུག་ལག་ཆེ་རིམ་ཞིབ་འཚོལ་སྡེ་ཚན་ནང་ རྒྱལ་འཛིན་གཙུག་ལག་སློབ་སྡེ་གི་ གཙུག་ལག་ཆེ་རིམ་ཞིབ་འཚོལ་ལས་རིམ་ཚུ་ འཛིན་སྐྱོང་དང་ལྟ་རྟོག་འབད་ནིའི་དོན་ལུ་ གྲོགས་རམ་འབདཝ་ཨིན།
ཁོ་གིས་གཙུག་ལག་སློབ་སྡེ་ནང་ འབྲུག་གི་ཞིབ་འཚོལ་དང་གསར་འཐོན་དུས་དེབ- Bhutan Journal of Research and Development(BJRD) ཟེར་མི་འདི་ ལོ་ངོ་གསུམ་ལྷག་ཙམ་ཅིག་བཟོ་བསྐྲུན་ཞུན་དག་པ་སྦེ་ འཛིན་སྐྱོང་འབད་ཞིན་དུ་ཡོདཔ་ཨིན།
ཁོ་གིས་ད་རུང་མཐོ་རིམ་སློབ་གྲྭ་ཚུ་ནང་དུས་དེབ་དཔེ་བསྐྲུན་འབད་ནིའི་དོན་ལུ་Open Journal System(OJS) ཟེར་མི་འདི་ གཞི་བཙུགས་འབད་དེ་ཡོདཔ་ཨིན།  དེ་མ་ཚད་ མཐོ་རིམ་སློབ་གྲྭ་ཚུ་ནང་ཡོད་པའི་ ཞིབ་འཚོལ་དུས་དེབ་ཚུ་ སྤུས་ཚད་དང་ལྡནམ་སྦེ་  དཔེ་བསྐྲུན་འབད་ཐབས་ཀྱི་ དོན་ལུ་ དུས་དེབ་ལམ་སྟོན་ཚུ་ རྒྱལ་སྤྱིའི་གནས་ཚད་ལྟར་དུ་ གསར་བསྐྲུན་འབད་ནིའི་རྒྱབ་སྐྱོར་འབདཝ་ཨིན། ཁོ་གིས་རྒྱལ་འཛིན་གཙུག་ལག་སློབ་སྡེ་ནང་ crossef ཞབས་ཏོག་ལག་ལེན་འཐབ་ནིའི་དོན་ལུ་ ངོ་ཚབ་གཙོ་བོ་སྦེ་ ཕྱག་ཞུཝ་ཨིན།
ཁོ་གིས་ ད་ལྟོའི་གོ་གནས་མ་འཆང་བའི་ཧེ་མ་ ཤེས་རབ་རྩེ་མཐོ་རིམ་སློབ་གྲྭ་ནང་ཞིབ་རོགས་ལེགས་སྦྱར་བ་སྦེ་ ལོ་གཅིག་དེ་ཅིག་ཕྱག་ཞུ་ནུག། ཁོ་གི་རྒྱ་གར་ནང་ཡོད་པའི་སྐེ་རེ་ལ (Kerala) མཐོ་རིམ་སློབ་གྲྭ་ནང་ལས་ རྩི་ཤིང་རིག་པའི་ གཙུག་ལག་ཤེས་ཚད་ཡོདཔ་ཨིན།


### China - Ran Dang
{{% imagewrap left %}}{{< figure src="/images/ambassadors/ran-dang2.jpg" alt="Ran-Dang" width="100%" >}}{{% /imagewrap %}}   

Ran Dang is Editorial Director of Atlantis Press, Springer Nature. She is in full management of Atlantis Press imprint within book division to drive the growth of open access conference proceedings within all major STM and HSS disciplines globally, by leading the team and overseeing and managing the strategic and day-to-day publishing activities, in collaboration with learnt societies, institutions in academia and departments within the whole group. She is also location lead of Zhengzhou office in Springer Nature China. Prior to joining Springer Nature, she gained extensive experience in academic publishing, including Managing Director China at Atlantis Press, Senior Managing Editor & Section Leader at MDPI, Publishing Support Manager at Elsevier, and Project/General Manager for MLS Journals. With over ten years’ experience in the STM publishing industry, Ran has successfully managed over 90 international journals, conference proceedings series and maintained strong external relationships in academia and industry. Ran is a passionate Open Access and Open Science advocator, who currently serves as volunteer Associate Editor of DOAJ (Directory of Open Access Journals). Ran would like to disseminate information about the value of Crossref services to her community in China, including how the wider global community can make the best use of metadata in Mandarin. In her spare time, Ran likes walking with her family and her puppy “Max”, as well as volunteering at “Tree Hole Rescue Team” using AI to proactively identify and help potential suicide victims who post messages online asking for help. You can contact Ran on [LinkedIn](https://www.linkedin.com/in/randang/).   

党冉女士现任施普林格自然集团旗下Atlantis Press的编辑总监。在图书部，她负责Atlantis Press独立品牌的全方位管理，包括战略部署和合作、团队领导以及日常出版活动。通过与学协会及集团内部各部门的合作，推动全球所有主要STM和HSS学科的开放获取会议论文集的发展。她同时是施普林格-自然中国郑州办公室的负责人。在加入施普林格-自然之前，她在学术出版领域积累了丰富的经验，包括Atlantis Press的中国区董事总经理、MDPI的高级编辑和部门负责人、爱思唯尔的出版经理以及MLS期刊的项目/总经理。在超过十年的STM出版工作经历中，党冉女士，成功管理/合作出版过超过90种国际期刊和会议论文集系列，并在学术界和工业界保持着强大的外部关系。与此同时，作为一名积极的开放获取和开放科学的倡导者，党冉女士目前也在担任DOAJ(开放获取期刊目录)的志愿副主编。党冉女士希望传播有关Crossref的服务对她所在的中国社区的价值的信息，包括更广泛的全球社区如何最好地利用普通话元数据。在业余时间，党冉女士喜欢和家人以及她的拉布拉多犬Max一起散步，并在“树洞救援团”做志愿者工作，利用人工智能技术，主动寻找和帮助那些在网上发布信息寻求帮助的潜在自杀者。你可以在[LinkedIn](https://www.linkedin.com/in/randang/)上联系Ran。

### India - Anjum Sherasiya

  {{% imagewrap left %}} <img src="/images/ambassadors/anjum-sherasiya.jpg" alt="Anjum Sherasiya" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Anjum Sherasiya has been Editor-in-Chief of Veterinary World since 2008 and the International Journal of One Health since 2015. With 11 years’ experience in scientific publishing, he has extensive experience in Open Access (OA) and scholarly publishing. He was the first to bring the idea of an OA Veterinary journal to India and made Veterinary World the first open access journal among Veterinary journals of India in 2011. He is enthusiastic about disseminating information about Crossref services to his community and throughout the world. His journals benefit from Crossref services such as Content Registration, Similarity Check, and Cited-By.

Dr. Anjum Sherasiya is a passionate advocate of the importance of plagiarism checking in academia. However, in his experience, few universities have adopted this practise in Southeast Asian countries. In addition to English, Dr. Anjum Sherasiya speaks Hindi and Gujarati. He is from Wankaner, Gujarat, India.      

### India - Babu Balraj     

{{% imagewrap left %}} <img src="/images/ambassadors/Babu_Balraj.jpg" alt="Babu Balraj" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Dr. Babu Balraj had earned his Ph.D. in 2017 from Bharathiar University, India. Later, he worked as a postdoctoral fellow at the Department of Physics at the National Chung Hsing University in Taiwan (2018–2019) and the Department of Electrical Engineering at the National Tsing Hua University in Taiwan (2019–2021). Dr. Babu is the founder of the Asian Research Association and the Publishing Director of IOR Press. In India, he initiated the use of DOI numbers in publishing Tamil scholarly articles and books, and he created awareness among Tamil scholars. Dr. Babu is now extending the same to the other Eighth Schedule Languages of the Indian Constitution. Furthermore, he is enthralled by the Crossref's Crossmark service.   

முனைவர் பாபு பால்ராஜ் அவர்கள், 2017 ஆம் ஆண்டு இந்தியாவிலுள்ள பாரதியார் பல்கலைக்கழகத்தில் முனைவர் பட்டம் பெற்றவர். பின்னர் தைவானில் அமைந்துள்ள புகழ்பெற்ற கல்வி நிறுவனமான தேசிய சுங் ஹ்சிங் பல்கலைக்கழகத்தின் இயற்பியல் துறையிலும் (2018-2019) தேசிய சிங் ஹுவா பல்கலைக்கழகத்தின் மின் பொறியியல் துறையிலும் (2019- 2021) முதுமுனைவராக பணியாற்றி உள்ளார். தற்போது பதிவு பெற்ற ஆராய்ச்சி நிறுவனமான ஆசிய ஆராய்ச்சி சங்கத்தின் நிறுவனராகவும், ஐ ஓ ஆர் பன்னாட்டுப் பதிப்பகத்தின்  வெளியீட்டு இயக்குனராகவும் இருந்துவருகிறார். தமிழாய்வுக் கட்டுரைகள் மற்றும் நூல்வெளியீட்டில் எண்ணிமப் பொருள் அடையாளங்காட்டி (DOI) பயன்பாட்டை நடைமுறைப்படுத்தியதில் குறிப்பிடத்தக்கவர். தமிழ்மொழிக்கு மட்டுமல்லாமல் இந்திய அரசியலமைப்பில் எட்டாவது அட்டவணையில் உள்ள பிற இந்திய மொழிகளுக்கும் இதே முறையை நடைமுறைப்படுத்தும் நோக்கில் செயல்பட்டுக்கொண்டிருக்கிறார். அதுமட்டுமல்லாமல் கிராஸ்ரெப்ஃபரன்ஸ் இன் கிராஸ்மார்க்  (Crossref's Crossmark) சேவைகளில் மிகவும் ஆர்வமும் ஈடுபாடும் உள்ளவர்.        

## India - Sushil Kumar

  {{% imagewrap left %}} <img src="/images/ambassadors/Sushil-Kumar.png" alt="Sushil Kumar" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Dr. Sushil Kumar is Deputy Dean of journals and publications at [Chitkara University Publications](https://publications.chitkara.edu.in/), Chandigarh, India. He has organized several national and international academic events as well as presenting his own research. He is founding editor of the 'Journal of Nuclear Physics, Material Sciences, Radiation and Applications' since 2013. Dr. Sushil set up the Open Journal Systems (OJS) platform for Chitkara University Publications. This role includes managing indexing, formatting, publication policies, membership, digital marketing, SEO of research articles and journal management for all Open Access journals. He is a strong advocate of the Open Access movement and all Chitkara University Publications are available via platinum Open Access.  Additionally, Sushil aims to increase awareness about publication and research ethics among the scientific community, along with providing technical support and information about indexing, repositories, preprint archives, Open Access rights and licenses, and persistent Identifiers.
Dr. Sushil is also a scientific [blogger](https://apniphysics.com) and has his own [YouTube channel](https://www.youtube.com/user/sushilk17able), with more than 73,500 subscribers, to share his learning and related information with the wider academic  community.

 डॉ0  सुशील कुमार, चितकारा विश्वविधालय प्रकाशन, चंडीगढ़, भारत में शोध पत्रिकाओं और प्रकाशन का कार्यभार  संभालते हैं। उन्होंने कई राष्ट्रीय और अंतर्राष्ट्रीय शैक्षणिक कार्यक्रम आयोजित किए हैं, और हिस्सा लिया है । वह 2013 में शुरू किए गए भौतिकी जर्नल के संस्थापक संपादक हैं। डॉ0 सुशील, ने चितकारा विश्वविद्यालय प्रकाशनों के लिए ओपन जर्नल सिस्टम (OJS) की स्थापना की है, और इसकी अनुक्रमण, लेआउट, प्रकाशन नीतियों, सदस्यता, डिजिटल मार्केटिंग का प्रबंधन किया है। सभी खुली पहुंच वाली पत्रिकाओं के लिए शोध लेख और जर्नल मैनेजर की भूमिका वहन कर रहे हैं । वह अनुसंधान पत्रों और पुस्तकों के प्रकाशन के लिए ओपन एक्सेस मॉडल का समर्थन और वकालत करते है। सभी जर्नल प्लैटिनम ओपन एक्सेस जर्नल हैं। इसके अलावा, वह शिक्षा और शोध समुदाय के बीच "प्रकाशन और अनुसंधान नैतिकता" के बारे में जागरूकता पैदा कर रहै हैं। साथ ही अनुक्रमण, रिपॉजिटरी, प्रीप्रिंट डायरेक्ट्रीज , खुले उपयोग के अधिकार और लाइसेंस, और लगातार पहचानकर्ताओं की तकनीकी जानकारी को युवा शोधार्थियों तक पहुंचाने का कार्य भी कर रहैं  है। डॉ0 सुशील एक विज्ञानं [ब्लॉगर](https://apniphysics.com) और [यूट्यूब चैनल](https://www.youtube.com/user/sushilk17able) संभालते है, जिसमें 73500 से अधिक व्यक्तियों के साथ शोध और प्रकाशन संबंधित जानकारी को साझा करते हैं।     


### Indonesia - Azhar Aziz Lubis

{{% imagewrap left %}} <img src="/images/ambassadors/azhar-aziz-lubis.png" alt="Azhar Aziz Lubis" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Azhar Aziz Lubis started his academic career as an English lecturer at the Faculty of Languages and Arts, State University of Medan in 2016 and moved to the Faculty of Teacher Training and Education, University of Bengkulu (UNIB) in 2018. He also serves as the technical coordinator for Open Journal System (OJS) at his university and chairs a weekly online forum with Indonesian journal managers and editors called ‘Jumat Jurnal’. Since 2019, he has been a member of Relawan Jurnal Indonesia (RJI), a voluntary non-profit organisation which aims to help Indonesian journal managers involved in academic publishing.

Azhar Aziz Lubis memulai kariernya sebagai dosen Bahasa Inggris pada Fakultas Bahasa dan Seni Universitas Negeri Medan di tahun 2016 dan pindah ke Fakultas Keguruan dan Ilmu Pendidikan Universitas Bengkulu (UNIB) pada 2018. Dia juga bekerja sebagai Koodinator Open Journal System (OJS) di universitasnya dan memimpin kegiatan rutin mingguan bernama ‘Jumat Jurnal’. Sejak 2019, ia terdaftar menjadi anggota Relawan Jurnal Indonesia (RJI), sebuah organisasi relawan non-profit yang bertujuan untuk membantu pengelola jurnal di Indonesia dalam menangani publikasi akademik.    

### Indonesia - Dora Dayu Rahma Turista
{{% imagewrap left %}} <img src="/images/ambassadors/Dora Dayu Rahma Turista.jpg" alt="Dora Dayu Rahma Turista" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Dora Dayu Rahma Turista is a biologist. Dora started her career in 2015 as a lecturer in the Department of Medical Laboratory Technology, STIKES Hutama Abdi Husada Tulungagung, East Java, Indonesia. In 2021 Dora moved to work as a lecturer at the Biology Education Study Program at Mulawarman University, East Kalimantan, Indonesia. In her daily life, Dora is also active as editor in several scientific journals. In addition, she is also a member of the reference manager software socialization community.

Dora Dayu Rahma Turista adalah seorang biologist. Dora memulai karir sebagai seorang dosen di Program Studi Teknologi Laboratorium Medis, STIKES Hutama Abdi Husada Tulungagung, Jawa Timur, Indonesia pada tahun 2015. Pada tahun 2021 Dora pindah bekerja sebagai dosen di Program Studi Pendidikan Biologi, Universitas Mulawarman, Kalimantan Timur, Indonesia. Dalam kesehariannya Dora juga aktif sebagai pengelola di beberapa jurnal ilmiah. Selain itu dia juga tergabung dalam komunitas sosialisasi software reference manager.   

### Indonesia - Muhamad Ratodi
{{% imagewrap left %}} <img src="/images/ambassadors/muhamad-ratodi.jpg" alt="Muhamad Ratodi" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Muhamad Ratodi has been in academic publishing since 2011 and currently works in the Department of Architecture at State Islamic University of Sunan Ampel (UINSA) Indonesia. He is also a member of Relawan Jurnal Indonesia, a non-profit volunteer organization committed to improving the quality of scientific publications in Indonesia. He is very enthusiastic in making publications in Indonesia easy to find, cite, link, and assess as well continuously campaigning the Open Access concept to his academic colleagues. He also serves as an editor in several academic journals and likes to write blogs in his spare time. You can interact with him on his twitter account: @m_ratodi

Muhamad Ratodi telah berkecimpung di dunia penerbitan akademik sejak 2015. Saat ini Muhamad tercatat sebagai dosen di Program Studi Arsitektur, Universitas Islam Negeri Sunan Ampel (UINSA). Saat ini Muhamad juga merupakan anggota dari Relawan Jurnal Indonesia, sebuah lembaga non-profit yang berkomitmen dalam peningkatan kualitas publikasi ilmiah di Indonesia. Antusiasmenya adalah membuat publikasi di Indonesia menjadi mudah ditemukan, diakses, ditautkan dan disitasi, serta mengkampanyekan konsep Open Access pada komunitas akademik di lingkungannya. Saat ini ia berperan sebagai editor pada beberapa jurnal akademik dan sering meluangkan waktu luangnya untuk menulis blog. Anda dapat berinteraksi dengannya melalui akun twitternya di @m_ratodi

### Indonesia - Zulidyana D Rusnalasari  

{{% imagewrap left %}}{{< figure src="/images/ambassadors/zulidyana-rusnalasari.jpg"  alt="Zulidyana-Rusnalasari ambassador" width="100%" >}}{{% /imagewrap %}}  

Zulidyana D Rusnalasari is a researcher and scientific journal editor. Teaching is not only her primary career but also her hobby. Zulidyana is a strong advocate of Open Science and believes that information, particularly related to public knowledge and science, should be available openly and reliably. As a lecturer and trainer, she campaigns for Open Science to her students and trainees. Her interest in this area began in 2010 when she researched the Open Source Community as part of her postgraduate studies in Cultural Studies at the University of Indonesia. Although Zulidyana is a junior lecturer, she works to improve her colleagues' knowledge regarding scientific publication literacy and its relation to Open Science Movements. Currently, she is finalizing her dissertation to complete her doctoral degree in Literature and Cultural Education. Zulidyana believes that education is the key to improving human quality of life. Being a Crossref Ambassador supports Zulidyana in her mission to improve scholarly communications among the communities and NGOs that she is involved in. She is also keen to help academic researchers know how to better use Crossref metadata. You can interact with her on her Twitter account @zulidyana.  

Zulidyana D. Rusnalasari, peneliti sekaligus editor di beberapa jurnal ilmiah di Indonesia. Sebenarnya, mengajar adalah hobi sekaligus profesi utamanya, sebagai dosen maupun trainer, dia senantiasa mengkampanyekan Sains Terbuka pada mahasiswa maupun peserta pelatihan. Zulidyana optimis dan percaya bahwa informasi terutama yang berkaitan dengan ilmu pengetahuan/sains seharusnya tersedia secara terbuka dan dapat dipercaya. Ketertarikannya pada Sains Terbuka dimulai sejak dia meneliti komunitas Open Source ketika dia menyeleseikan studi S2-nya di Universitas Indonesia dalam bidang Cultural Studies pada tahun 2010. Walaupun masih sebagai dosen junior di kampus, dia selalu berusaha meningkatkan literasi koleganya mengenai literasi publikasi ilmiah dan kaitannya dengan Sains Terbuka. Saat ini, dia sedang menyeleseikan disertasinya untuk menuntaskan pendidikan doktoralnya di bidang Pendidikan Sastra dan Budaya. Meskipun Pendidikan adalah domain baru baginya, namun dia selalu percaya bahwa Pendidikan adalah hal paling esensial untuk meningkatkan kualitas manusia. Anda dapat berinteraksi dengannya di akun twitter-nya @zulidyana.   

### Iraq - Salwan Abdulateef
{{% imagewrap left %}} <img src="/images/ambassadors/Salwan-Abdulateef.jpg" alt="Salwan Abdulateef" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Dr. Salwan M. Abdulateef currently works as Scientific Research Director, at the University of Anbar. Abdulateef specializes in animal behavior and endocrinology research. He is a member of the Advisory Board of QS World Universities Rankings. Dr. Abdulateef also works as Director of the Academic Journal’s Unit and Managing Editor of the Anbar Journal of Agricultural Sciences and is a member of the Scientific Sobriety Committee for Scientific Research and Publications. Abdulateef has authored more than 44 scientific papers in his field of specialization. He supports journals to join Crossref and expects, as an Ambassador, to improve the dissemination of knowledge and teaching on how to explore and use the tools and services offered by Crossref.   

<div style="text-align: right">
يعمل الدكتور سلوان محمد عبداللطيف حالياً مديراً للبحث العلمي في جامعة الأنبار. عبد اللطيف متخصص في علم الغدد الصماء وسلوك وفسلجة الحيوان. وهو عضو في المجلس الاستشاري لتصنيفات الجامعات العالمية. يعمل الدكتور عبداللطيف أيضًا كمدير لوحدة المجلة الأكاديمية ومدير التحرير في مجلة الأنبار للعلوم الزراعية وعضوًا في لجنة الامانة العلمية للبحث العلمي والنشر الرصين. قام عبد اللطيف بتأليف أكثر من أربعين بحث علمي في مجال تخصصه. وهو يدعم العديد من المجلات في الحصول على بادئة المعرف الرقمي الخاصة بها ويتوقع، كسفير، تحسين نشر المعرفة والتعليم حول كيفية استكشاف واستخدام جميع الأدوات التي تقدمها كروسريف، من خلال إجراء العديد من ورش العمل التدريبية والمحاضرات والندوات حول كروسريف.
</div>

### Kazakhstan – Gulzhaina Kuralbaevna Kassymova
{{% imagewrap left %}} <img src="/images/ambassadors/kassymova.jpg" alt="Gulzhaina Kuralbaevna Kassymova" width="200px" class="img-responsive" /> {{% /imagewrap %}}   


Ph.D., Dr. in Ed. Gulzhaina Kuralbaevna Kassymova is a teacher in adult education and a linguist in a foreign language. She started her career in 2008 as a flight attendant in the aviation industry and then transitioned to the educational sector. Currently, she teaches educational psychology at Abai University and Suleyman Demirel University in Kazakhstan. She published several research articles at the international level in a short time period during her bilateral doctoral studies in Indonesia and Kazakhstan, <a href="https://orcid.org/0000-0001-7004-3864">ORCID ID: 0000-0001-7004-3864</a>. In her spare time, she loves diving and reading scientific investigations. Besides teaching students, she likes to work with metadata and she is responsible for Membership of the Institute of Metallurgy and Ore Beneficiation with Crossref. She is also responsible for promoting Crossref in the Republic of Kazakhstan.

Ph.D., білім беру ғыл. док. Гулжайна Куралбаевна Касымова – ЖОО-да педагог және шет тілі бойынша лингвист. Ол өзінің еңбек жолын 2008 жылы авиация саласында стюардесса болып бастады, содан кейін мансабын білім беру саласына ауыстырды. Қазіргі уақытта ол Қазақстандағы Абай университетінде және Сүлеймен Демирел университетінде білім беру психологиясынан сабақ береді. Ол Индонезияда және Қазақстанда екіжақты докторантурада оқу кезінде қысқа уақыт ішінде халықаралық деңгейде бірнеше ғылыми мақалаларды басып шығара алды, <a href="https://orcid.org/0000-0001-7004-3864">ORCID ID: 0000-0001-7004-3864</a>. Ол бос уақытында суға түсуді және ғылыми зерттеулерді оқығанды ұнатады. Cабақ берумен қатар, ол DOI-мен жұмыс істегенді ұнатады және Crossref-пен Металлургия және кен байыту институтының  арасындағы қарым-қатынасқа жауапты. Ол сонымен қатар Қазақстан Республикасында Crossref DOI ілгерілетуіне жауапты.

### Malaysia - Mohammad Tariqur Rahman
{{% imagewrap left %}} <img src="/images/ambassadors/Mohammad-Tariq.png" alt="Mohammad Tariqur Rahman ambassador" width="200px" class="img-responsive" /> {{% /imagewrap %}}    

After completing his M.Sc. in Microbiology from the University of Dhaka, Mohammad Tariqur Rahman started his academic career as a Lecturer in Biotechnology at Khulna University, Bangladesh. Receiving the Interfaculty Council for Development Co-operation Scholarship he completed a M.Sc. and Ph.D. in Biochemistry from KU Leuven, Belgium and returned to Bangladesh where he served as the founding Chair of the Department of Pharmacy at East West University. Later Tariq joined the University of Fukui Medical School in Japan as CoE researcher and subsequently the Faculty of Science at the International Islamic University Malaysia. Since July 2015, he has been affiliated with the Faculty of Dentistry, University of Malaya. His research interests include metallo-biochemistry in health and diseases; neocortex development; medicinal applications of natural products; halal food science; stem cells; and scientometrics. Tariq is the current Executive Editor of Annals of Dentistry University of Malaya and has been elected as President of the Malaysian Society for Oral Microbiologists and Oral Immunologists, MySOMOI.

### Mongolia - Gantulga Lkhagva  
{{% imagewrap left %}} <img src="/images/ambassadors/Gantulga-Lkhagva.png" alt="Gantulga-Lkhagva" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Gantulga Lkhagva has over 19 years' experience working in research and academic institutions and public libraries in Mongolia. Gantulga’s work focuses on improving the publication quality of Mongolian scholarly communities, providing advise on scientific publishing processes, and promoting Mongolian knowledge in the digital age. Gantulga is the Founder and CEO of Mongolian Digital Knowledge Solutions(MDKS), LLC, and MongoliaJOL. He manages Mongolia Journals Online (MongoliaJOL) – a journal platform which was established through collaboration with INASP. MongoliaJOL is a Crossref member and also participates in additional services such as Similarity Check.

Gantulga is happy to support the introduction and promotion of Crossref services in Mongolia. He enjoys collaborating with others; exchanging ideas and experiences. Gantulga also plans to disseminate knowledge and provide training to authors, journal editors, as well as work with other stakeholders. You can contact Gantulga via Twitter @cybermongol or on LinkedIn   

### Nepal - Binayak Raj Pandey
{{% imagewrap left %}} <img src="/images/ambassadors/Binayak-Raj Pandey.jpg" alt="Binayak Raj Pandey" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Binayak Raj Pandey has a Master of Technology in biotechnology and works as Biology Expert at Innovate Tech, an EdTech company. He currently holds the honorary position of Associate Editor-in-Chief for the Nepal Journal of Biotechnology and is an Executive Board Member of the Biotechnology Society of Nepal. Working in a low-income country like Nepal, Binayak has researched and authored a few high-impact scientific publications. Previously, he has served as a lecturer, QA & QC Manager, and consultant for government organizations and INGOs such as Helvetas. He has experience in Open Journal Systems (OJS), JOL projects, and metadata management and holds several international certifications in data science and Python programming. You can find more information as well as how to contact Binayak via his website [binayakrp.com.np](https://binayakrp.com.np).   

विनायक राज पाण्डे ज्यूले बायोटेक्नोलोजीमा मास्टर अफ टेक्नोलोजी डिग्री हासिल गर्नुभएको छ। उहां एडटेक कम्पनी इनोभेट टेकमा जीवविज्ञान विशेषज्ञका रूपमा काम गर्नु हुन्छ । उहाँ हाल नेपाल जर्नल अफ बायोटेक्नोलोजीका लागि एसोसिएट एडिटर-इन-चीफको मानार्थ पदमा हुनुहुन्छ र नेपाल बायोटेक्नोलोजी सोसाइटीका कार्यकारी बोर्ड सदस्य पनि हुनुहुन्छ। नेपाल जस्तो कम आय भएको देशमा काम गर्दै, उहाले केही उच्च प्रभाव भएका वैज्ञानिक प्रकाशनहरूमा आफ्नो बैज्ञानिक अनुसन्धान प्रकाशित गरिसक्नु भएको छ । यसअघि, उहांले लेक्चरर, QA र QC प्रबन्धक, र हेल्भेटास जस्ता आईएनजीओहरू र केही सरकारी संस्थाहरूको लागि सल्लाहकारको रूपमा सेवा गरिसक्नु भएकाे छ। उहांसँग ओपन जर्नल प्रणाली (OJS), JOL परियोजनाहरू, र मेटाडाटा व्यवस्थापनमा अनुभव छ र डाटा विज्ञान र पाइथन प्रोग्रामिङमा धेरै अन्तर्राष्ट्रिय प्रमाणपत्रहरू पनि छन् । तपाईले उहाँको वेबसाइट [binayakrp.com.np](https://www.binayakrp.com.np/?i=2) मार्फत उहांकाे बारेमा थप जानकारीका साथै उहांलाई कसरी सम्पर्क गर्ने भनेर थाहा पाउन सक्नुहुन्छ।

### Singapore - Woei Fuh Wong
{{% imagewrap left %}} <img src="/images/ambassadors/woei-fuh-wong-sq2.png" alt="Woei Fuh Wong singa" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Woei Fuh Wong has gone through a career transformation from a researcher to an engineer and later, an information specialist over a period of three decades. During his 10 years with Web of Science (in Thomson Reuters) since 2004, he actively engaged with researchers and academics in Asia Pacific region on bibliometric insights and research assessment. In 2015, Woei Fuh started a consulting firm based in Singapore and he founded a research program, Research 123, helping younger researchers with research skill sets. Since then, he is a strong advocate of research best practices ranging from authoring, publishing and outreaching. His consultancy work focuses on research communication and he delivered numerous workshops to university researchers about: research impact, research storytelling, and research clustering & collaboration. In his recent consulting work, he helped local journal publishers in improving their visibility through research outreaching program for their research authors. He holds a Ph.D. in Polymer Science & Technology from the University of Manchester, UK, and a MBA from Louisville University, USA.

超过三十年的职业生涯，黄伟富博士从研究者转型成为工程师与企业管理者，目前则是资讯科学专家。在汤森路透旗下专售Web of Science部门的十年间，黄博士积极参与亚太地区书目计量与研究评量相关的各项活动。2015年于新加坡成立以研究传播为焦点的顾问团队，并致力推广为年轻研究者建立研究核心能力的计划Research 123，积极提倡从写作、出版、到行销全方位的研究最佳实践。黄博士经常至学术机构进行演讲与工作坊，主题包含研究的社会影响力、研究口语化、协同合作等。在近期的顾问计画中，他协助地区性的期刊出版社利用研究行销计画提高期刊与作者的能见度。黄博士于英国曼彻斯特大学高分子科学取得博士学位，并为美国路易斯维尔大学企管硕士。   

### South Korea - Yera Hur
{{% imagewrap left %}} <img src="/images/ambassadors/Yera-hur.jpg" alt="Yera Hur" width="200px" class="img-responsive" /> {{% /imagewrap %}}

My side hustle has transformed me from a full-time medical professor to a flower designer, research ethics lecturer, family therapist, and character traits instructor five years ago. But my involvement as a researcher (Hallym University College of Medicine), board member and reviewer for SCIE level journals continues to this day. During my experience as an associate editor of two major medical education journals in Korea (KJME and JEEHP), I discovered the excellent tools offered by Crossref. In light of how the two journals have benefited from Crossref’s diverse programs, I am looking forward to my role as an ambassador of Crossref. I always find that anything volunteered that does not involve money is always the most fun!

안녕하세요. N잡러, “꽃만지는 상담사” 허예라입니다. 5년전 경력전환을 하여 전임 의과대학 교수에서 현재는 한림의대 연구원, 연구윤리 강사, SCIE급 저널 심사위원과 편집위원, 플라워디자이너, 가족상담사, 그리고 도형심리 교육강사로 가장 활발히 활동하고 있습니다. Korean Journal of Medical Education의 부편장과 편집위원의 봉사 경력과, 지난 7년간 Journal of Educational Evaluation for Health Professions (JEEHP)의 부편집장으로 일하면서 Crossref를 접하게 되었습니다. JEEHP에서는 Crossref의 모든 서비스를 활용하고 있으며 이로 인해 학술지의 국제화에 큰 혜택을 보고 있습니다. Crossref ambassador 활동을 통해 다양한 프로그램 기획에 참여하고 싶고, 세계적인 네트워크뿐만 아니라, 즐거운 배움과 봉사활동도 함께 기대합니다. 자발적이면서 돈 버는 일이 아닌 것은 언제나 가장 즐거우니까요!   

### Taiwan - Iris Hsu
{{% imagewrap left %}} <img src="/images/ambassadors/Iris-Hsu.png" alt="Iris Hsu" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Iris Hsu has acquired an in-depth knowledge of scholarly publishing ecosystem progressively at different stages of her 15-year career. With the foundation of a Bachelor's Degree in Library & Information Science from National Taiwan University, she took several leadership roles in iGroup, the leading regional information provider for science and education in Asia Pacific. That has given her the opportunity to collaborate with global premier publishers like ACS (American Chemical Society). Currently Iris Hsu is the key consultant based in Taipei for ies Research, an innovative startup from iGroup helping researchers to improve their research visibility across different disciplines and in alignment with SDG (Sustainable Development Goal) Impacts. She is also the Chief Editor of ACCESS@LibraryLearningSpace.com, Asia’s newspaper on information products and services, sponsored by iGroup.

徐惠玲女士服務於學術出版相關產業多年, 自臺灣大學圖書資訊學系畢業後, 任職於亞太區規模最大之學術資訊提供商 iGroup, 其間經歷商品開發、行銷、教育訓練等不同職位, 與知名學術出版社如ACS (American Chemical Society, 美國化學會) 等維持著長期的合作關係。目前徐女士在iGroup旗下新創團隊ies Research中擔任諮詢角色, 協助研究者提高其研究能見度, 並結合SDG (Sustainable Development Goal, 聯合國永續發展指標) 以提升研究的社會影響力。徐女士同時亦擔任ACCESS@LibraryLearningSpace.com 的主編, 該網站以報導亞太區圖書資訊與數位出版動態為主要任務

### Turkey - Haydar Oruç
{{% imagewrap left %}} <img src="/images/ambassadors/Haydar-Oruc.jpg" alt="Haydar Oruç" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Haydar Oruc holds BA and MA degrees in public administration and is currently working towards his Ph.D. (to be completed by the end of 2021). He currently works at The Center for Middle Eastern Studies (ORSAM) as a Levant expert. His research scope includes Political History of Middle East, Israel Politics and Society, Turkey-Israel Relations, the Israel-Palestine conflict, Israel’s NGOs, and the role of NGOs in Israel’s Foreign Policy Making. He also studies humanitarian aid, refugee problems, cultural diplomacy, and energy issues. Haydar has had an interest in academic publishing since 2014 and currently works as editor of the Journal of Middle Eastern Studies (Ortadogu Etutleri), published by ORSAM since 2009, as well as serving as associate editor of DOAJ. Haydar has worked with various Crossref services such as Content Registration and Similarity Check and has participated in further training to broaden his knowledge. As an ambassador, Haydar strives to increase awareness and visibility of Crossref in Turkey and provides assistance and advice to Crossref members.

Haydar Oruç, kamu yönetimi alanında lisans ve yüksek lisans derecelerine sahiptir ve şu anda doktora eğitimini tamamlamaya çalışmaktadır (2021'in sonunda tamamlamayı ummaktadır). Halen Ortadoğu Araştırmaları Merkezi (ORSAM)’da Levant uzmanı olarak çalışmaktadır. Uzmanlık alanları; Ortadoğu Siyasi Tarihi, İsrail’de Toplum ve Siyaset, Türkiye-İsrail İlişkileri, İsrail-Filistin çatışması, İsrail STK'ları ve İsrail’in Dış Politika Yapımında STK'ların rolüdür. Ayrıca insani yardım, mülteci sorunları, kültürel diplomasi ve enerji konularıyla da ilgilenmektedir. 2014 yılından beri akademik yayıncılık ile ilgilenen Haydar, 2009 yılından bu yana ORSAM tarafından yayınlanan Ortadoğu Etütleri Dergisi'nin editörü ve DOAJ'ın yardımcı editörü olarak görev yapmaktadır. Haydar, içerik kaydı ve benzerlik kontrolü gibi çeşitli Crossref hizmetleri üzerine çalıştı ve bilgi seviyesini genişletmek için ileri eğitimlere katıldı. Bir büyükelçi olarak Haydar, Crossref'in Türkiye'deki farkındalığını ve görünürlüğünü artırarak, Crossref üyelerine yardım etmek ve çözüm önerileri sunmak için gayret etmektedir.

### UAE - Mohamad Mostafa

{{% imagewrap left %}} <img src="/images/ambassadors/Mohamad-Mostafa2.jpg" alt="Mohamad Mostafa" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Mohamad Mostafa is working at Knowledge E as a Production Manager and he holds a B.Sc. in Physics from Menoufia University, Egypt. Mohamad has a wide experience in Open Access (OA) and scholarly publishing and through his role at Knowledge E, he participated in the ORCID reviewer team to provide the Arabic interface and reviewed the translated outreach materials.

During his time at Knowledge E, Mohamad conceived a new initiative named ’Think. Check. Attend.‘ with the goal to help researchers choose the right conference to attend and present their research papers, which he presented for the first time at the annual meeting of the Asian Council of Science Editors. He also provided the first translation of the Think. Check. Submit. initiative into a non-English language and has delivered workshops in both Arabic and English to researchers, librarians, and journal editors at different universities across the Middle East including Saudi Arabia, United Arab Emirates, and Egypt.

His areas of expertise cover OA journals publishing and development, publication ethics, indexing databases and aggregators, and copyright policies. He has trained audiences to recognize good publishing practices among publishers and journals before submitting their manuscripts as well as has trained them on the various Crossref services.

<div style="text-align: right"> يعمل محمد مصطفي لدي نوليدچ إي كمحرر نشر وهو حاصل علي بكالوريوس علوم الفيزياء من جامعة المنوفيه بجمهورية مصر العربية. يمتلك محمد خبره واسعة في مجالات الوصول الحر والنشر الأكاديمي. خلال عمله لدي نوليدچ إي قام محمد بمشاركة فريق مراجعة أوركيد لتقديم الواجهه باللغة العربية

البحثية. وكان محمد قد أزاح الستار عن المبادرة للمرة الأولي أثناء التجمع السنوي للمجلس الآسيوي للمحررين العلميين. كذلك قام محمد بتقديم اول ترجمات مبادرة "فكر.راجع.ارسل" للغة اخري غير الإنجليزية. يقوم محمد بتقديم ورش عمل باللغتين العربية والإنجليزية للباحثين والمكتبيين ومحررو الدوريات في مختلف الجامعات في منطقة الشرق الأوسط تشمل السعودية والإمارات ومصر.

تشمل مجالات خبراته نشر وتطوير الدوريات العلمية بنظام الوصول الحر، أخلاقيات النشر، قواعد البيانات، وسياسات حقوق الملكية الفكرية. قام محمد بتدريب الحضور علي كيفية التعرف علي الممارسات الجيدة للنشر ما بين الناشرين والدوريات العلمية ليتسنى لهم حسن الاختيار قبل تقديم أوراقهم البحثية، كذلك قام بالتدريب علي مختلف خدمات كروس رف.
</div>

### Uzbekistan - Shokhrukhbek Sobirov
{{% imagewrap left %}} <img src="/images/ambassadors/Shokhrukhbek- Sobirov.jpg" alt="Shokhrukhbek Sobirov" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Shokhrukhbek is co-founder of “in Science” company and manager of the “inLibrary.uz” project. The goal of the project is to create an independent discussion platform for leading scientists of expert-analytical and academic circles, as well as to discuss innovative ideas of the world's leading research and educational institutions. Since 2021, Shokhrukhbek has been working as the editor-in-chief of the Society and Innovations Journal.

In 2021, development of software for the inLibrary.uz scientific electronic library began, based on the "Open Science" paradigm. In 2022, the project was launched and became one of the first in Uzbekistan to promote the transition from paper to electronic media by building an open, indexed science in Uzbekistan. During various meetings, conferences, seminars and webinars, we conduct explanatory work on the rules and technologies for using DOI and Crossref.

In Science kompaniyasining hammuassisi va inLibrary.uz ning loyiha rahbari hisoblanaman. Mening maqsadim - yetakchi olimlar, ekspert-tahliliy va akademik doiralar uchun mustaqil munozaralar maydonchasini yaratish, shuningdek, dunyoning ilg‘or tadqiqot va taʼlim muassasalarining innovatsion g‘oyalarini muhokama qilishdir. 2021-yildan buyon “Jamiyat va innovatsiyalar” jurnalining bosh muharriri lavozimida ishlab kelmoqdaman.

2021-yilda “Open science” paradigmasiga asoslangan inLibrary.uz ilmiy elektron kutubxonasining dasturiy taʼminotini ishlab chiqish boshlandi. 2022-yilda loyiha ishga tushirildi va birinchilardan bo‘lib O‘zbekistonda ilmiy ishlarni qog‘ozdan elektron vositalariga o‘tkazishni va ochiq, indeksatsiyalangan ilm-fanni yaratishni targ‘ib qila boshladi. Turli uchrashuvlar, konferentsiyalar, seminarlar va vebinarlar davomida biz DOI va Crossrefdan foydalanish qoidalari va texnologiyalari bilan bog‘liq tushuntirish ishlarini olib boramiz. 2022-yildan boshlab Crossref kompaniyasining O‘zbekistondagi vakili lavozimini bajarib kelmoqdaman.   


{{% /accordion %}} {{% /accordion-section %}}

{{% accordion %}} {{% accordion-section "Europe" %}}

### Belarus - Alexey Skalaban (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/Alexey-Skalaban.jpg" alt="Alexey Skalaban" width="200px" class="img-responsive" /> {{% /imagewrap %}}  

Alexey graduated from the Belarusian State University of Culture, specializing in automated library and information systems.
From 2009 to August 2017, he served as director of the Scientific Library of the Belarusian National Technical University. Prior to that, he was engaged in the acquisition of electronic resources in the National Library of Belarus. Specialist in the field of creating institutional open access repositories, managing and providing access to scientific electronic information resources, PIDs. Alexey is an author of more than 30 publications in professional journals and collections and trained in the largest libraries in the USA, Poland, Sweden, Germany.   

Окончил Белорусский государственный университет культуры, специализация — автоматизированные библиотечно-информационные системы.
С 2009 г. по август 2017 г. занимал должность директора Научной библиотеки Белорусского национального технического университета. До этого занимался комплектованием электронных ресурсов в Национальной библиотеке Беларуси. Специалист в области создания институциональных репозиториев открытого доступа, управлению и обеспечению доступа к научным электронным информационным ресурсам, постоянным идентификаторам.
Автор более 30 публикаций в профессиональных журналах и сборниках.
Стажировался в крупнейших библиотеках США, Польши, Швеции, Германии.

### Lithuania - Vincas Grigas
{{% imagewrap left %}} <img src="/images/ambassadors/Vincas-Grigas.jpg" alt="Vincas Grigas" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Vincas' research and teaching focuses on open access, media and information literacy, book piracy, information behavior, and scholarly communication. Vincas currently serves as an expert at the swissuniversities Open Science Program (since 2022), is a member of the Lithuanian Library Council at the Ministry of the Culture of the Republic of Lithuania (since 2020) and is a member of SHARP (The Society for the History of Authorship, Reading and Publishing) (since 2020). Vincas holds the positions of president of The Association of Lithuanian Serials, Editor-in-Chief at the scholarly journal 'Relevant Tomorrow' and is a member of the editorial board of the scholarly journal 'Information & Media'. As Head of Scholarly Journals at Vilnius University Press he is responsible for 40+ diamond open access journals. As a Crossref Ambassador Vincas will seek that all Lithuanian scholarly journals employ Crossref tools for better communication and visibility of their content.

Vincas skaito paskaitas ir vykdo mokslinius tyrimus atvirosios prieigos, medijų ir informacinio raštingumo, knygų piratavimo, informacinės elgsenos ir mokslo komunikacijos srityse. Vincas šiuo metu dirba ekspertu swissuniversities Open Science programoje (nuo 2022 m.), yra Lietuvos bibliotekų tarybos prie LR kultūros ministerijos narys (nuo 2020 m.) ir SHARP (The Society for the History of Authorship, Reading and Publishing) narys (nuo 2020 m.). Vincas užima Lietuvos mokslo periodikos asociacijos prezidento pareigas, yra mokslinio žurnalo „Relevant Tomorrow“ vyriausiasis redaktorius, mokslinio žurnalo „Information & Media“ redakcinės kolegijos narys. Kaip Vilniaus universiteto leidyklos Mokslo periodikos vadovas yra atsakingas už daugiau nei 40 deimantinės atvirosios prieigos žurnalų leidybą. Kaip Crossref ambasadorius sieks, kad visuose Lietuvos mokslo žurnaluose būtų panaudojamos Crossref suteikiamos priemonės žurnalo sklaidai ir matomumui pagerinti.

### Poland - Agnieszka Kałowska
{{% imagewrap left %}} <img src="/images/ambassadors/Agnieszka-Kałowska.jpg" alt="Agnieszka Kałowska" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Agnieszka, PhD in literary studies, works as Head of the Academic Journals Department at Lodz University Press, which publishes over 60 open access titles. She is also Managing Editor of the Polish edition of Oxford’s 'Very Short Introductions' series.
As a Crossref Ambassador, Agnieszka will strive to encourage all Central European scientific journals use Crossref tools for better visibility of their content and dissemination of research results.

Agnieszka, doktor nauk humanistycznych w zakresie literaturoznawstwa, pracuje jako kierownik Działu Wydawniczego Czasopism Naukowych w Wydawnictwie Uniwersytetu Łódzkiego, które wydaje ponad 60 tytułów open access. Jest redaktorem prowadzącym polskiej edycji oksfordzkiej serii 'Very Short Introductions'.
Jako Ambasador Crossref Agnieszka będzie dążyć do tego, aby wszystkie środkowoeuropejskie czasopisma naukowe korzystały z narzędzi Crossref dla jak najlepszej widoczności i rozpowszechniania wyników badań.   


### Russia - Maxim Mitrofanov (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/maxim-mitrofanov.png" alt="Maxim Mitrofanov" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Current position -- Not-for-Profit Partnership National Electronic Information Consortium (NP NEICON). NEICON provides all kinds of services for the editors, universities, libraries, etc., such as publishing, consulting, ensuring access to international databases and many more. I was born in Moscow, Russia April 8, 1977, graduated from the University of Foreign Relations and then worked for the Ministry of Foreign Affairs of Russia for nine years. Within that term I spent six years on different positions in Russian Embassies to Canada and then Ghana.

After I left the Ministry in 2007 I worked for the Russian largest Exhibition company, Expocentre and then joined NEICON in 2014. Besides the regular activities in 2015 I was invited to become a DOAJ Ambassador and Associate Editor and I've held this position since then. Within my activities I was one of the first in Russia who started to promote Crossref services and advise editors about their importance to the editorial process and science information exchange at personal meetings, conferences and seminars. NEICON is a Crossref Sponsor in Russia as well. As a result of the work and the recognition of DOIs by the wide Russian editorial audience I receive a number of applications for Crossref services daily and provide the applicants with the necessary information. Since 2018 Crossref Ambassador in Russia.

Максим Митрофанов. В настоящее время Руководитель партнерских программ в Некоммерческом партнерстве Национальный электронно-информационный консорциум (НП НЭИКОН). Основная задача НЭИКОН -- предоставление полного спектра услуг для научных организаций России -- вузов, университетов, библиотек, издательств научной литературы, это и помощь в издании научной периодики, обеспечение подпиской на международные ресурсы, консультации и прочее. Родился в Москве 8 апреля 1977 г. Закончил Московский государственный институт международных отношений после чего девять лет работал в МИД России, включая различные позиции в Посольствах России в Канаде и Гане.

С 2007 г. работал в крупнейшей российской выставочной компании Экспоцентр. Пришел в НЭИКОН в 2014 г. Помимо основной работы с 2015 г. являюсь представителем и редактором DOAJ в России. В 2014 г. в рамках НЭИКОН одним из первых в России начал пропагандировать использование цифровых идентификаторов для научного контента, пояснять правила и технологии использования doi на встречах, в ходе конференций и семинаров.  C 2018 г. являюсь представителем Crossref в России.

### Russia - Angela P. Maltseva (currently inactive)
{{% imagewrap left %}} <img src="/images/ambassadors/Maltseva-Angela.jpg" alt="Angela Maltseva" width="200px" class="img-responsive" /> {{% /imagewrap %}}

As Doctor of Philosophy, Associate Professor, Chief Researcher and Professor of the Department of Philosophy and Culturology, Angela Maltseva conducts lectures and seminars in Political Science, Sociology, Sociology of Trust, Modern Political Structure of Western countries, History and Philosophy of Science. She has been Editor-In-Chief of the scientific journal "Volga Region Pedagogical Search" since 2017. The journal is interested in the ways and means of creating trusted environments, the social and educational influences on country development, citizen well-being and dignity, and articles about the role of educational institutions and children-adult communities in the accumulation of social capital. Since 2018 her University (UlSPU named after I.N. Ulyanov) is the first Crossref Sponsor in the Volga region.

Мальцева Анжела Петровна. Будучи доктором философских наук и главным научным сотрудником, Мальцева А.П. в настоящее время читает лекции по социологии, политологии, социологии доверия, современному политическому устройству стран Запада, истории и философии науки. С 2017 года Анжела Петровна является главным редактором научного журнала «Поволжский педагогический поиск», публикующего материалы о доверительных средах и путях их создания, о роли образовательных институтов и взросло-детских сообществ в накоплении социального капитала, о влиянии социальных и образовательных институтов на благополучие граждан, чувство их собственного достоинства. C 2018 года УлГПУ им. И.Н. Ульянова функционирует как первый в Поволжье представитель Crossref и спонсор организаций, осознающих всю важность цифровой идентификации своего контента.   

### Serbia -  Lazar Stošić
{{% imagewrap left %}} <img src="/images/ambassadors/Lazar.png" alt=" Lazar Stošić" width="200px" class="img-responsive" /> {{% /imagewrap %}}   

Prof. Dr. Lazar Stošić, is a university professor and the President of the Association for the Development of Science, Engineering and Education, in Serbia. He also works as a leading researcher at the Center for Scientific Competence of DSTU, Department of Scientific and Technical Information and Scientific Publications Don State Technical University, Russia.
Lazar has over ten years’ experience in scholarly publishing. His expertise includes editorial workflow management, conference organization, web technologies, web design, indexing, XML production, SEO, digital marketing, and new media technologies.

Lazar is Editor in Chief of the International Journal of Cognitive Research in Science, Engineering and Education, and member and reviewer of many international scientific journals, providing technical support for submission of metadata to Crossref and use of Crossref tools. His main aim as an ambassador is to help organizations and researchers understand the benefits of Crossref membership and the services that Crossref provides.   

Prof. dr Lazar Stošić, je univerzitetski profesor i predsednik Udruženja za razvoj nauke,
inženjerstva i obrazovanja u Srbiji. Takođe radi kao vodeći naučni istraživač u Centru za naučne
kompetencije DSTU, Odeljenje za naučne i tehničke informacije i naučne publikacije, Donskog
državnog tehničkog univerziteta, Rostov na Donu, Rusija. Lazar ima više od deset godina iskustva u naučnom izdavaštvu. Njegova stručnost obuhvata upravljanje procesom rada u izdavaštvu, organizovanje konferencija, veb tehnologije, veb dizajn, indeksiranje, XML produkciju, SEO, digitalni marketing, i nove medijske tehnologije.

Lazar je glavni i odgovorni urednik WoS i Scopus indeksiranog časopisa International
Journal of Cognitive Research in Science, Engineering and Education, član i recenzent mnogih
međunarodnih naučnih časopisa gde pruža tehničku podršku za dostavljanje metapodataka u
Crossref-u kao i za korišćenje drugih alata koje pruža Crossref. Njegov glavni cilj kao
ambassadora je da pomogne istraživačkim organizacijama i istraživačima da shvate prednosti
članstva u Crossref-u i usluga koje Crossref nudi.  


### Ukraine - Anna Danilova
{{% imagewrap left %}} <img src="/images/ambassadors/Anna-Danilova.jpg" alt="Anna Danilova" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Anna Danilova, assistant to the Head of subscription agency "Ukrinformnauka", is a leading specialist of Publishing House "Academperiodyka", founded under the National Academy of Sciences (NAS) of Ukraine. Her primary focus is maintaining online platforms for the scientific journals of NAS of Ukraine; since 2014 she has been providing technical support for submission of the scientific publication metadata to Crossref. Anna takes active part in the organization and holding of conferences, seminars and workshops throughout Ukraine, where all attendees have an opportunity to get detailed information about Digital Object Identifiers (DOIs), additional Crossref services and guidance about submission of scientific publications to the database. She is an author of a number of articles, educational and training materials.

Данілова Анна - заступник директора передплатного агентства «Укрінформнаука», провідний спеціаліст Видавничого дому «Академперіодика» Національної академії наук України. Сфера її діяльності включає в себе роботу з інтернет-ресурсами наукових періодичних видань НАН України, а з 2014 року – забезпечення технічної підтримки процесів депонування метаданих наукових статей і монографій в базу даних Crossref. Анна бере активну участь в підготовці й проведенні конференцій, семінарів та майстер-класів в різних містах України, на яких усі бажаючі мають можливість отримати детальну інформацію про цифрові ідентифікатори DOI, додаткові сервіси Crossref, підготовку ресурсів наукових видань до депонування. Окрім цього, вона є автором низки статей та навчально-методичних посібників.   



{{% /accordion %}} {{% /accordion-section %}}

{{% accordion %}} {{% accordion-section "Oceania" %}}

### Australia - Melroy Almeida
{{% imagewrap left %}} <img src="/images/ambassadors/melroy-almeida-sq2.jpg" alt="Melroy Almeida" height="200px" width="200px" class="img-responsive" /> {{% /imagewrap %}}

Melroy currently works at the Australian Access Federation (AAF) as their ORCID Technical Support Analyst. AAF is the consortium lead for the Australian ORCID Consortium and as part of his day to day work Melroy works with the Australian ORCID Consortium members on their ORCID implementations as well as assists them in planning their communication and engagement strategy. As part of his work with ORCID, Melroy occasionally gets questions about DOIs, metadata and discoverability. "My aim is to help research organisations and researchers understand the benefits of PIDs, why it is needed and how it helps within the scholarly research lifecycle". In addition to English, Melroy also speaks Hindi and Marathi.
In his spare time after work and family commitments, Melroy can be found playing/coaching football (soccer) or sitting on the couch reading a good book.

{{% /accordion %}} {{% /accordion-section %}}  


Country groupings are based on the geographic regions defined under the [Standard Country or Area Codes for Statistical Use (known as M49)](https://unstats.un.org/unsd/methodology/m49) of the United Nations Statistics Division. The assignment of countries or areas to specific groupings does not imply any assumption regarding political or any other affiliation of countries or territories.   


## Apply to become an ambassador

If you are interested in finding out more about the Ambassador Program and working with us please fill out the [application form](/community/ambassadors/) to give us a little information about yourself. We'll then get back to you to follow-up and discuss your plans and ideas.

<a href="#top">Back to top</a>

---

Please contact our [outreach team](mailto:feedback@crossref.org) with any questions.