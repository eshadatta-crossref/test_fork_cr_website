+++
title = "Thank you for completing the Metadata Plus subscriber form"
date = "2019-07-10"
type = "join-thank-you"
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "apply-retrieve-thank-you"
x-version = "0.0.0"

+++

## Thanks for completing the Metadata Plus subscriber form

### Here's what happens next:

We'll send you a copy of the Metadata Plus service agreement using a service called "HelloSign". Once the agreement is signed and countersigned , we'll send you a pro-rated invoice for your subscriber fees for the remainder of this current calendar year.

Once the invoice is paid, we'll provide you with an access token and other information to get you started.


---
Please contact our [Metadata Plus support](mailto:plus@crossref.org) team if you have any questions in the meantime.