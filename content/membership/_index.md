+++
title = "Become a member"
date = "2021-05-10"
author = "Amanda Bartell"
draft = false
image = "/images/banner-images/join-hello-sand.jpg"
maskcolor = "crossref-blue"
rank = 5
aliases = [ "/01company/join_crossref.html", "/02publishers/00success.html", "/02publishers/22request_mem.html", "/02publishers/22request_mem_test.html", "/join_crossref.html", "/education/become-a-member", "/education/become-a-member/", "/education/become-a-member/what-it-means-to-be-a-member/", "/education/become-a-member/what-it-means-to-be-a-member", "/education/become-a-member/join-as-a-member", "/education/become-a-member/join-as-a-member/", "/education/metadata-stewardship/understanding-your-member-obligations", "/education/metadata-stewardship/understanding-your-member-obligations/", "/documentation/metadata-stewardship/understanding-your-member-obligations", "/documentation/metadata-stewardship/understanding-your-member-obligations/",]
x-version = "0.0.0"

[menu.main]
parent = "Get involved"
weight = 5

+++

You need to be a member of Crossref in order to get a DOI prefix so you can create Crossref DOIs and register content. You don't need to be a member to use others' metadata; head over to read more about our open [metadata retrieval](/services/metadata-retrieval) tools.

Membership allows you to connect your content with a global network of online scholarly research, currently over 17,000 other organizational members from 140 countries. It's so much more than just getting a DOI.

## Are you eligible?

{{< snippet "/_snippet-sources/eligibility.md" >}}


<a id=‘member-obligations’ href=‘#member-obligations’></a>
## Member obligations and benefits

{{< snippet "/_snippet-sources/benefits-obligations.md" >}}


## How much does it cost?

### Independent membership

Most members pay an [annual membership fee](/fees/#annual-membership-fees/), which is tiered depending on your publishing revenue/expenses. The tiers start at US$275 and are the same for both commercial and not for profit organizations. A pro-rated membership fee for the remainder of the current year is due before your account can be activated, meaning you’ll receive an invoice soon after applying, and then each and every subsequent year in January. You are responsible for paying these annual membership fees each year unless you actively cancel your membership.

There are also one-off fees for each item you register with us (but no further charges to update the metadata associated with these records once they are registered). We send out quarterly content registration invoices. [Content registration fees](/fees/#content-registration-fees) vary depending on content type and whether the content is current or backfile, but as a guide, the cost to register a single, current article is US$1.

Organizations located in the least economically-advantaged countries in the world do not pay an annual membership fee and do not pay content registration fees - find out more on our [Global Equitable Membership (GEM) program](/gem) page.

If you have further questions about billing, do visit our [fees page](/fees) and our [billing FAQs page](/members-area/all-about-invoicing-and-payment/).

### Membership via a Sponsor

We know that cost and technical capabilities can be barriers to participation, so we offer a sponsors program. Members who join via a sponsor have the same benefits and obligations as other members, but they have someone to support their membership. Sponsors pay one membership fee for all the members that they work with, and they also pay content registration fees on behalf of their members. They also provide technical and local language support for their members.

Sponsors are able to charge for their services, so it's important to discuss your agreement with your sponsor before joining. If a member is eligible for the [Global Equitable Membership (GEM) program](/gem), we do not charge their sponsors membership fees or content registration fees, but again, the sponsor is able to charge for their services. [Find out more](/membership/about-sponsors) about working with a sponsor.

## Ready to apply?

{{% row %}}
{{% column %}}

### Option 1: [Apply as an independent member](/membership/_apply)

{{% /column %}}

{{% column %}}

### Option 2: [Find a Sponsor to join through](/membership/about-sponsors)

{{% /column %}}
{{% /row %}}


---

If you have questions please consult our forum at [community.crossref.org](https://community.crossref.org) or [open a ticket with our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) where we'll reply within a few days.