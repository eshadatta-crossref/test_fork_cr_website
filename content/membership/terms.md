+++
title = "Membership terms"
date = "2018-09-14"
draft = false
author = "Ginny Hendricks"
Rank = 5
aliases = [ "/member-obligations", "/02publishers/59pub_rules.html",]
x-version = "0.0.0"

[menu.main]
parent = "Become a member"
weight = 3

+++

{{< member-terms >}}

---

If you would like to [apply to join](/membership) please visit our membership page which describes the obligations and leads to an application form. Please contact our [membership specialist](mailto:member@crossref.org) with any questions.