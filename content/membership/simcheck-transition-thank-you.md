+++
title = "Thank you for your request to upgrade"
date = "2019-03-28"
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "simcheck-transition-thank-you"
x-version = "0.0.0"

+++

## Thanks for your request to upgrade

We'll check your details and make sure that you still meet the Similarity Check eligibility criteria.

We'll then pass your request over to the team at Turnitin. They'll triple check that they can still index 90% of your content. If there are any problems, they will contact your Similarity Check technical contact. If there are no problems, they will send over login details for v2 to your Similarity Check editorial contact.

---