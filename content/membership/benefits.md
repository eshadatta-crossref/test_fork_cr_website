+++
title = "Member benefits"
date = "2021-04-30"
draft = false
author = "Ginny Hendricks"
Rank = 5
aliases = [ "/education/why-crossref", "/education/why-crossref/", "/education/become-a-member/why-choose-crossref", "/education/become-a-member/why-choose-crossref/", "/education/why-crossref/the-problem-crossref-is-here-to-solve/", "/education/why-crossref/the-problem-crossref-is-here-to-solve",]
x-version = "0.0.0"

[menu.main]
parent = "Become a member"
weight = 1

+++

You need to be a member of Crossref in order to get a DOI prefix so you can create Crossref DOIs and register your content with us. This means you can create a persistent identifier for each citeable object that you publish, and register metadata about them that are then freely and openly shared with hundreds of organizations in the scholarly ecosystem, making your content more discoverable. You'll be able to vote for (and stand for) our board, giving you a voice in Crossref governance. And you’ll join a community of publishers who are all linking to each other persistently through their references.

Here's more information about Crossref services:

* [Content Registration](/services/content-registration/): Crossref metadata specifically enables collection of essential information surrounding research outputs in a standard way across members: abstracts, references, funding data, ORCID iDs, license information, clinical trial numbers, and full-text links. That metadata can be registered along with DOI links for all your articles, books, research grants, preprints, and more. This metadata is then openly shared with the scholarly ecosystem.
* [Reference linking](/services/reference-linking/): Joining us means that you’re entering into a reciprocal linking agreement with  over 17,000 other Crossref members registering over {{< total-results "/works?rows=0" >}} research objects from 140 countries around the world. Using DOIs to your reference lists is an obligation of membership and means that you are linking your content with the growing network of scholarly research. That collective responsibility is a key benefit of membership.
* [Cited-by](/services/cited-by/) provides members with a way to look at Crossref metadata to find out how your publications have been cited by others and share that information with your readers and authors. Most citation data is also available openly via our public APIs.
* [Similarity Check](/services/similarity-check/): Our agreement with Turnitin offers our members reduced-rate access to iThenticate, a tool to check submitted or accepted works against the full-text of tens of millions of published research outputs. It's a critical part of editorial integrity.
* [Crossmark](/services/crossmark/): Crossmark gives members the ability to add a button to their pages and PDFs to show if it’s been updated, corrected, or retracted. It conveys that you are committed to maintaining the provenance and integrity of the work as well as giving readers vital signals (metadata) to assess whether they can trust the information.
* [Metadata retrieval](/services/metadata-retrieval/): All of the metadata that Crossref collects helps our members’ records be more discoverable. We make it available in a variety of formats (including an API) so that anyone can come to one place to get information from our thousands of diverse members. Information about your publications is being shared by and used in search engines, collaborative editing and authoring tools, discovery platforms, library databases and many, many other places.
* [Event Data](/services/event-data): This is a new service aimed at tracking and providing evidence of what happens to research objects outside of traditional scholarly tracking. News mentions, policy documents, social shares, and more, can be retrieved by anyone to interpret and the data is auditable and portable.

We work closely with [other agencies of the DOI Foundation](https://www.doi.org/registration_agencies.html) many of whom work with scholarly information for specific regions of the world, like mEDRA, JaLC, CNKI, KISTI, et al. Like us, DataCite works globally; read about the benefits of [DataCite](/community/datacite) alongside Crossref.

Providing robust, accurate metadata helps make your content more discoverable. You can easily track what metadata you have registered by visiting our [Participation Reports](https://www.crossref.org/members/prep/) and entering your organization name. These reports give a clear picture for anyone to see the metadata Crossref has.

We’re committed to maintaining the Crossref infrastructure and building on it in a sustainable way for everyone in the scholarly community, improving existing tools and services and building new ones as needs arise. All the while consulting and collaborating where we can.

Please see our [strategic roadmap](/strategy) for what we're working on and our specific [benefits for different constituencies](/community/working-for-you). In 2020, our board adopted the [Principles of Open Scholarly Infrastructure (POSI)](http://openscholarlyinfrastructure.org/) and we published [a self-assessment](/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure/) to benchmark where we are and we plan to track the progress of our commitments. We invite the community to hold us accountable to these openness and sustainability goals.
---
You can contact our [membership specialist](mailto:member@crossref.org) with any questions or to get set up, or you can get in touch with our [technical support specialists](mailto:support@crossref.org) for any technical or troubleshooting questions.