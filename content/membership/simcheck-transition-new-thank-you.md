+++
title = "Thank you for your application"
date = "2021-11-03"
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "simcheck-transition-new-thank-you"
x-version = "0.0.0"

+++

## Thanks for applying for Similarity Check

Once we've confirmed that you meet the obligations, we'll work with the team at Turnitin to check that they can index your content. If they have any problems, they'll contact the technical contact you have just supplied. Once you're all set up, they'll provide you with your login details for the iThenticate system, and we'll help you get yourself set up and underway.

---