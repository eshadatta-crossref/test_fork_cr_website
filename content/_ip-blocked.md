+++
title = "We've had to block your IP address :-("
date = "2019-02-14"
author = "Geoffrey Bilder"
Weight = 15
rank = 2
x-version = "0.0.0"

+++

Hello. We're delighted that you are using our REST API and we hope you find it useful.

Unfortunately, your usage also seems to be causing some problems with the API, which, in turn, is preventing other users from accessing it as well.

As such, we have temporarily blocked your access. We'll happily lift the block if you contact us at [support@crossref.org](mailto:support@crossref.org).

We don't like having to block IP addresses and we’d be happy to help you optimize your system to make the best, unobtrusive use of our API.