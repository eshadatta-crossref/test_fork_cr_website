---
x-version: 0.0.0
---

Members of Crossref can participate in Similarity Check. The service fee is 20% of your annual membership fee and is included in your annual membership fee invoice.

|Total annual revenue/expenses |Crossref annual membership fee|Similarity Check annual subscription fee|
| :------| :-----------:| :-----------:|
|<USD 1 million|USD 275|USD 55|
|USD 1 million - USD 5 million|USD 550|USD 110|
|USD 5 million - USD 10 million|USD 1,650| USD 330|
|USD 10 million - USD 25 million|USD 3,900|USD 780|
|USD 25 million - USD 50 million|USD 8,300|USD 1,660|
|USD 50 million - USD 100 million|USD 14,000|USD 2,800|
|USD 100 million - USD 200 million|USD 22,000|USD 4,400|
|USD 200 million - USD 500 million|USD 33,000|USD 6,600|
|>USD 500 million|USD 50,000|USD 10,000|