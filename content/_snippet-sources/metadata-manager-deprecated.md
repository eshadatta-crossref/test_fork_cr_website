---
x-version: 0.0.0
---

_The Metadata Manager tool is in beta and contains many bugs. It’s being deprecated at the end of 2021. We recommend using the web deposit tool as an alternative, or the OJS plugin if your content is hosted on the OJS platform from PKP._