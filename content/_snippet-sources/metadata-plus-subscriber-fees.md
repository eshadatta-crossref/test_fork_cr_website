---
x-version: 0.0.0
---

If you want to get and use our metadata, you don’t need to be a Crossref member. We have various free public APIs available that you can use without contacting us. We also offer the Metadata Plus service for an annual subscription fee - this is offered to members and non-members alike. You'll receive an annual fee for this service each January - but if you initially sign up during the year, your fees will be prorated for the remainder of that calendar year.

The [Metadata Plus](/services/metadata-retrieval/metadata-plus/) service offers combined machine access to all available metadata in XML and JSON formats with additional features. This is also available openly via our Public APIs. The Plus service offers extra guarantees and features.

Please select your tier from the table below, using one of the following criteria:

1. For _commercial organizations_, the subscriber is considered to be the largest legal entity. The fee tier is selected based on the **total annual revenue** of all the divisions of your organization for all activities.
2. For _non-profit and government organizations_, the subscriber is considered to be the department(s) or agency(ies) making use of the service. The fee tier is selected based on whichever is the higher between your **total annual revenue** (including earned and fundraised, e.g. grants) or **annual operating expenses** (including personnel and non-personnel, e.g. occupancy, equipment, licenses, etc.).  

|Total annual revenue or income or funding | Annual fee |
|:------| :-----------:|
| <USD 500,000 | USD 550
| USD 500,001-USD 999,999 | USD 2,200
| USD 1 - 5 million | USD 3,300
| USD 5,000,001-USD 9,999,999 | USD 11,000
| USD 10 million-USD 25 million | USD 16,500
| >USD 25 million | USD 44,000

You don’t have to tell us your total revenue/income/funding, just the category you are in. [Let us know](mailto:plus@crossref.org) if you’re not sure.