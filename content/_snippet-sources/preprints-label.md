---
x-version: 0.0.0
---

## Publishing preprints is about more than simply getting a DOI<a id='00078' href='#00078'><i class='fas fa-link'></i></a>

Crossref can help you to clearly label content as a preprint using a preprint-specific schema. It’s not advisable to register preprints as data, components, articles, or anything else, because a preprint is not any of those things. Our service allows you to ensure the relationships between preprints and any eventual article are asserted in the metadata, and accurately readable by both humans and machines.