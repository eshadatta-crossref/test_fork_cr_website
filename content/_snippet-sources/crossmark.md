---
x-version: 0.0.0
---

> #### The Crossmark button gives readers quick and easy access to the current status of an item of content, including any corrections, retractions, or updates to that record.

Crossmark provides a cross-platform way for readers to quickly discover the status of a research output along with additional metadata related to the editorial process. Crucially, the Crossmark button can also be embedded in PDFs, which means that members have a way of alerting readers to changes months or even years after it’s been downloaded. 
Crossmark provides a cross-platform way for readers to quickly discover the status of a research output along with additional metadata related to the editorial process. Crucially, the Crossmark button can also be embedded in PDFs, which means that members have a way of alerting readers to changes months or even years after it’s been downloaded. 

Research doesn’t stand still: even after publication, articles can be updated with supplementary data or corrections. It’s important to know if the content being cited has been updated, corrected, or retracted. Crossmark makes this information more visible to readers. With one click, you can see if content has changed, and access valuable additional metadata provided by the member, such as key publication dates (submission, revision, acceptance), plagiarism screening status, and information about licenses, handling editors, and peer review.

Crossmark lets readers know when a substantial change affecting the citation or interpretation has occurred, and that the member has updated the metadata record to reflect the new status.

Watch the introductory Crossmark animation in your language:
            <td align="center"><script src="https://fast.wistia.com/embed/medias/6mws4bpweh.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_6mws4bpweh popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/pou05f1b81.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_pou05f1b81 popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/gnx30nhagz.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_gnx30nhagz popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/82ugp0bnq8.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_82ugp0bnq8 popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/wvdp10rr8r.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_wvdp10rr8r popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/sfgh4bolbk.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_sfgh4bolbk popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/xxaibvw4zb.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_xxaibvw4zb popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">العربية</a></span>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/glm0em1z8v.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_glm0em1z8v popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">bahasa Indonesia</a></span></td>; or English below.

<script src="//fast.wistia.com/embed/medias/q4ahujl6u3.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_q4ahujl6u3 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## Benefits of Crossmark<a id='00321' href='#00321'><i class='fas fa-link'></i></a>

* Members can report updates to readers and showcase additional metadata.
* Researchers and librarians can easily see the changes to the content they are reading, which licenses apply to the content, see linked clinical trials, and more.
* Anyone can access metadata associated with Crossmark through our [REST API](https://api.crossref.org), providing a myriad of opportunities for integration with other systems and analysis of changes to the scholarly record.

## How Crossmark works<a id='00323' href='#00323'><i class='fas fa-link'></i></a>

Members place the Crossmark button close to the title of an item on their web pages and in PDFs. They commit to informing us if there is an update such as a correction or retraction, as well as optionally providing additional metadata about editorial procedures and practices. 

The Crossmark button can be added to any platform that links to the content using the DOI.

<figure><img src='/images/documentation/Crossmark-check-for-updates.png' alt='Crossmark button' title='' width='50%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image29">Show image</button>
<div id="image29" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Crossmark-check-for-updates.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

While members who implement Crossmark provide links to update policies and commit themselves to accurately reporting updates, the presence of Crossmark itself is not a guarantee. However, it allows the community to more easily verify how members are updating their content.

If you use Crossmark, the Crossmark button must be applied to all of your new content, not just content is updated. Selective implementation means that a reader, such as a research or librarian, who downloaded a PDF version before the update would have no way to know that it has been updated. We also encourage you to implement Crossmark for backfile content, although doing so is optional. At least, we encourage you to do so for backfile content that has been updated.

## Obligations for Crossmark<a id='00325' href='#00325'><i class='fas fa-link'></i></a>

Any member can provide update metadata and we strongly encourage you to do so, in addition to registering an update policy. If you are a member who implements the Crossmark button, you must:

* Maintain your content and promptly register any updates.
* Include the Crossmark button on all digital formats (HTML, PDF, ePub).
* Implement Crossmark using the script provided by us.
* Not alter the Crossmark button in any way other than adjusting its size.

Implementing the Crossmark button involves technical changes to your website and production processes. Check that you have the necessarily expertise to implement these before you start. If not, you can start to deliver update metadata and implement the Crossmark button at a later point.

Any organisation can also implement the Crossmark button on pages where they display content. If you do so, you must follow the guidelines above, except for the first point if you are not reponsible for the content.

There are no additional fees to participate in Crossmark.

## How to participate in Crossmark<a id='00324' href='#00324'><i class='fas fa-link'></i></a>

There are several steps to fully implementing Crossmark for members and these can be implemented incrementally over time:

First, devise an update policy and link it to all of your metadata records. The policy statement is a page on their website, which explains their participation in the service, their commitment to maintaining versions of any record that displays the Crossmark icon, and their policies on corrections, retractions, withdrawals, and other updates.

Second, add relevant metadata about editorial processes, procedures, and updates to metadata records. This can include information about key dates in the editorial process, peer review and editor decisions, clinical trials and licensing. 

Third, publish corrections and retractions for works where necessary. [Guidance on updates](/documentation/crossmark/version-control-corrections-and-retractions/) is available.

Third, implement the Crossmark button online and in PDFs. This is done by including a snippet of code containing the DOI of the related content. When the button is clicked a dialog box appears that displays information taken from the metadata record.
Third, implement the Crossmark button online and in PDFs. This is done by including a snippet of code containing the DOI of the related content. When the button is clicked a dialog box appears that displays information taken from the metadata record.

Learn more about [participating in Crossmark](/documentation/crossmark/participating-in-crossmark).

To see which Crossref members are registering Crossmark information, visit [Participation Reports](https://www.crossref.org/members/prep/). These reports give a clear picture for anyone to see the metadata Crossref has including Crossmark data.

Learn more about [version control, corrections, and retractions](/documentation/crossmark/version-control-corrections-and-retractions).

<figure><img src='/images/documentation/Infographic-Crossmark.png' alt='Crossmark infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image30">Show image</button>
<div id="image30" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Crossmark.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


[Download the Crossmark factsheet](/pdfs/about-crossmark.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).