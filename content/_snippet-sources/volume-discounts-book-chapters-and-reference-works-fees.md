---
x-version: 0.0.0
---

If you're depositing a lot of chapters or reference works for the same title in the same quarter year, the following discounts apply:

|Total number of registered DOIs per title per quarter | Registration fee per record (current)| Registration fee per record (backfile)|
| :------| :-----------:|:-----------:|
|0-250|USD 0.25| USD 0.15|
|251-1,000|USD 0.15| USD 0.15|
|1,001-10,000|USD 0.12|USD 0.12|
|10,001-100,000|USD 0.06|USD 0.06|
|100,001 and up|USD 0.02|USD 0.02|


The higher tiers are for encyclopaedias, in case you're wondering how a single title could possibly have 100,000 chapters!