---
x-version: 0.0.0
---

## Benefits of our custom support for preprints<a id='00079' href='#00079'><i class='fas fa-link'></i></a>

* Persistent identifiers for preprints to ensure successful links to the scholarly record over the course of time
* The preprint-specific metadata we ask for reflects researcher workflows from preprint to formal publication  
* Support for preprint versioning by providing relationships between metadata for different iterations of the same document.
* Notification of links between preprints and formal publications that may follow (such as journal articles, monographs)
* [Reference linking](/documentation/reference-linking/) for preprints, connecting up the scholarly record to associated literature
* [Auto-update of ORCID records](/community/orcid/) to ensure that preprint contributors are acknowledged for their work
* Preprints [include funding data](/blog/a-healthy-infrastructure-needs-healthy-funding-data/) so people can report research contributions based on funder and grant identification
* Discoverability: we make the metadata available for machine and human access, across multiple interfaces (including our [REST API](/documentation/retrieve-metadata/rest-api), [OAI-PMH](/documentation/retrieve-metadata/oai-pmh), and [Metadata Search](https://search.crossref.org/).