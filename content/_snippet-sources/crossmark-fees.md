---
x-version: 0.0.0
---

### Crossmark fees  

Crossmark allows members to register status updates to your published content such as corrections, retractions, and errata. It also enables you to put a button on both your html articles as well as on PDFs so readers can always know its status whether current, updated, or retracted. There is no annual service fee for Crossmark, but up to the end of 2019 there were fees for each record that was updated or corrected, and these were included in the quarterly content registration invoices.  

|Crossmark fee|  Current records  | Backfile records|
| :------------| :---------:|:------:|
|Status update fee |  USD 0.20 |  USD 0.02  

From January 2020 onwards these fees have been removed, so the last time members will see these fees will be on their Q4 2019 content registration invoice.