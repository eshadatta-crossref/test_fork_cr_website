---
x-version: 0.0.0
---

> #### Content Registration allows members to register and update metadata via machine or human interfaces.

When you join Crossref as a member you are issued a DOI prefix. You combine this with a suffix of your choice to create a DOI, which becomes active once registered with Crossref. Content Registration allows members to register a DOI and deposit or update its associated metadata, via machine or human interfaces.

## Benefits of content registration<a id='00534' href='#00534'><i class='fas fa-link'></i></a>

Academic and professional research travels further if it’s linked to the millions of other published papers. Crossref members register content with us to let the world know it exists, instead of creating thousands of bilateral agreements.

Members send information called *metadata* to us. Metadata includes fields like dates, titles, authors, affiliations, funders, and online location. Each metadata record includes a [persistent identifier](/documentation/research-nexus/) called a digital object identifier (DOI) that stays with the work even if it moves websites. Though the DOI doesn't change, its [associated metadata is kept up-to-date](/documentation/register-maintain-records/maintaining-your-metadata/) by the owner of the record.

Richer metadata makes content useful and easier to find. Through Crossref, members are distributing their metadata downstream, making it available to numerous systems and organizations that together help credit and cite the work, report impact of funding, track outcomes and activity, and more.

Members maintain and update metadata long-term, telling us if content moves to a new website, and they include more information as time goes on. This means that there is a growing chance that content is found, cited, linked to, included in assessment, and used by other researchers.

[Participation Reports](/documentation/reports/participation-reports/) give a clear picture for anyone to see the metadata Crossref has. See for yourself where the gaps are, and what our members could improve upon. Understand best practice through seeing what others are doing, and learn how to level-up.

> This is Crossref infrastructure. You can’t see infrastructure, yet research—and researchers all over the world—rely on it.

<figure><img src='/images/documentation/Infographic-Content-registration.png' alt='Content Registration infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image1">Show image</button>
<div id="image1" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Content-registration.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

[Download the content registration factsheet](/pdfs/about-content-registration.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## How content registration works<a id='00535' href='#00535'><i class='fas fa-link'></i></a>

To register content with Crossref, you need to be a member. You’ll use one of our [content registration methods](/documentation/member-setup/choose-content-registration-method/) to give us metadata about your content. Note that you don’t send us the content itself - you create a metadata record that links persistently (via a persistent identifier) to the content on your site or hosting platform. Learn more about [metadata](/documentation/metadata), [constructing your DOIs](/documentation/member-setup/constructing-your-dois/), and [ways to register your content](/documentation/member-setup/choose-content-registration-method/).

You should assign Crossref DOIs to and register content for anything that is likely to be cited in the scholarly literature.

No matter whether you [register content using one of our helper tools, or creating your own metadata files](/documentation/member-setup/choose-content-registration-method/), all metadata deposited with Crossref is submitted as XML, and formatted using our [metadata deposit schema section](/documentation/content-registration/metadata-deposit-schema/). Explore our [XML sample files](/xml-samples/) to help you create your own XML.

{{< snippet "/_snippet-sources/content-types.md" >}}

## Obligations and fees for content registration<a id='00536' href='#00536'><i class='fas fa-link'></i></a>

You pay a one-time [content registration fee](/fees#content-registration-fees) for each content item you register with us. content registration fees are different for different types of content and sometimes include volume discounts for large batches or backfile material. You don’t pay to update an existing metadata record. It’s an [obligation of membership](/documentation/metadata-stewardship/understanding-your-member-obligations/) that you maintain your metadata for the long term, including updating any URLs that change. In addition, we warmly encourage you to [correct and add to your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/), and there is no charge for redepositing (updating) existing metadata. Learn more about [maintaining your metadata](/documentation/register-maintain-records/maintaining-your-metadata/), and [managing existing DOIs](/documentation/register-maintain-records/maintaining-your-metadata).

Your content registration fees are billed quarterly in arrears. This means you’ll usually receive a bill at the beginning of each quarter for the content you registered in the previous quarter. The only exception is if you’ve only registered a small number of DOIs.