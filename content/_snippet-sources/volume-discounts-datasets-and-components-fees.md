---
x-version: 0.0.0
---

If you're registering a lot of components or datasets for a single title in the same quarter year, the following discounts apply. There is no difference in price between current and backfile content for datasets and components.

|Total number of registered DOIs per title per quarter | Registration fee per record (current and backfile)|
| :------| :-----------:|
|1-10,000|USD 0.06|
|10,001-100,000|USD 0.03|
|100,001-1,000,000|USD 0.02|
|1,000,001-10,000,000|USD 0.01|
|10,000,001 and up|USD 0.005|