---
x-version: 0.0.0
---

We'll take a look at your application within two working days. We may need to come back to you with a couple of questions. Where applicable, we then send you a pro-rated membership order (this is basically an invoice) for the remaining months of your first year of membership.  Once our billing team confirms that this has been paid, we’ll send you your new Crossref DOI prefix within two working days. We'll also help you set up your Crossref account credentials. Once these are created, you’ll be able to start registering your content straight away.