---
x-version: 0.0.0
---

We usually send out invoices by email to your named billing contact. The email will include full payment details including account numbers, but here are the basic payment methods. Please note we can only accept payment in US dollars.

#### 1 Credit or debit card payment using our payment portal
We recommend using our payment portal where possible, as other payment methods incur fees. We send out payment portal credentials to the billing contact on each new member account. If you don't already have credentials for our payment portal, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360003343631). Please note: your username and password for the payment portal is different from the [Crossref account credentials](/documentation/member-setup/account-credentials/) you use to register your content with us.

The portal accepts most major credit cards, plus debit cards with a VISA or Mastercard symbol. The credit/debit card needs to be able to make international payments in US$.

Members based in the US can also make ACH payments through the payment portal.

You can find the answer to [frequently asked questions about the payment portal](https://www.crossref.org/members-area/all-about-invoicing-and-payment/#10123) here.

#### 2 Other payment methods
If you aren't able to pay using our payment portal, we offer two other payment methods.

**Bank transfers**
* We accept wire transfers from most banks. If your bank is outside the US you'll need to add US$ 35 for a wire transfer fee.
* We can also accept Automated Clearing House (ACH) payments from US banks. There are no extra fees for ACH payments from US banks.

**Checks from banks**
We prefer checks drawn on US banks. If you are sending payment from a US$ bank account outside the US, please add US$ 50 to your payment to cover processing fees. Please mail checks, with a copy of the invoice or with the invoice number referenced on the check, to:

`Publishers International Linking Association, Inc.
dba Crossref  
50 Salem St. Building A
Suite 304
Lynnfield, MA 0194`.

***************************************

If you have not been receiving invoices, please [contact our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) to update the billing email address for your account. We recommend you give us a generic departmental email address such as `accounts@company.org` to avoid emails bouncing back from the accounts of colleagues who have left your organization. Thank you!