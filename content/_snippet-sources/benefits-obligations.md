---
x-version: 0.0.0
---

When you join as a member, you'll commit to the [Crossref membership terms](/membership/terms/), giving you access to all the [benefits of Crossref membership](/membership/benefits).

You'll join a community of publishers who are all linking to each other persistently through their references. We provide you with a DOI (Digital Object Identifier) prefix which will form the stem of links to all your metadata records. You create the suffixes of your DOIs as you [register](/services/content-registration) your DOIs and metadata in our system. The metadata you register with us will be [shared with hundreds of organizations](#00544) in the scholarly ecosystem, helping to make your content more discoverable and benefiting the entire scholarly community. You'll be able to vote for (and stand for) our board, giving you a voice in Crossref governance. You'll also be able to take advantage of our services (content registration, the Funder Registry, Similarity Check, Cited-by and Crossmark). And, of course, you're able to create a persistent identifier for each citable object that you publish.

But it also means that you have obligations to your fellow members and the wider scholarly community. Joining Crossref is much more than just getting a DOI - you're committing to a long term relationship with your content and metadata, and to the rest of the community.

When you accept the terms, you commit to these obligations:

### 1. Deposit metadata and create DOI links
You'll need to do this for all content published after joining. If you host and publish your journals using the OJS platform from the Public Knowledge Project, use the dedicated [OJS plugins](/documentation/member-setup/ojs-plugin/). Otherwise, registering your content can be done by [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) (perhaps using a vendor) or, if you can't supply XML, simply use our online [web deposit form](/documentation/member-setup/web-deposit-form/).

### 2. Register only what you have the legal rights to
Our terms (3 (c)) stipulate that "The Member will not deposit or register Metadata for any Content for which the Member does not have legal rights to do so".

### 3. Maintain and update your metadata and landing pages for the long term
You'll need to [maintain and update your metadata](/documentation/register-maintain-records/maintaining-your-metadata), including URLs if your content moves or changes, and adding rich metadata as you collect more. [Metadata distribution](/documentation/metadata/) through Crossref benefits you as well as the whole community.  

### 4. Follow the Crossref DOI [display guidelines](https://doi.org/10.13003/5jchdy)
You'll need to:
- Ensure that each DOI resolves to a unique [landing page](/documentation/member-setup/creating-a-landing-page/).
- Use the DOI as the permanent link to the page.
- Always display DOIs on your landing pages, PDFs, and elsewhere, close to the bibliographic citation information.

### 5. Undertake Reference linking

You'll need to actively maximize persistent links with other Crossref members by linking out from your reference lists, databases, and tools, using DOIs. You can find other members' DOI links using our [reference linking tools](/documentation/reference-linking/how-do-i-create-reference-links).

### 6. Pay your invoices
As a not-for-profit membership organization, we are sustained by fees. We have a duty to remain sustainable and manage our finances in a responsible way. Financial sustainability means we can keep the organization afloat and keep our dedicated service to scholarly communications running, so it's important that members pay their fees on time.

Please read the full [membership terms](/membership/terms) before applying.