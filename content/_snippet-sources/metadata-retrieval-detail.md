---
x-version: 0.0.0
---

Watch the introductory animation in your language:
            <td align="center"><script src="https://fast.wistia.com/embed/medias/mi77qq6dxn.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_mi77qq6dxn popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/iln5f4eed9.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_iln5f4eed9 popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/vcaawcflk8.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_vcaawcflk8 popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/s6sjaqeswu.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_s6sjaqeswu popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/pd3oqapkfp.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_pd3oqapkfp popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/ll7dvkcyrs.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_ll7dvkcyrs popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/q8z3n279hk.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_q8z3n279hk popover=true popoverContent=link" style="display:inline"><a href="#">العربية</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/v11asfct0n.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_v11asfct0n popover=true popoverContent=link" style="display:inline"><a href="#">bahasa Indonesia</a></span></td>; or English below.


<script src="//fast.wistia.com/embed/medias/awn9su8tw4.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_awn9su8tw4 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## How metadata retrieval works<a id='00356' href='#00356'><i class='fas fa-link'></i></a>

To retrieve metadata from Crossref, you do not need to be a member. We have various options for retrieving metadata - some are more suitable for human use, and some for machine use.

There are [interfaces for people](/documentation/retrieve-metadata/#00358) (to look up small numbers of DOIs): [Metadata Search](https://search.crossref.org/), and [Simple Text Query](https://apps.crossref.org/SimpleTextQuery).

There are [APIs for machines](/documentation/retrieve-metadata/#00359) (to look up metadata records or DOIs in volume), including: [REST API](/documentation/retrieve-metadata/rest-api), [XML API](/documentation/retrieve-metadata/xml-api/), [OAI-PMH](/documentation/retrieve-metadata/oai-pmh), and [OpenURL](/documentation/retrieve-metadata/openurl); and our paid [Metadata Plus](/documentation/metadata-plus/) service.

Take a look at a list of some of the [organizations who rely on our REST API](/services/metadata-retrieval/user-stories/).

Here is a comparison of the metadata retrieval options. Please note that all interfaces include Crossref test prefixes: 10.13003, 10.13039, 10.18810, 10.32013, 10.50505, 10.5555, 10.88888.

| Feature / option | Metadata Search | Simple Text Query | REST API | XML API | OAI-PMH | OpenURL | Metadata Plus (OAI-PMH + REST API) |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Interface for people or machines? | People | People | People (low volume and occasional use) and machines | Machines | Machines | Machines | Machines |
| Output format | Text, JSON | Text | JSON | XML | XML | XML | JSON, XML |
| Suitable for citation matching? | Yes (low volume) | Yes | Yes | Yes | No | No | Yes |
| Supports volume downloads? | No | No | Yes | No | Yes | No | Yes |
| Suitable for usage type | Frequent and occasional | Frequent and occasional | Frequent and occasional | Frequent | Frequent | Frequent | Frequent and occasional |
| Free or cost? | Free | Free | Free and cost options | Free and cost options | Cost for full service, more options available | Free | Cost |
| Includes all available metadata? | In JSON only | DOIs only | Yes | Yes | Yes | [Bibliographic](/documentation/content-registration/descriptive-metadata/) only | Yes |
| Documentation | [Metadata Search](/documentation/retrieve-metadata#00358) | [Simple Text Query](/documentation/retrieve-metadata#00358) | [REST API](/documentation/retrieve-metadata/rest-api)| [XML API](/documentation/retrieve-metadata/xml-api/) | [OAI-PMH](/documentation/retrieve-metadata/oai-pmh) | [OpenURL](/documentation/retrieve-metadata/openurl) | [Metadata Plus](/documentation/metadata-plus/) ([OAI-PMH](/documentation/retrieve-metadata/oai-pmh) + [REST API](/documentation/retrieve-metadata/rest-api)) |

If you’d like to share a case study for how you use Crossref metadata, and be featured on our blog, please [contact us](/contact/).

## Using content negotiation<a id='00357' href='#00357'><i class='fas fa-link'></i></a>

The APIs listed here provide metadata in a variety of representations (also known as output formats). If you want to access our metadata in a particular representation (for example, RDF, BibTex, XML, CSL), you can use [content negotiation](/documentation/retrieve-metadata/content-negotiation) to retrieve the metadata for a DOI in the representation you want. Content negotiation is supported by a number of DOI registration agencies including Crossref, DataCite, and mEDRA.

## Obligations and fees for metadata retrieval<a id='00360' href='#00360'><i class='fas fa-link'></i></a>

It is important that members understand that metadata is used by other software and services in the Crossref community. We encourage members to submit as much metadata as possible so that our APIs can include and deliver rich contextual information about their content.

If you’re using the public REST API, it is optional but encouraged to include your email address in header requests as this puts your query into the "polite" pool which has priority processing. Learn more about our [REST API etiquette](https://github.com/CrossRef/rest-api-doc/#etiquette).

All of our metadata is freely available, but there is a [fee for our premium Metadata Plus service](/fees#metadata-plus-subscriber-fees).  

Crossref generally provides metadata without restriction; however, some abstracts contained in the metadata may be subject to copyright by publishers or authors.  

## How to participate - interfaces for people<a id='00358' href='#00358'><i class='fas fa-link'></i></a>

Crossref provides a number of user interfaces to access Crossref metadata. Some are general-purpose, and others are more specialized.

| Service name  | Description  |
|---|---|
| [Metadata Search](https://search.crossref.org)  | Metadata Search is our primary user interface for searching and filtering of our metadata. It can be used to [look up the DOI for a reference](https://search.crossref.org/?q=Clow+GD%2C+McKay+CP%2C+Simmons+Jr.+GM%2C+and+Wharton+RA%2C+Jr.+1988.+Climatological+observations+and+predicted+sublimation+rates+at+Lake+Hoare%2C+Antarctica.+Journal+of+Climate+1%3A715-728) or [a partial reference](https://search.crossref.org/?q=Renear+Palmer+Ontologies+) or [a set of references](https://search.crossref.org/references), to look up metadata for a content item, submit a query on an author’s name, or find retractions registered with us. It can also be used to search and filter a number of elements, including [funding data](https://search.crossref.org/?q=RSG-09-276-01-CSM), [ISSN](https://search.crossref.org/?q=1740-8776), [ORCID iDs](https://search.crossref.org/?q=0000-0001-5556-6616), and [more](https://search.crossref.org/help/search).  |
| [Simple Text Query](https://apps.crossref.org/SimpleTextQuery)  | Simple Text Query is a tool designed to allow anyone to look up DOIs for multiple references. As such it’s particularly useful for members who want to [link their references](/documentation/reference-linking). Members can even use this tool to add linked references to their metadata.  |

## How to participate - APIs for machines<a id='00359' href='#00359'><i class='fas fa-link'></i></a>

We have a number of APIs for accessing metadata. There is one general-purpose API and several specialized ones. The specialized APIs are designed for our members so that they can manage their metadata or they are APIs based on standards that are popular in the community.

|API name|Description|
|--- |--- |
|[REST API](/documentation/retrieve-metadata/rest-api) |The REST API outputs in JSON and enables sophisticated, flexible machine and programmatic access to search and filter our metadata. It can be used, for example, to [look up the metadata for a content item](https://api.crossref.org/v1/works/10.1088/0004-637X/722/2/971) or [submit a query on an author’s name](https://api.crossref.org/v1/works?query=allen+renear) or [find retractions registered with us](https://api.crossref.org/v1/works?filter=update-type:retraction). It also allows users to search and filter on a number of elements, including a [funder](https://api.crossref.org/v1/works?filter=funder:10.13039/100000001), or [all content items with ORCID iDs](https://api.crossref.org/v1/works?filter=has-orcid:true). The REST API is open to all and it is included in the [Metadata Plus](/documentation/metadata-plus) service.|
|[OpenURL](/documentation/retrieve-metadata/openurl)|This API lets you look up a Crossref DOI for a reference, using a standard that is popular in the library community, and particularly with link resolver services.|
|[OAI-PMH](/documentation/retrieve-metadata/oai-pmh) |This API outputs in XML and uses a standard popular in the library community to harvest metadata. The OAI-PMH API is optimized to return a list of results matching the query parameters (such as publication year). The OAI-PMH API is included in the [Metadata Plus](/documentation/metadata-plus/) service.|
|[XML API](/documentation/retrieve-metadata/xml-api/) |The XML API supports XML-formatted querying. The XML API is optimized to return the best fit DOI based on the metadata supplied in the query.|

<figure><img src='/images/documentation/Infographic-Metadata-retrieval.png' alt='Metadata retrieval infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image33">Show image</button>
<div id="image33" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Metadata-retrieval.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


[Download the metadata retrieval factsheet](/pdfs/about-metadata-retrieval.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## Looking up metadata and identifiers<a id='00043' href='#00043'><i class='fas fa-link'></i></a>

We support a range of tools and APIs to help you get metadata (and identifiers) out of our system. Some query interfaces will return only one match, and only if fairly strict requirements are met. These interfaces may be used to populate citations with persistent identifiers. Other interfaces will return a range of results and may be used to retrieve a variety of metadata records or match metadata when metadata, DOIs, or other identifiers (such as ORCID iD, ISSN, ISBN, funder identifier) are provided.

### User interfaces<a id='00361' href='#00361'><i class='fas fa-link'></i></a>

* [Metadata Search](http://search.crossref.org) - any results containing the entered search terms will be returned. Search by full citation, title (or fragments of a title), authors, ISSN, ORCID, DOI (to retrieve metadata) and [more](https://search.crossref.org/help/search).
* [Simple Text Query](https://apps.crossref.org/SimpleTextQuery) - cut-and-paste your reference list into the form and retrieve exact DOI matches.

### APIs<a id='00362' href='#00362'><i class='fas fa-link'></i></a>

* [REST API](/documentation/retrieve-metadata/rest-api) - a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API that supports a wide range of facets and filters. By default, results are returned in JSON, and returning results in XML is an option. This API is currently publicly available (no account or token required), but there is a paid [Metadata Plus](/documentation/metadata-plus/) service available on a token for those who require guaranteed service levels
* [XML API](/documentation/retrieve-metadata/xml-api/) - the XML API will return a DOI that best fits the metadata supplied in the query. This API is suitable for automated population of citations with DOIs as the results are accurate and do not need evaluation. This API is available to members, or by supplying an email address.
* [OpenURL](/documentation/retrieve-metadata/openurl) - used mostly by libraries but also available to members, or by providing an email address. Learn more about [OpenURL access](/documentation/retrieve-metadata/openurl#00375).
* [OAI-PMH](/documentation/retrieve-metadata/oai-pmh) - as well as a free public list option, we provide a subscription-only OAI-PMH interface that may be used to retrieve sets of metadata records (subscribers only)
* [GetResolvedRefs](/documentation/retrieve-metadata/retrieving-identifiers-for-deposited-references) - retrieve DOIs matched with deposited references (members only)
* [Deposit harvester](/documentation/retrieve-metadata/deposit-harvester) - retrieve DOIs and metadata for a given member (members only).