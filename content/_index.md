+++
date = "2023-01-04"
Title = "You are Crossref"
type = "index"
image = "/images/homepage-images/carpathian-mountains-ukraine.jpg"
Caption = "“Beautiful sunrise in Carpathian mountains, Ukraine” by Vlad Kiselov on Unsplash. Selection inspired by Yuliya Gorenko"
aliases = [ "/02publishers/18gallery.html",]
x-version = "0.0.0"

+++

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. <a rel="me" href="https://mastodon.online/@crossref" style="display:none">Mastodon</a>