+++
title = "Brand & logos"
date = "2018-11-23"
draft = false
author = "Rosa Clark"
parent = "About us"
rank = 3
weight = 10
aliases = [ "/06members/49logos.html",]
x-version = "0.0.0"

+++

A brand is not just a logo, or a name, or any other individual element. A brand is a promise. Crossref helps bring together people and metadata in pursuit of an excellent research communications system for all. That’s our promise. Which makes each and every one of you a brand ambassador. You are Crossref, and everything that it embodies.

## Our color palette

If you need to reference our colors or our font type, you can use the codes and guidelines below:  

{{% divwrap red-highlight %}} Pantone: Red 032 C; Hex #ef3340 {{% /divwrap %}}  
{{% divwrap blue-highlight %}} Pantone: 631 C; Hex #3eb1c8 {{% /divwrap %}}  
{{% divwrap lightgrey-highlight %}} Pantone: Tan 7527 C; Hex #d8d2c4 {{% /divwrap %}}  
{{% divwrap yellow-highlight %}} Pantone Yellow 123C; Hex #ffc72c {{% /divwrap %}}  
{{% divwrap darkgrey-highlight %}} Pantone Grey 445C; Hex $4f5858  {{% /divwrap %}}  

## Our brand guidelines  

<p align="center"><embed src="/pdfs/brand-guide-members-march-2016.pdf" width="550" height="425" type="application/pdf">  </p>

## Our typeface

<p align="center"><img src="/images/blog/brand-font.png" alt="Our typography" width="75%" />
</p>

## Logo use

We know that many of you—both members, and other users of metadata in the wider scholarly research community—like to reference us in your communications, promoting how you are contributing to scholarly infrastructure. To support you with this we have created a set of [Account Badges](/brand/badges/). If you currently use the Crossref logo on your website, you might like to replace it with your Member Type badge.

If you need to use our logo(s) please reference them (not download), using the html snippets below. Please copy the code exactly, so that even if we update the logos, you’ll automatically get the correct file.

We recommend .svg versions online at 200px wide for the sharpest display. Contact our [outreach team](mailto:feedback@crossref.org) if you’d like a version for printing or have any questions.


## Referencing our logos

| Crossref logos | Image link |
|---|---|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-logo-200.svg" width="200" height="130" alt="Crossref logo">|`<img src="https://assets.crossref.org/logo/crossref-logo-200.svg" width="200" height="130" alt="Crossref logo"/>`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-logo-landscape-200.svg" width="200" height="68" alt="Crossref logo">|`<img src="https://assets.crossref.org/logo/crossref-logo-landscape-200.svg" width="200" height="68" alt="Crossref logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-content-registration-logo-200.svg" width="200" height="110" alt="Crossref Content Registration logo">|`<img src="https://assets.crossref.org/logo/crossref-content-registration-logo-200.svg" width="200" height="110" alt="Crossref Content Registration logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-cited-by-logo-200.svg" width="200" height="82" alt="Crossref Cited-by logo">|`<img src="https://assets.crossref.org/logo/crossref-cited-by-logo-200.svg" width="200" height="82" alt="Crossref Cited-by logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-metadata-apis-200.svg" width="200" height="83" alt="Crossref Metadata APIs logo">|`<img src="https://assets.crossref.org/logo/crossref-metadata-apis-200.svg" width="200" height="83" alt="Crossref Metadata APIs logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-metadata-search-logo-200.svg" width="200" height="106" alt="Crossref Metadata Search logo">|`<img src="https://assets.crossref.org/logo/crossref-metadata-search-logo-200.svg" width="200" height="106" alt="Crossref Metadata Search logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-similarity-check-logo-200.svg" width="200" height="98" alt="Crossref Similarity Check logo">|`<img src="https://assets.crossref.org/logo/crossref-similarity-check-logo-200.svg" width="200" height="98" alt="Crossref Similarity Check logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-funder-registry-logo-200.svg" width="200" height="112" alt="Crossref Funder Registry logo">|`<img src="https://assets.crossref.org/logo/crossref-funder-registry-logo-200.svg" width="200" height="112" alt="Crossref Funder Registry logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-event-data-logo-200.svg" width="200" height="83" alt="Crossref Event Data logo">|`<img src="https://assets.crossref.org/logo/crossref-event-data-logo-200.svg" width="200" height="83" alt="Crossref Event Data logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-cited-by-logo-200.svg" width="200" height="82" alt="Crossref Cited-by logo">|`<img src="https://assets.crossref.org/logo/crossref-cited-by-logo-200.svg" width="200" height="82" alt="Crossref Cited-by logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/crossref-crossmark-logo-200.svg" width="200" height="82" alt="Crossref Crossmark logo">|`<img src="https://assets.crossref.org/logo/crossref-crossmark-logo-200.svg" width="200" height="82" alt="Crossref Crossmark logo">`|
|<img class="center-block" src="https://assets.crossref.org/logo/metadata-from-crossref-logo-200.svg" width="200" height="68" alt="Metadata from Crossref logo">|`<img src="https://assets.crossref.org/logo/metadata-from-crossref-logo-200.svg" width="200" height="68" alt="Metadata from Crossref logo">`|