+++
title = "Annual meetings archive"
date = "2020-09-01"
draft = false
author = "Rachael Lammey"
parent = "Crossref LIVE Annual"
rank = 2
weight = 6
x-version = "0.0.0"

+++

An archive of every annual Crossref meeting since 2002, now called *Crossref LIVE*. Jump to a year to review the programs and speakers, and link to some recordings and slides.

## [2022](#2022)            [2021](#2021)
## [2020](#2020)    [2019](#2019)   [2018](#2018)    [2017](#2017)   [2016](#2016)   
## [2015](#2015)   [2014](#2014)    [2013](#2013)    [2012](#2012)   [2011](#2011)   
## [2010](#2010)   [2009](#2009)    [2008](#2008)    [2007](#2007)   [2006](#2006)   
## [2005](#2005)   [2004](#2004)    [2003](#2003)    [2002](#2002)   [2001](#2001)  

<a id="2022"></a>

October 26 | Online | Twitter Hashtag: #CRLIVE22

Theme: The Research Nexus, the importance of relationships in metadata and community stories of building the research nexus together.   

Please check out the materials from LIVE22 below, and cite the outputs as `Crossref Annual Meeting LIVE22, Ocrober 26, 2022 retrieved [date], [https://doi.org/10.13003/i3t7l9ub7t]`:

- [YouTube recording](https://www.youtube.com/watch_popup?v=csy4mf8QNtY)  
- [Recording transcript](https://docs.google.com/document/d/1h2Pc0orQqnMJoNGx4E6W_Yhr1fiZaeZRqF5ItcjEjkc/preview)    
- [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1sPlXW8rg3jMN2IiBbihdAuDc_zacUvIR8vXSiHEbNN0/preview#gid=0)    
- [Google slides](https://docs.google.com/presentation/d/1CWoueRlXDBr764zDxw3docHDWosD8UCqlXzE9zZxyRI/preview#slide=id.g14cb5cdd81a_1_14) or [pdf slides](/pdfs/crossref-annual-meeting-live22-presentation-shared.pdf)
- [Twitter thread #CRLIVE22](https://twitter.com/CrossrefOrg/status/1585301047748222979)   
- [Posters from community guest speakers](https://community.crossref.org/c/live22-presentations/36)    

[Board election results](/board-and-governance/elections)  


<a id="2021"></a>

## 2021 annual meeting

9 November | Online |  Twitter Hashtag: #CRLIVE21

Please check out the materials from LIVE21 below, and cite the outputs as `Crossref Annual Meeting LIVE21, November 9, 2021 retrieved [date], [https://doi.org/10.13003/s0slxfq]`:  

- [YouTube recording](https://youtu.be/ca7YAXIPI70)  
- [Recording transcript](https://docs.google.com/document/d/1281VWQOCrw63L2kVn3BbDsJMMTB293rgnmB2zovq0Mg/edit)  
- [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1KDSFCf70hATsDzZXG_9ZjoLzFSZoM4g9IWIyE51lcZY/edit#gid=0)  
- [Google slides](https://docs.google.com/presentation/d/1f3-y_qV8ZOqCvNR53jsrEYIuVNM5INMjtBtrH5ic6mQ/present#slide=id.gf67b34ab3e_0_8) or  [pdf slides](https://doi.org/10.13003/YWEfp4ZM)   

[Board election results](/board-and-governance/elections)


<a id="2020"></a>
## 2020 annual meeting

10 November | Online |  Twitter Hashtag: #CRLIVE20  

Crossref turned 20 in 2020 and despite the challenges in the world lately, it felt like a natural milestone to take our annual meeting, LIVE20, online.  

Please check out the materials from LIVE20 below, and cite the outputs as `Crossref Annual Meeting LIVE20, November 10, 2020, retrieved [date], https://doi.org/10.13003/5gq8v1q`:

- [YouTube recording](https://youtu.be/h07dzm9wjPY)  
- [Recording transcript](https://docs.google.com/document/d/1va-zvNF0MimRuZruZ0l7eDXnGmipnk2H4twYrIJAQjc/edit#heading=h.pi9vqyntnz3o)  
- [Zoom Q + A transcript](https://docs.google.com/document/d/1Ou3ESpmNNBx9Gj4RCHaSFNYdgGGaZvF0q_TrSimIHf0)  
- [Google slides](https://docs.google.com/presentation/d/1sUxhTO1kSRTKDbfFm2ocwdwAuLWblEDPX4lcNBYs6qY/edit) or [pdf slides](/pdfs/LIVE20-annual-meeting-election.pdf)  

[Board election results](/board-and-governance/elections)


<a id="2019"></a>
## 2019 annual meeting

13 - 14 November, 2019 | Tobacco Theater, Amsterdam  | Twitter Hashtag: #CRLIVE19  

Theme: `Have your say` where and it included an afternoon of scene-setting talks from Ed on our strategy and trends, and Ginny on research into the value of Crossref.

**Day 1 - Recording**   
- [Recorded sessions from November 13, 2019](https://www.youtube.com/playlist?list=PLe_-TawAqQj1-MtymG5FrvLneIbmtX8Bc)  
**Slides**  
- [Ed Pentz and Ginny Hendricks - "Welcome", "Perceived value of Crossref", "Strategic scene-setting" (PDF)](/pdfs/LIVE19-all-slides.pdf)  
- [Catriona Maccallum of Hindawi - “In our own words: Hindawi” (PDF)](/pdfs/LIVE19_maccallum.pdf)  
- [Todd Toler of Wiley - “Crossref’s Value in an era of Open Science” (PDF)](/pdfs/LIVE19_toler.pdf)  
- [Anna Danilova of Ukrinformauka - “Cooperation of SA “Ukrinformnauka” with Crossref (PDF)](/pdfs/LIVE19_danilova.pdf)  
- [Christian Gutknecht of the Swiss National Science Foundation - “Crossref from a funder perspective” (PDF)](/pdfs/LIVE19_gutknecht.pdf)  
- [Ludo Waltman of CWTS at Leiden University - “Researcher and metadata user view” (PDF)](/pdfs/LIVE19_waltman.pdf)  

**Day 2 - Recording**  
- [Recorded sessions November 14, 2019](https://www.youtube.com/playlist?list=PLe_-TawAqQj30VIBVYg0dfyV_80Tnyqnj)  
**Slides**  
- [Slides for Workshop 1](/pdfs/LIVE19_workshop_1_combined.pdf)  
- [Slides for Workshop 2](/pdfs/LIVE19_workshop_2_combined.pdf)  
- [Slides for Workshop 2](/pdfs/LIVE19_workshop_3_combined.pdf)  
- [Slides for the Workshop 'rollup'](/pdfs/LIVE19_workshop_activity_betting_rollup.pdf)   

Get the feel of the event by listening to talks from the session entitled “In their own words...” from: **Todd Toler of Wiley, Catriona Maccallum of Hindawi, Anna Danilova of Ukrinformauka, Christian Gutknecht of the Swiss National Science Foundation, and Ludo Waltman of CWTS at Leiden University.** The podcast also includes some of our guest’s comments about the value of Crossref.     

<iframe title="Crossref #CRLIVE19 talks and guest reactions" height="100" width="100%" style="border: none;" scrolling="no" data-name="pb-iframe-player" src="https://www.podbean.com/media/player/svc5f-d0e615?from=yiiadmin&download=1&version=1&skin=1&btn-skin=107&auto=0&share=1&fonts=Helvetica&download=1&rtl=0&pbad=1"></iframe>  

> Please note there will be no in-person event around the November 2020 annual election, which will instead be announced virtually. We want to take stock of the feedback from LIVE19, gather more from others throughout 2020, and following up on the value research outputs we'll be ready to report progress in 2021. Please check to see if there will be a [LIVE local](/events) near you instead.


<a id="2018"></a>
## 2018 annual meeting

13 - 14 November, 2018 | Toronto Reference Library, Canada  | Twitter Hashtag: #CRLIVE18  

Theme: `How good is your metadata?` where Crossref staff and invited speakers inspired us with their metadata tales of woe and wonder. It was two full days packed with a mixture of plenary sessions, the results of our members' newly elected board members, and interactive activities.
- [CrossrefLIVE18.sched.com agenda](https://crossreflive18.sched.com)
- [#CRLIVE18 Twitter stream](https://twitter.com/hashtag/crlive18)
- [Recordings from Day 1](https://www.youtube.com/playlist?list=PLe_-TawAqQj2QMxKbOmBs4WFHnIAK4iwn)
- [Recordings from Day 2](https://www.youtube.com/playlist?list=PLe_-TawAqQj2W3Lc_hMxz1CDZckbOEYnO)

<a id="2017"></a>
## 2017 annual meeting

14 - 15 November, 2017 | 8:00AM - 5:45PM | Hotel Fort Canning, Singapore | Twitter Hashtag: #CRLIVE17  

Theme: We focused on the theme of `Metadata + Relations + Infrastructure = Context`. We had our fullest program yet, with the broadest representation yet - researchers, editors, journal and book people, publishing execs, librarians, and funders. We talked a lot about relationships: between metadata; between research outputs; and between the scholarly community.  

### Agenda

**Tuesday, November 14**

08:00 Registration  
09:00 Ed Pentz: Year in Review & Strategy Introduction  [Video Recording](https://www.youtube.com/watch?v=NR7wrq253dM&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
09:30 Jennifer Lin: Metadata for the Scholarly Map [Video Recording](https://www.youtube.com/watch?v=_BMpC-tpcuU&t=0s&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa&index=2)  
10:00 Theodora Bloom: Handling the Dynamic Nature of Scholarly Communications [Video Recording](https://www.youtube.com/watch?v=iFbBIxEhHAo&index=3&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
10:30 Break  
11:00 Amanda Bartell, Susan Collins, Ginny Hendricks: How to Win at Being a Crossref Membern [Video Recording](https://www.youtube.com/watch?v=OkBMyGweVyE&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa&index=4)  
Vanessa Fairhurst, Rachael Lammey: Reaching our International Community [Video Recording](https://www.youtube.com/watch?v=2GKl9eBAvzI&index=5&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
Madeleine Watson and Patricia Feeney: Relations, Translations, & Versions - Oh My! [Video Recording](https://www.youtube.com/watch?v=qe42TX9--yw&index=6&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
Madeleine Watson and Chuck Koscher: This New Metadata Manager Will Change Your Life [Video Recording](https://www.youtube.com/watch?v=J7crUX-2Uv4&index=7&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
12:30 Lunch  
13:30 Yin-Leng Theng: ARIA: A Scholarly Metrics Information System for Universities [Video Recording](https://www.youtube.com/watch?v=NFT7dEujhV4&index=8&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
Mark Patterson: I4OC: The Initiative for Open Citations  [Video Recording](https://www.youtube.com/watch?v=noZUVdnLgec&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa&index=9)  
Nicholas Bailey: What does data science tell us about social challenges in scholarly publishing?  [Video Recording](https://www.youtube.com/watch?v=dZ6jVDChH7Q&index=10&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
Jennifer Kemp and Madeleine Watson: Exploring relationships with Event Data [Video Recording](https://www.youtube.com/watch?v=jfq61IJPwJc&index=11&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
15:00 Break  
15:30 P Showraiah, Venkatraman Anandalkshmn, Brandon Koh, Alisha Ramos: Singapore Three Minute Thesis (3MT) competition finalists  [Video Recording](https://www.youtube.com/watch?v=EZdV2T71zrE&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa&index=12)  
15:45 Casey Greene: Research and Literature Parasites in a Culture of Sharing  [Video Recording](https://www.youtube.com/watch?v=uk3DMWZLrBM&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa&index=13)  
16:25 John Chodacki: Metadata 2020: What Could Richer Metadata Enable?  [Video Recording](https://www.youtube.com/watch?v=ZAz-cYA6504&index=14&list=PLe_-TawAqQj0wijTadno-Qremr2jqUESa)  
16:45 Lisa Hart Martin: Annual Meeting & Board Election  
17:15 Ed Pentz, P Showraiah, Jean Christophe & We Are CONfidence: Drinks Reception & Entertainment  

**Wednesday, November 15**  
08:30 Coffee  
09:00 Geoffrey Bilder: Information Trust: Metadata = provenance; provenance = key to trust [Video Recording](https://www.youtube.com/watch?v=8doHwexxV7I&index=1&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO)  
09:30 Doreen Liu: Scholarly Publishing in Asia today [Video Recording](https://www.youtube.com/watch?v=GVAsNT0T0vQ&index=2&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO)  
10:00 Trevor Lane: Introduction to COPE & Research Integrity [Video Recording](https://www.youtube.com/watch?v=kz3xF22TvPY&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=3)  
10:20 Miguel Escobar Varela: Digital Humanities in Singapore: some thoughts for the future [Video Recording](https://www.youtube.com/watch?v=Z7keM0D-zbo&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=4)  
10:40 Break  
11:10 Geoffrey Bilder: You'll never guess who uses Crossref metadata the most [Video Recording](https://www.youtube.com/watch?v=IjhvIIimz6c&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=5)  
11:30 Drop-in: Trevor Lane: Bring Your Ethics Issues to Discuss with COPE
Drop-in: Gareth Malcolm and Madeleine Watson: our Similarity Check Questions Answered  Drop-in: Jennifer Lin: Bring your Crossmark questions around updates and corrections  
12:30 Lunch  
13:30 Jennifer Kemp: Introducing the Crossref Plus service [Video Recording](https://www.youtube.com/watch?v=PDKhsksw1kQ&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=6)  
13:50 Alan Rutledge: Anatomy of a personalized research recommendation engine [Video Recording](https://www.youtube.com/watch?v=aQSJzS7zvpY&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=7)  
14:00 Kuansan Wang: Democratize access to scholarly knowledge with AI [Video Recording](https://youtu.be/lIjFsL7s3Xo)  
14:15 Lenny Teytelman: Call to Reduce Random Collisions with Information [Video Recording](https://www.youtube.com/watch?v=Ta2M_gkgeKI&index=9&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO)  
14:45 Break  
15:15 Ed Pentz: Crossref Strategic Outlook [Video Recording](https://www.youtube.com/watch?v=o2FuKAuYFQo&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=10)  
15:40 Paul Peters: The OI Project: Disambiguating affiliation names [Video Recording](https://youtu.be/SEhNDgOSyQI)  
15:55 Adam Hyde: Developing an open source publishing ecosystem founded on community collaboration [Video Recording](https://www.youtube.com/watch?v=SEhNDgOSyQI&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO&index=11)  
16:15 Liz Allen: New models of publishing research outputs: the importance of infrastructure [Video Recording](https://www.youtube.com/watch?v=WKeoe5mbBQk&index=13&list=PLe_-TawAqQj16f8DRwCADugYIaaXN_fZO)  
16:40 Ed Pentz: Wrap-up & Key Takeaways  

---

<a id="2016"></a>
## 2016 annual meeting

2 November, 2016 | 8:30AM - 6PM | The Royal Society, London, UK | Twitter Hashtag: #LIVE16  

Theme:  `Smart alone, brilliant together`

### Agenda  
08:30 – 10:00 Registration and Breakfast  
10:00 – 10:30 Dario Taraborelli, Wikimedia: Citations for the sum of all human knowledge (as linked open data) [Video Recording](https://www.youtube.com/watch?v=n2DdObO7kkQ&index=10&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&t=17s)  
10:30 – 11:00 Ian Calvert, Digital Science: "You don't have metadata  [Video Recording](https://www.youtube.com/watch?v=vEvvb70TfiE&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=9)  
11:00 – 11:30 Break  
11:30 – 11:50 Ed Pentz: Crossref’s outlook & key priorities [Video Recording](https://www.youtube.com/watch?v=AqRXTibbkPk&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=8)  
11:50 – 12:10 Ginny Hendricks: A vision for membership [Video Recording](https://www.youtube.com/watch?v=PCncFB3_MTo&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=7)  
12:10 – 12:30 Lisa Hart Martin: The meaning of governance [Video Recording](https://www.youtube.com/watch?v=9IhNRQe1NTA&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=5)  
12:30 – 13:00 Business meeting & Election results, with Crossref's Chair and Treasurer.  Moderators: Lisa Hart Martin  
13:00 – 14:00 Join your colleagues for a hot lunch  
14:00 – 14:20 Geoffrey Bilder: The case of the missing leg [Video Recording](https://www.youtube.com/watch?v=oo9eOG1607c&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=6)  
14:20 – 14:40 Jennifer Lin: New territories in the Scholarly Research Map [Video Recording](https://www.youtube.com/watch?v=1-CmPIbwoUI&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=4)  
14:40 – 15:00 Chuck Koscher: Relationships and other notable things [Video Recording](https://www.youtube.com/watch?v=IMQoKMCoCXE&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=3)  
15:00 – 15:30 Break  
15:30 – 16:00 Carly Strasser, Gordon and Betty Moore Foundation:  Funders and Publishers as Agents of Change  [Video Recording](https://www.youtube.com/watch?v=3Ru0i3t0fjg&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=2)  
16:00 – 16:30 April Hathcock, New York University:  Opening Up the Margins [Video Recording](https://www.youtube.com/watch?v=A2c4N6f3lxg&list=PLgi6QpztxCJrblA4FDGmDfA4t9nLDR0JF&index=1)  
16:30 – 16:45 Closing Remarks  
16:45 – 18:00 Reception  

Get a flavor of the event by listening to three of the most popular talks from 2016. The podcast also includes some of our guest's comments about the talks, including **Crossref's Geoffrey Bilder** on our plans for a new organization identifier registry, **Dario Taraborelli from the Wikimedia Foundation**, and **April Hathcock of New York University**.  

<iframe src="https://www.podbean.com/media/player/famyg-6b4fb5?from=yiiadmin&skin=1&btn-skin=112&share=1&fonts=Helvetica&auto=0&download=1" height="100" width="100%" frameborder="0" scrolling="no" data-name="pb-iframe-player"></iframe>

---

<a id="2015"></a>
## 2015 annual meeting  

18 November, 2015 | 8:30AM - 5PM | The Taj Hotel, Boston, MA, USA | Twitter Hashtag: #crossref15  

### Agenda  
08:30 Registration and Breakfast  
09:00 (Optional) Taxonomies Interest Group  
09:00 (Optional) Business Meeting  
09:30 Registration and Breakfast  
09:55 Welcome  
10:00 Marc Abrahams, Improbable Research : Improbable research, the Ig Nobel Prizes, and you: [Presentation](http://www.slideshare.net/CrossRef/marc-abrahams-improbable-research-and-the-ig-nobel-prizes-crossref) | [Video Recording](https://crossref.wistia.com/medias/hbv5wvhxpa)  
10:45 Juan Pablo Alperin, Public Knowledge Project and Crossref: Two P's in a Cross [Presentation](http://www.slideshare.net/CrossRef/juan-pablo-alperin-crossref15) | [Video Recording](https://crossref.wistia.com/medias/bdqqwn6wda)  
11:30 Break  
12:00 Ed Pentz: Executive Update  [Presentation](http://www.slideshare.net/CrossRef/ed-pentz-crossref15) | [Video Recording](https://crossref.wistia.com/medias/i3axus3okd)  
12:20 Ginny Hendricks: Outreach & Brand   [Presentation](http://www.slideshare.net/CrossRef/ginny-hendricks-crossref15) | [Video Recording](https://crossref.wistia.com/medias/0y4a23a63b)  
12:45 Lunch  
13:45 Jennifer Lin: Products & Services [Presentation](http://www.slideshare.net/CrossRef/jennifer-lin-crossref15) | [Video Recording](https://crossref.wistia.com/medias/ffn23ezfbg)  
14:15 Chuck Koscher: The Metadata Engine  [Presentation](http://www.slideshare.net/CrossRef/chuck-koscher-crossref15) | [Video Recording](https://crossref.wistia.com/medias/bqnkeepdfb)  
14:35 Geoffrey Bilder: Strategic Initiatives  [Presentation](http://www.slideshare.net/CrossRef/geoffrey-bilder-crossref15) | [Video Recording](https://crossref.wistia.com/medias/w5eftc2u7m)  
15:00 Break  
15:30 Scott Chamberlain, rOpenSci: Thinking programmatically  [Video Recording](https://crossref.wistia.com/medias/w5eftc2u7m)  
16:15 Martin Eve, Birbeck, University of London: Open Access & The Humanities  [Presentation](https://web.archive.org/web/20170127064418/https://www.martineve.com/presentations/2015-CrossRef/#/) | [Video Recording](https://crossref.wistia.com/medias/i2xbeyq69j)  
17:00 Ed Pentz: Closing Remarks  
17:05 Champagne Reception  

---

<a id="2014"></a>
## 2014 annual meeting  

12 November, 2012 | 8:30AM - 6:30PM | The Royal Society, London, UK | Twitter Hashtag: #crossref14  

### Agenda  
8:30 - 10:00 Registration and Breakfast  
9:15 - 9:45 Corporate Annual Meeting for Members and Board Election (Ian Bannerman, Chair, Board of Directors, Bernard Rous, Treasurer, Ed Pentz, Executive Director, Lisa Hart, Secretary)  
10:00 - 10:20 Ed Pentz, Executive Director: Main Open Meeting, Introduction and CrossRef Overview  [Presentation](https://www.slideshare.net/CrossRef/pentz-2014-crossrefannualmeeting) | [Video Recording](http://zeeba.tv/media/conferences/crossref-2014/0201-Ed-Pentz/)  
10:20 - 10:40 Chuck Koscher, Director of Technology: System Update  [Presentation](http://www.slideshare.net/CrossRef/csk-system-update2014amm) | [Video Recording](http://zeeba.tv/system-update/)  
10:40 - 11:00 Geoffrey Bilder, Director of Strategic Initiatives: Strategic Initiatives Update  [Video Recording](http://zeeba.tv/strategic-initiatives-update-4/)  
11:00 - 11:30 Break  
11:30 - 12:15 Branding, Carol Anne Meyer, Business Development and Marketing:  CrossRef Flash Update  [Presentation](http://www.slideshare.net/CrossRef/1-2014-crossrefannualmeetingflashupdatebrandingcarolannemeyer) | [Video Recording](http://zeeba.tv/branding/)  
Rachael Lammey, Product Manager: CrossCheck & CrossRef Text and Data Mining [Presentation](http://www.slideshare.net/CrossRef/2-flash-presentations-for-annual-meeting-tdm-and-cross-check-final) | [Video Recording](http://zeeba.tv/crosscheck-crossref-text-and-data-mining/)  
Kirsty Meddings, Product Manager: CrossMark & FundRef  [Presentation](http://www.slideshare.net/CrossRef/2014-42765116) | [Video Recording](http://zeeba.tv/crossmark-fundref/)  
Karl Ward, R&D Programmer: CrossRef Metadata Search
[Presentation](http://www.slideshare.net/CrossRef/4-metadata-search) | [Video Recording](http://zeeba.tv/crossref-metadata-search/)  
Ed Pentz, Executive Director: ORCID  
[Presentation](http://www.slideshare.net/CrossRef/orcid-flash) | [Video Recording](http://zeeba.tv/orcid/)  
12:15 - 13:15 Lunch  
13:15 - 14:15 Keynote: Laurie Goodman, PhD., GigaScience: Ways and Needs to Promote Rapid Data Sharing  [Presentation](http://www.slideshare.net/CrossRef/pentz-2014-crossrefannualmeeting) | [Video Recording](http://zeeba.tv/keynote-ways-and-needs-to-promote-rapid-data-sharing/)   
14:15-14:45 Break  
14:45-16:00 Moderator: Carol Anne Meyer, Business Development and Marketing: Improving Peer Review Panel  
Adam Etkin, PRE: Securing Trust & Transparency in Peer Review   [Presentation](http://www.slideshare.net/CrossRef/adam-etkin-crossrefpre) | [Video Recording](http://zeeba.tv/securing-trust-transparency-in-peer-review/)
bioRxiv: the preprint server for biology: Richard Sever, Cold Spring Harbor Laboratory Press  [Presentation](http://www.slideshare.net/CrossRef/richard-sever-crossref-bio-rxiv-presentation) | [Video Recording](http://zeeba.tv/biorxiv-the-preprint-server-for-biology/)  
Mirjam Curno, Frontiers: Frontiers’ Collaborative Peer Review   [Presentation](http://www.slideshare.net/CrossRef/mirjam-curno-frontiers141108) | [Video Recording](http://zeeba.tv/frontiers%e2%80%99-collaborative-peer-review/)  
Janne-Tuomas Seppänen, Peerage of Science: Do it once, do it well – questioning submission and peer review traditions   [Presentation](http://www.slideshare.net/CrossRef/2014-crossref-annual-meeting) | [Video Recording](http://zeeba.tv/do-it-once-do-it-well-%e2%80%93-questioning-submission-and-peer-review-traditions/)  
16:00-17:00 Richard A. Jefferson, Cambia: Innovation Cartography: Creating impact from scholarly research requires maps not metrics  [Video Recording](http://zeeba.tv/closing-keynote-innovation-cartography-creating-impact-from-scholarly-research-requires-maps-not-metrics/)  
17:00-17:15 Wrap Up  
17:15-18:15 Cocktail reception  

---

<a id="2013"></a>
## 2013 annual meeting  

13 November, 2013 | 8:30AM - 5PM | The Charles Hotel, Cambridge, MA, USA | Twitter Hashtag: #crossref13  

### Agenda  
8:30 - 10:00 Registration and Breakfast  
9:15 - 9:45 Corporate Annual Meeting for Members and Board Election  
–– Bernie Rous, Treasurer, Board of Directors  
–– Ed Pentz, Executive Director  
–– Lisa Hart, Secretary  
10:00 - 10:20 Ed Pentz, Executive Director: Main Open Meeting: Introduction and CrossRef Overview  [Video recording & presentation](http://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-crossref-overview-edpentz)  
10:20 - 10:40 Chuck Koscher, Director of Technology: System Update  [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annualmeetingsystemupdatechuckkoscher)  
10:40 - 11:00 Geoffrey Bilder, Director of Strategic Initiatives : Strategic Initiatives Update [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-strategicupdategeoffreybilder)  
11:00 - 11:30 Break  
 11:30 - 12:15 CrossRef Flash Update  
–– Carol Anne Meyer, Business Development and Marketing: Branding [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annualmeetingflashupdatebrandingcarolannemeyer)  
–– Rachael Lammey, Product Manager: CrossCheck & CrossMark  [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annualmeetingflashupdatecrosscheckandcrossmarkrachaellammey)  
–– Kirsty Meddings, Product Manager: FundRef   [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annualmeetingflashupdatefundrefkirstymeddings)  
–– Karl Ward, R&D Programmer: CrossRef Metadata Search   [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annualmeetingflashmetadatakarlward)  
–– Ed Pentz, Executive Director: ORCID  [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-flash-update-orcid-edpentz)  
12:15 - 13:15 Lunch  
13:15 - 14:15 Keynote: Heather Piwowar, co-founder Impactstory: Building skyscrapers with our scholarship  [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-building-skyscrapers-heatherpiwowar)  
14:15 - 14:45 Kristen Fisher Ratan, PLOS: Agile Publishing: responding to the changing requirements in scholarly communication  [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-agile-publishing-kristen-ratan)  
14:45 - 15:15 Break  
15:15 - 16:00 Walter Warnick, Director of the U.S. Department of Energy Office of Scientific and Technical Information (OSTI): How CrossRef has Accelerated Science and Its Promise for the Future: A Federal Perspective   [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meeting-walter-warnick)  
16:00 - 16:45 CLOCKSS and Portico, Randy S. Kiefer, CLOCKSS and Kate Wittenberg, PORTICO: Archiving Publishing Panel: United on Preservation   [Video recording & presentation](https://www.slideshare.net/CrossRef/2013-crossref-annual-meetingunitedinpreservationrandykieferkatewittenberg)  
16:45 - 17:00 Wrap Up  

---

<a id="2012"></a>
## 2012 annual meeting  

14 November, 2012 | 8:30AM - 6:30PM | The Royal Society, London, UK | Twitter Hashtag: #crossref12  

### Agenda  
8:30 - 10:00 Registration and Breakfast  
9:15 - 9:45 Corporate Annual Meeting for Members and Board Election  
–– Linda Beebe, Chair, Board of Directors  
–– Ian Bannerman, Treasurer, Board of Directors  
–– Lisa Hart, Secretary  
–– Ed Pentz, Executive Director  
10:00 - 10:20 Ed Pentz, Executive Director: Main Open Meeting, Introduction and CrossRef Overview [Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-crossref-overview-ed-pentz) | [Video Recording](http://zeeba.tv/introduction-and-crossref-overview-2/)  
10:20 - 10:40 Chuck Koscher, Director of Technology: System Update  [Video Recording](http://zeeba.tv/system-update-2012/)  
10:40 - 11:00 Geoff Bilder, Director of Strategic Initiatives:  Strategic Initiatives Update  [Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-crossref-overview-ed-pentz) | [Video Recording](http://zeeba.tv/strategic-initiatives-update-3/)  
11:00 - 11:30 Break  
11:30 - 12:00 Laure Haak, ORCID:  The Role of ORCID in the Research Community  [Presentation](https://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-orcid-laure-haak) | [Video Recording](http://zeeba.tv/the-role-of-orcid-in-the-research-community/)  
12:00 - 13:00 Lunch  
13:00 - 14:00 Jason Scott, Archive Team:  CITIES ON THE EDGE OF NEVER: Life in the Trenches of the Web in 2012  [Video Recording](http://zeeba.tv/cities-on-the-edge-of-never-life-in-the-trenches-of-the-web-in-2012/)  
14:00-15:00 Carol Anne Meyer, Business Development and Marketing: Global Publishing Panel: Perspectives of using CrossRef from publishers in Lithuania, Brazil, South Korea and China  
––Eleonora Dagiene, Vilnius Gediminas University Press [Presentation](https://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-global-panel-eleonora-dagiene) | [Video Recording](http://zeeba.tv/crossref-in-lithuania-eastern-europe/)  
––Edilson Damasio, Universidade Estadual de Maringá – UEM - Eduem
[Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-global-panel-edilson-damasio) | [Video Recording](http://zeeba.tv/crossref-in-brazil/)  
––Choon Shil Lee, Sookmyung Women’s University, KAMJE
[Presentation](https://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-global-panel-choon-shil-lee) | [Video Recording](http://zeeba.tv/crossref-in-korea-what-we-do-at-kamje/)  
––Yan Shuai, Tsinghua University Press (TUP)
[Presentation](https://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-global-panel-yan-shuai) | [Video Recording](http://zeeba.tv/scholarly-publishing-and-crossref-in-china/)  
15:00 - 15:30 FundRef: In response to the need to standardize the collection and display of funding information for scholarly publications, CrossRef officially launched the FundRef project in March of 2012. Four funding agencies and seven publishers are working together to carry out a pilot project, with the goal of developing and demonstrating a community-wide solution. The pilot group plans to issue recommendations for full integration of funding information in early 2013.  
––Fred Dylla, American Institute of Physics (AIP) [Presentation](https://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-fundref-fred-dylla) | [Video Recording](http://zeeba.tv/progress-report-on-the-fundref-initiative/)  
––Kevin Dolby, Wellcome Trust [Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-fundref-kevin-dolby) | [Video Recording](http://zeeba.tv/fundref-%e2%80%93-kevin-dolby/)  
15:30 - 16:00 Break  
16:00 - 16:30 Rachael Lammey, Product Manager:  CrossCheck and CrossMark Update   [Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-crosscheck-crossmark-rachael-lammey) | [Video Recording](http://zeeba.tv/crosscheck-and-crossmark-update/)  
16:30 - 17:00 Virginia Barbour, PLOS, Committee on Publications Ethics (COPE):  Plagiarism as seen from the editors’ perspective  [Presentation](http://www.slideshare.net/CrossRef/crossref-annual-meeting-2012-cope-plagiarism-virginia-barbour) | [Video Recording](http://zeeba.tv/plagiarism-as-seen-from-the-editors-perspective/)  
17:00 - 17:15 Wrap Up  
17:15 - 18:30 Cocktail Reception  

---

<a id="2011"></a>
## 2011 annual meeting

15 November, 2011 | 8:30AM - 6:30PM | The Charles Hotel, Cambridge, MA, USA | Twitter Hashtag: #crossref11

### Agenda
8:30 - 10:00 Registration and Breakfast  
9:00 - 9:45 Corporate Annual Meeting for Members and Board Election  
–– Linda Beebe, Chair, Board of Directors  
–– Ian Bannerman, Treasurer, Board of Directors  
–– Ed Pentz, Executive Director  
10:00 - 10:20 Main Open Meeting, Introduction and Crossref Overview, Ed Pentz, Executive Director  
[Presentation](https://web.archive.org/web/20121111040438/http://www.slideshare.net/Crossref/introduction-and-crossref-overview-2011-crossref-annual-meeting) | [Video Recording](http://zeeba.tv/crossref-2011-introduction-and-crossref-overview/)  
10:20 - 10:40 System Update, Chuck Koscher, Director of Technology [Presentation](https://www.slideshare.net/CrossRef/system-update-chuckkoscher) | [Video Recording](http://zeeba.tv/system-update-2011/)  
10:40 - 11:00 Strategic Initiatives Update, Geoff Bilder, Director of Strategic Initiatives [Video Recording](http://zeeba.tv/strategic-initiatives-update-2/)  
11:00 - 11:30 Break  
11:30 - 11:45 Crossref Member Obligations (including Display Guidelines), Carol Anne Meyer, Business Development and Marketing [Presentation](https://www.slideshare.net/CrossRef/member-obligations-2011-crossref-annual-meeting?qid=dd4211ee-e007-4d8a-aaf9-c0d4834f6717&v=&b=&from_search=1) | [Video Recording](http://zeeba.tv/crossref-member-obligations-including-display-guidelines/)  
11:45 - 12:15 CrossMark Update  Evan Owens, American Institute of Physics [Presentation] | [Video Recording](https://www.youtube.com/watch?v=Q4ViU89O6Y0)  
Kirsty Meddings, Product Manager [Presentation](https://www2.slideshare.net/CrossRef/crossmark-update-241350156) | [Video Recording](http://zeeba.tv/crossmark-under-the-hood/)  
12:15 - 12:45 ORCID Update, Howard Ratner, Nature Publishing Group [Presentation](http://www.slideshare.net/Crossref/orcid-howard-ratner) | [Video Recording](http://zeeba.tv/orcid-update-2/)  
12:45 - 13:15 DataCite: the Perfect Complement to Crossref, James Mullins, Purdue University  [Presentation](http://www.slideshare.net/Crossref/data-cite-jamesmullins) | [Video Recording](http://zeeba.tv/datacite-the-perfect-complement-to-crossref/)  
13:15 - 14:15 Lunch  
14:15 - 15:15 Sex and the Scientific Publisher: How Journals and Journalists Collude (despite their best intentions) to Mislead the Public, Ellen Ruppel Shell, Boston University Center for Science & Medical Journalism  [Presentation](http://www.slideshare.net/Crossref/keynote-ellen-ruppelshell) | [Video Recording](http://zeeba.tv/sex-and-the-scientific-publisher-how-journals-and-journalists-collude-despite-their-best-intentions-to-mislead-the-public/)  
15:15-15:45 The Persistence of Error: A Study of Retracted Articles on the Internet, Phil Davis, Publishing Consultant  [Presentation](http://www.slideshare.net/Crossref/retracted-articles-studyphildavis) | [Video Recording](http://zeeba.tv/the-persistence-of-error-a-study-of-retracted-articles-on-the-internet/)  
- 16:15 Break  
16:15-16:45 Results from global journal editor survey on detecting plagiarism, Helen (Y.H) ZHANG, JZUS (Journal of Zhejiang University-SCIENCE) [Video Recording](http://zeeba.tv/results-from-global-journal-editor-survey-on-detecting-plagiarism/)  
16:45 - 17:15 The Good, the Bad and the Ugly: What Retractions Tell Us About Scientific Transparency, Ivan Oransky, Retraction Watch [Presentation](http://www.slideshare.net/Crossref/the-good-the-bad-and-the-ugly-what-retractions-tell-us-about-scientific-transparency-2011-annual-meeting) | [Video Recording](https://www.youtube.com/watch?v=8rllQ5PQHKM)  
17:15 - 17:30 Wrap up  
17:30 - 18:30 Cocktail Reception  

---

<a id="2010"></a>
## 2010 annual meeting

16 November, 2010 | 8:30AM - 6PM | One Great George Street, London, UK | Twitter Hashtag: #crossref10

> Credibility in Scholarly Communications

### Agenda
8:30 - 9:30 Registration and Breakfast  
9:00 - 9:45 Corporate Annual Meeting for Members and Board Election  
–– Bob Campbell, Chair, Crossref Board of Directors  
–– Linda Beebe, Treasurer, Crossref Board of Directors [Video Presentation](http://zeeba.tv/crossref-financial-review/)  
–– Ed Pentz, Executive Director [Video Presentation](http://zeeba.tv/crossref-11th-annual-meeting/)  
10:00 - 10:15 Main Open Meeting, Introduction and Crossref Overview, Ed Pentz, Executive Director [Video Presentation](http://zeeba.tv/introduction-and-crossref-overview/)  
10:15 - 10:30 System Update, Chuck Koscher, Director of Technology [Video Presentation](http://zeeba.tv/system-update-2010/)  
10:30 - 10:45 Strategic Initiatives Update,  Geoff Bilder, Director of Strategic Initiatives [Video Presentation](http://zeeba.tv/strategic-initiatives-update/)  
10:45 - 11:00 CrossCheck Kirsty Meddings, Product Manager [Video Presentation](http://zeeba.tv/crosscheck/)  
11:00 - 11:30 Break  
11:30 - 11:35 Transparency in Funding Sources, H. Frederick Dylla, American Institute of Physics [Video Presentation](http://zeeba.tv/transparency-in-funding-sources/)  
11:35 - 12:10 CrossMark Prototype Demo, Carol Anne Meyer, Business Development and Marketing [Video Presentation](http://zeeba.tv/crossmark-prototype-demo/)  
12:10 - 12:30 ORCID Update, Howard Ratner, Nature Publishing Group [Video Presentation](http://zeeba.tv/orcid-update/)  
12:30 - 1:30 Lunch  
13:30 - 14:30 Making sense of science and evidence, Tracey Brown, Sense About Science [Video Presentation](http://zeeba.tv/making-sense-of-science-and-evidence/)  
14:30 - 15:00 Which scientists can we trust? Christine Ottery, Science Journalist [Video Presentation](http://zeeba.tv/which-scientists-can-we-trust/)  
15:00 - 15:30 Break  
15:30 - 16:00 Scholarly eBooks - Improving discoverability and usage, Carol Anne Meyer, Business Development and Marketing Manager [Video Presentation](http://zeeba.tv/scholarly-ebooks-improving-discoverability-and-usage/)  
16:00 - 17:00 Publishing Data alongside Analysis: a case study from OECD, Toby Green, Organisation for Economic Co-operation and Development (OECD) [Video Presentation](http://zeeba.tv/publishing-data-alongside-analysis-a-case-study-from-oecd/)  
Communicating Data: New Roles for Researchers, Publishers and Libraries, MacKenzie Smith, Massachusetts Institute of Technology Libraries [Video Presentation](http://zeeba.tv/communicating-data-new-roles-for-researchers-publishers-and-libraries/)  
17:00 - 17:15 Wrap up  
17:15 - 18:00 Cocktail Reception  

---

<a id="2009"></a>
## 2009 annual meeting

10 November, 2009 | 8:30AM - 5PM | The Charles Hotel, Cambridge, MA, USA
Twitter Hashtag: #crossref09

### Agenda  
8:30 - 9:00 Registration and Breakfast  
9:00 - 10:00 Corporate Annual Meeting, Board Election and Strategy Overview
Bob Campbell, Chair, Crossref Board of Directors  
Linda Beebe, Treasurer, Crossref Board of Directors [Presentation](https://www.slideshare.net/Crossref/crossref-financial-review-2485290)
Ed Pentz, Executive Director [Presentation](https://www.slideshare.net/Crossref/crossref-strategy-overview-2480225)  
10:00 - 10:20 System Update, Chuck Koscher, Director of Technology [Presentation](https://www.slideshare.net/Crossref/crossref-system-update)  
10:20 - 10:50 Break  
10:50 - 11:15 Strategic Initiatives Update, Geoff Bilder, Director of Strategic Initiatives [Presentation](https://www.slideshare.net/Crossref/crossref-strategic-initiatives)  
11:15 - 12:00 Crossref DOIs in Use and Branding Guidelines, Carol Anne Meyer, Marketing and Business Development Manager [Presentation](https://www.slideshare.net/Crossref/crossref-dois-in-use-and-branding-guidelines)  
12:00 - 13:15 Lunch  
13:15 - 14:15 Trust, Communication and Academic Publication, Professor Onora O'Neill, Faculty of Philosophy, University of Cambridge (Baroness O'Neill of Bengarve)  
14:15 - 15:30 CrossCheck: Views from the Field. Kirsty Meddings, Product Manager [Presentation](https://www.slideshare.net/Crossref/crosscheck-update)  
Panelists:  
Phillip E. Canuto, Executive Editor, Cleveland Clinic Journal of Medicine [Presentation](https://www.slideshare.net/Crossref/review-articles-and-crosscheck-unscrambling-the-jigsaw-puzzle)  
Cathy Griffin, Journal of Bone and Joint Surgery [Presentation](https://www.slideshare.net/Crossref/crosscheck-in-use-at-the-jbjs)  
Howard Ratner, Nature Publishing Group [Presentation](https://www.slideshare.net/Crossref/nature-publishing-group-and-crosscheck)  
15:30 - 16:00 Break  
16:00 - 16:45 Plagiarism in the Academy: Now What Do We Do?, T. Scott
Pluchak Director, Lister Hill Library of the Health Sciences, University of Alabama at Birmingham [Presentation](https://www.slideshare.net/Crossref/plagiarism-in-the-academy)  
16:45 - 17:00 Wrap up  

---

<a id="2008"></a>
## 2008 annual meeting

18 November, 2008 | 9AM - 6PM | Lenox Hotel, Boston, MA

Theme: `Towards the Future of Scientific Communication`

### Agenda

9:00 - 10:00 Registration and coffee/tea
9:30 - 10:00 Corporate Annual Meeting, Board Election, and Strategy Overview - Bob Campbell, Chair, Crossref Board of Directors; Linda Beebe, Treasurer, Crossref Board of Directors; Ed Pentz, Executive Director  
10:00 - 10:20 Intro to Annual Member Meeting and Crossref Mission, Ed Pentz, Executive Director  
10:20 - 10:40 System Update, New Services, MyCrossref, Chuck Koscher, Crossref Technology Director  
10:40 - 11:00 CrossCheck Update, Ed Pentz, Executive Director  
11:00 - 11:30 Coffee and tea break  
11:30 - 12:15 New Strategic Initiatives, Geoff Bilder, Director of Strategic Initiatives  
12:15 - 13:30 Lunch  
13:30 - 14:15 Karen Hunter, Senior Vice President within the Global Academic and Customer Relations at Elsevier on Opportunities for Cooperation  
14:15 - 15:10 Jonathan Zittrain, author of and Co-Founder and Faculty Co-Director Berkman Center for Internet & Society on The Future of the Internet and How to Stop It  
15:10 - 15:30 Coffee and tea break  
15:30 - 16:15 John Wilbanks, VP of Science at Creative Commons, on Building a Knowledge Network  
16:15 - 17:15 Natalie Angier, Pulitzer Prize-winning author of, most recently, The Canon, will provide a fun and lively look at science literacy and how publishers aid and abet the cause  
17:15 - 17:30 Wrap up  
17:30 - 18:30 Cocktail reception  

---

<a id="2007"></a>
## 2007 annual meeting  

1 November, 2007 | 9AM - 6:30PM | The Royal College of Surgeons of England, London, UK  

### Agenda  

9:00 - 9:30 Registration and new member coffee  
9:30 - 10:00 Corporate Annual Meeting, Board Election, and Strategy Overview
  Anthony Durniak, Chair, Crossref Board of Directors  
  Bob Campbell, Vice-Chairman, Crossref Board of Directors
  Ed Pentz, Executive Director
10:00 - 10:20 System Update, New Services, Chuck Koscher, Crossref Technology Director  
10:20 - 10:40 Strategic Initiatives Update, Geoff Bilder, Crossref Director of Strategic Initiatives  
10:40 - 11:00 Coffee and tea break  
11:00 - 11:40 Dr. Kieron O'Hara. Electronic Publishing and Public Trust in Science. Senior Research Fellow, School of Electronics and Computer Science, University of Southampton.  
11:40 - 12:20 Alex Frost. Sermo as a model for online information sharing: relations to open science, open review, and post-publication review.Vice President for Research Initiatives, Sermo  
12:20 - 13:30 Lunch  
13:30 - 14:10 Dr. Ben Goldacre. On Popular Misunderstanding of Science. Medical Doctor who writes the Bad Science column in the Guardian.  
14:10 - 14:50 Richard Kidd. Project Prospect - Introducing Semantics into Chemical Science Publishing. Project Manager, Royal Society of Chemistry.  
14:50 - 15:10 Coffee and tea break  
15:10 - 15:50 Pritpal S Tamber. Faculty of 1000 Medicine: Post Publication Peer Recommendation. Managing Director, Faculty of 1000 Medicine.  
15:50 - 16:30 Edward Wates. Trustworthiness: Does the publisher have a role to play? UK Journal Production Director, Blackwell Publishing.  
16:30 - 17:30 Sally Morris. Quality and Trust in Scholarly Publishing. Editor-in-Chief, Learned Publishing, ALPSP. Audience questions and discussion.  
17:30 - 18:30 Cocktail reception

---

<a id="2006"></a>
## 2006 annual meeting

1 November, 2006 | 9AM-6PM | The Charles Hotel, Cambridge, MA, USA

> Building on Success

### Agenda

9:30 - 10:00 Registration and new member coffee  
10:00 - 10:30 Corporate Annual Meeting, Board Election, and Strategy Overview, Anthony Durniak, Chair, Crossref Board of Directors; Robert Campbell, Treasurer, Crossref Board of Directors; Ed Pentz, Executive Director  
10:30 - 10:50 System Update, New Services, and Data Quality Initiative
Chuck Koscher, Crossref Technology Director  
10:50 - 11:10 Board Committee Updates, with introduction by Amy Brand,
Director of Business and Product Development; Howard Ratner, Chair, CWS Committee; Bernard Rous, Chair, Institutional Repositories Committee  
11:10 - 11:30 Coffee and tea break    
11:30 - 12:15 KEYNOTE: Tim Berners-Lee, Director of the World Wide Web Consortium Identifying and describing things on the web  
12:15 - 13:40 Lunch  
**Changes in Research and their Impact on Publishing**  
13:40 - 14:20 DOIs for Biological Databases, Phil Bourne, Protein Data Bank    
14:00 - 14:40 Developments in Author Identification, Niels Weertman, Scopus; Taking the guesswork out of author searching, James Pringle, ISI; Smith, Lee, and Hirano T: How and Why to Find Authors  
14:40 - 15:00 Coffee and tea break  
15:00 - 15:40 The Future of Archiving, starting with the present, Michael Keller, Stanford  
15:40 - 17:30 "Building on Success" (moderated session)  
15:40 - 16:00 Introduction by Tony Durniak, Chair of Crossref  
16:00 - 17:00 Panel of Crossref members discussing possible future Crossref developments: Terry Hulbert, Institute of Physics Publishing; Richard Cave, Public Library of Science; Michael Krot, JSTOR; Carol Richman, Sage; Greg Suprock, Nature Publishing Group; Mark Doyle, American Physical Society  
17:00 - 17:30 Audience questions and discussion; wrap up  
17:30 - 18:30 Cocktail reception  

---

<a id="2005"></a>
## 2005 annual meeting  

15 November, 2005 | 9:30AM - 6PM | IOP, Portland Place, London, UK  

## Agenda

09:30 - 10:00 Registration, new members coffee  
10:00 - 10:45 Corporate annual meeting, board election  
10:45 - 11:30 Operational and strategic overview; Crossref Search update  
11:30 - 12:00 System report and new services
12:00 - 13:00 Lunch  
13:15 - 14:45 Digital preservation panel (Lynne Brindley, Johan Steenbakkers, Eileen Fenton)  
14:45 - 15:15 Coffee break  
15:15 - 15:45 Book DOI case study  
15:45 - 16:15 Developments in academic search tools  
16:15 - 16:45 Innovation in scientific publishing (Vitek Tracz)  
16:45 - 17:00 Closing remarks  
17:00 - 18:00 Cocktail reception  

---

<a id="2004"></a>
## 2004 annual meeting

9 November, 2004 | 10AM - 6PM | The Charles Hotel, Cambridge, MA, USA

### Agenda

9:30 - 10:00 Registration, "New Members Coffee", an opportunity for new members to meet staff, Board members and other members  
10:00 - 10:45 Corporate Annual Meeting/Board Election and Reports from Chair, Treasurer, and Executive Director  
10:45 - 11:30 Operational & Strategic Overview (The year in review; reports from committees)  
11:30 - 12:00 System Review & New Developments (Multiple Resolution, Stored Queries, Forward Linking)  
12:00 - 12:30 Crossref Search Update and Discussion  
12:30 - 13:30 Lunch at Rialto Restaurant (in Charles Hotel)  
13:45 - 14:30 A Crossref Case Study: DOIs and the secondary publisher - a match made in heaven? (Andrea Powell)  
14:30 - 15:00 Changing Routes to Content and Content Preservation in the Digital Age (Dale Flecker, Harvard University)  
15:00 - 15:30 Coffee Break  
15:30 - 16:00 The California Digital Library's eScholarship Program (Catherine Candee, CDL)  
16:00 - 16:30 The Semantic Web Initiative and its Implications for Publishing (Eric Miller, MIT)  
16:30 - 17:00 Intellectual Property Issues in Publishing Today (Allan Ryan Jr., Harvard Business School Publishing)  
17:00 Closing remarks  
17:00 - 18:00 Cocktail reception at Noir (in Charles Hotel lobby)  

---

<a id="2003"></a>
## 2003 annual meeting  

16 September, 2003 | 9AM - 4PM | IEE, Savoy Place, London, UK  

### Agenda

8:30 - 9:00 Registration, "New Members Coffee", an opportunity for new members to meet staff, Board members and "older" members  
9:00 - 10:00 Corporate Annual Meeting/Board Election  
10:00 - 12:30 Member Only Session  
10:00 - 10:20 Revised Membership Agreement and Business Development Review  
10:20 - 10:40 Crossref Search Recap and Crossref Financial Review  
10:40 - 11:05 Forward Linking  
11:05 - 11:25 Coffee Break  
11:25 - 11:50 Technical Update   
11:50 - 12:30 Strategic Discussion  
12:30 - 13:30 Lunch  
13:30 - 16:00 Open session, all welcome  
13:30 - 13:50 IDF Update (Norman Paskin)  
13:50 - 14:10 The library perspective: "The value of Crossref in an open access world" (Fred Friend, UCL)  
14:10 - 14:40 Developments at the British Library: "Preserving our Digital Heritage: the British Library Strategy and Plan for the 21st Century" (Richard Boulderstone)  
14:40 - 15:00 Coffee break  
15:00 - 15:30 DOI Case Study: Nature Publishing Group (Howard Ratner)  
15:30 - 16:00 Publisher Case Study: Blackwell Publishing (Jill Cousins): "Information Objects Are Hot, Documents Are Not"  

---

<a id="2002"></a>
## 2002 annual meeting

25-26 September, 2002 | Fairmont Copley Plaza Hotel, Boston, MA, USA

### Agenda, 25 September, 2002

8:30 - 9:00 Registration, Coffee  
9:00 - 10:00 Corporate Annual Meeting
  Call to Order - Eric Swanson, Chair, Board of Directors, Crossref
  Appointment of Inspector of Elections
  Opening Remarks - Eric Swanson, John Wiley & Sons
  Report from Executive Director - Ed Pentz, Executive Director, Crossref
  New Business  
10:00 Close of Corporate Annual Meeting  
10:00 -10:45 Main Session - Overview of New System - Chuck Koscher, Technical Director,
 Crossref, and Representatives from Atypon  
10:45 - 11:00  Coffee Break  
11:00 - 12:00   Crossref Search Prototype Demo and Feedback Forum - Ed Pentz, Executive  Director, Crossref  
12:00 - 13:30 Lunch  
13:30 - 16:00 Open Session  
13:15 - 13:45 Introduction to New System, Chuck Koscher, Crossref, and representatives from Atypon  
13:45 - 14:30 Speaker, Jim Neale, Columbia University, member Crossref Library Advisory Board. "What does the academic community want from Crossref?"  
14:30 - 14:45 Coffee Break  
14:45 - 15:15  Speaker, Jerry Cowhig, IOPP, Publisher Case Study  
15:15 - 16:00 Panel, "The Article Economy", with Wes Crews, Infotrieve, and Simon Inger, consultant, Simon Inger & Associates, on behalf of Ingenta; and Wes Crews, Infotrieve  

### Agenda, 26 September, 2002

Implementation Workshop -- Chuck Koscher, Crossref  
Implementing Reference Linking -- Mark Doyle, The American Physical Society  
Deposit Schema 2.0 -- Bruce D. Rosenblum, Inera Incorporated  

---

Please contact our [outreach team](mailto:feedback@crossref.org) with any questions.