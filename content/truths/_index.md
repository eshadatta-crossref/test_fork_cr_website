+++
title = "Our truths"
date = "2021-02-26"
draft = false
author = "Ginny Hendricks"
x-version = "0.0.0"

[menu.main]
rank = 4
parent = "About us"
weight = 18

+++

These are the Crossref truths. These are the principles that guide us in everything we do. They were tested with members and adopted by our board in 2016. We tweaked and added "What you see, what you get" in 2017.

{{% row %}}
{{% column %}}

{{% divwrap service-blue %}}
## Come one, come all
We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in many formats, and with all kinds of business models.
{{% /divwrap %}}

{{% divwrap service-yellow %}}
## One member, one vote
Help us set the agenda. It doesn’t matter how big or small you are, every member gets a single vote to create a board that represents all types of members.
{{% /divwrap %}}

{{% divwrap service-darkgrey %}}
## Smart alone, brilliant together
Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.
{{% /divwrap %}}

{{% /column %}}
{{% column %}}

{{% divwrap service-red %}}
## Love metadata, love technology
We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members’ content.
{{% /divwrap %}}

{{% divwrap service-lightgrey %}}
## What you see, what you get
Ask us anything. We’ll tell you what we know. Openness and transparency are principles that guide everything we do.
{{% /divwrap %}}

{{% divwrap service-blue %}}
## Here today, here tomorrow
We’re here for the long haul. Our obsession with persistence applies to all things---metadata, links, technology, and the organization. But “persistent” doesn’t mean “static”; as research communications continues to evolve, so do we.
{{% /divwrap %}}

{{% /column %}}
{{% /row %}}

---

Please let us know what you think by contacting our [outreach team](mailto:feedback@crossref.org).