+++
title = "Defunct DOI"
date = "2023-03-22"
author = "Shayn Smulyan"
Weight = 15
rank = 2
aliases = [ "/_deleted-doi", "/_deleted-doi/", "/deleted_DOI.html", "/deleted_DOI.html/",]
x-version = "0.0.0"

+++

This DOI is not currently attached to any metadata records.

DOIs can't actually ever be deleted (they're _persistent_), but sometimes our members create DOIs in error.

We do have a process to _approximate_ deletion which we follow only in rare cases where the DOI has been genuinely created in error, and most crucially, if the DOI has never been published anywhere online or in print and never otherwise distributed to or communicated with anyone (authors, readers, reviewers, etc.).

Try our [Metadata Search](https://search.crossref.org) tool to find what you're looking for.

---
Please consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org) with any questions.